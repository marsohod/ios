/*
 * MGSwipeTableCell is licensed under MIT license. See LICENSE.md file for more information.
 * Copyright (c) 2014 Imanol Fernandez @MortimerGoro
 */

#import "MGSwipeButton.h"

@class MGSwipeTableCell;

@implementation MGSwipeButton

+(instancetype) buttonWithTitle:(NSString *) title backgroundColor:(UIColor *) color
{
    return [MGSwipeButton buttonWithTitle:title icon:nil backgroundColor:color];
}

+(instancetype) buttonWithTitle:(NSString *) title backgroundColor:(UIColor *) color padding:(NSInteger) padding
{
    return [MGSwipeButton buttonWithTitle:title icon:nil backgroundColor:color padding:padding];
}

+(instancetype) buttonWithTitle:(NSString *) title backgroundColor:(UIColor *) color callback:(MGSwipeButtonCallback) callback
{
    return [MGSwipeButton buttonWithTitle:title icon:nil backgroundColor:color callback:callback];
}

+(instancetype) buttonWithTitle:(NSString *) title backgroundColor:(UIColor *) color padding:(NSInteger) padding callback:(MGSwipeButtonCallback) callback
{
    return [MGSwipeButton buttonWithTitle:title icon:nil backgroundColor:color padding:padding callback:callback];
}

+(instancetype) buttonWithTitle:(NSString *) title icon:(UIImage*) icon backgroundColor:(UIColor *) color
{
    return [MGSwipeButton buttonWithTitle:title icon:icon backgroundColor:color callback:nil];
}

+(instancetype) buttonWithTitle:(NSString *) title icon:(UIImage*) icon backgroundColor:(UIColor *) color padding:(NSInteger) padding
{
    return [MGSwipeButton buttonWithTitle:title icon:icon backgroundColor:color padding:padding callback:nil];
}

+(instancetype) buttonWithTitle:(NSString *) title icon:(UIImage*) icon backgroundColor:(UIColor *) color callback:(MGSwipeButtonCallback) callback
{
    return [MGSwipeButton buttonWithTitle:title icon:icon backgroundColor:color padding:10 callback:callback];
}

#define BUTTON_WIDTH 40.0

+(instancetype) buttonWithTitle:(NSString *) title icon:(UIImage*) icon backgroundColor:(UIColor *) color padding:(NSInteger) padding callback:(MGSwipeButtonCallback) callback
{
//    MGSwipeButton * button = [MGSwipeButton buttonWithType:UIButtonTypeCustom];
    MGSwipeButton * button = [[MGSwipeButton alloc] initWithFrame: CGRectMake(0.0, 0.0, BUTTON_WIDTH, BUTTON_WIDTH)];
    button.backgroundColor = color;
    
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setImage:icon forState:UIControlStateNormal];
    button.callback = callback;
    [button setPadding:padding];
    
    // by marsohod
    button.layer.cornerRadius = BUTTON_WIDTH / 2;
    button.clipsToBounds = YES;
    button.layer.masksToBounds = YES;
//    NSLog(@"%@", NSStringFromCGRect(button.frame));
    
//    [[button superview] addConstraint: [NSLayoutConstraint constraintWithItem: button
//                                                       attribute: NSLayoutAttributeHeight
//                                                       relatedBy: NSLayoutRelationEqual
//                                                          toItem: nil
//                                                       attribute: NSLayoutAttributeHeight
//                                                      multiplier: 1.0
//                                                        constant: BUTTON_WIDTH]];
    return button;
}

-(BOOL) callMGSwipeConvenienceCallback: (MGSwipeTableCell *) sender
{
    if (_callback) {
        return _callback(sender);
    }
    return NO;
}

-(void) setPadding:(CGFloat) padding
{
    self.contentEdgeInsets = UIEdgeInsetsMake(padding, padding, padding, padding);
//    [self sizeToFit];
}

@end
