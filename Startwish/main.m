
//
//  main.m
//  Startwish
//
//  Created by marsohod on 30/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
