//
//  AppDelegate.m
//  Startwish
//
//  Created by marsohod on 30/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "AppDelegate.h"
#import "LoginAPI.h"
#import "VKSdk.h"
#import "VKHandler.h"
#import "TwitterHandler.h"
#import "SDImageCache.h"
#import "SettingsAPI.h"
#import "UserAPI.h"
#import "PushNotificationHandler.h"
#import "Routing.h"
#import "GAI.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *defaultSettings = [[NSMutableDictionary alloc] init];

    [defaultSettings setObject:@"v6eam3Ap6GwU5yoP1t4XSnldE2NZfIMT" forKey:@"AID"];

    [userDefaults registerDefaults:defaultSettings];
    [userDefaults synchronize];
    
    
    [TwitterHandler start];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 2;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelNone];
    
    // Initialize tracker. Replace with your tracking ID.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-35436273-5"];
    
    [Fabric with:@[CrashlyticsKit]];
    
//    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    NSDictionary *localNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (localNotif) {
        [PushNotificationHandler sharedInstance].tappedPush = localNotif;
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[SDImageCache sharedImageCache] clearDisk];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    [FBSDKAppEvents activateApp];
    if ( [[UIApplication sharedApplication] isRegisteredForRemoteNotifications] ) {
        [PushNotificationHandler responseForRemoteNotifications];
    }
    
    if ( [UIApplication sharedApplication].applicationIconBadgeNumber != 0 ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    [[Routing sharedInstance] openLinkInApp:url];
//    [VKHandler isVKAuthUrl:url];
    
    // attempt to extract a token from the url
//    BOOL urlWasHandled = [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                   openURL:url
//                                         sourceApplication:sourceApplication
//                                                annotation:annotation];
//
//    return urlWasHandled;
//    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    return YES;
}




- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive) {
        if ( [userInfo[@"aps"] isKindOfClass:[NSDictionary class]] && userInfo[@"aps"][@"badge"] != (id)[NSNull null] ) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = [userInfo[@"aps"][@"badge"] integerValue];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil userInfo:@{@"count": userInfo[@"aps"][@"badge"]}];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedNewEvents" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": userInfo[@"aps"][@"alert"], @"push": userInfo}];
        }
        
        return;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STPushIsReceived" object:nil userInfo:@{@"push": userInfo}];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{

}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [PushNotificationHandler registerPushNotificationFromDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
}




@end
