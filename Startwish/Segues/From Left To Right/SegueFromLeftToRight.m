//
//  SegueFromLeftToRight.m
//  Startwish
//
//  Created by Pavel Makukha on 27/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SegueFromLeftToRight.h"


@implementation SegueFromLeftToRight

- (void)perform {
    
    __block UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    __block UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    
    destinationController.view.frame = CGRectMake(screenWidth, 0.0, screenWidth, screenHeight);
    
    [window insertSubview:destinationController.view aboveSubview:sourceViewController.view];
   
    
    
    [UIView animateWithDuration:0.3
                     animations:^{
//                         sourceViewController.view.frame = CGRectOffset(sourceViewController.view.frame, -screenWidth, 0.0);
                         destinationController.view.frame = CGRectOffset(destinationController.view.frame, -screenWidth, 0.0);
                     }
                     completion:^(BOOL finished) {
                         if ( sourceViewController.navigationController ) {
                             [sourceViewController.navigationController pushViewController:destinationController animated:NO];
                         } else {
                             SEL s = NSSelectorFromString(@"openKeyboard");
                             
                             [sourceViewController presentViewController:destinationController animated:NO completion:^{
                                 if ([destinationController respondsToSelector:s]) {
                                     #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                                        [destinationController performSelector:s withObject:nil];
                                     #pragma clang diagnostic pop
                                 }
                             }];
                         }
                     }];
    
    
    
}

@end
