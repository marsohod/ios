//
//  SeguePinterestOpen.m
//  Startwish
//
//  Created by Pavel Makukha on 27/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SeguePinterestOpen.h"
#import "PinterestViewController.h"
#import "UICollectionViewCellWish.h"
#import "UIFont+Helper.h"


@implementation SeguePinterestOpen

- (void)perform {
    
    __block PinterestViewController *sourceViewController = (PinterestViewController*)[self sourceViewController];
    __block UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window insertSubview:destinationController.view aboveSubview:sourceViewController.view];
    destinationController.view.alpha = 0.0;

    
    // Схитрил пипец. В origin.x так, как она всегда 0.0 засунул разницу в высоте
    // между желанием и всей высотой пина.
    CGRect tempWishFrame = [sourceViewController prepareWishToOpen];
    CGRect wishFrame = CGRectMake(0.0, tempWishFrame.origin.y, tempWishFrame.size.width, tempWishFrame.size.height);
    
    UICollectionViewCellWish *cell = (UICollectionViewCellWish *)[sourceViewController cellWishOpen];
    
    [cell.superview layoutIfNeeded];
    [cell setCoverHeight:wishFrame.size.height - tempWishFrame.origin.x];

    
    [UIView animateWithDuration:0.3
                     animations:^{
                         cell.frame = wishFrame;
                         destinationController.view.alpha = 1.0;
                         sourceViewController.view.alpha = 0.8;
                         [cell.superview layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {

                         if ( sourceViewController.navigationController ) {
                             [sourceViewController.navigationController pushViewController:destinationController animated:NO];
                         } else {
                             [sourceViewController presentViewController:destinationController animated:NO completion:nil];
                         }
                         
                         // Убираем плитку назад
                         sourceViewController.view.alpha = 1.0;
                         cell.frame = [sourceViewController prepareWishToClose];
                         [cell setImageViewCover];
                         [cell.superview layoutIfNeeded];
                     }];
}

@end
