//
//  SeguePinterestOpenUnwind.m
//  Startwish
//
//  Created by Pavel Makukha on 27/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SeguePinterestOpenUnwind.h"
#import "PinterestViewController.h"
#import "UICollectionViewCellWish.h"
#import "UIFont+Helper.h"


@implementation SeguePinterestOpenUnwind

- (void)perform {
    
    __block UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    __block PinterestViewController *destinationController = (PinterestViewController*)[self destinationViewController];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window insertSubview:destinationController.view belowSubview:sourceViewController.view];
    
    UICollectionViewCellWish *cell = (UICollectionViewCellWish *)[destinationController cellWishOpen];
    
    
    CGRect tempWishFrame = [destinationController prepareWishToOpen];
    CGRect wishOldFrame = CGRectMake(0.0, tempWishFrame.origin.y, tempWishFrame.size.width, tempWishFrame.size.height);
    CGRect wishFrame = [destinationController prepareWishToClose];
    
    // Устанавливаем плитку снова в увеличенный размер, чтобы потом красиво и радостно уменьшить
    [cell setCoverHeight:wishOldFrame.size.height - tempWishFrame.origin.x];
    cell.frame = wishOldFrame;
    
    [cell.superview layoutIfNeeded];
    [cell setImageViewCover];
    
    [UIView animateWithDuration: 0.3
                     animations:^{

                         cell.frame = wishFrame;
                         [cell.superview layoutIfNeeded];
                         [destinationController cellWishClose];
                     }
                     completion:^(BOOL finished) {
                         if ( sourceViewController.navigationController ) {
                             [sourceViewController.navigationController popViewControllerAnimated:NO];
                         } else {
                             [sourceViewController presentViewController:destinationController animated:NO completion:nil];
                         }
                     }];
    
    
    
}


@end
