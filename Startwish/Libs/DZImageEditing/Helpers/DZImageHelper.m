//
// Created by Dmitry Zozulya on 03.04.14.
// Copyright (c) 2014 MLSDev. All rights reserved.
//

#import "DZImageHelper.h"
#import "Settings.h"
#import "UIImage+Helper.h"


@implementation DZImageHelper

+ (UIImage *)cropImage:(UIImage *)image
    fromImageViewFrame:(CGRect)imageViewFrame
        fromScrollView:(UIScrollView *)scrollView
              withSize:(CGSize)size
{
    CGSize scaledSize = [self setScaledSizeScrollView:scrollView dependsOriginalImageSize:image.size];
    
    UIImage *scaledImage = [self imageWithImage:image
                                   scaledToSize:scaledSize
                                              x:(scrollView.contentOffset.x + scrollView.contentInset.left) / scrollView.zoomScale
                                              y:(scrollView.contentOffset.y + scrollView.contentInset.top- (scaledSize.height < image.size.height ? imageViewFrame.origin.y : 0)) / scrollView.zoomScale];
    
    if ( scaledImage.size.width > [Settings maxPhotoWidth] ) {
        scaledImage = [scaledImage resizedImage:scaledImage.CGImage size:CGSizeMake([Settings maxPhotoWidth], [Settings maxPhotoWidth] * scaledImage.size.height / scaledImage.size.width) transform:CGAffineTransformIdentity drawTransposed:NO interpolationQuality:kCGInterpolationMedium];
    }
    
    return scaledImage;
}

+ (CGFloat)minimumScaleFromSize:(CGSize)size toFitTargetSize:(CGSize)targetSize
{
    CGFloat widthScale = targetSize.width / size.width;
//    CGFloat heightScale = targetSize.height / size.height;
    return widthScale;
//    return size.width > size.height ? ((widthScale > heightScale) ? widthScale : heightScale) : widthScale;
}

#pragma mark - private

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize x:(CGFloat)x y:(CGFloat)y
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(-x, -y, image.size.width, image.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)cropImage:(UIImage *)image withRect:(CGRect)rect
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rect);
    UIImage *resultImage = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(imageRef);
    return resultImage;
}

+ (CGSize)setScaledSizeScrollView:(UIScrollView *)scrollView dependsOriginalImageSize:(CGSize)imageSize
{
    CGSize scaledSize = CGSizeMake( scrollView.frame.size.width / scrollView.zoomScale, scrollView.frame.size.height / scrollView.zoomScale);

    if ( scaledSize.height > imageSize.height ) {
        return CGSizeMake(scaledSize.width, imageSize.height);
    }
    
    return scaledSize;
}

@end