//
//  ContextMenuPeopleRating.m
//  Startwish
//
//  Created by marsohod on 04/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ContextMenuPeopleRating.h"
#import "UIFont+Helper.h"
#import "UILabel+typeOfText.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface PRMenuItemLocation : NSObject

@property (nonatomic) CGPoint position;
@property (nonatomic) CGFloat angle;

@end

@implementation PRMenuItemLocation

@end


@interface ContextMenuPeopleRating ()<UIGestureRecognizerDelegate>
{
    CADisplayLink *displayLink;
}

@property (nonatomic, strong) UILongPressGestureRecognizer* longPressRecognizer;

@property (nonatomic, assign) BOOL                  isShowing;
@property (nonatomic, assign) BOOL                  isPaning;

@property (nonatomic) CGPoint                       longPressLocation;
@property (nonatomic) CGPoint                       curretnLocation;

@property (nonatomic, strong) CALayer*              menuItem;
@property (nonatomic, strong) CALayer*              menuItemTitle;
@property (nonatomic, strong) CALayer*              menuItemRating;

@property (nonatomic) CGFloat                       radius;
@property (nonatomic) CGFloat                       radiusInnerMainItem;
@property (nonatomic) CGFloat                       radiusMainItem;
@property (nonatomic) CGFloat                       arcAngle;
@property (nonatomic) CGFloat                       angleBetweenItems;
@property (nonatomic, strong) PRMenuItemLocation*   itemLocation;
@property (nonatomic, strong) PRMenuItemLocation*   itemTitleLocation;
@property (nonatomic, strong) PRMenuItemLocation*   itemRatingLocation;
@property (nonatomic) NSInteger                     prevIndex;

@end

@implementation ContextMenuPeopleRating

#define GHShowAnimationID @"GHContextMenuViewRriseAnimationID"
#define GHDismissAnimationID @"GHContextMenuViewDismissAnimationID"


#define GHMainBorderWidth 2.0
#define GHMainBorderPadding 5.0
#define GHMainPaddingToItems 40.0
#define GHMainLineSize 202.0
#define GHMainItemSize 60
#define GHMenuItemSize 40.0
#define GHMenuItemPaddingLeftRight GHMenuItemSize / 2 + 13
#define GHBorderWidth 0

#define GHAnimationDuration 0.2
#define GHAnimationDelay GHAnimationDuration/5

- (id)init
{
    self = [super initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    if (self) {
        // Initialization code
        self.userInteractionEnabled = YES;
        
        self.backgroundColor  = [UIColor clearColor];
        
        _menuActionType = PRContextMenuActionTypePan;
        
        displayLink = [CADisplayLink displayLinkWithTarget:self
                                                  selector:@selector(highlightMenuItemForPoint)];
        
        [displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        
        _menuItem = [CALayer layer];
        _menuItemTitle = [CALayer layer];
        _menuItemRating = [CALayer layer];
        _itemLocation = [PRMenuItemLocation new];
        _itemTitleLocation = [PRMenuItemLocation new];
        _itemRatingLocation = [PRMenuItemLocation new];
        _arcAngle = M_PI_2;
        _radiusInnerMainItem = 50.0;
        _radiusMainItem = self.radiusInnerMainItem + GHMainBorderPadding;
        _radius = _radiusMainItem + GHMainPaddingToItems;
        
    }
    return self;
}

- (void) setMainDrawCircle:(float)r
{
    _radiusInnerMainItem = r;
    _radiusMainItem = self.radiusInnerMainItem + GHMainBorderPadding;
    _radius = _radiusMainItem + GHMainPaddingToItems;
}

#pragma mark -
#pragma mark Layer Touch Tracking
#pragma mark -
-(NSInteger)indexOfClosestMatchAtPoint:(CGPoint)point {

    if( CGRectContainsPoint( self.menuItem.frame, point ) ) {
        return 0;
    }

    return -1;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint menuAtPoint = CGPointZero;
    
    if ([touches count] == 1) {
        
        UITouch *touch = (UITouch *)[touches anyObject];
        CGPoint touchPoint = [touch locationInView:self];
        
        NSInteger menuItemIndex = [self indexOfClosestMatchAtPoint:touchPoint];
        if( menuItemIndex > -1 ) {
            menuAtPoint = [(CALayer *)self.menuItem position];
        }
        
        if( (self.prevIndex >= 0 && self.prevIndex != menuItemIndex)) {
            [self resetPreviousSelection];
        }
        self.prevIndex = menuItemIndex;
    }
    
    [self dismissWithSelectedIndexForMenuAtPoint: menuAtPoint];
}


#pragma mark -
#pragma mark LongPress handler
#pragma mark -

// Split this out of the longPressDetected so that we can reuse it with touchesBegan (above)
-(void)dismissWithSelectedIndexForMenuAtPoint:(CGPoint)point {
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(didSelectItemAtIndex: forMenuAtPoint:)] && self.prevIndex >= 0){
        [self.delegate didSelectItemAtIndex:self.prevIndex forMenuAtPoint:point];
        self.prevIndex = -1;
    }
    
    [self hideMenu];
}

- (void) longPressDetected:(UIGestureRecognizer*) gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        [self.delegate pauseAll:YES];
        self.prevIndex = -1;
        
        CGPoint pointInView = [gestureRecognizer locationInView:gestureRecognizer.view];
        if (self.dataSource != nil && [self.dataSource respondsToSelector:@selector(shouldShowMenuAtPoint:)] && ![self.dataSource shouldShowMenuAtPoint:pointInView]){
            return;
        }
        
        [[UIApplication sharedApplication].keyWindow addSubview:self];
        self.longPressLocation = [gestureRecognizer locationInView:self];
        
        // Удаляем всё нахер
        [self.dataSource GHMenuItemsAtPoint:[self convertPoint:self.longPressLocation toView:gestureRecognizer.view]];
        NSDictionary *loc = [self.dataSource GHLocationAtPoint:[self convertPoint:self.longPressLocation toView:gestureRecognizer.view]];
        
        // Если мы не нажали ни на один кругляш, а нажали на пустоту
        if ( loc == nil ) {
            [self reloadData];
            [self removeFromSuperview];
            return;
        }
    
        self.longPressLocation = CGPointMake([loc[@"x"] floatValue], [loc[@"y"] floatValue]);
        [self setMainDrawCircle:[loc[@"radius"] floatValue]];
        
        [self reloadData];
        
//        self.frame = [[UIScreen mainScreen] applicationFrame];
        self.layer.backgroundColor = [UIColor colorWithWhite:0.0f alpha:.6f].CGColor;
        
        self.isShowing = YES;
        [self animateMenu:YES];
        [self setNeedsDisplay];
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (self.isShowing && self.menuActionType == PRContextMenuActionTypePan) {
            self.isPaning = YES;
            self.curretnLocation =  [gestureRecognizer locationInView:self];
        }
    }
    
    // Only trigger if we're using the PRContextMenuActionTypePan (default)
    if( gestureRecognizer.state == UIGestureRecognizerStateEnded && self.menuActionType == PRContextMenuActionTypePan ) {
        CGPoint menuAtPoint = [self convertPoint:self.longPressLocation toView:gestureRecognizer.view];
        [self dismissWithSelectedIndexForMenuAtPoint:menuAtPoint];
    }
}

- (void) showMenu
{
    
}

- (void) hideMenu
{
    if (self.isShowing) {
        self.layer.backgroundColor = [UIColor clearColor].CGColor;
        self.isShowing = NO;
        self.isPaning = NO;
        [self animateMenu:NO];
        [self setNeedsDisplay];
        [self removeFromSuperview];
    }
    
    [self.delegate pauseAll:NO];
}

- (CALayer*) layerWithImage:(UIImage*) image  withColor:(UIColor *) color
{
    CALayer *layer = [CALayer layer];
    layer.bounds = CGRectMake(0, 0, GHMenuItemSize, GHMenuItemSize);
    layer.cornerRadius = GHMenuItemSize/2;
    layer.borderColor = [UIColor whiteColor].CGColor;
    layer.borderWidth = GHBorderWidth;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, -1);
    layer.backgroundColor = color.CGColor;
    
    CALayer* imageLayer = [CALayer layer];
    imageLayer.contents = (id) image.CGImage;
    imageLayer.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.position = CGPointMake(GHMenuItemSize/2, GHMenuItemSize/2);
    [layer addSublayer:imageLayer];
    
    return layer;
}

- (CALayer*) layerWithString:(NSString *)string
{
    CATextLayer *layer = [CATextLayer layer];
    CGSize textSize = [UILabel rectSizeString:string byWidth:150.0 maxHeight:50.0 fontName:[UIFont h_defaultFontNameBold] fontSize:13.0];
    
    layer.string = string;
    layer.font = (__bridge CFTypeRef)[UIFont h_defaultFontBoldSize:13.0].fontName;
    layer.fontSize = 13.0;
    layer.bounds = CGRectMake(0, 0, textSize.width, textSize.height < 31 ? 31 : textSize.height);
    layer.contentsScale = [[UIScreen mainScreen] scale];
    layer.alignmentMode = @"left";
//    [layer setBackgroundColor:[UIColor yellowColor].CGColor];
    layer.opacity = 0;
    
    return layer;
}

- (CALayer*) layerWithRating:(NSString *)string
{
    CALayer *layer = [CALayer layer];
    CATextLayer *layerText = [CATextLayer layer];

    UIImageView *ratingIconView = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"rating_profile_yellow.png"]];
    
    ratingIconView.layer.bounds = CGRectMake(0, 0, ratingIconView.image.size.width, ratingIconView.image.size.height);
    
    CGSize textSize = [UILabel rectSizeString:string byWidth:150.0 maxHeight:50.0 fontName:[UIFont h_defaultFontNameBold] fontSize:13.0];
    
    layerText.foregroundColor = [UIColor h_smoothDeepYellow].CGColor;
    layerText.string = string;
    layerText.font = (__bridge CFTypeRef)[UIFont h_defaultFontBoldSize:13.0].fontName;
    layerText.fontSize = 13.0;
    layerText.frame = CGRectMake(14, 18, textSize.width, textSize.height);
    layerText.contentsScale = [UIScreen mainScreen].scale;
    layerText.alignmentMode = @"right";
    layer.opacity = 0;
    layer.bounds = CGRectMake(0.0, 0.0, textSize.width + ratingIconView.frame.size.width + 10, GHMenuItemSize);

//    [layerText setBackgroundColor:[UIColor redColor].CGColor];
//    [layer setBackgroundColor:[UIColor greenColor].CGColor];
//    [ratingIconView.layer setBackgroundColor:[UIColor yellowColor].CGColor];
    
    [layer addSublayer:ratingIconView.layer];
    [layer insertSublayer:layerText below:ratingIconView.layer];
    
    layerText.bounds = layer.bounds;

    return layer;
}

- (void) setDataSource:(id<PRContextOverlayViewDataSource>)dataSource
{
    _dataSource = dataSource;
    [self reloadData];
    
}

# pragma mark - menu item layout

- (void) reloadData
{
    self.layer.sublayers = nil;
    
    self.menuItem = nil;
    self.menuItemTitle = nil;
    self.menuItemRating = nil;
    self.itemLocation = nil;
    self.itemTitleLocation = nil;
    self.itemRatingLocation = nil;
    
    if (self.dataSource != nil) {
        CALayer *layer = [self layerWithImage:[self.dataSource imageForItemAtIndex:0] withColor:[self.dataSource colorForItemAtIndex:0]];
        
        [self.layer addSublayer:layer];
        self.menuItem = layer;
        
        CALayer *layerTitle = [self layerWithString:[self.dataSource titleForItemAtIndex:0]];
        [self.layer addSublayer:layerTitle];
        self.menuItemTitle = layerTitle;
        
        CALayer *layerRating = [self layerWithRating:[self.dataSource ratingForItemAtIndex:0]];
        [self.layer addSublayer:layerRating];
        self.menuItemRating = layerRating;
        
    }
}

- (void) layoutMenuItems
{
    [self layoutMenuItems:NO];
}

- (void) layoutMenuItems:(BOOL)fixedAngle
{
//    self.itemLocation = nil;
//    self.itemTitleLocation = nil;
//    self.itemRatingLocation = nil;
    
    CGSize itemSize = CGSizeMake(GHMenuItemSize, GHMenuItemSize);
    CGFloat itemRadius = sqrt(pow(itemSize.width, 2) + pow(itemSize.height, 2)) / 2;
    self.arcAngle = (itemRadius / self.radius) * 1.5;
    

    BOOL isFullCircle = (self.arcAngle == M_PI*2);
    NSUInteger divisor = (isFullCircle) ? 1 : 0;
    
    divisor = divisor == 0 ? 1 : divisor;
    
    self.angleBetweenItems = self.arcAngle/divisor;
    self.itemLocation = [self locationForItemAtIndex:0 fixedAngle:fixedAngle];
    self.menuItem.transform = CATransform3DIdentity;
    
    CGFloat angle = [UIDevice currentDevice].orientation == UIDeviceOrientationLandscapeLeft ? M_PI_2 : -M_PI_2;
    
    // Rotate menu items based on orientation
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        self.menuItem.transform = CATransform3DRotate(CATransform3DIdentity, angle, 0, 0, 1);
    }
    
    
    self.itemTitleLocation = [self locationForItemTitleAtIndex:0 fixedAngle:fixedAngle];
    self.menuItemTitle.transform = CATransform3DIdentity;
    
    // Rotate menu items based on orientation
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        self.menuItemTitle.transform = CATransform3DRotate(CATransform3DIdentity, angle, 0, 0, 1);
    }
    
    
    self.itemRatingLocation = [self locationForItemRatingAtIndex:0 fixedAngle:fixedAngle];

    if( (self.longPressLocation.x - GHMenuItemSize / 2 < 0 || self.longPressLocation.x + GHMenuItemSize / 2 > [UIScreen mainScreen].bounds.size.width ) && fixedAngle == NO ) {
        [self layoutMenuItems:YES];
    }
    
    self.menuItemRating.transform = CATransform3DIdentity;
    
    // Rotate menu items based on orientation
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation)) {
        self.menuItemRating.transform = CATransform3DRotate(CATransform3DIdentity, angle, 0, 0, 1);
    }
    
    
}

- (PRMenuItemLocation*) locationForItemAtIndex:(NSUInteger) index fixedAngle:(BOOL)needFix
{
    CGFloat itemAngle = [self itemAngleAtIndex:index needFix:needFix];
    
    CGPoint itemCenter = CGPointMake(self.longPressLocation.x + cosf(itemAngle) * self.radius,
                                     self.longPressLocation.y + sinf(itemAngle) * self.radius);
    PRMenuItemLocation *location = [PRMenuItemLocation new];
    location.position = itemCenter;
    location.angle = itemAngle;
    
    return location;
}

- (PRMenuItemLocation*) locationForItemTitleAtIndex:(NSUInteger) index fixedAngle:(BOOL)needFix
{
    CGFloat itemAngle = [self itemAngleAtIndex:index needFix:needFix];
    
    CGFloat x = self.longPressLocation.x + cosf(itemAngle) * self.radius;
    CGFloat y = self.longPressLocation.y + sinf(itemAngle) * self.radius;
    CGFloat width = self.menuItemTitle.frame.size.width / 2;
    CGFloat height = 0.0;//(GHMenuItemSize - layer.frame.size.height) / 2;

    //    NSLog(@"%f %f", GHMenuItemSize, layer.frame.size.height);
    
    // Делаем отступ тайтла в зависимости от стороны, на которой произошёл тач
    if ( self.longPressLocation.x >= [UIScreen mainScreen].bounds.size.width / 2) {
        width = width - self.menuItemTitle.frame.size.width - (GHMenuItemPaddingLeftRight);
    } else {
        width = width + GHMenuItemPaddingLeftRight;
    }
    
    // Если шары отрисовываются сверху от нажатия
    if ( itemAngle <= 6.28 && itemAngle > 3.14 ) {
        height = -height;
    } else {
        //        height = -height;
    }
    
    
    CGPoint itemCenter = CGPointMake(x + width, y + height);
    PRMenuItemLocation *location = [PRMenuItemLocation new];
    location.position = itemCenter;
    location.angle = itemAngle;
    
    return location;
}

- (PRMenuItemLocation*) locationForItemRatingAtIndex:(NSUInteger) index fixedAngle:(BOOL)needFix
{
    CGFloat itemAngle = [self itemAngleAtIndex:index needFix:needFix];
    
    CGFloat x = self.longPressLocation.x + cosf(itemAngle) * self.radius;
    CGFloat y = self.longPressLocation.y + sinf(itemAngle) * self.radius;
    CGFloat width = self.menuItemRating.frame.size.width / 2;
    CGFloat height = 0.0;//(GHMenuItemSize - layer.frame.size.height) / 2;
    
    // Делаем отступ тайтла в зависимости от стороны, на которой произошёл тач
    if ( self.longPressLocation.x >= [UIScreen mainScreen].bounds.size.width / 2) {
        width = width + GHMenuItemPaddingLeftRight;
    } else {
        width = -width - (GHMenuItemPaddingLeftRight);
    }
    
    // Если шары отрисовываются сверху от нажатия
    if ( itemAngle <= 6.28 && itemAngle > 3.14 ) {
        height = -height;
    }
    
    
    CGPoint itemCenter = CGPointMake(x + width, y + height);
    
    // Так как рейтинг мы ставим последним, смотрим, чтобы он не вылазил за экран.
    // Если вылазиет, то всё меняем
    // Центр надписи с рейтингом + половина его длины - есть точка х
    // GHMenuItemPaddingLeftRight - отступ от кругляша до надписей
    if (itemCenter.x - self.menuItemRating.frame.size.width / 2 < 0 ) {
        itemCenter = CGPointMake(x + GHMenuItemPaddingLeftRight + self.menuItemRating.frame.size.width / 2, y + height);
        
        self.itemTitleLocation.position = CGPointMake(itemCenter.x + self.menuItemRating.frame.size.width / 2 + self.menuItemTitle.frame.size.width / 2 + 13 , self.itemTitleLocation.position.y);
        
    } else if ( itemCenter.x + self.menuItemRating.frame.size.width / 2 > [UIScreen mainScreen].bounds.size.width ) {
        itemCenter = CGPointMake(x + (-self.menuItemRating.frame.size.width / 2 - (GHMenuItemPaddingLeftRight)), y + height);
        
        self.itemTitleLocation.position = CGPointMake(itemCenter.x - self.menuItemRating.frame.size.width / 2 - self.menuItemTitle.frame.size.width / 2 - 13, self.itemTitleLocation.position.y);
    }
    
    PRMenuItemLocation *location = [PRMenuItemLocation new];
    location.position = itemCenter;
    location.angle = itemAngle;

    return location;
}

- (CGFloat) itemAngleAtIndex:(NSUInteger) index needFix:(BOOL)needFix
{
    float bearingRadians = [self angleBeweenStartinPoint:self.longPressLocation endingPoint:self.center];
//    NSLog(@"longPressLocation: %f %f %f %f", self.angleBetweenItems, self.longPressLocation.y, self.center.x, self.center.y);
    CGFloat angle =  bearingRadians - self.arcAngle/2;
    
    CGFloat itemAngle = angle + (index * self.angleBetweenItems);
    
    if (itemAngle > 2 *M_PI) {
        itemAngle -= 2*M_PI;
    }else if (itemAngle < 0){
        itemAngle += 2*M_PI;
    }
    
    // Если мы в состоянии показать item сверху
    if ( self.longPressLocation.y - (self.radius + GHMenuItemSize) > 0) {
        itemAngle = M_PI + M_PI_2;
    } else {
        itemAngle = M_PI_2;
    }
    
    // Если кругляшь не влезает и нам надо подправить угол
    if ( needFix ) {
        
        CGFloat BC = sqrtf(powf(self.radius, 2) + powf(GHMenuItemSize, 2));
        
        CGFloat alpha = acosf(self.radius / BC);
        
        if (self.longPressLocation.x < [UIScreen mainScreen].bounds.size.width / 2 ) {
            itemAngle = itemAngle + (itemAngle == M_PI / 2 ? -alpha : alpha);
        } else {
            itemAngle = itemAngle + (itemAngle == M_PI / 2 ? alpha : -alpha);
        }
    }
    
    return itemAngle;
}

# pragma mark - helper methiods

- (CGFloat) calculateRadius
{
    CGSize mainSize = CGSizeMake(_radiusMainItem, _radiusMainItem);
    CGSize itemSize = CGSizeMake(GHMenuItemSize, GHMenuItemSize);
    CGFloat mainRadius = sqrt(pow(mainSize.width, 2) + pow(mainSize.height, 2)) / 2;
    CGFloat itemRadius = sqrt(pow(itemSize.width, 2) + pow(itemSize.height, 2)) / 2;
    
    CGFloat minRadius = (CGFloat)(mainRadius + itemRadius);
    CGFloat maxRadius = (itemRadius / self.arcAngle) * 1.5;
    
    CGFloat radius = MAX(minRadius, maxRadius);
    
    return radius;
}

- (CGFloat) angleBeweenStartinPoint:(CGPoint) startingPoint endingPoint:(CGPoint) endingPoint
{
    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y);
    float bearingRadians = atan2f(originPoint.y, originPoint.x);
    
    bearingRadians = (bearingRadians > 0.0 ? bearingRadians : (M_PI*2 + bearingRadians));
    
    return bearingRadians;
}

- (CGFloat) validaAngle:(CGFloat) angle
{
    if (angle > 2*M_PI) {
        angle = [self validaAngle:angle - 2*M_PI];
    }
    
    return angle;
}

# pragma mark - animation and selection

- (void) resetPreviousSelection
{
    return;
}

- (void) animateMenu:(BOOL) isShowing
{
    if (isShowing) {
        [self layoutMenuItems];
    }
    
    self.menuItem.opacity = 0;
    
    CGPoint toPosition = self.itemLocation.position;
    
    double delayInSeconds = 0;
    
    CABasicAnimation *positionAnimation;
    
    positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    positionAnimation.fromValue = [NSValue valueWithCGPoint:isShowing ? self.longPressLocation : toPosition];
    positionAnimation.toValue = [NSValue valueWithCGPoint:isShowing ? toPosition : self.longPressLocation];
    positionAnimation.timingFunction = [CAMediaTimingFunction functionWithControlPoints:0.45f :1.2f :0.75f :1.0f];
    positionAnimation.duration = GHAnimationDuration;
    positionAnimation.beginTime = [self.menuItem convertTime:CACurrentMediaTime() fromLayer:nil] + delayInSeconds;
    [positionAnimation setValue:[NSNumber numberWithUnsignedInteger:0] forKey:isShowing ? GHShowAnimationID : GHDismissAnimationID];
    positionAnimation.delegate = self;
    
    [self.menuItem addAnimation:positionAnimation forKey:@"riseAnimation"];
}

- (void)animationDidStart:(CAAnimation *)anim
{
    if([anim valueForKey:GHShowAnimationID]) {
        CGFloat toAlpha = 1.0;

        self.menuItem.position = self.itemLocation.position;
        self.menuItem.opacity = toAlpha;
        self.menuItemTitle.opacity = toAlpha;
        self.menuItemTitle.position = self.itemTitleLocation.position;
        self.menuItemRating.opacity = toAlpha;
        self.menuItemRating.position = self.itemRatingLocation.position;
    }
    else if([anim valueForKey:GHDismissAnimationID]) {

        CGPoint toPosition = self.longPressLocation;
        [CATransaction begin];
        [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
        self.menuItem.position = toPosition;
        self.menuItem.backgroundColor = [self.dataSource colorForItemAtIndex:0].CGColor;
        self.menuItem.opacity = 0.0f;
        self.menuItem.transform = CATransform3DIdentity;
        [CATransaction commit];
    }
}



-  (void) highlightMenuItemForPoint
{
    if (self.isShowing && self.isPaning) {
        
        CGFloat angle = [self angleBeweenStartinPoint:self.longPressLocation endingPoint:self.curretnLocation];
        NSInteger closeToIndex = -1;

        if (fabs(self.itemLocation.angle - angle) < self.angleBetweenItems/2) {
            closeToIndex = 0;
        }

        
        if (closeToIndex == 0 ) {
            
            CGFloat distanceFromCenter = sqrt(pow(self.curretnLocation.x - self.longPressLocation.x, 2)+ pow(self.curretnLocation.y-self.longPressLocation.y, 2));
            
            CGFloat toleranceDistance = (self.radius - GHMainItemSize/(2*sqrt(2)) - GHMenuItemSize/(2*sqrt(2)) )/2;
            
            CGFloat distanceFromItem = fabs(distanceFromCenter - self.radius) - GHMenuItemSize/(2*sqrt(2)) ;
            
            if (fabs(distanceFromItem) < toleranceDistance ) {
                CALayer *layer = self.menuItem;
//                CATextLayer *layerTitle = self.menuItemTitle;
                //                layer.backgroundColor = self.itemBGHighlightedColor;
                
                CGFloat distanceFromItemBorder = fabs(distanceFromItem);
                
                CGFloat scaleFactor = 1 + 0.5 *(1-distanceFromItemBorder/toleranceDistance) ;
                
                if (scaleFactor < 1.0) {
                    scaleFactor = 1.0;
                }
                
                // Scale
                CATransform3D scaleTransForm =  CATransform3DScale(CATransform3DIdentity, scaleFactor, scaleFactor, 1.0);
                
                CATransform3D transLate = CATransform3DTranslate(scaleTransForm, 0, 0, 0);
                layer.transform = transLate;
//                layerTitle.opacity = 1;
                
                if ( ( self.prevIndex >= 0 && self.prevIndex != closeToIndex)) {
                    [self resetPreviousSelection];
                }
                
                self.prevIndex = closeToIndex;
                
            } else if(self.prevIndex >= 0) {
                [self resetPreviousSelection];
            }
        }else {
            [self resetPreviousSelection];
        }
    }
}



- (void)drawCircle:(CGPoint)locationOfTouch
{
    
    UIImage *circle = [UIImage imageNamed:@"bg_avatar_green_100"];
    
    CGContextRef ctx=  UIGraphicsGetCurrentContext();
    CGContextSaveGState(ctx);
    CGContextSetLineWidth(ctx, GHMainBorderWidth);
    
    CGContextSetRGBStrokeColor(ctx, 0.0, 173.0/255.0, 179.0/255.0, 1.0);
    CGContextAddArc(ctx,locationOfTouch.x,locationOfTouch.y,self.radiusMainItem,0.0,M_PI*2,YES);
    
    [circle drawInRect:CGRectMake(locationOfTouch.x - self.radiusInnerMainItem, locationOfTouch.y - self.radiusInnerMainItem, self.radiusInnerMainItem * 2, self.radiusInnerMainItem * 2)];
    
    CGContextStrokePath(ctx);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    if (self.isShowing) {
        [self drawCircle:self.longPressLocation];
    }
}

@end
