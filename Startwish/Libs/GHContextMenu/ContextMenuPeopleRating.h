//
//  ContextMenuPeopleRating.h
//  Startwish
//
//  Created by marsohod on 04/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PRContextMenuActionType){
    // Default
    PRContextMenuActionTypePan,
    // Allows tap action in order to trigger an action
    PRContextMenuActionTypeTap
};

@protocol PRContextOverlayViewDataSource;
@protocol PRContextOverlayViewDelegate;

//@interface GHContextMenuView : UIView
@interface ContextMenuPeopleRating : UIView

@property (nonatomic, assign) id<PRContextOverlayViewDataSource> dataSource;
@property (nonatomic, assign) id<PRContextOverlayViewDelegate> delegate;

@property (nonatomic, assign) PRContextMenuActionType menuActionType;

- (void) longPressDetected:(UIGestureRecognizer*) gestureRecognizer;

@end

@protocol PRContextOverlayViewDataSource <NSObject>

@required
- (NSInteger) numberOfMenuItems;
- (UIImage*) imageForItemAtIndex:(NSInteger) index;
- (NSString*) titleForItemAtIndex:(NSInteger) index;
- (NSString*) ratingForItemAtIndex:(NSInteger) index;

- (UIColor*) colorForItemAtIndex:(NSInteger) index;
- (void)GHMenuItemsAtPoint:(CGPoint)touchPoint;
- (NSDictionary *)GHLocationAtPoint:(CGPoint)touchPoint;

@optional
-(BOOL) shouldShowMenuAtPoint:(CGPoint) point;

@end

@protocol PRContextOverlayViewDelegate <NSObject>

- (void) didSelectItemAtIndex:(NSInteger) selectedIndex forMenuAtPoint:(CGPoint) point;
- (void) pauseAll:(BOOL)state;

@end
