//
//  ImageFilterControl.m
//  Startwish
//
//  Created by Pavel Makukha on 16/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ImageFilterControl.h"
#import "GPUImage.h"
#import "GPUImageFilterGroup.h"
#import "UIImage+Helper.h"


@interface ImageFilterControl ()

@property (nonatomic) CGSize originalImageSize;
@property (nonatomic) UIImageOrientation originalImageOrientation;
@property (nonatomic, strong) GPUImagePicture *originalImageGPU;
@property (nonatomic, strong) GPUImagePicture *angleImageGPU;

//@property (nonatomic, strong) GPUImageBrightnessFilter  *filterBrightness;
@property (nonatomic, strong) GPUImageExposureFilter    *filterExposure;
@property (nonatomic, strong) GPUImageContrastFilter    *filterContrast;
@property (nonatomic, strong) GPUImageTransformFilter   *filterAngle;

@property (nonatomic) CGFloat lastExposure;
@property (nonatomic) CGFloat lastContrast;
@property (nonatomic) CGFloat lastAngle;

@end


@implementation ImageFilterControl


#pragma mark - init

- (instancetype)initWithImage:(UIImage *)image
{
    self = [super init];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIColorControls"];
    
    self.originalImageSize = image.size;
    self.originalImageOrientation = image.imageOrientation;
    self.originalImageGPU = [[GPUImagePicture alloc] initWithImage:image];
//    self.angleImageGPU = self.originalImageGPU;
    
    [filter setDefaults];
    [filter setValue: [CIImage imageWithCGImage:image.CGImage] forKey: @"inputImage"];
    
    _exposure = 0.f;
    _contrast = [[filter valueForKey:@"inputContrast"] floatValue];
    _angle = 0.f;
    
    [self saveProperties];
    
    return self;
}


#pragma mark - Getter
- (GPUImageExposureFilter *)filterExposure
{
    if ( !_filterExposure) {
        _filterExposure = [[GPUImageExposureFilter alloc] init];
        [_filterExposure addTarget:self.filterContrast];
    }
    
    return _filterExposure;
}

- (GPUImageContrastFilter *)filterContrast
{
    if ( !_filterContrast) {
        _filterContrast = [[GPUImageContrastFilter alloc] init];
    }
    
    return _filterContrast;
}

- (GPUImageTransformFilter *)filterAngle
{
    if ( !_filterAngle) {
        _filterAngle = [[GPUImageTransformFilter alloc] init];
    }
    
    return _filterAngle;
}


#pragma mark - Setter

- (void)setImageAngle:(UIImage *)imageWithoutAngle withUpdateImageView:(BOOL)isNeedUpdateImageView
{
//    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage:imageWithoutAngle];
    CGAffineTransform transform = CGAffineTransformMakeRotation(_angle);
    transform = CGAffineTransformScale(transform, [self zoomScaleToBound], [self zoomScaleToBound]);

    self.filterAngle.affineTransform = transform;
    
    if ( isNeedUpdateImageView ) {
        [self.filterAngle addTarget:self.gpuImageView];
    } else {
        [self.filterAngle removeTarget:self.gpuImageView];
    }
    
    [self.originalImageGPU addTarget:self.filterAngle];
    [self.filterAngle useNextFrameForImageCapture];
    
    [self.originalImageGPU processImage];
    [self.originalImageGPU removeAllTargets];
    
    self.angleImageGPU = [[GPUImagePicture alloc] initWithImage:[self.filterAngle imageFromCurrentFramebufferWithOrientation:self.originalImageOrientation]];
//    self.angleImageGPU = [self.filterAngle imageFromCurrentFramebufferWithOrientation:self.originalImageOrientation];

    [self.filterAngle removeOutputFramebuffer];
    [self.filterAngle removeAllTargets];
    
    [[GPUImageContext sharedFramebufferCache] purgeAllUnassignedFramebuffers];
}

- (CGFloat)zoomScaleToBound
{
    CGFloat width = cos(fabs(self.angle)) * self.rv.frame.size.width + sin(fabs(self.angle)) * self.rv.frame.size.height;
    CGFloat height = sin(fabs(self.angle)) * self.rv.frame.size.width + cos(fabs(self.angle)) * self.rv.frame.size.height;
    CGFloat scaleW = width / self.rv.bounds.size.width;
    CGFloat scaleH = height / self.rv.bounds.size.height;
    CGFloat max = MAX(scaleW, scaleH);
    
    return isnan(max) ? 1 : max;
}

- (UIImage *)i_setExposure:(NSNumber *)exposure
{
    _exposure = [exposure floatValue];
    
    return [self renderImage];
}

- (UIImage *)i_setContrast:(NSNumber *)contrast
{
    _contrast = [contrast floatValue];
    
    return [self renderImage];
}

- (UIImage *)i_setAngle:(NSNumber *)angle
{
    return [self i_setAngle:angle withUpdateImageView:YES];
}

- (UIImage *)i_setAngle:(NSNumber *)angle withUpdateImageView:(BOOL)isNeedUpdateImageView
{
    _angle = [angle floatValue];
    
    [self setImageAngle:nil withUpdateImageView:isNeedUpdateImageView];
    
    return nil;
//    return self.angleImage;
}

- (UIImage *)setFilter:(PhotoEditToolFilter *)tool
{
    return [self setFilter:tool withPercent:100.f];
}

- (UIImage *)setFilter:(PhotoEditToolFilter *)tool withPercent:(CGFloat)percent
{
    self.filterTool = tool;
    self.filterTool.value = percent;
    
    return [self applyFilterTool];
}

// deprecated
- (NSArray *)applyFilterToolWithParams:(NSDictionary *)params
{
    SEL applyFilter = NSSelectorFromString( @"applyFilterToImage:withParams:" );
    
    if ( ![self.filterTool respondsToSelector:applyFilter] ) {
        NSLog(@"Unsupported filter apply method apply%@ToImage:withParams:", self.filterTool.type);
        return nil;
    }
    
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self.filterTool performSelector:applyFilter withObject:self.angleImageGPU withObject:params];
    #pragma clang diagnostic pop
}

- (NSArray *)GPUFilters
{
    SEL applyFilter = NSSelectorFromString( @"GPUFilters" );
    
    if ( ![self.filterTool respondsToSelector:applyFilter] ) {
        NSLog(@"Unsupported filter apply method apply%@ToImage:withParams:", self.filterTool.type);
        return nil;
    }
    
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    return [self.filterTool performSelector:applyFilter withObject:self.angleImageGPU];
#pragma clang diagnostic pop
}

- (UIImage *)applyFilterTool
{
    return [self renderImage];
}

- (UIImage *)getRenderedImage
{
    return [self renderImageAndReturnIt:YES];
}

- (UIImage *)renderImageAndReturnIt:(BOOL)isNeedReturn
{
    GPUImageFilterGroup *group = [[GPUImageFilterGroup alloc] init];
    NSArray *filters;
    CGFloat processingPercent = isNeedReturn ? 1.f : 0.9f;
    UIImage *imageOut;
 
    // 0. Clear angleImageGPU from targets
    [self.angleImageGPU removeAllTargets];
    
    // 1. Get filters if it selected
    if ( self.filterTool ) {
        filters = [self GPUFilters];
    }
    
    // 2. Set image processing size and set filters chain
    if ( filters ) {
        [[filters firstObject] forceProcessingAtSize:CGSizeMake(self.originalImageSize.width * processingPercent, self.originalImageSize.height * processingPercent)];
        
        for ( NSInteger i = 0, j = [filters count]; i < j - 1; i++ ) {
            [[filters objectAtIndex:i] addTarget:[filters objectAtIndex:i+1]];
        }
        
        [[filters lastObject] addTarget:self.filterExposure];
        
    } else {
        [self.filterExposure forceProcessingAtSize:CGSizeMake(self.originalImageSize.width * processingPercent, self.originalImageSize.height * processingPercent)];
    }
    
    [self.angleImageGPU addTarget:group];
    
    // 3. Set params
    if ( self.filterContrast.contrast != _contrast ) {
        self.filterContrast.contrast = _contrast;
    }
    
    if ( self.filterExposure.exposure != _exposure ) {
        self.filterExposure.exposure = _exposure;
    }
    
    // 4. Set group params
    [group setInitialFilters:@[filters != nil ? [filters firstObject] : self.filterExposure]];
    group.terminalFilter = self.filterContrast;
    
    // 5. Show processed image on view
    [self.filterContrast removeTarget:_gpuImageView];
    [self.filterContrast addTarget:_gpuImageView];
    
    // 6. Processing image
    if ( isNeedReturn ) {
        [group useNextFrameForImageCapture];
    }
    
    [self.angleImageGPU processImage];
    [self.angleImageGPU removeAllTargets];
    
    imageOut = isNeedReturn ? [group imageFromCurrentFramebufferWithOrientation:self.originalImageOrientation] : nil;
    
    // 7. Clear filters from targets
    [self.filterContrast removeOutputFramebuffer];
    [self.filterExposure removeOutputFramebuffer];
    
    // 7. Clear cache
//    [[GPUImageContext sharedFramebufferCache] purgeAllUnassignedFramebuffers];
    filters   = nil;
    
    return imageOut;
}

- (UIImage *)renderImage
{
    return [self renderImageAndReturnIt:NO];
}


#pragma mark - Save
- (void)saveProperties
{
    self.lastAngle = _angle;
    self.lastExposure = _exposure;
    self.lastContrast = _contrast;
}


#pragma mark -

- (UIImage *)loadActualProperties
{
    _angle = self.lastAngle;
    _contrast = self.lastContrast;
    _exposure = self.lastExposure;
    
    [self i_setAngle:[NSNumber numberWithFloat:_angle] withUpdateImageView:NO];
    
    return [self renderImage];
}

- (CGFloat)actualPropertyValue:(NSString *)type
{
    SEL property = NSSelectorFromString([type lowercaseString]);
    
    if ([self respondsToSelector:property]) {
        return [[self valueForKey:[type lowercaseString]] floatValue];
    }
    
    return 0.0;
}

- (void)saveFilterParams:(PhotoEditToolFilter *)tool
{
    _exposure = tool.exposure ? tool.exposure : _exposure;
    _contrast = tool.contrast ? tool.contrast : _contrast;
    
    [self saveProperties];
}

- (void)dealloc
{
    [[GPUImageContext sharedFramebufferCache] purgeAllUnassignedFramebuffers];
}

@end
