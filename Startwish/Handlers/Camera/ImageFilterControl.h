//
//  ImageFilterControl.h
//  Startwish
//
//  Created by Pavel Makukha on 16/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PhotoEditToolFilter.h"


@interface ImageFilterControl : NSObject

@property (nonatomic, strong) GPUImageView *gpuImageView;
@property (nonatomic, strong) PhotoEditToolFilter *filterTool;

// crop rotated view
@property (nonatomic) UIView *rv;

@property (nonatomic) CGFloat exposure;
@property (nonatomic) CGFloat contrast;
@property (nonatomic) CGFloat angle;

#pragma mark - init
- (instancetype)initWithImage:(UIImage *)image;

#pragma mark - Setter
- (UIImage *)i_setExposure:(NSNumber *)exposure;
- (UIImage *)i_setContrast:(NSNumber *)contrast;
- (UIImage *)i_setAngle:(NSNumber *)contrast;
- (UIImage *)setFilter:(PhotoEditToolFilter *)filter;
- (UIImage *)setFilter:(PhotoEditToolFilter *)filter withPercent:(CGFloat)percent;

#pragma mark - 
- (CGFloat)actualPropertyValue:(NSString *)type;
- (UIImage *)loadActualProperties;

#pragma mark -
- (UIImage *)getRenderedImage;

#pragma mark - Save
- (void)saveProperties;
- (void)saveFilterParams:(PhotoEditToolFilter *)tool;
@end
