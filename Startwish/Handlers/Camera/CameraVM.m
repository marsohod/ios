//
//  CameraVM.m
//  Startwish
//
//  Created by Pavel Makukha on 19/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CameraVM.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PBJVisionUtilities.h"
#import "UIImage+Helper.h"
#import "Settings.h"


@interface CameraVM() <PBJVisionDelegate>
@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) UIImage *photoImage;
@property (nonatomic, strong) NSData *photoData;
@property (nonatomic, assign) Float64 capturedVideoSeconds;
@property (nonatomic) CGRect previewFrame;
@end

@implementation CameraVM

- (BOOL)isPhotoModeCamera
{
    return self.vision.cameraMode == PBJCameraModePhoto;
}

- (BOOL)isFrontCamera
{
    return self.vision.cameraDevice == PBJCameraDeviceFront;
}

- (BOOL)isFlashOn
{
    return self.vision.flashMode == AVCaptureFlashModeOn;
}

- (BOOL)isFlashOff
{
    return self.vision.flashMode == AVCaptureFlashModeOff;
}

- (BOOL)isFlashAuto
{
    return self.vision.flashMode == AVCaptureFlashModeAuto;
}

- (void)takePhoto
{
    [self.vision capturePhoto];
}

- (void)disableCamera
{
    [self.vision stopPreview];
}


#pragma mark - Signals

- (RACSignal *)photoShootResultSignal
{
    return RACObserve(self, photoImage);
}

//- (RACSignal *)videoShootURLResultSignal
//{
//    return RACObserve(self, capturedVideoAssetURL);
//}

- (RACSignal *)capturingVideoDurationSignal
{
    return RACObserve(self, capturedVideoSeconds);
}


#pragma mark - Camera
- (void)setupPreviewFrame:(CGRect)frame
{
    self.previewFrame = frame;
}

- (void)setupPhotoCamera
{
    self.vision = [PBJVision sharedInstance];
    self.vision.delegate = self;
    self.vision.cameraMode = PBJCameraModePhoto;
    self.vision.cameraOrientation = PBJCameraOrientationPortrait;
    self.vision.focusMode = PBJFocusModeContinuousAutoFocus;
    self.vision.outputFormat = PBJOutputFormatPreset;
    self.vision.maximumCaptureDuration = CMTimeMakeWithSeconds(200, 600);
    self.vision.autoFreezePreviewDuringCapture = YES;
    self.vision.flashMode = AVCaptureFlashModeOff;
    self.vision.previewOrientation = PBJCameraOrientationPortrait;
    
    if ( !CGRectIsEmpty(self.previewFrame) ) {
        self.vision.previewLayer.frame = self.previewFrame;
    }
    
    _assetsLibrary = [[ALAssetsLibrary alloc] init];
    
    [self.vision startPreview];
}

- (void)setupVideoCamera
{
    self.vision.cameraMode = PBJCameraModeVideo;
    self.vision.cameraOrientation = PBJCameraOrientationPortrait;
    self.vision.focusMode = PBJFocusModeContinuousAutoFocus;
    self.vision.outputFormat = PBJOutputFormatPreset;
    self.vision.videoRenderingEnabled = YES;
    self.vision.maximumCaptureDuration = CMTimeMakeWithSeconds(20, 600);
    self.vision.autoFreezePreviewDuringCapture = YES;
    self.vision.additionalCompressionProperties = @{AVVideoProfileLevelKey : AVVideoProfileLevelH264Baseline30};

    _assetsLibrary = [[ALAssetsLibrary alloc] init];
    
    [self.vision startPreview];
}

- (void)changeOrientationCamera:(UIDeviceOrientation)orientation
{
    switch(orientation)
    {
        case UIDeviceOrientationPortrait:
            self.vision.previewOrientation = PBJCameraOrientationPortrait;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            self.vision.previewOrientation = PBJCameraOrientationPortraitUpsideDown;
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            self.vision.previewOrientation = PBJCameraOrientationLandscapeLeft;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            self.vision.previewOrientation = PBJCameraOrientationLandscapeRight;
            break;
            
        default:
            break;
    };
}

- (void)changeCameraDevice
{
    if (!self.isFrontCamera)
    {
        self.vision.cameraDevice = PBJCameraDeviceFront;
    }
    else
    {
        self.vision.cameraDevice = PBJCameraDeviceBack;
    }
}

- (void)enableFlash
{
    self.vision.flashMode = [self isFlashOn] ? AVCaptureFlashModeAuto : ([self isFlashAuto] ? AVCaptureFlashModeOff : AVCaptureFlashModeOn);
}

- (void)focusToPoint:(CGPoint)point inFrame:(CGRect)frame
{
    CGPoint adjustPoint = [PBJVisionUtilities convertToPointOfInterestFromViewCoordinates:point inFrame:frame];
    [self.vision focusExposeAndAdjustWhiteBalanceAtAdjustedPoint:adjustPoint];
}

// video

- (void)startVideoCapturing
{
    [self.vision startVideoCapture];
}

- (void)pauseVideoCapturing
{
    [self.vision pauseVideoCapture];
}

- (void)resumeVideoCapturing
{
    [self.vision resumeVideoCapture];
}

- (void)endVideoCapturing
{
    [self.vision endVideoCapture];
}

- (CGRect)convertFocusPoint:(CGPoint)point toFrame:(CGRect)frame
{
#if defined(__LP64__) && __LP64__
    frame.origin.x = rint(point.x - (frame.size.width * 0.5));
    frame.origin.y = rint(point.y - (frame.size.height * 0.5));
#else
    frame.origin.x = rintf(point.x - (frame.size.width * 0.5f));
    frame.origin.y = rintf(point.y - (frame.size.height * 0.5f));
#endif
    
    return frame;
}


#pragma mark - PBJVisionDelegate

#pragma mark Photo Capturing

- (void)vision:(PBJVision *)vision capturedPhoto:(nullable NSDictionary *)photoDict error:(nullable NSError *)error
{
    if (!error)
    {
        UIImage *img = photoDict[PBJVisionPhotoImageKey];
        CGImageRef cgRef;
        
        if ( img.size.width > [Settings maxPhotoWidth] ) {
            img = [img resizedImage:img.CGImage size:CGSizeMake([Settings maxPhotoWidth] * img.size.height / img.size.width, [Settings maxPhotoWidth]) transform:CGAffineTransformIdentity drawTransposed:NO interpolationQuality:kCGInterpolationMedium];
        }
        
        cgRef = img.CGImage;
        
        self.vision.previewOrientation = self.vision.previewOrientation == 0 ? PBJCameraOrientationPortrait : self.vision.previewOrientation;
        switch(self.vision.previewOrientation)
        {
            case PBJCameraOrientationPortrait:
                img = [self cropImageToPreviewFrame:img];
                break;
                
            case PBJCameraOrientationPortraitUpsideDown:
                img = [self cropImageToPreviewFrame:img];
                break;
                
            case PBJCameraOrientationLandscapeLeft:
                img = [[UIImage alloc] initWithCGImage:cgRef scale:1.0 orientation: [self isFrontCamera] ? UIImageOrientationDown : UIImageOrientationUp];
                break;
                
            case PBJCameraOrientationLandscapeRight:
                img = [[UIImage alloc] initWithCGImage:cgRef scale:1.0 orientation:[self isFrontCamera] ? UIImageOrientationUp : UIImageOrientationDown];
                break;
                
            default:
                break;
        };
        
        self.photoImage = [img fixOrientation];
    }
    
    [self.vision startPreview];
}

- (UIImage *)cropImageToPreviewFrame:(UIImage *)img
{
    CGImageRef cgRef = img.CGImage;
    CGFloat correctImgWidth = img.size.height * self.previewFrame.size.width/self.previewFrame.size.height;
    CGFloat correctImgHeight = img.size.width * self.previewFrame.size.height/self.previewFrame.size.width;
    
    cgRef = CGImageCreateWithImageInRect(cgRef, CGRectMake((img.size.height - correctImgHeight) / 2, (img.size.width - correctImgWidth) / 2, correctImgHeight, correctImgWidth));
    img = [[UIImage alloc] initWithCGImage:cgRef scale:1.0 orientation:img.imageOrientation];
    
    CGImageRelease(cgRef);
    
    return img;
}

#pragma mark Video Capturing

- (void)vision:(PBJVision *)vision capturedVideo:(nullable NSDictionary *)videoDict error:(nullable NSError *)error
{
    if (!self.isVideoRemoved)
    {
        [_assetsLibrary writeVideoAtPathToSavedPhotosAlbum:[NSURL URLWithString:videoDict[PBJVisionVideoPathKey]] completionBlock:^(NSURL *assetURL, NSError *error) {
            if (!error)
            {
                NSLog(@"Video saved to photo library successful");
            }
        }];
    }
    self.isVideoRemoved = NO;
}

// video capture progress
- (void)vision:(PBJVision *)vision didCaptureVideoSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    self.capturedVideoSeconds = vision.capturedVideoSeconds;
}

- (void)visionDidStopFocus:(PBJVision *)vision
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STCameraFocused" object:nil];
}

@end
