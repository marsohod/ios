//
//  CameraVM.h
//  Startwish
//
//  Created by Pavel Makukha on 19/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PBJVision.h"
#import "ReactiveCocoa.h"


@interface CameraVM : NSObject

@property (nonatomic, strong) PBJVision *vision;
@property (nonatomic, assign) BOOL isPhotoModeCamera;
@property (nonatomic, assign) BOOL isFrontCamera;
@property (nonatomic, assign) BOOL isVideoRemoved;
@property (nonatomic, assign) BOOL isFlashOn;
@property (nonatomic, assign) BOOL isFlashOff;
@property (nonatomic, assign) BOOL isFlashAuto;

#pragma mark - Signals

- (RACSignal *)photoShootResultSignal;
//- (RACSignal *)videoShootURLResultSignal;
- (RACSignal *)capturingVideoDurationSignal;

#pragma mark - Camera

- (void)takePhoto;
- (void)setupPreviewFrame:(CGRect)frame;
- (void)setupPhotoCamera;
- (void)setupVideoCamera;
- (void)disableCamera;
- (void)changeCameraDevice;
- (void)enableFlash;
- (void)focusToPoint:(CGPoint)point inFrame:(CGRect)frame;
- (void)changeOrientationCamera:(UIDeviceOrientation)orientation;

// video
- (void)startVideoCapturing;
- (void)pauseVideoCapturing;
- (void)resumeVideoCapturing;
- (void)endVideoCapturing;

// focus
- (CGRect)convertFocusPoint:(CGPoint)point toFrame:(CGRect)frame;

@end
