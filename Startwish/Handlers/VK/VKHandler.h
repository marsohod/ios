//
//  VKHandler.h
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "VKSdk.h"


typedef void(^emptyWithNoParams)();
@protocol VKHandlerDelegate;
@interface VKHandler : NSObject <VKSdkDelegate>

@property (weak, nonatomic) UIViewController *ctrl;
@property (nonatomic) BOOL denyByUser;
@property (weak, nonatomic) id<VKHandlerDelegate>handlerDelegate;

// Авторизация сайта при входе
+ (VKHandler *)sharedInstance;
+ (NSString *)getAppId;
+ (NSString *)getSnId;

// TODO: refactor
// deprecated
- (NSURL *)urlToAuth;
- (NSURL *)urlToAccessToken:(NSString *)code;
+ (NSString *)searchCodeInUrl:(NSString *)url;
+ (NSString *)searchTokenInUrl:(NSString *)url;
+ (BOOL)isDenyAuthInUrl:(NSString *)url;
+ (void)authByUrl:(NSString *)url success:(void (^)(NSString *token, NSString *userId, NSString *email))success deny:(void(^)(NSString *reason))deny;


- (void)setSettings:(NSDictionary *)settings;
- (void)wakeUpSession;
- (void)auth;
- (void)authRevokeAccess:(BOOL)access;
- (void)authToStartwish;
- (void)setVKSdkDelegate:(id)delegate;
- (BOOL)hasPermission;
- (void)logout;

- (void)email:(void (^)(NSDictionary *results))success failed:(void (^)(NSString *error))failed;
+ (uint32_t)userId;
+ (NSString *)userIdString;
+ (NSString *)email;
+ (NSString *)accessToken;
+ (void)isVKAuthUrl:(NSURL *)url;
+ (BOOL)canOpenApp;

- (void)postToWallWishId:(uint32_t)wishId
                    name:(NSString *)name
                    desc:(NSString *)desc
               imageLink:(NSString *)imageLink
                 success:(void (^)(NSDictionary *results))success
                  failed:(void (^)(NSString *error))failed;

- (void)postToWallWishId:(uint32_t)wishId
                    name:(NSString *)name
                    desc:(NSString *)desc
                   image:(UIImage *)image
                 success:(void (^)(NSDictionary *results))success
                  failed:(void (^)(NSString *error))failed;

- (void)postDialogWithWishId:(uint32_t)wishId
                        name:(NSString *)name
                        desc:(NSString *)desc
                   imageLink:(NSString *)imageLink
                     success:(void (^)(NSDictionary *results))success
                      failed:(void (^)(NSString *error))failed;

- (void)postDialogWithWishId:(uint32_t)wishId
                        name:(NSString *)name
                        desc:(NSString *)desc
                       image:(UIImage *)image
                     success:(void (^)(NSDictionary *results))success
                      failed:(void (^)(NSString *error))failed;

- (void)postWishId:(uint32_t)wishId
              name:(NSString *)name
              desc:(NSString *)desc
         imageLink:(NSString *)image
           success:(void (^)(NSDictionary *results))success
            failed:(void (^)(NSString *error))failed;

- (void)postWishId:(uint32_t)wishId
              name:(NSString *)name
              desc:(NSString *)desc
             image:(UIImage *)image
           success:(void (^)(NSDictionary *results))success
            failed:(void (^)(NSString *error))failed;

- (void)getUser:(void (^)(NSDictionary *user))success failed:(void (^)(NSString *error))failed;
- (void)getFriends:(void (^)(NSString *userName))success failed:(void (^)(NSString *error))failed;

@end

@protocol VKHandlerDelegate <NSObject>
@required
- (void)allowAccess;
- (void)denyAccess;
@end