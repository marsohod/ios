//
//  VKHandler.m
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "VKHandler.h"
#import "SettingsAPI.h"
#import "Settings.h"
#import "UIImage+Helper.h"
#import "NSString+Helper.h"


@interface VKHandler()
{
//    NSString*   app_id;
//    NSString*   api_version;
//    NSArray*    scope;
}
@end

@implementation VKHandler

//@"3470129"
#define APP_ID @"4749916"
#define SN_ID  @"3"
#define SCOPE @[VK_PER_OFFLINE, VK_PER_PHOTOS, VK_PER_FRIENDS, VK_PER_WALL, VK_PER_EMAIL]

//VK_PER_EMAIL, VK_PER_STATS
#define APP_VERSION @"5.27"
#define PHOTO_ALBUM_NAME @"Мой вишлист на Startwish"
#define PHOTO_ALBUM_NAME_DESC @"Мои желания на Startwish"

#define REDIRECT_URI @"oauth.vk.com/blank.html"
#define REDIRECT_TOKEN_URI @"http://startwish.ru"


+ (VKHandler *)sharedInstance
{
    static id api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[VKHandler alloc] init];
    });
    
    return api;
}

+ (NSString *)getAppId
{
    return APP_ID;
}

+ (NSString *)getSnId
{
    return SN_ID;
}

- (void)setSettings:(NSDictionary *)settings
{
//    app_id = settings[@"appId"];
//    api_version = settings[@"version"];
//    scope = settings[@"permissions"];
}

#pragma mark -
#pragma mark Auth Site during login
+ (void)isVKAuthUrl:(NSURL *)url
{
    NSRange vkRange = [[url.absoluteString lowercaseString] rangeOfString:[@"access_token=" lowercaseString]];
    NSRange vkUser = [[url.absoluteString lowercaseString] rangeOfString:[@"user_id=" lowercaseString]];
    
    if ( vkRange.location != NSNotFound ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STVKAuth" object:nil userInfo:@{@"user_id" : [url.absoluteString substringWithRange:NSMakeRange(vkUser.location + vkUser.length, [url.absoluteString length] - vkUser.location - vkUser.length)]}];
    }
}


- (NSURL *)urlToAuth
{
    return [NSURL URLWithString:[[NSString stringWithFormat:@"https://oauth.vk.com/authorize?lang=ru&client_id=%@&scope=%@&redirect_uri=%@&display=%@&v=%@&response_type=token", APP_ID, SCOPE, REDIRECT_URI, @"mobile", APP_VERSION] stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
}

- (NSURL *)urlToAccessToken:(NSString *)code
{
    return [NSURL URLWithString:[NSString stringWithFormat:@"https://oauth.vk.com/access_token?lang=ru&client_id=%@&code=%@&redirect_uri=%@", APP_ID, code, REDIRECT_URI]];
}

+ (NSString *)searchCodeInUrl:(NSString *)url
{
    NSRange textRange = [[url lowercaseString] rangeOfString:[@"#code=" lowercaseString]];
    
    if ( textRange.location != NSNotFound ) {
        return [url substringWithRange:NSMakeRange(textRange.location + textRange.length, [url length] - textRange.location - textRange.length)];
    }
    
    return nil;
}

+ (NSString *)searchTokenInUrl:(NSString *)url
{
    NSRange textRange   = [[url lowercaseString] rangeOfString:[@"access_token=" lowercaseString]];
    NSRange vkUser      = [[url lowercaseString] rangeOfString:[@"user_id=" lowercaseString]];
    
    if ( textRange.location != NSNotFound && vkUser.location != NSNotFound ) {
        NSString *withoutUserId = [url substringWithRange:NSMakeRange(0, vkUser.location - 1)];
        
        return [withoutUserId substringWithRange:NSMakeRange(textRange.location + textRange.length, [withoutUserId length] - textRange.location - textRange.length)];
    }
    
    return nil;
}

+ (BOOL)isDenyAuthInUrl:(NSString *)url
{
    NSRange textRange = [[url lowercaseString] rangeOfString:[@"#access_denied=" lowercaseString]];
    return textRange.location != NSNotFound;
}

+ (void)authByUrl:(NSString *)url success:(void (^)(NSString *token, NSString *userId, NSString *email))success deny:(void(^)(NSString *reason))deny
{
    NSArray *data = [url componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"=&"]];
    
    if( [data count] >= 8 ) {
        if ( [data[6] isEqualToString:@"email"] ) {
            if ( ![data[7] isEqualToString:@""] ) {
                success(data[1], data[5], data[7]);
                return;
            }
        }
    }
    
    // if hasn`t email
    if ( [data count] >= 6 ) {
        success(data[1], data[5], @"");
        return;
    }
    
    deny(@"");
}




- (void)setVKSdkDelegate:(id)delegate
{
    [VKSdk initializeWithDelegate: (delegate != nil ? (id<VKSdkDelegate>)delegate : (id<VKSdkDelegate>)self) andAppId:APP_ID];
}

- (void)wakeUpSession
{
    
    [VKSdk initializeWithDelegate:(id<VKSdkDelegate>)self andAppId:APP_ID];
    [VKSdk wakeUpSession];
}

- (BOOL)hasPermission
{
    return [VKSdk hasPermissions:SCOPE];
}

+ (BOOL)canOpenApp
{
    NSURL *url = [NSURL URLWithString:@"vkauthorize://authorize"];
    return [[UIApplication sharedApplication] canOpenURL:url];
}


#pragma mark -
#pragma mark Auth
- (void)auth
{
    [VKSdk authorize:SCOPE revokeAccess:self.denyByUser];
}

- (void)authRevokeAccess:(BOOL)access
{
    [VKSdk authorize:SCOPE revokeAccess:access];
}

- (void)authToStartwish
{
    [VKSdk authorize:SCOPE revokeAccess:NO forceOAuth:YES inApp:YES display:VK_DISPLAY_MOBILE];
}

- (void)logout
{
    [VKSdk forceLogout];
}


#pragma mark -
#pragma mark Post Dialog Image to Album
- (void)postDialogWithWishId:(uint32_t)wishId
                        name:(NSString *)name
                        desc:(NSString *)desc
                       image:(UIImage *)image
                     success:(void (^)(NSDictionary *results))success
                      failed:(void (^)(NSString *error))failed
{
    VKShareDialogController *shareDialog = [VKShareDialogController new];
    shareDialog.text = desc;
    shareDialog.shareLink = [[VKShareLink alloc] initWithTitle:name link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId]]];
    shareDialog.uploadImages = @[[VKUploadImage uploadImageWithImage:image andParams:[VKImageParameters jpegImageWithQuality:1.0]]];
    
    [shareDialog setCompletionHandler:^(VKShareDialogControllerResult result) {
        [self.ctrl dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self.ctrl presentViewController:shareDialog animated:YES completion:nil];
}


- (void)postDialogWithWishId:(uint32_t)wishId
                        name:(NSString *)name
                        desc:(NSString *)desc
                   imageLink:(NSString *)imageLink
                     success:(void (^)(NSDictionary *results))success
                      failed:(void (^)(NSString *error))failed
{
    
    [UIImage downloadWithUrl:[NSURL URLWithString:imageLink] onCompleteChange:^(UIImage *image) {
        [self postDialogWithWishId:wishId
                              name:name
                              desc:desc
                             image:image
                           success:success
                            failed:failed];
    }];
}


#pragma mark -
#pragma mark Post To Wall
- (void)postToWallWishId:(uint32_t)wishId
                    name:(NSString *)name
                    desc:(NSString *)desc
               imageLink:(NSString *)imageLink
                 success:(void (^)(NSDictionary *results))success
                  failed:(void (^)(NSString *error))failed
{
    [UIImage downloadWithUrl:[NSURL URLWithString:imageLink] onCompleteChange:^(UIImage *image) {
        [self postToWallWishId:wishId
                          name:name
                          desc:desc
                         image:image
                       success:success
                        failed:failed];
    }];
}


- (void)postToWallWishId:(uint32_t)wishId
                    name:(NSString *)name
                    desc:(NSString *)desc
                   image:(UIImage *)image
                 success:(void (^)(NSDictionary *results))success
                  failed:(void (^)(NSString *error))failed
{
    NSString *link = [NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId];
    
    VKAccessToken *token = [VKSdk getAccessToken];

    VKRequest *request = [VKApi uploadWallPhotoRequest:image parameters:[VKImageParameters jpegImageWithQuality:1.0] userId:[token.userId integerValue] groupId:0];

    [request executeWithResultBlock:^(VKResponse *response) {
        
        VKRequest *save = [[VKApi wall] post:@{VK_API_ATTACHMENTS : [NSString stringWithFormat:@"photo%@_%@,%@", token.userId, response.json[0][@"id"], link], VK_API_MESSAGE: desc, @"wall_id":@"0"}];
        
        [save executeWithResultBlock:^(VKResponse *response) {
            success(@{});
        } errorBlock:^(NSError *error) {
            failed([error localizedDescription]);
        }];

    } errorBlock:^(NSError *error) {
        failed([error localizedDescription]);
    }];
}


#pragma mark -
#pragma mark Post Image to Album
- (void)postWishId:(uint32_t)wishId
              name:(NSString *)name
              desc:(NSString *)desc
         imageLink:(NSString *)imageLink
           success:(void (^)(NSDictionary *results))success
            failed:(void (^)(NSString *error))failed;
{
    [UIImage downloadWithUrl:[NSURL URLWithString:imageLink] onCompleteChange:^(UIImage *image) {
        [self postWishId:wishId
                    name:name
                    desc:desc
                   image:image
                 success:success
                  failed:failed];
    }];
}

- (void)postWishId:(uint32_t)wishId
              name:(NSString *)name
              desc:(NSString *)desc
             image:(UIImage *)image
           success:(void (^)(NSDictionary *results))success
            failed:(void (^)(NSString *error))failed;
{   
    VKRequest *post = [VKApi requestWithMethod:@"photos.getAlbums" andParameters:nil andHttpMethod:@"POST"];
    
    desc = [NSString stringWithFormat:@"%@ \r%@%u", desc, [Settings sharedInstance].domainShareWish, wishId];
    
    [post executeWithResultBlock:^(VKResponse *response) {
        if ( response.json[@"error"] == nil ) {
            
            NSInteger aid = [self existPhotoAlbumId:response.json[@"items"]];
            
            if ( aid != 0 ) {
                [self addPhoto:image caption:desc toAlbumId:aid success:success failed:failed];
            } else {
                [self createAlbumAndPostPhoto:image caption:desc success:success failed:failed];
            }
            
        } else {
            failed(response.json[@"error"][@"error_msg"]);
        }
    } errorBlock:^(NSError *error) {
        failed( [error localizedDescription] );
    }];
}

- (NSInteger)existPhotoAlbumId:(NSArray *)albums
{
    __block NSInteger aid = 0;
    
    if ( albums ) {
        [albums enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ( [obj[@"title"] isEqualToString:PHOTO_ALBUM_NAME] ) {
                aid = [obj[@"id"] integerValue];
            }
        }];
    }
    
    return aid;
}

- (void)addPhoto:(UIImage *)image caption:(NSString *)caption toAlbumId:(NSInteger)aid success:(void (^)(NSDictionary *results))success failed:(void (^)(NSString *error))failed;
{
    VKRequest *server = [[VKApi photos] getUploadServer:aid];
    
    [server executeWithResultBlock:^(VKResponse *response) {

        VKRequest *upPhoto = [VKRequest  photoRequestWithPostUrl:response.json[@"upload_url"]
                                                 withPhotos:@[[VKUploadImage uploadImageWithData:UIImageJPEGRepresentation(image, 0.7) andParams:[VKImageParameters jpegImageWithQuality:1.0]]]];
        
        [upPhoto executeWithResultBlock:^(VKResponse *responseUpload) {

            if ( responseUpload.json[@"photos_list"] ) {
                VKRequest *savePhoto = [[VKApi photos] save:@{@"server"        :   responseUpload.json[@"server"],
                                                              @"hash"          :   responseUpload.json[@"hash"],
                                                              @"photos_list"   :   responseUpload.json[@"photos_list"],
                                                              @"caption"       :   caption,
                                                              @"album_id"      :   responseUpload.json[@"aid"]}];
                
                [savePhoto executeWithResultBlock:^(VKResponse *response) {
                    success(@{});
                } errorBlock:^(NSError *error) {
                   failed( [error localizedDescription] );
                }];
            } else {
                failed( @"" );
            }
        } errorBlock:^(NSError *error) {
             failed( [error localizedDescription] );
        }];
    } errorBlock:^(NSError *error) {
        failed( [error localizedDescription] );
    }];
}

- (void)createAlbumAndPostPhoto:(UIImage *)image caption:desc success:(void (^)(NSDictionary *results))success failed:(void (^)(NSString *error))failed
{

    VKRequest *post = [VKApi requestWithMethod:@"photos.createAlbum" andParameters: @{@"title"          : PHOTO_ALBUM_NAME,
                                                                                      @"description"    : PHOTO_ALBUM_NAME_DESC,
                                                                                      } andHttpMethod:@"POST"];
    
    [post executeWithResultBlock:^(VKResponse *response) {
        
        if ( response.json[@"error"] == nil ) {
            
            [self addPhoto:image caption:desc toAlbumId:[response.json[@"id"] integerValue] success:success failed:failed];
        } else {
            failed(response.json[@"error"][@"error_msg"]);
        }
    } errorBlock:^(NSError *error) {
        failed( [error localizedDescription] );
    }];

}

- (void)getUser:(void (^)(NSDictionary *user))success failed:(void (^)(NSString *error))failed
{
    VKRequest *get = [VKApi requestWithMethod:@"users.get" andParameters:@{@"user_ids": [VKHandler userIdString], @"fields": @"sex,bdate,city,country,photo_max,nickname,email"} andHttpMethod:@"GET"];
    
    get.preferredLang = @"ru";
    
    [get executeWithResultBlock:^(VKResponse *response) {
        if ( [(NSArray *)response.json count] > 0 ) {
            success( [self prepareUserVK:response.json[0]] );
        } else {
            success(@{});
        }
    } errorBlock:^(NSError *error) {
        failed([error localizedDescription]);
    }];
}

- (NSDictionary *)prepareUserVK:(NSDictionary *)userVK
{
    NSMutableDictionary *u = [NSMutableDictionary dictionary];
    NSArray *b;
    
    if ( userVK[@"bdate"] ) {
        b = [userVK[@"bdate"] componentsSeparatedByString:@"."];
    }
    
    u[@"sex"]           = userVK[@"sex"];
    u[@"username"]      = [NSString stringWithFormat:@"%@ %@", userVK[@"first_name"], userVK[@"last_name"]];
    u[@"avatar"]        = userVK[@"photo_max"];
    u[@"page_identity"] = userVK[@"nickname"];
    u[@"login"]         = [VKHandler email];
    u[@"email"]         = [VKHandler email];
    
    if ( b ) {
        u[@"birthday"]  = [NSString stringWithFormat:@"%@-%@-%@", [b lastObject], [b objectAtIndex:1], [b firstObject]];
    }
    
    if ( userVK[@"city"] ) {
        u[@"city"] = userVK[@"city"];
    }
    
    return u;
}

- (void)getFriends:(void (^)(NSString *userName))success failed:(void (^)(NSString *error))failed
{
    VKRequest *get = [VKApi requestWithMethod:@"friends.get" andParameters:@{@"user_id": [VKHandler userIdString]} andHttpMethod:@"GET"];
    [get executeWithResultBlock:^(VKResponse *response) {
        if ( [(NSArray *)response.json count] > 0 ) {
            NSLog(@"friends.get: %@", response.json);
            success(@"nona");
        } else {
            success(@"noName");
        }
    } errorBlock:^(NSError *error) {
        failed([error localizedDescription]);
    }];
}


+ (NSString *)accessToken
{
    VKAccessToken *token = [VKSdk getAccessToken];
    
    return token.accessToken;
}

+ (NSString *)email
{
    VKAccessToken *token = [VKSdk getAccessToken];
    
    return token.email;
}

+ (uint32_t)userId
{
    return [NSString stringToUint32_t:[VKSdk getAccessToken].userId];
}

+ (NSString *)userIdString
{
    return [NSString stringWithFormat:@"%@", [VKSdk getAccessToken].userId];
}

- (void)email:(void (^)(NSDictionary *results))success failed:(void (^)(NSString *error))failed
{
    VKRequest *e = [VKApi requestWithMethod:@"users.get" andParameters:@{@"fields" : @"contacts"} andHttpMethod:@"GET"];
    
    [e executeWithResultBlock:^(VKResponse *response) {
        if ( response.json[@"error"] == nil ) {

            success( response.json );
        } else {
            failed(response.json[@"error"][@"error_msg"]);
        }
    } errorBlock:^(NSError *error) {
        failed( [error localizedDescription] );
    }];
}

#pragma mark -
#pragma mark - VKSdkDelegate
- (void)vkSdkReceivedNewToken:(VKAccessToken*) newToken
{
    [VKHandler sharedInstance].denyByUser = NO;
    
    if ( [self.handlerDelegate respondsToSelector:@selector(allowAccess)] ) {
        [self.handlerDelegate allowAccess];
    }
}

- (void)vkSdkUserDeniedAccess:(VKError*) authorizationError
{
    [VKHandler sharedInstance].denyByUser = YES;
    
    if ( [self.handlerDelegate respondsToSelector:@selector(denyAccess)] ) {
        [self.handlerDelegate denyAccess];
    }
}

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    [self.ctrl presentViewController: controller animated:YES completion:nil];    
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:self.ctrl];
}

- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
}

- (void)vkSdkWillDismissViewController:(UIViewController *)controller
{
    
}



@end
