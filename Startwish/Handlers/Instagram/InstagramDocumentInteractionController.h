//
//  InstagramDocumentInteractionController.h
//  Startwish
//
//  Created by Pavel Makukha on 14/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface InstagramDocumentInteractionController : UIDocumentInteractionController

- (id)initWithUrl:(NSURL *)url;

@end
