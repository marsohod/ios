//
//  InstagramHandler.m
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <AssetsLibrary/AssetsLibrary.h>
#import "InstagramHandler.h"
#import "UIImage+Helper.h"
#import "NSString+Helper.h"


@implementation InstagramHandler

+ (InstagramHandler *)sharedInstance
{
    static id api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[InstagramHandler alloc] init];
    });
    
    return api;
}

+ (BOOL)canOpenApp
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    return [[UIApplication sharedApplication] canOpenURL:instagramURL];
}

- (void)postImage:(UIImage *)image caption:(NSString *)caption
{
    if ([InstagramHandler canOpenApp]) {
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        
                    NSString *writePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/instagram.igo"];
                    if (![imageData writeToFile:writePath atomically:YES]) {
                        // failure
                        return;
                    } else {
                        // success.
                    }

        NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", writePath]];
        NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://photo?path=%@&InstagramCaption=%@",igImageHookFile.absoluteString,[caption urlencode]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
            [[UIApplication sharedApplication] openURL:instagramURL];
        }
        
//        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
//        [library writeImageDataToSavedPhotosAlbum:imageData metadata:@{} completionBlock:^(NSURL *assetURL, NSError *error) {
//            NSURL *instagramURL = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@",igImageHookFile.absoluteString,[caption urlencode]]];
//            
//            if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
//                [[UIApplication sharedApplication] openURL:instagramURL];
//            }
//            
//        }];
    }
}

+ (void)fileUrlToImageUrl:(NSString *)url afterSave:(void (^)(NSURL *filePath))success
{
    [UIImage downloadWithUrl:[NSURL URLWithString:url] onCompleteChange:^(UIImage *image) {
        [self fileUrlToImage:image afterSave:success];
    }];
}

+ (void)fileUrlToImage:(UIImage *)image afterSave:(void (^)(NSURL *filePath))success
{
    if ([InstagramHandler canOpenApp]) {
        NSString  *imagePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"instagram.igo"];//[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/instagram.igo"];
        if (![UIImageJPEGRepresentation(image, 1.0) writeToFile:imagePath atomically:YES]) {
            // failure
            return;
        } else {
            
        }
        success( [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", imagePath]] );
    }
}


- (void)postImageUrl:(NSString *)imgUrl caption:(NSString *)caption
{
    [UIImage downloadWithUrl:[NSURL URLWithString:imgUrl] onCompleteChange:^(UIImage *image) {
        [self postImage:image caption:caption];
    }];

}

- (NSData *)prepareImageToPost:(UIImage *)image
{
    CGFloat cropVal = (image.size.height > image.size.width ? image.size.width : image.size.height);
    
    cropVal *= [image scale];
    
    CGRect cropRect = (CGRect){.size.height = cropVal, .size.width = cropVal};
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    NSData *imageData = UIImageJPEGRepresentation([UIImage imageWithCGImage:imageRef], 1.0);
    CGImageRelease(imageRef);
    
    return imageData;
}

@end
