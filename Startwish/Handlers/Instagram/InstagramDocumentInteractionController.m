//
//  InstagramDocumentInteractionController.m
//  Startwish
//
//  Created by Pavel Makukha on 14/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "InstagramDocumentInteractionController.h"


@implementation InstagramDocumentInteractionController


- (id)initWithUrl:(NSURL *)url
{
    self = [super init];
    
    if ( self ) {
        self.delegate = (id<UIDocumentInteractionControllerDelegate>)self;
        self.UTI = @"com.instagram.exclusivegram";
        self.URL = url;
        
    }
    
    return self;
}
//- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController *)controller
//{
//
//}
//- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
//{
//    return CGRectZero;
//}
//- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
//{
//    return nil;
//}
//-(void)documentInteractionController:(UIDocumentInteractionController *)controller willBeginSendingToApplication:(NSString *)application
//{
//
//}
//
//-(void)documentInteractionController: (UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
//{
//
//}
//
//- (BOOL)documentInteractionController:(UIDocumentInteractionController *)controller   canPerformAction:(SEL)action{
//    return false;
//}



@end
