//
//  InstagramHandler.h
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface InstagramHandler : NSObject

+ (InstagramHandler *)sharedInstance;

- (void)postImage:(UIImage *)image caption:(NSString *)caption;
- (void)postImageUrl:(NSString *)imgUrl caption:(NSString *)caption;
+ (BOOL)canOpenApp;

+ (void)fileUrlToImageUrl:(NSString *)url afterSave:(void (^)(NSURL *filePath))success;
+ (void)fileUrlToImage:(UIImage *)image afterSave:(void (^)(NSURL *filePath))success;

@end
