//
//  FacebookHandler.m
//  Startwish
//
//  Created by Pavel Makukha on 19/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "FacebookHandler.h"
#import "ActionTexts.h"
#import "Settings.h"


@implementation FacebookHandler

#define PERMISSION @"publish_actions"


#pragma mark - Login
+ (BOOL)isLogin
{
    return [FBSDKAccessToken currentAccessToken] ? YES : NO;
}

+ (void)login
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[PERMISSION] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if ([result.grantedPermissions containsObject:@"email"]) {
                // Do work
            }
        }
    }];
}

+ (void)loginWithSuccess:(emptyWithNoParams)success
{
    [self loginWithSuccess:success failed:^{}];
}

+ (void)loginWithSuccess:(emptyWithNoParams)success failed:(emptyWithNoParams)failed
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    if (![UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]])
    {
        login.loginBehavior = FBSDKLoginBehaviorWeb;
    }
    
    [login logInWithPublishPermissions:@[PERMISSION] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            failed();
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            success();
            
            if ([result.grantedPermissions containsObject:PERMISSION]) {
                success();
                return;
            }
        }
    }];
}

+ (void)hadPublishPermission:(emptyWithNoParams)success failed:(emptyWithNoParams)failed
{
    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        success();
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        
        if (![UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]]) {
            loginManager.loginBehavior = FBSDKLoginBehaviorWeb;
        }
        
        [loginManager logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//            if ( result.isCancelled ) {
//                failed();
//                return;
//            }
            
            if ([result.grantedPermissions containsObject:@"publish_actions"]) {
                success();
                return;
            }
            
            failed();
        }];
    }
}

#pragma mark - Logout

+ (void)logout
{
    FBSDKLoginManager *lm = [[FBSDKLoginManager alloc] init];
    [lm logOut];
}


+ (NSString *)token
{
    return [FBSDKAccessToken currentAccessToken].tokenString;
}

#pragma mark - Post to Feed

+ (void)postWishId:(uint32_t)wishId
              name:(NSString *)name
           caption:(NSString *)caption
              desc:(NSString *)desc
         imageLink:(NSString *)image
           success:(void (^)(NSDictionary *results))success
            failed:(void (^)(NSString *error))failed
         withQueue:(NSOperationQueue *)queue
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   name, @"name",
                                   caption, @"message",
                                   desc, @"description",
                                   [NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId], @"link",
                                   [image stringByReplacingOccurrencesOfString:@"storage_0" withString:@"storage"], @"picture",
                                   nil];

    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/feed"
                                                                   parameters:params
                                                                   HTTPMethod:@"POST"];
    

    if ([[FBSDKAccessToken currentAccessToken] hasGranted:@"publish_actions"]) {
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                success( result );
            } else {
                failed( error.description );
            }
        } withQueue:queue];        
    } else {
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        
        if (![UIApplication.sharedApplication canOpenURL:[NSURL URLWithString:@"fb://"]]) {
            loginManager.loginBehavior = FBSDKLoginBehaviorWeb;
        }
        
        [loginManager logInWithPublishPermissions:@[@"publish_actions"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if ( result.isCancelled ) {
                return;
            }
            
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 if (!error) {
                     success( result );
                 } else {
                     failed( error.description );
                 }
             } withQueue:queue];
        }];
    }
}

/*
#pragma mark -
#pragma mark Post
+ (void)postDialogWithWishId:(uint32_t)wishId
                        name:(NSString *)name
                     caption:(NSString *)caption
                        desc:(NSString *)desc
                   imageLink:(NSString *)image
                     success:(void (^)(NSDictionary *results))success
                      failed:(void (^)(NSString *error))failed
{
//    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
//    content.contentURL = [NSURL URLWithString:@"https://developers.facebook.com"];
    
    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
    params.link = [NSURL URLWithString:[NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId]];
    
    // If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params]) {
        // Present share dialog
        [FBDialogs presentShareDialogWithLink:params.link
                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                          if(error) {
                                              // An error occurred, we need to handle the error
                                              // See: https://developers.facebook.com/docs/ios/errors
                                              // NSLog(@"Error publishing story: %@", error.description);
                                              // [[ActionTexts sharedInstance] failedText:@"FacebookPost"]
                                              failed( error.description );
                                          } else {
                                              // Success
                                              success( results );
                                          }
                                      }];
    } else {
        // Put together the dialog parameters
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       name, @"name",
                                       caption, @"caption",
                                       desc, @"description",
                                       [NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId], @"link",
                                       [image stringByReplacingOccurrencesOfString:@"storage_0" withString:@"storage"], @"picture",
                                       nil];
        
        // Show the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // An error occurred, we need to handle the error
                                                          // See: https://developers.facebook.com/docs/ios/errors
                                                          failed( error.description );
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // User cancelled.
                                                          } else {
                                                              // Handle the publish feed callback
                                                              NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                                                              
                                                              if (![urlParams valueForKey:@"post_id"]) {
                                                                  // User cancelled.
                                                              } else {
                                                                  success( @{@"result": [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]]} );
                                                              }
                                                          }
                                                      }
                                                  }];
    }
}


+ (void)postWishId:(uint32_t)wishId
                      name:(NSString *)name
                   caption:(NSString *)caption
                      desc:(NSString *)desc
                 imageLink:(NSString *)image
                   success:(void (^)(NSDictionary *results))success
                    failed:(void (^)(NSString *error))failed
{
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   name, @"name",
                                   caption, @"caption",
                                   desc, @"description",
                                   [NSString stringWithFormat:@"%@%u", [Settings sharedInstance].domainShareWish, wishId], @"link",
                                   [image stringByReplacingOccurrencesOfString:@"storage_0" withString:@"storage"], @"picture",
                                   nil];
    
    // Make the request
    [FBRequestConnection startWithGraphPath:@"/me/feed"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                              if (!error) {
                                  // Link posted successfully to Facebook
                                  success( result );
                              } else {
                                  // An error occurred, we need to handle the error
                                  // See: https://developers.facebook.com/docs/ios/errors
                                  failed( error.description );
                              }
                          }];
}


// A function for parsing URL parameters returned by the Feed Dialog.
+ (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}



#pragma mark -
#pragma mark Login/Logout

+ (void)openActiveSession
{
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        
        // If there's one, just open the session silently, without showing the user the login UI
        [FBSession openActiveSessionWithReadPermissions:@[PERMISSION]
                                           allowLoginUI:NO
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          // Handler for session state changes
                                          // Call this method EACH time the session state changes,
                                          //  NOT just when the session open
                                          [self sessionStateChanged:session state:state error:error];
                                      }];
        
    }
}

+ (BOOL)isLogin
{
    return [FBSession activeSession].isOpen;
}

+ (void)login
{
    [self loginWithSuccess:^{} failed:^{}];
}

+ (void)loginWithSuccess:(emptyWithNoParams)success
{
    [self loginWithSuccess:success failed:^{}];
}

+ (void)loginWithSuccess:(emptyWithNoParams)success failed:(emptyWithNoParams)failed
{

    // If the session state is any of the two "open" states when the button is clicked
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        
        
//        if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded) {
//            // we have a cached token, so open the session
//            [[FBSession activeSession] openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent
//                                      completionHandler:completionHandler];
//        } else {
////            [self clearAllUserInfo];
//            // create a new facebook session
//            FBSession *fbSession = [[FBSession alloc] initWithPermissions:@[PERMISSION]];
//            [FBSession setActiveSession:fbSession];
//            [fbSession openWithBehavior:FBSessionLoginBehaviorUseSystemAccountIfPresent
//                      completionHandler:completionHandler];
//        }
        
        [FBSession openActiveSessionWithReadPermissions:@[PERMISSION]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                          
                                          // Retrieve the app delegate
                                          // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
                                          [self sessionStateChanged:session state:state error:error success:success failed:failed];
                                      }];

    }
}

+ (void)logout
{
    [FBSession.activeSession closeAndClearTokenInformation];
//    [FBSession renewSystemCredentials:^(ACAccountCredentialRenewResult result, NSError *error) {
//        NSLog(@"%@", error);
//    }];
//    [FBSession setActiveSession:nil];
}


+ (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    [self sessionStateChanged:session state:state error:error success:^{} failed:^{}];
}


// Handles session state changes in the app
+ (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error success:(emptyWithNoParams)success failed:(emptyWithNoParams)failed
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        
        // Show the user the logged-in UI
        success();
//        [self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        
        // Show the user the logged-out UI
        failed();
//        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
//            alertText = [FBErrorUtility userMessageForError:error];
//            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        failed();
//        [self userLoggedOut];
    }
}

+ (NSString *)token
{
    return [FBSession.activeSession accessTokenData].accessToken;
}
*/

@end
