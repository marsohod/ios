//
//  FacebookHandler.h
//  Startwish
//
//  Created by Pavel Makukha on 19/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>

typedef void(^emptyWithNoParams)();

@interface FacebookHandler : NSObject

#pragma mark - Login
+ (BOOL)isLogin;
+ (void)loginWithSuccess:(emptyWithNoParams)success;
+ (void)loginWithSuccess:(emptyWithNoParams)success failed:(emptyWithNoParams)failed;

+ (void)hadPublishPermission:(emptyWithNoParams)success failed:(emptyWithNoParams)failed;

#pragma mark - Logout
+ (void)logout;

#pragma mark - Post to Feed
+ (void)postWishId:(uint32_t)wishId
                name:(NSString *)name
             caption:(NSString *)caption
                desc:(NSString *)desc
           imageLink:(NSString *)image
             success:(void (^)(NSDictionary *results))success
              failed:(void (^)(NSString *error))failed
         withQueue:(NSOperationQueue *)queue;

#pragma mark - Token
+ (NSString *)token;

@end
