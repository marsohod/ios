//
//  PushNotificationHandler.h
//  Startwish
//
//  Created by Pavel Makukha on 21/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface PushNotificationHandler : NSObject

@property (nonatomic) NSDictionary *tappedPush;

+ (PushNotificationHandler *) sharedInstance;
+ (NSString *)token;
+ (BOOL)hasToken;
+ (void)saveToken:(NSString *)token;
+ (void)cleanTokenWithCallBack:(void(^)())callback;
+ (void)prepareControllerToPush:(NSDictionary *)userInfo;

+ (void)registerPushNotificationFromDeviceToken:(NSData *)deviceToken;
+ (void)responseForRemoteNotifications;

+ (void)cleanNewEventsCount;
@end
