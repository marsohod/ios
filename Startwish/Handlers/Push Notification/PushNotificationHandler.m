//
//  PushNotificationHandler.m
//  Startwish
//
//  Created by Pavel Makukha on 21/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PushNotificationHandler.h"
#import "LoginAPI.h"
#import "Me.h"
#import "UserAPI.h"
#import "ModelWish.h"
#import "WishesAPI.h"
#import "Routing.h"
#import "NSString+Helper.h"
#import "ActionTexts.h"


// @sector id
//  1. Комментарий
//  3. +1 подписчик если friendship_status == 0; Вы друзья если friendship_status == 1;
//  4.
//  5.
//  6. Желание исполнилось
//  7.
//  8. День Рождения
//  9.
//  10. Общее желание
//  11. Событие от Startwish
//  12. Зарегистрировался друг из VK или FB зависит от snType
//  13.

#define NAME @"pushToken"

@implementation PushNotificationHandler

+ (PushNotificationHandler *) sharedInstance
{
    static PushNotificationHandler *p = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        p = [[PushNotificationHandler alloc] init];
    });
    
    return p;
}

+ (NSString *)token
{
    NSUserDefaults *usersDefault = [NSUserDefaults standardUserDefaults];

    NSLog(@"deviceToken: %@", [usersDefault objectForKey:NAME]);
    
    if ( [usersDefault objectForKey:NAME] ) {
        return [usersDefault objectForKey:NAME];
    }

    return @"";
}

+ (BOOL)hasToken
{
    NSUserDefaults *usersDefault = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"deviceToken: %@", [usersDefault objectForKey:NAME]);
    
    return [usersDefault objectForKey:NAME] ? YES : NO;
}

+ (void)saveToken:(NSString *)token
{
    NSUserDefaults *usersDefault = [NSUserDefaults standardUserDefaults];
    [usersDefault setObject:token forKey:NAME];
}

+ (void)cleanTokenWithCallBack:(void(^)())callback
{
    [[UserAPI sharedInstance] removePushNotification:[self token] callback:^{
        NSUserDefaults *usersDefault = [NSUserDefaults standardUserDefaults];
        [usersDefault removeObjectForKey:NAME];
        
        if ( callback ) {
            callback();
        }
    }];
}

+ (void)prepareControllerToPush:(NSDictionary *)userInfo
{
    if ( userInfo[@"sectorId"] && userInfo[@"objectId"] ) {
        switch ( [userInfo[@"sectorId"] integerValue] ) {
            case 1: {
                [[WishesAPI sharedInstance] getWishById:[NSString stringToUint32_t:userInfo[@"objectId"]] withCallBack:^(id item) {
                    [[Routing sharedInstance] showCommentsControllerWithWishId:[item getId] wishName:[item getName] comments:nil maxCount:[item getCommentsCount] replyToCommentId:0 replyToUserName:nil];
                }];
                
                break;
            }
                
            case 3: {
                [[Routing sharedInstance] showUserControllerWithUser:[[ModelUser alloc] initWithId:[NSString stringToUint32_t:userInfo[@"emitterId"]]] fromMenu:NO];

                break;
            }
                
            case 8: {
                [[UserAPI sharedInstance] getUserById:[NSString stringToUint32_t:userInfo[@"emitterId"]] withCallback:^(ModelUser *man) {
                    [[Routing sharedInstance] showUserControllerWithUser:man fromMenu:NO];
                }];
                
                break;
            }
                
            case 10: {
                [[WishesAPI sharedInstance] getWishById:[NSString stringToUint32_t:userInfo[@"objectId"]] withCallBack:^(id item) {
                    [[Routing sharedInstance] showWishCtrlWithWish:item];
                }];
                
                break;
            }
                
            case 11: {
                [[Routing sharedInstance] showEventsController];
                
                break;
            }
                
            case 12: {
                [[UserAPI sharedInstance] getUserById:[NSString stringToUint32_t:userInfo[@"emitterId"]] withCallback:^(ModelUser *man) {
                    [[Routing sharedInstance] showUserControllerWithUser:man fromMenu:NO];
                }];
                
                break;
            }
                
            default:
                break;
        }
        
        if ( [UIApplication sharedApplication].applicationIconBadgeNumber == 1 ) { [[UserAPI sharedInstance] redeemEvents]; }
        [UIApplication sharedApplication].applicationIconBadgeNumber--;
    }
}

+ (void)registerPushNotificationFromDeviceToken:(NSData *)deviceToken
{
    if ( ![self hasToken] ) {
        NSString *token = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [[UserAPI sharedInstance] registerPushNotification:token success:^(id item) {
            [self saveToken:token];
        } failedBlock:^(id item) {
            
        }];
    }
}

+ (void)responseForRemoteNotifications
{
    if ( ![PushNotificationHandler hasToken] && [[LoginAPI sharedInstance] isLogged] ) {
        if ( [[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)] ) {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        } else {
    #if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_8_0
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    #endif
        }
    }
}

+ (void)cleanNewEventsCount
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil];
}

@end
