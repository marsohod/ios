//
//  TwitterHandler.m
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "TwitterHandler.h"
#import "FHSTwitterEngine.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "UIImage+Helper.h"
#import "ActionTexts.h"


#define KEY @"CSZKOzLNvb33lNZpwHrGv1YhG"
#define KEY_SECRET @"5WdPFplzYx4NPch3fh1fIBcZ8JuF3KLBTikVrN8FdLdZ7AmEwg"

@implementation TwitterHandler

+ (void)start
{
    [[FHSTwitterEngine sharedEngine] permanentlySetConsumerKey:KEY andSecret:KEY_SECRET];
    [[FHSTwitterEngine sharedEngine] loadAccessToken];
}

+ (BOOL)isLogin
{
    return [[FHSTwitterEngine sharedEngine] isAuthorized];
}


+ (void)login:(void (^)())success canceled:(void (^)(NSString *error))failed ctrl:(UIViewController *)ctrl
{
    UIViewController *loginController = [[FHSTwitterEngine sharedEngine] loginControllerWithCompletionHandler:^(BOOL isSuccess) {
        
        isSuccess ? success() : failed([[ActionTexts sharedInstance] failedText:@"TwitterAccess"]);
        
    }];
    
    [ctrl presentViewController:loginController animated:YES completion:nil];
}

+ (void)logout
{
    [[FHSTwitterEngine sharedEngine] clearAccessToken];
}

+ (void)postTweetWithWishId:(uint32_t)wishId text:(NSString *)text imageLink:(NSString *)imageLink success:(void (^)())success canceled:(void (^)(NSString *string))failed
{
    [UIImage downloadWithUrl:[NSURL URLWithString:imageLink] onCompleteChange:^(UIImage *image) {
        [self postTweetWithWishId:wishId text:text image:image success:success canceled:failed];
    }];
}

+ (void)postTweetWithWishId:(uint32_t)wishId text:(NSString *)text image:(UIImage *)image success:(void (^)())success canceled:(void (^)(NSString *string))failed
{
    return [self postTweet:[NSString stringWithFormat:@"%@ %@%u", text, [Settings sharedInstance].domainShareWish, wishId] withImage:image success:success canceled:failed];
}

+ (void)postTweet:(NSString *)text success:(void (^)())success canceled:(void (^)(NSString *string))failed
{
    NSError *error = [[FHSTwitterEngine sharedEngine] postTweet:text];

    if( error == nil ) {
        success();
    } else {
        failed( [error localizedDescription] );
    }
    
//    [self statusUpdate:text mediaId:@"" success:success failed:failed];
}

+ (void)postTweet:(NSString *)text withImage:(UIImage *)image success:(void (^)())success canceled:(void (^)(NSString *string))failed
{
    NSError *error = [[FHSTwitterEngine sharedEngine] postTweet:text withImageData:UIImageJPEGRepresentation(image, 0.7)];
    if( error == nil ) {
        success();
    } else {
        failed( [error localizedDescription] );
    }
}

@end
