//
//  TwitterHandler.h
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface TwitterHandler : NSObject

+ (void)start;

+ (BOOL)isLogin;
+ (void)login:(void (^)())success canceled:(void (^)(NSString *error))failed ctrl:(UIViewController *)ctrl;
+ (void)logout;

+ (void)postTweetWithWishId:(uint32_t)wishId text:(NSString *)text image:(UIImage *)image success:(void (^)())success canceled:(void (^)(NSString *string))failed;
+ (void)postTweetWithWishId:(uint32_t)wishId text:(NSString *)text imageLink:(NSString *)imageLink success:(void (^)())success canceled:(void (^)(NSString *string))failed;
+ (void)postTweet:(NSString *)text withImage:(UIImage *)image success:(void (^)())success canceled:(void (^)(NSString *string))failed;
+ (void)postTweet:(NSString *)text success:(void (^)())success canceled:(void (^)(NSString *string))failed;
@end
