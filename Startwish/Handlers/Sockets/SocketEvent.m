//
//  SocketEvent.m
//  Startwish
//
//  Created by Pavel Makukha on 02/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SocketEvent.h"
#import "Settings.h"


@implementation SocketEvent

+ (SocketEvent *)sharedInstance
{
    static SocketEvent *socket = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/test/websockets", [Settings sharedInstance].domainAPI]];
        socket = [[SocketEvent alloc] initWithURLRequest:[NSURLRequest requestWithURL:url]];
        socket.delegate = (id<SRWebSocketDelegate>)self;
    });
    
    return socket;
}

//- (void)open
//{
//
//}
//
//- (void)close
//{
//
//}
//
//- (void)send:(id)data
//{
//
//}

#pragma mark -
#pragma mark SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{

}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{

}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{

}


@end
