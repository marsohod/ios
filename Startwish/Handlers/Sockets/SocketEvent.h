//
//  SocketEvent.h
//  Startwish
//
//  Created by Pavel Makukha on 02/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "SRWebSocket.h"


@interface SocketEvent : SRWebSocket

+ (SocketEvent *)sharedInstance;

@end
