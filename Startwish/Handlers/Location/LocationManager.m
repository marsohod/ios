//
//  LocationManager.m
//  Startwish
//
//  Created by Pavel Makukha on 17/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "LocationManager.h"
#import "ActionTexts.h"


@interface LocationManager ()
{
    CLLocationManager   *locationManager;
    CLGeocoder          *geocoder;
    CLPlacemark         *placemark;
}

@end

@implementation LocationManager

- (instancetype)init
{
    self = [super init];

    if ( self ) {
        geocoder = [[CLGeocoder alloc] init];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = (id<CLLocationManagerDelegate>)self;
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
        locationManager.distanceFilter = 500; // meters
        [locationManager requestWhenInUseAuthorization];
    }
    
    return self;
}

- (void)startUpdatingLocation
{
    [locationManager startUpdatingLocation];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:[[ActionTexts sharedInstance] failedText:@"default"] message:[[ActionTexts sharedInstance] failedText:@"LocationDoesNotDetermine"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
//    CLLocationDegrees latitude;
//    CLLocationDegrees longitude;
    
    if (currentLocation != nil) {
        [self.delegate afterDetectLocation:currentLocation placemark:nil];
    }
    
//    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
//        
//        if (error == nil && [placemarks count] > 0) {
//            placemark = [placemarks lastObject];
//            NSLog(@"%@", placemark.locality);
////            NSLog(@"%@", [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
////                          placemark.subThoroughfare, placemark.thoroughfare,
////                          placemark.postalCode, placemark.locality,
////                          placemark.administrativeArea,
////                          placemark.country]);
//        } else {
//            NSLog(@"%@", error.debugDescription);
//        }
//    } ];
}

@end
