//
//  LocationManager.h
//  Startwish
//
//  Created by Pavel Makukha on 17/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol LocationManagerDelegate;
@interface LocationManager : NSObject <CLLocationManagerDelegate>

@property(nonatomic, weak) id<LocationManagerDelegate>delegate;

- (void)startUpdatingLocation;

@end

@protocol LocationManagerDelegate <NSObject>

- (void)afterDetectLocation:(CLLocation *)currentLocation placemark:(NSObject *)placemark;
- (void)showErrorDetectLocation:(NSString *)error;

@end
