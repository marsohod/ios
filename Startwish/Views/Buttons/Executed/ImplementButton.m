//
//  ImplementButton.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImplementButton.h"
#import "UIColor+Helper.h"
#import "ActionTexts.h"


@implementation ImplementButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor: [UIColor h_orange2]];
    [self setTitle:[[ActionTexts sharedInstance] titleText:@"Execute"] forState:UIControlStateNormal];
    [self addIcon: [[UIImage imageNamed:@"done_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(8.0, 0.0, 10.0, 18.0)];
}

@end
