//
//  CustomButton.m
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomButton.h"
#import "UIFont+Helper.h"


@implementation CustomButton

#define BUTTON_RADIUS 3.0
#define BUTTON_HEIGHT 44.0

- (void)awakeFromNib
{
    self.layer.cornerRadius = BUTTON_RADIUS;
    self.titleLabel.font = [UIFont systemFontOfSize:22.0];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTintColor:[UIColor whiteColor]];

    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
}

- (void)hideUnderline
{
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];
    
    [attrStr removeAttribute:NSUnderlineStyleAttributeName range:NSMakeRange(0, [attrStr length])];
    
    self.titleLabel.attributedText = attrStr;
    
//    [attrStr enumerateAttributesInRange:NSMakeRange(0, [attrStr length])
//                                options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired
//                             usingBlock:^(NSDictionary *attributes, NSRange range, BOOL *stop)
//     {
//         NSMutableDictionary *mutableAttributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
//         [mutableAttributes removeObjectForKey:NSUnderlineStyleAttributeName];
//         [attrStr setAttributes:mutableAttributes range:range];
//     }];
}

@end
