//
//  YellowButton.m
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "YellowButton.h"
#import "UIColor+Helper.h"


@implementation YellowButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor h_yellow];
}

@end
