//
//  RedButton.m
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "RedButton.h"
#import "UIColor+Helper.h"


@implementation RedButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor h_red];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
