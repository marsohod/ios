//
//  FillGreenButton.h
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "FillCircleButton.h"

@interface FillGreenButton : FillCircleButton

//- (void)addIcon:(UIImage *)icon;
- (void)setButtonBackground;

@end
