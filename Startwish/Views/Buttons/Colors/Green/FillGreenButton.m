//
//  FillGreenButton.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "FillGreenButton.h"
#import "UIColor+Helper.h"

@implementation FillGreenButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setButtonBackground];
}

- (void)setButtonBackground
{
    [self setBackgroundColor: [UIColor h_smoothGreen]];
}

//- (void)addIcon:(UIImage *)icon
//{
//    [self setImage: icon forState:UIControlStateNormal];
//    [self setImage: icon forState:UIControlStateHighlighted];
//}

@end
