//
//  OrangeMiddleButton.m
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "OrangeMiddleButton.h"
#import "UIColor+Helper.h"


@implementation OrangeMiddleButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor h_orange_middle];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
