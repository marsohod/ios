//
//  AddButton.h
//  Startwish
//
//  Created by Pavel Makukha on 27/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface AddButton : UIButton

- (void)setImageColor:(NSString *)color;

@end
