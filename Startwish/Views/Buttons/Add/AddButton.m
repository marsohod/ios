//
//  AddButton.m
//  Startwish
//
//  Created by Pavel Makukha on 27/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import "AddButton.h"

@implementation AddButton

#define BUTTON_HEIGHT 20.0

- (void)awakeFromNib
{
    [self setTitle:@"" forState:UIControlStateNormal];
    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
    // Устанавливаем ширину кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setTintColor:[UIColor whiteColor]];
    [self setImageColor:@"white"];
}

- (void)setImageColor:(NSString *)color
{
    [self setBackgroundImage:[[UIImage imageNamed: [NSString stringWithFormat: @"plus_%@", color]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

@end
