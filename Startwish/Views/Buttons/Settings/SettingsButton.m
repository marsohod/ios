//
//  SettingsButton.m
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "SettingsButton.h"


@implementation SettingsButton

- (void)awakeFromNib
{
    [self setTitle:@"" forState:UIControlStateNormal];
    [self setTintColor: [UIColor whiteColor]];
    [self setImage: [UIImage imageNamed:@"settings_white.png"] forState:UIControlStateNormal];
    [self setUserInteractionEnabled: YES];
    [self setImageEdgeInsets: UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)];
}

@end
