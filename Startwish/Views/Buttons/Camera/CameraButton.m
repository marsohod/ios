//
//  CameraButton.m
//  Startwish
//
//  Created by Pavel Makukha on 01/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CameraButton.h"
#import "UIColor+Helper.h"


@implementation CameraButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self draw];
}

- (void)draw
{
    [self setTitle:@"" forState:UIControlStateNormal];
    [self setTintColor: [UIColor whiteColor]];
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.clipsToBounds = YES;
    [self setImage: [[UIImage imageNamed:@"camera_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    [self setBackgroundColor: [UIColor h_orange2]];
}

@end
