//
//  DeleteButton.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "DeleteButton.h"
#import "UIColor+Helper.h"
#import "ActionTexts.h"


@implementation DeleteButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor: [UIColor h_red]];
    [self setTitle:[[ActionTexts sharedInstance] titleText:@"Delete"] forState:UIControlStateNormal];
    [self addIcon: [[UIImage imageNamed:@"trash_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(8.0, 0.0, 10.0, 22.0)];
}


@end
