    //
//  ButtonBeast.m
//  Startwish
//
//  Created by marsohod on 27/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ButtonBeast.h"
#import "Routing.h"


@interface ButtonBeast()

@end

@implementation ButtonBeast

#define DIAMETR 70.0
#define BOTTOM_MARGIN 16.0
#define RIGHT_MARGIN 16.0

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        [self render];
    }
    
    return self;
}

- (instancetype)initWithPoint:(CGPoint)point
{
    self = [self init];
    
    if ( self ) {
        self.frame = CGRectMake(point.x - DIAMETR - RIGHT_MARGIN, point.y - DIAMETR - BOTTOM_MARGIN, DIAMETR, DIAMETR);
    }
    
    return self;
}

- (void)awakeFromNib
{
    [self render];
}

- (void)render
{
    self.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - DIAMETR - RIGHT_MARGIN, [UIScreen mainScreen].bounds.size.height - DIAMETR - BOTTOM_MARGIN, DIAMETR, DIAMETR);
    [self setBackgroundImage:[UIImage imageNamed:@"btn_rule.png"] forState:UIControlStateNormal];
    [self setBackgroundImage:[UIImage imageNamed:@"btn_rule_tap.png"] forState:UIControlStateHighlighted];
    [self setTitle:@"" forState:UIControlStateNormal];
    [self addTarget:self action:@selector(actionButtonBeast:) forControlEvents: UIControlEventTouchUpInside];
}

- (IBAction)actionButtonBeast:(id)sender
{
//    [[Routing sharedInstance] showCameraFiltersController:nil];
    [[Routing sharedInstance] showCameraController];
}

- (void)hide
{
    if( self.layer.opacity != 0.0 ) {
        [UIView animateWithDuration:0.4 animations:^{
            self.layer.opacity = 0.0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
        }];
    }
}

- (void)show
{
    [UIView animateWithDuration:0.2 animations:^{
        self.hidden = NO;
        self.layer.opacity = 1.0;
    } completion:^(BOOL finished) {}];
}

@end
