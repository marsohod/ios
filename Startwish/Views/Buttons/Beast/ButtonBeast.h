//
//  ButtonBeast.h
//  Startwish
//
//  Created by marsohod on 27/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ButtonBeast : UIButton

- (instancetype)initWithPoint:(CGPoint)point;
- (void)show;
- (void)hide;

@end
