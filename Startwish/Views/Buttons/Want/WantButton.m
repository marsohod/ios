//
//  WantButton.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "WantButton.h"
#import "UIColor+Helper.h"
#import "ActionTexts.h"


@implementation WantButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor: [UIColor h_orange2]];
    [self setTitle:[[ActionTexts sharedInstance] titleText:@"Want"] forState:UIControlStateNormal];
    [self addIcon: [[UIImage imageNamed:@"want_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(10.0, 0.0, 10.0, 12.0)];
}

@end
