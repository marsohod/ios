//
//  MenuButton.m
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "MenuButton.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"

#define INSET 10.0
#define ANIMATION_SPEED 0.5

@implementation MenuButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundImage:[[UIImage imageNamed:@"menu_in_white_circle.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    
    _countEvents = [[UILabel alloc] initWithFrame:self.frame];
    _countEvents.textColor = [UIColor whiteColor];
    _countEvents.textAlignment = NSTextAlignmentCenter;
    _countEvents.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _countEvents.font = [UIFont h_defaultFontSize: 16.0];
    _countEvents.layer.masksToBounds = YES;
    _countEvents.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    _countEvents.frame = CGRectZero;
    _countEvents.text = @"";
    [_countEvents setBackgroundColor:[UIColor h_yellow]];
    [self addSubview:_countEvents];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEventCount:) name:@"STNewEventsCountUpdate" object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)showEventCountAnimation
{
    _countEvents.transform = CGAffineTransformMakeScale(0,0);
    
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        _countEvents.layer.cornerRadius = self.frame.size.width / 2;
        _countEvents.transform = CGAffineTransformMakeScale(1.2,1.2);
        // если вдруг скрыт кругляш
        self.layer.opacity = 1.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.3 animations:^{
            _countEvents.transform = CGAffineTransformMakeScale(1.0,1.0);
        } completion:^(BOOL finished) {
            _countEvents.transform = CGAffineTransformMakeScale(1.0,1.0);
        }];
    }];
}

- (void)hideEventCountAnimation
{
    [UIView animateWithDuration:0.3 animations:^{
        _countEvents.layer.cornerRadius = self.frame.size.width / 2;
        _countEvents.transform = CGAffineTransformMakeScale(1.2,1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            _countEvents.transform = CGAffineTransformMakeScale(0.01,0.01);
        } completion:^(BOOL finished) {
            _countEvents.frame = CGRectZero;
            _countEvents.transform = CGAffineTransformMakeScale(1.0,1.0);
        }];
    }];
}

- (void)showEventCount
{
    [self showEventCount:[UIApplication sharedApplication].applicationIconBadgeNumber];
}

- (void)showEventCount:(NSInteger)count
{
    [self showEventCount:count animate:YES];
}

- (void)showEventCount:(NSInteger)count animate:(BOOL)animate
{
    if ( count != 0 ) {
        [_countEvents setText:[NSString stringWithFormat:@"%lu", (long)count]];
        _countEvents.frame = CGRectMake(0.0, 0.0, self.frame.size.width, self.frame.size.height);
        
        if ( animate ) {
            [self showEventCountAnimation];
        }
    } else {
        if ( animate ) {
            [self hideEventCountAnimation];
        } else {
            _countEvents.frame = CGRectZero;
        }
        
//        [self setButtonBackground];
    }
}

- (void)updateEventCount:(NSNotification *)notif
{
    if ( notif && [notif userInfo]) {
        [self showEventCount:[[notif userInfo][@"count"] integerValue]];
        return;
    }
    
    [self showEventCount];
}

@end
