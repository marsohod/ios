//
//  MenuButton.h
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "CircleButton.h"

@interface MenuButton : CircleButton
@property (strong, nonatomic) UILabel *countEvents;
- (void)showEventCount;
- (void)showEventCount:(NSInteger)count;
- (void)showEventCount:(NSInteger)count animate:(BOOL)animate;
- (void)updateEventCount:(NSNotification *)notif;
@end
