//
//  MenuFillButton.h
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "FillGreenButton.h"


@interface MenuFillButton : FillGreenButton

@property (strong, nonatomic) UILabel *countEvents;

- (void)showEventCount;
- (void)showEventCount:(NSInteger)count;
- (void)showEventCount:(NSInteger)count animate:(BOOL)animate;
- (void)updateEventCount:(NSNotification *)notif;

@end
