//
//  CommentButton.m
//  Startwish
//
//  Created by marsohod on 10/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CommentButton.h"
#import "UIColor+Helper.h"
#import "ActionTexts.h"


@implementation CommentButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor: [UIColor h_smoothGreen]];
    [self setTitle:[[ActionTexts sharedInstance] titleText:@"Send"] forState:UIControlStateNormal];
}

@end
