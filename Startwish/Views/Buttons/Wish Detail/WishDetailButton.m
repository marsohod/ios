//
//  WishDetailButton.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "WishDetailButton.h"

@implementation WishDetailButton

- (void)awakeFromNib
{
    [self.titleLabel setFont: [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0]];
    [self setTintColor: [UIColor whiteColor]];
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
}

- (void)addIcon:(UIImage *)icon
{
    [self setImage: icon forState:UIControlStateNormal];
    [self setImage: icon forState:UIControlStateHighlighted];
}

@end
