//
//  WishDetailButton.h
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface WishDetailButton : UIButton

- (void)addIcon:(UIImage *)icon;

@end
