//
//  WantAlreadyButton.m
//  Startwish
//
//  Created by Pavel Makukha on 06/05/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "WantAlreadyButton.h"
#import "UIColor+Helper.h"


@implementation WantAlreadyButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundColor: [UIColor h_bg_collectionView]];
    [self setTitle:@"Уже Хочу" forState:UIControlStateNormal];
    [self addIcon: nil];
    self.userInteractionEnabled = NO;
}

@end
