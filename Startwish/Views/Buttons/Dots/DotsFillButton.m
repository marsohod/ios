//
//  DotsFillButton.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "DotsFillButton.h"


@implementation DotsFillButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addIcon: [[UIImage imageNamed:@"dots_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(11.0, 0.0, 10.0, 0.0)];
}


@end
