//
//  LockButton.m
//  Startwish
//
//  Created by marsohod on 12/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LockButton.h"
#import "UIColor+Helper.h"


@implementation LockButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addIcon: [UIImage imageNamed:@"lock_closed_white"]];
    [self setBackgroundColor: [UIColor h_bg_profile_circle]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(-1.0, 1.0, 0.0, 0.0)];
}

@end
