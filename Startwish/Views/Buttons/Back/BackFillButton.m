//
//  BackFillButton.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "BackFillButton.h"


@implementation BackFillButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addIcon: [[UIImage imageNamed:@"arrow_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [self setImageEdgeInsets: UIEdgeInsetsMake(11.0, 11.0, 10.0, 10.0)];
}


@end
