//
//  BackButton.m
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "BackButton.h"

@implementation BackButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundImage:[[UIImage imageNamed:@"back.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
