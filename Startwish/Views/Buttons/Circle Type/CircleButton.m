//
//  CircleButton.m
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "CircleButton.h"

@implementation CircleButton

#define BUTTON_HEIGHT 44.0

- (void)awakeFromNib
{
    [self setTitle:@"" forState:UIControlStateNormal];
    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
    // Устанавливаем ширину кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];

}


@end
