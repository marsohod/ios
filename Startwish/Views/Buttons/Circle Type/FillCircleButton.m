//
//  FillCircleButton.m
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "FillCircleButton.h"

@implementation FillCircleButton

#define BUTTON_HEIGHT 40.0
- (void)awakeFromNib
{
    [self setTitle:@"" forState:UIControlStateNormal];
    [self setTintColor: [UIColor whiteColor]];
    self.layer.cornerRadius = BUTTON_HEIGHT / 2;
    self.clipsToBounds = YES;
    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
    // Устанавливаем ширину кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0
                                                                constant:BUTTON_HEIGHT]];
}

- (void)addIcon:(UIImage *)icon
{
    [self setImage: icon forState:UIControlStateNormal];
    [self setImage: icon forState:UIControlStateHighlighted];
}

@end
