//
//  FillCircleButton.h
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface FillCircleButton : UIButton

- (void)addIcon:(UIImage *)icon;

@end
