//
//  CameraFiltersCollectionView.m
//  Startwish
//
//  Created by Pavel Makukha on 25/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CameraFiltersCollectionView.h"
#import "FilterCell.h"
#import "AmaroFilter.h"
#import "ClearFilter.h"
#import "SierraFilter.h"
#import "InkwellFilter.h"
#import "EarlybirdFilter.h"
#import "HudsonFilter.h"
#import "MayfairFilter.h"
#import "ValenciaFilter.h"
#import "LoFiFilter.h"
#import "RiseFilter.h"
#import "XProllFilter.h"


#define FilterCellName @"filterCell"

@interface CameraFiltersCollectionView ()

@property (nonatomic) NSArray *items;

@end


@implementation CameraFiltersCollectionView

#pragma mark - init

- (CameraFiltersCollectionView *)initWithCoder:(NSCoder *)aDecoder
{
    if ( self = [super initWithCoder:aDecoder] ) {
        [self registerNib:[UINib nibWithNibName:@"FilterCell" bundle:nil] forCellWithReuseIdentifier:FilterCellName];
        self.delegate = self;
        self.dataSource = self;
    }
    
    return self;
}

- (NSArray *)items
{
    if ( _items == nil ) {
        _items = [[NSArray alloc] initWithObjects:
                        [[PhotoEditToolBase alloc] initBrightnessTool],
                        [[PhotoEditToolBase alloc] initCropTool],
                        [[PhotoEditToolBase alloc] initContrastTool],
                        [[ClearFilter alloc] init],
                        [[MayfairFilter alloc] init],
                        [[AmaroFilter alloc] init],
                        [[XProllFilter alloc] init],
                        [[ValenciaFilter alloc] init],
                        [[RiseFilter alloc] init],
                        [[LoFiFilter alloc] init],
                        [[InkwellFilter alloc] init],
                        [[SierraFilter alloc] init],
                        [[HudsonFilter alloc] init],
                        [[EarlybirdFilter alloc] init],nil];
    }
    
    return _items;
}


#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:FilterCellName forIndexPath:indexPath];
}


#pragma mark -
#pragma mark UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(FilterCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [_items count] ) {
        [cell setCell: _items[indexPath.row]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [_items count] ) {
        [self.filtersDelegate selectTool:_items[indexPath.row]];
    }
}


#pragma mark -
#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(70.0, 70.0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake( 0.0, 15.0, 0.0, 18.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 35.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 18.0;
}

- (void)dealloc
{
    self.items = nil;
    self.delegate = nil;
    self.dataSource = nil;
    
    [[GPUImageContext sharedFramebufferCache] purgeAllUnassignedFramebuffers];
}

@end

