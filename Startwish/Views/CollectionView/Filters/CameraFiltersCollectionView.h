//
//  CameraFiltersCollectionView.h
//  Startwish
//
//  Created by Pavel Makukha on 25/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PhotoEditToolBase.h"
#import "PhotoEditToolFilter.h"


@protocol CameraFiltersCollectionViewDelegate;
@interface CameraFiltersCollectionView : UICollectionView <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) id<CameraFiltersCollectionViewDelegate>filtersDelegate;

@end

@protocol CameraFiltersCollectionViewDelegate <NSObject>

- (void)selectTool:(PhotoEditTool *)tool;

@end
