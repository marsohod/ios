//
//  UICollectionViewWish.h
//  Startwish
//
//  Created by marsohod on 13/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UICollectionViewWaterFall.h"
#import "UICollectionViewCellWish.h"
#import "GHContextMenuView.h"
#import "ModelWish.h"


@protocol UICollectionViewWishDelegate;

@interface UICollectionViewWish : UICollectionViewWaterFall <GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>

@property (nonatomic, weak) id<UICollectionViewWishDelegate> tapDelegate;
@property (nonatomic, strong) NSArray *longTapAllItems;
@property (nonatomic, strong) NSMutableArray *longTapItems;
@property (nonatomic) CGRect smallOpenWish;

- (CGRect)prepareToCloseWish;
- (CGRect)prepareToOpenWish;
- (UICollectionViewCellWish*)selectedWishCell;
- (void)deselectSelectedItem;
- (NSString *)lastItemDate;
- (NSString *)firstItemDate;

- (void)updateProfileInfoOwnWishes;
- (NSInteger)itemsCount;
@end


@protocol UICollectionViewWishDelegate <NSObject>
@required

- (void)showUserDetail:(id)user;
- (void)showWishDetail:(id)wish;
- (void)showCommentDetail:(id)wish;

- (void)implementWish:(ModelWish *)wish;
- (void)rewish:(ModelWish *)wish;
- (void)deleteWish:(ModelWish *)wish;

- (void)loadItems;

@end
