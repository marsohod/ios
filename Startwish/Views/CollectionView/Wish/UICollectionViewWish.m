//
//  UICollectionViewWish.m
//  Startwish
//
//  Created by marsohod on 13/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UICollectionViewWish.h"
#import "UIScreen+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "ActionTexts.h"
#import "Me.h"
#import "ModelUser.h"


@interface UICollectionViewWish ()
{
    BOOL isLongTapToWish;
    GHContextMenuView* overlay;
}
@end

@implementation UICollectionViewWish

//static NSString * const reuseIdentifier = @"CellPin";

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    [self registerNib:[UINib nibWithNibName:@"WishCell" bundle:nil] forCellWithReuseIdentifier:@"CellPin"];
    
    _longTapAllItems = [[NSArray alloc] initWithObjects:@{@"image"          : @"done_white",
                                                      @"color"          : [UIColor h_orange2],
                                                      @"title"          : [[ActionTexts sharedInstance] titleText:@"Execute"],
                                                      @"index"          : [NSNumber numberWithInteger:0]},
                        
                                                    @{@"image"          : @"want_white",
                                                      @"color"          : [UIColor h_orange2],
                                                      @"title"          : [[ActionTexts sharedInstance] titleText:@"WantImportant"],
                                                      @"index"          : [NSNumber numberWithInteger:1]},
                    
                                                    @{@"image"          : @"comment_white",
                                                      @"color"          : [UIColor h_smoothGreen],
                                                      @"title"          : [[ActionTexts sharedInstance] titleText:@"Comment"],
                                                      @"index"          : [NSNumber numberWithInteger:2]},
                                                    
                                                    @{@"image"          : @"trash_white",
                                                      @"color"          : [UIColor h_red],
                                                      @"title"          : [[ActionTexts sharedInstance] titleText:@"Delete"],
                                                      @"index"          : [NSNumber numberWithInteger:3]},
                                                    nil];
    
    self.longTapItems = [[NSMutableArray alloc] initWithArray:_longTapAllItems];
    
    [self addContextMenu];
    
    return self;
}

#define TagWhiteView 9999

- (CGRect)prepareToOpenWish
{
    UICollectionViewCellWish *cell = [self selectedWishCell];
    _smallOpenWish = cell.frame;
    float dif = cell.frame.size.height - cell.imageCover.frame.size.height;
    
    CGRect smallPin = [cell.imageCover convertRect:cell.imageCover.bounds toView:self.superview.superview];
    
    UIView *view = [[UIView alloc] initWithFrame:self.superview.frame];
    view.tag = TagWhiteView;
    view.backgroundColor = [UIColor whiteColor];
    view.alpha = 0.6;
    [self addSubview:view];
//    [cell setCoverHeight:[UIScreen mainScreen].bounds.size.width * smallPin.size.height /smallPin.size.width];
    
    [self bringSubviewToFront:cell];
    
    return CGRectMake(dif, self.contentOffset.y, [UIScreen mainScreen].bounds.size.width, dif + [UIScreen mainScreen].bounds.size.width * smallPin.size.height / (smallPin.size.width == 0 ? 1 : smallPin.size.width));
}

- (CGRect)prepareToCloseWish
{
//    UICollectionViewCellWish *cell = [self selectedWishCell];
    
//    [self sendSubviewToBack:cell];
    [[self viewWithTag:TagWhiteView] removeFromSuperview];
    
    return CGRectMake(_smallOpenWish.origin.x, _smallOpenWish.origin.y, _smallOpenWish.size.width, _smallOpenWish.size.height);
}

- (UICollectionViewCellWish*)selectedWishCell
{
    NSArray *indexPaths = [self indexPathsForSelectedItems];
    
    if ( [indexPaths count] > 0 ) {
        return (UICollectionViewCellWish *)[self cellForItemAtIndexPath:indexPaths[0]];
    }
    
    return [[UICollectionViewCellWish alloc] init];
}

- (void)deselectSelectedItem
{
    NSArray *indexPaths = [self indexPathsForSelectedItems];
    
    if ( [indexPaths count] > 0 ) {
        [self deselectItemAtIndexPath:indexPaths[0] animated:NO];
    }
}

- (NSString *)lastItemDate
{    
    return [items lastObject] ? [[items lastObject] getUpdatedDate] : [NSString h_dateNow];
}

- (NSString *)firstItemDate
{
    return [[items firstObject] getUpdatedDate];
}

- (void)updateProfileInfoOwnWishes
{
    [items enumerateObjectsUsingBlock:^(ModelWish *obj, NSUInteger idx, BOOL *stop) {
        if ( [obj isMyWish] ) {
            [[obj getOwner] setAvatar:[Me myAvatarUrl]];
            [[obj getOwner] setFullname:[Me myProfileName]];
        }
    }];
    
    [self reloadData];
}

- (NSInteger)itemsCount
{
    return [items count];
}


#pragma mark -
#pragma mark <UICollectionViewDataSource>

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCellWish *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
    
    return cell;
}

#pragma mark - <UIScrollViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCellWish *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath && indexPath.row < [items count] ) {
        cell.delegate = (id<UICollectionViewCellWishDelegate>)self;
        [cell setWithItem:[items objectAtIndex:indexPath.row]];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    if( self.contentSize.height - (self.frame.size.height * 2) < self.contentOffset.y ) {
        [self.tapDelegate loadItems];
    }
}


#pragma mark -
#pragma mark UITapGestureRecognizer

- (void)userDetail:(UIView *)view
{
    ModelWish *wish = [self itemFromTapCellSubview:view];
    
    if( wish ) {
        [self.tapDelegate showUserDetail:[wish getOwner]];
    }
}

- (void)wishDetail:(UIView *)view
{
    ModelWish *wish = [self itemFromTapCellSubview:view];
    
    if( wish ) {
        [self.tapDelegate showWishDetail:wish];
    }
}

- (void)reDrawCellWithHeight:(float)height
{
    [self setInvalidateLayout];
}


#pragma mark -
#pragma mark <UICollectionViewDelegateFlowLayout>

#define AVATAR_HEIGHT_WITH_PADDING 45.0

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake([UIScreen h_getPinWidthDependsDevice], [UICollectionViewCellWish cellHeightForItem:items[indexPath.row]]);
}



- (void)addContextMenu
{
    overlay = [[GHContextMenuView alloc] init];
    overlay.dataSource = self;
    overlay.delegate = self;
    
    // Do any additional setup after loading the view.
    //    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressDetected:)];
    [self addGestureRecognizer:_longPressRecognizer];
}



#pragma mark - GHMenu methods

- (NSInteger) numberOfMenuItems
{
    return [_longTapItems count];
}

- (UIImage*)imageForItemAtIndex:(NSInteger)index
{
    if( [_longTapItems count] > index ) {
        return [UIImage imageNamed:[_longTapItems objectAtIndex:index][@"image"]];
    }
    
    return [[UIImage alloc] init];
}

- (UIColor*) colorForItemAtIndex:(NSInteger) index
{
    
    if( [_longTapItems count] > index ) {
        return [_longTapItems objectAtIndex:index][@"color"];
    }
    
    return [UIColor grayColor];
}

- (NSString *) titleForItemAtIndex:(NSInteger) index
{
    if( [_longTapItems count] > index ) {
        return [_longTapItems objectAtIndex:index][@"title"];
    }

    
    return @"";
}

- (void) didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point
{
    NSIndexPath* indexPath = [self indexPathForItemAtPoint:point];

    if ( indexPath == nil ) {
        return;
    }
    
    if( [_longTapItems count] > selectedIndex ) {
        switch ( [[_longTapItems objectAtIndex:selectedIndex][@"index"] integerValue] ) {
            case 0:
                [self.tapDelegate implementWish:[items objectAtIndex:indexPath.row]];
                break;
            
            case 1:
                [self.tapDelegate rewish:[items objectAtIndex:indexPath.row]];
                break;
                
            case 2:
                [self.tapDelegate showCommentDetail:[items objectAtIndex:indexPath.row]];
                break;
                
            case 3:
                [self.tapDelegate deleteWish:[items objectAtIndex:indexPath.row]];
                break;
            default:
                break;
        }
    }
}

- (void) longPressDetected:(UIGestureRecognizer*) gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        isLongTapToWish = [self canShowContextMenu:[gestureRecognizer locationInView:self]];
    }

    if ( isLongTapToWish ) {
        [overlay longPressDetected:gestureRecognizer];
    }
}

- (BOOL)canShowContextMenu:(CGPoint)touchPoint
{
    return [self indexPathForItemAtPoint:touchPoint] != nil;
}

- (NSArray *)GHMenuItemsAtPoint:(CGPoint)touchPoint
{
    NSIndexPath *indexPath = [self indexPathForItemAtPoint:touchPoint];
    
    if ( indexPath == nil ) {
        [_longTapItems removeAllObjects];
    }
    
    if( indexPath.row < [items count] ) {
        [_longTapItems removeAllObjects];
        
        if ( [[items objectAtIndex:indexPath.row] isMyWish] ) {
            if( ![[items objectAtIndex:indexPath.row] isImplement] ) {
                [_longTapItems addObject:_longTapAllItems[0]];
            }
            
            [_longTapItems addObject:_longTapAllItems[2]];
            [_longTapItems addObject:_longTapAllItems[3]];
        } else {
            if ( ![[items objectAtIndex:indexPath.row] isWishInWishlist] ) {
                [_longTapItems addObject:_longTapAllItems[1]];
            }
            
            [_longTapItems addObject:_longTapAllItems[2]];
        }
    }
    
    return _longTapItems;
}

@end
