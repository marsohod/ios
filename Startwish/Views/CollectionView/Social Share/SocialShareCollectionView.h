//
//  SocialShareCollectionView.h
//  Startwish
//
//  Created by Pavel Makukha on 01/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol SocialShareCollectionViewDelegate;
@interface SocialShareCollectionView : UICollectionView <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) NSArray *items;
@property (weak, nonatomic) id<SocialShareCollectionViewDelegate>socialDelegate;

- (NSArray *)selectedSocial;
- (void)socialShareWishId:(uint32_t)wishId text:(NSString *)text desc:(NSString *)desc image:(UIImage *)image;
- (void)socialShareWishId:(uint32_t)wishId text:(NSString *)text desc:(NSString *)desc imageLink:(NSString *)imageLink;

@end

@protocol SocialShareCollectionViewDelegate <NSObject>

@end
