//
//  SocialShareCollectionView.m
//  Startwish
//
//  Created by Pavel Makukha on 01/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SocialShareCollectionView.h"
#import "SocialShareViewCell.h"
#import "UIColor+Helper.h"
#import "VKHandler.h"
#import "InstagramHandler.h"
#import "TwitterHandler.h"
#import "UIScreen+Helper.h"
#import "ActionTexts.h"
#import "UIImage+Helper.h"
#import "UserAPI.h"


#define SocialCell @"socialCell"
@implementation SocialShareCollectionView

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.delegate = self;
    self.dataSource = self;
    
    [self registerNib: [UINib nibWithNibName:@"SocialShareCell" bundle:nil] forCellWithReuseIdentifier: SocialCell];
    [self setBackgroundColor: [UIColor clearColor]];
    
    _items = [[NSArray alloc] initWithObjects:@{@"name": @"vkontakte", @"idx" : @"0"},
//                   @{@"name": @"facebook", @"idx" : @"1"},
                   @{@"name": @"twitter", @"idx" : @"2"},
                   @{@"name": @"instagram", @"idx" : @"3"}, nil];
}

#pragma mark -
#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SocialShareViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SocialCell forIndexPath:indexPath];
    
    return cell;
}

#pragma mark -
#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(SocialShareViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [_items count] ) {
        [cell setSocialShareCell:_items[indexPath.row][@"name"]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SocialShareViewCell *cell = (SocialShareViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ( indexPath.row >= [_items count] ) {
        return;
    }
    
    if( cell.customSelect == NO ) {
        switch ( indexPath.row ) {
            case 0:
                if ( ![[VKHandler sharedInstance] hasPermission] ) {
                    [[VKHandler sharedInstance] auth];
                } else {
                    [cell selectCell:_items[indexPath.row][@"name"]];
                }
                break;
            case 1:{
//                if ( [FacebookHandler isLogin] ) {
//                    [cell selectCell:_items[indexPath.row][@"name"]];
//                } else {
//                    [FacebookHandler loginWithSuccess:^{
//                        [cell selectCell:_items[indexPath.row][@"name"]];
//                    } failed:^{
//                        [cell deselectCell:_items[indexPath.row][@"name"]];
//                    }];
//                }
//                break;
//            }
//            case 2:{
                if ( ![TwitterHandler isLogin] ) {
                    [cell selectCell:_items[indexPath.row][@"name"]];
                    
                    [TwitterHandler login:^{

                    } canceled:^(NSString *error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"TwitterAccess"]}];

                        [TwitterHandler logout];
                        [cell deselectCell:_items[indexPath.row][@"name"]];
                    } ctrl:(UIViewController *)self.socialDelegate];
                } else {
                    [cell selectCell:_items[indexPath.row][@"name"]];
                }
                break;
            }
            case 2:
                if ( ![InstagramHandler canOpenApp] ) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"InstagramAccess"]}];
                    [cell deselectCell:_items[indexPath.row][@"name"]];
                } else {
                    [cell selectCell:_items[indexPath.row][@"name"]];
                }
                break;
            default:
                [cell selectCell:_items[indexPath.row][@"name"]];
                break;
        }
        
        
    } else {
        [cell deselectCell:_items[indexPath.row][@"name"]];
    }
    
}


#pragma mark -
#pragma mark UICollectionViewDelegateFlowLayout

const CGFloat cellWidth = 50.f;
const CGFloat cvPaddingLeft = 20.f;

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellWidth, cellWidth);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake( 0.0, cvPaddingLeft, 0.0, 0.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return (self.frame.size.width - (cvPaddingLeft * 2) - (cellWidth * [self.items count])) / ( [self.items count] - 1 );
}


#pragma mark -
#pragma mark VKHandlerDelegate
- (void)allowAccess
{
    SocialShareViewCell *cell = (SocialShareViewCell *)[self cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    [cell selectCell:_items[0][@"name"]];
    
    // save social network to profile
    [[UserAPI sharedInstance] setVKToken:[VKHandler accessToken] success:^(id item) {} failedBlock:^(id item) {}];
}

- (void)denyAccess
{
    SocialShareViewCell *cell = (SocialShareViewCell *)[self cellForItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    [cell deselectCell:_items[0][@"name"]];
}


#pragma mark -
- (NSArray *)selectedSocial
{
    __block NSMutableArray *ss = [NSMutableArray array];
    
    [_items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        SocialShareViewCell *cell = (SocialShareViewCell *)[self cellForItemAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];

        if ( cell.customSelect ) {
            [ss addObject:[NSNumber numberWithInteger:[obj[@"idx"] integerValue]]];
        }
    }];
    
    return [NSArray arrayWithArray:ss];
}


#pragma mark -
#pragma mark Share
- (void)socialShareWishId:(uint32_t)wishId text:(NSString *)text desc:(NSString *)desc imageLink:(NSString *)imageLink
{
    [UIImage downloadWithUrl:[NSURL URLWithString:imageLink] onCompleteChange:^(UIImage *image) {
        [self socialShareWishId:wishId text:text desc:desc image:image];
    }];
}

- (void)socialShareWishId:(uint32_t)wishId text:(NSString *)text desc:(NSString *)desc image:(UIImage *)image
{
    
}

- (BOOL)isLastSocialPost:(NSUInteger)length successLength:(NSUInteger)successLenght
{
    return YES;
}

- (void)trySendInstagramPost
{

}


@end
