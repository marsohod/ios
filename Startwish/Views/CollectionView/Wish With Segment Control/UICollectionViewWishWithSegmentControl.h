//
//  UICollectionViewWishWithSegmentControl.h
//  Startwish
//
//  Created by marsohod on 28/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UICollectionViewWish.h"
#import "UICollectionViewHeaderWishWithSegmentControl.h"


@interface UICollectionViewWishWithSegmentControl : UICollectionViewWish
{
    UICollectionViewHeaderWishWithSegmentControl *header;
}
@end
