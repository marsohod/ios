//
//  UICollectionViewHeaderWishWithSegmentControl.h
//  Startwish
//
//  Created by marsohod on 28/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol UICollectionViewHeaderDelegate;

@interface UICollectionViewHeaderWishWithSegmentControl : UICollectionReusableView

@property (nonatomic, weak) id<UICollectionViewHeaderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;

- (IBAction)actionValueChangeSegmentControl:(id)sender;

@end

@protocol UICollectionViewHeaderDelegate <NSObject>
@required

- (void)getWishesWithPopularStatus:(BOOL)status;

@end