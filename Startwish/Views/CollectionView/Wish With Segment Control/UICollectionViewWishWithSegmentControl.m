//
//  UICollectionViewWishWithSegmentControl.m
//  Startwish
//
//  Created by marsohod on 28/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UICollectionViewWishWithSegmentControl.h"


@implementation UICollectionViewWishWithSegmentControl

#define HEADER_HEIGHT 40.0

- (UICollectionViewWishWithSegmentControl *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if ( self ) {
        [self registerNib:[UINib nibWithNibName:@"WishSegmentControl" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"wishesSegment"];
    }
    
    return self;
}


// Устанавливаем высоту хэдера
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}


// Достаём и инициируем хэдер
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewHeaderWishWithSegmentControl *reusableView = nil;
    
    if ([kind isEqualToString:CHTCollectionElementKindSectionHeader]) {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                          withReuseIdentifier:@"wishesSegment"
                                                                 forIndexPath:indexPath];
        header = reusableView;
        header.delegate = (id<UICollectionViewHeaderDelegate>)self.tapDelegate;
        
    } else if ([kind isEqualToString:CHTCollectionElementKindSectionFooter]) {
        
    }
    
    return reusableView;
}

@end
