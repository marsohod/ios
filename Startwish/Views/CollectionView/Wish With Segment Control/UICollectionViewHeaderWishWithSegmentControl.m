//
//  UICollectionViewHeaderWishWithSegmentControl.m
//  Startwish
//
//  Created by marsohod on 28/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UICollectionViewHeaderWishWithSegmentControl.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@implementation UICollectionViewHeaderWishWithSegmentControl

- (void)awakeFromNib
{
    UIImage *imageSelected = [UIImage imageNamed:@"segment_bg_red.png"];
    UIImage *imageDefault = [UIImage imageNamed:@"segment_bg_gray.png"];
    
    // Настраиваем неактивный сегмент
    [_segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont systemFontOfSize:13.0]} forState:UIControlStateNormal];

    // Настраиваем активный сегмент
    [_segmentControl setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:13.0]} forState:UIControlStateSelected];
    
    // Ставим фон у неактивного сегмента
    [_segmentControl setBackgroundImage: [imageDefault scaleToSize:CGSizeMake(_segmentControl.frame.size.width / 2, _segmentControl.frame.size.height)]
                                                   forState:UIControlStateNormal
                                                  barMetrics:UIBarMetricsDefault];

    // Ставим фон у активного сегмента
    [_segmentControl setBackgroundImage: [imageSelected scaleToSize:CGSizeMake(_segmentControl.frame.size.width / 2, _segmentControl.frame.size.height)]
                               forState:UIControlStateSelected
                             barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между активным и неактивным сегментом
    [_segmentControl setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, _segmentControl.frame.size.height)] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между неактивным и активным сегментом
    [_segmentControl setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, _segmentControl.frame.size.height)] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    
    _segmentControl.layer.cornerRadius = 5.0;
    _segmentControl.clipsToBounds = YES;
}

- (IBAction)actionValueChangeSegmentControl:(id)sender {
//    NSLog(@"adsf %lu", [_segmentControl selectedSegmentIndex]);
    [self.delegate getWishesWithPopularStatus:(BOOL)[_segmentControl selectedSegmentIndex]];
}

@end