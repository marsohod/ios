//
//  UICollectionViewWaterFall.h
//  Startwish
//
//  Created by marsohod on 28/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CHTCollectionViewWaterfallLayout.h"
#import "SSPullToRefreshView.h"


@interface UICollectionViewWaterFall : UICollectionView <UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout>
{
    NSMutableArray *items;
    int page;
    NSString *cellIdentifer;
}

@property (nonatomic) SSPullToRefreshView *pullToRefreshView;
@property (nonatomic) SSPullToRefreshView *pullToRefreshViewBottom;
@property (nonatomic) CHTCollectionViewWaterfallLayout *collectionViewLayout;
@property (nonatomic) CGFloat offsetTop;
@property (nonatomic) CGFloat offsetTopByMoveToTop;

- (void)updateWithItems:(NSArray *)newItems;
- (void)updateWithNewestItems:(NSArray *)newItems;
- (void)reloadItem:(id)item;
- (void)removeItem:(id)item;
- (int)currentPage;
- (void)clean;
- (id)itemFromTapCellSubview:(UIView *)view;
- (void)setInvalidateLayout;

- (void)hideBottomLoader;

@end
