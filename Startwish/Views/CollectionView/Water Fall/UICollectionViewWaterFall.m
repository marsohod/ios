//
//  UICollectionViewWaterFall.m
//  Startwish
//
//  Created by marsohod on 28/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <objc/runtime.h>

#import "UICollectionViewWaterFall.h"
#import "UIColor+Helper.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "SSPullToRefreshSimpleContentView.h"
#import "LoadingView.h"
#import "UIScreen+Helper.h"
#import "SDImageCache.h"


@implementation UICollectionViewWaterFall

- (UICollectionViewWaterFall *)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        [self setBackgroundColor:[UIColor h_bg_collectionView]];
        
        self.collectionViewLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
//        [self setShowsVerticalScrollIndicator:NO];
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        [self addBottomPullToRefresh];
        [self addTopPullToRefresh];
        
        self.dataSource = self;
        self.delegate = self;
        
        items = [NSMutableArray array];
        page = 0;
        cellIdentifer = @"CellPin";
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    
    return self;
}

- (void)addTopPullToRefresh
{
    // Устанавливаем pullToRefresh для верхнего пула
    self.pullToRefreshView = [[SSPullToRefreshView alloc] initWithScrollView:self delegate:nil];
    self.pullToRefreshView.contentView = [[LoadingView alloc] init];
    self.pullToRefreshView.expandedHeight = [LoadingView loadindViewHeight] + 10.0;
}

- (void)addBottomPullToRefresh
{
    self.bottomRefreshControl = [UIRefreshControl new];
}


- (int)currentPage
{
    return page;
}

- (void)clean
{
    page = 0;
    [items removeAllObjects];
}

- (void)updateWithItems:(NSArray *)newItems
{
    if( [newItems count] > 0 ) {
        page++;
    }
    
    [items addObjectsFromArray:newItems];
    
    [self.bottomRefreshControl endRefreshing];

    
//    NSMutableArray *indexsPaths = [[NSMutableArray alloc] init];
//    
//    for ( NSUInteger i = [items count] - [newItems count]; i < [items count]; i++ ) {
//        [indexsPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
//    }
//    
//    [UIView setAnimationsEnabled:NO];
//
//    [self performBatchUpdates:^{
//        if ( [items count] - [newItems count] != 0 ) {
//            [self insertItemsAtIndexPaths:indexsPaths];
//        } else {
//            [self reloadItemsAtIndexPaths:indexsPaths];
//        }
//    } completion:^(BOOL finished) {
//        [UIView setAnimationsEnabled:YES];
//    }];

    [self reloadData];
}

- (void)updateWithNewestItems:(NSArray *)newItems
{
    for ( NSInteger i = [newItems count] - 1; i >= 0; i-- ) {
        [items insertObject:newItems[i] atIndex:0];
    }
    
    [self reloadData];
}

- (void)reloadItem:(id)item
{
    NSArray *indexPaths = [self indexPathsForSelectedItems];
    
    if ( [items indexOfObject:item] <= [items count] ) {
        [self reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:[items indexOfObject:item] inSection:0]]];
        
         if ( [indexPaths count] > 0 ) {
             [self selectItemAtIndexPath:indexPaths[0] animated:NO scrollPosition:UICollectionViewScrollPositionNone];
         }
    }
}

- (void)removeItem:(id)item
{
//    NSLog(@"%u" , [items indexOfObject:item]);
    [self performBatchUpdates:^{
        NSUInteger i = [items indexOfObject:item];
        if ( i <= [items count] ) {
            [items removeObjectAtIndex:i];
            [self deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]]];
        }
    } completion:^(BOOL finished) {
       
    }];
    
}



#pragma mark -
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [items count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifer forIndexPath:indexPath];
}




#pragma mark -
#pragma mark <UICollectionViewDelegateFlowLayout>

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake( [UIScreen h_paddingPin], [UIScreen h_paddingPin], [UIScreen h_paddingPin], [UIScreen h_paddingPin]);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100.0, 100.0);
}





#pragma mark -
#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)setInvalidateLayout
{
    [self.collectionViewLayout invalidateLayout];
}



#pragma mark -
#pragma mark UITapGestureRecognizer

- (id)itemFromTapCellSubview:(UIView *)view
{
    CGRect point = [view convertRect:view.bounds toView:self];
    NSIndexPath *indexPath = [self indexPathForItemAtPoint:point.origin];
    
    if( indexPath.row > [items count] - 1 )
    {
        return nil;
    }
    
    [self selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    return items[indexPath.row];
}


#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( _offsetTop >= 0 ) {
        if ( _offsetTop < scrollView.contentOffset.y ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STHideOnScroll" object:nil];
            _offsetTopByMoveToTop = 0;
        } else {
            if ( _offsetTopByMoveToTop > 100 || _offsetTop == 0 ) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"STShowOnScroll" object:nil];
            } else {
                _offsetTopByMoveToTop += _offsetTop - scrollView.contentOffset.y;
            }
        }
    }
    
    [self refreshingShow:scrollView.contentOffset];
    
    _offsetTop = scrollView.contentOffset.y;
}

- (void)memoryWarning
{
    [[SDImageCache sharedImageCache] clearMemory];
}


#pragma mark -
- (void)hideBottomLoader
{
    [self.bottomRefreshControl endRefreshing];
}

- (void)dealloc
{

}

@end
