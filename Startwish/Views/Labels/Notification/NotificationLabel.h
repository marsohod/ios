//
//  NotificationLabel.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface NotificationLabel : UILabel
{
    BOOL isError;
    NSMutableArray *items;
}

- (void)showNotify:(NSUInteger)type withText:(NSString *)text;
- (void)showNotify:(NSUInteger)type withText:(NSString *)text withPush:(NSDictionary *)push;

@end
