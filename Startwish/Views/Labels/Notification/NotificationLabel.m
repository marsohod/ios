//
//  NotificationLabel.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "NotificationLabel.h"
#import "PushNotificationHandler.h"
#import "UIColor+Helper.h"


@interface NotificationLabel ()
{
    NSDictionary *pushNotif;
}
@end

@implementation NotificationLabel

#define NOTIFY_HEIGHT 47.0

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tryOpenPush:)];
    [tap setNumberOfTapsRequired:1];
    
    [self setFont:[UIFont systemFontOfSize:13.0]];
    self.textAlignment          = NSTextAlignmentCenter;
    self.numberOfLines          = 0;
    self.textColor              = [UIColor whiteColor];
    self.userInteractionEnabled = YES;
    
//    UITapGestureRecognizer *tap = UITapGestureRecognizer 
    
    // Устанавливаем высоту
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:NOTIFY_HEIGHT]];
    // Устанавливаем ширину
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeWidth
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeWidth
                                                              multiplier:1.0
                                                                constant:[UIScreen mainScreen].bounds.size.width]];
    // Устанавливаем отступ от верхнего края (скрываем view)
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeTop
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.superview
                                                               attribute:NSLayoutAttributeTop
                                                              multiplier:1.0
                                                                constant:-NOTIFY_HEIGHT]];
    
    items = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNotify:) name:@"STNotification" object:nil];
    [self addGestureRecognizer:tap];
}

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        
    }
    
    return self;
}

- (void)showNotify:(NSNotification *)notif
{
    if ( notif.userInfo != nil ) {
        __block BOOL isAlreadyExistNotif;
        
        if ( [items count] == 0 ) {
            [self showNotify:[notif.userInfo[@"type"] integerValue]  withText:notif.userInfo[@"text"] withPush:notif.userInfo[@"push"] ? notif.userInfo[@"push"] : @{}];
        }
        
        [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ( [obj[@"text"] isEqualToString:notif.userInfo[@"text"]] ) {
                isAlreadyExistNotif = YES;
                *stop = YES;
            }
        }];
        
        if ( !isAlreadyExistNotif ) {
            [items insertObject:@{@"type" : notif.userInfo[@"type"], @"text" : notif.userInfo[@"text"], @"push": notif.userInfo[@"push"] ? notif.userInfo[@"push"] : @{}} atIndex:0];
        }
        
        if ( notif.userInfo[@"push"] ) {
            
        }
    }
}

- (void)showNotify:(NSUInteger)type withText:(NSString *)text
{
    [self showNotify:type withText:text withPush:nil];
}

- (void)showNotify:(NSUInteger)type withText:(NSString *)text withPush:(NSDictionary *)push
{
    self.text = text;
    pushNotif = push;
    pushNotif = pushNotif[@"aps"] ? pushNotif : nil;
    
    [self setBackgroundColorWithStatus:type];
    
    // Спрятать на самый верх :)
    [self goToTheDarkness];
    [self showNotifyWithPush:pushNotif];
}


- (void)showNotifyWithPush:(NSDictionary *)push
{
//    dispatch_sync(dispatch_get_main_queue(), ^{
    if ( !push ) {
        [self.superview layoutIfNeeded];
        [UIView beginAnimations:@"notifyShow" context:nil];
        [UIView setAnimationDelay:0];
        [UIView setAnimationDuration:0.5];
        
        self.frame = CGRectMake(self.frame.origin.x, 0, self.frame.size.width, self.frame.size.height);
        [UIView commitAnimations];
        [self hideNotify];
    } else {
        [self.superview layoutIfNeeded];
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.frame = CGRectMake(self.frame.origin.x, 0, self.frame.size.width, self.frame.size.height);
                             [self.superview layoutIfNeeded];
                         } completion:^(BOOL finished) {
                             [self performSelector:@selector(hideNotify) withObject:nil afterDelay:3.5];
                         }];
    }
//    });
}

- (void)hideNotify
{
    [UIView animateWithDuration:0.5
                          delay:3.5
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.frame = CGRectMake(self.frame.origin.x, -NOTIFY_HEIGHT, self.frame.size.width, self.frame.size.height);
                         [self.superview layoutIfNeeded];
                     } completion:^(BOOL finished) {
                         [items removeLastObject];
                         
                         pushNotif = nil;
                         
                         if ( [items count] > 0 ) {
                             [self showNotify:[[items lastObject][@"type"] integerValue] withText:[items lastObject][@"text"] withPush:[items lastObject][@"push"]];
                         }
                         
                     }];
}


// type - номер плашки
// 0 - ошибка красная плашка
// 1 - оранжевая плашка
// 2 - жёлтая плашка
- (void)setBackgroundColorWithStatus:(NSUInteger)type
{
    switch (type) {
        case 0:
            [self setBackgroundColor: [UIColor colorWithRed:249.0/255.0f green:35.0/255.0f blue:1.0/255.0f alpha:1.0f]];
            break;
            
        case 1:
            [self setBackgroundColor: [UIColor colorWithRed:255.0/255.0f green:115.0/255.0f blue:0.0/255.0f alpha:1.0f]];
            break;
            
        case 2:
            [self setBackgroundColor: [UIColor h_notifYellow]];
            break;
            
        case 3:
            [self setBackgroundColor: [UIColor colorWithRed:158.0/255.0f green:158.0/255.0f blue:158.0/255.0f alpha:1.0f]];
            break;
            
        default:
            break;
    }
}


- (void)goToTheDarkness
{
    [self.layer removeAllAnimations];
}




- (void)tryOpenPush:(UITapGestureRecognizer *)sender
{
    if ( pushNotif ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STPushIsReceived" object:nil userInfo:@{@"push": pushNotif}];
    }
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, 10, 0, 10};
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
