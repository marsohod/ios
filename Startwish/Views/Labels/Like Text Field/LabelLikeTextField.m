//
//  LabelLikeTextField.m
//  Startwish
//
//  Created by marsohod on 03/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "LabelLikeTextField.h"
#import "UIColor+Helper.h"
#import "UILabel+typeOfText.h"
#import "UIFont+Helper.h"


@interface LabelLikeTextField ()
{
    NSString *iconName;
    BOOL rightDirect;
    
    BOOL isHighlighted;
    float iconWidthMax;
    float iconPaddingTop;
    float iconPaddingLeft;
    NSString *placeholder;
    
}
@end

@implementation LabelLikeTextField

#define FIELD_HEIGHT 42.0
#define FIELD_FONT_SIZE 19.0
#define ICON_MAX_WIDTH 28.0

- (void)awakeFromNib
{
    _colorDefault = @"yellow";
    _colorHighlight = @"white";
    iconPaddingTop = -4.0;
    iconPaddingLeft = 0;
    // Устанавливаем максимульную ширину (отступ) текста от иконки
    iconWidthMax = ICON_MAX_WIDTH;
    // Запоминаем первое, что было написано
    placeholder = self.text;
    
    self.font = [UIFont h_defaultFontLightSize:FIELD_FONT_SIZE];
//    [self setFont: self.font];
    [self setTextAlignment: NSTextAlignmentLeft];
    self.layer.masksToBounds = YES;
    
//    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:FIELD_HEIGHT]];
}

// Ставим нижний бордер
- (void)customSetBackground
{
    NSString *bgColorName = isHighlighted ? _colorHighlight : _colorDefault;
    UIColor *borderColor;
    
    if ( [_colorDefault isEqualToString: @"yellow"] ) {
        borderColor = [bgColorName isEqualToString: @"yellow"] ? [UIColor h_smoothYellow] : [UIColor whiteColor];
    } else if ( [_colorDefault isEqualToString: @"black"] ) {
        borderColor = [UIColor blackColor];
    }

    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = borderColor.CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(-1, FIELD_HEIGHT-1, [UIScreen mainScreen].bounds.size.width, 1);
    
    self.layer.sublayers = nil;
    [self.layer addSublayer:bottomBorder];

    [self setTextColor: borderColor];
}


- (void)customSetText:(NSString *)text
{
    [self setText: text];
    
    // Если есть иконка, то вставляем её расскрашенную
    if ( iconName != nil ) {
        if ( [text isEqualToString: placeholder] || [text isEqualToString:@""] ) {
            isHighlighted = NO;
        } else {
            isHighlighted = YES;
        }
        [self setTextWithIcon];
    }
    
    if ( [text isEqualToString: placeholder] || [text isEqualToString:@""] ) {
        if ( iconName == nil ) {
            [self setText: placeholder];
        }
        
        isHighlighted = NO;
    } else {
        isHighlighted = YES;
    }
    
    [self customSetBackground];
}

- (NSString *)customGetText
{
    // удаляем иконку, если она есть слева или справа
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    
    [mas replaceCharactersInRange:NSMakeRange(rightDirect == NO ? 0 : placeholder.length, mas.length - placeholder.length) withString:@""];
    
//    NSLog(@"_%@ __%@", self.attributedText, [mas string]);
    
    if( [[[mas string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:placeholder] ) {
        return @"";
    } else {
        return [[mas string] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
}

// Добавляем иконку слева или справа от текста
- (void)setTextWithIcon
{
    UIImage *icon = [UIImage imageNamed:[NSString stringWithFormat:@"%@_%@.png", iconName, isHighlighted ? _colorHighlight : _colorDefault]];
//    NSLog(@"%@", self.text);
    self.attributedText = [UILabel tt_label: self.text
                                  withImage: icon
                                     bounds: CGRectMake(iconPaddingLeft, iconPaddingTop, icon.size.width, icon.size.height)
                                    padding: [self paddingFromIcon: icon]
                                       left: !rightDirect]
    ;
}

// Добавляем пробелы для равного отступа всех иконок
// Высчитываем их длину и смотрим на разницу между максимумом и тем, что есть
- (NSString *)paddingFromIcon:(UIImage *)icon
{
    NSString *string = @"";
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:self.font, NSFontAttributeName, nil];
    
    for( int i = 0; i < 5; i++ ) {
        if( (NSInteger)[[[NSAttributedString alloc] initWithString: string attributes:attributes] size].width < (NSInteger)(iconWidthMax - icon.size.width) ) {
            string = [string stringByAppendingString:@" "];
        } else {
            break;
        }
    }
    
    return string;
}

// Устанавливаем значения иконки и её местоположения
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect
{
    iconName = icon;
    rightDirect = rDirect;
    [self customSetText: placeholder];
}


// Устанавливаем значения иконки и её местоположения
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth
{
    iconName = icon;
    iconWidthMax = iconWidth;
    rightDirect = rDirect;
    [self customSetText: placeholder];
}

// Устанавливаем значения иконки, её местоположения и отступ
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth paddingTop:(float)top paddingLeft:(float)left
{
    iconPaddingTop = top;
    iconPaddingLeft = left;
    
    [self setIcon: icon toTheRightDirect: rDirect withMaxIconWidth:iconWidth];
}

@end
