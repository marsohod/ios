//
//  LabelLikeTextField.h
//  Startwish
//
//  Created by marsohod on 03/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface LabelLikeTextField : UILabel

//@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) NSString *colorDefault;
@property (nonatomic, strong) NSString *colorHighlight;

- (void)customSetText:(NSString *)text;
- (NSString *)customGetText;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth paddingTop:(float)top paddingLeft:(float)left;
@end
