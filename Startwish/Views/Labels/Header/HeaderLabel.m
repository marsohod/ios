//
//  HeaderLabel.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HeaderLabel.h"


@implementation HeaderLabel

- (void)awakeFromNib
{
    [self setFont: [UIFont fontWithName:@"HelveticaNeue-Light" size:30.0]];
    self.textColor = [UIColor whiteColor];
    [self sizeToFit];
}

@end
