//
//  TitleLabel.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "TitleLabel.h"
#import "UIColor+Helper.h"


@implementation TitleLabel

- (void)awakeFromNib
{
    [self setFont: [UIFont fontWithName:@"HelveticaNeue" size:16.0]];
    self.textColor = [UIColor h_smoothYellow];
    [self sizeToFit];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
