//
//  CustomTextField.m
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomTextField.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"


@interface CustomTextField ()

@end

@implementation CustomTextField

#define FIELD_HEIGHT 36.0
#define FIELD_FONT_SIZE 21.0
#define ICON_MAX_WIDTH 28.0

- (void)awakeFromNib
{
    [self setColorDefault:@"yellow"];
    [self setColorHighlight:@"white"];

    iconPaddingTop = 0.0;
    iconPaddingLeft = 0.0;
    [self setFont:[UIFont h_defaultFontLightSize:FIELD_FONT_SIZE]];
    [self setBorderStyle:UITextBorderStyleNone];
    [self setTextColor:[UIColor whiteColor]];
    [self setTintColor:[UIColor whiteColor]];
    self.textAlignment = NSTextAlignmentLeft;
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    
    // Устанавливаем максимульную ширину (отступ) текста от иконки
    iconWidthMax = ICON_MAX_WIDTH;
    
    // Добавляем иконку если не обходимо
//    [self addIcon:_colorDefault];
    
    [self addTarget:self action:@selector(endEnterTextFieldAction:) forControlEvents:UIControlEventEditingDidEnd];
    [self addTarget:self action:@selector(endEnterTextFieldAction:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self addTarget:self action:@selector(startEnterTextFieldAction:) forControlEvents:UIControlEventEditingDidBegin];
    
    // Устанавливаем высоту кнопки
    [self.superview addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                               attribute:NSLayoutAttributeHeight
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:nil
                                                               attribute:NSLayoutAttributeHeight
                                                              multiplier:1.0
                                                                constant:FIELD_HEIGHT]];
}

- (void)setColorDefault:(NSString *)colorDefault
{
    _colorDefault = colorDefault;
   
    [self customSetPlaceholder: self.placeholder];
    
    // Ставим подчёркивание в виде картинки
    [self customSetBackground:_colorDefault];
}

- (void)customSetPlaceholder:(NSString *)string
{
    UIColor *colorPlaceholder;
    
    if( [_colorDefault isEqualToString:@"yellow"] ) {
        colorPlaceholder = [UIColor h_smoothYellow];
    } else if ( [_colorDefault isEqualToString:@"black"] ) {
        colorPlaceholder = [UIColor blackColor];
    } else {
        colorPlaceholder = [UIColor h_smoothYellow];
    }
    
    // Устанавливаем цвет для placeholder
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString: (string != nil ? string : @"")
                                                                 attributes: @{
                                                                               NSForegroundColorAttributeName:  colorPlaceholder,
                                                                               NSFontAttributeName:             [UIFont h_defaultFontLightSize:FIELD_FONT_SIZE]
                                                                               }];
    
    // Запоминаем placeholder
    placeholder = self.attributedPlaceholder;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // Если не пустое поле, то добавляем нужный стиль
    if ( ![self.text isEqualToString:@""] ) {
        [self startEnterTextFieldAction:self];
    }
    
}

- (IBAction)startEnterTextFieldAction:(id)sender
{
    [self customSetBackground:_colorHighlight];
    [self addIcon:_colorHighlight];
    self.placeholder = nil;
}

- (IBAction)endEnterTextFieldAction:(id)sender
{
    if ( [[self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ) {
        [self customSetBackground:_colorDefault];
        [self addIcon:_colorDefault];
        self.attributedPlaceholder = placeholder;
    } else {
        [self customSetBackground:_colorHighlight];
        [self addIcon:_colorHighlight];
        self.placeholder = nil;
    }
    
    [sender resignFirstResponder];
}

- (void)customSetBackground:(NSString *)bgColor
{
    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"textField_border_bottom_%@", bgColor]];
    
    if ( image != nil ) {
        [self setBackground:image];
    }
}


// Добавляем иконку слева или справа от текста
- (void)addIcon:(NSString *)bgColor
{
    if ( iconName != nil ) {
        // Создаём иконку
        UIImageView *icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_%@.png", iconName, bgColor]]];
//        float iconHeightPadding = FIELD_HEIGHT - ( icon.frame.size.height + FIELD_FONT_SIZE / 2 );
        
        if( icon != nil ) {
            icon.contentMode = UIViewContentModeTopLeft;
            
            if( rightDirect ) {
                icon.frame = CGRectMake(0.0, 0.0, icon.image.size.width + (iconWidthMaxRight - icon.image.size.width + iconPaddingRight), FIELD_HEIGHT + iconPaddingTop);
                
                // Задаём иконку справа от текста
                self.rightView = icon;
                self.rightViewMode = UITextFieldViewModeAlways;
            } else {
                icon.frame = CGRectMake(0.0, 0.0, icon.image.size.width + (iconWidthMax - icon.image.size.width + iconPaddingLeft), FIELD_HEIGHT + iconPaddingTop);
                
                //задаём иконку слева от текста
                self.leftView = icon;
                self.leftViewMode = UITextFieldViewModeAlways;
            }
        }
    }
}

// Устанавливаем значения иконки и её местоположения
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect
{
    iconName = icon;
    rightDirect = rDirect;
    [self addIcon:_colorDefault];
}


// Устанавливаем значения иконки и её местоположения
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth
{
    iconName = icon;
    if( rDirect == NO ) {
        iconWidthMax = iconWidth;
    } else {
        iconWidthMaxRight = iconWidth;
    }
    
    rightDirect = rDirect;
    [self addIcon:_colorDefault];
}

- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth paddingTop:(float)top paddingLeft:(float)left
{
    iconPaddingTop = top;
    if( rDirect == NO ) {
        iconPaddingLeft = left;
    } else {
        iconPaddingRight = left;
    }
    
    [self setIcon: icon toTheRightDirect: rDirect withMaxIconWidth:iconWidth];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, iconWidthMax, 3);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, iconWidthMax, 3);
}

@end
