//
//  CustomTextField.h
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CustomTextField : UITextField
{
    NSString *iconName;
    NSAttributedString *placeholder;
    BOOL rightDirect;
    float iconWidthMax;
    float iconWidthMaxRight;
    float iconPaddingTop;
    float iconPaddingLeft;
    float iconPaddingRight;
}

@property (nonatomic, strong) NSString *colorDefault;
@property (nonatomic, strong) NSString *colorHighlight;

- (void)customSetPlaceholder:(NSString *)string;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth;
- (void)setIcon:(NSString *)icon toTheRightDirect:(BOOL)rDirect withMaxIconWidth:(float)iconWidth paddingTop:(float)top paddingLeft:(float)left;
@end
