//
//  CustomSwitch.m
//  Startwish
//
//  Created by marsohod on 29/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomSwitch.h"
#import "UIColor+Helper.h"


@implementation CustomSwitch

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self render];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        [self render];
    }
    
    return self;
}
- (void)render
{
    self.thumbTintColor = [UIColor h_smoothYellow];
    [self setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch_off.png"]]];
    self.layer.cornerRadius = 16.0;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [UIColor h_smoothYellow].CGColor;
    self.clipsToBounds = YES;
    self.shadowColor = [UIColor blackColor];
    self.activeColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch_off.png"]];
    self.onImage = [UIImage imageNamed:@"switch_on.png"];
    
//    self.thumbTintColor = [UIColor h_smoothYellow];
//    self.tintColor = [UIColor h_smoothYellow];
//    self.onTintColor = [UIColor h_switchOn];
//    [self setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch_off.png"]]];
//    self.layer.cornerRadius = 16.0;
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [UIColor h_smoothYellow].CGColor;
//    self.clipsToBounds = YES;

}

- (void)drawRect:(CGRect)rect
{

}
@end
