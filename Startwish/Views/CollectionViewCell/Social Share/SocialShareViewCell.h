//
//  SocialShareViewCell.h
//  Startwish
//
//  Created by marsohod on 23/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface SocialShareViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (nonatomic) BOOL customSelect;

- (void)setSocialShareCell:(NSString *)name;
- (void)deselectCell:(NSString *)name;;
- (void)selectCell:(NSString *)name;;

@end
