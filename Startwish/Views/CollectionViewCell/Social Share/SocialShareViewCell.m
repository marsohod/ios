//
//  SocialShareViewCell.m
//  Startwish
//
//  Created by marsohod on 23/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "SocialShareViewCell.h"


//@interface SocialShareViewCell ()
//{
//    NSString *imageName;
//}
//@end

@implementation SocialShareViewCell

- (void)prepareForReuse
{
    
}
- (void)setSocialShareCell:(NSString *)name
{
    if ( self.customSelect ) {
        if( ![name isEqualToString:@""] ) {
            _image.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@_selected.png", name]];
        }
    } else {
        if( ![name isEqualToString:@""] ) {
            _image.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png", name]];
        }
    }
}

- (void)deselectCell:(NSString *)name
{
    _image.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png", name]];
    self.customSelect = NO;
}

- (void)selectCell:(NSString *)name
{
    _image.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@_selected.png",name]];
    self.customSelect = YES;
}


@end
