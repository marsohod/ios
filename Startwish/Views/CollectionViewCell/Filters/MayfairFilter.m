//
//  MayfairFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "MayfairFilter.h"
#import "UIColor+Helper.h"


@interface MayfairFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation MayfairFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_mayfair];
        self.type = @"Mayfair";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,20.f),
                                           ToneValue(85.f,110.f),
                                           ToneValue(125.f,170.f),
                                           ToneValue(221.f,232.f),
                                           ToneValue(254.f,242.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,15.f),
                                             ToneValue(40.f,55.f),
                                             ToneValue(80.f,95.f),
                                             ToneValue(142.f,196.f),
                                             ToneValue(188.f,215.f),
                                             ToneValue(255.f,230.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,15.f),
                                            ToneValue(45.f,60.f),
                                            ToneValue(85.f,115.f),
                                            ToneValue(135.f,185.f),
                                            ToneValue(182.f,215.f),
                                            ToneValue(235.f,230.f),
                                            ToneValue(255.f,225.f)];
    }
    
    return @[tcFilter];
}

@end
