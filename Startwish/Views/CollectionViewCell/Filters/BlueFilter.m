//
//  BlueFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 02/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "BlueFilter.h"
#import "UIColor+Helper.h"
#import "GPUImage.h"


@interface BlueFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;

@property (nonatomic) CGFloat intensity;

@end

@implementation BlueFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_blue];
        self.type = @"Blue";
    }
    
    [self setDefaultMaxValue];
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.intensity = 1.f;
}


#pragma mark - Amatorka

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    GPUImageAmatorkaFilter *aFilter = [[GPUImageAmatorkaFilter alloc] initWithIntensity:self.intensity*self.portion];
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        self.stillImageSource = [[GPUImagePicture alloc] initWithImage:image];
        self.inputImage = image;
    }
    
    [aFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    [self.stillImageSource addTarget:aFilter];
    
    [aFilter useNextFrameForImageCapture];
    [self.stillImageSource processImage];
    [self.stillImageSource removeAllTargets];
    
    return [aFilter imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

@end
