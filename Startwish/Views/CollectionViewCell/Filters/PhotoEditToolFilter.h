//
//  PhotoEditToolFilter.h
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PhotoEditTool.h"
#import "GPUImage.h"


#define m(a,b) (a+b)/2/255.f
#define ToneValue(a, b) [NSValue valueWithCGPoint:CGPointMake(m(a,b) + (a/255.f-m(a,b))*self.portion, m(a,b) + (b/255.f-m(a,b))*self.portion)]

@interface PhotoEditToolFilter : PhotoEditTool

@property (nonatomic) CGFloat contrast;
@property (nonatomic) CGFloat exposure;
@property (nonatomic) CGFloat value;

#pragma mark - apply

- (NSArray *)GPUFilters;
- (NSArray *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params;

- (void)saveValue;
- (CGFloat)actualValue;

- (CGFloat)portion;
@end
