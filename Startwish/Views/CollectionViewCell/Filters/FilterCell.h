//
//  FilterCell.h
//  Startwish
//
//  Created by Pavel Makukha on 25/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PhotoEditTool.h"


@interface FilterCell : UICollectionViewCell

- (void)setCell:(PhotoEditTool *)cell;

@end
