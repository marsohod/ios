//
//  PhotoEditTool.h
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface PhotoEditTool : NSObject

@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) CGFloat borderWidth;
@property (nonatomic) CGFloat minValue;
@property (nonatomic) CGFloat maxValue;
@property (nonatomic) CGFloat sliderMinValue;
@property (nonatomic) CGFloat sliderMaxValue;

- (CGFloat)actualValueBySliderValue:(CGFloat)sliderValue withMaxSliderValue:(CGFloat )maxChangeValue;
- (CGFloat)sliderValueFromActualValue:(CGFloat)actualValue withMaxSliderValue:(CGFloat )maxChangeValue;
@end
