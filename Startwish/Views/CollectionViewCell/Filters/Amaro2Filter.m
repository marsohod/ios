//
//  CoralFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 10/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://photodoto.com/instagram-filters-tutorial-amaro-mayfair/
//


#import "Amaro2Filter.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface Amaro2Filter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageContrastFilter *cFilter;
@property (nonatomic, strong) GPUImageBrightnessFilter *bFilter;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageMultiplyBlendFilter *mbFilter;
@property (nonatomic, strong) GPUImageRGBFilter *rgbFilter;
@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;


@property (nonatomic) CGFloat blueLevel;
@property (nonatomic) CGFloat bright;
@property (nonatomic) CGFloat contr;
@property (nonatomic) CGFloat contr2;
@property (nonatomic) CGFloat r;
@property (nonatomic) CGFloat g;
@property (nonatomic) CGFloat b;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;

@end

@implementation Amaro2Filter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_coral];
        self.type = @"Coral";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.mbFilter   = [[GPUImageMultiplyBlendFilter alloc] init];
        self.bFilter    = [[GPUImageBrightnessFilter alloc] init];
        self.cFilter    = [[GPUImageContrastFilter alloc] init];
        self.rgbFilter  = [[GPUImageRGBFilter alloc] init];
        self.vFilter    = [[GPUImageVignetteFilter alloc] init];
        
        
        [self.bFilter addTarget:self.cFilter];
        [self.cFilter addTarget:self.mbFilter];
        [self.mbFilter addTarget:self.lFilter];
        [self.lFilter addTarget:self.rgbFilter];
        [self.rgbFilter addTarget:self.vFilter];
        
        [self.group setInitialFilters:@[self.bFilter]];
        [self.group setTerminalFilter:self.vFilter];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.blueLevel  = 117.0/255.f;
    self.bright     = 0.1f;
    self.contr      = 0.1f;
    self.contr2     = 0.1f;
    self.r          = 0.052f;
    self.g          = 0.025f;
    self.b          = 0.024f;
    self.vstart     = 0.4f;
    self.vend       = 1.2f;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    self.lookupImageSource  = [[GPUImagePicture alloc] initWithImage:[UIImage h_imageWithColor:[UIColor colorWithRed:254/255.f green:247/255.f blue:217/255.f alpha:self.portion] size:image.size]];
    
    // 1. Set size image for process
    [self.bFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    // 2. Set brightness
    self.bFilter.brightness = self.bright * self.portion;

    // 3. Set contrast
    self.cFilter.contrast = 1.f + self.contr * self.portion;

    // 4. Set levels
    [self.lFilter setBlueMin:0.f gamma:1.f max:1.f minOut:self.blueLevel*self.portion maxOut:1.f];

    // 5. ProcessImage for save filters state before
    [self.cFilter useNextFrameForImageCapture];
    
    // 6. Correct contrast
    self.cFilter.contrast = 1.f - self.contr2 * self.portion;

    // 7. Change rgb channels
    self.rgbFilter.red      = 1.f + self.r*self.portion;
    self.rgbFilter.green    = 1.f + self.g*self.portion;
    self.rgbFilter.blue     = 1.f + self.b*self.portion;

    // 8. Prepare gray corners for burn
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;

    // 9. Link Filters and Images
    [self.lookupImageSource addTarget:self.mbFilter];
 
    // 10. Processing image
    [self.group useNextFrameForImageCapture];
    
    [self.lookupImageSource processImage];
    [self.stillImageSource processImage];
    
    // 11. Remove link from image
    [self.lookupImageSource removeAllTargets];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        
        [self.stillImageSource addTarget:self.group];
        
        
        self.inputImage = image;
    }
}

@end
