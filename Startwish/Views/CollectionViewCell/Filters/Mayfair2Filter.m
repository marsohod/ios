//
//  MelloYellowFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 10/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://photodoto.com/instagram-filters-tutorial-amaro-mayfair/
//

#import "Mayfair2Filter.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface Mayfair2Filter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageMonochromeFilter *mFilter;
@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;
@property (nonatomic, strong) GPUImageAlphaBlendFilter *aFilter;
@property (nonatomic, strong) GPUImageAlphaBlendFilter *aFilter2;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageFalseColorFilter *gFilter;
//@property (nonatomic, strong) GPUImageRGBFilter *rgbFilter;
//@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;


@property (nonatomic) CGFloat gamma;
@property (nonatomic) CGFloat levelMaxIn;
@property (nonatomic) CGFloat toneIn;
@property (nonatomic) CGFloat toneOut;
@property (nonatomic) CGFloat toneIn2;
@property (nonatomic) CGFloat toneOut2;
@property (nonatomic) CGFloat opacity;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;

@end

@implementation Mayfair2Filter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_mellowYellow];
        self.type = @"MellowYellow";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.mFilter    = [[GPUImageMonochromeFilter alloc] init];
        self.aFilter    = [[GPUImageAlphaBlendFilter alloc] init];
        self.aFilter2   = [[GPUImageAlphaBlendFilter alloc] init];
        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
        self.gFilter    = [[GPUImageFalseColorFilter alloc] init];
//        self.luFilter   = [[GPUImageLookupFilter alloc] init];
//        self.rgbFilter  = [[GPUImageRGBFilter alloc] init];
//        self.vFilter    = [[GPUImageVignetteFilter alloc] init];
        
        
        [self.mFilter addTarget:self.lFilter];
        [self.lFilter addTarget:self.tcFilter];
        [self.tcFilter addTarget:self.aFilter];
        [self.aFilter addTarget:self.gFilter];
        [self.gFilter addTarget:self.aFilter2];
//        [self.rgbFilter addTarget:self.vFilter];
        
        [self.group setInitialFilters:@[self.mFilter]];
        [self.group setTerminalFilter:self.aFilter2];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.gamma      = 0.24f;
    self.levelMaxIn = 0.13f/255.f;
    self.toneIn     = 85.f/255.f;
    self.toneOut    = 56.f/255.f;
    self.toneIn2    = 178.f/255.f;
    self.toneOut2   = 198.f/255.f;
    self.opacity    = 0.5f;
    self.vstart     = 0.4f;
    self.vend       = 1.2f;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    self.lookupImageSource  = [[GPUImagePicture alloc] initWithImage:image];
    
    // 1. Set size image for process
    [self.mFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    // 2. Set black and white image color
    self.mFilter.color = (GPUVector4){0.5,0.5,0.5,1.f};
    self.mFilter.intensity = 1.f * self.portion;
    
    // 3. Set levels
    [self.lFilter setMin:0.f gamma:1.f + self.gamma*self.portion max:1.f - self.levelMaxIn*self.portion minOut:0.f maxOut:1.f];
    
    // 4. Set tone curve
    NSValue *v0 = [NSValue valueWithCGPoint:CGPointMake(0.f, 0.f)];
    NSValue *v1 = [NSValue valueWithCGPoint:CGPointMake(self.toneIn*self.portion, self.toneOut*self.portion)];
    NSValue *v2 = [NSValue valueWithCGPoint:CGPointMake(1.f - (1.f - self.toneIn2)*self.portion, 1.f - (1.f - self.toneOut2)*self.portion)];
    NSValue *v3 = [NSValue valueWithCGPoint:CGPointMake(1.f, 1.f)];
    
    if ( self.portion != 0.0 ) {
        [self.tcFilter setRgbCompositeControlPoints:[NSArray arrayWithObjects:v0,v1,v2,v3,nil]];
    }
    
    // 5. Set opacity for black and white layer
    self.aFilter.mix = self.opacity * self.portion;
    
    // 6. Set violent/orange gradient

    self.gFilter.firstColor = (GPUVector4){41.f/255.f, 10.f/255.f, 89.f/255.f, 0.75f*self.portion}; //violet
    self.gFilter.secondColor = (GPUVector4){1.f, 124.f/255.f, 0.f, 0.75f*self.portion}; //orange
    self.aFilter2.mix = 1.f - 0.25f * self.portion;
    /*
    // 5. ProcessImage for save filters state before
    [self.cFilter useNextFrameForImageCapture];
    
    // 6. Correct contrast
    self.cFilter.contrast = 1.f - self.contr2 * self.portion;
    
    // 7. Change rgb channels
    self.rgbFilter.red      = 1.f + self.r*self.portion;
    self.rgbFilter.green    = 1.f + self.g*self.portion;
    self.rgbFilter.blue     = 1.f + self.b*self.portion;
    
    // 8. Prepare gray corners for burn
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;
    
    // 9. Link Filters and Images
    [self.lookupImageSource addTarget:self.mbFilter];
*/
    // 10. Processing image
    [self.lookupImageSource addTarget:self.group];
    [self.lookupImageSource addTarget:self.aFilter2];
    [self.group useNextFrameForImageCapture];
    
    [self.stillImageSource processImage];
    [self.lookupImageSource processImage];
    
//    UIImage *topImage = [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
    
    
    
    // 11. Remove link from image
    [self.lookupImageSource removeAllTargets];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];// imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        
//        [self.stillImageSource addTarget:self.mbFilter];
        [self.stillImageSource addTarget:self.aFilter];
//        [self.stillImageSource addTarget:self.group];
        
        
        self.inputImage = image;
    }
}

@end
