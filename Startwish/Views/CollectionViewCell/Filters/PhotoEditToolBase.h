//
//  PhotoEditToolBase.h
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PhotoEditTool.h"


@interface PhotoEditToolBase : PhotoEditTool

- (instancetype)initBrightnessTool;
- (instancetype)initCropTool;
- (instancetype)initContrastTool;

@end
