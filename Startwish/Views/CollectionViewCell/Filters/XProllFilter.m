//
//  XProllFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "XProllFilter.h"
#import "GPUImage.h"
#import "UIColor+Helper.h"


@interface XProllFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation XProllFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_xpro];
        self.type = @"XProll";
        
        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,0.f),
                                           ToneValue(42.f,28.f),
                                           ToneValue(105.f,100.f),
                                           ToneValue(148.f,160.f),
                                           ToneValue(185.f,208.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(40.f,25.f),
                                             ToneValue(85.f,75.f),
                                             ToneValue(125.f,130.f),
                                             ToneValue(165.f,180.f),
                                             ToneValue(212.f,230.f),
                                             ToneValue(255.f,255.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,30.f),
                                            ToneValue(40.f,58.f),
                                            ToneValue(82.f,90.f),
                                            ToneValue(125.f,125.f),
                                            ToneValue(170.f,160.f),
                                            ToneValue(235.f,210.f),
                                            ToneValue(255.f,222.f)];
    }
    
    return @[tcFilter];
}

@end
