//
//  HudsonFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "HudsonFilter.h"
#import "UIColor+Helper.h"


@interface HudsonFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation HudsonFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_hudson];
        self.type = @"Hudson";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,35.f),
                                           ToneValue(42.f,68.f),
                                           ToneValue(85.f,115.f),
                                           ToneValue(124.f,165.f),
                                           ToneValue(170.f,200.f),
                                           ToneValue(215.f,228.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(45.f,60.f),
                                             ToneValue(102.f,135.f),
                                             ToneValue(140.f,182.f),
                                             ToneValue(192.f,215.f),
                                             ToneValue(255.f,255.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,0.f),
                                            ToneValue(24.f,42.f),
                                            ToneValue(60.f,100.f),
                                            ToneValue(105.f,170.f),
                                            ToneValue(145.f,208.f),
                                            ToneValue(210.f,235.f),
                                            ToneValue(255.f,245.f)];
    }
    
    return @[tcFilter];
}


@end
