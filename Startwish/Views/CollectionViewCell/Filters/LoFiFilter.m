//
//  LoFiFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "LoFiFilter.h"
#import "UIColor+Helper.h"


@interface LoFiFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation LoFiFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_lofi];
        self.type = @"LoFi";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,0.f),
                                           ToneValue(40.f,20.f),
                                           ToneValue(88.f,80.f),
                                           ToneValue(125.f,150.f),
                                           ToneValue(170.f,200.f),
                                           ToneValue(230.f,245.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(35.f,15.f),
                                             ToneValue(90.f,70.f),
                                             ToneValue(105.f,105.f),
                                             ToneValue(148.f,180.f),
                                             ToneValue(188.f,218.f),
                                             ToneValue(255.f,255.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,0.f),
                                            ToneValue(62.f,50.f),
                                            ToneValue(100.f,95.f),
                                            ToneValue(130.f,155.f),
                                            ToneValue(150.f,182.f),
                                            ToneValue(190.f,220.f),
                                            ToneValue(255.f,255.f)];
    }
    
    return @[tcFilter];

}

@end
