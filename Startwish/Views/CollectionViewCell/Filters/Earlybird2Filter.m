//
//  OrangeFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 09/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
//
// Video instruction "How make filter" http://photoshoproadmap.com/video-tutorials/yt/2GwOi1lJYPo/


#import "Earlybird2Filter.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface Earlybird2Filter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSourceEdge;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageMultiplyBlendFilter *mbFilter;
@property (nonatomic, strong) GPUImageColorBurnBlendFilter *cbFilter;
@property (nonatomic, strong) GPUImageSaturationFilter *sFilter;
@property (nonatomic, strong) GPUImageBrightnessFilter *bFilter;
@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;



@property (nonatomic) CGFloat gamma;
@property (nonatomic) CGFloat gammaStep2;
@property (nonatomic) CGFloat redLevel;
@property (nonatomic) CGFloat bright;
@property (nonatomic) CGFloat saturation;
@property (nonatomic) CGFloat saturationNormal;
@property (nonatomic) CGFloat saturationStep2;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;

@end

@implementation Earlybird2Filter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_orange];
        self.type = @"Orange";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.mbFilter   = [[GPUImageMultiplyBlendFilter alloc] init];
        self.cbFilter   = [[GPUImageColorBurnBlendFilter alloc] init];
        self.sFilter    = [[GPUImageSaturationFilter alloc] init];
        self.bFilter    = [[GPUImageBrightnessFilter alloc] init];
        self.vFilter    = [[GPUImageVignetteFilter alloc] init];

        [self.mbFilter addTarget:self.sFilter];
        [self.sFilter addTarget:self.lFilter];
        [self.lFilter addTarget:self.bFilter];
        [self.bFilter addTarget:self.cbFilter];
//        [self.bFilter addTarget:self.vFilter];
        
        [self.group setInitialFilters:@[self.mbFilter]];
        [self.group setTerminalFilter:self.cbFilter];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.gamma      = 0.2f;
    self.gammaStep2 = 0.9f;
    self.redLevel   = 25.0/255.f;
    self.saturation = 0.7f; // change from 0 to 2. In Photoshop it change from -100 to 100;
    self.saturationNormal = 1.f;
    self.saturationStep2 = 0.85f;
    self.bright     = 0.1f;
    self.vstart     = 0.4f;
    self.vend       = 0.6f;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    self.lookupImageSource  = [[GPUImagePicture alloc] initWithImage:[UIImage h_imageWithColor:[UIColor colorWithRed:252/255.f green:243/255.f blue:214/255.f alpha:self.portion] size:image.size]];
    
    // 1. Set size image for process
    [self.mbFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    // 2. Set saturation
    self.sFilter.saturation = self.saturationNormal - (self.saturationNormal-self.saturation) * self.portion;
    
    // 3. Set Levels
    [self.lFilter setMin:0.f gamma:1.f + self.gamma*self.portion max:1.f minOut:0.f maxOut:1.f];
    [self.lFilter setRedMin:0.f gamma:1.f max:1.f minOut:self.redLevel*self.portion maxOut:1.f];
    
    // 4. Set Brightness
    self.bFilter.brightness = self.bright*self.portion;
    
    // 5. ProcessImage for save filters state before
    [self.sFilter useNextFrameForImageCapture];
    
    // 6. Correct saturation
    self.sFilter.saturation = self.saturationNormal - (self.saturationNormal-self.saturationStep2) * self.portion;
    
    // 7. ProcessImage for save filters state before
    [self.lFilter useNextFrameForImageCapture];
    
    // 8. Correct Levels
    [self.lFilter setMin:0.f gamma:1.f - (1.f-self.gammaStep2) * self.portion max:1.f minOut:0.f maxOut:1.f];
    
    // 9. Prepare gray corners for burn
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;
    self.vFilter.vignetteColor = (GPUVector3){184.f/255.f, 184.f/255.f, 184.f/255.f};
    
    // 10. Link Filters and Images
    self.lookupImageSourceEdge = [[GPUImagePicture alloc] initWithImage:[self.vFilter imageByFilteringImage:[UIImage h_imageWithColor:[UIColor whiteColor] size:image.size]]];
    [self.lookupImageSourceEdge addTarget:self.cbFilter];
    [self.lookupImageSource addTarget:self.mbFilter];
    
    // 11. Processing image
    [self.group useNextFrameForImageCapture];
    
    [self.lookupImageSource processImage];
    [self.lookupImageSourceEdge processImage];
    [self.stillImageSource processImage];
    
    // 12. Remove link from image
    [self.lookupImageSource removeAllTargets];
    [self.lookupImageSourceEdge removeAllTargets];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        
        [self.stillImageSource addTarget:self.group];
        
        
        self.inputImage = image;
    }
}

@end
