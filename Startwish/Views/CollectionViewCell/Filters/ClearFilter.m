//
//  NoFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 01/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ClearFilter.h"
#import "GPUImage.h"


@implementation ClearFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor clearColor];
        self.borderColor = [UIColor whiteColor];
        self.borderWidth = 2.f;
        self.type = @"NotFilter";
    }
    
    return self;
}

- (NSArray *)GPUFilters
{
    return nil;
}


@end
