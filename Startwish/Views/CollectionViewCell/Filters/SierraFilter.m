//
//  SierraFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "SierraFilter.h"
#import "UIColor+Helper.h"


@interface SierraFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation SierraFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_sierra];
        self.type = @"Sierra";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,10.f),
                                           ToneValue(48.f,88.f),
                                           ToneValue(105.f,155.f),
                                           ToneValue(130.f,180.f),
                                           ToneValue(190.f,212.f),
                                           ToneValue(232.f,234.f),
                                           ToneValue(255.f,245.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(38.f,72.f),
                                             ToneValue(85.f,124.f),
                                             ToneValue(124.f,160.f),
                                             ToneValue(172.f,186.f),
                                             ToneValue(218.f,210.f),
                                             ToneValue(255.f,230.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,30.f),
                                            ToneValue(45.f,82.f),
                                            ToneValue(95.f,132.f),
                                            ToneValue(138.f,164.f),
                                            ToneValue(176.f,182.f),
                                            ToneValue(210.f,200.f),
                                            ToneValue(255.f,218.f)];
    }
    
    return @[tcFilter];

}

@end
