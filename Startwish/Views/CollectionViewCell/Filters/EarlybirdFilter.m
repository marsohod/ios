//
//  EarlybirdFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "EarlybirdFilter.h"
#import "UIColor+Helper.h"


@interface EarlybirdFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation EarlybirdFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_earlybird];
        self.type = @"EarlybirdFilter";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,25.f),
                                           ToneValue(45.f,80.f),
                                           ToneValue(85.f,135.f),
                                           ToneValue(120.f,180.f),
                                           ToneValue(230.f,240.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(40.f,55.f),
                                             ToneValue(88.f,112.f),
                                             ToneValue(132.f,172.f),
                                             ToneValue(168.f,198.f),
                                             ToneValue(215.f,218.f),
                                             ToneValue(255.f,240.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,18.f),
                                            ToneValue(42.f,58.f),
                                            ToneValue(90.f,102.f),
                                            ToneValue(120.f,130.f),
                                            ToneValue(164.f,170.f),
                                            ToneValue(212.f,195.f),
                                            ToneValue(255.f,210.f)];
    }
    
    return @[tcFilter];
}

@end

