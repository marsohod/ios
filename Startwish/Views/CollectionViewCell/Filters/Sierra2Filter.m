//
//  RedFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 01/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://mashable.com/2013/10/20/photoshop-instagram-filters/
//


#import "Sierra2Filter.h"
#import "UIColor+Helper.h"


@interface Sierra2Filter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;
@property (nonatomic, strong) GPUImageContrastFilter *cFilter;
@property (nonatomic, strong) GPUImageRGBFilter *rgbFilter;
@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;


@property (nonatomic) CGFloat greenColor;
@property (nonatomic) CGFloat redColor;
@property (nonatomic) CGFloat tone;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;
@property (nonatomic) CGFloat contr;

@end

@implementation Sierra2Filter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_red];
        self.type = @"Red";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.rgbFilter  = [[GPUImageRGBFilter alloc] init];
        self.vFilter    = [[GPUImageVignetteFilter alloc] init];
        self.cFilter    = [[GPUImageContrastFilter alloc] init];
        
        [self.tcFilter addTarget:self.lFilter];
        [self.lFilter addTarget:self.cFilter];
        [self.cFilter addTarget:self.rgbFilter];
        [self.rgbFilter addTarget:self.vFilter];
        
        [self.group setInitialFilters:@[self.tcFilter]];
        self.group.terminalFilter = self.vFilter;
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.greenColor = 0.05f;
    self.redColor   = 21.0/255.f;
    self.tone       = 54.0/255.f;
    self.vstart     = 0.4f;
    self.vend       = 1.f;
    self.contr      = 0.15f;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;

    [self cacheInputImageIfNeed:image];
    
    // 1. Set size image for process
    [self.tcFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    // 2. Set tone curve rgb
    self.tcFilter.rgbCompositeControlPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, self.tone*self.portion)], [NSValue valueWithCGPoint:CGPointMake(1.f, 1.f)], nil];
    
    // 3. Set Levels
    [self.lFilter setRedMin:0.f gamma:1.f max:1.f minOut:self.redColor*self.portion maxOut:1.f];
    
    // 4. Set contrast
    self.cFilter.contrast = 1.f + self.contr*self.portion;
    
    // 5. Change rgb channels
    self.rgbFilter.green = 1.f + self.greenColor*self.portion;
    
    // 6. Set dark corners
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;
    
    // 7. Processing Image
    [self.group useNextFrameForImageCapture];
    [self.stillImageSource processImage];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        [self.stillImageSource addTarget:self.group];
        
        self.inputImage = image;
    }
}

@end
