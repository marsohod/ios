//
//  GreenFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 01/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "GreenFilter.h"
#import "UIColor+Helper.h"
#import "GPUImage.h"


@interface GreenFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageLookupFilter *luFilter;

@property (nonatomic) CGFloat blueColor;
@property (nonatomic) CGFloat redColor;
@property (nonatomic) CGFloat intensity;

@end

@implementation GreenFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_smoothDeepGreen];
        self.type = @"Green";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.luFilter   = [[GPUImageLookupFilter alloc] init];
        self.lookupImageSource = [[GPUImagePicture alloc] initWithImage:[UIImage imageNamed:@"1977blowout"]];
        
        //    GPUImageAlphaBlendFilter    *aFilter    = [[GPUImageAlphaBlendFilter alloc] init];
        
        [self.lFilter addTarget:self.luFilter];
        //    [luFilter addTarget:aFilter];
        [self.group setInitialFilters:@[self.lFilter]];
        self.group.terminalFilter = self.luFilter;
        //    [lookupSource addTarget:aFilter];
        [self.lookupImageSource addTarget:self.luFilter];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.blueColor  = 44.0/255.f;
    self.redColor   = 91.0/255.f;
    self.intensity  = 0.2f;
}


#pragma mark - 1977

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        [self.stillImageSource addTarget:self.group];
        
        self.inputImage = image;
    }
    
    [self.lFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    [self.lFilter setRedMin:self.redColor*self.portion gamma:1.f max:1.f minOut:self.redColor*self.portion maxOut:1.f];
    [self.lFilter setBlueMin:self.blueColor*self.portion gamma:1.f max:1.f minOut:self.blueColor*self.portion maxOut:1.f];
    self.luFilter.intensity = self.intensity*self.portion;
//    aFilter.mix = 0.5f;
    

    [self.group useNextFrameForImageCapture];
    [self.lookupImageSource processImage];
    [self.stillImageSource processImage];
    
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

@end
