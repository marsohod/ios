//
//  WhiteFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 01/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "InkwellFilter.h"
#import "GPUImage.h"
#import "UIImage+Helper.h"


@interface InkwellFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImageMonochromeFilter *mFilter;
@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation InkwellFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor whiteColor];
        self.type = @"White";
        
        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
        self.mFilter    = [[GPUImageMonochromeFilter alloc] init];
    }
    
    return self;
}

- (NSArray *)GPUFilters
{
    self.mFilter.color = (GPUVector4){0.5,0.5,0.5,1.f};
    self.mFilter.intensity = 1.f * self.portion;
    
    // 3. Set tone curve
    NSValue *v0 = [NSValue valueWithCGPoint:CGPointMake(0.f, 0.f)];
    NSValue *v1 = [NSValue valueWithCGPoint:CGPointMake((60.f/255.f)*self.portion, (75.f/255.f)*self.portion)];
    NSValue *v2 = [NSValue valueWithCGPoint:CGPointMake(1.f - (1.f - 168.f/255.f)*self.portion, 1.f - (1.f - 218.f/255.f)*self.portion)];
    NSValue *v3 = [NSValue valueWithCGPoint:CGPointMake(1.f, 1.f)];
    
    if ( self.portion != 0.0 ) {
        [self.tcFilter setRgbCompositeControlPoints:[NSArray arrayWithObjects:v0,v1,v2,v3,nil]];
    }
    
    [self.mFilter addTarget:self.tcFilter];
    
    return @[self.mFilter, self.tcFilter];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        
        [self.stillImageSource addTarget:self.mFilter];
        [self.stillImageSource addTarget:self.tcFilter];
        
        self.inputImage = image;
    }
}

@end
