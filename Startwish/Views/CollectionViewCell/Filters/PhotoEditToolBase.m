//
//  PhotoEditToolBase.m
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PhotoEditToolBase.h"
#import "UIColor+Helper.h"


@implementation PhotoEditToolBase

- (void)setDefault
{
    self.bgColor = [UIColor h_smoothGreen];
    self.minValue = -1.0;
    self.maxValue = 1.0;
    self.sliderMinValue = -100.f;
    self.sliderMaxValue = 100.f;
}

- (instancetype)initBrightnessTool
{
    self = [super init];
    
    if ( self ) {
        [self setDefault];
        self.icon = [UIImage imageNamed:@"brightness_white"];
        self.type = @"Exposure";
    }
    
    return self;
}

- (instancetype)initCropTool
{
    self = [super init];
    
    if ( self ) {
        [self setDefault];
        self.minValue = -0.25;
        self.maxValue = 0.25;
        self.icon = [UIImage imageNamed:@"crop_white"];
        self.type = @"Angle";
    }
    
    return self;
}

- (instancetype)initContrastTool
{
    self = [super init];
    
    if ( self ) {
        [self setDefault];
        self.minValue = 0.7;
        self.maxValue = 1.3;
        self.icon = [UIImage imageNamed:@"contrast_white"];
        self.type = @"Contrast";
    }
    
    return self;
}

//- (CGFloat)actualValueBySliderValue:(CGFloat)sliderValue withMaxSliderValue:(CGFloat )maxChangeValue
//{
//    if ( sliderValue < 0 ) {
//        return self.minValue * fabs(sliderValue) / maxChangeValue;
//    } else {
//        return self.maxValue * fabs(sliderValue) / maxChangeValue;
//    }
//}
//
//- (CGFloat)sliderValueFromActualValue:(CGFloat)actualValue withMaxSliderValue:(CGFloat )maxChangeValue
//{
//    CGFloat middle = (self.minValue + self.maxValue) / 2;
//    
//    if ( actualValue < middle ) {
//        return -maxChangeValue * actualValue / self.minValue;
//    } else {
//        return maxChangeValue * actualValue / self.maxValue;
//    }
//}

- (CGFloat)actualValueBySliderValue:(CGFloat)sliderValue withMaxSliderValue:(CGFloat )maxChangeValue
{
    CGFloat fullValues = (self.maxValue - self.minValue);
    CGFloat changeBySlider = fullValues * fabs(sliderValue) / (2 * maxChangeValue);
    CGFloat middle = (self.minValue + self.maxValue) / 2;
    
    if ( sliderValue < 0 ) {
        return middle - changeBySlider;
    } else {
        return middle + changeBySlider;
    }
}

- (CGFloat)sliderValueFromActualValue:(CGFloat)actualValue withMaxSliderValue:(CGFloat )maxChangeValue
{
    CGFloat fullValues = (self.maxValue - self.minValue);
    CGFloat middle = (self.minValue + self.maxValue) / 2;
    
    return (actualValue - middle) * 2 * maxChangeValue / fullValues;
}

@end
