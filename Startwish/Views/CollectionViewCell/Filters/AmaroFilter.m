//
//  AmaroFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "AmaroFilter.h"
#import "UIColor+Helper.h"


@interface AmaroFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation AmaroFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_amaro];
        self.type = @"Amaro";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,19.f),
                                           ToneValue(30.f,62.f),
                                           ToneValue(82.f,88.f),
                                           ToneValue(128.f,148.f),
                                           ToneValue(145.f,200.f),
                                           ToneValue(255.f,250.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(48.f,72.f),
                                             ToneValue(115.f,120.f),
                                             ToneValue(160.f,188.f),
                                             ToneValue(233.f,245.f),
                                             ToneValue(255.f,255.f)];

        tcFilter.blueControlPoints = @[ToneValue(0.f,25.f),
                                            ToneValue(35.f,80.f),
                                            ToneValue(106.f,75.f),
                                            ToneValue(151.f,188.f),
                                            ToneValue(215.f,215.f),
                                            ToneValue(240.f,235.f),
                                            ToneValue(255.f,245.f)];
    }
    
    return @[tcFilter];
}

@end