//
//  SmoothGreenFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 08/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "Hudson2Filter.h"
#import "GPUImage.h"
#import "UIColor+Helper.h"


@interface Hudson2Filter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;
@property (nonatomic, strong) GPUImageBrightnessFilter *bFilter;

@property (nonatomic) CGFloat redColor;
@property (nonatomic) CGFloat greenColor;
@property (nonatomic) CGFloat blueColorOutput;
@property (nonatomic) CGFloat bright;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;

@end


@implementation Hudson2Filter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_smoothGreen];
        self.type = @"SmoothGreen";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.vFilter    = [[GPUImageVignetteFilter alloc] init];
        self.bFilter    = [[GPUImageBrightnessFilter alloc] init];
        
        [self.lFilter addTarget:self.bFilter];
        [self.bFilter addTarget:self.vFilter];
        
        [self.group setInitialFilters:@[self.lFilter]];
        self.group.terminalFilter = self.vFilter;
        
    }
    
    [self setDefaultMaxValue];
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.blueColorOutput = 57.0/255.f;
    self.redColor = 15.0/255.f;
    self.greenColor = 6.0/255.f;
    self.vstart     = 0.45f;
    self.vend       = 1.f;
    self.bright     = 0.2f;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    // 0. Set image processing size for optimize time and gpu
    [self.lFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    // 1. Set Levels
    [self.lFilter setBlueMin:self.blueColorOutput*self.portion gamma:1.f max:1.f minOut:self.blueColorOutput*self.portion maxOut:1.f];
    [self.lFilter setRedMin:self.redColor*self.portion gamma:1.f max:1.f minOut:0.f maxOut:1.f];
    [self.lFilter setGreenMin:self.greenColor*self.portion gamma:1.f max:1.f minOut:0.f maxOut:1.f];
    
    
    // 2. Set Brightness
    self.bFilter.brightness = self.bright * self.portion;
    
    // 3. Set dark corners
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;
    
    // 4. Processing image
    [self.group useNextFrameForImageCapture];
    [self.stillImageSource processImage];
    [self.stillImageSource removeAllTargets];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        [self.stillImageSource addTarget:self.group];
        
        self.inputImage = image;
    }
}

@end
