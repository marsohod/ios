//
//  RiseFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "RiseFilter.h"
#import "UIColor+Helper.h"


@interface RiseFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation RiseFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_rise];
        self.type = @"Rise";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,25.f),
                                           ToneValue(30.f,70.f),
                                           ToneValue(130.f,192.f),
                                           ToneValue(170.f,200.f),
                                           ToneValue(223.f,223.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,25.f),
                                             ToneValue(30.f,72.f),
                                             ToneValue(65.f,118.f),
                                             ToneValue(100.f,158.f),
                                             ToneValue(152.f,195.f),
                                             ToneValue(210.f,230.f),
                                             ToneValue(255.f,255.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,35.f),
                                            ToneValue(40.f,75.f),
                                            ToneValue(82.f,124.f),
                                            ToneValue(120.f,162.f),
                                            ToneValue(175.f,188.f),
                                            ToneValue(220.f,214.f),
                                            ToneValue(255.f,255.f)];
    }
    
    return @[tcFilter];

}

@end
