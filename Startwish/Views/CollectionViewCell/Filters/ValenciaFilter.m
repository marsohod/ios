//
//  ValenciaFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://www.instructables.com/id/How-to-make-Instagram-Filters-in-Photoshop/?ALLSTEPS
//


#import "ValenciaFilter.h"
#import "UIColor+Helper.h"

@interface ValenciaFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
//@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;

@end

@implementation ValenciaFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_valencia];
        self.type = @"Valencia";
        
//        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
    }
    
    return self;
}


- (NSArray *)GPUFilters
{
    GPUImageToneCurveFilter *tcFilter = [[GPUImageToneCurveFilter alloc] init];
    
    if ( self.portion != 0.0 ) {
        tcFilter.redControlPoints = @[ToneValue(0.f,0.f),
                                           ToneValue(20.f,0.f),
                                           ToneValue(80.f,50.f),
                                           ToneValue(120.f,85.f),
                                           ToneValue(162.f,128.f),
                                           ToneValue(224.f,228.f),
                                           ToneValue(240.f,255.f),
                                           ToneValue(255.f,255.f)];
        
        tcFilter.greenControlPoints = @[ToneValue(0.f,0.f),
                                             ToneValue(12.f,18.f),
                                             ToneValue(70.f,60.f),
                                             ToneValue(128.f,104.f),
                                             ToneValue(162.f,128.f),
                                             ToneValue(178.f,148.f),
                                             ToneValue(224.f,212.f),
                                             ToneValue(255.f,255.f)];
        
        tcFilter.blueControlPoints = @[ToneValue(0.f,0.f),
                                            ToneValue(20.f,0.f),
                                            ToneValue(62.f,42.f),
                                            ToneValue(104.f,80.f),
                                            ToneValue(144.f,124.f),
                                            ToneValue(182.f,170.f),
                                            ToneValue(210.f,220.f),
                                            ToneValue(230.f,255.f),
                                            ToneValue(255.f,255.f)];
    }
    
    return @[tcFilter];

}

@end
