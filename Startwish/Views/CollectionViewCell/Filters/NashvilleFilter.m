//
//  NashvilleFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 11/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import "NashvilleFilter.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"

@interface NashvilleFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *hFilter;
@property (nonatomic, strong) GPUImageLookupFilter *luFilter;
@property (nonatomic, strong) GPUImageHueFilter *hueFilter;

@property (nonatomic) CGFloat blueColor;
@property (nonatomic) CGFloat blueColor2;
@property (nonatomic) CGFloat greenColor;
@property (nonatomic) CGFloat redColor;
@property (nonatomic) CGFloat intensity;
@property (nonatomic) CGFloat hue;

@end

@implementation NashvilleFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_apricot];
        self.type = @"Apricot";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.hFilter    = [[GPUImageLevelsFilter alloc] init];
        self.luFilter   = [[GPUImageLookupFilter alloc] init];
        self.hueFilter  = [[GPUImageHueFilter alloc] init];
        
        [self.hFilter addTarget:self.luFilter];
        [self.luFilter addTarget:self.hueFilter];
        
        [self.group setInitialFilters:@[self.hFilter]];
        [self.group setTerminalFilter:self.hueFilter];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.blueColor  = 70.f/255.f;
    self.blueColor2 = 0.21f;
    self.greenColor = 0.09;
    self.redColor   = -0.07f;
    self.intensity  = 0.1f;
    self.hue        = 8;
}

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    [self.hFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    
    [self.hFilter setBlueMin:_blueColor*self.portion gamma:1.f max:1.f minOut:_blueColor*self.portion maxOut:1.f];
    self.luFilter.intensity = _intensity*self.portion;
    self.hueFilter.hue = _hue*self.portion;
    
    [self.hFilter setRedMin:_redColor*self.portion gamma:1.f max:1.f minOut:_redColor*self.portion maxOut:1.f];
    [self.hFilter setGreenMin:_greenColor*self.portion gamma:1.f max:1.f minOut:_greenColor*self.portion maxOut:1.f];
    [self.hFilter setBlueMin:_blueColor2*self.portion gamma:1.f max:1.f minOut:_blueColor2*self.portion maxOut:1.f];
    
    [self.group useNextFrameForImageCapture];
    
    [self.stillImageSource processImage];
    [self.lookupImageSource processImage];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        [self.lookupImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        self.lookupImageSource  = [[GPUImagePicture alloc] initWithImage:[UIImage h_imageWithColor:[UIColor colorWithRed:246/255.f green:221/255.f blue:173/255.f alpha:1.f] size:image.size]];
        
        [self.stillImageSource addTarget:self.group];
        [self.lookupImageSource addTarget:self.luFilter];
        
        self.inputImage = image;
    }
}

@end
