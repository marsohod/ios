//
//  FilterCell.m
//  Startwish
//
//  Created by Pavel Makukha on 25/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "FilterCell.h"


@interface FilterCell ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@end

@implementation FilterCell

- (void)setCell:(PhotoEditTool *)cell
{
    _bgView.layer.cornerRadius = self.frame.size.height / 2 - 1;
    _bgView.layer.masksToBounds = YES;
    
    if ( cell.bgColor ) {
        [_bgView setBackgroundColor: cell.bgColor];
    }
    
    _icon.image = cell.icon;
    _bgView.layer.borderWidth = cell.borderWidth;
    
    if ( cell.borderWidth != 0.0 ) {
        _bgView.layer.borderColor = cell.borderColor.CGColor;
    }
}

@end
