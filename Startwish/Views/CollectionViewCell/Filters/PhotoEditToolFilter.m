//
//  PhotoEditToolFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PhotoEditToolFilter.h"


@interface PhotoEditToolFilter ()
{
    CGFloat lastValue;
}

@end

@implementation PhotoEditToolFilter

- (instancetype)init
{
    self = [super init];
    
    self.minValue = 0.f;
    self.maxValue = 100.f;
    
    self.sliderMaxValue = 100.f;
    self.sliderMinValue = 0.f;
    
    self.value = self.maxValue;
    lastValue = self.value;
    
    return self;
}

- (NSArray *)GPUFilters
{
    return nil;
}

- (UIImage *)applyFilterToImage:(GPUImagePicture *)image withParams:(NSDictionary *)params
{
    return nil;
}

- (CGFloat)sliderValueFromActualValue:(CGFloat)actualValue withMaxSliderValue:(CGFloat )maxChangeValue
{
    return self.value;
}

- (void)saveValue
{
    lastValue = self.value;
}

- (CGFloat)actualValue
{
    return lastValue;
}

- (CGFloat)portion
{
    return self.value/self.maxValue;
}

@end
