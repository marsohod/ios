//
//  YellowFilter.m
//  Startwish
//
//  Created by Pavel Makukha on 05/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//
// Instruction "How make filter" http://mashable.com/2013/10/20/photoshop-instagram-filters/
//


#import "YellowFilter.h"
#import "GPUImage.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface YellowFilter()

@property (nonatomic, strong) UIImage *inputImage;
@property (nonatomic, strong) GPUImagePicture *stillImageSource;
@property (nonatomic, strong) GPUImagePicture *lookupImageSource;
@property (nonatomic, strong) GPUImageFilterGroup *group;
@property (nonatomic, strong) GPUImageLevelsFilter *lFilter;
@property (nonatomic, strong) GPUImageLookupFilter *luFilter;
@property (nonatomic, strong) GPUImageToneCurveFilter *tcFilter;
@property (nonatomic, strong) GPUImageVignetteFilter *vFilter;
@property (nonatomic, strong) GPUImageSaturationFilter *sFilter;

@property (nonatomic) CGFloat gammaMax;
@property (nonatomic) CGFloat gammaMin;
@property (nonatomic) CGFloat intensity;
@property (nonatomic) CGFloat tone;
@property (nonatomic) CGFloat vstart;
@property (nonatomic) CGFloat vend;
@property (nonatomic) CGFloat saturation;
@property (nonatomic) CGFloat saturationNormal;

@end


@implementation YellowFilter

- (instancetype)init
{
    if ( self = [super init] ) {
        self.bgColor = [UIColor h_yellow];
        self.type = @"Yellow";
        
        self.group      = [[GPUImageFilterGroup alloc] init];
        self.lFilter    = [[GPUImageLevelsFilter alloc] init];
        self.luFilter   = [[GPUImageLookupFilter alloc] init];
        self.tcFilter   = [[GPUImageToneCurveFilter alloc] init];
        self.vFilter    = [[GPUImageVignetteFilter alloc] init];
        self.sFilter    = [[GPUImageSaturationFilter alloc] init];
        
        [self.lFilter addTarget:self.luFilter];
        [self.luFilter addTarget:self.tcFilter];
        [self.tcFilter addTarget:self.vFilter];
        [self.vFilter addTarget:self.sFilter];
        
        [self.group setInitialFilters:@[self.lFilter]];
        [self.group setTerminalFilter:self.sFilter];
        
        [self setDefaultMaxValue];
    }
    
    return self;
}

- (void)setDefaultMaxValue
{
    self.gammaMin   = 0.85f;
    self.gammaMax   = 1.f;
    self.intensity  = 0.15f;
    self.tone       = 45.0/255.f;
    self.vstart     = 0.4f;
    self.vend       = 1.f;
    self.saturation = 0.92f; // change from 0 to 2. In Photoshop it change from -100 to 100;
    self.saturationNormal = 1.f;
}

#pragma mark - Brannan

- (UIImage *)applyFilterToImage:(UIImage *)image withParams:(NSDictionary *)params
{
    CGFloat processingPercent = [params[@"fullSize"] boolValue] ? 1.f : 0.5f;
    
    [self cacheInputImageIfNeed:image];
    
    [self.lFilter forceProcessingAtSize:CGSizeMake(image.size.width * processingPercent, image.size.height * processingPercent)];
    [self.lFilter setMin:0.f gamma:self.gammaMax - (self.gammaMax-self.gammaMin) * self.portion max:1.f minOut:0.f maxOut:1.f];
    self.luFilter.intensity = self.intensity*self.portion;
    self.tcFilter.rgbCompositeControlPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:CGPointMake(0.0, self.tone*self.portion)], [NSValue valueWithCGPoint:CGPointMake(1.f, 1.f)], nil];
    
    self.vFilter.vignetteStart = self.vstart*self.portion;
    self.vFilter.vignetteEnd = self.vend*self.maxValue/self.value;
    
    self.sFilter.saturation = self.saturationNormal - (self.saturationNormal-self.saturation) * self.portion;
    
    [self.group useNextFrameForImageCapture];
    [self.lookupImageSource processImage];
    [self.stillImageSource processImage];
    
    return [self.group imageFromCurrentFramebufferWithOrientation:image.imageOrientation];
}

- (void)cacheInputImageIfNeed:(UIImage *)image
{
    if ( self.inputImage != image || self.stillImageSource == nil ) {
        [self.stillImageSource removeAllTargets];
        [self.lookupImageSource removeAllTargets];
        
        self.stillImageSource   = [[GPUImagePicture alloc] initWithImage:image];
        self.lookupImageSource  = [[GPUImagePicture alloc] initWithImage:[UIImage h_imageWithColor:[UIColor colorWithRed:237/255.f green:221/255.f blue:158/255.f alpha:0.7f] size:image.size]];
        
        [self.stillImageSource addTarget:self.group];
        [self.lookupImageSource addTarget:self.luFilter];
        
        self.inputImage = image;
    }
}


@end
