//
//  ProfileInfoCellFirst.h
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "FillCircleButton.h"


@interface ProfileInfoCellFirst : UICollectionViewCell

@property (weak, nonatomic) IBOutlet FillCircleButton *button;

@end
