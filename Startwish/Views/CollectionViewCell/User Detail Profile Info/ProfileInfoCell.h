//
//  ProfileInfoCell.h
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface ProfileInfoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *count;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *border;

- (void)setCellItemCount:(NSInteger)count name:(NSString *)name;
- (void)setCellItemCount:(NSInteger)c name:(NSString *)name type:(NSInteger)type;

@end
