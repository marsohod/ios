//
//  ProfileInfoCellFirst.m
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ProfileInfoCellFirst.h"
#import "UIColor+Helper.h"
#import "Routing.h"


@implementation ProfileInfoCellFirst

- (void)awakeFromNib
{
    [_button addIcon: [[UIImage imageNamed:@"link_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [_button setBackgroundColor: [UIColor h_bg_profile_circle]];
    _button.userInteractionEnabled = NO;
//    [_button addTarget:self action:@selector(showContacts:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)showContacts:(id)sender
{
}

@end
