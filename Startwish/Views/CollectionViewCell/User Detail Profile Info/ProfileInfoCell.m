//
//  ProfileInfoCell.m
//  Startwish
//
//  Created by marsohod on 11/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ProfileInfoCell.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"


@implementation ProfileInfoCell

- (void)awakeFromNib
{
//    [self setBackgroundColor: [UIColor blackColor]];
    [_count setFont: [UIFont fontWithName:@"Avenir" size: 20.0]];
//    [_count setTextColor: [UIColor h_smoothYellow]];
    [_count setTextAlignment: NSTextAlignmentCenter];
    [_count setLineBreakMode: NSLineBreakByWordWrapping];
    
    [_name setFont: [UIFont h_defaultFontSize: 8.0]];
//    [_name setTextColor: [UIColor h_smoothYellow]];
    [_name setTextAlignment: NSTextAlignmentCenter];
    [_name setNumberOfLines: 0];
    [_name setLineBreakMode: NSLineBreakByWordWrapping];
    
    [_border setBackgroundColor: [UIColor h_smoothYellow]];
    
}

- (void)setCellItemCount:(NSInteger)c name:(NSString *)name
{
    [self setCellItemCount:c name:name type:0];
}

- (void)setCellItemCount:(NSInteger)c name:(NSString *)name type:(NSInteger)type
{
    UIColor *color = type == 0 ? [UIColor h_smoothYellow] : [UIColor whiteColor];
    _count.text = [NSString stringWithFormat:@"%lu", (long)c];
    _name.text = name;
    
    [_count setTextColor: color];
    [_name setTextColor: color];
    [_border setBackgroundColor: color];
}

@end
