//
//  UICollectionViewCellWish.h
//  Startwish
//
//  Created by marsohod on 24/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelWish.h"
#import "UIViewAvatar.h"
#import "UIImageViewWish.h"


@protocol UICollectionViewCellWishDelegate;

@interface UICollectionViewCellWish : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageViewWish *imageCover;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIViewAvatar *viewImageAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelRewishCount;
@property (weak, nonatomic) IBOutlet UILabel *labelCommentsCount;
@property (weak, nonatomic) IBOutlet UIImageView *imageRewish;
@property (weak, nonatomic) IBOutlet UIImageView *imageComment;
@property (weak, nonatomic) IBOutlet UIView *viewWishInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageCoverHeight;


@property(nonatomic, weak) id<UICollectionViewCellWishDelegate> delegate;

- (void)setImageViewCover;
- (void)setCoverHeight:(CGFloat)height;
- (void)setWithItem:(ModelWish *)wish;
+ (float)cellHeightForItem:(ModelWish *)wish;

@end


@protocol UICollectionViewCellWishDelegate <NSObject>
@required

- (void)wishDetail:(UIView *)sender;
- (void)userDetail:(UIView *)sender;
- (void)reDrawCellWithHeight:(float)height;

@end
