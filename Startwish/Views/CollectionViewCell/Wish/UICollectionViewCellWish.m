//
//  UICollectionViewCellWish.m
//  Startwish
//
//  Created by marsohod on 24/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UICollectionViewCellWish.h"
#import "UIImage+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIScreen+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UILabel+typeOfText.h"
#import "ActionTexts.h"


@interface UICollectionViewCellWish()
{
    ModelWish *item;
}
@end

@implementation UICollectionViewCellWish

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self prepareImageView];
    [self prepareLabelName];
    [self prepareLabelDescription];
    [self prepareImageAvatar];
    [self prepareLabelUserName];
//    [self prepareIcons];
    self.layer.cornerRadius = 5.0f;
}



#define HORIZONTAL_CELL_PADDING 5.0
#define VERTICAL_USER_PADDING_FROM_WISH_NAME 8.0
#define FONT_SIZE_WISH_NAME 12.0
#define FONT_SIZE_WISH_DESC 11.0
#define LABEL_IMPLEMENT [[ActionTexts sharedInstance] successText:@"Executed"]

- (void)prepareImageView
{
//    _imageCover.contentMode = UIViewContentModeTopLeft;
//    _imageCover.contentMode = UIViewContentModeScaleAspectFit;
    _imageCover.contentMode = UIViewContentModeScaleToFill;
    [_imageCover setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_imageCover addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showWishDetail:)]];
}

- (void)prepareLabelName
{
//    _labelName.enlargedSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 20);
    [_labelName setFont: [UIFont h_defaultFontBoldSize: 12.0]];
    [_labelName setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_labelName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showWishDetail:)]];
}

- (void)prepareLabelDescription
{
    [_labelDescription setFont: [UIFont h_defaultFontSize: 11.0]];
}

- (void)prepareImageAvatar
{
    [_imageAvatar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_viewImageAvatar addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserDetail:)]];
}

- (void)prepareLabelUserName
{
    [_labelUserName setFont: [UIFont h_defaultFontSize: 11.0]];
    [_labelUserName setTextColor: [UIColor h_smoothGreen]];
    [_labelUserName setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_labelUserName addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserDetail:)]];
}

- (void)prepareIcons
{
    [_imageRewish setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_imageComment setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_labelCommentsCount setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_labelRewishCount setTranslatesAutoresizingMaskIntoConstraints:NO];
}

#define cCellDependImplement [item isImplement] ? [UIColor h_smoothDeepYellow] : [UIColor h_bg_commentViewBorder]
#define cInfoCellDependImplement [item isImplement] ? [UIColor h_smoothShallowYellow] : [UIColor whiteColor]


- (void)renderImplementWish
{
    [_viewWishInfo setBackgroundColor: cInfoCellDependImplement];
    [self setBackgroundColor: cCellDependImplement];
    
    // set background for no render label every time by scroll (because we remove opacity)
    [_labelUserName setBackgroundColor:cCellDependImplement];
    [_labelDescription setBackgroundColor:cCellDependImplement];
    [_labelName setBackgroundColor:cInfoCellDependImplement];
    [_labelCommentsCount setBackgroundColor:cInfoCellDependImplement];
    [_labelRewishCount setBackgroundColor:cInfoCellDependImplement];
}

- (void)setWithItem:(ModelWish *)wish
{
    item = wish;
    
    [self setImageViewCover];
    [self labelWishNameWithWish];
    [self labelDescriptionWithWish];
    [self imageViewAvatarWithWish];
    [self labelOwnerFromWish];
    [self iconsWithWish];
    [self renderImplementWish];
}

#pragma mark -
#pragma mark imageViewCover

- (void)setImageViewCover
{
    float scale = [UICollectionViewCellWish scaleCoverWish:item];
    _imageCover.backgroundColor = [UIColor h_randomWishBackgroundColor];
    _imageCover.image = nil;

    [self setCoverHeight:[UIScreen h_getPinWidthDependsDevice] / scale];

    // Если вдруг нет обложки
    if ( [item hasExecutedImageWithSizes] ) {
        [self downloadCoverImageFromURL:[NSURL URLWithString:[item getCoverExecuted]] withScale:scale];
        return;
    }
    
    if( ![[item getCover] isEqualToString:@""] ) {
        [self downloadCoverImageFromURL:[NSURL URLWithString:[item getCover]] withScale:scale];
    }
}

- (void)downloadCoverImageFromURL:(NSURL *)url withScale:(float)scale
{
//    [[SDImageCache sharedImageCache] clearMemory];
//    [[SDImageCache sharedImageCache] clearDisk];

    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:url.absoluteString done:^(UIImage *image, SDImageCacheType cacheType) {
        NSString *cover = [item hasExecutedImageWithSizes] ? [item getCoverExecuted] : [item getCover];

        if( image != nil ) {
            if ( [url.absoluteString isEqualToString:cover] ) {
                _imageCover.image = image;
                [_imageCover setBackgroundColor: [UIColor clearColor]];
            }
        } else {
            [_imageCover sd_cancelCurrentAnimationImagesLoad];
            [_imageCover sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                NSString *newcover = [item hasExecutedImageWithSizes] ? [item getCoverExecuted] : [item getCover];
                
                if( image != nil ) {
                    if ( [url.absoluteString isEqualToString:newcover] ) {
                        [_imageCover animateAppearanceImage:image];
                        [_imageCover setBackgroundColor: [UIColor clearColor]];
                    }
                }
            }];
        }
    }];
}

- (void)showWishDetail:(UITapGestureRecognizer *)sender
{
    [self.delegate wishDetail:sender.view];
}

- (void)setCoverHeight:(CGFloat)height
{
    if ( !isnan(height) ) {
        _imageCoverHeight.constant = height;
    }
}



#pragma mark -
#pragma mark labelWishName

- (void)labelWishNameWithWish
{

    if ( [item isImplement] ) {
        NSMutableAttributedString *name = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@", LABEL_IMPLEMENT, [item getName]]];
        [name setAttributes:@{NSForegroundColorAttributeName: [UIColor h_orange2]} range:NSMakeRange(0, LABEL_IMPLEMENT.length)];
        _labelName.attributedText = name;
    } else {
        _labelName.text = [item getName];
    }
}


#pragma mark -
#pragma mark labelDescription

- (void)labelDescriptionWithWish
{
    _labelDescription.text = [item getDescription];
    [_labelDescription sizeToFit];
}


#pragma mark -
#pragma mark imageAvatar

- (void)imageViewAvatarWithWish
{
    NSURL *url = [NSURL URLWithString:[[item getOwner] getAvatar]];
    [_viewImageAvatar customDrawView];
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:url.absoluteString done:^(UIImage *image, SDImageCacheType cacheType) {
        
        if( image != nil ) {
            if ( [url.absoluteString isEqualToString:[[item getOwner] getAvatar]] ) {
                [_viewImageAvatar setImage:image];
            }
        } else {
            [UIImage downloadWithUrl:url withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [_viewImageAvatar animateAppearanceImage:image];
                }
            }];
        }
    }];
}

- (IBAction)showUserDetail:(UITapGestureRecognizer *)sender
{
    [self.delegate userDetail:sender.view];
}


#pragma mark -
#pragma mark labelOwnerWish

-(void)labelOwnerFromWish
{
    _labelUserName.text = [UICollectionViewCellWish fioFormatted: [[item getOwner] getFullname]];
}

+ (NSString *)fioFormatted:(NSString *)fioString
{
    NSArray *fio = [fioString componentsSeparatedByString:@" "];
    
    if ( [fio count] == 1 ) {
        return fio[0];
    }
    
    if ( [fio count] > 1 ) {
        // 1 - пробел, который мы не пишем
        return [NSString stringWithFormat:@"%@\r%@", fio[0], [fioString substringWithRange:NSMakeRange([fio[0] length] + 1, [fioString length] - [fio[0] length] - 1)]];
    }
    
    return nil;
}

-(void)labelOwnerTap:(UITapGestureRecognizer *)sender {
    [self.delegate userDetail:sender.view];
}


#pragma mark -
#pragma mark setIcons

- (void)iconsWithWish
{
//    if ( [item getRewishesCount] > 0 ) {
        _labelRewishCount.text = [NSString stringWithFormat:@"%ld", (long)[item getRewishesCount]];
        _labelRewishCount.hidden = NO;
        _imageRewish.hidden = NO;
//    } else {
//        _labelRewishCount.hidden = YES;
//        _imageRewish.hidden = YES;
//    }
    
    if ( [item getCommentsCount] > 0 ) {
        _labelCommentsCount.text = [NSString stringWithFormat:@"%ld", (long)[item getCommentsCount]];
        _labelCommentsCount.hidden = NO;
        _imageComment.hidden = NO;
    } else {
        _labelCommentsCount.hidden = YES;
        _imageComment.hidden = YES;
    }
    
    [_labelRewishCount sizeToFit];
    [_labelCommentsCount sizeToFit];
    
//    NSLog(@"%f %f", self.viewWishInfo.frame.size.height, _labelName.frame.size.height);
    
}


- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    return layoutAttributes;
}


#define VIEW_WITH_NAME_AND_ICONS_HEIGHT 43.0

+ (float)scaleCoverWish:(ModelWish *)wish
{
    if ( [wish hasExecutedImageWithSizes] ) {
        return [wish getCoverExecutedWidth] / [wish getCoverExecutedHeight];
    }
    
    return [wish getCoverWidth] / [wish getCoverHeight];
}

+ (float)cellHeightForItem:(ModelWish *)wish
{
    float scale = [self scaleCoverWish:wish];
    // Отступ слева и справа от общего View
    float paddingToView = 18.0;
    // Максимальная высота для текста
    float maxHeight = 300.0;
    float imageHeight = [UIScreen h_getPinWidthDependsDevice] / scale;
    
    CGSize nameSize = [UILabel rectSizeString: [wish isImplement] ? [NSString stringWithFormat:@"%@%@", LABEL_IMPLEMENT, [wish getName]] : [wish getName]
                                      byWidth: [UIScreen h_getPinWidthDependsDevice] - paddingToView
                                    maxHeight: maxHeight
                                     fontName: [UIFont h_defaultFontNameBold]
                                     fontSize: 12.0];

    CGSize descriptionSize = [UILabel rectSizeString: [wish getDescription]
                                             byWidth: [UIScreen h_getPinWidthDependsDevice] - paddingToView
                                           maxHeight: maxHeight
                                            fontName: [UIFont h_defaultFontName]
                                            fontSize: 11.0];
    
    return
    imageHeight +                                               // Высота картинки
    VIEW_WITH_NAME_AND_ICONS_HEIGHT +                           // Высота белого View минус высота названия
    nameSize.height +                                           // Высота названия
    ([[wish getDescription] isEqualToString:@""] ? -3.0 : 10) +  // Отступ между белым вью и описанием
    ([[wish getDescription] isEqualToString:@""] ? 0.0 : descriptionSize.height) +  // Высота описания
    17 +                                                        // Отступ аватарки сверху + снизу
    [UIViewAvatar avatarViewSize:0].height; // Высота аватарки
}



@end
