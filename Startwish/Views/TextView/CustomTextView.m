//
//  CustomTextView.m
//  Startwish
//
//  Created by marsohod on 29/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomTextView.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"


@implementation CustomTextView

#define paddingEdge 15.0

- (void)awakeFromNib
{
    [self setBackgroundColor: [UIColor clearColor]];
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.0;
    [self setTextColor: [UIColor whiteColor]];
    [self setFont: [UIFont h_defaultFontSize: 21.0]];
    self.textContainerInset = UIEdgeInsetsMake(paddingEdge, paddingEdge, paddingEdge, paddingEdge);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
