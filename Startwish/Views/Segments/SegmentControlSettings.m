//
//  SegmentControlSettings.m
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "SegmentControlSettings.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@implementation SegmentControlSettings

- (void)awakeFromNib
{
    UIImage *imageSelected = [UIImage imageNamed:@"segment_bg_yellow.png"];
    UIImage *imageDefault = [UIImage h_imageWithColor:[UIColor clearColor] size:CGSizeMake(1.f, 1.f)];
    
    // Настраиваем неактивный сегмент
    [self setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor h_smoothYellow], NSFontAttributeName: [UIFont systemFontOfSize:15.0]} forState:UIControlStateNormal];
    
    // Настраиваем активный сегмент
    [self setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor h_smoothDeepGreen], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0]} forState:UIControlStateSelected];
    
    // Ставим фон у неактивного сегмента
    [self setBackgroundImage: [imageDefault scaleToSize:CGSizeMake(self.frame.size.width / 2, self.frame.size.height)]
                               forState:UIControlStateNormal
                             barMetrics:UIBarMetricsDefault];
    
    self.layer.borderColor = [UIColor h_smoothYellow].CGColor;
    self.layer.borderWidth = 1.0f;
    
    // Ставим фон у активного сегмента
    [self setBackgroundImage: [imageSelected scaleToSize:CGSizeMake(self.frame.size.width / 2, self.frame.size.height)]
                               forState:UIControlStateSelected
                             barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между активным и неактивным сегментом
    [self setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, self.frame.size.height)] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между неактивным и активным сегментом
    [self setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, self.frame.size.height)] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
