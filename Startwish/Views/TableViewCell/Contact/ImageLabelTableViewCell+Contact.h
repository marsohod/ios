//
//  ImageLabelTableViewCell+Contact.h
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImageLabelTableViewCell.h"


@interface ImageLabelTableViewCell (Contact)

- (void)setContactCellName:(NSString *)name value:(NSString *)value duringEdit:(BOOL)duringEdit byIndex:(NSInteger)index;

@end
