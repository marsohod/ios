//
//  ImageLabelTableViewCell+Contact.m
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImageLabelTableViewCell+Contact.h"
#import "UIColor+Helper.h"


@implementation ImageLabelTableViewCell (Contact)

- (void)setContactCellName:(NSString *)name value:(NSString *)value duringEdit:(BOOL)duringEdit byIndex:(NSInteger)index
{
    self.title.hidden = duringEdit;
    self.textFieldEdit.hidden = !duringEdit;
    
    if( ![name isEqualToString:@""] ) {
        self.title.text = [NSString stringWithFormat:@"%@", value];
        self.textFieldEdit.text = [NSString stringWithFormat:@"%@", value];
        self.cellIndex = index;
        [self.image setBackgroundColor: [UIColor h_randomWishBackgroundColor]];
        self.image.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@.png", name]];
    }
}

@end
