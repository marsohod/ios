//
//  CommentTableViewCell.h
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "ModelComment.h"
#import "UIViewAvatar.h"


@protocol CommentDelegate;
@interface CommentTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UIViewAvatar *viewImageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelUserComment;
@property (weak, nonatomic) IBOutlet UILabel *labelUserCommentDate;
@property (weak, nonatomic) IBOutlet UILabel *labelLoadMore;
@property (nonatomic, weak) id<CommentDelegate> buttonDelegate;

- (void)setRightButtons;
- (void)renderLoadMore:(NSInteger)count;
- (void)setCellFromComment:(ModelComment *)comment;
- (void)implementWish:(BOOL)isImplement;
+ (float)cellHeightForComment:(ModelComment *)comment;
+ (float)cellHeightForLoadingMoreComment;

@end

@protocol CommentDelegate <NSObject>
@required
- (void)replyUserFromComment:(ModelComment *)comment;

@optional
- (void)removeComment:(ModelComment *)comment;
- (void)showUserDetail:(ModelUser *)user;
@end
