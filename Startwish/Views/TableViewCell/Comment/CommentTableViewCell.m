//
//  CommentTableViewCell.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CommentTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIScreen+Helper.h"
#import "UIFont+Helper.h"
#import "NSString+Helper.h"
#import "UILabel+typeOfText.h"
#import "MGSwipeButton.h"


@interface CommentTableViewCell ()
{
    ModelComment *item;
}
@end

@implementation CommentTableViewCell

#define COMMENT_FONT_SIZE 13.0
#define paddingCommentToViewRightSide 19.0
#define paddingCommentToViewLeftSide 67.0
#define swipeBGColor [UIColor h_bg_commentView]
- (void)awakeFromNib {    
    [self.labelUserName setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];
    [self.labelUserComment setFont: [UIFont h_defaultFontSize: COMMENT_FONT_SIZE]];
    [self.labelUserCommentDate setFont: [UIFont h_defaultFontSize: 12.0]];
    
    [self.labelUserComment setNumberOfLines: 0];
    [self.labelUserComment setLineBreakMode: NSLineBreakByWordWrapping];
    
    [self.labelUserName setTextColor: [UIColor h_smoothGreen]];
    [self.labelUserName setUserInteractionEnabled: YES];
    
    [self.labelUserCommentDate setTextColor: [UIColor h_gray]];
    [self.labelUserComment setLineBreakMode: NSLineBreakByTruncatingTail];
    
    self.labelLoadMore.hidden = YES;
    [self.labelLoadMore setTextColor:[UIColor h_orange2]];
    [self.labelLoadMore setFont: [UIFont h_defaultFontBoldSize: 13.0]];
    [self.labelLoadMore setTextAlignment:NSTextAlignmentLeft];
    
    [_viewImageAvatar addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showUserDetail:)]];
    [self addGestureRecognizer: [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(replyToComment:)]];
}

- (void)drawRect:(CGRect)rect
{
    float labelNameWidth = [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide) - self.labelUserCommentDate.frame.size.width - 5.0;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
    
    // ставим ширину имени пользователя во всю оставшуюся длину
    self.labelUserName.frame = CGRectMake(self.labelUserName.frame.origin.x, self.labelUserName.frame.origin.y, labelNameWidth , self.labelUserName.frame.size.height);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

// Добавляем представление выбранной ячейки
- (void)initSelectedView
{
    // Полоска бордера
    UIView *borderTop = [[UIView alloc] initWithFrame: CGRectMake(0.0, 0.0, self.frame.size.width, 1.0)];
    UIView *borderBottom = [[UIView alloc] initWithFrame: CGRectMake(0.0, self.frame.size.height + 0.5, self.frame.size.width, 1)];
    
    [borderTop setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    [borderBottom setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    
    UIView *bgColorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [bgColorView setBackgroundColor: [UIColor h_bg_commentView]];
    [bgColorView addSubview: borderTop];
    [bgColorView addSubview: borderBottom];
    
    [self setSelectedBackgroundView: bgColorView];
}


//configure right buttons
- (void)setRightButtons
{
    if( [item isMyComment] ) {
        self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"trash_white"] backgroundColor: [UIColor h_red]]];
    } else {
        self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"reply_white"] backgroundColor: [UIColor h_smoothGreen]]];
    }
    
    

//    [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"ghost_white"] backgroundColor: [UIColor blackColor]],
    
    self.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    self.rightSwipeSettings.buttonMargin = 5.0;
    self.swipeBackgroundColor = swipeBGColor;
    self.swipeBackgroundColorDefault = [UIColor whiteColor];
    self.delegate = (id<MGSwipeTableCellDelegate>)self;
}


- (void)setCellFromComment:(ModelComment *)comment
{
    item = comment;
    
    [self renderLoadMore:0];
    self.labelUserName.text = [[item getUser] getFullname];
    self.labelUserComment.text = [item getText];
    self.labelUserCommentDate.text = [NSString h_timeAgoUserFriendlyFromStringDate:[item getDate]];
    [self.labelUserCommentDate sizeToFit];
    
    [self downloadAvatar];
    [self initSelectedView];
}

- (void)downloadAvatar
{
    [_viewImageAvatar customDrawView];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[[item getUser] getAvatar] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            [_viewImageAvatar setImage:image];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: [[item getUser] getAvatar] ] withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [_viewImageAvatar animateAppearanceImage:image];
                }
            }];
            
        }
    }];
}

- (void)renderLoadMore:(NSInteger)count
{
    if( count > 0 ) {
        self.labelLoadMore.text = [NSString stringWithFormat:@"Ещё %ld %@", (long)count, [NSString h_russianNumeralFromInteger: count ends:@[@"комментарий", @"комментария", @"комментариев"]]];
    }
    
    self.labelLoadMore.hidden = !(count > 0);
    self.labelUserComment.hidden = (count > 0);
    self.labelUserCommentDate.hidden = (count > 0);
    self.labelUserName.hidden = (count > 0);
    self.viewImageAvatar.hidden = (count > 0);
}

- (void)implementWish:(BOOL)isImplement
{
    UIColor *color = isImplement ? [UIColor h_smoothYellow] : [UIColor whiteColor];
    
    [self setBackgroundColor: color];
    [_labelUserComment setBackgroundColor: color];
    [_labelUserName setBackgroundColor: color];
    [_labelUserCommentDate setBackgroundColor: color];
    [_labelLoadMore setBackgroundColor: color];
    self.swipeBackgroundColorDefault = color;
    self.swipeBackgroundColor = isImplement ? [UIColor h_smoothYellow] : swipeBGColor;
}


#pragma mark -
#pragma mark Cell Height

+ (float)cellHeightForComment:(ModelComment *)comment
{
    float height = 0;
    float maxHeightComment = 100.0;
    
    CGSize expectedCommentSize;
    
    if ( comment != nil ) {
        expectedCommentSize = [UILabel rectSizeString: [comment getText]
                                              byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                            maxHeight: maxHeightComment
                                             fontName: [UIFont h_defaultFontName]
                                             fontSize: COMMENT_FONT_SIZE];
        
        height =    expectedCommentSize.height +
                    16.0 +      // высота имени пользователя
                    6.0 +       // отступ фио от коммента
                    22.0;       // отступы сверху снизу
    } else {
        height = 20.0;
    }
    
    return height;
}

+ (float)cellHeightForLoadingMoreComment
{
    return 48.0;
}

#pragma mark -
#pragma mark MGSwipeTableCellDelegate

- (BOOL)swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    if( [item isMyComment] ) {
        if ( index == 0 ) {
            [self.buttonDelegate removeComment:item];
        }
    } else {
        if ( index == 0 ) {
            [self.buttonDelegate replyUserFromComment:item];
        }
    }
    
    return YES;
}


- (void)showUserDetail:(UITapGestureRecognizer *)sender
{
    [self.buttonDelegate showUserDetail:[item getUser]];
}
     
 - (void)replyToComment:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded) {
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        [self.buttonDelegate replyUserFromComment:item];
    }
    
}
@end
