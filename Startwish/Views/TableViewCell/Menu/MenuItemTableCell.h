//
//  MenuItemTableCell.h
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TableViewCell.h"


@interface MenuItemTableCell : TableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *count;

@end
