//
//  MenuItemTableCell.m
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "MenuItemTableCell.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UITableViewCell+Helper.h"


@implementation MenuItemTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.count.font = [UIFont h_defaultFontSize: 16.0];
    self.count.layer.masksToBounds = YES;
    self.count.layer.cornerRadius = self.count.frame.size.width / 2;
    self.count.frame = CGRectZero;
    
    [self.name setFont: [UIFont h_defaultFontSize: 16.0]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    [self setMenuItem: item];
}

- (void)setMenuItem:(id)menuItem
{
    item = (NSDictionary *)menuItem;
    
    if ( item[@"icon"] ) {
        
        self.icon.image = [UIImage imageNamed: [NSString stringWithFormat:@"%@_%@.png", item[@"icon"], self.isSelected ? @"white" : @"yellow"]];
        
        self.icon.frame = CGRectMake(self.icon.frame.origin.x, self.icon.frame.origin.y, self.icon.image.size.width, self.icon.image.size.height);
    }
    
    if ( item[@"title"] ) {
        self.name.text = item[@"title"];
        [self.name setTextColor: (self.isSelected ? [UIColor whiteColor] : [UIColor h_smoothYellow])];
    }
    
    self.count.alpha = 0.0;
    
    if( item[@"count"] && [item[@"count"] integerValue] != 0 ) {
        self.count.text = [NSString stringWithFormat:@"%@", item[@"count"]];
        [self.count setBackgroundColor: ([item[@"countColor"] isEqualToString: @"yellow"] ? [UIColor h_yellow] : [UIColor h_orange2])];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.count.alpha = 1.0;
        } completion:nil];
    }
    
    [self.count setTextColor: (self.isSelected ? [UIColor whiteColor] : [UIColor h_smoothYellow])];
}

- (void)drawRect:(CGRect)rect
{
    [self h_setSeparatorInsetZero];
}

@end
