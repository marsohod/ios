//
//  TableViewCellPeople.h
//  Startwish
//
//  Created by marsohod on 23/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UserExtend.h"


@interface TableViewCellPeople : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UIButton *buttonRelationship;
- (IBAction)buttonRelationshipAction:(UIButton *)sender;

+(float)cellHeight;
-(void)cellWithUser:(UserExtend *)user;

@end
