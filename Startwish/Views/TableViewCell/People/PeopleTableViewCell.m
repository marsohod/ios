//
//  PeopleTableViewCell.m
//  Startwish
//
//  Created by marsohod on 24/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "PeopleTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UILabel+typeOfText.h"
#import "ActionTexts.h"


@interface PeopleTableViewCell()
{
    ModelUser *item;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerYbuttonSubscribe;
@end

@implementation PeopleTableViewCell

#define DESCRIPTION_FONT_SIZE 13.0
#define NAME_FONT_SIZE 16.0
#define paddingCommentToViewRightSide 19.0
#define paddingCommentToViewLeftSide 58.0
#define paddingDescToRightView 5.0

- (void)awakeFromNib {
    [_labelUserName setFont: [UIFont h_defaultFontSize: NAME_FONT_SIZE]];
    [_labelUserDescription setFont: [UIFont h_defaultFontSize: DESCRIPTION_FONT_SIZE]];
    [_labelUserMutualFriends setFont: [UIFont h_defaultFontSize: DESCRIPTION_FONT_SIZE]];
    
    [_labelUserDescription setNumberOfLines: 0];
    [_labelUserDescription setLineBreakMode: NSLineBreakByWordWrapping];
    
    [_labelUserName setTextColor: [UIColor h_smoothGreen]];
    
    _buttonSubscribe.hidden = YES;
    _buttonSubscribe.titleLabel.font = [UIFont h_defaultFontMediumSize:10.0];
    _buttonSubscribe.layer.cornerRadius = 5.0;
    _buttonSubscribe.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8);
    _buttonSubscribe.layer.borderColor = [UIColor h_greenDirty].CGColor;
    _buttonSubscribe.layer.borderWidth = 1.f;
    _buttonSubscribe.clipsToBounds = YES;
    
    self.clipsToBounds = YES;
    
    [self drawButtonSubscribe];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
//    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    [self initSelectedView];
}

- (void)drawRect:(CGRect)rect
{
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}


// Добавляем представление выбранной ячейки
- (void)initSelectedView
{
    // Полоска бордера
    UIView *borderTop = [[UIView alloc] initWithFrame: CGRectMake(0.0, 0.0, self.frame.size.width, 1.0)];
    UIView *borderBottom = [[UIView alloc] initWithFrame: CGRectMake(0.0, self.frame.size.height + 0.5, self.frame.size.width, 1)];
    
    [borderTop setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    [borderBottom setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    
    UIView *bgColorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [bgColorView setBackgroundColor: [UIColor h_bg_commentView]];
    [bgColorView addSubview: borderTop];
    [bgColorView addSubview: borderBottom];
    
    [self setSelectedBackgroundView: bgColorView];
}


- (IBAction)actionButtonSubscribe:(id)sender
{
    if ( [item isSubscriber] ) {
        [self.cellDelegate unsubscribeFromUser:[item getId]];
        [item setSubscribed:NO];
        [self drawButtonSubscribe];
        return;
    }
    
    [self.cellDelegate subscribeToUser:[item getId]];
    [item setSubscribed:YES];
    [self drawButtonSubscribe];
}

- (void)cellWithUser:(ModelUser *)user searchRange:(NSRange)range
{
    item = user;
    
    self.labelUserDescription.text = [item getDescription];
    
    // Проверяем есть ли общие друзья

    if( [item getCountFriendsMutual] > 0 ) {
        self.imageViewMutualFriends.hidden = NO;
        self.labelUserMutualFriends.hidden = NO;
        self.labelUserMutualFriends.text = [NSString stringWithFormat:@"%lu", (unsigned long)[item getCountFriendsMutual]];
    } else {
        self.imageViewMutualFriends.hidden = YES;
        self.labelUserMutualFriends.hidden = YES;
        self.labelUserMutualFriends.text = @"";
    }
    
    [self.labelUserMutualFriends sizeToFit];
    
    [self.viewImageAvatar customDrawView];
    [self downloadAvatar];
    [self drawButtonSubscribe];
    [self renderLabelUserName:range];
    
    [self initSelectedView];
}

const CGFloat paddingRightBorder    = 10.0;
const CGFloat paddingRightFullname  = 7.0;

- (void)renderLabelUserName:(NSRange)range
{
    float mutualFriendsPadding;
    float fullnamePadding;
    
    // Если происходит поиск, то выделяем поисковую фразу
    // Добавляем Имя
    NSMutableAttributedString *highlightedName = [[NSMutableAttributedString alloc] initWithString: [item getFullname]];
    [highlightedName addAttributes:@{NSForegroundColorAttributeName: [UIColor h_orange2]} range:range];
    
    self.labelUserName.attributedText = highlightedName;
    
    if ( _cellType == 1 ) {
        fullnamePadding = paddingRightFullname + paddingRightFullname + self.buttonSubscribe.frame.size.width;
    } else {
        mutualFriendsPadding = (_labelUserMutualFriends.frame.size.width != 0 ? _labelUserMutualFriends.frame.size.width + paddingRightBorder + paddingRightFullname + _imageViewMutualFriends.frame.size.width : 0);
        fullnamePadding = paddingRightFullname + mutualFriendsPadding;
    }
    
    _marginRightLabelUserName.constant = fullnamePadding;

    [_labelUserName setNeedsLayout];
}

- (void)drawButtonSubscribe
{
    if ( _cellType == 1 ) {
        CGSize expectedCommentSize;
        NSString *buttonTitle = [item isSubscriber] ? [[ActionTexts sharedInstance] titleText:@"Unsubscribe"] : [[ActionTexts sharedInstance] titleText:@"Subscribe"];
        _buttonSubscribe.hidden = NO;
        
        _buttonSubscribe.titleLabel.text = buttonTitle;
        [_buttonSubscribe setTitle: buttonTitle forState:UIControlStateNormal];
        
        [_buttonSubscribe setTitleColor:[item isSubscriber] ? [UIColor whiteColor] : [UIColor h_greenDirty] forState:UIControlStateHighlighted & UIControlStateNormal & UIControlStateSelected];
        
        [_buttonSubscribe setTintColor:[item isSubscriber] ? [UIColor whiteColor] : [UIColor h_greenDirty]];
        [_buttonSubscribe setBackgroundImage:[UIImage h_imageWithColor:[item isSubscriber] ? [UIColor h_greenDirty] : [UIColor whiteColor] size:CGSizeMake(10,10)] forState:UIControlStateHighlighted & UIControlStateNormal & UIControlStateSelected];
        [_buttonSubscribe sizeToFit];
        
        _marginRightLabelUserDesc.constant = _buttonSubscribe.frame.size.width + paddingDescToRightView;
        
        if ( [item getCountFriendsMutual] > 0 ) {
            
            expectedCommentSize = [UILabel rectSizeString: [item getDescription]
                                                  byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                                maxHeight: 150.0f
                                                 fontName: [UIFont h_defaultFontName]
                                                 fontSize: DESCRIPTION_FONT_SIZE];

            if ( expectedCommentSize.height <= 15.5 ) // 15.5 magic height one string of description
            {
                _centerYbuttonSubscribe.constant = -10.f;
            }
        }
        
        [_labelUserDescription setNeedsLayout];
    }
}

- (void)downloadAvatar
{
    [_viewImageAvatar customDrawView];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[item getAvatar] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            [_viewImageAvatar setImage:image];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: [item getAvatar]] withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [_viewImageAvatar animateAppearanceImage:image];
                }
            }];
            
        }
    }];
}

+ (float)cellHeightForUser:(ModelUser *)user cellType:(NSInteger)cellType
{
    float height = 0;
    float maxHeightComment = 150.0;
    CGSize expectedCommentSize;
    
    if ( user != nil ) {
        expectedCommentSize = [UILabel rectSizeString: [user getDescription]
                                              byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                            maxHeight: maxHeightComment
                                             fontName: [UIFont h_defaultFontName]
                                             fontSize: DESCRIPTION_FONT_SIZE];

        height =
            10.0 +              // верхний отступ
            NAME_FONT_SIZE +    // высота имени пользователя
            6.0 +               // отступ фио от коммента
            (expectedCommentSize.height == 0 ? DESCRIPTION_FONT_SIZE + 3.0 : expectedCommentSize.height) +
            (cellType == 1 ? 10.0 : 0.0) +
            12.0;               // нижний отступ
    } else {
        height = 20.0;
    }
    
    return height;
}

@end
