//
//  TableViewCellPeople.m
//  Startwish
//
//  Created by marsohod on 23/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "TableViewCellPeople.h"
#import "UIImage+Helper.h"
#import "ActionTexts.h"


@implementation TableViewCellPeople

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)cellWithUser:(UserExtend *)user
{
    
    [UIImage downloadWithUrl:[NSURL URLWithString:[user getAvatar]] onCompleteChange:^(UIImage *image) {
        if( image != nil ) {
//            self.imageAvatar.image = [image scaleToSize:CGSizeMake(self.imageAvatar.frame.size.width, self.imageAvatar.frame.size.height)];
        }
    }];

//    self.imageAvatar.layer.cornerRadius = self.imageAvatar.frame.size.width / 2;
//    self.imageAvatar.layer.masksToBounds = YES;
//    self.imageAvatar.backgroundColor = [UIColor grayColor];
//    self.labelText.text = [user getFullname];
    
    if( [user getFriendshipStatus] == 1 ) {
        [self.buttonRelationship setTitle:[[ActionTexts sharedInstance] titleText:@"Remove"] forState:UIControlStateNormal];
    } else {
        [self.buttonRelationship setTitle:[[ActionTexts sharedInstance] titleText:@"Add"] forState:UIControlStateNormal];
    }
}

- (IBAction)buttonRelationshipAction:(UIButton *)sender {
//    if( user.friendship_status == 1 ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Оу" message:@"Авторизуйся" delegate:nil cancelButtonTitle:@"Okey" otherButtonTitles:nil];
    
        [alert show];
//    }
}

+(float)cellHeight
{
    return 60.0;
}

@end
