//
//  PeopleTableViewCell.h
//  Startwish
//
//  Created by marsohod on 24/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIViewAvatar.h"
#import "ModelUser.h"


@protocol PeopleTableViewCellDelegate;
@interface PeopleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIViewAvatar *viewImageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelUserDescription;
@property (weak, nonatomic) IBOutlet UILabel *labelUserMutualFriends;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMutualFriends;
@property (weak, nonatomic) IBOutlet UIButton *buttonSubscribe;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginRightLabelUserName;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *marginRightLabelUserDesc;
@property (nonatomic) NSInteger cellType;
@property (nonatomic, weak) id<PeopleTableViewCellDelegate> cellDelegate;

- (IBAction)actionButtonSubscribe:(id)sender;

- (void)cellWithUser:(ModelUser *)user searchRange:(NSRange)range;
+ (float)cellHeightForUser:(ModelUser *)user cellType:(NSInteger)cellType;

@end

@protocol PeopleTableViewCellDelegate <NSObject>

- (void)subscribeToUser:(uint32_t)userId;
- (void)unsubscribeFromUser:(uint32_t)userId;
@end
