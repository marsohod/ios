//
//  ImageLabelTableViewCell+Categories.h
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImageLabelTableViewCell.h"
#import "ModelWishCategory.h"


@interface ImageLabelTableViewCell (Categories)

- (void)setCategories:(ModelWishCategory *)category;

@end
