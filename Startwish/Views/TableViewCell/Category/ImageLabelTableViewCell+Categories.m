//
//  ImageLabelTableViewCell+Categories.m
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImageLabelTableViewCell+Categories.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@implementation ImageLabelTableViewCell (Categories)

- (void)setCategories:(ModelWishCategory *)category
{
    self.item = category;
//        [[SDImageCache sharedImageCache] clearMemory];
//        [[SDImageCache sharedImageCache] clearDisk];
    if( category != nil ) {
        self.title.text = [category getName];
        self.image.image = nil;
        [self.image setBackgroundColor:[UIColor h_randomWishBackgroundColor]];
        
        // Если есть изображение в кэше, то берём оттуда
        // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
        [[SDImageCache sharedImageCache] queryDiskCacheForKey:[category getPoster] done:^(UIImage *image, SDImageCacheType cacheType) {
            self.image.backgroundColor = [UIColor clearColor];
            
            if( image != nil ) {
                [self.image setImage:image];
            } else {
                
                [UIImage downloadWithUrl: [NSURL URLWithString:[self.item getPoster]] withIdentify:[self.item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                    if( image != nil && identify == [self.item getId] ) {
                        [self.image animateAppearanceImage:image];
                    }
                }];
                
            }
        }];        
    }
}

@end
