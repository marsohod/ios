//
//  WishlistTableViewCell.m
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishlistTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UILabel+typeOfText.h"
#import "UITableViewCell+Helper.h"
#import "MGSwipeButton.h"
#import "ActionTexts.h"


@interface WishlistTableViewCell()
{
    ModelWishlist *item;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingRightLabelName;
@end
@implementation WishlistTableViewCell

#define FONT_SIZE_NAME 16.0
#define FONT_SIZE_DESC 13.0
#define PADDING_INNER_CELL 24.0
#define PREVIEW_SIZE 50.0

- (void)awakeFromNib {
    [self renderName];
    [self renderDesc];
    [self renderItemsCount];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)renderName
{
    [_labelName setFont: [UIFont h_defaultFontSize: FONT_SIZE_NAME]];
    [_labelName setTextColor: [UIColor h_smoothGreen]];
    [_labelName setTextAlignment: NSTextAlignmentLeft];
    [_labelName setNumberOfLines: 0];
}

- (void)renderDesc
{
    [_labelDesc setFont: [UIFont h_defaultFontSize: FONT_SIZE_DESC]];
    [_labelDesc setTextColor: [UIColor blackColor]];
    [_labelDesc setTextAlignment: NSTextAlignmentLeft];
    [_labelDesc setNumberOfLines: 0];
}

- (void)renderItemsCount
{
    [_labelItemsCount setFont: [UIFont h_defaultFontSize: FONT_SIZE_NAME]];
    [_labelItemsCount setTextColor: [UIColor h_smoothGreen]];
    [_labelItemsCount setTextAlignment: NSTextAlignmentLeft];
    [_labelItemsCount setNumberOfLines: 1];
}

- (void)renderPreview:(UIImageView *)preview
{
    preview.contentMode = UIViewContentModeScaleAspectFill;
    preview.layer.cornerRadius = 5.0;
    preview.clipsToBounds = YES;
    [preview setBackgroundColor: [UIColor h_randomColor]];
    preview.hidden = NO;
}


- (void)setWithItem:(ModelWishlist *)wishlist
{
    item = wishlist;
    _labelName.text = [item getName];
    _labelItemsCount.text = [NSString stringWithFormat:@"%lu", (long)[item getWishesCount]];
    _labelDesc.text = [item getDescription];
    
    [self renderPreviewsFromWishes:[item getWishes]];
    
    switch ( [item getPrivacy] ) {
        case 2:
            _imageViewPrivateToAll.hidden = NO;
            _imageViewPrivateToAllExceptFriends.hidden = YES;
            break;
            
        case 1:
            _imageViewPrivateToAll.hidden = YES;
            _imageViewPrivateToAllExceptFriends.hidden = NO;
            break;
            
        default:
            _imageViewPrivateToAll.hidden = YES;
            _imageViewPrivateToAllExceptFriends.hidden = YES;
            break;
    }
    
    CGSize expectedItemsCountSize = [UILabel rectSizeString: [NSString stringWithFormat:@"%lu", (long)[wishlist getWishesCount]]
                                             byWidth: 29.0
                                           maxHeight: 200.f
                                            fontName: [UIFont h_defaultFontName]
                                            fontSize: FONT_SIZE_NAME];
    
    _paddingRightLabelName.constant = expectedItemsCountSize.width + PADDING_INNER_CELL + ([item getPrivacy] != 0 ? _imageViewPrivateToAllExceptFriends.frame.size.width : 0.f);
    [_labelName setNeedsLayout];
}

- (void)addPreview:(UIImageViewWish *)preview byURL:(NSString *)url
{
    // для 5/5s/5с не надо показывать последнюю превью
    if( [UIScreen mainScreen].bounds.size.width < preview.frame.origin.x ) {
        return;
    }
    
    [self renderPreview: preview];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:url done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            [preview setImage:image];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: url] withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [preview animateAppearanceImage:image];
                }
            }];
            
        }
    }];
    
//    [UIImage downloadWithUrl: url onCompleteChange:^(UIImage *image) {
//        if( image != nil ) {
//            preview.image = image;
//        }
//    }];

}

- (void)renderPreviewsFromWishes:(NSArray *)wishes
{
    // скрываем все превьюхи с глаз долой
    for (NSInteger i = 0; i < [_previewCollection count]; i++) {
        UIImageView *pv = _previewCollection[i];
        pv.hidden = YES;
    }
    
    if ( [wishes count] > 0 ) {
        for (NSInteger i = 0; i < [wishes count] && i < [_previewCollection count]; i++) {
            [self addPreview:_previewCollection[i] byURL: [wishes[i] getCover]];
        }
    }
}

+ (float)cellHeightWishlist:(ModelWishlist *)wishlist
{
    float paddingPreview = 0;
    float height = 0;
    float paddingDescToView = 12.0 * 2; // dublicate PADDING_INNER_CELL
    float maxHeightDesc = 200.0;
    CGSize expectedItemsCountSize;
    CGSize expectedDescSize;
    CGSize expectedNameSize;
    
    if ( wishlist != nil ) {
        expectedItemsCountSize = [UILabel rectSizeString: [NSString stringWithFormat:@"%lu", (long)[wishlist getWishesCount]]
                                           byWidth: 29.0
                                         maxHeight: maxHeightDesc
                                          fontName: [UIFont h_defaultFontName]
                                          fontSize: FONT_SIZE_NAME];
        
        expectedDescSize = [UILabel rectSizeString: [wishlist getDescription]
                                              byWidth: [UIScreen mainScreen].bounds.size.width - paddingDescToView
                                            maxHeight: maxHeightDesc
                                             fontName: [UIFont h_defaultFontName]
                                             fontSize: FONT_SIZE_DESC];

        expectedNameSize = [UILabel rectSizeString: [wishlist getName]
                                               byWidth: [UIScreen mainScreen].bounds.size.width - expectedItemsCountSize.width - 43.0
                                             maxHeight: maxHeightDesc
                                              fontName: [UIFont h_defaultFontName]
                                              fontSize: FONT_SIZE_NAME];
        
        // Добавляем маленький отступ превью от верхнего края ячейки
        if ( [wishlist getWishesCount] > 0 ) {
            paddingPreview = expectedDescSize.height == 0.0 ? 10.0 : 20.0;
        }
        
        height =    expectedDescSize.height +
                    expectedNameSize.height +
                    (expectedDescSize.height == 0 ? 0 : 5.0) +  // отступ имени вишлиста от описания
                    ( [wishlist getWishesCount] > 0 ? 50 : 0) +   // превьюхи
                    paddingPreview +                            // отступ от превью
                    26.0;                                       // отступы сверху снизу
    } else {
        height = 40.0;
    }
    
    return height;
}

// добавляем кнопки по свайпу
- (void)drawRect:(CGRect)rect
{
    [self h_setSeparatorInsetZero];
}

#define tSwipeButtonTrash 0
#define tSwipeButtonEdit 1
#define tSwipeButtonLink 2

- (void)setRightButtons
{
    MGSwipeButton *trash    = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"trash_white"] backgroundColor: [UIColor h_red]];
    MGSwipeButton *edit     = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"settings_white"] backgroundColor: [UIColor h_smoothGreen]];
    MGSwipeButton *link     = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"link_white"] backgroundColor: [UIColor h_orange_middle]];
    trash.tag = tSwipeButtonTrash;
    edit.tag = tSwipeButtonEdit;
    link.tag = tSwipeButtonLink;
    
    self.rightButtons = @[trash, link, edit];
    
    [self setSwipeSettings];
}

- (void)setRightButtonsWithoutTrashButton
{
    MGSwipeButton *edit     = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"settings_white"] backgroundColor: [UIColor h_smoothGreen]];
    MGSwipeButton *link     = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"link_white"] backgroundColor: [UIColor h_orange_middle]];
    
    edit.tag = tSwipeButtonEdit;
    link.tag = tSwipeButtonLink;
    
    self.rightButtons = @[link, edit];
    [self setSwipeSettings];
}

- (void)setOnlyCopyLinkRightButton
{
    MGSwipeButton *link     = [MGSwipeButton buttonWithTitle:@"" icon: [UIImage imageNamed:@"link_white"] backgroundColor: [UIColor h_orange_middle]];
    
    link.tag = tSwipeButtonLink;
    
    self.rightButtons = @[link];
    [self setSwipeSettings];
}

- (void)setSwipeSettings
{
    self.rightSwipeSettings.transition = MGSwipeTransitionDrag;
    self.rightSwipeSettings.buttonMargin = 5.0;
    self.swipeBackgroundColor = [UIColor h_bg_commentView];
    self.swipeBackgroundColorDefault = [UIColor whiteColor];
    self.delegate = (id<MGSwipeTableCellDelegate>)self;
}

- (BOOL)swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSInteger tag = ((MGSwipeButton *)self.rightButtons[ index ]).tag;
    
    switch (tag) {
        case tSwipeButtonTrash: {
            if ( [item getWishesCount] == 0 ) {
                [self.buttonsDelegate removeEmptyWishlist:[item getId]];
                return YES;
            }
            
            [self.buttonsDelegate tryRemoveWishlist:[item getId]];

            break;
        }
        case tSwipeButtonEdit:
            [self.buttonsDelegate editWishlist:item];
            break;
            
        case tSwipeButtonLink: {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            pasteboard.string = [item getPathToWishlist];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification"
                                                                object:nil
                                                              userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"CopyWishlistUrl"]}];
            break;
        }
        default:
            break;
    }
    
    return YES;
}

@end
