//
//  WishlistTableViewCell.h
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "ModelWishlist.h"
#import "UIImageViewWish.h"


@protocol WishlistCellDelegate;

@interface WishlistTableViewCell : MGSwipeTableCell
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelDesc;
@property (weak, nonatomic) IBOutlet UILabel *labelItemsCount;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPrivateToAll;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewPrivateToAllExceptFriends;
@property(nonatomic, weak) id<WishlistCellDelegate> buttonsDelegate;
@property (strong, nonatomic) IBOutletCollection(UIImageViewWish) NSArray *previewCollection;

- (void)setRightButtons;
- (void)setRightButtonsWithoutTrashButton;
- (void)setOnlyCopyLinkRightButton;
- (void)setWithItem:(ModelWishlist *)wishlist;
+ (float)cellHeightWishlist:(ModelWishlist *)wishlist;

@end


@protocol WishlistCellDelegate <NSObject>
@required
- (void)editWishlist:(ModelWishlist *)wishlist;
- (void)tryRemoveWishlist:(uint32_t)wishlist_id;
- (void)removeEmptyWishlist:(uint32_t)wishlist_id;

@end