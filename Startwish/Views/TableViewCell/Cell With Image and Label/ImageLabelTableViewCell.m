//
//  ContactTableViewCell.m
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ImageLabelTableViewCell.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "UITableViewCell+Helper.h"


@implementation ImageLabelTableViewCell

#define FONT_SIZE_TITLE 16.0

- (void)awakeFromNib {

    [_title setFont: [UIFont h_defaultFontSize: FONT_SIZE_TITLE]];
    [_title setTextColor: [UIColor blackColor]];
    _image.layer.cornerRadius = 5.0;
    _image.clipsToBounds = YES;
    _image.contentMode = UIViewContentModeScaleAspectFill;
    [_image setBackgroundColor: [UIColor h_randomWishBackgroundColor]];
    
    if ( _textFieldEdit ) {
        [self.textFieldEdit addTarget:self action:@selector(textFieldText:) forControlEvents:UIControlEventEditingChanged];
//        [self.textFieldEdit addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    if( self.isSelected ) {
        [_title setFont: [UIFont h_defaultFontMediumSize: FONT_SIZE_TITLE]];
    } else {
        [_title setFont: [UIFont h_defaultFontSize: FONT_SIZE_TITLE]];
    }
}

- (void)drawRect:(CGRect)rect
{
    [self h_setSeparatorInsetZero];
    
    UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, self.frame.size.width, self.frame.size.height)];
    [selectView setBackgroundColor: [UIColor h_bg_contacts]];
    UIView *borderTop = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, self.frame.size.width, 0.5)];
    UIView *borderBottom = [[UIView alloc] initWithFrame:CGRectMake( 0, self.frame.size.height, self.frame.size.width, 0.5)];
    
    [borderTop setBackgroundColor: [UIColor h_bg_contacts_border]];
    [borderBottom setBackgroundColor: [UIColor h_bg_contacts_border]];
    [selectView addSubview: borderTop];
    [selectView addSubview: borderBottom];
    [self setSelectedBackgroundView: selectView];
}

- (void)dealloc
{
//    [self.textFieldEdit removeObserver:self forKeyPath:@"text"];
}

- (IBAction)textFieldText:(id)sender
{
    if ( !self.textFieldEdit.hidden ) {
        if ( ![self.textFieldEdit.text isEqualToString:self.title.text] ) {
            [self.cellDelegate label:self.textFieldEdit.text byIndex: self.cellIndex];
        }
    }
}


@end
