//
//  ContactTableViewCell.h
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIImageViewWish.h"


@protocol ImageLabelTableViewCellDelegate;
@interface ImageLabelTableViewCell : UITableViewCell

@property (strong, nonatomic) id                                item;
@property (weak, nonatomic) IBOutlet UIImageViewWish*           image;
@property (weak, nonatomic) IBOutlet UILabel*                   title;
@property (weak, nonatomic) IBOutlet UITextField*               textFieldEdit;
@property (weak, nonatomic) id<ImageLabelTableViewCellDelegate> cellDelegate;
@property (nonatomic) NSInteger                                 cellIndex;
@end

@protocol ImageLabelTableViewCellDelegate <NSObject>

- (void)label:(NSString *)value byIndex:(NSInteger)index;

@end
