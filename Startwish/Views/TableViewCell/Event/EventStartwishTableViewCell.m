//
//  EventStartwishTableViewCell.m
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventStartwishTableViewCell.h"
#import "UILabel+typeOfText.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"


@interface EventStartwishTableViewCell()
{
    ModelNotification *item;
}
@end

@implementation EventStartwishTableViewCell

#define COMMENT_FONT_SIZE 13.0
#define paddingCommentToViewRightSide 19.0
#define paddingCommentToViewLeftSide 58.0

- (void)awakeFromNib {
    [_labelEventTitle setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];
    [_labelEventDescription setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];

    
    [_labelEventTitle setNumberOfLines: 0];
    [_labelEventDescription setNumberOfLines: 0];
    
    [_labelEventTitle setLineBreakMode: NSLineBreakByWordWrapping];
    [_labelEventDescription setLineBreakMode: NSLineBreakByWordWrapping];
    
    [_labelEventTitle setTextColor: [UIColor blackColor]];
    [_labelEventDescription setTextColor: [UIColor h_orange2]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    [self initSelectedViewWithColor: [UIColor whiteColor]];
}

- (void)drawRect:(CGRect)rect
{
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)cellFromItem:(ModelNotification *)event
{
    item = event;

    _labelEventTitle.text = [item getText];
    _labelEventDescription.text = [item getTitle];
}

+ (float)cellHeightForItem:(ModelNotification *)event
{
    float height = 0;
    float maxHeightText = 100.0;
    
    CGSize expectedTitleSize;
    CGSize expectedDescSize;
    
    if ( event != nil ) {
        expectedTitleSize = [UILabel rectSizeString: [event getText]
                                           byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                         maxHeight: maxHeightText
                                          fontName: [UIFont h_defaultFontNameBold]
                                          fontSize: COMMENT_FONT_SIZE];
        
        expectedDescSize = [UILabel rectSizeString: [event getTitle]
                                           byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                         maxHeight: maxHeightText
                                          fontName: [UIFont h_defaultFontNameBold]
                                          fontSize: COMMENT_FONT_SIZE];
        
        height =
        7.0 +        // отступ сверху
        expectedTitleSize.height +
        6.0 +       // отступ фио от тайтла
        expectedDescSize.height +
        15.0;       // отступы сверху снизу
    } else {
        height = 10.0;
    }
    
    return height;
}

// Добавляем представление выбранной ячейки
- (void)initSelectedViewWithColor:(UIColor *)bgColor
{
    // Полоска бордера
    UIView *borderTop = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, 0.5)];
    UIView *borderBottom = [[UIView alloc] initWithFrame: CGRectMake(0, self.frame.size.height, self.frame.size.width, 0.5)];
    
    [borderTop setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    [borderBottom setBackgroundColor: [UIColor h_bg_commentViewBorder]];
    
    UIView *bgColorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [bgColorView setBackgroundColor: bgColor];
    [bgColorView addSubview: borderTop];
    [bgColorView addSubview: borderBottom];
    
    [self setSelectedBackgroundView: bgColorView];
}


@end
