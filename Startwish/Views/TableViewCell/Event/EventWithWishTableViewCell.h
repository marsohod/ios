//
//  EventWithWishTableViewCell.h
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIViewAvatar.h"
#import "ModelNotification.h"
#import "ModelWish.h"
#import "UIImageViewWish.h"


@interface EventWithWishTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIViewAvatar *viewImageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDate;
@property (weak, nonatomic) IBOutlet UIImageViewWish *imageViewWish;
@property (weak, nonatomic) IBOutlet UILabel *labelEventTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelWishName;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWishWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWishHeight;

- (void)cellFromItem:(ModelNotification *)item;
+ (float)cellHeightForItem:(ModelNotification *)item;

@end
