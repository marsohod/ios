//
//  EventWithWishTableViewCell.m
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventWithWishTableViewCell.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"
#import "NSString+Helper.h"
#import "UILabel+typeOfText.h"
#import "UIImageView+WebCache.h"


@interface EventWithWishTableViewCell()
{
    ModelNotification *item;
}
@end

@implementation EventWithWishTableViewCell

#define COMMENT_FONT_SIZE 13.0
#define paddingCommentToViewRightSide 19.0
#define paddingCommentToViewLeftSide 58.0
#define widthImageWish 50.0

- (void)awakeFromNib {
    [_labelUserName setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];
    [_labelEventDate setFont: [UIFont h_defaultFontSize: 12.0]];
    [_labelWishName setFont: [UIFont h_defaultFontSize: COMMENT_FONT_SIZE]];
    
    [_labelWishName setNumberOfLines: 0];
    [_labelWishName setLineBreakMode: NSLineBreakByWordWrapping];

    [_labelUserName setTextColor: [UIColor h_smoothGreen]];
    
    _imageViewWish.layer.cornerRadius = 5.0;
    _imageViewWish.clipsToBounds = YES;
    _imageViewWish.contentMode = UIViewContentModeScaleAspectFill;
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if( [item isNotificationWish] ) {
        [self initSelectedViewWithColor:[UIColor h_bg_contacts]];
        
    }
}

- (void)drawRect:(CGRect)rect
{
    float labelNameWidth = [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide) - _labelEventDate.frame.size.width - 5.0;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
    
    // ставим ширину имени пользователя во всю оставшуюся длину
    _labelUserName.frame = CGRectMake(_labelUserName.frame.origin.x, _labelUserName.frame.origin.y, labelNameWidth , _labelUserName.frame.size.height);
}


- (void)cellFromItem:(ModelNotification *)event
{
    UIColor *bgColor = [UIColor whiteColor];
    item = event;
    _labelUserName.text = [item getEventUserName];
    _labelWishName.text = [NSString stringWithFormat:@"«%@»", [[item getObject] getName]];
    _labelEventDate.text = [NSString h_timeAgoUserFriendlyFromStringDate:[item getCreatedDate]];
    [_labelEventDate sizeToFit];
    [_labelEventTitle setTextColor: [UIColor h_orange2]];
    [_labelEventTitle setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];
    [_labelEventDate setTextColor: [UIColor h_yellow]];
    
    if( [item isNotificationWish] ) {
        _labelEventTitle.text = @"Хочет того же:";
        bgColor = [UIColor h_bg_contacts];
    } else if ( [item isNotificationComment] ) {
        [_labelEventTitle setTextColor: [UIColor blackColor]];
        [_labelEventDate setTextColor: [UIColor h_gray]];
        _labelEventTitle.text = [NSString stringWithFormat:@"Оставил%@ комментарий:", [[event getEventUser] isMan] ? @"" : @"a"];
        _labelWishName.text = [NSString stringWithFormat:@"\"%@\"", [event getText]];
        [_labelEventTitle setFont: [UIFont h_defaultFontSize: COMMENT_FONT_SIZE]];
    }
    
    [self setBackgroundColor: bgColor];
    [_labelUserName setBackgroundColor: bgColor];
    [_labelWishName setBackgroundColor: bgColor];
    [_labelEventDate setBackgroundColor: bgColor];
    [_labelEventTitle setBackgroundColor: bgColor];
    
    [self downloadAvatar];
    [self reloadConstraintsWish];
    [self downloadWish];
}

- (void)downloadAvatar
{
    [_viewImageAvatar customDrawView];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[item getEventUserAvatar] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            [_viewImageAvatar setImage:image];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: [item getEventUserAvatar]] withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [_viewImageAvatar animateAppearanceImage:image];
                }
            }];
            
        }
    }];
}

- (void)downloadWish
{
    NSString *cover = [[item getObject] hasExecutedImageWithSizes] ? [[item getObject] getCoverExecuted] : [[item getObject] getCover];
    
    [_imageViewWish setBackgroundColor: [UIColor h_randomWishBackgroundColor]];
    _imageViewWish.image = nil;
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:cover done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            _imageViewWish.image = image;
            [_imageViewWish setBackgroundColor: [UIColor clearColor]];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: cover] withIdentify:[item getId] toCacheWithSize:CGSizeMake( widthImageWish, [EventWithWishTableViewCell coverHeight:item] * widthImageWish / [EventWithWishTableViewCell coverWidth:item] ) onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId]) {
                    [_imageViewWish animateAppearanceImage:image];
                    [_imageViewWish setBackgroundColor: [UIColor clearColor]];
                }
            }];
            
        }
    }];
}

+ (CGFloat)coverHeight:(ModelNotification *)item
{
//    NSLog(@"coverHeight: %f %d", [[item getObject] hasExecutedImageWithSizes] ? [[item getObject] getCoverExecutedHeight] : [[item getObject] getCoverHeight], [[item getObject] hasExecutedImageWithSizes]);
    return [[item getObject] hasExecutedImageWithSizes] ? [[item getObject] getCoverExecutedHeight] : [[item getObject] getCoverHeight];
}

+ (CGFloat)coverWidth:(ModelNotification *)item
{
//    NSLog(@"coverWidth: %f %d", [[item getObject] hasExecutedImageWithSizes] ? [[item getObject] getCoverExecutedWidth] : [[item getObject] getCoverWidth], [[item getObject] hasExecutedImageWithSizes]);
    return [[item getObject] hasExecutedImageWithSizes] ? [[item getObject] getCoverExecutedWidth] : [[item getObject] getCoverWidth];
}

- (void)reloadConstraintsWish
{
    CGFloat cWidth      = [EventWithWishTableViewCell coverWidth:item];
    CGFloat cHeight     = [EventWithWishTableViewCell coverHeight:item];
    
//    NSLog(@"reloadConstraintsWish: %@ %f | %f | %f", item, widthImageWish, cWidth, cHeight);
    _imageWishWidth.constant = widthImageWish;
    _imageWishHeight.constant = widthImageWish * cHeight / (cWidth == 0 ? 1 : cWidth);
}

+ (float)cellHeightForItem:(ModelNotification *)item
{
    float height = 0;
    float maxHeightText = 300.0;
    float imageWishHeight = [self coverHeight:item] * widthImageWish / [self coverWidth:item];
    float minCellHeight = 52.0 + floorf(imageWishHeight);
    
    CGSize expectedTextSize;
    
    if ( item != nil ) {
        expectedTextSize = [UILabel rectSizeString: ([item isNotificationComment] ? [NSString stringWithFormat:@"\"%@\"", [item getText]] : [NSString stringWithFormat:@"«%@»", [[item getObject] getName]])
                                           byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide) - widthImageWish - 10.0
                                         maxHeight: maxHeightText
                                          fontName: [UIFont h_defaultFontName]
                                          fontSize: COMMENT_FONT_SIZE];
        
        //refactor
        height =
        7.0 +        // отступ сверху
        16.0 +      // высота имени пользователя
        7.0 +       // отступ фио от тайтла
        16.0 +        // высота тайтла
        expectedTextSize.height +
        15.0;       // отступ снизу
    } else {
        height = 80.0;
    }
    
    return height < minCellHeight ? minCellHeight : height;
}


// Добавляем представление выбранной ячейки
- (void)initSelectedViewWithColor:(UIColor *)bgColor
{
    UIView *bgColorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [bgColorView setBackgroundColor: bgColor];
    
    [self setSelectedBackgroundView: bgColorView];
}

@end
