//
//  EventStartwishTableViewCell.h
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelNotification.h"


@interface EventStartwishTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelEventTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDescription;

- (void)cellFromItem:(ModelNotification *)item;
+ (float)cellHeightForItem:(ModelNotification *)item;

@end
