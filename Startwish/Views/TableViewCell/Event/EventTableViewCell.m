//
//  EventTableViewCell.m
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventTableViewCell.h"
#import "UIImage+Helper.h"
#import "UILabel+typeOfText.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "NSString+Helper.h"
#import "ActionTexts.h"


@interface EventTableViewCell()
{
    ModelNotification *item;
}
@end

@implementation EventTableViewCell

#define COMMENT_FONT_SIZE 13.0
#define paddingCommentToViewRightSide 19.0
#define paddingCommentToViewLeftSide 58.0

- (void)awakeFromNib {
    [_labelUserName setFont: [UIFont h_defaultFontBoldSize: COMMENT_FONT_SIZE]];
    [_labelEventText setFont: [UIFont h_defaultFontSize: COMMENT_FONT_SIZE]];
    [_labelEventDate setFont: [UIFont h_defaultFontSize: 12.0]];
    
    [_labelEventText setNumberOfLines: 0];
    [_labelEventText setLineBreakMode: NSLineBreakByWordWrapping];
    
    [_labelUserName setTextColor: [UIColor h_smoothGreen]];
    
    [_labelEventDate setTextColor: [UIColor h_gray]];
    
    _viewMoreFollowers.hidden = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    if ( [item isNotificationBirthday] ) {
        [self initSelectedViewWithColor:[UIColor h_red]];
    } else {
        [self initSelectedViewWithColor:[UIColor whiteColor]];
    }
}

- (void)drawRect:(CGRect)rect
{
    float labelNameWidth = [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide) - _labelEventDate.frame.size.width - 5.0;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
    
    // ставим ширину имени пользователя во всю оставшуюся длину
    _labelUserName.frame = CGRectMake(_labelUserName.frame.origin.x, _labelUserName.frame.origin.y, labelNameWidth , _labelUserName.frame.size.height);
}

- (void)cellFromItem:(ModelNotification *)event
{
    item = event;
    
//    NSMutableDictionary *tempUsers = [[NSMutableDictionary alloc] init];
//    NSArray *tempUsersArray;
    
    _labelUserName.text     = [item getEventUserName];
    _labelEventText.text    = [EventTableViewCell preTitleForTextEvent:item];
    _labelEventDate.text    = [NSString h_timeAgoUserFriendlyFromStringDate:[item getCreatedDate]];
    
    [_labelEventDate sizeToFit];
    
    if ( [item isNotificationRelation] ) {
        // refactor
        // 
//        tempUsersArray = [[NSArray alloc] initWithObjects:  @{@"avatar": [[item eventUser] getAvatar]},
//                                                            @{@"avatar": [[item eventUser] getAvatar]},
//                                                            @{@"avatar": [[item eventUser] getAvatar]},
//                                                            @{@"avatar": [[item eventUser] getAvatar]},
//                                                            @{@"avatar": @"http://storage_01.startwish.ru/images/5a/fe/4879/3b12ec06374d61c69b042ae7b2dac8f57339_c.jpg"},
//                                                            @{@"avatar": [[item eventUser] getAvatar]}, nil];
//        tempUsers[@"users"] = tempUsersArray;
//        [tempUsers[@"users"] addObject:tempUsersArray];
        
//        [self renderMoreFollowers:tempUsers];
//        _viewMoreFollowers.hidden = NO;
    } else {
//        [self renderMoreFollowers:tempUsers];
//        _viewMoreFollowers.hidden = YES;
    }
    
    if ( [item isNotificationBirthday] ) {
        _viewImageAvatar.isHighlighted = YES;
        [_labelUserName setTextColor: [UIColor h_smoothYellow]];
        [_labelUserName setBackgroundColor:[UIColor h_red]];
        [_labelEventDate setTextColor: [UIColor h_smoothDeepYellow]];
        [_labelEventDate setBackgroundColor:[UIColor h_red]];
        [_labelEventText setTextColor: [UIColor whiteColor]];
        [_labelEventText setBackgroundColor:[UIColor h_red]];
        [self setBackgroundColor:[UIColor h_red]];
    } else {
        _viewImageAvatar.isHighlighted = NO;
        [_labelUserName setTextColor: [UIColor h_smoothGreen]];
        [_labelUserName setBackgroundColor:[UIColor whiteColor]];
        [_labelEventDate setTextColor: [UIColor h_gray]];
        [_labelEventDate setBackgroundColor:[UIColor whiteColor]];
        [_labelEventText setTextColor: [UIColor blackColor]];
        [_labelEventText setBackgroundColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor whiteColor]];
    }
    
    [_viewImageAvatar customDrawView];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[item getEventUserAvatar] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            [_viewImageAvatar setImage:image];
        } else {
            
            [UIImage downloadWithUrl: [NSURL URLWithString: [item getEventUserAvatar]] withIdentify:[item getId] onCompleteChange:^(UIImage *image, uint32_t identify) {
                if( image != nil && identify == [item getId] ) {
                    [_viewImageAvatar animateAppearanceImage:image];
                }
            }];
            
        }
    }];
}


+ (float)cellHeightForItem:(ModelNotification *)event
{
    float height = 0;
    float maxHeightText = 100.0;
    
    CGSize expectedTextSize;
    
    if ( event != nil ) {
        expectedTextSize = [UILabel rectSizeString: [EventTableViewCell preTitleForTextEvent:event]
                                              byWidth: [UIScreen mainScreen].bounds.size.width - (paddingCommentToViewLeftSide + paddingCommentToViewRightSide)
                                            maxHeight: maxHeightText
                                             fontName: [UIFont h_defaultFontName]
                                             fontSize: COMMENT_FONT_SIZE];

        height =
        7.0 +        // отступ сверху
        16.0 +      // высота имени пользователя
        6.0 +       // отступ фио от тайтла
        expectedTextSize.height +
//        ([event sectorId] == 3 ? 36.0 + 8.0 : 0) + // refactor. Height more followers view and padding
        15.0;       // отступы сверху снизу
    } else {
        height = 10.0;
    }

    return height;
}

+ (NSString *)preTitleForTextEvent:(ModelNotification *)event
{
    NSString *title = @"";
    
    if ( [event isNotificationComment] ) {
        //[[[event eventComment] getUser] isMan]
        title = [NSString stringWithFormat:@"Оставил%@ комментарий: \r%@", [[[event getObject] getUser] isMan] ? @"" : @"a",[event getText]];
        
    } else if ( [event isNotificationRelation] ) {
        //eventUser
        if( ![[event getEmitter] isFriend] ) {
            title = [NSString stringWithFormat:@"Подписал%@ на ваши обновления", [[event getEmitter] isMan] ? @"ся" : @"ась"];
        } else {
            title = [[ActionTexts sharedInstance] titleText:@"EventNewFriend"];
        }
        
    } else if ( [event isNotificationBirthday] ) {
        //eventUser
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
        NSArray *birth = [[[event getObject] getBirthday] componentsSeparatedByString:@"-"];

        CGFloat daysToBirthday = [NSString h_daysFromNowToDateSting:[NSString stringWithFormat:@"%zd-%@-%@", components.year, birth[1], [birth lastObject]]];
        
        if ( [birth count] < 3 ) {
            return title;
        }
        
        if ( daysToBirthday <= 1 && daysToBirthday > -1 ) {
            if ( daysToBirthday >= 0 ) {
                title = [[ActionTexts sharedInstance] titleText:@"BirthdayToday"];
            } else if ( daysToBirthday < 0 ) {
                title = [[ActionTexts sharedInstance] titleText:@"BirthdayTomorrow"];
            } else {
                //eventUser
                title = [NSString stringWithFormat:@"День рождения %@", [NSString h_dateToUserFriendlyFromStringDate:[[event getObject] getBirthday]]];
            }
        } else {
            if ( daysToBirthday >= 1.0 && daysToBirthday < 2.0 ) {
                title = [[ActionTexts sharedInstance] titleText:@"BirthdayYesterday"];
            } else {
                //eventUser
                title = [NSString stringWithFormat:@"День рождения %@", [NSString h_dateToUserFriendlyFromStringDate:[[event getObject] getBirthday]]];
            }
        }

        
    } else if ( [event isNotificationUser] ) {
        title = [NSString stringWithFormat:@"Ваш друг из %@ теперь здесь.", [event getNetwork]];
    }
    
    return title;
}


// Добавляем представление выбранной ячейки
- (void)initSelectedViewWithColor:(UIColor *)bgColor
{
    UIView *bgColorView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [bgColorView setBackgroundColor: bgColor];
    
    [self setSelectedBackgroundView: bgColorView];
    
    [_labelEventDate setBackgroundColor:bgColor];
    [_labelEventText setBackgroundColor:bgColor];
    [_labelUserName setBackgroundColor:bgColor];
    [self setBackgroundColor: bgColor];
}


- (void)renderMoreFollowers:(NSDictionary *)tempUsers
{
    //[[_viewMoreFollowers subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (tempUsers == nil ) { return; }
    
//    for( NSInteger i = 0; i < [tempUsers[@"users"] count]; i ++ ) {
    [tempUsers[@"users"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        // refactor
        // в связи с переходом на новое отображение аватара нет метода initWithLeftBound
//        UIImageViewAvatar *avatar = [[UIImageViewAvatar alloc] initWithLeftBound:([UIImageViewAvatar avatarViewSize].width * idx + (5.0 * idx)) topBound:0.0];
        
//        [_viewMoreFollowers addSubview:avatar];
        
        // refactor
        // когда будут настоящии пользователи - брать avatar по нормальному
//        [UIImage downloadWithUrl: [NSURL URLWithString: obj[@"avatar"]] onCompleteChange:^(UIImage *image) {
//            if( image != nil ) {
//                avatar.image = [[avatar prepareImageToAvatarImage: image] scaleToSize:[UIImageViewAvatar avatarImageSize]];
//            }
//        }];
    }];
}

@end
