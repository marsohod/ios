//
//  EventTableViewCell.h
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIViewAvatar.h"
#import "ModelNotification.h"
#import "ModelUser.h"
#import "ModelComment.h"
#import "ModelWish.h"


@interface EventTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIViewAvatar *viewImageAvatar;
@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelEventText;
@property (weak, nonatomic) IBOutlet UILabel *labelEventDate;
@property (weak, nonatomic) IBOutlet UIView *viewMoreFollowers;

- (void)cellFromItem:(ModelNotification *)item;
+ (float)cellHeightForItem:(ModelNotification *)item;

@end
