//
//  CityViewCell.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CityViewCell.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"


@implementation CityViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.labelRegion setFont: [UIFont h_defaultFontSize: 13.0]];
    [self.labelRegion setTextColor: [UIColor whiteColor]];
    
    [self.labelCity setFont: [UIFont h_defaultFontSize: 16.0]];
}


- (void)cellWithItem:(ModelCity *)city
{
    
}

- (void)setMenuItem:(id)city
{
    item = (ModelCity *)city;
    
    self.labelRegion.text = [item getRegionName];
    self.labelCity.text = [item getName];
    [self.labelCity setTextColor: self.isSelected ? [UIColor h_yellowStrong] : [UIColor h_smoothYellow]];
    self.imageDone.hidden = !self.isSelected;

}

- (void)drawRect:(CGRect)rect
{
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsMake(0, 20.0, 0, 20.0)];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
