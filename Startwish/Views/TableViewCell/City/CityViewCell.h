//
//  CityViewCell.h
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TableViewCell.h"
#import "ModelCity.h"


@interface CityViewCell : TableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelCity;
@property (weak, nonatomic) IBOutlet UILabel *labelRegion;
@property (weak, nonatomic) IBOutlet UIImageView *imageDone;

- (void)cellWithItem:(ModelCity *)city;

@end
