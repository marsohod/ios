//
//  TableViewCell.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "TableViewCell.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@implementation TableViewCell

- (void)awakeFromNib {
    [self setBackgroundColor: [UIColor clearColor]];
    [self initSelectedView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    [self setMenuItem: item];
}

// Добавляем представление выбранной ячейки
- (void)initSelectedView
{
    // Полоска бордера
    UIImage *border = [[UIImage imageNamed: @"pickerView_border.png"] scaleToSize: CGSizeMake(self.frame.size.width, 1.0)];
    UIImageView *borderTop = [[UIImageView alloc] initWithFrame: CGRectMake(0.0, 0.0, self.frame.size.width, 0.5)];
    UIImageView *borderBottom = [[UIImageView alloc] initWithFrame: CGRectMake(0.0, self.frame.size.height + 1.0, self.frame.size.width, 0.5)];
    borderTop.image = border;
    borderBottom.image = border;
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tableCell_selected_bg.png"]];
    [bgColorView addSubview: borderTop];
    [bgColorView addSubview: borderBottom];
    
    [self setSelectedBackgroundView: bgColorView];
}

- (void)setMenuItem:(id)item
{
    
}

@end
