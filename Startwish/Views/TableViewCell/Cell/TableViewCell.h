//
//  TableViewCell.h
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface TableViewCell : UITableViewCell
{
    id item;
}

- (void)initSelectedView;
- (void)setMenuItem:(id)item;

@end
