//
//  SpamTableViewCell.h
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelFeedback.h"


@interface SpamTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

- (void)setCell:(ModelFeedback *)item;

@end
