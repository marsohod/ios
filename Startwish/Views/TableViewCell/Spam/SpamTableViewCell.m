//
//  SpamTableViewCell.m
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SpamTableViewCell.h"


@implementation SpamTableViewCell

- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setCell:(ModelFeedback *)item
{
    _label.text = [item getName];
}

@end
