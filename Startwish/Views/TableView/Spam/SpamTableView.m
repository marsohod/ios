//
//  SpamTableView.m
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SpamTableView.h"
#import "SpamTableViewCell.h"
#import "UIColor+Helper.h"
#import "ModelFeedback.h"


@implementation SpamTableView

#define CellName @"spamCell"

- (void)awakeFromNib
{
    [self registerNib:[UINib nibWithNibName:@"SpamCell" bundle:nil] forCellReuseIdentifier: CellName];
    [self setSeparatorColor: [UIColor h_bg_commentViewBorder]];
    self.tableFooterView = [[UIView alloc] init];
    
    self.delegate = (id<UITableViewDelegate>)self;
    self.dataSource = (id<UITableViewDataSource>)self;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SpamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellName];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(SpamTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [_items count] ) {
        [cell setCell:_items[indexPath.row]];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [_items count] ) {
        [self.spamDelegate willSelectCellId:[(ModelFeedback *)_items[indexPath.row] getId]];
    }
    
    return indexPath;
}

@end
