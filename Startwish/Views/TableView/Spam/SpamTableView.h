//
//  SpamTableView.h
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol SpamTableViewDelegate;
@interface SpamTableView : UITableView

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, assign) id<SpamTableViewDelegate>spamDelegate;

@end


@protocol SpamTableViewDelegate <NSObject>

- (void)willSelectCellId:(NSInteger)cellId;

@end