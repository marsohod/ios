//
//  CustomTableView.m
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "CustomTableView.h"
#import "UIColor+Helper.h"
#import "UIScrollView+BottomRefreshControl.h"


@implementation CustomTableView

- (CustomTableView *)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        [self setBackgroundColor:[UIColor h_bg_collectionView]];
        
        self.bottomRefreshControl = [UIRefreshControl new];
        
        self.dataSource = self;
        self.delegate = self;
        
        
        items = [NSMutableArray array];
        page = 0;
    }

    return self;
}

- (NSUInteger)currentPage
{
    return page;
}

- (void)clean
{
    [self cleanPage];
    [items removeAllObjects];
    [self reloadData];
}

- (void)cleanPage
{
    page = 0;
}

- (NSInteger)itemsCount
{
    return [items count];
}

- (void)removeAllObjects
{
    [items removeAllObjects];
}

- (void)updateWithItems:(NSArray *)newItems
{
    if( [newItems count] > 0 ) {
        page++;
    }
    
    [items addObjectsFromArray:newItems];
    
    [self.bottomRefreshControl endRefreshing];
    
//    [self reloadNewData:newItems];
    [self reloadData];
}

- (void)updateWithItemsIncludeOldItems:(NSArray *)newItems
{
    if ( page == 0 ) {
        [self removeAllObjectsFromTable];
    }
    
    if( [newItems count] > 0 ) {
        page++;
    }
    
    [self.bottomRefreshControl endRefreshing];
    [self reloadNewData:newItems];
}

- (void)reloadNewData:(NSArray *)newItems
{
    NSMutableArray *oldItems = [[NSMutableArray alloc] init];
    NSMutableArray *indexsPaths = [[NSMutableArray alloc] init];
    
    [oldItems setArray:items];
    [items removeAllObjects];
    [items addObjectsFromArray:newItems];
    
    for ( NSUInteger i = [oldItems count]; i < [newItems count]; i++ ) {
        [indexsPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    
    if ( [indexsPaths count] != 0 ) {
        [self beginUpdates];
        [self insertRowsAtIndexPaths:indexsPaths withRowAnimation:UITableViewRowAnimationNone];
        [self endUpdates];
    }
}

- (void)updateWithNewestItems:(NSArray *)newItems
{
    for ( NSInteger i = [newItems count] - 1; i >= 0; i-- ) {
        [items insertObject:newItems[i] atIndex:0];
    }
    
    [self reloadData];
}

- (void)removeAllObjectsFromTable
{
    NSMutableArray *indexsPaths = [[NSMutableArray alloc] init];
    
    for ( NSUInteger i = 0; i < [items count]; i++ ) {
        [indexsPaths addObject:[NSIndexPath indexPathForItem:i inSection:0]];
    }
    
    [items removeAllObjects];
    
    if ( [indexsPaths count] != 0 ) {
        [self beginUpdates];
        [self deleteRowsAtIndexPaths:indexsPaths withRowAnimation:UITableViewRowAnimationNone];
    
        [self endUpdates];
    }
}


#pragma mark -
#pragma mark <UITableViewDataSource>


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier: @"" forIndexPath: indexPath];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self refreshingShow:scrollView.contentOffset];
}

@end
