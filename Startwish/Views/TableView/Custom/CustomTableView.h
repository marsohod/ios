//
//  CustomTableView.h
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CustomTableView : UITableView <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *items;
    NSUInteger page;
}

- (void)updateWithItems:(NSArray *)newItems;
- (void)updateWithItemsIncludeOldItems:(NSArray *)newItems;
- (void)updateWithNewestItems:(NSArray *)newItems;
- (NSUInteger)currentPage;

- (void)clean;
- (void)cleanPage;
- (void)removeAllObjects;
- (NSInteger)itemsCount;

@end
