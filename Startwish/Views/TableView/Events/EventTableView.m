//
//  EventTableView.m
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventTableView.h"
#import "EventTableViewCell.h"
#import "EventWithWishTableViewCell.h"
#import "EventStartwishTableViewCell.h"
#import "ModelNotification.h"
#import "ModelWish.h"
#import "ModelUser.h"
#import "ModelComment.h"
#import "LoadingView.h"
#import "NSString+Helper.h"


@interface EventTableView ()
{
    NSString *cellName;
    NSString *cellName2;
    NSString *cellName3;
}
@end


@implementation EventTableView


- (EventTableView *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if( self ) {
        cellName    = @"eventCell";
        cellName2   = @"eventWithWishCell";
        cellName3   = @"eventStartwishCell";
        
        [self registerNib:[UINib nibWithNibName:@"EventCell" bundle:nil] forCellReuseIdentifier: cellName];
        [self registerNib:[UINib nibWithNibName:@"EventWithWishCell" bundle:nil] forCellReuseIdentifier: cellName2];
        [self registerNib:[UINib nibWithNibName:@"EventStartwishCell" bundle:nil] forCellReuseIdentifier: cellName3];
        [self setBackgroundColor:[UIColor whiteColor]];
        
        self.pullToRefreshView = [[SSPullToRefreshView alloc] initWithScrollView:self delegate:nil];
        self.pullToRefreshView.contentView = [[LoadingView alloc] init];
        self.pullToRefreshView.expandedHeight = [LoadingView loadindViewHeight] + 10.0;
    }
    
    return self;
}

#pragma mark -
#pragma mark <UITableViewDataSource>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row < [items count] ) {
        if( [items[indexPath.row] isNotificationWish] || [items[indexPath.row] isNotificationComment] ) {
            return [EventWithWishTableViewCell cellHeightForItem: items[indexPath.row]];
        } else if ( [items[indexPath.row] isNotificationStartwish] ) {
            return [EventStartwishTableViewCell cellHeightForItem: items[indexPath.row]];
        } else {
            return [EventTableViewCell cellHeightForItem: items[indexPath.row]];
        }
    }
    
    return 60.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell;
    
    if ( indexPath.row >= [items count] ) {
        return cell;
    }
    
    if( [items[indexPath.row] isNotificationWish] || [items[indexPath.row] isNotificationComment] ) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellName2];
        cell = (EventWithWishTableViewCell *)cell;
    } else if ( [items[indexPath.row] isNotificationStartwish] ) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellName3];
        cell = (EventStartwishTableViewCell *)cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:cellName];
        cell = (EventTableViewCell *)cell;
    }
    
    return cell;
}


#pragma mark -
#pragma mark <UITableViewDelegate>
- (void)tableView:(UITableView *)tableView willDisplayCell:(id)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( items[indexPath.row] != nil ) {
        [cell cellFromItem:items[indexPath.row]];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row >= [items count] ) {
        return;
    }
    
    if ( [items[indexPath.row] isNotificationWish] ) {
        [self.tapDelegate showWish:[items[indexPath.row] getObject]];
    } else if ( [items[indexPath.row] isNotificationUser] ) {
        [self.tapDelegate showUser:[items[indexPath.row] getEmitter]];
    } else if ( [items[indexPath.row] isNotificationComment] ) {
        [self.tapDelegate showWishComments:[items[indexPath.row] getObject] replyToUserName:[items[indexPath.row] getEventUserName] replyToCommentId:[[[(ModelWish *)[items[indexPath.row] getObject] getComments] firstObject] getId]];
    }
}

#pragma mark -
#pragma mark <UIScrollViewDelegate>
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    
    if( self.contentSize.height - (self.frame.size.height * 2) < self.contentOffset.y ) {
        [self.tapDelegate loadItems];
    }
}

#pragma mark -
- (NSString *)lastItemDate
{
    return [items lastObject] ? [[items lastObject] getCreatedDate] : [NSString h_dateNow];
}

- (NSString *)firstItemDate
{
    return [[items firstObject] getCreatedDate];
}

- (NSInteger)itemsCount
{
    return [items count];
}

@end
