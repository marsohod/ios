//
//  EventTableView.h
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomTableView.h"
#import "SSPullToRefreshView.h"


@protocol EventTableViewDelegate;

@interface EventTableView : CustomTableView

@property (nonatomic) SSPullToRefreshView *pullToRefreshView;
@property (nonatomic, weak) id<EventTableViewDelegate> tapDelegate;

- (NSString *)lastItemDate;
- (NSString *)firstItemDate;
- (NSInteger)itemsCount;
@end

@protocol EventTableViewDelegate <NSObject>
@required
- (void)showWish:(id)wish;
- (void)showWishComments:(id)wish replyToUserName:(NSString *)userName replyToCommentId:(uint32_t)commentId;
- (void)showUser:(id)user;
- (void)loadItems;

@end