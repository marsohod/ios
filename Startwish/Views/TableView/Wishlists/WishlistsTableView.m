//
//  WishlistsTableView.m
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "WishlistsTableView.h"
#import "WishlistTableViewCell.h"
#import "Me.h"


@interface WishlistsTableView ()
{
    NSString *cellName;
}
@end

@implementation WishlistsTableView

- (WishlistsTableView *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];

    if( self ) {
        cellName = @"wishlistCell";
        [self registerNib:[UINib nibWithNibName:@"WishlistCell" bundle:nil] forCellReuseIdentifier: cellName];
        self.tableFooterView = [[UIView alloc] init];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderWishlist:) name:@"STWishlistUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderNewWishlist:) name:@"STWishlistAdd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishesCountInWishlist:) name:@"STWishAdd" object:nil];
    
    return self;
}

- (void)renderWishlist:(NSNotification *)notif
{
    if ( notif.userInfo[@"wishlist"] ) {
        NSUInteger index = [self itemIndexById:[notif.userInfo[@"wishlist"] getId]];
        ModelWishlist *oldWishlist = [items objectAtIndex:index];
        
        [oldWishlist setName:[(ModelWishlist *)notif.userInfo[@"wishlist"] getName]];
        [oldWishlist setDescription:[(ModelWishlist *)notif.userInfo[@"wishlist"] getDescription]];
        [oldWishlist setPrivacy:[(ModelWishlist *)notif.userInfo[@"wishlist"] getPrivacy]];
        
        [items replaceObjectAtIndex:index withObject:oldWishlist];
        
        [self reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[items indexOfObject:notif.userInfo[@"wishlist"]] inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (void)renderNewWishlist:(NSNotification *)notif
{
    if ( notif.userInfo[@"wishlist"] ) {
        [items insertObject:notif.userInfo[@"wishlist"] atIndex:0];
        [self reloadData];
    }
}

- (void)increaseWishesCountInWishlist:(NSNotification *)notif
{
    if ( notif.userInfo[@"wishlistId"] ) {
        NSUInteger index = [self itemIndexById:(uint32_t)[notif.userInfo[@"wishlistId"] longLongValue]];
        ModelWishlist *w = [self itemById:(uint32_t)[notif.userInfo[@"wishlistId"] longLongValue]];
        ModelWishlist *oldWishlist = [items objectAtIndex:index];
        
        if ( w != nil ) {
            [oldWishlist reduceWishesCount:NO];
            [oldWishlist addWish:notif.userInfo[@"item"]];

            [items replaceObjectAtIndex:index withObject:oldWishlist];
            [self reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)removeItemById:(uint32_t)itemId
{
    NSUInteger index = [self itemIndexById:itemId];
    [items removeObjectAtIndex:index];
    [self deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
}

- (void)updateItem:(ModelWishlist *)wishlist byId:(uint32_t)itemId
{
    NSInteger index = [self itemIndexById:itemId];
    
    if ( items[index] ) {
        items[index] = wishlist;
        [self reloadData];
    }
    
}

- (NSUInteger)itemIndexById:(uint32_t)itemId
{
    __block NSUInteger index = 0;
    
    [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ( [obj getId] == itemId ) {
            index = idx;
            *stop = YES;
        }
    }];
    
    return index;
}

- (ModelWishlist *)itemById:(uint32_t)itemId
{
    __block ModelWishlist *w;
    
    [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ( [obj getId] == itemId ) {
            w = obj;
            *stop = YES;
        }
    }];
    
    return w;
}


#pragma mark -
#pragma mark <UITableViewDataSource>

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WishlistTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier: cellName forIndexPath: indexPath];
    
    if ( indexPath.row < [items count] ) {
        [cell setWithItem: items[indexPath.row]];
        
        if( [Me isMyProfile:[items[indexPath.row] getOwnerId]] ) {
            if ( [items count] == 1 ) {
                [cell setRightButtonsWithoutTrashButton];
            } else {
                [cell setRightButtons];
            }
        } else {
            [cell setOnlyCopyLinkRightButton];
        }
        
        cell.buttonsDelegate = (id<WishlistCellDelegate>)self.tapDelegate;
    }
    
    return cell;
}



#pragma mark -
#pragma mark UITapGestureRecognizer




#pragma mark -
#pragma mark <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ModelWishlist *w = [items objectAtIndex:indexPath.row];
    
    [self.tapDelegate wishlistDetail: [w getId]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    if( indexPath.row < [items count] ) {
        return [WishlistTableViewCell cellHeightWishlist: items[ indexPath.row ] ];
    } else {
        return 60.0;
    }
}


#pragma mark -
#pragma mark <UIScrollViewDelegate>
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if( self.contentSize.height - (self.frame.size.height * 2) < self.contentOffset.y ) {
        [self.tapDelegate loadItems];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
