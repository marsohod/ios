//
//  WishlistsTableView.h
//  Startwish
//
//  Created by marsohod on 16/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTableView.h"
#import "ModelWishlist.h"


@protocol WishlistsDelegate;

@interface WishlistsTableView : CustomTableView

@property (nonatomic, weak) id<WishlistsDelegate> tapDelegate;

- (void)removeItemById:(uint32_t)itemId;
- (void)updateItem:(ModelWishlist *)wishlist byId:(uint32_t)itemId;
- (NSUInteger)itemIndexById:(uint32_t)itemId;
- (ModelWishlist *)itemById:(uint32_t)itemId;

@end


@protocol WishlistsDelegate <NSObject>
@required

- (void)removeWishlist:(uint32_t)wishlistId;
- (void)editWishlist:(ModelWishlist *)wishlist;
- (void)wishlistDetail:(uint32_t)wishlistId;
- (void)loadItems;

@end