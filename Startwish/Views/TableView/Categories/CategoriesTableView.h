//
//  CategoriesTableView.h
//  Startwish
//
//  Created by Pavel Makukha on 06/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CustomTableView.h"


@interface CategoriesTableView : UITableView

@property (strong, nonatomic) NSArray *items;

- (NSArray *)selectedItemsId;

@end
