//
//  CategoriesTableView.m
//  Startwish
//
//  Created by Pavel Makukha on 06/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CategoriesTableView.h"
#import "ImageLabelTableViewCell.h"
#import "ImageLabelTableViewCell+Categories.h"
#import "ModelWishCategory.h"


static NSString *cellIdentifier = @"explorerCell";

@implementation CategoriesTableView

- (void)awakeFromNib
{
    [self registerNib: [UINib nibWithNibName:@"CategoryCell" bundle:nil] forCellReuseIdentifier: cellIdentifier];
    
    self.delegate = (id<UITableViewDelegate>)self;
    self.dataSource = (id<UITableViewDataSource>)self;
    
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (NSArray *)selectedItemsId
{
    NSMutableArray *si = [[NSMutableArray alloc] init];
    
    [[self indexPathsForSelectedRows] enumerateObjectsUsingBlock:^(NSIndexPath *obj, NSUInteger idx, BOOL *stop) {
        [si addObject: [NSString stringWithFormat:@"%u", [(ModelWishCategory *)_items[obj.row] getId]]];
    }];
    
    return [[NSArray alloc] initWithArray:si];
}

#pragma mark -
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath: indexPath];
    
    [cell setCategories: self.items[indexPath.row]];
    
    return cell;
}


#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *selectedIndexPath = [tableView indexPathForSelectedRow];
    
    if ( selectedIndexPath && indexPath.row == selectedIndexPath.row ) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STEditDidEndTextField" object:nil];
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STEditDidEndTextField" object:nil];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STDeselectCategoryRow" object:nil];
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.frame = CGRectMake(-cell.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    cell.layer.opacity = 0;
    
    [UIView animateWithDuration:.3 animations:^{
        cell.frame = CGRectMake(0.0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        cell.layer.opacity = 1;
    }];
}

@end
