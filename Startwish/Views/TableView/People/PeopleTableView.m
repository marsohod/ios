//
//  PeopleTableView.m
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "PeopleTableView.h"
#import "PeopleTableViewCell.h"


@interface PeopleTableView ()
{
    NSString    *cellName;
    BOOL        isAnimationNeed;
}
@end

@implementation PeopleTableView

- (PeopleTableView *)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if( self ) {
        cellName = @"peopleCell";
        _cellType = 0;
        [self registerNib:[UINib nibWithNibName:@"PeopleCell" bundle:nil] forCellReuseIdentifier: cellName];
        [self setBackgroundColor:[UIColor whiteColor]];
        self.tableFooterView = [[UIView alloc] init];
    }
    
    return self;
}

- (void)awakeFromNib
{
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark -
#pragma mark <UITableViewDataSource>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [PeopleTableViewCell cellHeightForUser: items[indexPath.row][@"item"] cellType:_cellType];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PeopleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    return cell;
}


#pragma mark -
#pragma mark <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row < [items count] && items[indexPath.row] ) {
        [self.cellDelegate showUser:items[indexPath.row][@"item"]];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(PeopleTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if( indexPath.row < [items count] && items[indexPath.row] != nil ) {
        cell.cellType = _cellType;
        [cell cellWithUser:items[indexPath.row][@"item"] searchRange:[items[indexPath.row][@"range"] rangeValue]];
        cell.cellDelegate = (id<PeopleTableViewCellDelegate>)self.cellDelegate;
    }
    
    if ( !isAnimationNeed ) {
        return;
    }
        
    cell.frame = CGRectMake(-cell.frame.size.width, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    cell.layer.opacity = 0;
    
    [UIView animateWithDuration:.3 animations:^{
        cell.frame = CGRectMake(0.0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    }];
    
    [UIView animateWithDuration:0.6 animations:^{
        cell.layer.opacity = 1;
    }];
}

#pragma mark -
#pragma mark <UIScrollViewDelegate>
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
    //    NSLog(@"scroll %f %f", self.contentSize.height - self.frame.size.height, self.contentOffset.y);
    if( self.contentSize.height - (self.frame.size.height * 2) < self.contentOffset.y ) {
        [self.cellDelegate loadItems];
    }
    
}


- (NSArray *)getItems
{
    return items;
}

- (void)updateWithItems:(NSArray *)newItems animation:(BOOL)animation
{
    isAnimationNeed = animation;
    [super updateWithItems:newItems];
}

- (void)updateWithItemsIncludeOldItems:(NSArray *)newItems animation:(BOOL)animation
{
    isAnimationNeed = animation;
    [super updateWithItemsIncludeOldItems:newItems];
}
@end
