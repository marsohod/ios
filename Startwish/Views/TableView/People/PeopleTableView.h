//
//  PeopleTableView.h
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomTableView.h"
#import "RedButton.h"


@protocol PeopleTableViewDelegate;
@interface PeopleTableView : CustomTableView

@property (nonatomic, weak) id<PeopleTableViewDelegate> cellDelegate;
@property (nonatomic) NSInteger cellType;

- (NSArray *)getItems;
- (void)updateWithItems:(NSArray *)newItems animation:(BOOL)animation;
- (void)updateWithItemsIncludeOldItems:(NSArray *)newItems animation:(BOOL)animation;

@end

@protocol PeopleTableViewDelegate <NSObject>
@required

- (void)showUser:(id)user;
- (void)loadItems;

@end