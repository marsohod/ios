//
//  ContactsTableView.h
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol ContactsTableViewDelegate;
@interface ContactsTableView : UITableView <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *itemsKey;
@property (strong, nonatomic) NSArray *itemsValue;
@property (nonatomic) BOOL isEditing;
@property (weak, nonatomic) id<ContactsTableViewDelegate> tableDelegate;

@end

@protocol ContactsTableViewDelegate <NSObject>

@required
- (void)selectItemKey:(NSUInteger)idx;

- (void)changeValue:(NSString *)value byInd:(NSUInteger)idx;
@end
