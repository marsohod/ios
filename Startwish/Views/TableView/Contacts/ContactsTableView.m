//
//  ContactsTableView.m
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ContactsTableView.h"
#import "ImageLabelTableViewCell.h"
#import "ImageLabelTableViewCell+Contact.h"


@implementation ContactsTableView

static NSString *cellIdentifier = @"contactCell";

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        self.delegate = self;
        [self registerNib: [UINib nibWithNibName: @"ContactCell" bundle:nil] forCellReuseIdentifier: cellIdentifier];
    }
    
    return self;
}

- (void)awakeFromNib
{
    self.delegate = self;
    self.dataSource = self;
    [self registerNib: [UINib nibWithNibName: @"ContactCell" bundle:nil] forCellReuseIdentifier: cellIdentifier];
}

- (void)drawRect:(CGRect)rect
{
    // Убираем паддинг разделителя слева

    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_itemsValue count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath: indexPath];
    
    [cell setContactCellName: _itemsKey[ indexPath.row ] value: _itemsValue[ indexPath.row ] duringEdit:_isEditing byIndex:indexPath.row];
    cell.cellDelegate = (id<ImageLabelTableViewCellDelegate>)self;
    cell.textFieldEdit.delegate = (id<UITextFieldDelegate>)self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableDelegate selectItemKey:indexPath.row];
}


#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}


#pragma mark -
#pragma mark ImageLabelTableViewCellDelegate
- (void)label:(NSString *)value byIndex:(NSInteger)index
{
    if ( index < [_itemsValue count] ) {
        [self.tableDelegate changeValue:value byInd:index];
    }
}


#pragma mark -
#pragma UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    CGRect point = [theTextField convertRect:theTextField.bounds toView:self];
    NSIndexPath *indexPath = [self indexPathForRowAtPoint:point.origin];
    
    if ( indexPath ) {
        if ( indexPath.row + 1 < [_itemsKey count] ) {
            
            ImageLabelTableViewCell *cell = (ImageLabelTableViewCell *)[self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:0]];
            
            if ( cell ) {
                [cell.textFieldEdit becomeFirstResponder];
            }
        }
    }
    
    
                                     
    //    if (theTextField == self.textPassword) {
    //        [theTextField resignFirstResponder];
    //    } else if (theTextField == self.textUsername) {
    //        [self.textPassword becomeFirstResponder];
    //    }
    return YES;
}


@end
