//
//  CommentsTableView.m
//  Startwish
//
//  Created by Pavel Makukha on 08/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CommentsTableView.h"
#import "CommentTableViewCell.h"
#import "UIColor+Helper.h"




@implementation CommentsTableView

static NSString *commentCell = @"commentCell";

- (void)awakeFromNib
{
    [self registerNib: [UINib nibWithNibName:@"CommentCell" bundle:nil] forCellReuseIdentifier: commentCell];
    [self setSeparatorColor: [UIColor h_bg_commentViewBorder]];
    
    self.delegate = (id<UITableViewDelegate>)self;
    self.dataSource = (id<UITableViewDataSource>)self;

    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (BOOL)isLoadedAllComments
{
    //    NSLog(@"%u %lu %lu", [[wish getComments] count] == [wish getCommentsCount], [[wish getComments] count], [wish getCommentsCount]);
    return [_items count] == _maxItemsCount;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    //    [self commentViewHeight];
}




#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self isLoadedAllComments] ? _maxItemsCount : [_items count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier: commentCell forIndexPath:indexPath];
}


#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( ![self isLoadedAllComments] && indexPath.row == 0 ) {
        return [CommentTableViewCell cellHeightForLoadingMoreComment];
    }
    
    return [CommentTableViewCell cellHeightForComment: [_items objectAtIndex: indexPath.row - ( [self isLoadedAllComments] ? 0 : 1 )]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = indexPath.row - ([self isLoadedAllComments] ? 0 : 1);
    
    if( index >= 0 ) {
        if ( [_items objectAtIndex:index] ) {}
    } else {
        [self.tableDelegate didSelectLoadMoreCell];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( ![self isLoadedAllComments] && indexPath.row == 0 ) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(CommentTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath ) {
        if( ![self isLoadedAllComments] && indexPath.row == 0 ) {
            [cell renderLoadMore: _maxItemsCount - [_items count]];
        } else {
            [cell setCellFromComment: [_items objectAtIndex: indexPath.row - ( [self isLoadedAllComments] ? 0 : 1 )]];
            [cell setRightButtons];
        }
        
        [cell implementWish:_areCommentsExecutedWish];
        
        cell.buttonDelegate = (id<CommentDelegate>)self.tableDelegate;
    }
    
    if( (indexPath.row > [_items count] - 1 && ![self isLoadedAllComments]) || (indexPath.row == [_items count] - 1 && [self isLoadedAllComments]) ) {
        [self.tableDelegate willDisplayLastCell];
    }
}

@end
