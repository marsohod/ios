//
//  CommentsTableView.h
//  Startwish
//
//  Created by Pavel Makukha on 08/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelComment.h"


@protocol CommentsTableViewDelegate;

@interface CommentsTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *items;
@property (nonatomic) NSInteger maxItemsCount;
@property (nonatomic) BOOL areCommentsExecutedWish;
@property (weak, nonatomic) id<CommentsTableViewDelegate> tableDelegate;

@end


@protocol CommentsTableViewDelegate <NSObject>

- (void)didSelectLoadMoreCell;
- (void)willDisplayLastCell;

@end
