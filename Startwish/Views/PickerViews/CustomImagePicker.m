//
//  CustomImagePicker.m
//  Startwish
//
//  Created by marsohod on 13/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CustomImagePicker.h"
#import "ClusterPrePermissions+Helper.h"
#import "DZImageEditingController.h"
#import "ActionTexts.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface CustomImagePicker ()

@property (nonatomic) UIImageView *overlayImageView;
@property (nonatomic) CGRect frameRect;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation CustomImagePicker

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        self.delegate = (id<UINavigationControllerDelegate,UIImagePickerControllerDelegate, DZImageEditingControllerDelegate>)self;
        self.allowsEditing = NO;
//        self.modalPresentationStyle = UIModalPresentationCurrentContext;
//        self.modalPresentationStyle = UIModalPresentationFullScreen;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        self.overlayImageView = [self createOverlayCropView:CGRectZero];
        self.overlayImageView.image = [[UIImage alloc] init];

    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#define imagesFolder @"Startwish"

+ (void)saveImagetoStartwishAlbum:(UIImage *)image
{
    __block PHAssetCollection *selectedAlbum;
    __block PHObjectPlaceholder *placeholder;
    
    PHFetchResult *localAlbums = [PHCollectionList fetchTopLevelUserCollectionsWithOptions:nil];
    
    [localAlbums enumerateObjectsUsingBlock:^(PHAssetCollection *obj, NSUInteger idx, BOOL *stop) {
        if ( [obj.localizedTitle isEqualToString:imagesFolder] ) {
            selectedAlbum = obj;
        }
    }];
    
    if ( !selectedAlbum ) {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest * request = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:imagesFolder];
            placeholder = request.placeholderForCreatedAssetCollection;
        } completionHandler:^(BOOL success, NSError *error) {
            if (success) {
                PHFetchResult *collectionFetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[placeholder.localIdentifier]
                                                                                                            options:nil];
                selectedAlbum = collectionFetchResult.firstObject;
                [self saveImage:image toAlbum:selectedAlbum];
            }
            if (error) {
                NSLog(@"Error creating album: %@", error);
            }
            
        }];
        
        return;
    }
    
    [self saveImage:image toAlbum:selectedAlbum];
    
    // IOS 7.*
    //    [_assetsLibrary addAssetsGroupAlbumWithName:imagesFolder resultBlock:^(ALAssetsGroup *group) {
    //        [s.assetsLibrary writeImageToSavedPhotosAlbum:s.photoView.image.CGImage orientation:orientation completionBlock:^(NSURL *assetURL, NSError *error) {
    //
    //        }];
    //    } failureBlock:^(NSError *error) {
    //
    //    }];
}

+ (void)saveImage:(UIImage *)image toAlbum:(PHAssetCollection *)album
{
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        
        if (album) {
            PHAssetCollectionChangeRequest *assetCollectionChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:album];
            [assetCollectionChangeRequest addAssets:@[[assetChangeRequest placeholderForCreatedAsset]]];
        }
    } completionHandler:nil];
}


- (void)checkAccessToCamera
{
    [ClusterPrePermissions h_showCameraPermissionsWithCallBack:^{
        self.sourceType = UIImagePickerControllerSourceTypeCamera;
//        self.sourceType = UIImagePickerControllerCameraCaptureModeVideo;
        self.toolbarHidden = YES;
        self.navigationBarHidden = YES;
        self.edgesForExtendedLayout = UIRectEdgeTop;
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
        //                                                    edgesForExtendedLayout and extendedLayoutIncludesOpaqueBars
        //                                                    pickerPhoto.cameraOverlayView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,320)];
        
        [self.ctrlDelegate presentController:self];
    } rejectFirstStep:^{
    } rejectSecondStep:^{
    }];
}

- (void)checkAccessToPhotos
{
    [ClusterPrePermissions h_showPhotoPermissionsWithCallBack:^{
        self.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        [self setMediaTypes: [NSArray arrayWithObject:kUTTypeMovie]];
//        self.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        [self.ctrlDelegate presentController:self];
    } rejectFirstStep:^{
    } rejectSecondStep:^{
    }];
}

- (void)allowEditing:(BOOL)state
{
    self.allowsEditing = state;
}

#pragma mark -
#pragma mark UIImagePickerControllerDelegate

// Берём фотку из стандартного UIImagePickerController
// закрываем контроллер
// создаём контроллер для редактирования фотки и показываем его
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    __weak CustomImagePicker *s = self;
    
    DZImageEditingController *editingViewController = [DZImageEditingController new];
    editingViewController.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    editingViewController.overlayView = self.overlayImageView;
    editingViewController.cropRect = self.frameRect;
    editingViewController.delegate = (id<DZImageEditingControllerDelegate>)s;
    [self pushViewController:editingViewController animated:NO];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
//    [self popViewControllerAnimated:YES];
    [self removeFromParentViewController];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        
    }
}

#pragma mark - DZImageEditingControllerDelegate

- (void)imageEditingControllerDidCancel:(DZImageEditingController *)editingController
{
    [self popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// После того, как выбрали окончательный размер фотки сохраняем картинку и передаём её дальше в предыдущий контроллер
- (void)imageEditingController:(DZImageEditingController *)editingController didFinishEditingWithImage:(UIImage *)editedImage
{
    [self.ctrlDelegate setImageSelected:editedImage];
    
//     Если из камеры взяли изображение, то сохраняем
    if ( self.sourceType != UIImagePickerControllerSourceTypePhotoLibrary ) {
        [ClusterPrePermissions h_showPhotoPermissionsWithCallBack:^{
            UIImageWriteToSavedPhotosAlbum(editedImage,
                                           self,
                                           @selector(image:finishedSavingWithError:contextInfo:),
                                           nil);
        } rejectFirstStep:^{
        } rejectSecondStep:^{
        } showAlertViewAfterSecondReject:NO];
    }
    
//    [self popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)navigationController:(UINavigationController *)navigationController
     willShowViewController:(UIViewController *)viewController
                   animated:(BOOL)animated
{
    self.navigationBar.hidden = [viewController isKindOfClass:[DZImageEditingController class]];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIViewController *)childViewControllerForStatusBarHidden
{
    return nil;
}


const CGFloat bottomBarHeight = 130.f;

- (void)setCropSquare
{
    self.overlayImageView = [self createOverlayCropView:CGRectMake(0, ( [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.width ) / 2, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width)];
}

- (void)setCropToSize:(CGSize)size
{
    self.overlayImageView = [self createOverlayCropView:CGRectMake(0, ([UIScreen mainScreen].bounds.size.height - bottomBarHeight - size.height) / 2, size.width, size.width)];
}


- (UIImageView *)createOverlayCropView:(CGRect)rect
{
    self.frameRect = CGRectEqualToRect(rect, CGRectZero) ? CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - bottomBarHeight) : rect;
    
    UIImageView *iv = [[UIImageView alloc] initWithFrame:self.frameRect];
//    CALayer *innerBorder = [CALayer layer];
//    
//    if ( !CGRectEqualToRect(rect, CGRectZero) ) {
//        iv.layer.borderColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8].CGColor;
//        iv.layer.borderWidth = 2.0f;
//        
//        innerBorder.frame = CGRectMake(0, 0, self.frameRect.size.width, self.frameRect.size.height);
//        innerBorder.borderColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5].CGColor;
//        innerBorder.borderWidth = 3.0f;
//        [iv.layer addSublayer:innerBorder];
//    }
    
    return iv;
}

@end
