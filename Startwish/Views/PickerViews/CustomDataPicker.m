//
//  CustomDataPicker.m
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomDataPicker.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@interface CustomDataPicker ()

@property (nonatomic, strong, readwrite) NSDate *minimumDate;
@property (nonatomic, strong, readwrite) NSDate *maximumDate;
@property (nonatomic, strong, readwrite) NSDate *date;

@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSDateComponents *currentDateComponents;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIFont *fontSelected;


@end

@implementation CustomDataPicker

const NSUInteger NUM_COMPONENTS = 3;

// Типы барабанов
typedef enum {
    kSBDatePickerDay,
    kSBDatePickerMonth,
    kSBDatePickerYear,
    kSBDatePickerInvalid = 0
} CustomDatePickerComponent;

static CustomDatePickerComponent _components[NUM_COMPONENTS];

#define FONT_SIZE 16.0
#define YEARS_COUNT 100
#define MIN_YEARS_FOR_BIRTHDAY 12


#pragma mark -
#pragma mark init

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    
    if (!self) {
        return nil;
    }
    
    [self commonInit];
    
    return self;
}

- (void)commonInit {
    NSDateComponents *component = [[NSDateComponents alloc] init];
    
    self.font = [UIFont fontWithName: @"HelveticaNeue-Light" size: FONT_SIZE];
    self.fontSelected = [UIFont fontWithName: @"HelveticaNeue-Light" size: FONT_SIZE];
    self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
//    [self setLocale: [NSLocale currentLocale]];
    [self setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"]];
    self.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.dataSource = self;
    self.delegate = self;
    
    self.date = [NSDate date];
    
    component.year = [self yearToday] - YEARS_COUNT;
    component.month = 1;
    component.day = 1;
    
    [self setMinimumDate:[self.calendar dateFromComponents:component]];
}

- (void)drawRect:(CGRect)rect
{
    [((UIView *)[self.subviews objectAtIndex:1]) setBackgroundColor: [UIColor colorWithPatternImage: [[UIImage imageNamed:@"pickerView_border.png"] scaleToSize: CGSizeMake(self.frame.size.width, 1.0) ]]];
    [((UIView *)[self.subviews objectAtIndex:2]) setBackgroundColor: [UIColor colorWithPatternImage: [[UIImage imageNamed:@"pickerView_border.png"] scaleToSize: CGSizeMake(self.frame.size.width, 1.0) ]]];
    
//    [self.layer setBorderColor: [UIColor h_smoothYellow].CGColor];
//    [self.layer setBorderWidth: 1.0];
}

#pragma mark -
#pragma mark Setup

- (void)setMinimumDate:(NSDate *)minimumDate {
    _minimumDate = minimumDate;
    
    [self updateComponents];
}

- (void)setMaximumDate:(NSDate *)maximumDate {
    _maximumDate = maximumDate;
    
    [self updateComponents];
}

- (void)setMaximumDateForBirthday
{
    NSDateComponents *component = [[NSDateComponents alloc] init];
    
    component.year = [self yearToday] - MIN_YEARS_FOR_BIRTHDAY;
    component.month = 12;
    component.day = 31;
    
    [self setMaximumDate:[self.calendar dateFromComponents:component]];
}

- (void)setDate:(NSDate *)date {
    [self setDate:date animated:NO];
}

- (void)setDate:(NSDate *)date animated:(BOOL)animated {
    self.currentDateComponents = [self.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    
    [self reloadAllComponents];
    [self setIndicesAnimated:YES];
}

- (NSInteger)getMinimumYear
{
    NSDateComponents *components;
    NSInteger minYear = 0;
    
    if ( _minimumDate ) {
        components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:_minimumDate];
        minYear = components.year;
    }
    
    return minYear;
}

- (NSDate *)date {
    return [self.calendar dateFromComponents:self.currentDateComponents];
}

- (NSInteger)yearToday
{
    return [self.calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: [NSDate date]].year;
}

- (void)setLocale:(NSLocale *)locale {
    self.calendar.locale = locale;
    
    [self updateComponents];
}

// Получаем типы по буквам из локали
- (CustomDatePickerComponent)componentFromLetter:(NSString *)letter {
    if ([letter isEqualToString:@"y"]) {
        return kSBDatePickerYear;
    }
    else if ([letter isEqualToString:@"m"]) {
        return kSBDatePickerMonth;
    }
    else if ([letter isEqualToString:@"d"]) {
        return kSBDatePickerDay;
    }
    else {
        return kSBDatePickerInvalid;
    }
}

- (CustomDatePickerComponent)thirdComponentFromFirstComponent:(CustomDatePickerComponent)component1
                                       andSecondComponent:(CustomDatePickerComponent)component2 {
    
    NSMutableIndexSet *set = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, NUM_COMPONENTS)];
    [set removeIndex:component1];
    [set removeIndex:component2];
    
    return (CustomDatePickerComponent) [set firstIndex];
}


- (void)updateComponents {
    // yMMMMd for usa local
    
    NSString *componentsOrdering = [NSDateFormatter dateFormatFromTemplate:@"dMMMMy" options:0 locale:self.calendar.locale];
    componentsOrdering = [componentsOrdering lowercaseString];
    
    NSString *firstLetter = [componentsOrdering substringToIndex:1];
    NSString *lastLetter = [componentsOrdering substringWithRange: NSMakeRange(7, 1)];
    
    _components[0] = [self componentFromLetter: firstLetter];
    _components[2] = [self componentFromLetter: lastLetter];
    _components[1] = [self thirdComponentFromFirstComponent: _components[2] andSecondComponent: _components[0]];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.calendar = self.calendar;
    self.dateFormatter.locale = self.calendar.locale;
    
    [self reloadAllComponents];
    
    [self setIndicesAnimated:NO];
}

- (void)setIndexForComponentIndex:(NSUInteger)componentIndex value:(NSInteger)val animated:(BOOL)animated {
    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    NSRange unitRange = [self rangeForComponent:component];
    
    NSInteger value;
    
    if (component == kSBDatePickerYear) {
        value = val == -1 ? self.currentDateComponents.year : val;
        self.currentDateComponents.year = value;
        [self editDayIfNeed];        
    }
    else if (component == kSBDatePickerMonth) {
        value = val == -1 ? self.currentDateComponents.month : val;
        self.currentDateComponents.month = value;
        [self editDayIfNeed];
    }
    else if (component == kSBDatePickerDay) {
        value = val == -1 ? self.currentDateComponents.day: val;
        self.currentDateComponents.day = value;
    }
    else {
        assert(NO);
    }
    
    NSInteger index = (value - (value > [self getMinimumYear] ? [self getMinimumYear] : 0) - unitRange.location);
    NSInteger middleIndex = index;
    
    if (component == kSBDatePickerYear) {
        //        middleIndex = ([self numberOfRowsFromComponent:component] / 2) - ([self numberOfRowsFromComponent:component] / 2) % unitRange.length + index;
    } else {
        //        middleIndex = index;
    }
    
    if( self.delegate != nil ) {
        if ( middleIndex >= [self numberOfRowsFromComponent:component] ) {
            middleIndex--;
        }
        [self selectRow:middleIndex inComponent:componentIndex animated:animated];
        [self sendEventToController];
    }
}

- (void)setIndexForComponentIndex:(NSUInteger)componentIndex animated:(BOOL)animated {
    [self setIndexForComponentIndex:componentIndex value:-1 animated:animated];
}


- (void)setIndicesAnimated:(BOOL)animated {
    for (NSUInteger componentIndex = 0; componentIndex < NUM_COMPONENTS; componentIndex++) {
        [self setIndexForComponentIndex:componentIndex animated:animated];
    }
}

- (CustomDatePickerComponent)componentForIndex:(NSInteger)componentIndex {
    
    return _components[componentIndex];
}

- (NSCalendarUnit)unitForComponent:(CustomDatePickerComponent)component {
    if (component == kSBDatePickerYear) {
        return NSCalendarUnitYear;
    }
    else if (component == kSBDatePickerMonth) {
        return NSCalendarUnitMonth;
    }
    else if (component == kSBDatePickerDay) {
        return NSCalendarUnitDay;
    }
    else {
        assert(NO);
    }
}

- (NSRange)rangeForComponent:(CustomDatePickerComponent)component {
    NSCalendarUnit unit = [self unitForComponent:component];
    NSRange unitRange = [self.calendar maximumRangeOfUnit:unit];
    
    // Достаём нужное количество дней в месяце
    if ( component == kSBDatePickerDay ) {
        if ( self.currentDateComponents != nil ) {            
            unitRange = [self.calendar rangeOfUnit:unit inUnit:NSCalendarUnitMonth forDate: self.date];
        }
    }
    
    // Ограничиваем количество лет
    else if( component == kSBDatePickerYear ) {
        unitRange = [self.calendar maximumRangeOfUnit:unit];
        
        if ( _minimumDate ) {
            unitRange.length = [self yearToday] - [self getMinimumYear];
        } else {
            unitRange.length = [self yearToday];
        }
//        unitRange.length = [self yearToday];
    } else {
        unitRange = [self.calendar maximumRangeOfUnit:unit];
    }
    
    return unitRange;
}


#pragma mark -
#pragma mark Data source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)componentIndex
{
    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    return [self numberOfRowsFromComponent:component];
}

- (NSInteger)numberOfRowsFromComponent:(CustomDatePickerComponent)component
{
    NSRange unitRange = [self rangeForComponent:component];
    if (component == kSBDatePickerYear) {
        return unitRange.length;
    }
    else if (component == kSBDatePickerMonth) {
        return unitRange.length;
    }
    else if (component == kSBDatePickerDay) {
        return unitRange.length;
    }
    else {
        return 0;
    }
}

#pragma mark -
#pragma mark - Delegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)componentIndex {
    
    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    
    if (component == kSBDatePickerYear) {
        CGSize size = [@"0000" sizeWithAttributes:@{NSFontAttributeName : self.font}];
        
        return size.width + 1.0f;
    }
    else if (component == kSBDatePickerMonth) {
        CGFloat maxWidth = 0.0f;
        
        for (NSString *monthName in self.dateFormatter.monthSymbols) {
            CGFloat monthWidth = [monthName sizeWithAttributes:@{NSFontAttributeName : self.font}].width;
            
            maxWidth = MAX(monthWidth, maxWidth);
        }
        
        return maxWidth + 25.0f;
    }
    else if (component == kSBDatePickerDay) {
        CGSize size = [@"00" sizeWithAttributes:@{NSFontAttributeName : self.font}];
        
        return size.width + 18.0f;
    }
    else {
        return 0.01f;
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 22.0f;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(CustomDatePickerComponent)component
{
    NSInteger minYear = 0;
    
    
    if ( component == kSBDatePickerYear ) {
        minYear = [self getMinimumYear];
    }
    
    NSRange unitRange = [self rangeForComponent:component];
    NSInteger value = unitRange.location + (row % unitRange.length) + minYear;
    
    if (component == kSBDatePickerYear) {
        return [NSString stringWithFormat:@"%li", (long) value];
    }
    else if (component == kSBDatePickerMonth) {
        return [[self.dateFormatter.monthSymbols objectAtIndex:(value - 1)] capitalizedString];
    }
    else if (component == kSBDatePickerDay) {
        return [NSString stringWithFormat:@"%li", (long) value];
    }
    else {
        return @"";
    }
}

- (NSInteger)valueForRow:(NSInteger)row andComponent:(CustomDatePickerComponent)component
{
    NSRange unitRange = [self rangeForComponent:component];
    NSInteger minYear = 0;
    
    if ( component == kSBDatePickerYear ) {
        minYear = [self getMinimumYear];
    }
    
    return (row % unitRange.length) + unitRange.location + minYear;
}

- (BOOL)isEnabledRow:(NSInteger)row forComponent:(NSInteger)componentIndex {
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    
    dateComponents.year = self.currentDateComponents.year;
    dateComponents.month = self.currentDateComponents.month;
    dateComponents.day = self.currentDateComponents.day;
    
    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    NSInteger value = [self valueForRow:row andComponent:component];
    
    if (component == kSBDatePickerYear) {
        dateComponents.year = value;
    }
    else if (component == kSBDatePickerMonth) {
        dateComponents.month = value;
    }
    else if (component == kSBDatePickerDay) {
        dateComponents.day = value;
    }
    
    NSDate *rowDate = [self.calendar dateFromComponents:dateComponents];
    
    if (self.minimumDate != nil && [self.minimumDate compare:rowDate] == NSOrderedDescending) {
        return NO;
    }
    else if (self.maximumDate != nil && [rowDate compare:self.maximumDate] == NSOrderedDescending) {
        return NO;
    }
    
    return YES;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)componentIndex reusingView:(UIView *)view
{
    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    UILabel *label;
    
    if ([view isKindOfClass:[UILabel class]]) {
        label = (UILabel *) view;
    }
    else {
        label = [[UILabel alloc] init];
        [label setFont: self.font];
        [label setUserInteractionEnabled: YES];
        [label setBackgroundColor: [UIColor clearColor]];
    }
    
    // Устанавливаем тайтл
    [label setText: [self titleForRow:row forComponent:component]];
    
    // Крайний правый штук делаем по правому краю. Для симметрии
    if (component == kSBDatePickerYear) {
        [label setTextAlignment: NSTextAlignmentRight];
    } else {
        [label setTextAlignment: NSTextAlignmentLeft];
    }

    // Проверяем на максимальное и минимальные значения
    if ( [self isEnabledRow:row forComponent:componentIndex] ) {

        // Расскрашиваем выбранный элемент
        if( [self selectedRowInComponent:componentIndex] == row ) {
            [self setStyleToSelectedRow: label];
        } else {
            [label setTextColor: [UIColor h_smoothYellow]];
        }
    }
    else {
        [label setTextColor: [UIColor blackColor]];
        return nil;
    }
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componentIndex {

    CustomDatePickerComponent component = [self componentForIndex:componentIndex];
    NSInteger value = [self valueForRow:row andComponent:component];
    
    if (component == kSBDatePickerYear) {
        self.currentDateComponents.year = value;
        [self editDayIfNeed];
    }
    else if (component == kSBDatePickerMonth) {
        self.currentDateComponents.month = value;
        [self editDayIfNeed];
    }
    else if (component == kSBDatePickerDay) {
        self.currentDateComponents.day = value;
    }
    else {
        assert(NO);
    }
    
    [self setIndexForComponentIndex:componentIndex animated:NO];
    
    if (self.minimumDate != nil && [self.date compare:self.minimumDate] == NSOrderedAscending) {
        [self setDate:self.minimumDate animated:YES];
    }
    else if (self.maximumDate != nil && [self.date compare:self.maximumDate] == NSOrderedDescending) {
        [self setDate:self.maximumDate animated:YES];
    }
    else {
        [self reloadAllComponents];
    }
    
//    [self sendEventToController];
    
//    self.values[component] = [NSString stringWithFormat: @"lu", value];
//    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

// в ситуациях, когда стоит дата 31 декабря и меняется месяц на февраль
- (void)editDayIfNeed
{
   
    NSDateComponents *pseudoDateComponents = [[NSDateComponents alloc] init];
    pseudoDateComponents.day = 1;
    pseudoDateComponents.month = self.currentDateComponents.month;
    pseudoDateComponents.year = self.currentDateComponents.year;
    
    // Получаем диапазон дней в месяце по выбранной дате
    NSRange localUnitRange = [self.calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate: [self.calendar dateFromComponents: pseudoDateComponents]];

    if ( localUnitRange.length - 1 <= self.currentDateComponents.day ) {
        self.currentDateComponents.day = localUnitRange.length;
    }
}

- (void)sendEventToController
{
    [[NSNotificationCenter defaultCenter] postNotificationName: @"pickerViewChangeValue"
                                                        object: self userInfo: @{ @"day" : [NSString stringWithFormat: @"%ld", (long)self.currentDateComponents.day],
                                                                                  @"month" : [NSString stringWithFormat: @"%ld", (long)self.currentDateComponents.month],
                                                                                  @"year" : [NSString stringWithFormat: @"%ld", (long)self.currentDateComponents.year]}];
}

- (void)setStyleToSelectedRow:(UILabel *)label
{
    [label setTextColor: [UIColor whiteColor]];
    [label setFont: self.fontSelected];
}

- (void)setDateFromString:(NSString *)date
{
    NSArray *dateSplit = [date componentsSeparatedByString:@"-"];
    
    NSInteger year = [dateSplit[0] integerValue];
    NSInteger month = [dateSplit[1] integerValue];
    NSInteger day = [dateSplit[2] integerValue];
    
    if ( [dateSplit count] > 0 ) {
        [self setIndexForComponentIndex:2 value:year animated:NO];
    }
    
    if ( [dateSplit count] > 1 ) {
        [self setIndexForComponentIndex:1 value:month animated:NO];
    }
    
    if ( [dateSplit count] > 2 ) {
        [self setIndexForComponentIndex:0 value:day animated:NO];
    }
}

@end
