//
//  CustomDataPicker.h
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CustomDataPicker : UIPickerView <UIPickerViewDataSource, UIPickerViewDelegate>

- (void)setMaximumDateForBirthday;
- (void)sendEventToController;
- (void)setDateFromString:(NSString *)date;

@end
