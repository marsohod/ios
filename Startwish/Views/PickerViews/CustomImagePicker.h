//
//  CustomImagePicker.h
//  Startwish
//
//  Created by marsohod on 13/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@import Photos;
@protocol IPDelegate;
@interface CustomImagePicker : UIImagePickerController

@property(nonatomic, weak) id<IPDelegate> ctrlDelegate;

+ (void)saveImagetoStartwishAlbum:(UIImage *)image;
+ (void)saveImage:(UIImage *)image toAlbum:(PHAssetCollection *)albumName;

- (void)checkAccessToCamera;
- (void)allowEditing:(BOOL)state;
- (void)setCropSquare;
- (void)setCropToSize:(CGSize)size;

@end

@protocol IPDelegate <NSObject>
@required
- (void)presentController:(id)ctrl;
- (void)presentController:(id)ctrl animation:(BOOL)animation;
- (void)setImageSelected:(UIImage *)image;

@end
