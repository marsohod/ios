//
//  UIImageViewAvatar.h
//  Startwish
//
//  Created by marsohod on 08/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIImageViewAvatar : UIImageView
{
    float view_width;
    float view_radius;
    float view_border_width;
    float image_width;
    float image_radius;
}

@property(nonatomic) BOOL isHighlighted;

+ (CGSize)avatarViewSize;
+ (CGSize)avatarImageSize;
- (void)customDrawView;
- (UIImage *)prepareImageToAvatarImage:(UIImage *)image;
- (UIImageViewAvatar *)initWithLeftBound:(float)left topBound:(float)top;

@end
