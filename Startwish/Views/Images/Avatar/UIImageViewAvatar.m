//
//  UIImageViewAvatar.m
//  Startwish
//
//  Created by marsohod on 08/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIImageViewAvatar.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@implementation UIImageViewAvatar

#define VIEW_WIDTH 36.0f
#define IMAGE_WIDTH 30.0f
#define IMAGE_RADIUS IMAGE_WIDTH / 2

- (void)setSizes
{
    view_width = VIEW_WIDTH;
    view_border_width = 2.0f;
    view_radius = view_width / 2;
    image_width = IMAGE_WIDTH;
    image_radius = image_width / 2;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setSizes];
    [self setDefaultView];
}

- (void)drawRect:(CGRect)rect
{
    [self setDefaultView];
}

- (UIImageViewAvatar *)initWithLeftBound:(float)left topBound:(float)top;
{
    CGRect frame = CGRectMake(left, top, VIEW_WIDTH, VIEW_WIDTH);
    
    self = [[UIImageViewAvatar alloc] initWithFrame:frame];
    [self setSizes];
    [self setDefaultView];
    return self;
}

+ (CGSize)avatarViewSize
{
    return CGSizeMake(VIEW_WIDTH, VIEW_WIDTH);
}

+ (CGSize)avatarImageSize
{
    return CGSizeMake(IMAGE_WIDTH, IMAGE_WIDTH);
}

- (UIImage *)prepareImageToAvatarImage:(UIImage *)image
{
//    return image;
    return [UIImage makeRoundedImage:image radius:image.size.width/2];
}

- (void)setDefaultView
{
    self.contentMode = UIViewContentModeCenter;
//    self.contentMode = UIViewContentModeScaleAspectFit;
//    self
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
    
    self.layer.cornerRadius = view_radius;
    [self.layer setBorderWidth: view_border_width];
    
    [self customDrawView];
    
    // Даём возвожность взаимодействия с пользователем
    // Например можно вешать события по Tap
    self.userInteractionEnabled = YES;
}

- (void)customDrawView
{
    if ( self.isHighlighted ) {
        [self.layer setBorderColor: [UIColor h_smoothYellow].CGColor];
        self.image = [[UIImage imageNamed:@"bg_avatar_yellow.png"] scaleToSize: [UIImageViewAvatar avatarImageSize]];
    } else {
        [self.layer setBorderColor: [UIColor h_smoothGreen].CGColor];
        self.image = [[UIImage imageNamed:@"bg_avatar_green.png"] scaleToSize: [UIImageViewAvatar avatarImageSize]];
    }
}

//- (void)drawRect:(CGRect)rect {
//    NSLog(@"draw avatar");
//}

@end
