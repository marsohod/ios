//
//  UIImageViewAvatarInMenu.h
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "UIImageViewAvatarBig.h"

@interface UIImageViewAvatarInMenu : UIImageViewAvatarBig

+ (CGSize)avatarViewSize;
+ (CGSize)avatarImageSize;
- (UIImage *)prepareImageToAvatarImage:(UIImage *)image;

@end
