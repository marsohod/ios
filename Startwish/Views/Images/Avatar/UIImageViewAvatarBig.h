//
//  UIImageViewAvatarBig.h
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIImageViewAvatar.h"


@interface UIImageViewAvatarBig : UIImageViewAvatar

+ (CGSize)avatarViewSize;
+ (CGSize)avatarImageSize;

- (void)hideImage;
- (UIImage *)prepareImageToAvatarImage:(UIImage *)image;

@end
