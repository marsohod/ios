//
//  UIImageViewAvatarInMenu.m
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIImageViewAvatarInMenu.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@implementation UIImageViewAvatarInMenu

#define VIEW_WIDTH 85.0f
#define IMAGE_WIDTH 70.0f
#define IMAGE_RADIUS IMAGE_WIDTH / 2

- (void)setSizes
{
    view_width = VIEW_WIDTH;
    view_border_width = 3.0f;
    view_radius = view_width / 2;
    image_width = IMAGE_WIDTH;
    image_radius = image_width / 2;
}

- (UIImage *)prepareImageToAvatarImage:(UIImage *)image
{
    return [UIImage makeRoundedImage:image radius:image.size.width/2];
}

+ (CGSize)avatarViewSize
{
    return CGSizeMake(VIEW_WIDTH, VIEW_WIDTH);
}

+ (CGSize)avatarImageSize
{
    return CGSizeMake(IMAGE_WIDTH, IMAGE_WIDTH);
}

- (void)customDrawView
{
    if ( self.isHighlighted ) {
        
    } else {
        [self.layer setBorderColor: [UIColor h_smoothYellow].CGColor];
        self.image = [[UIImage imageNamed:@"bg_avatar_yellow_menu.png"] scaleToSize: [UIImageViewAvatarInMenu avatarImageSize]];
    }
}

@end
