//
//  UIImageViewAvatarBig.m
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIImageViewAvatarBig.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@implementation UIImageViewAvatarBig

#define VIEW_WIDTH 175.0f
#define IMAGE_WIDTH 165.0f
#define IMAGE_RADIUS IMAGE_WIDTH / 2

- (void)setSizes
{
    view_width = VIEW_WIDTH;
    view_border_width = 2.0f;
    view_radius = view_width / 2;
    image_width = IMAGE_WIDTH;
    image_radius = image_width / 2;
}

+ (CGSize)avatarViewSize
{
    return CGSizeMake(VIEW_WIDTH, VIEW_WIDTH);
}

+ (CGSize)avatarImageSize
{
    return CGSizeMake(IMAGE_WIDTH, IMAGE_WIDTH);
}

- (UIImage *)prepareImageToAvatarImage:(UIImage *)image
{
    return [UIImage makeRoundedImage:image radius:image.size.width/2];
}

- (void)customDrawView
{
    if ( self.isHighlighted ) {
        
    } else {
        [self.layer setBorderColor: [UIColor h_smoothYellow].CGColor];
        self.image = [[UIImage imageNamed:@"bg_avatar_yellow_big.png"] scaleToSize: [UIImageViewAvatarBig avatarImageSize]];
    }
}

- (void)hideImage
{
    self.image = nil;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
