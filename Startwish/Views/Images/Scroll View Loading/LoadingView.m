//
//  LoadingView.m
//  Startwish
//
//  Created by marsohod on 01/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LoadingView.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"


@interface LoadingView()
{
    UIImageView *imageView;
    UIActivityIndicatorView *indicator;
}
@end

@implementation LoadingView

float progress;
float progress_prev;

#define LOGO_WIDTH 70.0

+ (float)loadindViewHeight
{
    return LOGO_WIDTH;
}

- (id)init
{
    self = [super init];
    
    if( self ) {
        progress = 0.0;
        progress_prev = 0.0;
        imageView = [[UIImageView alloc] init];
        indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
//        [self addSubview: imageView];
        [self addSubview: indicator];
//        [self customSetImage];
        [self customSetIndicator];
        [indicator startAnimating];
        [self setBackgroundColor: [UIColor h_bg_collectionView]];
    }
    
    return self;
}

- (void)setState:(SSPullToRefreshViewState)state withPullToRefreshView:(SSPullToRefreshView *)view
{
    switch( state ) {
        case SSPullToRefreshViewStateNormal:
//            [self setBackgroundColor: [UIColor blackColor]];
            break;
            
        case SSPullToRefreshViewStateLoading:
            // Анимация биения сердца
//            [self setBackgroundColor: [UIColor whiteColor]];
//            [_indicator startAnimating];
//            _barColor = [UIColor colorWithRed:.7 green:1.0 blue:.7 alpha:1.0];
            break;
            
        case SSPullToRefreshViewStateReady:
//            [self hideImage];
//            [self setBackgroundColor: [UIColor yellowColor]];
//            _barColor = [UIColor greenColor];
            break;
            
        case SSPullToRefreshViewStateClosing:
            [self hideIndicator];
//            [self hideImage];
//            [self setBackgroundColor: [UIColor redColor]];
//            [_indicator stopAnimating];
//            _barColor = [UIColor whiteColor];
            break;
            
        default:
            break;
    }
}

- (void)setPullProgress:(CGFloat)pullProgress {
    progress_prev = progress;
    progress = pullProgress > 1.0 ? 1.0 : pullProgress;
//    NSLog(@"%f %@", pullProgress, NSStringFromCGRect(self.frame));
//    [self customSetImage];
    [self customSetIndicator];
    [self setNeedsDisplay];
}

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

- (void)customSetImage
{
    
    imageView.frame = CGRectMake(self.frame.size.width / 2 - ((LOGO_WIDTH * progress) / 2),
                                 self.frame.size.height - LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress);
    
    imageView.image = [[UIImage imageNamed:@"loading_beast.png"] scaleToSize:CGSizeMake(LOGO_WIDTH * progress, LOGO_WIDTH * progress)];
    
    // Потихоньку по-поинтно крутим чувака
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue = [NSNumber numberWithFloat: M_PI / 2 * ( 1 - progress_prev )];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI / 2 * ( 1 - progress )];
    rotationAnimation.fillMode = kCAFillModeForwards;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.duration = 0;
    rotationAnimation.repeatCount = 1;

    [imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)customSetIndicator
{
    indicator.frame = CGRectMake(self.frame.size.width / 2 - ((LOGO_WIDTH * progress) / 2),
                                 self.frame.size.height - LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress);

    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fillMode = kCAFillModeForwards;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.duration = 0;
    rotationAnimation.repeatCount = 1;
    
    if ( indicator.hidden && progress != 0 ) {
        [indicator startAnimating];
    } else if ( progress == 0 ) {
        [indicator stopAnimating];
    }
    
    indicator.hidden = progress == 0 ? YES : NO;
//    
    [indicator.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)hideIndicator
{
    progress = 0;
    [UIView beginAnimations:@"hideImageView" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:0.5];
    indicator.frame = CGRectMake(self.frame.size.width / 2 - ((LOGO_WIDTH * progress) / 2),
                                 LOGO_WIDTH - LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress);
    indicator.hidden = YES;
    [UIView commitAnimations];

}

- (void)hideImage
{
    progress = 0;
    [UIView beginAnimations:@"hideImageView" context:nil];
    [UIView setAnimationDelay:0];
    [UIView setAnimationDuration:0.5];
    imageView.frame = CGRectMake(self.frame.size.width / 2 - ((LOGO_WIDTH * progress) / 2),
                                 LOGO_WIDTH - LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress,
                                 LOGO_WIDTH * progress);

    [UIView commitAnimations];
}

@end
