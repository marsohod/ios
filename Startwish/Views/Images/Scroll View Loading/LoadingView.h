//
//  LoadingView.h
//  Startwish
//
//  Created by marsohod on 01/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SSPullToRefresh.h"


@interface LoadingView : UIView <SSPullToRefreshContentView>

+ (float)loadindViewHeight;

@end
