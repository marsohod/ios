//
//  MenuButton.m
//  Startwish
//
//  Created by marsohod on 27/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "MenuImage.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"


@implementation MenuImage

#define DIAMETR 40.0

- (void)awakeFromNib
{
    [self setMenuImage];
}

- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        [self setMenuImage];
    }
    
    return self;
}

- (void)setMenuImage
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, DIAMETR, DIAMETR);
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = DIAMETR / 2;
    self.clipsToBounds = YES;
    self.contentMode = UIViewContentModeCenter;
    self.image = [[UIImage imageNamed:@"menu_white.png" ] scaleToSize:CGSizeMake(DIAMETR / 2, 12.0)];
    [self setBackgroundColor: [UIColor h_smoothGreen]];
}

@end
