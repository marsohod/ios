//
//  UIImageViewWish.h
//  Startwish
//
//  Created by marsohod on 21/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIImageViewWish : UIImageView

- (void)animateAppearanceImage:(UIImage *)image;
- (void)animateAppearanceImage:(UIImage *)image duration:(float)duration;

@end
