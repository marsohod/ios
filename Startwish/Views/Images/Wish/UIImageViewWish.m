//
//  UIImageViewWish.m
//  Startwish
//
//  Created by marsohod on 21/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "UIImageViewWish.h"


@implementation UIImageViewWish

#define ANIMATE_DURATION 0.3f

- (void)animateAppearanceImage:(UIImage *)image duration:(float)duration
{
    self.layer.opacity = 0;
    self.image = image;
    
    [UIView animateWithDuration:duration animations:^{
        self.layer.opacity = 1.0;
    }];
}

- (void)animateAppearanceImage:(UIImage *)image
{
    [self animateAppearanceImage:image duration:ANIMATE_DURATION];
}

@end
