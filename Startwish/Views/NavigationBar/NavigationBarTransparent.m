//
//  NavigationBarTransparent.m
//  Startwish
//
//  Created by marsohod on 27/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "NavigationBarTransparent.h"


@implementation NavigationBarTransparent

#define SHIFT_BAR 5.0

- (void)drawRect:(CGRect)rect {
    [self setBackgroundColor: [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0]];
    self.layer.frame = CGRectMake(-SHIFT_BAR, 0.0, [UIScreen mainScreen].bounds.size.width, 44.0 + 9.0);
}


@end
