//
//  Circle.h
//  Startwish
//
//  Created by marsohod on 06/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PRCircleProtocol;

@interface Circle : NSObject
@property (nonatomic) float             x;
@property (nonatomic) float             y;
@property (nonatomic) float             radius;
@property (nonatomic) NSMutableArray*   followingCircles;
@property (nonatomic) float             angleMove;
@property (nonatomic) CGPoint           speedMove;
@property (nonatomic) CALayer*          addedLayer;
@property (nonatomic) BOOL              isChangeAngleMove;
@property (nonatomic, weak) id<PRCircleProtocol> moveDelegate;

- (void)    setCoordinates:(CGPoint)location;
- (CGPoint) getCenter;
- (void)    startMove;
- (void)    moveCircle;
- (void)    followToCrossingCircle:(Circle *)circle;
- (CGPoint) moveDestinationLocation;
- (void)calculateAngleAfterCrossWithCircle:(Circle *)circle;

@end

@protocol PRCircleProtocol <NSObject>

- (void)ifCircleAbroad:(Circle *)circle;

@end
