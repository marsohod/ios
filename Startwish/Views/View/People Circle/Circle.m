//
//  Circle.m
//  Startwish
//
//  Created by marsohod on 06/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "Circle.h"


#define DISTANCE_MOVE 50.0
#define SPEED 0.2

@implementation Circle

- (void)setCoordinates:(CGPoint)location
{
    self.x = location.x;
    self.y = location.y;
}

- (CGPoint)getCenter
{
    return CGPointMake(self.x + self.radius, self.y + self.radius);
}

- (void)setAngleMove:(float)angle
{
    _angleMove = angle;
    self.speedMove = CGPointMake(cosf(self.angleMove) * SPEED, sinf(self.angleMove) * SPEED);
}

- (NSMutableArray *)followingCircles
{
    if( !_followingCircles ) {
        _followingCircles = [[NSMutableArray alloc] init];
    }
    
    return _followingCircles;
}

- (void)startMove
{
//    [self addObserver:self forKeyPath:@"x" options:NSKeyValueObservingOptionNew context:nil];
//    CGRectIntersectsRect
    
//    [CATransaction begin];
//    [CATransaction setAnimationDuration:6.0f];
//    [self moveCircle];
//    
//    [CATransaction commit];
}

- (void)moveCircle
{
//    [CATransaction begin];
//    [CATransaction setAnimationDuration:2.0f];

    CGPoint step = [self moveStep];
    [self setCoordinates: CGPointMake(self.x + step.x, self.y + step.y)];
    self.addedLayer.frame = CGRectMake(self.x, self.y, self.addedLayer.frame.size.width, self.addedLayer.frame.size.height);
    [self.moveDelegate ifCircleAbroad:self];
    
//    [CATransaction commit];
    
    
    
}


- (void)moveCircleToEndOfMove
{
    [self setCoordinates: [self moveDestinationLocation]];
    self.addedLayer.frame = CGRectMake(self.x, self.y, self.addedLayer.frame.size.width, self.addedLayer.frame.size.height);
}

- (CGPoint)moveStep
{
//    NSLog(@"cos angle : %f angle: %f", cosf(self.angleMove), self.angleMove );
    return self.speedMove;
//    return CGPointMake(cosf(self.angleMove) * self.speedMove.x, sinf(self.angleMove) * self.speedMove.y);
}

- (CGPoint)moveDestinationLocation
{
    return CGPointMake(self.x + cosf(self.angleMove) * DISTANCE_MOVE, self.y + sinf(self.angleMove) * DISTANCE_MOVE);
}

- (void)followToCrossingCircle:(Circle *)circle
{
//    CGPoint crossPoint = [self crossPointWithCircle:circle];
//    CGPoint destinationLocation = [self moveDestinationLocation];
//    CGPoint circleDestinationLocation = [circle moveDestinationLocation];
    
    [self.followingCircles addObject:circle];
    
    // Если хоть один из вдух кругов будет двигаться к точке, то надо мониторить их движение
//    if( (fabsf(crossPoint.x - [self getCenter].x) >= fabsf(crossPoint.x - destinationLocation.x) &&
//        fabsf(crossPoint.y - [self getCenter].y) >= fabsf(crossPoint.y - destinationLocation.y)) ||
//        (fabsf(crossPoint.x - [circle getCenter].x) >= fabsf(crossPoint.x - circleDestinationLocation.x) &&
//        fabsf(crossPoint.y - [circle getCenter].y) >= fabsf(crossPoint.y - circleDestinationLocation.y))) {
//        [_followingCircles addObject:circle];
//    }
}

- (CGPoint)crossPointWithCircle:(Circle *)circle
{
    CGFloat K1 = tanf(M_PI * 2 - self.angleMove);
    CGFloat b1 = K1 * [self getCenter].x + [self getCenter].y;
    CGFloat K2 = tanf(M_PI * 2 - circle.angleMove);
    CGFloat b2 = K2 * [circle getCenter].x + [circle getCenter].y;
    CGFloat x = 0;
    CGFloat y = 0;
    
    if ( K1 - K2 == 0 ) {
        y = -b1;
    } else {
        x = (b1-b2)/(K1-K2);
        y = K1 * x - b1;
    }
    
    return CGPointMake(x, y);
}

- (void)calculateAngleAfterCrossWithCircle:(Circle *)circle
{
//    self.speedMove = CGPointMake(-self.speedMove.x, -self.speedMove.y);
//    circle.speedMove = CGPointMake(-circle.speedMove.x, -circle.speedMove.y);
//    CGFloat K = ([self getCenter].y - [circle getCenter].y) / ( [self getCenter].x - [circle getCenter].x);
//    CGFloat gamma = atanf(K);
//    
//    CGFloat U1x = cosf(self.angleMove - gamma) * self.radius;
//    CGFloat U1y = sinf(self.angleMove - gamma) * self.radius;
//    CGFloat U2x = -cosf(circle.angleMove - gamma) * circle.radius;
//    CGFloat U2y = sinf(circle.angleMove - gamma) * circle.radius;
//
//    CGFloat V1x = (U1x+U2x-(U1x-U2x))*0.5; // Сейчас найти новые скорости
//    CGFloat V2x = (U1x+U2x-(U2x-U1x))*0.5;
//    CGFloat V1 = sqrtf( powf(V1x, 2) + powf(U1y, 2));
//    CGFloat V2 = sqrtf( powf(V2x, 2) + powf(U2y, 2));
//    
//    CGFloat cosA = V1x * V1x / (V1x * V1);
//    CGFloat cosB = V2x * V2x / (V2x * V2);
//    
//    CGFloat newAngle1 = M_PI - acosf(cosA) + gamma;
//    CGFloat newAngle2 = M_PI - acosf(cosB) + gamma;
//    circle.angleMove = newAngle1 + (self.angleMove - circle.angleMove < 0 ? M_PI_2 : M_PI_2);
    CGFloat tempAngle = self.angleMove;
    self.angleMove = circle.angleMove;
    circle.angleMove = tempAngle;

}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    NSLog(@"View changed its geometry 222");
//}

@end
