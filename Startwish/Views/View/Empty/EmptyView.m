//
//  EmptyView.m
//  Startwish
//
//  Created by Pavel Makukha on 03/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "EmptyView.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"


@interface EmptyView()

@property (weak, nonatomic) IBOutlet UILabel *labelText;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet OrangeMiddleButton *buttonAction;
@property (weak, nonatomic) IBOutlet BackButton *buttonBack;
@property (weak, nonatomic) IBOutlet MenuButton *buttonMenu;

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionButton:(id)sender;
- (IBAction)actionMenuButton:(id)sender;

@end


@implementation EmptyView

- (instancetype)initToView:(UIView *)view withText:(NSString *)text desc:(NSString *)desc buttonTitle:(NSString *)buttonTitle;
{
    if ( self = [super init] ) {
        NSArray * nib = [[NSBundle mainBundle]
                         loadNibNamed: @"EmptyView"
                         owner: self
                         options: nil];
        
        self = nib[0];
        
        [self.labelText setText: text];
        [_labelDescription setText: desc];
        [_buttonAction setTitle:buttonTitle forState:UIControlStateNormal];
        
        [view addSubview:self];
    }
    
    return self;
}

- (void)show
{
    [self show:YES];
}

- (void)hide
{
    [self show:NO];
}

- (void)show:(BOOL)state
{
    [self show:state withButton:NO];
}

- (void)show:(BOOL)state withButton:(BOOL)willButtonShow
{
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.hidden = !state;
    
    if ( state ) {
        _buttonAction.hidden = willButtonShow ? NO : YES;
        _labelDescription.hidden = _labelDescription.text ? NO : YES;
        
        _buttonBack.hidden = _needMenuButton;
        _buttonMenu.hidden = !_needMenuButton;
    }
}

- (id)init
{
    self = [super init];
    
    return self;
}

- (void)awakeFromNib
{
    [_labelText setFont: [UIFont h_defaultFontLightSize: 30.0]];
    [_labelText setTextAlignment: NSTextAlignmentCenter];
    [_labelText setNumberOfLines: 0];
    [_labelText setTextColor: [UIColor whiteColor]];
    [_labelText setTextColor:[UIColor whiteColor]];
    
    [_labelDescription setFont: [UIFont h_defaultFontLightSize: 16.0]];
    [_labelDescription setTextAlignment: NSTextAlignmentCenter];
    [_labelDescription setNumberOfLines: 0];
    [_labelDescription setTextColor: [UIColor h_smoothDeepYellow]];
}


#pragma mark -
#pragma mark EmptyViewDelegate
- (IBAction)actionBackButton:(id)sender {
    [self.edelegate actionBackButton:sender];
}

- (IBAction)actionMenuButton:(id)sender
{
    [self.edelegate actionMenuButton:sender];
}

- (IBAction)actionButton:(id)sender {
    [self.edelegate actionButton:sender];
}
@end
