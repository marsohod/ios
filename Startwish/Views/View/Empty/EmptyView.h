//
//  EmptyView.h
//  Startwish
//
//  Created by Pavel Makukha on 03/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BGBlueView.h"
#import "OrangeMiddleButton.h"
#import "BackButton.h"
#import "MenuButton.h"


@protocol EmptyViewDelegate;


@interface EmptyView : BGBlueView

@property (weak, nonatomic) id<EmptyViewDelegate> edelegate;
@property (nonatomic) BOOL needMenuButton;

- (instancetype)initToView:(UIView *)view withText:(NSString *)text desc:(NSString *)desc buttonTitle:(NSString *)buttonTitle;

- (void)show;
- (void)hide;
- (void)show:(BOOL)state;
- (void)show:(BOOL)state withButton:(BOOL)willButtonShow;

@end


@protocol EmptyViewDelegate <NSObject>

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionMenuButton:(id)sender;
- (IBAction)actionButton:(id)sender;

@end
