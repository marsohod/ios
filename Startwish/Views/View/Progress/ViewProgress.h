//
//  ViewProgress.h
//  Startwish
//
//  Created by Pavel Makukha on 01.10.15.
//  Copyright © 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface ViewProgress : UIView

- (void)setValue:(CGFloat)percent;
- (void)show;
- (void)hide;

@end
