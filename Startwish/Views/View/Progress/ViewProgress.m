//
//  ViewProgress.m
//  Startwish
//
//  Created by Pavel Makukha on 01.10.15.
//  Copyright © 2015 marsohod. All rights reserved.
//


#import "ViewProgress.h"
#import "UIColor+Helper.h"

#define height 3.f
@implementation ViewProgress

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
//        self.hidden = YES;
        self.alpha = 0.0;
        self.backgroundColor = [UIColor h_notifYellow];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hide) name:@"STNotification" object:nil];
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setValue:(CGFloat)percent
{
    [self show];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.frame = CGRectMake(0.0,0.0, [UIScreen mainScreen].bounds.size.width * percent, height);
        [self setNeedsDisplay];
    });
}

- (void)show
{
    if ( self.alpha != 0.0 ) {
        return;
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
        [self setNeedsDisplay];
    } completion:^(BOOL finished) {
//        self.hidden = NO;
    }];
}

- (void)hide
{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
        [self setNeedsDisplay];
    } completion:^(BOOL finished) {
//        self.hidden = YES;
    }];
}

@end
