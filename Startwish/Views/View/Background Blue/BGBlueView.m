//
//  BGBlueView.m
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "BGBlueView.h"
#import "UIImage+Helper.h"


@implementation BGBlueView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ( self = [super initWithCoder:aDecoder] ) {
        self.contentMode = UIViewContentModeTopLeft;
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage resizeImage:[UIImage imageNamed:@"bg_blue"] toSize:self.frame.size]];
    }
    
    return self;
}

- (void) setFrame:(CGRect)frame
{
    // Call the parent class to move the view
    [super setFrame:frame];
    
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage resizeImage:[UIImage imageNamed:@"bg_blue"] toSize:self.frame.size]];
    // Do your custom code here.
}

- (void)drawRect:(CGRect)rect {
    
}


@end
