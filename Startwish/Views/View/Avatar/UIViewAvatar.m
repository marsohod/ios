//
//  UIViewAvatar.m
//  Startwish
//
//  Created by marsohod on 20/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "UIViewAvatar.h"
#import "UIColor+Helper.h"


@implementation UIViewAvatar

#define VIEW_WIDTH 36.0f
#define IMAGE_WIDTH 30.0f

#define MIDDLE_VIEW_WIDTH 85.0f
#define MIDDLE_IMAGE_WIDTH 70.0f

#define BIG_VIEW_WIDTH 175.0f
#define BIG_IMAGE_WIDTH 165.0f

#define VIEW_BORDER_WIDTH 2.0f
#define ANIMATE_DURATION 0.3f

+ (CGSize)avatarViewSize:(NSUInteger)type
{
    switch ( type ) {
        case 0:
            return CGSizeMake(VIEW_WIDTH, VIEW_WIDTH);
            break;
            
        case 1:
            return CGSizeMake(MIDDLE_VIEW_WIDTH, MIDDLE_VIEW_WIDTH);
            break;
            
        case 2:
            return CGSizeMake(BIG_VIEW_WIDTH, BIG_VIEW_WIDTH);
            break;
            
        default:
            break;
    }
    return CGSizeMake(VIEW_WIDTH, VIEW_WIDTH);
}

- (void)awakeFromNib
{
    self.imageView = [self.subviews count] > 0 ? self.subviews[0] : nil;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2;
    self.imageView.layer.masksToBounds = YES;
    [self setBackgroundColor:[UIColor clearColor]];
    self.layer.masksToBounds = YES;
    self.clipsToBounds = YES;
    
    self.layer.cornerRadius = self.frame.size.width / 2;
    [self.layer setBorderWidth: VIEW_BORDER_WIDTH];
    // Даём возвожность взаимодействия с пользователем
    // Например можно вешать события по Tap
    self.userInteractionEnabled = YES;
    [self customDrawView];
}


- (void)drawRect:(CGRect)rect
{
//    self.imageView.frame = CGRectMake(self.imageView.frame.origin.x, self.imageView.frame.origin.y, IMAGE_WIDTH, IMAGE_WIDTH);
}

- (void)customDrawView
{
    if ( self.isHighlighted ) {
        [self.layer setBorderColor: [UIColor h_smoothYellow].CGColor];
        
        if( self.imageView != nil ) {
            self.imageView.image = [UIImage imageNamed: [NSString stringWithFormat:@"bg_avatar_yellow_%ld.png", (long)self.imageView.frame.size.width]];
        }
        
    } else {
        [self.layer setBorderColor: [UIColor h_smoothGreen].CGColor];

        if( self.imageView != nil ) {
            self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_avatar_green_%ld.png", (long)self.imageView.frame.size.width]];
        }
    }
}

- (void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

- (void)animateAppearanceImage:(UIImage *)image duration:(float)duration
{
    _imageView.layer.opacity = 0;
    [self setImage:image];
    
    [UIView animateWithDuration:duration animations:^{
        _imageView.layer.opacity = 1.0;
    }];
}

- (void)animateAppearanceImage:(UIImage *)image
{
    [self animateAppearanceImage:image duration:ANIMATE_DURATION];
}


@end
