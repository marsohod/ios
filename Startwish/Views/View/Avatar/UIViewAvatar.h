//
//  UIViewAvatar.h
//  Startwish
//
//  Created by marsohod on 20/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewAvatar : UIView

@property(nonatomic) BOOL isHighlighted;
@property(nonatomic, weak) IBOutlet UIImageView *imageView;

+ (CGSize)avatarViewSize:(NSUInteger)type;

- (void)setImage:(UIImage *)image;
- (void)customDrawView;
- (void)animateAppearanceImage:(UIImage *)image;
- (void)animateAppearanceImage:(UIImage *)image duration:(float)duration;

@end
