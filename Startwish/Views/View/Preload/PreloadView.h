//
//  PreloadView.h
//  Startwish
//
//  Created by marsohod on 29/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface PreloadView : UIView

@property (strong, nonatomic) IBOutlet UIImageView *circle;
@property (strong, nonatomic) IBOutlet UIImageView *beast;
@property (strong, nonatomic) NSString *colorString;

- (void)renderPreloaderDependsSuperView;
- (instancetype)initWithStringColor:(NSString *)color;
- (void)hidePreload;
- (void)showPreload;
- (void)hide;

@end
