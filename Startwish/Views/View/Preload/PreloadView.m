//
//  PreloadView.m
//  Startwish
//
//  Created by marsohod on 29/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "PreloadView.h"
#import "UIColor+Helper.h"
#import "HTTPManager.h"


@implementation PreloadView

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if( self ) {
        self.colorString = @"white";
        [self setup];
    }
    
    return self;
}

- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        self.colorString = @"white";
        [self setup];
    }
    
    return self;
}

- (instancetype)initWithStringColor:(NSString *)color
{
    self = [super init];
    
    if( self ) {
        self.colorString = color;
        [self setup];
    }
    
    return self;
}

- (void)renderContraints
{
//    [self.superview addConstraint: [NSLayoutConstraint constraintWithItem: self
//                                                                attribute: NSLayoutAttributeTopMargin
//                                                                relatedBy: NSLayoutRelationEqual
//                                                                   toItem: self.superview
//                                                                attribute: NSLayoutAttributeTop
//                                                               multiplier: 1.0
//                                                                 constant: 0.0]];
//
//    [self.superview addConstraint: [NSLayoutConstraint constraintWithItem: self
//                                                                attribute: NSLayoutAttributeBottomMargin
//                                                                relatedBy: NSLayoutRelationEqual
//                                                                   toItem: self.superview
//                                                                attribute: NSLayoutAttributeBottom
//                                                               multiplier: 1.0
//                                                                 constant: 0.0]];
//    
//    [self.superview addConstraint: [NSLayoutConstraint constraintWithItem: self
//                                                                attribute: NSLayoutAttributeLeading
//                                                                relatedBy: NSLayoutRelationEqual
//                                                                   toItem: self.superview
//                                                                attribute: NSLayoutAttributeLeft
//                                                               multiplier: 1.0
//                                                                 constant: 0.0]];
//    
//    [self.superview addConstraint: [NSLayoutConstraint constraintWithItem: self
//                                                                attribute: NSLayoutAttributeTrailing
//                                                                relatedBy: NSLayoutRelationEqual
//                                                                   toItem: self.superview
//                                                                attribute: NSLayoutAttributeRight
//                                                               multiplier: 1.0
//                                                                 constant: 0.0]];
}

- (void)setup
{
    UIColor *bgColor = [UIColor whiteColor];
    
    self.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    if( [self.colorString isEqualToString: @"gray"] ) {
        bgColor = [UIColor h_bg_collectionView];
    } else if ( [self.colorString isEqualToString: @"transparent"] ) {
        bgColor = [UIColor clearColor];
    } else if ( [self.colorString isEqualToString: @"black"] ) {
        bgColor = [UIColor blackColor];
    }
    
    [self setBackgroundColor: bgColor];
    [self addLogo];
    [self addCircle];
//    [self startAnimate];
}

#define logoHeight 70.0
#define circleHeight 85.0

- (void)addLogo
{
    self.beast = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - logoHeight) / 2, (self.frame.size.height - logoHeight) / 2, logoHeight, logoHeight)];
    self.beast.image = [UIImage imageNamed:@"loading_beast.png"];
    [self addSubview:self.beast];
}

- (void)addCircle
{
    self.circle = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - circleHeight) / 2, (self.frame.size.height - circleHeight) / 2, circleHeight, circleHeight)];
    self.circle.image = [UIImage imageNamed: [NSString stringWithFormat:@"circle_opacity_rainbow_%@_small.png", self.colorString]];
    [self addSubview:self.circle];
}

- (void)renderPreloaderDependsSuperView
{
    self.circle.frame = CGRectMake((self.superview.frame.size.width - circleHeight) / 2, (self.superview.frame.size.height - circleHeight) / 2, circleHeight, circleHeight);
    self.beast.frame = CGRectMake((self.superview.frame.size.width - logoHeight) / 2, (self.superview.frame.size.height - logoHeight) / 2, logoHeight, logoHeight);
}

- (void)startAnimate
{
    [CATransaction begin];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 20.0 * 2.0 ];
    rotationAnimation.duration = 20.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1;
//    rotationAnimation.removedOnCompletion = YES;
    
    [CATransaction setCompletionBlock:^{
        [self hidePreload];
    }];
    
    [self.circle.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [CATransaction commit];
}

- (void)hidePreload
{
    if( self.layer.opacity != 0.0 ) {
        [UIView animateWithDuration:0.4 animations:^{
            self.layer.opacity = 0.0;
        } completion:^(BOOL finished) {
            self.hidden = YES;
            [self.circle.layer removeAllAnimations];
        }];
    }
}

- (void)showPreload
{
    if ( ![HTTPManager sharedInstance].isInternetConnectionAvailable ) {
        [self hidePreload];
        return;
    }
    
    [self startAnimate];
    
    [UIView animateWithDuration:0.2 animations:^{
        self.hidden = NO;
        self.layer.opacity = 1.0;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide
{
    self.hidden = YES;
    self.layer.opacity = 0.0;
}

@end
