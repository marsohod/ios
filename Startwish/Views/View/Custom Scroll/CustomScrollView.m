//
//  CustomScrollView.m
//  Startwish
//
//  Created by marsohod on 17/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CustomScrollView.h"
#import "SevenSwitch.h"


@implementation CustomScrollView

- (BOOL) touchesShouldCancelInContentView:(UIView *)view {
    return NO;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    if ( [view isKindOfClass:[SevenSwitch class]] ) {
        [(SevenSwitch *)view beginTrackingWithTouch:(UITouch*)[touches anyObject] withEvent:event];
        [(SevenSwitch *)view continueTrackingWithTouch:(UITouch*)[touches anyObject] withEvent:event];
        [(SevenSwitch *)view endTrackingWithTouch:(UITouch*)[touches anyObject] withEvent:event];
        return NO;
    }
    
    return YES;
}

@end
