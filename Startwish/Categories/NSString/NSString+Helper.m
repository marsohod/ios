//
//  NSString+Helper.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "NSString+Helper.h"


@implementation NSString (Helper)

#define dateFormat @"yyyy-MM-dd'T'HH:mm:ssZZZZZ"

+ (BOOL)h_isEmailString:(NSString *)text
{
    NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:@"[0-9a-z._%+-]+@[a-z0-9.-_]+\\.[a-z]{2,5}" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if( [regExp numberOfMatchesInString:text options:0 range:NSMakeRange(0, [text length])] == 1 ) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)h_isUrlString:(NSString *)text
{
    NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:@"^((https?://(www\\.)?)|(www\\.))([a-zа-я0-9_+-]+\\.)+[a-zа-я0-9]{2,6}/?([a-zа-я0-9_+-]+(\\.[a-zа-я]+)?)?$" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if( [regExp numberOfMatchesInString:text options:0 range:NSMakeRange(0, [text length])] == 1 ) {
        return YES;
    }
    
    return NO;
}

// @num - number
// @ends - array of 3 values. Numerals == @[1, 2, 10]
+ (NSString *)h_russianNumeralFromInteger:(NSInteger)num ends:(NSArray *)ends
{
    NSString * russianNumeral;
    NSInteger i;
    
    num = num % 100;
    
    if ( num >= 11 && num <= 19 ) {
        russianNumeral = ends[2];
    }
    else {
        i = num % 10;
        switch ( i ) {
            case (1): russianNumeral = ends[0]; break;
            case (2):
            case (3):
            case (4): russianNumeral = ends[1]; break;
            default: russianNumeral = ends[2];
        }
    }
    
    return russianNumeral;
}

+ (NSString *)h_russianNumeralWithInteger:(NSInteger)num ends:(NSArray *)ends
{
    return [NSString stringWithFormat:@"%ld %@", (long)num, [NSString h_russianNumeralFromInteger:num ends:ends]];
}

+ (NSString *)h_howSpendTimeToMs:(double)sec
{
    NSInteger min     = 60;
    NSInteger hour    = min * 60;
    NSInteger day     = hour * 24;
    NSInteger week    = day * 7;
    NSInteger year    = day * 30 * 12;
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setRoundingMode:NSNumberFormatterRoundHalfEven];

    if ( sec / year >= 1 ) {
        return [NSString stringWithFormat:@"%ld %@", (long)floor(sec / year), @"г."];
    } else if ( sec / week >= 1) {
        return [NSString stringWithFormat:@"%ld %@", (long)floor(sec / week), @"нед."];
    } else if ( sec / day >= 1) {
        return [NSString stringWithFormat:@"%ld %@", (long)floor(sec / day), @"дн."];
    } else if ( sec / hour >= 1) {
        return [NSString stringWithFormat:@"%ld %@", (long)floor(sec / hour), @"ч."];
    } else if ( sec / min >= 1) {
        return [NSString stringWithFormat:@"%ld %@", (long)floor(sec / min), @"мин."];
    } else {
        return [NSString stringWithFormat:@"%ld %@", (long)sec, @"сек."];
    }
    
    return @"";
}

+ (NSString *)h_howSpendFullTimeToMs:(double)sec
{
    NSInteger min     = 60;
    NSInteger hour    = min * 60;
    NSInteger day     = hour * 24;
    NSInteger month   = day * 30;
    NSInteger year    = month * 12;
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setRoundingMode:NSNumberFormatterRoundHalfEven];
    
    if ( sec / year >= 1 ) {
        return [NSString stringWithFormat:@"%@ %@", [NSString h_russianNumeralWithInteger: floor(sec / year) ends: @[@"год", @"года", @"лет"]], [NSString h_howSpendFullTimeToMs:(fmod(sec, year))]];
    } else if ( sec / month >= 1) {
        return [NSString h_russianNumeralWithInteger: floor(sec / month) ends: @[@"месяц", @"месяца", @"месяцев"]];
    } else if ( sec / day >= 1) {
        return [NSString h_russianNumeralWithInteger: floor(sec / day) ends: @[@"день", @"дня", @"дней"]];
    } else if ( sec / hour >= 1) {
        return [NSString h_russianNumeralWithInteger: floor(sec / hour) ends: @[@"час", @"часа", @"часов"]];
    } else if ( sec / min >= 1) {
        return [NSString h_russianNumeralWithInteger: floor(sec / min) ends: @[@"минуту", @"минуты", @"минут"]];
    } else {
        return [NSString h_russianNumeralWithInteger: (NSInteger)sec ends: @[@"ceкунду", @"секунды", @"секунд"]];
    }
    
    return @"";
}

+ (CGFloat)h_daysInSec:(double)sec
{
    NSInteger day = 3600 * 24;
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    [f setRoundingMode:NSNumberFormatterRoundHalfEven];
    
    return roundf(sec / day);
}

+ (double)h_secondFromDate:(NSString *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    NSDate *dateFromString = [df dateFromString: date];
//    NSLog(@"%@ %f %@", date, [NSDate timeIntervalSinceReferenceDate] - [dateFromString timeIntervalSinceReferenceDate], [NSTimeZone localTimeZone]);
    return ([NSDate timeIntervalSinceReferenceDate] - [dateFromString timeIntervalSinceReferenceDate]);
}

+ (NSString *)h_timeAgoFromSecond:(double)secondToEvent
{
    return [NSString stringWithFormat:@"%@ назад", [NSString h_howSpendTimeToMs:secondToEvent]];
}

+ (NSString *)h_timeFromSecond:(double)secondToEvent
{
    return [NSString h_howSpendTimeToMs:secondToEvent];
}

+ (NSString *)h_timeFullFromSecond:(double)secondToEvent
{
    return [NSString h_howSpendFullTimeToMs:secondToEvent];
}

+ (NSString *)h_timeAgoUserFriendlyFromStringDate:(NSString *)date
{
//    NSLog(@"%@", [NSString timeAgoFromSecond:[NSString secondFromDate:date]]);
    return [NSString h_timeFromSecond:[NSString h_secondFromDate:date]];
}

+ (NSString *)h_timeFullUserFriendlyFromStringDate:(NSString *)date
{
    return [NSString h_timeFullFromSecond:[NSString h_secondFromDate:date]];
}



+ (NSString *)h_dateToUserFriendlyFromStringDate:(NSString *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    [df setDateFormat:dateFormat];
    
    NSDate *dateFromString = [df dateFromString: date];
    [df setDateFormat:@"d MMMM yyyy"];
    
    return [df stringFromDate: dateFromString];
}

+ (NSString *)h_dateNow
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
    return [df stringFromDate:[[NSDate alloc] init]];
}

+ (CGFloat)h_daysFromNowToDateSting:(NSString *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:dateFormat];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    NSDate *dateFromString = [df dateFromString: date];
    
    NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:dateFromString];
    
    return diff / (3600 * 24);
}

- (NSString *)urlencode {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[self UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

+ (uint32_t)stringToUint32_t:(id)string
{
    uint32_t n;
    
    if ( !self ) {
        return 0;
    }
    
    if( [string isKindOfClass:[NSNumber class]] ) {
        n = (uint32_t)[string unsignedLongValue];
    } else {
        sscanf([string UTF8String], "%u", &n);
    }
    
    return n;
}

+ (NSString *)randomStringWithLength:(NSInteger)len
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (NSInteger i = 0; i < len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((uint32_t)[letters length])]];
    }
    
    return randomString;
}
@end
