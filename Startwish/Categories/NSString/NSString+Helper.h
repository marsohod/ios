//
//  NSString+Helper.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface NSString (Helper)

+ (BOOL)h_isEmailString:(NSString *)text;
+ (BOOL)h_isUrlString:(NSString *)text;

+ (NSString *)h_russianNumeralWithInteger:(NSInteger)num ends:(NSArray *)ends;
+ (NSString *)h_russianNumeralFromInteger:(NSInteger)num ends:(NSArray *)ends;
+ (NSString *)h_howSpendTimeToMs:(double)sec;
+ (double)h_secondFromDate:(NSString *)date;

+ (CGFloat)h_daysInSec:(double)sec;
+ (NSString *)h_timeFromSecond:(double)secondToEvent;
+ (NSString *)h_timeAgoFromSecond:(double)secondToEvent;
+ (NSString *)h_timeAgoUserFriendlyFromStringDate:(NSString *)date;
+ (NSString *)h_timeFullUserFriendlyFromStringDate:(NSString *)date;

+ (NSString *)h_dateToUserFriendlyFromStringDate:(NSString *)date;
+ (NSString *)h_dateNow;
+ (CGFloat)h_daysFromNowToDateSting:(NSString *)date;
- (NSString *)urlencode;

+ (uint32_t)stringToUint32_t:(id)string;

+ (NSString *)randomStringWithLength:(NSInteger)len;

@end
