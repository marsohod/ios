//
//  UITableViewCell+Helper.h
//  Startwish
//
//  Created by marsohod on 17/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UITableViewCell (Helper)

- (void)h_setSeparatorInsetZero;

@end
