//
//  UITableViewCell+Helper.m
//  Startwish
//
//  Created by marsohod on 17/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UITableViewCell+Helper.h"


@implementation UITableViewCell (Helper)

- (void)h_setSeparatorInsetZero
{
    if( [self respondsToSelector:@selector(setSeparatorInset:)] ) {
        [self setSeparatorInset: UIEdgeInsetsZero];
    }

    if( [self respondsToSelector:@selector(setLayoutMargins:)] ) {
        [self setLayoutMargins: UIEdgeInsetsZero];
    }
}
@end
