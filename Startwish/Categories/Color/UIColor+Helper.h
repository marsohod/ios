//
//  UIColor+Helper.h
//  Startwish
//
//  Created by marsohod on 22/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIColor (Helper)

+ (UIColor *)h_randomColor;
+ (UIColor *)h_randomWishBackgroundColor;
+ (UIColor *)h_yellow;
+ (UIColor *)h_yellowStrong;
+ (UIColor *)h_red;
+ (UIColor *)h_orange;
+ (UIColor *)h_orange2;
+ (UIColor *)h_orange_middle;
+ (UIColor *)h_smoothShallowYellow;
+ (UIColor *)h_smoothYellow;
+ (UIColor *)h_smoothDeepYellow;
+ (UIColor *)h_bg_contacts;
+ (UIColor *)h_bg_contacts_border;
+ (UIColor *)h_smoothGreen;
+ (UIColor *)h_smoothDeepGreen;
+ (UIColor *)h_greenDirty;
+ (UIColor *)h_gray;
+ (UIColor *)h_blue;
+ (UIColor *)h_bg_collectionView;
+ (UIColor *)h_bg_commentView;
+ (UIColor *)h_bg_commentViewBorder;
+ (UIColor *)h_bg_profile_stats;
+ (UIColor *)h_bg_profile_circle;
+ (UIColor *)h_switchOn;
+ (UIColor *)h_switchOff;
+ (UIColor *)h_notifYellow;

#pragma mark - Filters Colors
+ (UIColor *)h_coral;
+ (UIColor *)h_mellowYellow;
+ (UIColor *)h_apricot;
+ (UIColor *)h_mayfair;
+ (UIColor *)h_amaro;
+ (UIColor *)h_xpro;
+ (UIColor *)h_valencia;
+ (UIColor *)h_rise;
+ (UIColor *)h_lofi;
+ (UIColor *)h_sierra;
+ (UIColor *)h_hudson;
+ (UIColor *)h_earlybird;

@end
