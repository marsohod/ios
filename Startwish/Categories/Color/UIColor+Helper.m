//
//  UIColor+Helper.m
//  Startwish
//
//  Created by marsohod on 22/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIColor+Helper.h"

#define RGB(r, g, b) [UIColor colorWithRed:(r)/255.f green:(g)/255.f blue:(b)/255.f alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:(r)/255.f green:(g)/255.f blue:(b)/255.f alpha:(a)]

@implementation UIColor (Helper)

+ (UIColor *)h_randomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

+ (UIColor *)h_randomWishBackgroundColor
{

    NSArray *colors = [[NSArray alloc] initWithObjects:
        RGB(255, 191, 170),
        RGB(255, 234, 170),
        RGB(255, 250, 170),
        RGB(236, 255, 170),
        RGB(220, 255, 170),
        RGB(170, 232, 255),
        RGB(170, 178, 255),
        RGB(198, 170, 255),
        RGB(244, 170, 255),
        RGB(255, 170, 184),
        RGB(252, 193, 129),
        RGB(78, 86, 97),
        RGB(103, 186, 169),
        RGB(67, 176, 177), nil];
    
    return colors[ arc4random_uniform((uint32_t)([colors count] - 1) ) ];
}

+ (UIColor *)h_yellow
{
    return RGB(255, 168, 0);
}

+ (UIColor *)h_yellowStrong
{
    return RGB(255, 242, 0);
}

+ (UIColor *)h_red
{
    return RGB(255, 72, 0);
}

+ (UIColor *)h_orange
{
    return RGB(249.0, 105.0, 19.0);
}

+ (UIColor *)h_orange2
{
    return RGB(255, 96, 0);
}

+ (UIColor *)h_orange_middle
{
    return RGB(255, 138, 0);
}

+ (UIColor *)h_smoothShallowYellow
{
    return RGB(255.0, 255.0, 216.0);
}

+ (UIColor *)h_smoothYellow
{
    return RGB(255.0, 255.0, 191.0);
}

+ (UIColor *)h_smoothDeepYellow
{
    return RGB(255.0, 244.0, 192.0);
}

+ (UIColor *)h_bg_contacts
{
    return RGB(255.0, 255.0, 229.0);
}

+ (UIColor *)h_bg_contacts_border
{
    return RGB(236.0, 236.0, 236.0);
}

+ (UIColor *)h_smoothGreen
{
    return RGB(0.0, 173.0, 179.0);
}

+ (UIColor *)h_smoothDeepGreen
{
    return RGB(0.0, 136.0, 165.0);
}

+ (UIColor *)h_greenDirty
{
    return RGB(36.0, 191.0, 76.0);
}

+ (UIColor *)h_bg_collectionView
{
    return RGB(191.0, 191.0, 191.0);
}

+ (UIColor *)h_bg_commentView
{
    return RGB(242.0, 242.0, 242.0);
}

+ (UIColor *)h_bg_commentViewBorder
{
    return RGB(235.0, 235.0, 235.0);
}

+ (UIColor *)h_bg_profile_stats
{
    return RGBA(3.0, 189.0, 196.0, 0.3);
}

+ (UIColor *)h_bg_profile_circle
{
    return RGB(3.0, 189.0, 196.0);
}

+ (UIColor *)h_switchOn
{
    return RGB(0.0, 204.0, 200.0);
}

+ (UIColor *)h_switchOff
{
    return RGB(0.0, 204.0, 200.0);
}

+ (UIColor *)h_notifYellow;
{
    return RGB(255.0, 191.0, 0.0);
}

+ (UIColor *)h_gray
{
    return RGB(153.0, 153.0, 153.0);
}

+ (UIColor *)h_blue
{
    return RGB(86.0, 158.0, 234.0);
}


#pragma mark - Filters Colors

+ (UIColor *)h_coral
{
    return RGB(255.f, 127.f, 80.f);
}

+ (UIColor *)h_mellowYellow
{
    return RGB(248.f, 222.f, 126.f);
}

+ (UIColor *)h_apricot
{
    return RGB(251.f, 206.f, 177.f);
}

+ (UIColor *)h_mayfair
{
    return RGB(243.f, 183.f, 161.f);
}

// Dark slate blue
+ (UIColor *)h_amaro
{
    return RGB(72.f, 61.f, 139.f);
}

// June bud
+ (UIColor *)h_xpro
{
    return RGB(189.f, 218.f, 87.f);
}

// Light Khaki
+ (UIColor *)h_valencia
{
    return RGB(240.f, 230.f, 140.f);
}

// Pastel orange
+ (UIColor *)h_rise
{
    return RGB(255.f, 179.f, 71.f);
}

// Eggplant
+ (UIColor *)h_lofi
{
    return RGB(97.f, 64.f, 81.f);
}

+ (UIColor *)h_sierra
{
    return [self h_coral];
}

// Glaucous
+ (UIColor *)h_hudson
{
    return RGB(96.f, 130.f, 182.f);
}

// Amber
+ (UIColor *)h_earlybird
{
    return RGB(255.f, 191.f, 0.f);
}


@end
