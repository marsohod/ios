//
//  UIImage+Helper.h
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef void (^downloadCompleteImage)(UIImage *image);
typedef void (^downloadCompleteImageWithIdentify)(UIImage *image, uint32_t identify);

@interface UIImage (Helper)

+ (void)rotationImage:(UIImageView *)image duration:(NSTimeInterval)duration
                curve:(int)curve degrees:(CGFloat)degrees;

// Просто вытаскиваем
+ (void)downloadWithUrl:(NSURL *)url onCompleteChange:(downloadCompleteImage)onComplete;

// Пережимаем картинку для сохранения в кэш
+ (void)downloadWithUrl:(NSURL *)url toCacheWithSize:(CGSize)imageSize onCompleteChange:(downloadCompleteImage)onComplete;

// Вытаскиваем с идентификатором, чтобы учесть замыкания метода
+ (void)downloadWithUrl:(NSURL *)url withIdentify:(uint32_t)identify onCompleteChange:(downloadCompleteImageWithIdentify)onComplete;

// Вытаскиваем с идентификатором, чтобы учесть замыкания метода и сохраним пережатую картинку
+ (void)downloadWithUrl:(NSURL *)url withIdentify:(uint32_t)identify toCacheWithSize:(CGSize)imageSize onCompleteChange:(downloadCompleteImageWithIdentify)onComplete;

+ (UIImage *)makeRoundedImage:(UIImage *) image radius: (float) radius;
+ (UIImage *)makeRoundedImage:(UIImage *)image radius:(float)radius withBorderColor:(UIColor *)bColor width:(CGFloat)bWidth;

+ (id)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color font:(UIFont*)font;
+ (id)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color font:(UIFont*)font position:(CGPoint)position;
//+ (id)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color position:;

+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size;
+ (UIImage *)resizeImage:(UIImage *)image toRect:(CGRect)rect;
+ (UIImage *)emptyCover;

- (UIImage*)scaleToSize:(CGSize)size;
- (UIImage *)resizedImage:(CGImageRef)imageRef
                     size:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality;

- (UIImage *)fixOrientation;
- (UIImage *)h_imageWithAlpha:(CGFloat) alpha;

+ (UIImage *)applyAspectFillImage:(UIImage *)image InRect:(CGRect)bounds;
+ (UIImage *)h_imageWithColor:(UIColor *)color size:(CGSize)size;
+ (CGFloat)h_max_qualityOfImage:(UIImage *)image;
+ (CGFloat)h_max_qualityOfImage:(UIImage *)image afterQuality:(CGFloat)q;

@end
