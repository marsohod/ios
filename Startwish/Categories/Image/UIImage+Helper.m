//
//  UIImage+Helper.m
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIImage+Helper.h"
#import "UIImageView+WebCache.h"


@implementation UIImage (Helper)

#define maxUploadImageSize 8.0

CGRect CGRectCenteredInRect(CGRect rect, CGRect mainRect)
{
    CGFloat xOffset = CGRectGetMidX(mainRect)-CGRectGetMidX(rect);
    CGFloat yOffset = CGRectGetMidY(mainRect)-CGRectGetMidY(rect);
    return CGRectOffset(rect, xOffset, yOffset);
}


// Calculate the destination scale for filling
CGFloat CGAspectScaleFill(CGSize sourceSize, CGRect destRect)
{
    CGSize destSize = destRect.size;
    CGFloat scaleW = destSize.width / sourceSize.width;
    CGFloat scaleH = destSize.height / sourceSize.height;
    return MAX(scaleW, scaleH);
}


CGRect CGRectAspectFillRect(CGSize sourceSize, CGRect destRect)
{
    CGSize destSize = destRect.size;
    CGFloat destScale = CGAspectScaleFill(sourceSize, destRect);
    CGFloat newWidth = sourceSize.width * destScale;
    CGFloat newHeight = sourceSize.height * destScale;
    CGFloat dWidth = ((destSize.width - newWidth) / 2.0f);
    CGFloat dHeight = ((destSize.height - newHeight) / 2.0f);
    CGRect rect = CGRectMake (dWidth, dHeight, newWidth, newHeight);
    return rect;
}

#define M_PI   3.14159265358979323846264338327950288   /* pi */
#define DEGREES_TO_RADIANS(angle) (angle / 180.0 * M_PI)
#define RADIANS(degrees) ((degrees * M_PI) / 180.0)

+ (void)rotationImage:(UIImageView *)image duration:(NSTimeInterval)duration
              curve:(int)curve degrees:(CGFloat)degrees
{
//    CGAffineTransform startAnimationTransform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(0.0));
//    CGAffineTransform endAnimationTransform = CGAffineTransformRotate(CGAffineTransformIdentity, RADIANS(0.0));
    
//    image.transform = startAnimationTransform;  // starting point
    
    [UIView beginAnimations:@"wobble" context:(__bridge void *)(image)];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationRepeatCount:1]; // adjustable
    [UIView setAnimationDuration:duration];
    [UIView setAnimationDelegate:image];
    image.transform = CGAffineTransformMakeRotation(M_PI); // end here & auto-reverse
    
    [UIView commitAnimations];
}


- (UIImage*)scaleToSize:(CGSize)size
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    
    [self drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return scaledImage;
}

+ (void)downloadWithUrl:(NSURL *)url onCompleteChange:(downloadCompleteImage)onComplete
{
    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {

        if (image)
        {
            onComplete(image);
        }
    }];
}

+ (void)downloadWithUrl:(NSURL *)url withIdentify:(uint32_t)identify onCompleteChange:(downloadCompleteImageWithIdentify)onComplete
{
    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        if (image)
        {
            onComplete(image, identify);
        }
    }];
}

+ (void)downloadWithUrl:(NSURL *)url withIdentify:(uint32_t)identify toCacheWithSize:(CGSize)imageSize onCompleteChange:(downloadCompleteImageWithIdentify)onComplete
{
    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        UIImage *scaledImage = [image scaleToSize:imageSize];
        [[SDImageCache sharedImageCache] storeImage:scaledImage forKey:[NSString stringWithFormat:@"%@", url]];
        onComplete(scaledImage, identify);
    }];
}

+ (void)downloadWithUrl:(NSURL *)url toCacheWithSize:(CGSize)imageSize onCompleteChange:(downloadCompleteImage)onComplete
{
    [[SDWebImageManager sharedManager] downloadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        
        if (image)
        {
//            UIImage *scaledImage = [image resizedImage:image.CGImage
//                                                  size:imageSize
//                                             transform:CGAffineTransformIdentity
//                                        drawTransposed:NO
//                                  interpolationQuality:kCGInterpolationHigh];
            
            UIImage *scaledImage = [image scaleToSize:imageSize];
            [[SDImageCache sharedImageCache] storeImage:scaledImage forKey:[NSString stringWithFormat:@"%@", url]];
            onComplete(scaledImage);
        }
    }];
}

+ (UIImage *)makeRoundedImage:(UIImage *)image radius:(float)radius
{
    return [UIImage makeRoundedImage:image radius:radius withBorderColor:nil width:0.f];
}

+ (UIImage *)makeRoundedImage:(UIImage *)image radius:(float)radius withBorderColor:(UIColor *)bColor width:(CGFloat)bWidth
{
    CALayer *imageLayer = [CALayer layer];
    CGSize roundedImageSize = CGSizeMake(image.size.width, image.size.height);
    
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = image.size.width/2;
    
    if ( bColor ) {
        imageLayer.borderColor = bColor.CGColor;
        imageLayer.borderWidth = bWidth;
    }
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(roundedImageSize, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(roundedImageSize);
    }

    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;//[roundedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
}


+ (id)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color font:(UIFont*)font
{
    return [UIImage imageFromImage:image string:string color:color font:font position:CGPointMake(0,0)];
}

+ (id)imageFromImage:(UIImage*)image string:(NSString*)string color:(UIColor*)color font:(UIFont*)font position:(CGPoint)position
{
    
    CGSize expectedTextSize = [string sizeWithAttributes:@{NSFontAttributeName: font}];
    int width = expectedTextSize.width;
    int height = expectedTextSize.height + image.size.height + 15;
    
    CGSize size = CGSizeMake((float)width, (float)height);
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if ( context ) {
        CGContextSetFillColorWithColor(context, color.CGColor);
        [string drawAtPoint:position withAttributes:@{NSFontAttributeName: font}];

        // Images upside down so flip them
        CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, size.height);
        CGContextConcatCTM(context, flipVertical);
        CGContextDrawImage(context, (CGRect){ { ( size.width - image.size.width )/2 , 0}, {image.size.width, image.size.height} }, [image CGImage]);
    } else {
    
    }
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)emptyCover
{
    return [UIImage imageNamed:@"break_cover.png"];
}



- (UIImage *)resizedImage:(CGImageRef)imageRef
                     size:(CGSize)newSize
                transform:(CGAffineTransform)transform
           drawTransposed:(BOOL)transpose
     interpolationQuality:(CGInterpolationQuality)quality {
    
    CGRect newRect;
    if ([self respondsToSelector:@selector(scale)])
        newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width * self.scale, newSize.height * self.scale));
    else
        newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectMake(0, 0, newRect.size.height, newRect.size.width);
    //CGImageRef imageRef = self.CGImage;
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                0,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    UIImage *newImage;
    if ([self respondsToSelector:@selector(scale)] && [UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)]) {
        newImage = [UIImage imageWithCGImage:newImageRef scale:self.scale orientation:self.imageOrientation];
    } else {
        newImage = [UIImage imageWithCGImage:newImageRef];
    }
    
    
    // Clean up
    CGContextRelease(bitmap);
    CGImageRelease(newImageRef);
    
    return newImage;
}

+ (UIImage *)resizeImage:(UIImage *)image toSize:(CGSize)size
{
    return [self resizeImage:image toRect:CGRectMake(0, 0, size.width, size.height)];
}

+ (UIImage *)resizeImage:(UIImage *)image toRect:(CGRect)rect
{
    UIGraphicsBeginImageContext( rect.size );
    [image drawInRect:rect];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


+ (UIImage *)applyAspectFillImage:(UIImage *)image InRect:(CGRect)bounds
{
    CGRect destRect;
    
    UIGraphicsBeginImageContext(bounds.size);
    CGRect rect = CGRectAspectFillRect(image.size, bounds);
    destRect = CGRectCenteredInRect(rect, bounds);
    
    [image drawInRect: destRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)fixOrientation {
    
    // No-op if the orientation is already correct
    if (self.imageOrientation == UIImageOrientationUp) return self;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    
    return img;
}

+ (UIImage *)h_imageWithColor:(UIColor *)color size:(CGSize)size
{
    if ( CGSizeEqualToSize(CGSizeZero, size) ) {
        size = CGSizeMake(1.f, 1.f);
    }
    
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, CGRectMake(0.f, 0.f, size.width, size.height));
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)h_imageWithAlpha:(CGFloat) alpha
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(self.size);
    }
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, self.size.width, self.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, self.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (CGFloat)h_max_qualityOfImage:(UIImage *)image
{
    return [self h_max_qualityOfImage:image afterQuality:1];
}

+ (CGFloat)h_max_qualityOfImage:(UIImage *)image afterQuality:(CGFloat)q
{
    NSData *imgData = UIImageJPEGRepresentation(image, q);
    
    if ( [imgData length] / (1000*1024) > maxUploadImageSize ) {
        return [self h_max_qualityOfImage:image afterQuality:q-0.1];
    }
    
    return q;
}

@end
