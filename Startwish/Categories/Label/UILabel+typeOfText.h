//
//  UILabel+typeOfText.h
//  Startwish
//
//  Created by marsohod on 08/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (typeOfText)

- (void)tt_initCommentAttributes;
- (void)tt_initUsernameAttributes;

// Устанавливаем иконку слева от текста
+ (NSMutableAttributedString *)tt_label: (NSString *)text withImage: (UIImage *)image bounds:(CGRect)bounds padding: (NSString *)padding left:(BOOL)isLeftSide;

// Предполагаемые размеры label
+ (CGSize)rectSizeString:(NSString*)string byWidth:(float)width maxHeight:(float)maxHeight fontName:(NSString *)fontName fontSize:(float)fontSize;

@end
