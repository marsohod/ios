//
//  UILabel+typeOfText.m
//  Startwish
//
//  Created by marsohod on 08/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UILabel+typeOfText.h"


@implementation UILabel (typeOfText)

- (void)tt_initUsernameAttributes
{
    self.textColor = [UIColor blueColor];
    self.numberOfLines = 0;
    [self setFont: [UIFont systemFontOfSize:12.0]];
}

- (void)tt_initCommentAttributes
{
    self.textColor = [UIColor blackColor];
    self.numberOfLines = 0;
    [self setFont: [UIFont systemFontOfSize:12.0]];
}


// Устанавливаем иконку слева от текста
// text - тест справа от иконки
// image - изображение иконки
// bounds - размеры иконки
// padding - отступ текста справа от иконки

+ (NSMutableAttributedString *)tt_label: (NSString *)text withImage: (UIImage *)image bounds:(CGRect)bounds padding: (NSString *)padding left:(BOOL)isLeftSide
{
    NSTextAttachment *icon = [[NSTextAttachment alloc] init];
    icon.image = image;
    icon.bounds = bounds;
    
    NSAttributedString *stringIcon = [NSAttributedString attributedStringWithAttachment:icon];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString: isLeftSide ? [padding stringByAppendingString: text] : [text stringByAppendingString: padding]];
    
    [string insertAttributedString: stringIcon atIndex: isLeftSide ? 0 : string.length];
    
    return string;
}

// Предполагаемые размеры label
+ (CGSize)rectSizeString:(NSString*)string byWidth:(float)width maxHeight:(float)maxHeight fontName:(NSString *)fontName fontSize:(float)fontSize
{
    
    UILabel  * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 9999)];
    label.numberOfLines = 0;
    label.font = [UIFont fontWithName:fontName size:fontSize];
    label.text = string;
    
    CGSize maximumLabelSize = CGSizeMake(width, 9999);
    CGSize expectedSize = [label sizeThatFits:maximumLabelSize];
    return expectedSize;
    
//    CGSize maximumLabelSize = CGSizeMake(width, maxHeight);
//    NSDictionary *stringAttributes = [NSDictionary dictionaryWithObject: [UIFont fontWithName:fontName size:fontSize] forKey: NSFontAttributeName];
//    
//    CGSize expectedLabelSize = [string boundingRectWithSize: maximumLabelSize
//                                                    options: NSStringDrawingUsesLineFragmentOrigin
//                                                 attributes: stringAttributes
//                                                    context: nil].size;
    
//    return expectedLabelSize;
}

@end
