//
//  UIScrollView+Helper.m
//  Startwish
//
//  Created by marsohod on 16/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "UIScrollView+Helper.h"


@implementation UIScrollView (Helper)

- (void)h_redrawHeight
{
    self.contentSize = [self h_contentHeight].size;
}

- (CGRect)h_contentHeight
{
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    
    return contentRect;
}

@end
