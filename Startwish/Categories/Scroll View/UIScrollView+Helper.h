//
//  UIScrollView+Helper.h
//  Startwish
//
//  Created by marsohod on 16/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIScrollView (Helper)

- (void)h_redrawHeight;
- (CGRect)h_contentHeight;

@end
