//
//  ClusterPrePermissions+Helper.m
//  Startwish
//
//  Created by Pavel Makukha on 12/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ClusterPrePermissions+Helper.h"
#import "ActionTexts.h"


@implementation ClusterPrePermissions (Helper)

+ (void)h_showPhotoPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
{
    [self h_showPhotoPermissionsWithCallBack:cb rejectFirstStep:fs rejectSecondStep:ss showAlertViewAfterSecondReject:YES];
}

+ (void)h_showPhotoPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert
{
    [[ClusterPrePermissions sharedPermissions] showPhotoPermissionsWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessPhotos"]
                                                                     message:[[ActionTexts sharedInstance] text:@"AccessPhotosAllow"]
                                                             denyButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessRejected"]
                                                            grantButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessApproved"]
                                                           completionHandler:^(BOOL hasPermission,
                                                                               ClusterDialogResult userDialogResult,
                                                                               ClusterDialogResult systemDialogResult) {
                                                               
                                                               if( userDialogResult == ClusterDialogResultDenied ) {
                                                                   fs();
                                                               } else {
                                                                   if (hasPermission) {
                                                                       cb();
                                                                   } else {
                                                                       if( systemDialogResult != ClusterDialogResultDenied ) {
                                                                           ss();
                                                                           if ( showAlert ) {
                                                                               UIAlertView *av = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessPhotos"]
                                                                                                                            message:[[ActionTexts sharedInstance] text:@"AccessPhotosDeny"]
                                                                                                                           delegate:self
                                                                                                                  cancelButtonTitle:@"Отмена"
                                                                                                                  otherButtonTitles:UIApplicationOpenSettingsURLString ? @"В Настройки" : nil, nil];
                                                                               [av show];
                                                                           }
                                                                       }
                                                                   }
                                                               }
                                                           }];
}

+ (void)h_showCameraPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
{
    [self h_showCameraPermissionsWithCallBack:cb rejectFirstStep:fs rejectSecondStep:ss showAlertViewAfterSecondReject:YES];
}

+ (void)h_showCameraPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert
{
    [[ClusterPrePermissions sharedPermissions] showCameraPermissionsWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessCamera"]
                                                                      message:[[ActionTexts sharedInstance] text:@"AccessCameraAllow"]
                                                              denyButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessRejected"]
                                                             grantButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessApproved"]
                                                            completionHandler:^(BOOL hasPermission,
                                                                                ClusterDialogResult userDialogResult,
                                                                                ClusterDialogResult systemDialogResult) {
                                                                
                                                                if( userDialogResult == ClusterDialogResultDenied ) {
                                                                    fs();
                                                                } else {
                                                                    if (hasPermission) {
                                                                        cb();
                                                                    } else {
                                                                        if( systemDialogResult != ClusterDialogResultDenied ) {
                                                                            ss();
                                                                            if ( showAlert ) {
                                                                                UIAlertView *av = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessCamera"]
                                                                                                                             message:[[ActionTexts sharedInstance] text:@"AccessCameraDeny"]
                                                                                                                            delegate:self
                                                                                                                   cancelButtonTitle:@"Отмена"
                                                                                                                   otherButtonTitles:UIApplicationOpenSettingsURLString ? @"В Настройки" : nil, nil];
                                                                                [av show];
                                                                            }
                                                                        }
                                                                        // Handle access not being available
                                                                    }
                                                                }
                                                            }];
}

+ (void)h_showLocationPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss
{
    [self h_showLocationPermissionsWithCallBack:cb rejectFirstStep:fs rejectSecondStep:ss showAlertViewAfterSecondReject:YES];
}

+ (void)h_showLocationPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert
{
    [[ClusterPrePermissions sharedPermissions] showLocationPermissionsForAuthorizationType:ClusterLocationAuthorizationTypeWhenInUse
                                                                                     title:[[ActionTexts sharedInstance] titleText:@"AccessLocation"]
                                                                                   message:[[ActionTexts sharedInstance] text:@"AccessLocationAllow"]
                                                                           denyButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessRejected"]
                                                                          grantButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessApproved"]
                                                                         completionHandler:^(BOOL hasPermission, ClusterDialogResult userDialogResult, ClusterDialogResult systemDialogResult) {
                                                                             if( userDialogResult == ClusterDialogResultDenied ) {
                                                                                 fs();
                                                                             } else {
                                                                                 if (hasPermission) {
                                                                                     cb();
                                                                                 } else {
                                                                                     if( systemDialogResult != ClusterDialogResultDenied ) {
                                                                                         ss();
                                                                                         if ( showAlert ) {
                                                                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessLocation"]
                                                                                                                                          message:[[ActionTexts sharedInstance] text:@"AccessLocationDeny"]
                                                                                                                                         delegate:self
                                                                                                                                cancelButtonTitle:[[ActionTexts sharedInstance] titleText:@"Cancel"]
                                                                                                                                otherButtonTitles:UIApplicationOpenSettingsURLString ? [[ActionTexts sharedInstance] titleText:@"InSettings"] : nil, nil];
                                                                                             [av show];
                                                                                         }
                                                                                     }
                                                                                     // Handle access not being available
                                                                                 }
                                                                             }
                                                                         }];
}

+ (void)h_showContactsPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss
{
    [self h_showContactsPermissionsWithCallBack:cb rejectFirstStep:fs rejectSecondStep:ss showAlertViewAfterSecondReject:YES];
}

+ (void)h_showContactsPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert
{
    [[ClusterPrePermissions sharedPermissions] showContactsPermissionsWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessContacts"]
                                                                                   message:[[ActionTexts sharedInstance] text:@"AccessContactsAllow"]
                                                                           denyButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessRejected"]
                                                                          grantButtonTitle:[[ActionTexts sharedInstance] titleText:@"AccessApproved"]
                                                                         completionHandler:^(BOOL hasPermission, ClusterDialogResult userDialogResult, ClusterDialogResult systemDialogResult) {
                                                                             if( userDialogResult == ClusterDialogResultDenied ) {
                                                                                 fs();
                                                                             } else {
                                                                                 if (hasPermission) {
                                                                                     cb();
                                                                                 } else {
                                                                                     if( systemDialogResult != ClusterDialogResultDenied ) {
                                                                                         ss();
                                                                                         if ( showAlert ) {
                                                                                             UIAlertView *av = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"AccessContacts"]
                                                                                                                                          message:[[ActionTexts sharedInstance] text:@"AccessContactsDeny"]
                                                                                                                                         delegate:self
                                                                                                                                cancelButtonTitle:[[ActionTexts sharedInstance] titleText:@"Cancel"]
                                                                                                                                otherButtonTitles:UIApplicationOpenSettingsURLString ? [[ActionTexts sharedInstance] titleText:@"InSettings"] : nil, nil];
                                                                                             [av show];
                                                                                         }
                                                                                     }
                                                                                     // Handle access not being available
                                                                                 }
                                                                             }
                                                                         }];
}



+ (void)alertView:(UIAlertView *)aView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex != 0 ) {
        if( UIApplicationOpenSettingsURLString ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

@end
