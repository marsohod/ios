//
//  ClusterPrePermissions+Helper.h
//  Startwish
//
//  Created by Pavel Makukha on 12/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClusterPrePermissions.h"
#import <ClusterPrePermissions/ClusterPrePermissions.h>


@interface ClusterPrePermissions (Helper)

+ (void)h_showPhotoPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
+ (void)h_showPhotoPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert;

+ (void)h_showCameraPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
+ (void)h_showCameraPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert;

+ (void)h_showLocationPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
+ (void)h_showLocationPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert;


+ (void)h_showContactsPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss;
+ (void)h_showContactsPermissionsWithCallBack:(void (^)())cb rejectFirstStep:(void (^)())fs rejectSecondStep:(void (^)())ss showAlertViewAfterSecondReject:(BOOL)showAlert;

@end
