//
//  UISegmentedControl+CustomTintColor.h
//  Startwish
//
//  Created by marsohod on 22/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UISegmentedControl (CustomTintColor)

- (void)setTag:(NSInteger)tag forSegmentAtIndex:(NSUInteger)segment;
- (void)setTintColor:(UIColor*)color forTag:(NSInteger)aTag;

@end
