//
//  UISegmentedControl+CustomTintColor.m
//  Startwish
//
//  Created by marsohod on 22/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UISegmentedControl+CustomTintColor.h"


@implementation UISegmentedControl(CustomTintColor)

- (void)setTag:(NSInteger)tag forSegmentAtIndex:(NSUInteger)segment {
    [[[self subviews] objectAtIndex:segment] setTag:tag];
}

- (void)setTintColor:(UIColor*)color forTag:(NSInteger)aTag {
    // must operate by tags.  Subview index is unreliable
    UIView *segment = [self viewWithTag:aTag];
    SEL tint = @selector(setTintColor:);
    
    // UISegment is an undocumented class, so tread carefully
    // if the segment exists and if it responds to the setTintColor message
    if (segment && ([segment respondsToSelector:tint])) {
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [segment performSelector:tint withObject:color];
        #pragma clang diagnostic pop
    }
}
@end

