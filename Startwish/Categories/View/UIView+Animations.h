//
//  UIView+Animations.h
//  Startwish
//
//  Created by Pavel Makukha on 30/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIView (Animations)

- (void)a_heartStop:(BOOL *)stop;

@end
