//
//  UIView+Animations.m
//  Startwish
//
//  Created by Pavel Makukha on 30/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "UIView+Animations.h"


@implementation UIView (Animations)

- (void)a_heartStop:(BOOL *)stop
{
    [UIView animateWithDuration:0.3
                     animations:^(){
                         self.transform = CGAffineTransformMakeScale(1.15, 1.15);
                     }
                     completion:^(BOOL finished) {
                         if ( *stop ) { return; }
                         [UIView animateWithDuration:0.3
                                          animations:^(){
                                              self.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                          }
                                          completion:^(BOOL finished) {
                                              if ( *stop ) { return; }
                                              
                                              [UIView animateWithDuration:0.2
                                                               animations:^(){
                                                                   self.transform = CGAffineTransformMakeScale(1.07, 1.07);
                                                               }
                                                               completion:^(BOOL finished) {
                                                                   if ( *stop ) { return; }
                                                                   
                                                                   [UIView animateWithDuration:0.2
                                                                                    animations:^(){
                                                                                        self.transform = CGAffineTransformMakeScale(1.0, 1.0);
                                                                                    }
                                                                                    completion:^(BOOL finished) {
                                                                                        if ( *stop ) { return; }
                                                                                        
                                                                                        SEL theSelector = NSSelectorFromString(@"a_heartStop:");
                                                                                        NSInvocation *anInvocation = [NSInvocation
                                                                                                                      invocationWithMethodSignature:[UIImageView instanceMethodSignatureForSelector:theSelector]];
                                                                                        
                                                                                        [anInvocation setSelector:theSelector];
                                                                                        [anInvocation setTarget:self];

                                                                                        [anInvocation setArgument:(void *)&stop atIndex:2];
                                                                                        
                                                                                        [anInvocation performSelector:@selector(invoke) withObject:nil afterDelay:0.5];
                                                                                    }];
                                                               }];
                                          }];
                     } ];

}

@end
