//
//  UIScreen+Helper.h
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIScreen (Helper)

+ (float)h_getPinWidthDependsDevice;
+ (float)h_getPinMinHeight;
+ (float)h_paddingPin;
+ (BOOL)isIphone4;
+ (BOOL)isIphone6;
+ (BOOL)isIphone6Plus;
@end
