//
//  UIScreen+Helper.m
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIScreen+Helper.h"

#define PIN_HEIGHT 200.0

#define isiPhone4  ([[UIScreen mainScreen] bounds].size.height == 480)?TRUE:FALSE
#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#define isiPhone6  ([[UIScreen mainScreen] bounds].size.height == 667)?TRUE:FALSE
#define isiPhone6plus  ([[UIScreen mainScreen] bounds].size.height == 736)?TRUE:FALSE
#define isiPhone  (UI_USER_INTERFACE_IDIOM() == 0)?TRUE:FALSE

@implementation UIScreen (Helper)

+ (float)h_getPinWidthDependsDevice
{
    return ([[UIScreen mainScreen] bounds].size.width / 2) - ( [UIScreen h_paddingPin] * 3 / 2 );
}

+ (float)h_getPinMinHeight
{
    return PIN_HEIGHT;
}

+ (float)h_paddingPin
{
    return 10.0;
}

+ (BOOL)isIphone4
{
    return isiPhone4;
}

+ (BOOL)isIphone6
{
    return isiPhone6;
}

+ (BOOL)isIphone6Plus
{
    return isiPhone6plus;
}

@end
