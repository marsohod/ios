//
//  UIFont+Helper.h
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIFont (Helper)

+ (NSString *)h_defaultFontName;
+ (NSString *)h_defaultFontNameBold;
+ (NSString *)h_defaultFontNameLight;
+ (NSString *)h_defaultFontNameMedium;

+ (UIFont *)h_defaultFontSize:(float)size;
+ (UIFont *)h_defaultFontBoldSize:(float)size;
+ (UIFont *)h_defaultFontLightSize:(float)size;
+ (UIFont *)h_defaultFontMediumSize:(float)size;

@end
