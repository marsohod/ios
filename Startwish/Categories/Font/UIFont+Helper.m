//
//  UIFont+Helper.m
//  Startwish
//
//  Created by marsohod on 09/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UIFont+Helper.h"


@implementation UIFont (Helper)

#define NORMAL @"Helvetica Neue"
#define BOLD @"HelveticaNeue-Bold"
#define LIGHT @"HelveticaNeue-LIGHT"
#define MEDIUM @"HelveticaNeue-Medium"

+ (NSString *)h_defaultFontName
{
    return NORMAL;
}

+ (NSString *)h_defaultFontNameBold
{
    return BOLD;
}

+ (NSString *)h_defaultFontNameLight
{
    return LIGHT;
}

+ (NSString *)h_defaultFontNameMedium
{
    return MEDIUM;
}

+ (UIFont *)h_defaultFontSize:(float)size
{
    return [UIFont fontWithName: NORMAL size: size];
}

+ (UIFont *)h_defaultFontBoldSize:(float)size
{
    return [UIFont fontWithName: BOLD size: size];
}

+ (UIFont *)h_defaultFontLightSize:(float)size
{
    return [UIFont fontWithName: LIGHT size: size];
}

+ (UIFont *)h_defaultFontMediumSize:(float)size
{
    return [UIFont fontWithName: MEDIUM size: size];
}

@end
