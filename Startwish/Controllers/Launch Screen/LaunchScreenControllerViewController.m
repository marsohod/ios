//
//  LaunchScreenControllerViewController.m
//  Startwish
//
//  Created by marsohod on 20/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LaunchScreenControllerViewController.h"
#import "UIImage+Helper.h"
#import "WishesAPI.h"
#import "UserAPI.h"
#import "LoginAPI.h"
#import "Me.h"
#import "Routing.h"
#import "UIScreen+Helper.h"
#import "UIView+Animations.h"
#import "NSString+Helper.h"


@interface LaunchScreenControllerViewController ()
{
    BOOL isTimerFired;
    NSArray *items;
    BOOL isGoingToNextController;
}
@end

@implementation LaunchScreenControllerViewController

// время, которое точно будет крутиться кругляшь
const CGFloat animationApprovedTime =  6.f;
const CGFloat animationCircleTurnover = 8.f;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self renderViewsForCurrentDevice];
    
    if ( ![Me needMoreSubscriptions] ) {
        [self loadItems];
    }
    
    [[UserAPI sharedInstance] updateMyProfileInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:[LoginAPI sharedInstance] selector:@selector(cleanProfile) name:@"STNeedAuthUser" object:nil];
    
    // Устанавливаем таймер на выполение проверки загрузки желаний через время animationApprovedTime
    [NSTimer scheduledTimerWithTimeInterval:animationApprovedTime
                                     target:self
                                   selector:@selector(prepareGoToForward)
                                   userInfo:nil
                                    repeats:NO];
    
    
    // Заполняем массив с картинками
//    NSArray *launchImages = [[NSArray alloc] initWithObjects:@"screen3", nil];
    
    // Выбираем одну из картинок
    self.imageViewBackground.image = [UIImage imageNamed:@"screen3"];
    
    // Добавляем анимацию
    [self setAnimationToRainbow];
    [self setAnimationToBeast];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Launch Screen";
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:[LoginAPI sharedInstance]];
    NSLog(@"REMOVE LAUNCHVIEWCONTROLLER");
}

- (void)renderViewsForCurrentDevice
{
    if ( [UIScreen isIphone4] ) {
        _paddingTitleToTop.constant = 20.0;
        _paddingLabelToTitle.constant = 15.0;
    }
    
    [self.view setNeedsLayout];
}

- (void)setAnimationToRainbow
{
    // Добавляем анимацию для радужного круга.
    // Используем CATransaction для установки callback блока
    [CATransaction begin];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * animationCircleTurnover * 2.0];
    rotationAnimation.duration = animationCircleTurnover;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1;
    
    //     Callback после окончания анимации
    [CATransaction setCompletionBlock:^{
        [self goForwardAfterAnimationStop:YES];
    }];
    
    [self.imageViewRainbow.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    [CATransaction commit];
}

- (void)setAnimationToBeast
{
    [_imageViewBeast a_heartStop:&isGoingToNextController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    if (self.isViewLoaded && !self.view.window) {
        self.imageViewBackground.image = nil;
    }
}


- (void)prepareGoToForward {
    isTimerFired = YES;
    
    if ( items != nil && !isGoingToNextController ) {
        [self goForward];
    }
}

- (void)goForward
{
    [self goForwardAfterAnimationStop:NO];
}

- (void)goForwardAfterAnimationStop:(BOOL)isStoped
{
    if ( isGoingToNextController ) { return; }
    
    if ( isStoped ) {
        // Только если желания не загружены - переходим в контроллер
        // Если желания уже были загружены, то переход осуществился ранее.
        if ( (items == nil || [items count] == 0) && !isGoingToNextController ) {
            [self performSegueWithIdentifier:@"startDetail" sender:items];
            return;
        }
    } else {
        [self performSegueWithIdentifier:@"startDetail" sender:items];
    }
    
    
}

- (void)loadItems
{
   [[UserAPI sharedInstance] feedsBeforeUpdate:[NSString h_dateNow] withCallBack:^(NSArray *newWishes) {
       [[NSNotificationCenter defaultCenter] postNotificationName:@"STLoadFirstFeed" object:nil userInfo:@{@"items": newWishes}];
       items = [[NSArray alloc] initWithArray:newWishes];
       
       if ( isTimerFired == YES && !isGoingToNextController ) {
           [self goForward];
       }
       
   } withFailedCallBack:^(id item) {
       [[NSNotificationCenter defaultCenter] postNotificationName:@"STLoadFirstFeed" object:nil userInfo:@{@"items": @[]}];
       
       [self performSegueWithIdentifier:@"startDetail" sender:@[]];
   }];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    isGoingToNextController = YES;
    [Routing prepareForSegue:segue sender:sender];
}

@end
