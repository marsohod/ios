//
//  LaunchScreenControllerViewController.h
//  Startwish
//
//  Created by marsohod on 20/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"


@interface LaunchScreenControllerViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewRainbow;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewBeast;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingTitleToTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingLabelToTitle;

@end
