//
//  WishlistsViewController.m
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishlistsViewController.h"
#import "WishlistsAPI.h"
#import "WishesAPI.h"
#import "Routing.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "ActionTexts.h"
#import "WishlistSelectViewController.h"
#import "Me.h"


//#import "UIScrollView+BottomRefreshControl.h"


@interface WishlistsViewController ()
{
    BOOL isLoading;
    BOOL isNotAvailable;
}
@end

@implementation WishlistsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor: [UIColor h_smoothGreen]];
    
    [_labelView setBackgroundColor: [UIColor h_smoothGreen]];
    [_labelView setFont: [UIFont h_defaultFontSize: 16.0]];
    [_labelView setTextColor: [UIColor h_bg_contacts]];
    [_labelView setTextAlignment: NSTextAlignmentCenter];
    [_labelView setText: [_labelView.text uppercaseString]];
    
    _tableView.tapDelegate = (id<WishlistsDelegate>)self;
    _tableView.backgroundColor = [UIColor whiteColor];
    
    [_tableView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    self.preload = [[PreloadView alloc] init];
    
    _viewEmpty = [[EmptyView alloc] initToView:self.view
                                      withText:[[ActionTexts sharedInstance] titleText:@"WishlistEmpty"]
                                          desc:([Me isMyProfile:[user getId]] ? [[ActionTexts sharedInstance] titleText:@"EmptyViewDesc"] : nil)
                                   buttonTitle:([Me isMyProfile:[user getId]] ? [[ActionTexts sharedInstance] titleText:@"AddWish"] : nil)];
    _viewEmpty.edelegate = (id<EmptyViewDelegate>)self;
    _viewEmpty.needMenuButton = NO;
    
    [self.view addSubview:self.preload];
    
    [_preload showPreload];
    
    _buttonAdd.hidden = ![Me isMyProfile:[user getId]];
    [self showEmptyWishesView:NO];
    [self loadItems];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reduceWishesCountInWishlist:) name:@"STWishDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reduceWishesCountInWishlist:) name:@"STWishExecutedDelete" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
    [WishlistSelectViewController sharedInstance].delegate = (id<PUViewProtocol>)self;
    self.screenName = @"Wishlists Screen";
}

- (void)wishlistsByUser:(ModelUser *)u
{
    user = u;
}

- (IBAction)actionButtonAdd:(id)sender
{
    [[Routing sharedInstance] showCreateWishlistControllerWithWishlist:nil];
}

- (void)showEmptyWishesView:(BOOL)status
{
    //    _viewEmptyUserWishes.hidden = !status;

    _tableView.hidden = status;
    
    [_viewEmpty show:status withButton:[Me isMyProfile:[user getId]]];
}


#pragma mark -
#pragma mark WishlistDelegate

- (void)editWishlist:(ModelWishlist *)wishlist
{
    [[Routing sharedInstance] showCreateWishlistControllerWithWishlist:wishlist];
}

- (void)tryRemoveWishlist:(uint32_t)wishlist_id
{
    [WishlistSelectViewController sharedInstance].hideWishlistId = wishlist_id;
    
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle: [[ActionTexts sharedInstance] titleText:@"WishlistDelete"]
                                                    delegate: (id<UIActionSheetDelegate>)self
                                           cancelButtonTitle: @"Отменить"
                                      destructiveButtonTitle: nil
                                           otherButtonTitles: [[ActionTexts sharedInstance] titleText:@"WishlistDeleteWishDelete"], [[ActionTexts sharedInstance] titleText:@"WishlistDeleteWishImplement"], [[ActionTexts sharedInstance] titleText:@"WishlistDeleteWishMove"], nil];
    [as showInView:self.view];
}

// reason:
// 1 - remove wishes from wishlist
// 2 - execute wishes from wishlist
// 3 - move wishes to another wishlist

- (void)removeEmptyWishlist:(uint32_t)wishlist_id
{
    [self removeWishlists:wishlist_id reason:1 toWishlistId:0];
}

- (void)removeWishlists:(uint32_t)wishlist_id reason:(NSInteger)reason toWishlistId:(uint32_t)toWid
{
    if ( [_tableView itemsCount] == 1 ) {
        [_norification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"TryRemoveLastWishlist"]];
        return;
    }
    
    [[WishlistsAPI sharedInstance] removeWishlistById:wishlist_id reason:reason toWishlistId:toWid withCallBack:^(id item) {
        NSString *wishesType = reason == 1 ? @"wishesDelete" : ( reason == 2 ? @"wishesExecute" : @"wishesMove" );
        ModelWishlist *removedWishlist      = [_tableView itemById:wishlist_id];
        NSArray *wishesOfremovedWishlist    = [removedWishlist getWishes];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishlistDelete"
                                                            object:nil
                                                          userInfo:@{@"increase"    : [NSNumber numberWithBool:NO],
                                                                     @"wishlistId"  : [NSNumber numberWithLongLong:wishlist_id],
                                                                     wishesType     : [NSNumber numberWithInteger:[removedWishlist getWishesCount]]
                                                                     }];
        

        [_tableView removeItemById:wishlist_id];
        [_tableView reloadData];
        
        if ( toWid != 0 && reason == 3 ) {
            ModelWishlist *toW = [_tableView itemById:toWid];
            
            [wishesOfremovedWishlist enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                [toW reduceWishesCount:NO];
                [toW addWish:obj];
            }];
            
            [_tableView updateItem:toW byId:toWid];
        }
    } failedBlock:^(id item) {
        [_norification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishlistDelete"]];
    }];
}


#pragma mark -
#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ( buttonIndex ) {
        case 0:
            [self removeWishlists:[WishlistSelectViewController sharedInstance].hideWishlistId reason:1 toWishlistId:0];
            break;
        case 1:
            [self removeWishlists:[WishlistSelectViewController sharedInstance].hideWishlistId reason:2 toWishlistId:0];
            break;
        case 2:
            [[WishlistSelectViewController sharedInstance] showInView:self.view animated:YES];
            break;
            
        default:
            break;
    }
}


#pragma mark -
- (void)loadItems
{
    if( isLoading || isNotAvailable ) {
        [_tableView.bottomRefreshControl endRefreshing];
        return;
    }
    
    isLoading = YES;
    
    [[WishlistsAPI sharedInstance] getItemsFromPage: [_tableView currentPage] byUserId: [user getId] withCallBack:^(NSArray *items) {
        NSMutableArray *newItems = [[NSMutableArray alloc] initWithArray:items];
        
        isLoading = NO;
        if ( [items count] == 0 ) {
            isNotAvailable = YES;
            
            if ( [_tableView currentPage] == 0 ) {
                [self showEmptyWishesView:YES];
            }
            
            [_preload hidePreload];
            return;
        }
        
        [newItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [self loadWishesToWishlist:obj wishlistIndex:idx];
        }];
        
        [_tableView updateWithItems:newItems];
        [self.preload hidePreload];
    } withFailedCallBack:^(id item) {
        [_preload hidePreload];
    }];
}

- (void)loadWishesToWishlist:(ModelWishlist *)obj wishlistIndex:(NSUInteger)idx
{
    [[WishlistsAPI sharedInstance] getShortWishesWishlistById:[obj getId] withCallBack:^(NSArray *shortItems) {
        [obj setWishes:[[NSMutableArray alloc] initWithArray:shortItems]];
        
        [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

- (void)wishlistDetail:(uint32_t)wishlistId
{
//    id sender = @{@"id" : [NSString stringWithFormat:@"%u", wishlistId] };
    selectedWishlist = wishlistId;
    [self performSegueWithIdentifier:@"wishlistDetail" sender:nil];
}


#pragma mark -
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [Routing goToWishlistDetailCtrl:segue wishlistId:selectedWishlist byUser:user];
}

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionButton:(id)sender {
    [[Routing sharedInstance] showCreateWishControllerWithImage:nil];
}

#pragma mark -
#pragma mark PUViewProtocol
- (void)successPopUp:(uint32_t)wishlistId
{
    [self removeWishlists:[WishlistSelectViewController sharedInstance].hideWishlistId reason:3 toWishlistId:wishlistId];
}

#pragma mark -
- (void)reduceWishesCountInWishlist:(NSNotification *)notif
{
    ModelWishlist *w = [_tableView itemById:(uint32_t)[notif.userInfo[@"wishlistId"] longLongValue]];
    
    if ( w != nil ) {
        [w reduceWishesCount:YES];
        [w removeWishById:(uint32_t)[notif.userInfo[@"wishId"] longLongValue]];
        [_tableView reloadData];
    }
}

@end
