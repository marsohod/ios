//
//  WishlistsViewController.h
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "WishlistsTableView.h"
#import "ModelUser.h"
#import "PreloadView.h"
#import "NotificationLabel.h"
#import "EmptyView.h"
#import "AddButton.h"
#import "GAITrackedViewController.h"


@interface WishlistsViewController : GAITrackedViewController
{
    ModelUser *user;
    uint32_t selectedWishlist;
}

@property (strong, nonatomic) PreloadView *preload;
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (weak, nonatomic) IBOutlet WishlistsTableView *tableView;
@property (weak, nonatomic) IBOutlet NotificationLabel *norification;
@property (strong, nonatomic) EmptyView *viewEmpty;
@property (weak, nonatomic) IBOutlet AddButton *buttonAdd;

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionButtonAdd:(id)sender;
- (void)wishlistsByUser:(ModelUser *)u;

@end
