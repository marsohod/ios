//
//  PinterestViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 27/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PinterestViewController.h"


@interface PinterestViewController ()

@end

@implementation PinterestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    buttonBeast = [[ButtonBeast alloc] init];
    viewProgress = [[ViewProgress alloc] init];
    
    [self.view addSubview:viewProgress];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideOnScroll) name:@"STHideOnScroll" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showOnScroll) name:@"STShowOnScroll" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEventCount:) name:@"STNewEventsCountUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showProgress:) name:@"STUploadProgress" object:nil];
    // Do any additional setup after loading the view.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGRect)prepareWishToClose
{
    return CGRectZero;
}

- (CGRect)prepareWishToOpen
{
    return CGRectZero;
}

- (UIView *)cellWishOpen
{
    return [[UIView alloc] init];
}

- (void)cellWishClose
{
    
}

- (void)hideOnScroll
{
    if ( buttonBeast.layer.opacity != 0.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            buttonBeast.layer.opacity = 0.0;
            [self hideTopLeftButton:YES];
        }];
    }
    
}

- (void)showOnScroll
{
    if ( buttonBeast.layer.opacity != 1.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            buttonBeast.layer.opacity = 1.0;
            [self hideTopLeftButton:NO];
        }];
    }
}

- (void)showProgress:(NSNotification *)notif
{
    [viewProgress setValue:(float)[notif.userInfo[@"upload"] longLongValue] / (float)[notif.userInfo[@"total"] longLongValue]];
}

- (void)hideTopLeftButton:(BOOL)state
{
    
}

- (void)updateEventCount:(NSNotification *)notif
{

}


@end
