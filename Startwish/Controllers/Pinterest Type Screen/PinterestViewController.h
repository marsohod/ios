//
//  PinterestViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 27/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ButtonBeast.h"
#import "ViewProgress.h"
#import "GAITrackedViewController.h"


@interface PinterestViewController : GAITrackedViewController
{
    ButtonBeast *buttonBeast;
    ViewProgress*viewProgress;
}

- (CGRect)prepareWishToOpen;
- (CGRect)prepareWishToClose;
- (UIView *)cellWishOpen;
- (void)cellWishClose;
- (void)hideTopLeftButton:(BOOL)state;
- (void)updateEventCount:(NSNotification *)notif;
- (void)showOnScroll;

@end
