//
//  SoketViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 20/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SoketViewController.h"
#import "SocketEvent.h"


@interface SoketViewController ()

@end

@implementation SoketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [SocketEvent sharedInstance].delegate = (id<SRWebSocketDelegate>)self;
    [[SocketEvent sharedInstance] open];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark SRWebSocketDelegate

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    _textView.text = [NSString stringWithFormat:@"%@\r%@", _textView.text, message];
}

- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    _textView.text = @"soket open";
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    _textView.text = [NSString stringWithFormat:@"soket failed with error: %@", [error localizedDescription]];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    _textView.text = [NSString stringWithFormat:@"soket close with code: %ld, reason: %@", (long)code, reason];
}

- (IBAction)actionButtonStart:(id)sender {
    [[SocketEvent sharedInstance] send:@"test message"];
}


@end
