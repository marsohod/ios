//
//  SoketViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 20/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SoketViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *buttonStart;

- (IBAction)actionButtonStart:(id)sender;

@end
