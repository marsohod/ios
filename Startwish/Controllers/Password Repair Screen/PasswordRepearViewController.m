//
//  PasswordRepearViewController.m
//  Startwish
//
//  Created by marsohod on 21/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PasswordRepearViewController.h"
#import "LoginAPI.h"
#import "MBProgressHUD.h"
#import "ActionTexts.h"


@interface PasswordRepearViewController ()
{

}
@end

#define ICON_WIDTH_WITH_PADDING 28.0

@implementation PasswordRepearViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    [self.view addGestureRecognizer:tapView];
    
    [_textFieldOldPass setIcon:@"lock" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-8.0 paddingLeft:0.0];
    [_textFieldNewPass setIcon:@"lock" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-8.0 paddingLeft:0.0];
    
    [_textFieldNewPass addTarget:self action:@selector(actionChangePasswordButton:) forControlEvents:UIControlEventEditingDidEnd];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Password Repear Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.notification.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.notification.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

- (BOOL)isFormValid
{
    if( [_textFieldOldPass.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordOldNotEnter"]];
        [_textFieldOldPass becomeFirstResponder];
        
        return NO;
        
    } else if( [_textFieldNewPass.text isEqualToString:@""] ) {
        
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordNewNotEnter"]];
        [_textFieldNewPass becomeFirstResponder];
        
        return NO;
    } else if ( [_textFieldOldPass.text isEqualToString:_textFieldNewPass.text] ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordsNotEqual"]];
        [_textFieldNewPass becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}


#pragma mark -
#pragma mark Actions Button

- (IBAction)actionBackButton:(id)sender {
}

- (IBAction)actionChangePasswordButton:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ( [self isFormValid] ) {
//        [[LoginAPI sharedInstance] changePasswordToNew:_textFieldNewPass.text withCallBack:^(NSDictionary *response) {
//            [self closeAlert:nil];
//            [_notification showNotify:1 withText:[[ActionTexts sharedInstance] successText:@"PasswordUpdated"]];
//        } withError:^(NSString *error) {
//            [self closeAlert:nil];
//            [_notification showNotify:0 withText:error];
//        }];
        
        [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(closeAlert:) userInfo:nil repeats:NO];
    }
}

- (void)closeAlert:(id)sender
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end
