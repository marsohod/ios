//
//  PasswordRepearViewController.h
//  Startwish
//
//  Created by marsohod on 21/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface PasswordRepearViewController : GAITrackedViewController

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionChangePasswordButton:(id)sender;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldOldPass;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldNewPass;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

@end
