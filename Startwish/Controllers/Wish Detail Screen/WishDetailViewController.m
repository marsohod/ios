//
//  WishDetailViewController.m
//  Startwish
//
//  Created by marsohod on 13/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishDetailViewController.h"
#import "WishesAPI.h"
#import "UIScreen+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "NSString+Helper.h"
#import "BackButton.h"
#import "Routing.h"
#import "Settings.h"

#import "UILabel+typeOfText.h"
#import "ModelComment.h"
#import "ActionTexts.h"
#import "MailViewController.h"
#import "ClusterPrePermissions+Helper.h"
#import "SocialShareWishViewController.h"
#import "EventsViewController.h"


@interface WishDetailViewController ()
{
    ModelWish *wish;
    NSString *wishId;
    BOOL isLoading;
    BOOL isDeleting;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) CGFloat offsetTop;

// Желание
@property (weak, nonatomic) IBOutlet UIImageViewWish *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *rewishLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightImageView;

// Кноки
@property (weak, nonatomic) IBOutlet WantAlreadyButton *buttonAlreadyWant;
@property (weak, nonatomic) IBOutlet WantButton *buttonWant;
@property (weak, nonatomic) IBOutlet ImplementButton *buttonImplement;
@property (weak, nonatomic) IBOutlet DeleteButton *buttonDelete;
@property (weak, nonatomic) IBOutlet BackFillButton *buttonBack;
@property (weak, nonatomic) IBOutlet DotsFillButton *buttonDots;

- (IBAction)actionButtonWant:(id)sender;
- (IBAction)actionButtonImplement:(id)sender;
- (IBAction)actionButtonDelete:(id)sender;
- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionSendButton:(id)sender;
- (IBAction)actionAdditionalButton:(id)sender;


// Создатель желания
@property (weak, nonatomic) IBOutlet UIView *masterView;
@property (weak, nonatomic) IBOutlet UIViewAvatar *viewMasterAvatar;
@property (weak, nonatomic) IBOutlet UILabel *masterUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *masterAddWishDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *masterDescription;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingMasterUsernameRightFromView;

// Комменты
@property (weak, nonatomic) IBOutlet CommentsTableView *commentsTableView;
@property (weak, nonatomic) IBOutlet LabelLikeTextField *labelAddComment;
@property (weak, nonatomic) IBOutlet CommentButton *buttonAddComment;

// Уведомление
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

@end


@implementation WishDetailViewController

#define cImplement      [UIColor h_smoothYellow]
#define cNotImplement   [UIColor whiteColor]

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ( wish == nil ) {
        return;
    }
    
    [self renderView];
    [self renderMasterWishView];
    
    [self setWishCover];
    [self setWishName];
    [self setMasterWishView];
    [self setRewishCount];
    [self renderAddComment];
    [self renderButtons];
    [self renderCommentsTableViewWithHiddenState:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderUpdateWishData) name:@"STWishUpdated" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_commentsTableView deselectRowAtIndexPath:[_commentsTableView indexPathForSelectedRow] animated:YES];
    [WishlistSelectViewController sharedInstance].delegate = (id<PUViewProtocol>)self;
    
    [self renderCommentIcon];
    [self renderCommentsTableViewWithHiddenState:NO];
    
    self.screenName = @"Wish Detail Screen";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ( _commentsTableView.layer.opacity == 0.0 ) {
        [UIView animateWithDuration:.3 animations:^{
            _commentsTableView.layer.opacity = 1.0;
            _buttonDelete.layer.opacity = 1.0;
            _buttonWant.layer.opacity = 1.0;
            _buttonImplement.layer.opacity = 1.0;
            _buttonAddComment.layer.opacity = 1.0;
            _masterView.layer.opacity = 1.0;
            _labelAddComment.layer.opacity = 1.0;
        }];
    }
    
    [self reloadScrollView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{
    // Ставим высоту таблицы по высоте комментов
    [self.commentsTableView removeConstraints: [self.commentsTableView constraints]];
    [self.commentsTableView addConstraint: [NSLayoutConstraint constraintWithItem: self.commentsTableView
                                                                         attribute: NSLayoutAttributeHeight
                                                                         relatedBy: NSLayoutRelationEqual
                                                                            toItem: nil
                                                                         attribute: NSLayoutAttributeHeight
                                                                        multiplier: 1.0
                                                                          constant: self.commentsTableView.contentSize.height]];
}

- (void)setDetail:(ModelWish *)item
{
    wish = item;
    if( [wish getCommentsCount] > 0 && [[wish getComments] count] != [wish getCommentsCount] ) {
        [self getComments];
    }
}

- (void)setWishId:(uint32_t)wId
{
    [[WishesAPI sharedInstance] getWishById:wId withCallBack:^(id item) {
        [self setDetail:item];
        [self viewDidLoad];
    }];
}

- (void)getComments
{
    isLoading = YES;
    [[WishesAPI sharedInstance] getCommentsWishById:[wish getId] offset: [[wish getComments] count] update:^(NSArray *items) {
        
        [wish pushComments:items];
        
        if ( self.commentsTableView ) {
            self.commentsTableView.items = [[NSMutableArray alloc] initWithArray:[wish getComments]];
            self.commentsTableView.maxItemsCount = [wish getCommentsCount];
            [self.commentsTableView reloadData];
            [self viewDidLayoutSubviews];
        }
        
        [self reloadScrollView];
        [self renderCommentIcon];
        isLoading = NO;
    } failedBlock:^(id item) {
        isLoading = NO;
    }];
}

#pragma mark -
#pragma mark Render Element

- (void)renderView
{
    [self.view setBackgroundColor: [wish isImplement] ? cImplement : cNotImplement];
}

- (void)reloadScrollView
{
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.labelAddComment.frame.origin.y + self.labelAddComment.frame.size.height + 10.0)];
}


- (void)renderCommentIcon
{
    if( [wish getCommentsCount] > 0 ) {
        self.commentLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[wish getCommentsCount]];
        self.commentImageView.hidden = NO;
        self.commentLabel.hidden = NO;
    } else {
        self.commentImageView.hidden = YES;
        self.commentLabel.hidden = YES;
    }
}

- (void)renderMasterWishView
{
//    _masterView.layer.opacity = 0.0;
    UIColor *cMasterView = [wish isImplement] ? [UIColor h_smoothDeepYellow] : [UIColor h_smoothGreen];
    UIColor *cMasterName = [wish isImplement] ? [UIColor h_smoothGreen] : [UIColor h_smoothYellow];
    UIColor *cMasterDate = [wish isImplement] ? [UIColor blackColor] : [UIColor whiteColor];
    
    [self.masterAddWishDateLabel setFont: [UIFont h_defaultFontSize: 12.0]];
    [self.masterUsernameLabel setFont: [UIFont h_defaultFontBoldSize: 13.0]];
    [self.masterDescription setFont: [UIFont h_defaultFontSize: 12.0]];
    self.masterDescription.numberOfLines = 0;
    
    // Устанавливаем цвета в зависимости от того исполнено желание или нет
    [self.masterUsernameLabel setTextColor: cMasterName];
    [self.masterUsernameLabel setBackgroundColor: cMasterView];
    [self.masterAddWishDateLabel setTextColor: cMasterDate];
    [self.masterAddWishDateLabel setBackgroundColor: cMasterView];
    [self.masterDescription setTextColor: cMasterDate];
    [self.masterDescription setBackgroundColor: cMasterView];
    self.viewMasterAvatar.isHighlighted = ![wish isImplement];
    [self.masterView setBackgroundColor: cMasterView];
    
    // Перерисовываем аватар по умолчанию
    [self.viewMasterAvatar customDrawView];
}

- (void)renderAddComment
{
//    _buttonAddComment.layer.opacity = 0.0;
//    _labelAddComment.layer.opacity = 0.0;
    
    UITapGestureRecognizer *tapLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSendButton:)];
    [self.labelAddComment addGestureRecognizer:tapLabel];
    
    self.labelAddComment.colorDefault = @"black";
    self.labelAddComment.colorHighlight = @"black";
    [self.labelAddComment setIcon:@"comment" toTheRightDirect:NO withMaxIconWidth: 20.0 paddingTop: -2.0 paddingLeft: 0.0];
    [self.labelAddComment setFont: [UIFont h_defaultFontSize: 13.0]];
    [self.labelAddComment customSetText: @"ваш комментарий"];
    
    
}

- (void)renderButtons
{
//    _buttonDelete.layer.opacity = 0.0;
//    _buttonWant.layer.opacity = 0.0;
//    _buttonImplement.layer.opacity = 0.0;
    _buttonDelete.hidden = YES;
    _buttonImplement.hidden = YES;
    
    if ( [wish isMyWish] ) {
        _buttonImplement.hidden = [wish isImplement];
        _buttonWant.hidden = ![wish isImplement];
        _buttonDelete.hidden = [wish isDeleted];
    } else {
        _buttonWant.hidden = [wish isWishInWishlist];
        _buttonAlreadyWant.hidden = ![wish isWishInWishlist];
    }
}

- (void)renderCommentsTableViewWithHiddenState:(BOOL)isHidden
{
    self.commentsTableView.layer.opacity = isHidden ? 0.f : 1.f;
    self.commentsTableView.tableDelegate = (id<CommentsTableViewDelegate>)self;
    self.commentsTableView.items = [[NSMutableArray alloc] initWithArray:[wish getComments]];
    self.commentsTableView.maxItemsCount = [wish getCommentsCount];
    self.commentsTableView.areCommentsExecutedWish = [wish isImplement];
    [self.commentsTableView reloadData];
}


- (void)renderUpdateWishData
{
    [self renderCommentIcon];
    [self setRewishCount];
    [self setWishName];
    [self renderButtons];
    [self renderMasterWishView];
    [self setMasterWishView];
    [self renderView];
    
    if ( [wish getCoverExecutedOriginalTemp] ) {
        _imageView.image = [wish getCoverExecutedOriginalTemp];
        _heightImageView.constant = [[UIScreen mainScreen] bounds].size.width / (_imageView.image.size.width/_imageView.image.size.height);
        [self reloadScrollView];
    }
    
    _commentsTableView.areCommentsExecutedWish = [wish isImplement];
    _commentsTableView.items = [[NSMutableArray alloc] initWithArray:[wish getComments]];
    _commentsTableView.maxItemsCount = [wish getCommentsCount];
    [_commentsTableView reloadData];
}


#pragma mark -
#pragma mark Set Element

- (void)setWishCover
{
    CGRect screen = [[UIScreen mainScreen] bounds];
    float scale =  [wish hasExecutedImageWithSizes] ? [wish getCoverExecutedWidth] / [wish getCoverExecutedHeight] : [wish getCoverWidth] / [wish getCoverHeight];
    
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[NSString stringWithFormat:@"%@", [wish hasExecutedImageWithSizes] ? [wish getCoverExecuted] : [wish getCover]] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            _imageView.image = [image scaleToSize:CGSizeMake([UIScreen h_getPinWidthDependsDevice], [UIScreen h_getPinWidthDependsDevice] / scale)];
        } else {
            _imageView.image = [[UIImage imageNamed:@"transparent.png"] scaleToSize:CGSizeMake([UIScreen h_getPinWidthDependsDevice], [UIScreen h_getPinWidthDependsDevice] / scale)];
        }
    }];
    
    
    // Ставим высоту картинки ещё до того, как она загрузилась
    _heightImageView.constant = screen.size.width / scale;
    
    [self reloadScrollView];
    
    // Если есть изображение в кэше, то берём оттуда
    // Иначе скачиваем, ужимаем, засовываем в кэш и отрисовываем
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:[NSString stringWithFormat:@"%@", [wish hasExecutedImageWithSizes] ? [wish getCoverExecutedOriginal] : [wish getCoverOriginal]] done:^(UIImage *image, SDImageCacheType cacheType) {
        if( image != nil ) {
            _imageView.image = image;
        } else {
            
            [UIImage downloadWithUrl:[NSURL URLWithString:[wish hasExecutedImageWithSizes] ? [wish getCoverExecutedOriginal] : [wish getCoverOriginal]] onCompleteChange:^(UIImage *image) {
                if( image != nil ) {
                    _imageView.image = image;
//                    [_imageView animateAppearanceImage:image];
//                    self.imageView.image = [image scaleToSize:CGSizeMake(screen.size.width, screen.size.width / scale)];
                }
            }];
        }
    }];

    
}


- (void)setRewishCount
{
    self.rewishLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[wish getRewishesCount]];
}


- (void)setWishName
{
    [self.labelName setFont: [UIFont h_defaultFontLightSize: 30.0]];
    self.labelName.text = [wish getName];
    [self.labelName setBackgroundColor: [wish isImplement] ? cImplement : cNotImplement];
}

#define paddingFromDateLabel 5.0
- (void)setMasterWishView
{
    
    self.masterAddWishDateLabel.text = [NSString h_timeAgoUserFriendlyFromStringDate:[wish getCreatedDate]];
    self.masterUsernameLabel.text = [[wish getOwner] getFullname];
    self.masterDescription.text = [wish getDescription];
    self.masterDescription.userInteractionEnabled = YES;
    
    [self.masterAddWishDateLabel sizeToFit];
    self.paddingMasterUsernameRightFromView.constant = -self.masterAddWishDateLabel.frame.size.width - paddingFromDateLabel;
    
    if ( ![[wish getUrl] isEqualToString:@""] ) {
        NSURL *url = [NSURL URLWithString:[wish getUrl]];
        NSString *host = url.host != nil ? url.host : [wish getUrl];
        
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\rДобавлено с %@", [wish getDescription], host]];
        
        [string addAttributes: @{NSForegroundColorAttributeName : [wish isImplement] ? [UIColor h_smoothDeepGreen] : [UIColor h_smoothDeepYellow]} range: NSMakeRange(string.length - host.length, host.length)];
        
        self.masterDescription.attributedText = string;
    }
    
    UITapGestureRecognizer *descTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToDesc:)];
    UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTapAction:)];
    [self.viewMasterAvatar addGestureRecognizer:avatarTap];
    [self.masterDescription addGestureRecognizer:descTap];
    
    [UIImage downloadWithUrl: [NSURL URLWithString:[[wish getOwner] getAvatar]] onCompleteChange:^(UIImage *image) {
        if( image != nil ) {
            [self.viewMasterAvatar setImage:image];
        }
    }];
}

- (IBAction)tapToDesc:(UIGestureRecognizer *)recognizer
{
    if ( ![[wish getUrl] isEqualToString:@""] ) {
        NSURL *url = [NSURL URLWithString:[wish getUrl]];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)avatarTapAction:(UIGestureRecognizer *)sender
{
    [self performSegueWithIdentifier:@"userDetail" sender:[wish getOwner]];
}

- (BOOL)isLoadedAllComments
{
//    NSLog(@"%u %lu %lu", [[wish getComments] count] == [wish getCommentsCount], [[wish getComments] count], [wish getCommentsCount]);
    return [[wish getComments] count] == [wish getCommentsCount];
}

#pragma mark -
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if( [segue.identifier isEqualToString:@"userDetail"] ) {
        [Routing prepareForSegue:segue sender:sender];
    } else if ( [segue.identifier isEqualToString:@"comments"] ) {
        [Routing goToCommentsController:segue wishId:[wish getId] wishName:[wish getName] comments:[wish getComments] maxCount:[wish getCommentsCount] replyToCommentId:(uint32_t)[sender[@"cid"] unsignedLongLongValue] replyToUserName:sender[@"userName"]];
    } else if ( [segue.identifier isEqualToString:@"executeWish"] ) {
        [Routing prepareForSegue:segue sender:sender];
    } else if ( [segue.identifier isEqualToString:@"showSpam"] ) {
        [Routing showSpamController:segue wishId:[wish getId]];
    }
}

- (IBAction)actionBackButton:(id)sender {
    NSInteger ctrlCount = [[self.navigationController viewControllers] count];
    
    if ( ctrlCount > 1 ) {
        UIViewController *ctrl = [self.navigationController viewControllers][ [[self.navigationController viewControllers] count] - 2 ];
        
        if ( [ctrl isKindOfClass:[EventsViewController class]] ) {
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
    
    [self performSegueWithIdentifier:@"wishDetailUnwind" sender:nil];
}

- (IBAction)actionSendButton:(id)sender {
    [self performSegueWithIdentifier:@"comments" sender:@{@"cid": [NSNumber numberWithInteger:0],
                                                          @"userName": @""}];
}

- (IBAction)actionAdditionalButton:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *share = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"SocialShare"]
                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                        
                                                        SocialShareWishViewController *ctrl = [[SocialShareWishViewController alloc] initWithNibName:@"ShareWish" bundle:nil];
                                                        [ctrl setDetail:wish];
                                                        [self presentViewController:ctrl animated:YES completion:nil];

                                                    }];
    
    UIAlertAction *savePicture = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"SavePicture"]
                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                        [ClusterPrePermissions h_showPhotoPermissionsWithCallBack:^{
                                                            UIImageWriteToSavedPhotosAlbum( _imageView.image,
                                                                                           self,
                                                                                           @selector(image:finishedSavingWithError:contextInfo:),
                                                                                           nil);
                                                        } rejectFirstStep:^{
                                                            [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"AccessPhotosDeny"]];
                                                        } rejectSecondStep:^{
                                                        }];
                                                    }];
    
    UIAlertAction *sendByEmail = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"SendByEmail"]
                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                        
                                                        if([MailViewController canSendMail]) {
                                                            MailViewController *mail = [[MailViewController alloc] init];
                                                            mail.notification = _notification;
                                                            [mail setMessageBody:[NSString stringWithFormat:[[ActionTexts sharedInstance] text:@"FriendWantWishByEmail"], [[wish getOwner] getFullname], [wish getName], [Settings sharedInstance].domain, [wish getId], [Settings sharedInstance].domain, [wish getId]] isHTML:YES];
                                                            [mail insertImageToBody:_imageView.image];
                                                            [mail setSubject:[NSString stringWithFormat:[[ActionTexts sharedInstance] text:@"FriendWantWishByEmailSubject"], [[wish getOwner] getFullname], [wish getName]]];
                                                            
                                                            [self presentViewController:mail animated:YES completion:nil];
                                                        } else {
                                                            [_notification showNotify:1 withText:[[ActionTexts sharedInstance] text:@"EmailDenySend"]];
                                                        }
                                                    }];
    
    UIAlertAction *copyLink = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"CopyWishUrl"]
                                                style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                                    
                                                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                    pasteboard.string = [NSString stringWithFormat:@"%@wish/%zd", [Settings sharedInstance].domain, [wish getId]];
                                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification"
                                                                                                        object:nil
                                                                                                      userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"CopyWishUrl"]}];
                                                    
                                                }];
    
    UIAlertAction *spam = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"Spam"]
                                                    style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                        [self performSegueWithIdentifier:@"showSpam" sender:nil];
                                                    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"Cancel"]
                                                    style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                    }];
    
    [alert addAction:share];
    [alert addAction:savePicture];
    [alert addAction:copyLink];
    [alert addAction:sendByEmail];
    [alert addAction:spam];
    [alert addAction:cancel];
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = _buttonDots;
    popPresenter.sourceRect = _buttonDots.bounds;
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}


#pragma mark -
#pragma mark - ClusterPrePermissions
- (void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        [_notification showNotify:0 withText:[error localizedDescription]];
    } else {
        [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"PhotoSaved"]];
    }
}


#pragma mark -
#pragma mark CommentDelegate

- (void)replyUserFromComment:(ModelComment *)comment
{
    [self performSegueWithIdentifier:@"comments" sender:@{@"cid": [NSNumber numberWithUnsignedLongLong:[comment getId]],
                                                          @"userName": [[comment getUser] getName]}];
}

- (void)removeComment:(ModelComment *)comment
{
    if ( isDeleting ) { return; }

    isDeleting = YES;
    
    [[WishesAPI sharedInstance] deleteComment:[comment getId] withCallBack:^(id item) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeCommentFromCC" object:nil userInfo:@{@"wishId"   : [NSNumber numberWithUnsignedLong:[wish getId]], @"commentId"  : [NSNumber numberWithUnsignedLong:[comment getId]]}];

//        [_commentsTableView reloadData];
        [self reloadScrollView];
        isDeleting = NO;
    } failedBlock:^(id errorText){
        isDeleting = NO;
    }];
}

- (void)showUserDetail:(ModelUser *)user
{
    [[Routing sharedInstance] showUserControllerWithUser:user fromMenu:NO];
}


#pragma mark -
#pragma mark ActionsWish
- (IBAction)actionButtonWant:(id)sender {
    [[WishlistSelectViewController sharedInstance] showInView:self.view animated:YES];
}

- (IBAction)actionButtonImplement:(id)sender
{
    [self performSegueWithIdentifier:@"executeWish" sender:wish];
}

- (IBAction)actionButtonDelete:(id)sender
{
    if ( [wish isMyWish] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"WishDelete"]
                                                        message:[[ActionTexts sharedInstance] text:@"WishDelete"]
                                                       delegate:self
                                              cancelButtonTitle:@"Нет"
                                              otherButtonTitles:[[ActionTexts sharedInstance] titleText:@"Delete"], nil];
        [alert show];
    } else {
        [self tryDeleteWish];
    }
}


- (void)tryDeleteWish
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] deleteWish:[wish getId] withCallBack:^(id item) {
        isLoading = NO;
        
        if ( [wish isMyWish] ) {
            [wish deleteWish];
            [self actionBackButton:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishDelete"]}];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:[wish isImplement] ? @"STWishExecutedDelete" : @"STWishDelete"
                                                                object:nil
                                                              userInfo:@{
                                                                         @"increase"    : [NSNumber numberWithBool:NO],
                                                                         @"wishlistId"  : [NSNumber numberWithUnsignedLongLong:[wish getWishlistId]],
                                                                         @"wishId"      : [NSNumber numberWithUnsignedLongLong:[wish getId]]}];
            
//            [_collectionView removeItem:wishInAction];
        } else {
            [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"WishDelete"]];
            [wish wishInWishlist:NO];
        }
        
    } failedBlock:^(id item) {
        isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": [item isEqualToString:@""] ? [[ActionTexts sharedInstance] failedText:@"WishDelete"] : item}];
    }];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if ( buttonIndex == 1) {
        [self tryDeleteWish];
    }
}



#pragma mark -
#pragma mark PUViewProtocol

- (void)successPopUp:(uint32_t)wlistId
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] rewish:[wish getId] toWishlistId:wlistId withCallBack:^(id item) {
        isLoading = NO;
        [wish wishInWishlist];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishRewish"]}];
    } failedBlock:^(id item) {
        isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": [item isEqualToString:@""] ? [[ActionTexts sharedInstance] failedText:@"WishRewish"] : item}];
    }];
}


#pragma mark -
#pragma mark CommentsTableViewDelegate

- (void)didSelectLoadMoreCell
{
    [self performSegueWithIdentifier:@"comments" sender:nil];
}

- (void)willDisplayLastCell
{
    [self reloadScrollView];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( _offsetTop >= 0 ) {
        if ( _offsetTop < scrollView.contentOffset.y ) {
            [self hideOnScroll];
        } else {
            [self showOnScroll];
        }
    }
    
    _offsetTop = scrollView.contentOffset.y;
}

- (void)hideOnScroll
{
    if ( _buttonBack.layer.opacity != 0.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            _buttonBack.layer.opacity = 0.0;
            _buttonDots.layer.opacity = 0.0;
        }];
    }
    
}

- (void)showOnScroll
{
    if ( _buttonBack.layer.opacity != 1.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            _buttonBack.layer.opacity = 1.0;
            _buttonDots.layer.opacity = 1.0;
        }];
    }
}

@end
