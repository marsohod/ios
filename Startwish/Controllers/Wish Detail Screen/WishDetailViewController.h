//
//  WishDetailViewController.h
//  Startwish
//
//  Created by marsohod on 13/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelWish.h"
#import "LabelLikeTextField.h"
#import "UIViewAvatar.h"
#import "UIImageViewWish.h"
#import "WantAlreadyButton.h"
#import "WantButton.h"
#import "ImplementButton.h"
#import "DeleteButton.h"
#import "NotificationLabel.h"
#import "WishlistSelectViewController.h"
#import "CommentButton.h"
#import "CommentsTableView.h"
#import "BackFillButton.h"
#import "DotsFillButton.h"
#import "GAITrackedViewController.h"


@interface WishDetailViewController : GAITrackedViewController <UIScrollViewDelegate>

- (void)setDetail:(ModelWish *)item;
- (void)setWishId:(uint32_t)wishId;

@end
