//
//  EventsViewController.m
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventsViewController.h"
#import "RevealViewController.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "SSPullToRefreshView.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UserAPI.h"
#import "Routing.h"
#import "ActionTexts.h"
#import "PushNotificationHandler.h"


@interface EventsViewController ()
{
    BOOL isLoading;
    BOOL isNotAvailable;
}
@end

@implementation EventsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Немного жестов для открытия меню
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [self.viewTop setBackgroundColor: [UIColor h_smoothGreen]];
    [_labelView setFont: [UIFont h_defaultFontSize: 16.0]];
    [_labelView setTextColor: [UIColor h_bg_contacts]];
    [_labelView setTextAlignment: NSTextAlignmentCenter];
    [_labelView setText: [_labelView.text uppercaseString]];
    [_labelView setBackgroundColor: [UIColor h_smoothGreen]];
    
    [_tableView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    
    _tableView.pullToRefreshView.delegate = (id<SSPullToRefreshViewDelegate>)self;
    [_tableView.pullToRefreshView.contentView setBackgroundColor:[UIColor whiteColor]];

    [_tableView setSeparatorColor: [UIColor h_bg_commentViewBorder]];
    _tableView.tapDelegate = (id<EventTableViewDelegate>)self;
    
    self.preload = [[PreloadView alloc] init];
    [self.view addSubview: self.preload];
    
    _viewEmpty = [[EmptyView alloc] initToView:self.view
                                      withText:[[ActionTexts sharedInstance] titleText:@"EmptyEvents"]
                                          desc:[[ActionTexts sharedInstance] titleText:@"EmptyEventsDescription"]
                                   buttonTitle:@"найти друзей"];
    _viewEmpty.edelegate = (id<EmptyViewDelegate>)self;
    _viewEmpty.needMenuButton = YES;
    
    [self.view insertSubview:_viewEmpty belowSubview:_notification];
    
    [_viewEmpty hide];
    [_preload showPreload];
    
    [self loadItems];
    
    [PushNotificationHandler cleanNewEventsCount];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNewItems) name:@"STNeedNewEvents" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNewItems) name:@"STPushIsReceived" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Events Screen";
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)loadItems
{
    if( isLoading || isNotAvailable ) { return; }
    isLoading = YES;
    
    [_tableView handleBeginRefreshing];
    [[UserAPI sharedInstance] eventsWithOffset:[_tableView itemsCount] success:^(NSArray *items) {
        isLoading = NO;
        [self.preload hidePreload];
        [_tableView handleEndRefreshing];
        
        if ( [items count] == 0 ) {
            isNotAvailable = YES;
            _tableView.bottomRefreshControl = nil;
            
            if ( _tableView.currentPage == 0 ) {
                [_viewEmpty show:YES withButton:YES];
            }
            return;
        }
                                           
        if ( [_tableView currentPage] == 0 ) {
            [[UserAPI sharedInstance] redeemEvents];
        }
        
        [_tableView updateWithItems:items];
    } failed:^(id item) {
        [self.preload hidePreload];
        [_tableView handleEndRefreshing];
        isLoading = NO;
    }];
     
//    } withPage: [_tableView currentPage]];
}

- (void)loadNewItems
{
    [PushNotificationHandler cleanNewEventsCount];
    
    if ( !isLoading && (self.isViewLoaded && self.view.window) ) {
        isLoading = YES;
        [_tableView.pullToRefreshView startLoadingAndExpand:YES animated:YES];
        
        [[UserAPI sharedInstance] eventsAfterUpdate:[_tableView firstItemDate] withCallBack:^(NSArray *items) {
            isLoading = NO;
            [self.preload hidePreload];
            
            [_tableView.pullToRefreshView finishLoading];
            [_tableView updateWithNewestItems:items];
            [[UserAPI sharedInstance] redeemEvents];
        } withFailedCallBack:^(id item) {
            isLoading = NO;
            [self.preload hidePreload];
            [_tableView.pullToRefreshView finishLoading];
        }];
        
        return;
    }
    
    [_tableView.pullToRefreshView finishLoading];
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
    [self loadNewItems];
}

#pragma mark -
#pragma mark EventTableViewDelegate


- (void)showWish:(id)sender
{
    [self performSegueWithIdentifier:@"wishDetail" sender:sender];
}

- (void)showWishComments:(id)wish replyToUserName:(NSString *)userName replyToCommentId:(uint32_t)commentId;
{
    if ( !wish ) { return; }
    
    [self performSegueWithIdentifier:@"comments" sender:@{@"wishId"     : [NSNumber numberWithLongLong:[wish getId]],
                                                          @"wishName"   : [wish getName],
                                                          @"commentId"  : [NSNumber numberWithLongLong:commentId],
                                                          @"maxCount"   : [NSNumber numberWithInteger:[wish getCommentsCount]],
                                                          @"userName"    : userName}];
}

- (void)showUser:(id)user
{
//    [self.navigationController pushViewController:[Routing userCtrlWithUser:user] animated:YES];
    
    [self performSegueWithIdentifier:@"userDetail" sender:user];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if( [segue.identifier isEqualToString: @"comments"] ) {
        [Routing goToCommentsController:segue
                                 wishId:(uint32_t)[sender[@"wishId"] longLongValue]
                               wishName:sender[@"wishName"]
                               comments:nil
                               maxCount:[sender[@"maxCount"] integerValue]
                       replyToCommentId:(uint32_t)[sender[@"commentId"] longLongValue]
                        replyToUserName:sender[@"userName"]];
    } else {
        [Routing prepareForSegue:segue sender:sender];
    }
}

#pragma mark - EmptyViewDelegate
- (IBAction)actionButton:(id)sender {
    [[Routing sharedInstance] showPeopleSearchCtrlFromMenu:YES];
}
@end
