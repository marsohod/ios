//
//  EventsViewController.h
//  Startwish
//
//  Created by marsohod on 25/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "EventTableView.h"
#import "PreloadView.h"
#import "GAITrackedViewController.h"
#import "EmptyView.h"
#import "NotificationLabel.h"


@interface EventsViewController : GAITrackedViewController

@property (strong, nonatomic) PreloadView *preload;
@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet EventTableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (strong, nonatomic) IBOutlet EmptyView *viewEmpty;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

- (IBAction)actionMenuButton:(id)sender;

@end
