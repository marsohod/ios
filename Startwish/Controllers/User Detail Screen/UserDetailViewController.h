//
//  UserDetailViewController.h
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ModelUser.h"
#import "UIViewAvatar.h"
#import "FillCircleButton.h"
#import "BGBlueView.h"
#import "RedButton.h"
#import "BackButton.h"
#import "MenuButton.h"
#import "SettingsButton.h"
#import "GAITrackedViewController.h"


@interface UserDetailViewController : GAITrackedViewController <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate>

- (IBAction)actionMenuButton:(id)sender;
- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionSubscribe:(id)sender;
- (IBAction)actionSettingsButton:(id)sender;
- (IBAction)actionAdditionalButton:(id)sender;

@property (weak, nonatomic) IBOutlet BGBlueView *contentView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeight;


@property (weak, nonatomic) IBOutlet UIViewAvatar *viewAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UICollectionView *info;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *privateLabel;
@property (weak, nonatomic) IBOutlet FillCircleButton *lock;
@property (weak, nonatomic) IBOutlet RedButton *buttonSubscribe;
@property (weak, nonatomic) IBOutlet BackButton *buttonBack;
@property (weak, nonatomic) IBOutlet MenuButton *buttonMenu;
@property (weak, nonatomic) IBOutlet UIButton *buttonDots;
@property (weak, nonatomic) IBOutlet SettingsButton *buttonSettings;

- (void)setDetail:(ModelUser *)userShort fromMenu:(BOOL)state;
- (BOOL)thisUserIsOpened:(ModelUser *)userShort;

@end
