//
//  UserDetailViewController.m
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UserDetailViewController.h"
#import "RevealViewController.h"
#import "ProfileInfoCellFirst.h"
#import "ProfileInfoCell.h"
#import "Routing.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "NSString+Helper.h"
#import "UILabel+typeOfText.h"
#import "UserAPI.h"
#import "Me.h"
#import "WishesAPI.h"
#import "NSString+Helper.h"
#import "ActionTexts.h"
#import "Settings.h"


@interface UserDetailViewController ()
{
    ModelUser *user;
    NSMutableArray *userStats;
    BOOL isRequestFromMenu;
    BOOL isLoading;
}
@end

@implementation UserDetailViewController

static NSString *infoCellName;
static NSString *infoCellNameFirst;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInfoAfterRemoveWishlist:) name:@"STWishlistDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishlistCount:) name:@"STWishlistAdd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishesCount:) name:@"STWishDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishesCount:) name:@"STWishAdd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishesExecutedCount:) name:@"STWishExecutedDelete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(increaseWishesExecutedCount:) name:@"STWishExecutedAdd" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contactsUpdate:) name:@"STContactsUpdate" object:nil];
    
    // Аватарка
    _viewAvatar.isHighlighted = YES;
    [_viewAvatar customDrawView];
    
    [self renderUserName];
    [self renderDescription];
    [self renderInfo];
    [self renderLocation];
    [self renderButtons];
    
    _contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self getUser];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"User Detail Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ( _buttonMenu.hidden == NO ) {
        [_buttonMenu updateEventCount:nil];
    }
}

- (void)dealloc
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
//    NSLog(@"111 %f %f %@", [[UIScreen mainScreen] bounds].size.height, _contentView.frame.size.height,  NSStringFromCGSize(_scrollView.contentSize ));
//    [_contentView layoutSubviews];
//    NSLog(@"%@", NSStringFromCGSize(_scrollView.contentSize ));
//    if( [[UIScreen mainScreen] bounds].size.height - 84.0 > _contentView.frame.size.height ) {
//        [self.view addConstraint: [NSLayoutConstraint constraintWithItem: _contentView
//                                                               attribute: NSLayoutAttributeHeight
//                                                               relatedBy: NSLayoutRelationEqual
//                                                                  toItem: nil
//                                                               attribute: NSLayoutAttributeHeight
//                                                              multiplier: 1.0
//                                                                constant: [[UIScreen mainScreen] bounds].size.height - 84.0]];
//    }
    [self reloadScrollView];
}


- (void)getUser
{
    if( [user getId] ) {  
        [[UserAPI sharedInstance] getUserById:[user getId] withCallback:^(ModelUser *u){
            user = u;
            [self setProfile];
            
        }];
    }
}


#pragma mark -
#pragma mark Render Elements

- (void)setProfile
{
    // Имя пользоваетеля
    _userName.text = [user getFullname];
    [_userName sizeToFit];
    
    // Аватарка
    [UIImage downloadWithUrl: [NSURL URLWithString: [user getAvatar]] onCompleteChange:^(UIImage *image) {
        [_viewAvatar setImage:image];
//        _avatar.image = [[_avatar prepareImageToAvatarImage: image] scaleToSize:[UIImageViewAvatarBig avatarImageSize]];
    }];
    
    if( ![Me isMyProfile:[user getId]] && ![user canSeeProfile] ) {
        [self renderPrivateProfileInfo];
    } else {
        [self renderProfileInfo];
    }
}

- (void)renderPrivateProfileInfo
{
    NSArray *userNameByWord = [[user getFullname] componentsSeparatedByString:@" "];
    
    _info.hidden = YES;
    _desc.hidden = YES;
    _desc.text = @"";
    [_desc sizeToFit];
    _location.text = @"";
    [_location sizeToFit];
    _location.hidden = YES;
    
    [_info removeFromSuperview];
//    [_desc removeFromSuperview];
//    [_location removeFromSuperview];
    
    _privateLabel.hidden = NO;
    _lock.hidden = NO;
    
    [_privateLabel setFont: [UIFont h_defaultFontLightSize: 16.0]];
    [_privateLabel setTextColor: [UIColor whiteColor]];
    [_privateLabel setTextAlignment: NSTextAlignmentCenter];
    [_privateLabel setNumberOfLines: 0];
    [_privateLabel setLineBreakMode: NSLineBreakByWordWrapping];
    
    _privateLabel.text = [NSString stringWithFormat: @"%@ ограничил%@ доступ \r к своим данным", [userNameByWord count] > 0 ? userNameByWord[0] : @"", ([user isMan] ? @"" : @"а")];
    
    [self renderSubscribeButton];
}

- (void)renderProfileInfo
{
    _info.hidden = NO;
    _desc.hidden = NO;
    _location.hidden = NO;
    
    _privateLabel.hidden = YES;
    _lock.hidden = YES;
    
    _desc.text = [user getDescription];

    [_desc sizeToFit];
    [self setLocationInfo];
    [self renderSubscribeButton];
    
    if ( ![Me isMyProfile:[user getId]] ) {
        if ( user && [user getCountFriendsMutual] == 0 ) {
            [userStats removeObjectAtIndex:6];
        }
        
        if ( user && [user getCountWishesMutual] == 0 ) {
            [userStats removeObjectAtIndex:3];
        }
    }
    
    [_info reloadData];
}

- (void)renderSubscribeButton
{
    if ( [Me isMyProfile:[user getId]] ) {
    } else if( [user isSubscriber] ) {
        [_buttonSubscribe setTitle: [[ActionTexts sharedInstance] titleText:@"Unsubscribe"] forState: UIControlStateNormal];
        _buttonSubscribe.hidden = NO;
        _buttonSubscribe.backgroundColor = [UIColor h_greenDirty];
        [_buttonSubscribe setNeedsDisplay];
    } else {
        _buttonSubscribe.backgroundColor = [UIColor h_red];
        [_buttonSubscribe setTitle: [[ActionTexts sharedInstance] titleText:@"Subscribe"] forState: UIControlStateNormal];
        _buttonSubscribe.hidden = NO;
    }
}

- (void)setLocationInfo
{
    NSMutableArray *strings = [[NSMutableArray alloc] init];
    
    if ( ![[user getCityName] isEqualToString: @""]) {
        if ( ![[user getCountryName] isEqualToString: @""]) {
            [strings addObject: [NSString stringWithFormat:@"%@, %@", [user getCityName], [user getCountryName]]];
        } else {
            [strings addObject: [user getCityName]];
        }
    } else if ( ![[user getCountryName] isEqualToString: @""]) {
        [strings addObject: [user getCountryName]];
    }
    
    if ( ( [Me isMyProfile:[user getId]] || [user canSeeBirthday] ) && ![[user getBirthday] isEqualToString: @""] ) {
        [strings addObject: [NSString stringWithFormat:@"День рождения: %@", [NSString h_dateToUserFriendlyFromStringDate:[user getBirthday]]]];
    }
    
    if ( ![[user getRegistered] isEqualToString: @""] ) {
        [strings addObject: [NSString stringWithFormat:@"С нами: %@", [NSString h_timeFullUserFriendlyFromStringDate:[user getRegistered]]]];
    }
    
    _location.text = [strings componentsJoinedByString:@"\r"];
    [_location sizeToFit];
}

- (void)renderUserName
{
    [_userName setFont: [UIFont h_defaultFontLightSize: 28.0]];
    [_userName setTextColor: [UIColor h_smoothYellow]];
    [_userName setTextAlignment: NSTextAlignmentCenter];
    [_userName setNumberOfLines: 0];
    [_userName setLineBreakMode: NSLineBreakByWordWrapping];
}

- (void)renderDescription
{
    [_desc setFont: [UIFont h_defaultFontBoldSize: 13.0]];
    [_desc setTextColor: [UIColor whiteColor]];
    [_desc setTextAlignment: NSTextAlignmentCenter];
    [_desc setNumberOfLines: 0];
    [_desc setLineBreakMode: NSLineBreakByWordWrapping];
}

- (void)renderInfo
{
    [self defaultUserInfo];
    infoCellName = @"profileInfoCell";
    infoCellNameFirst = @"profileInfoCellFirst";
    
    [_info registerNib: [UINib nibWithNibName:@"ProfileInfoCell" bundle:nil] forCellWithReuseIdentifier: infoCellName];
    [_info setBackgroundColor: [UIColor h_bg_profile_stats]];
    [_info registerNib: [UINib nibWithNibName:@"ProfileInfoCellFirst" bundle:nil] forCellWithReuseIdentifier: infoCellNameFirst];
    [_info setBackgroundColor: [UIColor h_bg_profile_stats]];
    
    _lock.hidden = YES;
}

- (void)renderLocation
{
    [_location setFont: [UIFont h_defaultFontSize: 12.0]];
    [_location setTextColor: [UIColor whiteColor]];
    [_location setTextAlignment: NSTextAlignmentCenter];
    [_location setNumberOfLines: 0];
    [_location setLineBreakMode: NSLineBreakByWordWrapping];
}

- (void)renderButtons
{
    _buttonBack.hidden = isRequestFromMenu;
    _buttonMenu.hidden = !isRequestFromMenu;
    _buttonSettings.hidden = ![Me isMyProfile:[user getId]];
    _buttonSubscribe.hidden = YES;
    _buttonSubscribe.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
//    [_buttonSubscribe setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
}

- (void)reloadScrollView
{
    CGFloat height = _location.frame.origin.y + _location.frame.size.height;
    CGFloat minHeight = [UIScreen mainScreen].bounds.size.height - _buttonSubscribe.frame.size.height - 40;
    
    _contentViewHeight.constant = height < minHeight ? minHeight : height;
    [_contentView setNeedsLayout];
    
//    NSLog(@"reloadScroll %f %f", _location.frame.origin.y + _location.frame.size.height,_buttonSubscribe.frame.size.height);
}

- (void)defaultUserInfo
{
    // тут мы будем просто хранить индексы для
    // определения типа ячейки
    userStats = [[NSMutableArray alloc] init];
    
    for( NSInteger i = 0; i < 9; i++ ) {
        [userStats addObject:@{@"index": [NSNumber numberWithInteger:i]}];
    }
    
//    NSLog(@"%lu %@ %u",[[userDefaults objectForKey:@"userId"] unsignedLongValue], [userDefaults objectForKey:@"userId"], [user getId]);
    
    if( [Me isMyProfile:[user getId]] ) {
        // удаляем ячейку с общими друзьями и общими желаниями
        [userStats removeObjectAtIndex:6];
        [userStats removeObjectAtIndex:3];
    }
}



#pragma mark -
#pragma userView

- (void)setDetail:(ModelUser *)userShort fromMenu:(BOOL)state
{
    user = userShort;
    isRequestFromMenu = state;
}

- (BOOL)thisUserIsOpened:(ModelUser *)userShort
{
    if ( !user ) { return NO; }
    
    return [user getId] == [userShort getId];
}



#pragma mark -
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"people"]) {
        
        [Routing goToPeopleCtrl:segue userId: [user getId] peopleType:[sender[@"type"] integerValue]];
        
    } else if( [segue.identifier isEqualToString:@"wishes"] ) {
        
        [Routing goToWishesCtrl:segue user: user isCommonWishes:[sender[@"common"] boolValue] isImplementWishes: [sender[@"implement"] boolValue]];
        
    } else if( [segue.identifier isEqualToString:@"wishlists"] ) {
        
        [Routing goToWishlistsCtrl:segue user: user];
        
    } else if ( [segue.identifier isEqualToString:@"contactsDetail"] ) {
        
        [Routing goToContactsController:segue
                         canSeeContacts: [Me isMyProfile:[user getId]] ? YES : [user canSeeContacts]
                               contacts: [user getContactsList]
                               userName: [user getFullname]
                                 userId: [user getId]
                              isUserMan: [user isMan]];
        
    }
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)actionSubscribe:(id)sender
{
    if ( isLoading ) { return; }
    
    isLoading = YES;
    _buttonSubscribe.alpha = 0.7f;
    
    [[UserAPI sharedInstance] subscribe:![user isSubscriber] toUserId:[user getId] withCallBack:^(id item) {
        BOOL isFriendInPast = [user isFriend];
        
        [user setSubscribed:![user isSubscriber]];
        [self increaseRelashionship:[user isSubscriber] isFriendInPast:isFriendInPast];
        [self renderSubscribeButton];
        isLoading = NO;
        _buttonSubscribe.alpha = 1.0f;
    } failedBlock:^(id item) {
        isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification"
                                                            object:nil
                                                          userInfo:@{@"type":@"0", @"text": [[ActionTexts sharedInstance] failedText:[user isSubscriber] ? @"Unsubscribe": @"Subscribe"]}];
        _buttonSubscribe.alpha = 1.0f;
    }];
    
    
}

- (IBAction)actionSettingsButton:(id)sender {
    
}

- (IBAction)actionAdditionalButton:(id)sender
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *c = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"CopyProfileUrl"]
                                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                        
                                                        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                                                        pasteboard.string = [NSString stringWithFormat:@"%@%@", [Settings sharedInstance].domain, [user getPathToProfile]];
                                                        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification"
                                                                                                            object:nil
                                                                                                          userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"CopyProfileUrl"]}];
                                                        
                                                    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"Cancel"]
                                                     style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                                     }];
    
    [alert addAction:c];
    [alert addAction:cancel];
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = _buttonDots;
    popPresenter.sourceRect = _buttonDots.bounds;
    
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [userStats count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:infoCellName forIndexPath:indexPath];
    ProfileInfoCellFirst *cellFirst;
    
    NSInteger cellCount = 0;
    NSString *cellTitle;
    NSInteger cellType = 0;
    
    if( [userStats count] > indexPath.row ) {
        switch ( [[userStats objectAtIndex: indexPath.row][@"index"] integerValue] ) {
            case 0:
                cellFirst = [collectionView dequeueReusableCellWithReuseIdentifier: infoCellNameFirst forIndexPath:indexPath];
                
                return cellFirst;
                break;
                
            case 1:
                cellCount = [user getCountWishes];
                cellTitle = [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ЖЕЛАНИЕ", @"ЖЕЛАНИЯ", @"ЖЕЛАНИЙ"]];

                break;
            
            case 2:
                cellCount = [user getCountWishlists];
                cellTitle = [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ВИШЛИСТ", @"ВИШЛИСТА", @"ВИШЛИСТОВ"]];

                break;
                
            case 3:
                cellCount = [user getCountWishesMutual];
                cellTitle = [NSString stringWithFormat:@"%@ %@", [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ОБЩЕЕ", @"ОБЩИХ", @"ОБЩИХ"]], [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ЖЕЛАНИЕ", @"ЖЕЛАНИЯ", @"ЖЕЛАНИЙ"]]];
                
                break;
                
            case 4:
                cellCount = [user getCountWishesExecuted];
                cellTitle = @"ИСПОЛНЕНО";
                break;
                
            case 5:
                cellCount = [user getCountFriends];
                cellTitle = [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ДРУГ", @"ДРУГА", @"ДРУЗЕЙ"]];
                cellType = 1;

                break;
                
            case 6:
                cellCount = [user getCountFriendsMutual];
                cellTitle = [NSString stringWithFormat:@"%@ %@", [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ОБЩИЙ", @"ОБЩИХ", @"ОБЩИХ"]], [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ДРУГ", @"ДРУГА", @"ДРУЗЕЙ"]]];
                cellType = 1;

                break;
                
            case 7:
                cellCount = [user getCountSubscribers];
                cellTitle = [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ПОДПИСЧИК", @"ПОДПИСЧИКА", @"ПОДПИСЧИКОВ"]];
                cellType = 1;
        
                break;
                
            case 8:
                cellCount = [user getCountSubscriptions];
                cellTitle = [NSString h_russianNumeralFromInteger: cellCount ends:@[@"ПОДПИСКА", @"ПОДПИСКИ", @"ПОДПИСОК"]];
                cellType = 1;
        
                break;
                
            default:
                break;
        }
    }
             
    [cell setCellItemCount: cellCount
                      name: cellTitle
                      type: cellType];

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( !user ) {
        return;
    }
    
    if( [userStats count] > indexPath.row ) {
        switch ( [[userStats objectAtIndex: indexPath.row][@"index"] integerValue] ) {
            case 0:
                [self performSegueWithIdentifier:@"contactsDetail" sender: [user getContactsList]];
                break;
                
            case 1:
                [self performSegueWithIdentifier:@"wishes" sender:@{@"implement": [NSNumber numberWithBool:NO], @"common": [NSNumber numberWithBool:NO]}];
                break;
                
            case 2:
                [self performSegueWithIdentifier:@"wishlists" sender:nil];
                break;
                
            case 3:
                [self performSegueWithIdentifier:@"wishes" sender:@{@"implement": [NSNumber numberWithBool:NO], @"common": [NSNumber numberWithBool:YES]}];
                break;
                
            case 4:
                [self performSegueWithIdentifier:@"wishes" sender:@{@"implement": [NSNumber numberWithBool:YES], @"common": [NSNumber numberWithBool:NO]}];
                break;
                
            case 5:
                [self performSegueWithIdentifier:@"people" sender:@{@"type": [NSNumber numberWithInt:0]}];
                break;
                
            case 6:
                [self performSegueWithIdentifier:@"people" sender:@{@"type": [NSNumber numberWithInt:3]}];
                break;
                
            case 7:
                [self performSegueWithIdentifier:@"people" sender:@{@"type": [NSNumber numberWithInt:1]}];
                break;
            
            case 8:
                [self performSegueWithIdentifier:@"people" sender:@{@"type": [NSNumber numberWithInt:2]}];
                break;
                
            default:
                break;
        }
    }
}


#pragma mark -
#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.row == 0 ) {
        return CGSizeMake(40.0, 48.0);
    } if ( indexPath.row == 1 ) {
        return CGSizeMake(70.0, 45.0);
    } else {
        return  CGSizeMake(75.0, 45.0);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake( 0.0, 20.0, 0.0, 20.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


#pragma mark -
#pragma mark <UICollectionViewDelegateFlowLayout>

- (void)updateInfoAfterRemoveWishlist:(NSNotification *)notif
{
    [self increaseWishlistCount:notif];
    
    if ( notif.userInfo[@"wishesDelete"] ) {
        [user increaseCountWishes:NO byCount:[notif.userInfo[@"wishesDelete"] integerValue]];
        return;
    }
    
    if ( notif.userInfo[@"wishesExecute"] ) {
        [user increaseCountWishesExecuted:YES byCount:[notif.userInfo[@"wishesExecute"] integerValue]];
        [user increaseCountWishes:NO byCount:[notif.userInfo[@"wishesExecute"] integerValue]];
        return;
    }
}

- (void)increaseWishlistCount:(NSNotification *)notif
{
    if ( ![Me isMyProfile:[user getId]] ) { return; }
    
    [user increaseCountWishlists:[notif.userInfo[@"increase"] boolValue]];
    [_info reloadData];
}

- (void)contactsUpdate:(NSNotification *)notif
{
    if ( ![Me isMyProfile:[user getId]] ) { return; }
    
    [user setContactsList:notif.userInfo[@"contacts"]];
}

- (void)increaseWishesCount:(NSNotification *)notif
{
    if ( ![Me isMyProfile:[user getId]] ) { return; }
    [user increaseCountWishes:[notif.userInfo[@"increase"] boolValue]];
    [_info reloadData];
}

- (void)increaseWishesExecutedCount:(NSNotification *)notif
{
    if ( ![Me isMyProfile:[user getId]] ) { return; }
    [user increaseCountWishesExecuted:[notif.userInfo[@"increase"] boolValue]];
    
    if ( [notif.userInfo[@"increase"] boolValue] == YES ) {
        [user increaseCountWishes:NO];
    }
    
    [_info reloadData];
}

- (void)increaseRelashionship:(BOOL)increase isFriendInPast:(BOOL)isFriendInPast
{
    
    if ( increase ) {
        [user increaseCountSubscribers:increase];
        
        if ( [user isFriend] || [user isMySubscription] ) {
            [user increaseCountFriends:increase];
        }
        
        [_info reloadData];
        return;
    }
    
    [user increaseCountSubscribers:increase];
    
    if ( isFriendInPast ) {
        [user increaseCountFriends:increase];
    }
    
    [_info reloadData];
}

@end
