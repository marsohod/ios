//
//  CommentsViewController.m
//  Startwish
//
//  Created by marsohod on 09/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CommentsViewController.h"
#import "CommentTableViewCell.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "WishesAPI.h"
#import "ModelComment.h"
#import "Routing.h"
#import "ActionTexts.h"


@interface CommentsViewController ()
{
    uint32_t        wishId;
    NSString        *wishName;
    NSMutableArray  *comments;
    NSString        *replyTo;
    NSMutableArray  *replyToId;
    NSUInteger      maxCommentsCount;
    BOOL            isSending;
}

@end

@implementation CommentsViewController

#define COMMENT_VIEW_HEIGHT 60.0

static NSString *commentCell = @"commentCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_topView setBackgroundColor:[UIColor h_smoothGreen]];
    self.notification.hidden = YES;
    
    [self renderTextView];
    [self renderButtonSend];
    [self renderTableView];
    [self renderTopView];
    // Следим за тем, когда клавиатура появляется или исчезает
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    if ( !replyToId ) {
        replyToId = [NSMutableArray array];
    }
    
    [self loadComments];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Comments Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.notification.hidden = NO;
    
    if ( [comments count] > 0 ) {
        [_commentsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[comments count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
    [_textView becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.notification.hidden = YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Render
- (void)renderTopView
{
    [_labelWishName setFont: [UIFont h_defaultFontSize: 16.0]];
    [_labelWishName setTextColor: [UIColor h_bg_contacts]];
    [_labelWishName setText: wishName];
    [_labelWishName setBackgroundColor: [UIColor h_smoothGreen]];
}

- (void)renderTextView
{
    [_textView setPlaceholderColor:[UIColor blackColor]];
    [_textView setPlaceholder:[[ActionTexts sharedInstance] titleText:@"YourComment"]];
    [_textView.constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_textView removeConstraint:obj];
    }];
    
    if ([_textView respondsToSelector:@selector(textContainerInset)])
        _textView.textContainerInset = UIEdgeInsetsMake(5, 15, 4, 5);
    
    [_textView setFont:[UIFont h_defaultFontSize: 13.0]];
    _textView.maxNumberOfLine = 4;
    _textView.delegate = (id<UITextViewDelegate>)self;
    
    if ( replyTo != nil && ![replyTo isEqualToString:@""] ) {
        _textView.text = [NSString stringWithFormat:@"%@, ", replyTo];
        [_textView becomeFirstResponder];
    }
}

- (void)renderButtonSend
{
    _buttonSend.layer.opacity = ![_textView.text isEqualToString:@""] ? 1.0 : 0.8;
    [_buttonSend setEnabled:![_textView.text isEqualToString:@""] ? YES : NO];
}

- (void)renderTableView
{
    _commentsTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _commentsTableView.tableDelegate = (id<CommentsTableViewDelegate>)self;
    _commentsTableView.items = comments;
    _commentsTableView.maxItemsCount = maxCommentsCount;
    _commentsTableView.areCommentsExecutedWish = NO;
    
    if ( [comments count] > 0 ) {
        [_commentsTableView reloadData];
    }
    
}

- (void)loadComments
{
    isSending = YES;
    
    [[WishesAPI sharedInstance] getCommentsWishById:wishId offset: [comments count] update:^( NSArray *items) {
        [items enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [comments insertObject:obj atIndex:0];
        }];

        //TODO
        //проследить
        
//        maxCommentsCount = count;
//        _commentsTableView.maxItemsCount = count;
        [self.commentsTableView reloadData];
        
        isSending = NO;    
    } failedBlock:^(id item) {
        isSending = NO;
    }];
}


#pragma mark - UITextViewDelegate

#define maxLengthOfComment 150
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ( textView.text.length >= maxLengthOfComment && ![text isEqualToString:@""] ) {
        return NO;
    }
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ( textView.text.length > maxLengthOfComment ) {
        _textView.text = [textView.text substringToIndex:maxLengthOfComment];
    }
    
    [self renderButtonSend];
//    [_textView setText:[_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]];
}


#pragma mark -
#pragma mark Keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    BOOL isNeedScrollToBottomComment = _commentsTableView.contentSize.height - _commentsTableView.frame.size.height == _commentsTableView.contentOffset.y;
    
    _bottomSpaceCommentViewToView.constant = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [_commentsView layoutIfNeeded];
                         
                         if ( isNeedScrollToBottomComment ) {
                             [_commentsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[comments count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                         }
                         
                     }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _bottomSpaceCommentViewToView.constant = 0;
    [self.view layoutIfNeeded];
}


#pragma mark -
#pragma mark CommentsTableViewDelegate
- (void)didSelectLoadMoreCell
{
    if ( isSending == NO ) {
        [self loadComments];
    }
}

- (void)willDisplayLastCell
{

}


#pragma mark -
#pragma mark Navigation

- (void)setComments:(NSArray *)items fromWishId:(uint32_t)wid wishName:(NSString *)wName maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName
{
    wishId = wid;
    wishName = wName;
    comments = [[NSMutableArray alloc] initWithArray:items];
    maxCommentsCount = count;
    
    if ( userName && ![userName isEqualToString:@""] ) {
        replyTo = userName;
        replyToId = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithUnsignedLong:commentId], nil];
    }
}

- (IBAction)actionButtonBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionButtonSend:(id)sender
{
    if ( !_buttonSend.isEnabled ) { return; }
    if ( isSending == YES ) { return; }
    
    isSending = YES;
    _buttonSend.enabled = NO;
    
    [[WishesAPI sharedInstance] postComment:_textView.text toWishId:wishId replyToComments:replyToId withCallBack:^(id item) {
        _commentsTableView.maxItemsCount++;
        [comments addObject:item];
        
        [_commentsTableView reloadData];
        
        if ( _commentsTableView.frame.size.height < _commentsTableView.contentSize.height ) {
            [_commentsTableView setContentOffset:CGPointMake(0, _commentsTableView.contentSize.height - _commentsTableView.frame.size.height) animated:YES];
        }
        
        _textView.text = @"";
        isSending = NO;
        [self renderButtonSend];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addCommentFromCC" object:nil userInfo:@{@"wishId"   : [NSNumber numberWithUnsignedLong:wishId], @"comment"  : item}];
    } failedBlock:^(id errorText){
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"default"]];
        isSending = NO;
        [self renderButtonSend];
    }];
    
    [replyToId removeAllObjects];
}


#pragma mark -
#pragma mark CommentDelegate

- (void)replyUserFromComment:(ModelComment *)comment
{
    _textView.text = [NSString stringWithFormat:@"%@, %@", [[comment getUser] getName], _textView.text];
    [replyToId removeAllObjects];
    [replyToId addObject:[NSNumber numberWithUnsignedLong:[comment getId]]];
    [_textView becomeFirstResponder];
}

- (void)removeComment:(ModelComment *)comment
{
    if ( isSending == YES ) { return; }
    
    isSending = YES;
    
    [[WishesAPI sharedInstance] deleteComment:[comment getId] withCallBack:^(id item) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeCommentFromCC" object:nil userInfo:@{@"wishId"   : [NSNumber numberWithUnsignedLong:wishId], @"commentId"  : [NSNumber numberWithUnsignedLong:[comment getId]]}];
        
        _commentsTableView.maxItemsCount--;
        [comments removeObject:comment];
        [_commentsTableView reloadData];
        isSending = NO;
    } failedBlock:^(id errorText){
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"default"]];
        isSending = NO;
    }];
}

- (void)showUserDetail:(ModelUser *)user
{
    [[Routing sharedInstance] showUserControllerWithUser:user fromMenu:NO];
}

@end
