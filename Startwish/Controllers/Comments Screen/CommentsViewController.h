//
//  CommentsViewController.h
//  Startwish
//
//  Created by marsohod on 09/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CCGrowingTextView.h"
#import "CustomTextField.h"
#import "CommentsTableView.h"
#import "CommentButton.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface CommentsViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet CommentsTableView *commentsTableView;
@property (weak, nonatomic) IBOutlet CommentButton *buttonSend;
@property (weak, nonatomic) IBOutlet CCGrowingTextView *textView;
@property (weak, nonatomic) IBOutlet UIView *viewWithCommentField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceCommentViewToView;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *commentsView;
@property (weak, nonatomic) IBOutlet UILabel *labelWishName;


- (IBAction)actionButtonBack:(id)sender;
- (IBAction)actionButtonSend:(id)sender;
- (void)setComments:(NSArray *)items fromWishId:(uint32_t)wid wishName:(NSString *)wishName maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName;

@end
