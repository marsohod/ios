//
//  ChannelRecomendationViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 06/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ActionTexts.h"
#import "BGBlueView.h"
#import "CategoriesTableView.h"
#import "ChannelRecomendationViewController.h"
#import "MBProgressHUD.h"
#import "Me.h"
#import "PeopleTableView.h"
#import "UIColor+Helper.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UserAPI.h"
#import "WishesAPI.h"
#import "Routing.h"


@interface ChannelRecomendationViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet CategoriesTableView *tableViewCategories;
@property (weak, nonatomic) IBOutlet PeopleTableView *tableViewUsers;
@property (weak, nonatomic) IBOutlet BGBlueView *topView;
@property (weak, nonatomic) IBOutlet UIButton *buttonSkip;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewPadding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewPaddingBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewUsersPaddingBottom;

@property (nonatomic) BOOL isLoadingUsers;
@property (nonatomic) BOOL isFirstStep;
@property (nonatomic) BOOL isNotAvailable;

- (IBAction)actionBottomButton:(id)sender;
- (IBAction)actionButtonSkip:(id)sender;

@end

const NSInteger selectedCategoriesCount = 5;

@implementation ChannelRecomendationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isFirstStep = YES;
    self.revealViewController.panGestureRecognizer.enabled = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIButton:) name:@"STEditDidEndTextField" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUIButton:) name:@"STDeselectCategoryRow" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToAuthController) name:@"STNeedAuthUser" object:nil];
    [self loadCategories];
    [self renderUI];
    [self updateUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - UI
- (void)renderUI
{
    _buttonNext.layer.cornerRadius = 5.f;
    _buttonNext.layer.masksToBounds = YES;
    _buttonSkip.layer.cornerRadius = 5.f;
    _buttonSkip.layer.masksToBounds = YES;
    _tableViewUsers.cellType = 1;
    _tableViewUsers.cellDelegate = (id<PeopleTableViewDelegate>)self;
    [_tableViewUsers.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
}

- (void)updateUI
{
    _labelTitle.text    = [[ActionTexts sharedInstance] titleText: _isFirstStep ? @"CRCategories" : @"CRPeople"];
    _tableViewCategories.hidden = !_isFirstStep;
    _tableViewUsers.hidden      = _isFirstStep;
    
    [_labelTitle sizeToFit];
    
    [self updateUIButton:nil];
}

- (void)updateUIButton:(NSNotification *)notif
{
    NSInteger sicnt = 0;
    NSString *buttonTitle;
    
    if ( _isFirstStep ) {
        sicnt = [[_tableViewCategories selectedItemsId] count];
        buttonTitle = sicnt < selectedCategoriesCount ? [NSString stringWithFormat:@"Выберите %zd для продолжения", selectedCategoriesCount - sicnt] : @"Продолжить";
        
        _buttonNext.enabled = sicnt >= selectedCategoriesCount;
    } else {
        sicnt = [Me needMoreSubscriptions];
        buttonTitle = sicnt ? [NSString stringWithFormat:@"Выберите %zd для продолжения", sicnt] : @"Продолжить";
        
        _buttonNext.enabled = !sicnt;
    }
    
    
    
    _buttonNext.titleLabel.text = buttonTitle;
    [_buttonNext setTitle: buttonTitle forState:UIControlStateNormal];
    [_buttonNext setBackgroundColor: _buttonNext.enabled ? [UIColor h_blue] : [UIColor h_bg_commentViewBorder]];
    [_buttonNext setTitleColor:_buttonNext.enabled ? [UIColor whiteColor] : [UIColor h_gray] forState:UIControlStateNormal];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3
                     animations:^{
                         if ( _isFirstStep ) {
                             [_tableViewCategories layoutIfNeeded];
                         } else {
                             [_tableViewUsers layoutIfNeeded];
                         }
                         
                         [_bottomView layoutIfNeeded];
                         
                     }
                     completion:nil];

}

- (void)changePageAnimation
{
    CATransition *animationBottomView = [CATransition animation];
    animationBottomView.duration = 0.5f;
    animationBottomView.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animationBottomView.type = kCATransitionPush;
    

    animationBottomView.subtype = kCATransitionFromRight;
    [self.bottomView.layer addAnimation:animationBottomView forKey:nil];
    [self.topView.layer addAnimation:animationBottomView forKey:nil];
}

#pragma mark - Navigation
- (IBAction)actionBottomButton:(id)sender
{
    if (_buttonNext.enabled ) {
        if ( _isFirstStep ) {
            _isFirstStep = NO;
            [self loadItems];
            [self updateUI];
            [self changePageAnimation];
            return;
        }
        
        [self performSegueWithIdentifier:@"welcome" sender:nil];
    }
}

- (IBAction)actionButtonSkip:(id)sender
{
    [Me dontNeedMoreSubsciptions];
    [self performSegueWithIdentifier:@"welcome" sender:nil];
//    [[Routing sharedInstance] showFeedController:@[] andHideModal:NO];
}

- (void)goToAuthController
{
    [[Routing sharedInstance] showAuthController];
}


#pragma mark - API
- (void)loadCategories
{    
    if ( [[WishesAPI getCategoriesFromCache] count] > 0 ) {
        _tableViewCategories.items = [WishesAPI getCategoriesFromCache];
    }
    
    [[WishesAPI sharedInstance] getCategories:^(NSArray *items) {
        if ( [_tableViewCategories.items isEqualToArray:items] ) {
            return;
        }
        
        _tableViewCategories.items = [[NSArray alloc] initWithArray:items];
        [WishesAPI setCategoriesToCache:items];
        [_tableViewCategories reloadData];
    }];
}

// Cant call loadUsers becouse it is required method PeopleTableViewCellDelegate
- (void)loadItems
{
    if ( self.isLoadingUsers ) { return; }
    
    if ( _isNotAvailable ) {
        [_tableViewUsers handleEndRefreshing];
        return;
    }
    
    self.isLoadingUsers = YES;
    
    if ( [_tableViewUsers currentPage] == 0 ) {
        [MBProgressHUD showHUDAddedTo:_tableViewUsers animated:YES];
    }
    
    [[UserAPI sharedInstance] searchActivePeopleByCategories:[_tableViewCategories selectedItemsId] withPage:[_tableViewUsers currentPage] withCallback:^(NSArray *items) {
        
        NSMutableArray *ar = [[NSMutableArray alloc] initWithArray:[_tableViewUsers getItems]];
        
        [items enumerateObjectsUsingBlock:^(ModelUser* obj, NSUInteger idx, BOOL *stop) {
            [ar addObject: @{@"range": [NSValue valueWithRange:((NSRange){0,0})] , @"item": obj}];
        }];
        
        if ( [items count] == 0 ) {
            _isNotAvailable = YES;
        }
        
        [_tableViewUsers updateWithItemsIncludeOldItems:[[NSArray alloc] initWithArray:ar]];
        
        [_tableViewUsers.bottomRefreshControl endRefreshing];
        [MBProgressHUD hideHUDForView:_tableViewUsers animated:YES];
        _isLoadingUsers = NO;
    } failed:^{
        [_tableViewUsers.bottomRefreshControl endRefreshing];
        [MBProgressHUD hideHUDForView:_tableViewUsers animated:YES];
        _isLoadingUsers = NO;
    }];
}


#pragma mark -
#pragma mark PeopleTableViewCellDelegate

- (void)subscribeToUser:(uint32_t)uId
{
    [Me increaseSubscriptionCount:YES];
    [self updateUI];
    [[UserAPI sharedInstance] subscribe:YES toUserId:uId withCallBack:^(id item) {} failedBlock:^(id item) {}];
    
}

- (void)unsubscribeFromUser:(uint32_t)uId
{
    [Me increaseSubscriptionCount:NO];
    [self updateUI];
    [[UserAPI sharedInstance] subscribe:NO toUserId:uId withCallBack:^(id item) {} failedBlock:^(id item) {}];
}

- (void)showUser:(id)user
{
 
}





@end
