//
//  CitySearchViewController.h
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "CustomTableView.h"
#import "LocationManager.h"
#import "GAITrackedViewController.h"


@protocol CityDelegate;

@interface CitySearchViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

- (IBAction)actionClose:(id)sender;
- (IBAction)actionChangeSearchDidEnd:(id)sender;
- (IBAction)actionChangeSearch:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet CustomTableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarAndCtrl;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldSearch;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *itemsFiltered;

@property (nonatomic, weak) id<CityDelegate> chooseCityDelegate;
@end

@protocol CityDelegate <NSObject>
@required

- (void)chooseCityWithUpdate:(NSString *)cityName cityId:(NSNumber *)cityId;

@end