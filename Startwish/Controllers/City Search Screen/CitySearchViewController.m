//
//  CitySearchViewController.m
//  Startwish
//
//  Created by marsohod on 08/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "ClusterPrePermissions+Helper.h"
#import "CitySearchViewController.h"
#import "CityViewCell.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "SettingsAPI.h"
#import "ModelCity.h"


@interface CitySearchViewController ()
{
    LocationManager *locationManager;
    NSString *lastSearchText;
}
@end

@implementation CitySearchViewController

NSString *cellName = @"cityCell";

- (NSMutableArray *)itemsFiltered
{
    if( !_itemsFiltered ) {
        _itemsFiltered = [[NSMutableArray alloc] init];
    }
    
    return _itemsFiltered;
}

- (NSMutableArray *)items
{
    if( !_items ) {
        _items = [[NSMutableArray alloc] init];
    }
    
    return _items;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    
    tapView.cancelsTouchesInView = NO;
    
    [self.view addGestureRecognizer:tapView];
    
    // Говорим какую ячейку отображать
    [self.tableView registerNib: [UINib nibWithNibName:@"CityCell" bundle:nil] forCellReuseIdentifier: cellName];

    // Разукрашиваем таблицу:
    // Ставим прозрачный фон и разделитель ячейки
    [self.tableView setBackgroundColor: [UIColor clearColor]];
    [self.tableView setSeparatorColor: [UIColor colorWithPatternImage: [[UIImage imageNamed: @"pickerView_border.png"] scaleToSize: CGSizeMake(self.tableView.frame.size.width, 1.0)]]];
    
    [self.textFieldSearch setIcon:@"city" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-5.0 paddingLeft:0.0];
    
    locationManager = [[LocationManager alloc] init];
    locationManager.delegate = (id<LocationManagerDelegate>)self;
    
    [ClusterPrePermissions h_showLocationPermissionsWithCallBack:^{
        [locationManager startUpdatingLocation];
    } rejectFirstStep:^{
        [self loadItems];
    } rejectSecondStep:^{
        [self loadItems];
    }];
    
//    [self loadItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"City Search Screen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [self.tableView setSeparatorInset: UIEdgeInsetsMake(0, 100.0, 0, 0)];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setSeparatorInset: UIEdgeInsetsMake(0, self.view.frame.size.width, 0, 0)];
    }
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}




#pragma mark -
#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.itemsFiltered count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CityViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier: cellName];
    
    if( indexPath ) {
        [cell setMenuItem: self.itemsFiltered[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}



#pragma mark -
#pragma UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( indexPath.row < [self.itemsFiltered count] && self.itemsFiltered[indexPath.row] ) {
        [self.chooseCityDelegate chooseCityWithUpdate: [self.itemsFiltered[indexPath.row] getName] cityId: [NSNumber numberWithUnsignedLong:[self.itemsFiltered[indexPath.row] getId]]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)addItemsFiltered
{
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.itemsFiltered addObject: obj];
    }];
}

- (void)searchItemsLocal
{
    [self.items enumerateObjectsUsingBlock:^(ModelCity *obj, NSUInteger idx, BOOL *stop) {
        NSRange searchRange = [[obj getName] rangeOfString: self.textFieldSearch.text options:NSCaseInsensitiveSearch];
        
        if ( searchRange.length > 0 ) {
            [self.itemsFiltered addObject: obj];
        }
    }];
}


#pragma mark -
#pragma mark Load | Change | Show items

- (void)loadItems
{
    [_indicator startAnimating];
    
    lastSearchText = _textFieldSearch.text;
    
    [[SettingsAPI sharedInstance] citiesByName:self.textFieldSearch.text callBack:^(NSArray *response) {
        
        [self.items removeAllObjects];
        [self.items addObjectsFromArray:response];
        [self actionChangeSearchDidEnd:nil];
        [_indicator stopAnimating];
    } failedBack:^(id item) {
        [_indicator stopAnimating];
    }];
}

- (IBAction)actionClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionChangeSearchDidEnd:(id)sender
{
    if ( sender != nil && [lastSearchText isEqualToString:_textFieldSearch.text] ) {
        return;
    }
    
    [self.tableView clean];
    [self.itemsFiltered removeAllObjects];
    
    // Если мы что-то ввели
    // или всё удалили,
    // то всё равно ищем через API
    if( sender != nil ) {
        [self loadItems];
    } else {
        [self addItemsFiltered];
    }
    
    [self.tableView updateWithItems: self.itemsFiltered];
}

- (IBAction)actionChangeSearch:(id)sender
{
    [self.tableView clean];
    [self.itemsFiltered removeAllObjects];
    
    // Если поиска нет, то передаём всех вытащенных друзей
    // Иначе ищем совпадения в имени пользователя по строке поиска
    
    if( [_textFieldSearch.text isEqualToString: @""] ) {
        [self addItemsFiltered];
    } else {
        [self searchItemsLocal];
    }
    
    [self.tableView updateWithItems: self.itemsFiltered];
}


#pragma mark -
#pragma mark LocationManagerDelegate
- (void)afterDetectLocation:(CLLocation *)currentLocation placemark:(NSObject *)placemark
{
    if ( _indicator.isAnimating ) { return; }
    
    [_indicator startAnimating];
    
    [[SettingsAPI sharedInstance] cityByLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude callBack:^(id item) {
        [self.items removeAllObjects];
        
        if ( !item ) {
            [_indicator stopAnimating];
            return;
        }
        
        [self.items addObject:item];
        [self actionChangeSearchDidEnd:nil];
        
        if ( ![[item getName] isEqualToString:@""] ){
            self.textFieldSearch.text = [item getName];
        }
        
        [_indicator stopAnimating];
    } failedBack:^(id item) {
        [_indicator stopAnimating];
    }];
}

- (void)showErrorDetectLocation:(NSString *)error
{

}

@end
