//
//  WishCollectionViewController.m
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "FeedViewController.h"
#import "WishesAPI.h"
#import "UserAPI.h"
#import "Routing.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "SSPullToRefreshView.h"
#import "RevealViewController.h"
#import "ActionTexts.h"
#import "SeguePinterestOpenUnwind.h"
#import "PushNotificationHandler.h"


@implementation FeedViewController

static void (^failedActionBlock)(id errorText);
static void (^successActionBlock)(NSString *actionName);

- (void)dealloc
{
    _preloadView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // PinterestViewController
    [self.view addSubview:buttonBeast];
    
    [self prepareCollectionView];
    [self initActionBlocks];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderWishCell:) name:@"STWishUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderWishUserInfo) name:@"STProfileUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addItemsFromLaunchScreen:) name:@"STLoadFirstFeed" object:nil];
    
    // Инициализируем для того, чтобы корректно работать с удалением, добавлением вишлистов
    [WishlistSelectViewController sharedInstance];
    
    // Запрашиваем разрешение на Push Notification
    [PushNotificationHandler responseForRemoteNotifications];
    
    if ( [PushNotificationHandler sharedInstance].tappedPush ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STPushIsReceived" object:nil userInfo:@{@"push": [PushNotificationHandler sharedInstance].tappedPush}];
        [PushNotificationHandler sharedInstance].tappedPush = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.view.userInteractionEnabled = YES;
    self.revealViewController.panGestureRecognizer.enabled = YES;
    [WishlistSelectViewController sharedInstance].delegate = (id<PUViewProtocol>)self;
//    [_buttonMenu showEventCount:2];
    [self showOnScroll];
    
    [self firstUpdateItems];
    
    self.screenName = @"Feed Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_buttonMenu updateEventCount:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadItems
{
    if ( !isLoading ) {
        isLoading = YES;
        
        [_collectionView handleBeginRefreshing];
        
        [[UserAPI sharedInstance] feedsBeforeUpdate:[self.collectionView lastItemDate] withCallBack:^(NSArray *newFeedItems) {
            _viewEmptyWishes.hidden = !([newFeedItems count] == 0 && [self.collectionView currentPage] == 0);
            
            [self.collectionView updateWithItems:newFeedItems];
            isLoading = NO;
            
            [_collectionView handleEndRefreshing];
            
            if ( _preloadView ) {
                [_preloadView hidePreload];
                _preloadView = nil;
            }
        } withFailedCallBack:^(id item) {
            isLoading = NO;
            [_collectionView handleEndRefreshing];
        }];
        
        return;
    }
    
//    [_collectionView hideBottomLoader];
}

- (void)loadNewItems
{    
    if ( !isLoading ) {
        isLoading = YES;
        [self.collectionView.pullToRefreshView startLoading];
        
        [[UserAPI sharedInstance] feedsAfterUpdate:[self.collectionView firstItemDate] withCallBack:^(NSArray *newFeedItems) {
            isLoading = NO;
            [self.collectionView updateWithNewestItems:newFeedItems];
            
            if ( [newFeedItems count] != 0 ) {
                _viewEmptyWishes.hidden = YES;
            }
            
            // Обновляем только новые желания
            [self.collectionView.pullToRefreshView finishLoading];
            
        } withFailedCallBack:^(id item) {
            isLoading = NO;
            [self.collectionView.pullToRefreshView finishLoading];
        }];
    }
    
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self.collectionView.pullToRefreshView selector:@selector(finishLoading) userInfo:nil repeats:NO];
}


#pragma mark - SSPullToRefreshViewDelegate

- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
    [self loadNewItems];
}


#pragma mark - Render

- (void)renderWishCell:(NSNotification *)notif
{
    [_collectionView reloadItem:notif.userInfo[@"wish"]];
}

- (void)renderWishUserInfo
{
    [_collectionView updateProfileInfoOwnWishes];
}

- (void)addItemsFromLaunchScreen:(NSNotification *)notif
{
    isLoading = NO;
    if ( [notif.userInfo[@"items"] count] == 0 ) {
        [self loadItems];
    } else {
        [_collectionView handleEndRefreshing];
        [self.collectionView updateWithItems:notif.userInfo[@"items"]];
    }
}

- (void)prepareCollectionView
{
    [self.collectionView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    self.collectionView.pullToRefreshView.delegate = (id<SSPullToRefreshViewDelegate>)self;
    self.collectionView.tapDelegate = (id<UICollectionViewWishDelegate>)self;
    self.collectionView.contentSize = self.view.frame.size;
}

- (void)firstUpdateItems
{
    // Для обновления желаний, полученных из LaunchScreen
    RevealViewController *ctrl = (RevealViewController *)self.revealViewController;
    
    if ( [self.collectionView currentPage] != 0 ) {
        [self loadNewItems];
        return;
    }
    
    // Если уже загрузились желания. Смотрим на WelcomeViewController
    // После авторизации
    if ( feedItems != nil ) {
        if ( [feedItems count] != 0 ) {
            [self.collectionView updateWithItems:feedItems];
        } else {
            [self loadItems];
        }
        return;
    }
    
    // Если уже загрузились желания. Смотрим на LaunchViewController и RevealViewController
    // После запуска приложения под авторизованным пользователем
    if( ctrl.feedItems != nil && [ctrl.feedItems count] != 0 ) {
        [self.collectionView updateWithItems:ctrl.feedItems];
        ctrl.feedItems = nil;
        return;
    } else if ( ctrl.feedItems == nil ) {
        // Ждём NSNotification от LaunchScreen
        // Лента не успела загрузиться во время анимации при открытии приложения или авторизации
        isLoading = YES;
        [_collectionView handleBeginRefreshing];
        return;
    } else if( [self.collectionView currentPage] == 0 ) {
        [self loadItems];
    }
}

- (void)initActionBlocks
{
    failedActionBlock = ^(id errorText){
        wishInAction = nil;
        isLoading = NO;
        
        if ( errorText == nil || [errorText isEqualToString:@""] ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": [[ActionTexts sharedInstance] failedText:@"WishImplement"]}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": errorText}];
        }
    };
    
    successActionBlock = ^(NSString *actionName){
        isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:actionName]}];
        wishInAction = nil;
    };
}

- (void)hideTopLeftButton:(BOOL)state
{
    if ( [_buttonMenu isKindOfClass:[MenuFillButton class]] ) {
        
        if ( [_buttonMenu.countEvents.text isEqualToString:@""] ) {
            _buttonMenu.layer.opacity = state ? 0.0 : 1.0;
            return;
        }
        
        _buttonMenu.layer.opacity = 1.0;
        return;
    }
    
    _buttonMenu.layer.opacity = state ? 0.0 : 1.0;
}

#pragma mark -
#pragma mark UICollectionViewWishDelegate

- (void)showUserDetail:(id)user
{
    [self performSegueWithIdentifier:@"userDetail" sender:user];
}

- (void)showWishDetail:(id)wish
{
    [self performSegueWithIdentifier:@"wishDetail" sender:wish];
}

- (void)showCommentDetail:(id)wish
{
    [self performSegueWithIdentifier:@"comments" sender:wish];
}

- (void)implementWish:(ModelWish *)wish
{
    [self performSegueWithIdentifier:@"executeWish" sender:wish];
}

- (void)rewish:(ModelWish *)wish
{
    wishInAction = wish;
    [[WishlistSelectViewController sharedInstance] showInView:self.view animated:YES];
}

- (void)deleteWish:(ModelWish *)wish
{
    wishInAction = wish;
    
    if ( [wish isMyWish] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"WishDelete"]
                                                        message:[[ActionTexts sharedInstance] text:@"WishDelete"]
                                                       delegate:self
                                              cancelButtonTitle:@"Нет"
                                              otherButtonTitles:[[ActionTexts sharedInstance] titleText:@"Delete"], nil];
        [alert show];
    } else {
        [self tryDeleteWish];
    }
}

- (void)tryDeleteWish
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] deleteWish:[wishInAction getId] withCallBack:^(id item) {
        isLoading = NO;

        if ( [wishInAction isMyWish] ) {
            [_collectionView removeItem:wishInAction];
        } else {
            [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"WishDelete"]];
            [wishInAction wishInWishlist:NO];
        }
        
    } failedBlock:failedActionBlock];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if ( buttonIndex == 1) {
        [self tryDeleteWish];
    }
}


#pragma mark -
#pragma mark Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:@"comments"] ) {
        [Routing goToCommentsController:segue wishId:[sender getId] wishName:[sender getName] comments:nil maxCount:[sender getCommentsCount] replyToCommentId:0 replyToUserName:nil];
    } else {
        [Routing prepareForSegue:segue sender:sender];
    }
}

- (void)setStartFeed:(NSArray *)items
{    
    feedItems = items;
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionButton:(id)sender {
    [[Routing sharedInstance] showPeopleSearchCtrlFromMenu:NO];
}


#pragma mark -
#pragma mark PUViewProtocol

- (void)successPopUp:(uint32_t)wishlistId
{
    if ( wishInAction == nil ) {return;}

    if ( isLoading ) { return; }
    isLoading = YES;

    [[WishesAPI sharedInstance] rewish:[wishInAction getId] toWishlistId:wishlistId withCallBack:^(id item) {
        [wishInAction wishInWishlist];
        [_collectionView reloadItem:wishInAction];
        
        successActionBlock(@"WishRewish");
    } failedBlock:failedActionBlock];
}

- (CGRect)prepareWishToClose
{
    return [_collectionView prepareToCloseWish];
}

- (CGRect)prepareWishToOpen
{
    return [_collectionView prepareToOpenWish];
}

- (UIView *)cellWishOpen
{
    return [_collectionView selectedWishCell];
}

- (void)cellWishClose
{
    [_collectionView deselectSelectedItem];
}


#pragma mark -
#pragma mark SegueForUnwind
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    if ( [identifier isEqualToString:@"wishDetailUnwind"] ) {
        return [[SeguePinterestOpenUnwind alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    }
    
    return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

- (IBAction)returnFromSegueActions:(UIStoryboardSegue *)sender
{
    
}




@end
