//
//  FeedViewController.h
//  Startwish
//
//  Created by marsohod on 02/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PinterestViewController.h"
#import "ModelWish.h"
#import "UICollectionViewWish.h"
#import "ButtonBeast.h"
#import "NotificationLabel.h"
#import "WishlistSelectViewController.h"
#import "MenuFillButton.h"
#import "PreloadView.h"


@interface FeedViewController : PinterestViewController
{
    NSArray     *feedItems;
    BOOL        isNeedPopularWishes;
    BOOL        isLoading;
    ModelWish   *wishInAction;
}
- (IBAction)actionMenuButton:(id)sender;


@property (strong, nonatomic) IBOutlet UICollectionViewWish *collectionView;
//@property (weak, nonatomic) IBOutlet ButtonBeast *buttonBeast;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (weak, nonatomic) IBOutlet MenuFillButton *buttonMenu;
@property (strong, nonatomic) PreloadView *preloadView;
@property (weak, nonatomic) IBOutlet UIView *viewEmptyWishes;

//Записать желаний полученных в LaunchScreen
- (void) setStartFeed:(NSArray *)items;

@end
