//
//  RegisterViewController.h
//  Startwish
//
//  Created by marsohod on 01/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface RegisterViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldPasswordRepeat;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
- (IBAction)actionRegisterButton:(id)sender;
- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionEnterEmailDidEnd:(id)sender;
- (IBAction)actionEnterPasswordDidEnd:(id)sender;

@end
