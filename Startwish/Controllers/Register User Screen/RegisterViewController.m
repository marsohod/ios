//
//  RegisterViewController.m
//  Startwish
//
//  Created by marsohod on 01/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "RegisterViewController.h"
#import "NSString+Helper.h"
#import "LoginAPI.h"
#import "UserAPI.h"
#import "Me.h"
#import "Routing.h"
#import "MBProgressHUD.h"
#import "ActionTexts.h"
#import "WishlistsAPI.h"


@interface RegisterViewController ()
{

}
@end

@implementation RegisterViewController

#define ICON_WIDTH_WITH_PADDING 28.0

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notification.hidden = YES;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    [self.view addGestureRecognizer:tapView];
    
    // Устанавливаем иконки для полей
    [self.textFieldEmail setIcon:@"email" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-20.0 paddingLeft:0.0];
    [self.textFieldPassword setIcon:@"lock" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-8.0 paddingLeft:0.0];
    [self.textFieldPasswordRepeat setIcon:@"lock" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-8.0 paddingLeft:0.0];
    
    // Биндим проверку полей по нажатию на кнопку SEND в поле пароля
    [self.textFieldPasswordRepeat addTarget:self action:@selector(actionRegisterButton:) forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Register Screen";
    [self.textFieldEmail becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.notification.hidden = NO;
}

// Скрываем сообщение об ошибке
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.notification.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

- (BOOL)isFormValid
{
    if( [self.textFieldEmail.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailNotEnter"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    } else if( ![NSString h_isEmailString:self.textFieldEmail.text] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailBad"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    } else if( [self.textFieldPassword.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordNotEnter"]];
        [self.textFieldPassword becomeFirstResponder];
        
        return NO;
    } else if ( self.textFieldPassword.text.length < 6 ) {
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordMustBeMore6letters"]];
        [self.textFieldPassword becomeFirstResponder];
        
        return NO;
    } else if ( ![self.textFieldPassword.text isEqualToString:self.textFieldPasswordRepeat.text] ) {
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordsNotEqual"]];
        [self.textFieldPasswordRepeat becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}

// Пытаемся зарегаться
- (IBAction)actionRegisterButton:(id)sender {
    
//    [self performSegueWithIdentifier:@"settingsDetail" sender:nil];
//    return;
    
    if ( [self isFormValid] ) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self actionRegister];
        
        [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(closeAlert:) userInfo:nil repeats:NO];
        
    }
}

- (void)actionRegister
{
    [[LoginAPI sharedInstance] registerEmail: self.textFieldEmail.text withPassword: self.textFieldPassword.text withCallBack:^(ModelUser *man) {
            
        [[UserAPI sharedInstance] getMe:^(ModelUser *man) {
            [self closeAlert:nil];
            
            [Me setProfileAfterLogin:man];
            
            if ( [Me needMoreSubscriptions] ) {
                [[Routing sharedInstance] showChannelRecomendationController];
                return;
            }
            
            [[Routing sharedInstance] showChannelRecomendationController];
        } failedBlock:^(NSString *error) {
            [self closeAlert:nil];
            [self.notification showNotify:0 withText:error];
        }];
        
    } withFailedCallBack:^(NSString *error) {
        [self closeAlert:nil];
        [self.notification showNotify:0 withText:error];
    }];

}


#pragma mark -
#pragma mark Navigation
- (void)closeAlert:(id)sender
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (IBAction)actionBackButton:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionEnterEmailDidEnd:(id)sender
{
    [_textFieldPassword becomeFirstResponder];
}

- (IBAction)actionEnterPasswordDidEnd:(id)sender
{
    [_textFieldPasswordRepeat becomeFirstResponder];
}


@end
