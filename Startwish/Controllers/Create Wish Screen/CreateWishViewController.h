//
//  CreateWishViewController.h
//  Startwish
//
//  Created by marsohod on 23/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "LabelLikeTextField.h"
#import "NotificationLabel.h"
#import "CCGrowingTextView.h"
#import "CameraButton.h"
#import "SocialShareCollectionView.h"
#import "InstagramDocumentInteractionController.h"
#import "GAITrackedViewController.h"


@interface CreateWishViewController : GAITrackedViewController

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionCreateWish:(id)sender;
- (IBAction)actionEnterNameDidEnd:(id)sender;

@property (weak, nonatomic) IBOutlet UIView             *topView;
@property (weak, nonatomic) IBOutlet CameraButton       *buttonCamera;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldName;
@property (weak, nonatomic) IBOutlet CCGrowingTextView  *textViewDesc;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldWishlistName;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldCategory;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldLink;
//@property (weak, nonatomic) IBOutlet LabelLikeTextField *labelPlace;
@property (weak, nonatomic) IBOutlet UILabel            *labelSocialShare;
@property (weak, nonatomic) IBOutlet SocialShareCollectionView *collectionViewSocialShare;
@property (weak, nonatomic) IBOutlet UIScrollView       *scrollView;
@property (weak, nonatomic) IBOutlet UIView             *viewForPickerViewWishlist;
@property (weak, nonatomic) IBOutlet UIView             *viewForPickerViewCategories;
@property (weak, nonatomic) IBOutlet UIPickerView       *pickerViewWishlist;
@property (weak, nonatomic) IBOutlet UIPickerView       *pickerViewCategories;
@property (weak, nonatomic) IBOutlet NotificationLabel  *notification;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeight;
@property (weak, nonatomic) IBOutlet UIImageView        *imageViewCover;
@property (nonatomic, strong) InstagramDocumentInteractionController *documentController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewForPickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightViewForPickerViewWishlists;

- (void)setDetail:(UIImage *)cover;
@end
