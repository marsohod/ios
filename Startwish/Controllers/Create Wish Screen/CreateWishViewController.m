//
//  CreateWishViewController.m
//  Startwish
//
//  Created by marsohod on 23/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CreateWishViewController.h"
#import "SocialShareViewCell.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "WishlistsAPI.h"
#import "WishesAPI.h"
#import "ActionTexts.h"
#import "UIImage+Helper.h"
#import "VKHandler.h"
#import "InstagramHandler.h"
#import "TwitterHandler.h"
#import "MBProgressHUD.h"
#import "NSString+Helper.h"
#import "Routing.h"
#import "ModelWishlist.h"
#import "ModelWishCategory.h"
#import "Me.h"


@interface CreateWishViewController()
{
    NSMutableArray      *userWishlists;
    NSMutableArray      *categories;
    UIImage             *image;
    BOOL                isLoading;

}
@end

@implementation CreateWishViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [_topView setBackgroundColor: [UIColor h_smoothDeepYellow]];

    // Скрываем клаву при скролле
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self renderTextFields];
    [self renderViewsForPickerView];
    [self refreshWishlists];
    [self refreshCategories];
    [self renderCover:image];
    
//    [[VKHandler sharedInstance] setVKSdkDelegate:(id<VKSdkDelegate>)[VKHandler sharedInstance]];
    [VKHandler sharedInstance].ctrl = self;
    [VKHandler sharedInstance].handlerDelegate = (id<VKHandlerDelegate>)_collectionViewSocialShare;
    [[VKHandler sharedInstance] wakeUpSession];
    _collectionViewSocialShare.socialDelegate = (id<SocialShareCollectionViewDelegate>)self;
}


- (void)viewDidLayoutSubviews
{
    [self renderLabels];
//    _viewForPickerViewWishlist.frame = CGRectMake(_viewForPickerViewWishlist.frame.origin.x, _viewForPickerViewWishlist.frame.origin.y, _viewForPickerViewWishlist.frame.size.width, 0.0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Create Wish Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self animateScrollDownHalfCover];
}

- (void)animateScrollDownHalfCover
{
    if ( _scrollView.contentOffset.y == 0.0 && _scrollView.contentSize.height > _scrollView.frame.size.height ) {
        CGFloat offset = _scrollView.contentSize.height - _scrollView.frame.size.height;
        
        [UIView beginAnimations:@"scrollTo" context:nil];
        [UIView setAnimationDuration:0.5f];
        
        [_scrollView setContentOffset:CGPointMake(0, offset < _imageViewCover.frame.size.height / 2 ? offset : _imageViewCover.frame.size.height / 2 )];
        [UIView commitAnimations];
    }
}

#pragma mark -
#pragma mark Render Elements

- (void)renderTextFields
{
    
    [_textFieldName setColorDefault: @"black"];
    [_textFieldName setColorHighlight: @"black"];
    [_textFieldName setIcon:@"wishlist_name" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-10.0 paddingLeft:0.0];
    [_textFieldName setTextColor:[UIColor blackColor]];
    [_textFieldName setTintColor:[UIColor blackColor]];
    
    _textViewDesc.delegate = (id<UITextViewDelegate>)self;
    [_textViewDesc setPlaceholderColor:[UIColor blackColor]];
    [_textViewDesc setPlaceholder:@"описание"];
    [_textViewDesc.constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_textViewDesc removeConstraint:obj];
    }];
    if ([_textViewDesc respondsToSelector:@selector(textContainerInset)])
        _textViewDesc.textContainerInset = UIEdgeInsetsMake(5, 29, 4, 5);
    
    [_textViewDesc setFont:[UIFont h_defaultFontLightSize: 21.0]];
    _textViewDesc.maxNumberOfLine = 4;
    _textViewDesc.delegate = (id<UITextViewDelegate>)self;

    [_textFieldWishlistName setColorDefault: @"black"];
    [_textFieldWishlistName setColorHighlight: @"black"];
    [_textFieldWishlistName setIcon:@"wishlist" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-10.0 paddingLeft:0.0];
    [_textFieldWishlistName setIcon:@"arrow_down" toTheRightDirect:YES withMaxIconWidth:12.0 paddingTop:-35.0 paddingLeft:10.0];
    [_textFieldWishlistName setTextColor:[UIColor blackColor]];
    [_textFieldWishlistName setTintColor:[UIColor blackColor]];
    [_textFieldWishlistName setUserInteractionEnabled: YES];
    _textFieldWishlistName.delegate = (id<UITextFieldDelegate>)self;
    
    [_textFieldCategory setColorDefault: @"black"];
    [_textFieldCategory setColorHighlight: @"black"];
    [_textFieldCategory setIcon:@"category" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-22.0 paddingLeft:0.0];
    [_textFieldCategory setIcon:@"arrow_down" toTheRightDirect:YES withMaxIconWidth:12.0 paddingTop:-35.0 paddingLeft:10.0];
    [_textFieldCategory setTextColor:[UIColor blackColor]];
    [_textFieldCategory setTintColor:[UIColor blackColor]];
    _textFieldCategory.delegate = (id<UITextFieldDelegate>)self;
    
    [_textFieldLink setColorDefault: @"black"];
    [_textFieldLink setColorHighlight: @"black"];
    [_textFieldLink setIcon:@"link" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-12.0 paddingLeft:0.0];
    [_textFieldLink setTextColor:[UIColor blackColor]];
    [_textFieldLink setTintColor:[UIColor blackColor]];
}

- (void)renderLabels
{
    /*[_labelPlace setColorDefault: @"black"];
    [_labelPlace setColorHighlight: @"black"];
    [_labelPlace setIcon:@"city" toTheRightDirect:NO withMaxIconWidth:33.0 paddingTop:-2.0 paddingLeft:0.0];*/
    
    [_labelSocialShare setFont: [UIFont h_defaultFontMediumSize: 8.0]];
    [_labelSocialShare setTextColor: [UIColor h_gray]];
    [_labelSocialShare setText: [_labelSocialShare.text uppercaseString]];
}

- (void)renderViewsForPickerView
{
    _viewForPickerViewWishlist.clipsToBounds = YES;
    _viewForPickerViewCategories.clipsToBounds = YES;
}

- (void)renderCover:(UIImage *)cover
{
    if ( cover != nil ) {
        float scale = cover.size.width / cover.size.height;
        
        _imageViewCover.image = [cover scaleToSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width / scale)];
        
        // Ставим высоту картинки ещё до того, как она загрузилась
        _topViewHeight.constant = [[UIScreen mainScreen] bounds].size.width / scale;
        
    }
}
    
- (void)showPickerView:(NSString *)name
{
    float viewHeight;
    UIView *viewForPicker;
    UIPickerView *picker;
    NSLayoutConstraint *lc;
    
    if ( [name isEqualToString:@"wishlist"] ) {
        lc = self.heightViewForPickerViewWishlists;
        viewForPicker = _viewForPickerViewWishlist;
        picker = _pickerViewWishlist;
    } else {
        lc = self.heightViewForPickerView;
        viewForPicker = _viewForPickerViewCategories;
        picker = _pickerViewCategories;
    }
    
    viewHeight = viewForPicker.frame.size.height == 0 ? 140.0 : 0.0;
    
    lc.constant = viewHeight;
    
    [self.view endEditing:YES];
    
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
        [self.view layoutIfNeeded];
        
        if ( _scrollView.contentOffset.y < viewForPicker.frame.origin.y / 2 ) {
            [_scrollView setContentOffset:CGPointMake(0, viewForPicker.frame.origin.y / 2)];
        }
        
    } completion:nil];
}

- (void)refreshWishlists
{
    if ( [[Me myWishlists] count] == 0 ) {
        [[WishlistsAPI sharedInstance] getMyAllWishlistsWithCallBack:^(NSArray *items) {
            [Me setMyWishlist:items];
            
            userWishlists = [[NSMutableArray alloc] initWithArray:items];
            [_pickerViewWishlist reloadAllComponents];
            
            if ( [userWishlists count] > 0 ) {
                _textFieldWishlistName.text = [userWishlists[0] getName];
            }
            
        }];
    } else {
        userWishlists = [[NSMutableArray alloc] initWithArray:[Me myWishlists]];
        
        if ( [userWishlists count] > 0 ) {
            _textFieldWishlistName.text = [userWishlists[0] getName];
        }
    }
}

- (void)refreshCategories
{
    if ( [[WishesAPI getCategoriesFromCache] count] > 0 ) {
        ModelWishCategory *emptyCategory = [[ModelWishCategory alloc] init];
        [emptyCategory setName:@""];
        [emptyCategory setID:0];
        categories = [[NSMutableArray alloc] initWithArray:[WishesAPI getCategoriesFromCache]];
        [categories insertObject:emptyCategory atIndex:0];
    }
    
    [[WishesAPI sharedInstance] getCategories:^(NSArray *items) {
        ModelWishCategory *emptyCategory = [[ModelWishCategory alloc] init];
        categories = [[NSMutableArray alloc] initWithArray:items];
        [emptyCategory setName:@""];
        [emptyCategory setID:0];
        [categories insertObject:emptyCategory atIndex:0];
        [WishesAPI setCategoriesToCache:items];
        [_pickerViewCategories reloadAllComponents];
    }];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ( [textField isEqual:_textFieldWishlistName] ) {
        [self showPickerView:@"wishlist"];
        return NO;
    } else if ( [textField isEqual:_textFieldCategory] ) {
        [self showPickerView:@"category"];
        return NO;
    }
    
    return YES;
}


#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)componentIndex
{
    if( pickerView.tag == 0 ) {
        return [userWishlists count];
    }
    else {
        return [categories count];
    }
}


#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)componentIndex {

    return [UIScreen mainScreen].bounds.size.width - 40;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)componentIndex reusingView:(UIView *)view
{
    UILabel *label;
    
    if ([view isKindOfClass:[UILabel class]]) {
        label = (UILabel *) view;
    }
    else {
        label = [[UILabel alloc] init];
        [label setFont: [UIFont h_defaultFontSize:13.0]];
        [label setUserInteractionEnabled: YES];
        [label setBackgroundColor: [UIColor clearColor]];
    }
    
    // Устанавливаем тайтл
    if( pickerView.tag == 0 ) {
        [label setText: [userWishlists[row] getName]];
    }
    else {
        [label setText: [categories[row] getName]];
    }
    
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if( pickerView.tag == 0 ) {
        _textFieldWishlistName.text = [userWishlists[row] getName];
    }
    else {
        _textFieldCategory.text = row != 0 ? [categories[row] getName] : _textFieldCategory.placeholder;
    }
    
}

//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
//{
//    return 22.0f;
//}


#pragma mark -
#pragma mark Navigations

- (IBAction)actionBackButton:(id)sender {
    [(MainNavigationController *)self.navigationController willEnableGestureBack:YES];
    [[Routing sharedInstance] closeCameraController];
    // Check if the Facebook app is installed and we can present the share dialog
//        [FacebookHandler login];
}

- (IBAction)actionCreateWish:(id)sender
{
    if ( isLoading ) { return; }
    
    if ( ![self isValidData] ) { return; }
    
    isLoading = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [(MainNavigationController *)self.navigationController willEnableGestureBack:NO];
    
    [[WishesAPI sharedInstance] createWish:_textFieldName.text
                                      desc:_textViewDesc.text
                                  wishlist:[userWishlists[[_pickerViewWishlist selectedRowInComponent:0]] getId]
                                  category:[categories[[_pickerViewCategories selectedRowInComponent:0]] getId]
                                     cover:_imageViewCover.image
                                      link:_textFieldLink.text
                              withCallBack:^(ModelWish *item) {
                                  [item setOwner:[Me myProfile]];
                                  
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishAdd" object:nil userInfo:@{@"increase" : [NSNumber numberWithBool:YES], @"item": item, @"wishlistId": [NSNumber numberWithUnsignedLong:[(ModelWish *)item getWishlistId]]}];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"WishCreate"]}];
                                  
                                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                                  isLoading = NO;
                                  
                                  [self socialShareWish:(ModelWish *)item];
                              }
                               failedBlock:^(id item) {
                                   isLoading = NO;
                                   
                                   [(MainNavigationController *)self.navigationController willEnableGestureBack:YES];
                                   
                                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                                   [_notification showNotify:0 withText:[NSString stringWithFormat:@"%@", item]];
                               }];
    
    if ( ![self isInstagramSelected] ) {
        [(MainNavigationController *)self.navigationController willEnableGestureBack:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STClearFilters" object:nil userInfo:nil];
        [[Routing sharedInstance] closeCreateWishController];
    }
}

- (IBAction)actionEnterNameDidEnd:(id)sender
{
    [_textViewDesc becomeFirstResponder];
}

- (BOOL)isValidData
{
    
    if ( [[_textFieldName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishEmptyNameFull"]];
        return NO;
    }
    
    if ( [_textFieldName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length < 3 ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishlistNameToShort"]];
        return NO;
    }
    
    if ( !_imageViewCover.image ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"AddPicture"]];
        return NO;
    }
    
    if ( [userWishlists[[_pickerViewWishlist selectedRowInComponent:0]] getId] == 0 ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishlistNotSelected"]];
        return NO;
    }
    
    if ( [_pickerViewCategories selectedRowInComponent:0] == 0 ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"CategoryNotSelected"]];
        return NO;
    }
    
    if ( ![_textFieldLink.text isEqualToString:@""] && ![NSString h_isUrlString:_textFieldLink.text] ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"NotLink"]];
        return NO;
    }
    
    return YES;
}

- (void)setDetail:(UIImage *)cover
{
    image = cover;
}

- (void)socialShareWish:(ModelWish *)item
{
    NSArray *selectedSocial = [_collectionViewSocialShare selectedSocial];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    
//    if ( socialLength == 0 ) {
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
//    }
    
    [selectedSocial enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        switch ( [obj integerValue] ) {
            case 0:{
                // post to vk
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[VKHandler sharedInstance] postToWallWishId: [item getId]
                                                            name: [item getName]
                                                            desc: [NSString stringWithFormat:@"Я хочу %@", [item getName]]
                                                       imageLink: [item getCoverOriginal] success:^(NSDictionary *results) {
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"VKPost"]}];
                                                           
                                                       } failed:^(NSString *error) {
                                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"VKPost"]}];
                                                       }];
                });
                break;
            }
            case 1:{
                // post to fb
                [myQueue addOperationWithBlock:^{
                    /*[FacebookHandler postWishId: [item getId]
                                             name: [item getName]
                                          caption: @""
                                             desc: [NSString stringWithFormat:@"Я хочу %@", [item getName]]
                                        imageLink: [item getCoverOriginal]
                                          success:^(NSDictionary *results) {
                                              [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"FacebookPost"]}];
                                              
                                          } failed:^(NSString *error) {
                                              [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"FacebookPost"]}];
                                          } withQueue:myQueue];*/
                }];
                break;
            }
            case 2: {
                // post to tw
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [TwitterHandler postTweetWithWishId:[item getId]
                                                   text:[NSString stringWithFormat:@"Я хочу %@", [item getName]]
                                                  image:_imageViewCover.image success:^{
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"TwitterPost"]}];
                                                  } canceled:^(NSString *string) {
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"TwitterPost"]}];
                    }];
                });
                break;
            }
            case 3:
                // post to instagram
                [self trySendInstagramPost];
                break;
                
            default:
                break;
        }
        
    }];
}

- (BOOL)isInstagramSelected
{
    __block BOOL isIt = NO;
    
    [[_collectionViewSocialShare selectedSocial] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ( [obj integerValue] == 3 ) {
            isIt = YES;
        }
    }];
    
    return isIt;
}

- (void)trySendInstagramPost
{
    [InstagramHandler fileUrlToImage:_imageViewCover.image afterSave:^(NSURL *filePath) {
        if ( self.documentController == nil ) {
            self.documentController = [[InstagramDocumentInteractionController alloc] initWithUrl:filePath];
            self.documentController.delegate = (id<UIDocumentInteractionControllerDelegate>)self;
        } else {
            self.documentController.URL = filePath;
        }
        
        self.documentController.annotation = [NSDictionary dictionaryWithObject: [[ActionTexts sharedInstance] userName:@"Я" wantWishName:_textFieldName.text] forKey:@"InstagramCaption"];
        
        [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView: self.view animated: YES ];
    }];
}

#pragma mark -
#pragma mark UIDocumentInteractionControllerDelegate
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
    [(MainNavigationController *)self.navigationController willEnableGestureBack:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STClearFilters" object:nil userInfo:nil];
    [[Routing sharedInstance] closeCreateWishController];
}


#pragma mark -
#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    [_textViewDesc setText:[_textViewDesc.text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]];
}

@end
