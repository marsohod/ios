//
//  ContactsViewController.h
//  Startwish
//
//  Created by marsohod on 12/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#include "FillCircleButton.h"
#import "ContactsTableView.h"
#import "OrangeMiddleButton.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface ContactsViewController : GAITrackedViewController

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionEditButton:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (weak, nonatomic) IBOutlet UILabel *labelPrivate;
@property (weak, nonatomic) IBOutlet FillCircleButton *lock;
@property (weak, nonatomic) IBOutlet UIView *viewPrivate;
@property (weak, nonatomic) IBOutlet UIView *viewContacts;
@property (weak, nonatomic) IBOutlet ContactsTableView *tableVewContacts;
@property (weak, nonatomic) IBOutlet OrangeMiddleButton *buttonEdit;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingBottomTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingBottomButtonEdit;

- (void)setDetail:(NSDictionary *)contacts canSeeContacts:(BOOL)privateStatus userName:(NSString *)name userId:(uint32_t)uid isUserMan:(BOOL)isMan;

@end
