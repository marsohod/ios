//
//  ContactsViewController.m
//  Startwish
//
//  Created by marsohod on 12/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ContactsViewController.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "Routing.h"
#import "Me.h"
#import "UserAPI.h"
#import "ActionTexts.h"


@interface ContactsViewController ()
{
    NSMutableArray *itemsValue;
    NSMutableArray *itemsKey;
    NSMutableArray *itemsFullValue;
    NSMutableArray *itemsFullKey;
    BOOL canSeeContacts;
    BOOL isUserMan;
    BOOL isContactsChanged;
    NSString *userName;
    uint32_t userId;
}
@end


@implementation ContactsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self renderLabelView];
    [self renderLabelPrivate];
    [self renderButtonEdit];
    [self renderViews];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"User Contacts Screen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)renderLabelView
{
    [_labelView setFont: [UIFont h_defaultFontSize:16.0]];
    [_labelView setTextAlignment: NSTextAlignmentCenter];
    [_labelView setTextColor: [UIColor h_bg_contacts]];
    [_labelView setText: [_labelView.text uppercaseString]];
}

- (void)renderLabelPrivate
{
    NSArray *name = [userName componentsSeparatedByString:@" "];
    
    [_labelPrivate setFont: [UIFont h_defaultFontLightSize: 30.0]];
    [_labelPrivate setTextAlignment: NSTextAlignmentCenter];
    [_labelPrivate setNumberOfLines: 0];
    [_labelPrivate setTextColor: [UIColor whiteColor]];

    [_labelPrivate setText: [NSString stringWithFormat: @"%@ ограничил%@ доступ \r к своим контактным данным.", (name != nil ? name[0] : @"Пользователь"), (isUserMan ? @"" : @"a") ]];
}

- (void)renderButtonEdit
{
    _buttonEdit.hidden = ![Me isMyProfile:userId];
    _paddingBottomTableView.constant = [Me isMyProfile:userId] ? _paddingBottomTableView.constant : 0;
}

- (void)renderViews
{
    if( !canSeeContacts ) {
        _viewContacts.hidden = YES;
        _viewPrivate.hidden = NO;
        _tableVewContacts.itemsKey = @[];
        _tableVewContacts.itemsValue = @[];
    } else {
        _viewPrivate.hidden = YES;
        [self.view setBackgroundColor: [UIColor h_bg_profile_circle]];
        _tableVewContacts.itemsKey = [NSArray arrayWithArray: itemsKey];
        _tableVewContacts.itemsValue = [NSArray arrayWithArray: itemsValue];
        _tableVewContacts.tableDelegate = (id<ContactsTableViewDelegate>)self;
        [_tableVewContacts reloadData];
        
//        _tableVewContacts.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        
        // Следим за тем, когда клавиатура появляется или исчезает
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHide:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];

    }
}

- (void)setDetail:(NSDictionary *)contacts canSeeContacts:(BOOL)privateStatus userName:(NSString *)name userId:(uint32_t)uid isUserMan:(BOOL)isMan
{
//    NSMutableArray *itemsFull = [contacts allValues];
    
    itemsValue = [[NSMutableArray alloc] initWithArray: [contacts count] ? [contacts allValues] : @[]];
    itemsKey = [[NSMutableArray alloc] initWithArray: [contacts count] ? [contacts allKeys] : @[]];
    itemsFullValue = [[NSMutableArray alloc] initWithArray: itemsValue];
    itemsFullKey = [[NSMutableArray alloc] initWithArray: itemsKey];
    
    [self removeEmptyContacts];
    
    canSeeContacts = privateStatus;
    userName = name;
    isUserMan = isMan;
    userId = uid;
}

- (void)removeEmptyContacts
{
    for ( NSUInteger i = 0; i < [itemsValue count]; i++ ) {
        if ( [[NSString stringWithFormat:@"%@", [itemsValue objectAtIndex:i]] isEqualToString:@""] ) {
            [itemsValue removeObjectAtIndex:i];
            
            if ( i < [itemsKey count] ) {
                [itemsKey removeObjectAtIndex:i];
            }
            
            i--;
        }
    }
}


- (IBAction)actionBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionEditButton:(id)sender
{
    if ( _tableVewContacts.isEditing ) {
        [self cutValues];
        itemsKey = [[NSMutableArray alloc] initWithArray:itemsFullKey copyItems:YES];
        itemsValue = [[NSMutableArray alloc] initWithArray:itemsFullValue copyItems:YES];
        [self removeEmptyContacts];
        
        _tableVewContacts.itemsKey = [NSArray arrayWithArray: itemsKey];
        _tableVewContacts.itemsValue = [NSArray arrayWithArray: itemsValue];
        
        _buttonEdit.titleLabel.text = [[ActionTexts sharedInstance] titleText:@"Edit"];
        
        if ( isContactsChanged ) {
            [[UserAPI sharedInstance] setContacts:[[NSDictionary alloc] initWithObjects:itemsFullValue forKeys:itemsFullKey] goodBlock:^(id item) {
                // Говорим User Deatil View Controller, что обновились контакты пользователя
                [[NSNotificationCenter defaultCenter] postNotificationName:@"STContactsUpdate" object:nil userInfo:@{@"contacts": item}];
                [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"ContactsSave"]];
                isContactsChanged = NO;
            } failedBlock:^(id item) {
                [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"ContactsSave"]];
                isContactsChanged = NO;
            }];
        }
    } else {
        _tableVewContacts.itemsKey = [NSArray arrayWithArray: itemsFullKey];
        _tableVewContacts.itemsValue = [NSArray arrayWithArray: itemsFullValue];
        _buttonEdit.titleLabel.text = [[ActionTexts sharedInstance] titleText:@"Save"];
    }
    
    _tableVewContacts.isEditing = !_tableVewContacts.isEditing;
    _tableVewContacts.allowsSelection = !_tableVewContacts.allowsSelection;
    [_tableVewContacts reloadData];
}


#pragma mark -
#pragma mark ContactsTableViewDelegate
- (void)selectItemKey:(NSUInteger)idx
{
    if ( idx < [itemsKey count] ) {
        if ( ![[self canSocialOpenInBrowser:idx] isEqualToString:@""] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self canSocialOpenInBrowser:idx]]];
        }
    }
}

- (void)changeValue:(NSString *)value byInd:(NSUInteger)idx
{
    if ( idx < [itemsFullValue count] ) {
        itemsFullValue[idx] = value;
        isContactsChanged = YES;
    }
}

- (void)cutValues
{
    NSError *error = nil;
    
    for (NSInteger i = 0; i < [itemsFullValue count]; i++) {
        if ( [itemsFullKey[i] isEqualToString:@""] || itemsFullKey[i] == NULL ) {
            continue;
        }
        
        if ( [itemsFullKey[i] isEqualToString:@"livejournal"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?" options:NSRegularExpressionCaseInsensitive error:&error];
            NSRegularExpression *regex2 = [NSRegularExpression regularExpressionWithPattern:@"\\.livejournal\\.com\\/?" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            itemsFullValue[i] = [regex2 stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        }
        
        if ( [itemsFullKey[i] isEqualToString:@"vkontakte"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?(new\\.)?(vk\\.com\\/)|(vkontakte\\.ru\\/)" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        }
        
        if ( [itemsFullKey[i] isEqualToString:@"mail_ru"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?my\\.mail\\.ru\\/?" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        }
                            
        if ( [itemsFullKey[i] isEqualToString:@"youtube"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?youtube\\.com\\/?" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        }

        if ( [itemsFullKey[i] isEqualToString:@"odnoklassniki"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?ok\\.ru\\/profile\\/" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        }

        if ( [itemsFullKey[i] isEqualToString:@"googleplus"] ) {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"https?:\\/\\/(www\\.)?plus\\.google\\.com\\/" options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
            continue;
        } else {
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"https?:\\/\\/(www\\.)?%@\\.com\\/", itemsFullKey[i]] options:NSRegularExpressionCaseInsensitive error:&error];
            itemsFullValue[i] = [regex stringByReplacingMatchesInString:itemsFullValue[i] options:0 range:NSMakeRange(0, [itemsFullValue[i] length]) withTemplate:@""];
        }


    }
    
}

- (NSString *)canSocialOpenInBrowser:(NSUInteger)idx {
    if ( [itemsKey[idx] isEqualToString:@"mail_ru"] ) {
        return [NSString stringWithFormat: @"http://my.mail.ru/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"facebook"] ) {
        return [NSString stringWithFormat: @"http://facebook.com/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"twitter"] ) {
        return [NSString stringWithFormat: @"http://twitter.com/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"googleplus"] ) {
        return [NSString stringWithFormat: @"http://plus.google.com/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"instagram"] ) {
        return [NSString stringWithFormat: @"http://instagram.com/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"livejournal"] ) {
        return [NSString stringWithFormat: @"http://%@.livejournal.com", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"odnoklassniki"] ) {
        return [NSString stringWithFormat: @"http://ok.ru/profile/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"vkontakte"] ) {
        return [NSString stringWithFormat: @"http://vk.com/%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"website"] ) {
        return itemsValue[idx];
    } else if ( [itemsKey[idx] isEqualToString:@"email"] ) {
        return [NSString stringWithFormat: @"mailto:%@", itemsValue[idx]];
    } else if ( [itemsKey[idx] isEqualToString:@"youtube"] ) {
        return [NSString stringWithFormat: @"http://youtube.com/%@", itemsValue[idx]];
    }

    return @"";
}


#pragma mark -
#pragma mark Keyboard

#define SIZE_PADDING_BOTTOM_TABLE 83.0
#define SIZE_PADDING_BOTTOM_BUTTON 20.0
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
//    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    
//    _tableVewContacts.contentInset = contentInsets;
//    _tableVewContacts.scrollIndicatorInsets = contentInsets;
    _paddingBottomButtonEdit.constant = keyboardSize.height + SIZE_PADDING_BOTTOM_BUTTON;
    _paddingBottomTableView.constant = keyboardSize.height + SIZE_PADDING_BOTTOM_TABLE;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _tableVewContacts.contentInset = UIEdgeInsetsZero;
    _paddingBottomButtonEdit.constant = SIZE_PADDING_BOTTOM_BUTTON;
    _paddingBottomTableView.constant = SIZE_PADDING_BOTTOM_TABLE;
}


@end
