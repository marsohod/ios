//
//  WishlistDetailViewController.m
//  Startwish
//
//  Created by marsohod on 30/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishesViewController.h"
#import "UserDetailViewController.h"
#import "WishDetailViewController.h"
#import "WishlistsAPI.h"
#import "WishesAPI.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "Routing.h"
#import "RevealViewController.h"
#import "ActionTexts.h"
#import "UserApi.h"
#import "Me.h"
#import "SeguePinterestOpenUnwind.h"


@interface WishesViewController ()
{
}
@end

@implementation WishesViewController

static NSString * const reuseIdentifier = @"Cell";
static void(^callback)(NSArray *items);

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:buttonBeast];
    
    // Callback после получения желания
    callback = ^(NSArray *items) {
        isLoading = NO;
        [_collectionView handleEndRefreshing];

        if( [items count] == 0 ) {
            isNotAvailable = YES;
            
            if ( [self.collectionView currentPage] == 0 ) {
                [self showEmptyWishesView:YES];
                [_preload hidePreload];
            }
            
            return;
        }
        
        // Проверяем если открываем ссылку на вишлист в браузере через приложение
        if ( ![[user getAvatar] isEqualToString:@""] ) {
            NSMutableArray *newItems = [[NSMutableArray alloc] initWithArray:items];
            
            [newItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                [obj setOwner:user];
            }];
            
            [self.collectionView updateWithItems: newItems];
            [self.preload hidePreload];
            return;
        }
        
        [self.collectionView updateWithItems: items];
        [self.preload hidePreload];
    };
    
    [self.collectionView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    self.collectionView.tapDelegate = (id<UICollectionViewWishDelegate>)self;
    [_collectionView.pullToRefreshView.contentView removeFromSuperview];
    _collectionView.pullToRefreshView.delegate = (id<SSPullToRefreshViewDelegate>)self;
    
    _viewEmpty = [[EmptyView alloc] initToView:self.view
                                      withText:[self textForEmptyView]
                                          desc:[self descForEmptyView]
                                   buttonTitle:([Me isMyProfile:[user getId]] ? [[ActionTexts sharedInstance] titleText:@"AddWish"] : nil)];
    
    [self.view insertSubview:_viewEmpty belowSubview:_notification];
    
    _viewEmpty.edelegate = (id<EmptyViewDelegate>)self;
    _viewEmpty.needMenuButton = isRequestFromMenu;
    
    [self showEmptyWishesView:NO];
    [self loadItems];

    _buttonBackFill.hidden = isRequestFromMenu;
    _buttonMenu.hidden = !isRequestFromMenu;
    
    
    self.preload = [[PreloadView alloc] initWithStringColor:@"gray"];
    [self.view addSubview:self.preload];
    [_preload showPreload];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderWishCell:) name:@"STWishUpdated" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadNewItems:) name:@"STWishAdd" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // при каждом появлении экрана нам нужно менять делегат
    [WishlistSelectViewController sharedInstance].delegate = (id<PUViewProtocol>)self;
    if ( isImpeleventWishesRequest ) {
        self.screenName = @"User Executed Wishes Screen";
    } else if ( isMutualWishesRequest ) {
        self.screenName = @"User Mutual Wishes Screen";
    } else if ( isRequestFromMenu ) {
        self.screenName = @"My Wishes Screen";
    } else if ( wishlistId != 0 ) {
        self.screenName = @"User Wishlist Wishes Screen";
    } else {
        self.screenName = @"User Wishes Screen";
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_buttonMenu updateEventCount:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setDetailWishlist:(uint32_t)listId byUser:(ModelUser *)u
{
    [self resetControllerToDefault];
    wishlistId = listId;
    user = u;
}


#pragma mark -
#pragma mark Set Detail

- (void)setDetailUserWishes:(ModelUser *)u
{
    [self setDetailUserWishes:u mutualWishes:NO implementWishes:NO];
}

- (void)setDetailUserWishes:(ModelUser *)u fromMenu:(BOOL)state
{
    [self setDetailUserWishes:u mutualWishes:NO implementWishes:NO fromMenu:YES];
}

- (void)setDetailUserWishes:(ModelUser *)u mutualWishes:(BOOL)isMutual implementWishes:(BOOL)isImplement
{
    [self setDetailUserWishes:u mutualWishes:isMutual implementWishes:isImplement fromMenu: NO];
}

- (void)setDetailUserWishes:(ModelUser *)u mutualWishes:(BOOL)isMutual implementWishes:(BOOL)isImplement fromMenu:(BOOL)isMenu
{
    [self resetControllerToDefault];
    user = u;
    isImpeleventWishesRequest = isImplement;
    isMutualWishesRequest = isMutual;
    isRequestFromMenu = isMenu;
}


#pragma mark -
#pragma mark Render
- (void)resetControllerToDefault
{
//    wishlist = nil;
    wishlistId = 0;
    user = nil;
    isImpeleventWishesRequest = NO;
    isMutualWishesRequest = NO;
    isRequestFromMenu = NO;
}

- (void)showEmptyWishesView:(BOOL)status
{
//    _viewEmptyUserWishes.hidden = !status;
    _buttonBackFill.hidden = status;
    _collectionView.hidden = status;
    buttonBeast.hidden = status;
    
    [_viewEmpty show:status withButton:[Me isMyProfile:[user getId]]];
}

- (void)renderWishCell:(NSNotification *)notif
{
    if ( [Me isMyProfile:[user getId]] ) {
        ModelWish *w = (ModelWish *)notif.userInfo[@"wish"];
        
        if ( [w isDeleted] ) {
            [_collectionView removeItem:w];
            return;
        }
        
        if ( [w isImplement] && !isImpeleventWishesRequest ) {
            [_collectionView removeItem:w];
            return;
        }
    }
    
    [_collectionView reloadItem:notif.userInfo[@"wish"]];
}

- (void)hideTopLeftButton:(BOOL)state
{
    if ( _buttonBackFill.hidden == NO && [_buttonMenu isKindOfClass:[MenuFillButton class]] ) {
        
        if ( [_buttonMenu.countEvents.text isEqualToString:@""] ) {
            _buttonMenu.layer.opacity = state ? 0.0 : 1.0;
            _buttonBackFill.layer.opacity = state ? 0.0 : 1.0;
            return;
        }
        
        _buttonMenu.layer.opacity = 1.0;
        _buttonBackFill.layer.opacity = 1.0;
        return;
    }
    
    _buttonBackFill.layer.opacity = state ? 0.0 : 1.0;
    _buttonMenu.layer.opacity = state ? 0.0 : 1.0;
}

- (NSString *)textForEmptyView
{
    if ( wishlistId != 0 ) {
        return [[ActionTexts sharedInstance] titleText:@"WishlistEmpty"];
    } else {
        NSString *userType = [Me isMyProfile:[user getId]] ? @"вас" : @"пользователя";
        NSString *type = @"";
        
        if ( isMutualWishesRequest ) {
            type = @"общих ";
            userType = @"вас";
        }
        
        type = isImpeleventWishesRequest ? @"исполненных " : type;
        
        return [NSString stringWithFormat:@"У %@ пока нет \r %@желаний.", userType, type];
    }
}

- (NSString *)descForEmptyView
{
    if ( isImpeleventWishesRequest ) {
        return nil;
    } if ( isMutualWishesRequest ) {
        return [[ActionTexts sharedInstance] titleText:@"EmptyViewDesc"];
    }
    
    return ([Me isMyProfile:[user getId]] ? [[ActionTexts sharedInstance] titleText:@"EmptyViewDesc"] : nil);
}

#pragma mark -
#pragma mark Load Data
- (void)loadItems
{
    if ( isLoading ) {
        return;
    }
    
    if ( isNotAvailable ) {
        [_collectionView handleEndRefreshing];
        return;
    }
    
    isLoading = YES;
        
    [self showEmptyWishesView:NO];
    [_collectionView handleBeginRefreshing];
    
    // Загружаем жалания из вишлиста
    if( wishlistId != 0 ) {
        [[WishlistsAPI sharedInstance] getWishesWishlistById:wishlistId page:[self.collectionView currentPage] withCallBack:^(NSArray *items) {
            [_collectionView handleEndRefreshing];
            isLoading = NO;
            
            if( [self.collectionView currentPage] == 0 && [items count] == 0 ) {
                isNotAvailable = YES;

                [self showEmptyWishesView:YES];
                [_preload hidePreload];
            } else {
                callback(items);
            }
        } withFailedCallBack:^(id item) {
            [_collectionView handleEndRefreshing];
            isLoading = NO;
            
            if( [self.collectionView currentPage] == 0 ) {
//                isNotAvailable = YES;
                
                [self showEmptyWishesView:YES];
                [_preload hidePreload];
            }
        }];

    } else {
        // Загружаем общие желания с пользователем с id == userid
        if ( isMutualWishesRequest == YES ) {
            [[WishesAPI sharedInstance] getMutualWishesByUserId:[user getId] withPage:[_collectionView currentPage] withCallBack:callback withFailedCallBack:^(id item) {
                isLoading = NO;
                [_preload hidePreload];
                [_collectionView handleEndRefreshing];
            }];
        } else if ( isImpeleventWishesRequest == YES ) {
            
            // Загружаем исполненные желания пользователя
            [[WishesAPI sharedInstance] getExecutedWishesByUserId:[user getId] withPage:[_collectionView currentPage] withCallBack:callback withFailedCallBack:^(id item) {
                isLoading = NO;
                [_preload hidePreload];
                [_collectionView handleEndRefreshing];
            }];
        } else {
            
            // Загружаем желания пользователя
            [[WishesAPI sharedInstance] getWishesByUserId:[user getId] withPage:[_collectionView currentPage] withCallBack:callback withFailedCallBack:^(id item) {
                isLoading = NO;
                [_preload hidePreload];
                [_collectionView handleEndRefreshing];
            }];
        }
    }
}

- (void)loadNewItems:(NSNotification *)notif
{
    if ( [Me isMyProfile:[user getId]] ) {
        ModelWish *wish = (ModelWish *)notif.userInfo[@"item"];
        
        if( wishlistId != 0 ) {
            if ( [wish getWishlistId] == wishlistId ) {
                if ( [_collectionView itemsCount] == 0 ) {
                    [self showEmptyWishesView:NO];
                }
                
                [_collectionView updateWithNewestItems:@[wish]];
            }
        } else if ( (isMutualWishesRequest == NO && isImpeleventWishesRequest == NO) ) {
            if ( [_collectionView itemsCount] == 0 ) {
                [self showEmptyWishesView:NO];
            }
            [_collectionView updateWithNewestItems:@[wish]];
        }
    }
}


- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
    [self.collectionView.pullToRefreshView finishLoading];
}


#pragma mark -
#pragma mark UICollectionViewWishDelegate
- (void)showUserDetail:(id)userInstance
{
    [[Routing sharedInstance] showUserControllerWithUser:userInstance fromMenu:NO];
}

- (void)showWishDetail:(id)wish
{
    
    [self performSegueWithIdentifier:@"wishDetail" sender:wish];
}

- (void)showCommentDetail:(id)comment
{
    [self performSegueWithIdentifier:@"comments" sender:comment];
}

- (void)implementWish:(ModelWish *)wish
{
    [self performSegueWithIdentifier:@"executeWish" sender:wish];
}

- (void)rewish:(ModelWish *)wish
{
    wishInAction = wish;
    [[WishlistSelectViewController sharedInstance] showInView:self.view animated:YES];
}

- (void)deleteWish:(ModelWish *)wish
{
    wishInAction = wish;
    
    if ( [wish isMyWish] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"WishDelete"]
                                                        message:[[ActionTexts sharedInstance] text:@"WishDelete"]
                                                       delegate:self
                                              cancelButtonTitle:@"Нет"
                                              otherButtonTitles:[[ActionTexts sharedInstance] titleText:@"Delete"], nil];
        [alert show];
    } else {
        [self tryDeleteWish];
    }
}

- (void)tryDeleteWish
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] deleteWish:[wishInAction getId] withCallBack:^(id item) {
        isLoading = NO;
        
        if ( [wishInAction isMyWish] ) {
            [_collectionView removeItem:wishInAction];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishDelete"]}];
            [[NSNotificationCenter defaultCenter] postNotificationName:[wishInAction isImplement] ? @"STWishExecutedDelete" : @"STWishDelete"
                                                                object:nil
                                                              userInfo:@{
                                                                         @"increase"    : [NSNumber numberWithBool:NO],
                                                                         @"wishlistId"  : [NSNumber numberWithUnsignedLongLong:[wishInAction getWishlistId]],
                                                                         @"wishId"      : [NSNumber numberWithUnsignedLongLong:[wishInAction getId]]}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishDelete"]}];
            [wishInAction wishInWishlist:NO];
        }
        
    } failedBlock:^(id item) {
        wishInAction = nil;
        isLoading = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": item}];
    }];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if ( buttonIndex == 1) {
        [self tryDeleteWish];
    }
}


#pragma mark -
#pragma mark PUViewProtocol

- (void)successPopUp:(uint32_t)wlistId
{
    if ( wishInAction == nil ) {return;}
    
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] rewish:[wishInAction getId] toWishlistId:wlistId withCallBack:^(id item) {
        [wishInAction wishInWishlist];
        [_collectionView reloadItem:wishInAction];
        isLoading = NO;
        wishInAction = nil;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishRewish"]}];
    } failedBlock:^(id item) {
        NSString *errorText = ( item == nil || [item isEqualToString:@""] ) ? [[ActionTexts sharedInstance] failedText:@"WishImplement"] : item;
        
        wishInAction = nil;
        isLoading = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": errorText}];
    }];
}




#pragma mark -
#pragma mark Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:@"comments"] ) {
        [Routing goToCommentsController:segue wishId:[sender getId] wishName:[sender getName] comments:nil maxCount:[sender getCommentsCount] replyToCommentId:0 replyToUserName:nil];
    } else {
        [Routing prepareForSegue:segue sender:sender];
    }
}

- (IBAction)actionBackButton:(id)sender {
    callback = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionButton:(id)sender {
    [[Routing sharedInstance] showCameraController];
}

- (CGRect)prepareWishToClose
{
    return [_collectionView prepareToCloseWish];
}

- (CGRect)prepareWishToOpen
{
    return [_collectionView prepareToOpenWish];
}

- (UIView *)cellWishOpen
{
    return [_collectionView selectedWishCell];
}

- (void)cellWishClose
{
    [_collectionView deselectSelectedItem];
}

#pragma mark -
#pragma mark SegueForUnwind
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    if ( [identifier isEqualToString:@"wishDetailUnwind"] ) {
        return [[SeguePinterestOpenUnwind alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    }
    
    return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

- (IBAction)returnFromSegueActions:(UIStoryboardSegue *)sender
{
    
}

@end
