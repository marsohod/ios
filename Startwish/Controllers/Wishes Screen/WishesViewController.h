//
//  WishlistDetailViewController.h
//  Startwish
//
//  Created by marsohod on 30/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PinterestViewController.h"
#import "UICollectionViewWish.h"
#import "BackFillButton.h"
#import "OrangeMiddleButton.h"
#import "MenuFillButton.h"
#import "ModelUser.h"
#import "PreloadView.h"
#import "NotificationLabel.h"
#import "WishlistSelectViewController.h"
#import "ButtonBeast.h"
#import "EmptyView.h"


@interface WishesViewController : PinterestViewController
{
    uint32_t    wishlistId;
    ModelUser   *user;
    ModelWish   *wishInAction;
    BOOL        isImpeleventWishesRequest;
    BOOL        isMutualWishesRequest;
    BOOL        isRequestFromMenu;
    BOOL        isLoading;
    BOOL        isNotAvailable;
}

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionMenuButton:(id)sender;

@property (strong, nonatomic) PreloadView *preload;
@property (weak, nonatomic) IBOutlet UICollectionViewWish *collectionView;
@property (weak, nonatomic) IBOutlet BackFillButton *buttonBackFill;
@property (weak, nonatomic) IBOutlet MenuFillButton *buttonMenu;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (strong, nonatomic) EmptyView *viewEmpty;

- (void)setDetailWishlist:(uint32_t)listId byUser:(ModelUser *)u;
- (void)setDetailUserWishes:(ModelUser *)uId;
- (void)setDetailUserWishes:(ModelUser *)uId fromMenu:(BOOL)state;
- (void)setDetailUserWishes:(ModelUser *)uId mutualWishes:(BOOL)isMutual implementWishes:(BOOL)isImplement;

@end
