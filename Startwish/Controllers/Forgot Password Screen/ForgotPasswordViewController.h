//
//  ForgotPasswordViewController.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface ForgotPasswordViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet CustomTextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

- (IBAction)actionSendPassword:(id)sender;
- (IBAction)actionBackButton:(id)sender;

- (void)setDetail:(NSString *)sender;
@end
