//
//  ForgotPasswordViewController.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ForgotPasswordViewController.h"
#import "NSString+Helper.h"
#import "LoginAPI.h"
#import "ActionTexts.h"
#import "MBProgressHUD.h"


@interface ForgotPasswordViewController ()
{
    NSString *email;
    UIAlertView *alertView;
}
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    [self.view addGestureRecognizer:tapView];
    
    self.textFieldEmail.text = email;
    
    // Устанавливаем иконки для полей
    [self.textFieldEmail setIcon:@"email" toTheRightDirect:NO withMaxIconWidth:28.0 paddingTop:-20.0 paddingLeft:0.0];
    // Биндим проверку полей по нажатию на кнопку SEND в поле пароля
//    [self.textFieldEmail addTarget:self action:@selector(actionSendPassword:) forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Forgot Password Screen";
    [self.textFieldEmail becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.notification.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.notification.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

// Выслать пароль
- (IBAction)actionSendPassword:(id)sender
{
    if ( [self isFormValid] ) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [[LoginAPI sharedInstance] remindPasswordToEmail:self.textFieldEmail.text withCallBack:^(NSDictionary *response) {

            [self closeAlert:nil];
            [self.notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"MailSendToEmail"]];
            
        } withError:^(NSString *error){
            
            [self closeAlert:nil];
            [self.notification showNotify:0 withText:error];
            
        }];
        
        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(closeAlert:) userInfo:nil repeats:NO];
    } else {

    }
}

- (void)closeAlert:(id)sender
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

// Вернуться к предыдущему контроллеру
- (IBAction)actionBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setDetail:(NSString *)sender
{
    email = sender;
}

- (BOOL)isFormValid
{
    if( [self.textFieldEmail.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailNotEnter"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    } else if( ![NSString h_isEmailString:self.textFieldEmail.text] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailBad"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    }
    
    return YES;
}

@end
