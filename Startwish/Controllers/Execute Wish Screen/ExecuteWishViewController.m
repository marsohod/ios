//
//  ExecuteWishViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 01/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ExecuteWishViewController.h"
#import "WishesAPI.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "ActionTexts.h"
#import "CustomImagePicker.h"
#import "UIImage+Helper.h"
#import "VKHandler.h"
#import "InstagramHandler.h"
#import "TwitterHandler.h"
#import "MBProgressHUD.h"
#import "Routing.h"


@interface ExecuteWishViewController()
{
    ModelWish    *wish;
    BOOL        isLoading;
    BOOL        isSetExecutedCover;
}
@end

@implementation ExecuteWishViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_imageViewCover addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard:)]];
    _imageViewCover.userInteractionEnabled = YES;
    
    if ( wish != nil ) {
        [UIImage downloadWithUrl:[NSURL URLWithString:[wish getCover]] onCompleteChange:^(UIImage *image) {
            [self renderCover:image];
        }];
        
        [UIImage downloadWithUrl:[NSURL URLWithString:[wish getCoverOriginal]] onCompleteChange:^(UIImage *image) {
            [self renderCover:image];
        }];
    }
    
    [self renderLabelAndTextField];
    // Скрываем клаву при скролле
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [_buttonCamera addTarget:self action:@selector(openSelectImageDialog:) forControlEvents:UIControlEventTouchUpInside];
    
    [VKHandler sharedInstance].ctrl = self;
    [VKHandler sharedInstance].handlerDelegate = (id<VKHandlerDelegate>)_collectionViewSocialShare;
    [[VKHandler sharedInstance] wakeUpSession];
    _collectionViewSocialShare.socialDelegate = (id<SocialShareCollectionViewDelegate>)self;
    
    // Следим за тем, когда клавиатура появляется или исчезает
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Executed Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self animateScrollDownHalfCover];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark -
#pragma mark Render
- (void)renderLabelAndTextField
{
    [_labelSocialShare setFont: [UIFont h_defaultFontMediumSize: 8.0]];
    [_labelSocialShare setTextColor: [UIColor h_gray]];
    [_labelSocialShare setText: [_labelSocialShare.text uppercaseString]];
    
    _textViewDesc.delegate = (id<UITextViewDelegate>)self;
    [_textViewDesc setPlaceholderColor:[UIColor blackColor]];
    [_textViewDesc setPlaceholder:@"ваш комментарий"];
    [_textViewDesc.constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_textViewDesc removeConstraint:obj];
    }];
    if ([_textViewDesc respondsToSelector:@selector(textContainerInset)])
        _textViewDesc.textContainerInset = UIEdgeInsetsMake(5, 29, 4, 5);
    
    [_textViewDesc setFont:[UIFont h_defaultFontLightSize: 21.0]];
    _textViewDesc.maxNumberOfLine = 4;
}

- (IBAction)hideKeyboard:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Keyboard


- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize kbSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, _textViewDesc.frame.origin) ) {
        [_scrollView scrollRectToVisible:_textViewDesc.frame animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
}


#pragma mark -
#pragma mark Upload Image
- (void)openSelectImageDialog:(UITapGestureRecognizer *)gesture
{
    __weak id d = self;
    [self hideKeyboard:nil];
    [[Routing sharedInstance] showCameraControllerWithDelegate:d];
}

#pragma mark - CameraDelegate

- (void)takePhoto:(UIImage *)photo
{
    __weak id s = self;
    
    [[Routing sharedInstance] showCameraFiltersController:photo withFinishController:s];
}

- (void)takeFilteredPhoto:(UIImage *)photo
{
    
}


#pragma mark -
#pragma mark IPDelegate

- (void)setImageSelected:(UIImage *)selectedImage
{
    [self renderCover:selectedImage];
    [self animateScrollDownHalfCover];
    isSetExecutedCover = YES;
}

- (void)renderCover:(UIImage *)cover
{
    if ( cover != nil ) {
        float scale = cover.size.width / cover.size.height;
        
        _imageViewCover.image = [[cover scaleToSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width / scale)] fixOrientation];
        
        // Ставим высоту картинки ещё до того, как она загрузилась
        _coverHeight.constant = [[UIScreen mainScreen] bounds].size.width / scale;
//        [self.view setNeedsLayout];
    }
}

- (void)animateScrollDownHalfCover
{
    if ( _scrollView.contentSize.height > _scrollView.frame.size.height ) {
        CGFloat offset = _scrollView.contentSize.height - _scrollView.frame.size.height;
        
        [UIView beginAnimations:@"scrollTo" context:nil];
        [UIView setAnimationDuration:0.5f];
        
        [_scrollView setContentOffset:CGPointMake(0, offset < _coverHeight.constant / 2 ? offset : _coverHeight.constant / 2 )];
        [UIView commitAnimations];
    }
}


#pragma mark -
#pragma mark Actions
- (IBAction)actionExecuteButton:(id)sender
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ( isSetExecutedCover ) {
        [wish setCoverExecutedOriginalTemp:_imageViewCover.image];
    }
    
    // set status executed
    [wish implement];
    
    [(MainNavigationController *)self.navigationController willEnableGestureBack:NO];
    
    [[WishesAPI sharedInstance] implementWish:[wish getId]
                                        cover: isSetExecutedCover ? _imageViewCover.image : nil
                                         desc:_textViewDesc.text
                                 withCallBack:^(id w){
                                     
                                     // add comment to with with execute description
                                     if ( ![_textViewDesc.text isEqualToString:@""] ) {
                                         [[WishesAPI sharedInstance] postComment:[NSString stringWithFormat:@"Желание исполнилось: %@", _textViewDesc.text] toWishId:[wish getId] replyToComments:nil withCallBack:^(id item) {
                                         } failedBlock:^(id item) {}];
                                     }
                                     
                                     // hide preloader
                                     isLoading = NO;
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                                     
                                     [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishExecutedAdd" object:nil userInfo:@{@"increase" : [NSNumber numberWithBool:YES]}];
                                     
                                     [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"WishImplement"]}];
                                     
                                     // share wish in social
                                     [self socialShareWish:w];
                                     
                                 } failedBlock:^(id errorText){
                                     
                                     isLoading = NO;
                                     [MBProgressHUD hideHUDForView:self.view animated:YES];
        
                                     if ( errorText == nil || [errorText isEqualToString:@""] ) {
                                         [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishImplement"]];
                                     } else {
                                         [_notification showNotify:0 withText:errorText];
                                     }
                                     
                                 }];
    
    if ( ![self isInstagramSelected] ) {
        [self actionBackButton:nil];
    }
}

- (void)socialShareWish:(id)item
{
    NSArray *selectedSocial = [_collectionViewSocialShare selectedSocial];
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    
    [selectedSocial enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        switch ( [obj integerValue] ) {
            case 0:{
                // post to vk
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [[VKHandler sharedInstance] postToWallWishId: [wish getId]
                                                            name: [wish getName]
                                                            desc: [NSString stringWithFormat:@"Желание %@ исполнилось. %@", [wish getName], _textViewDesc.text]
                                                           image: _imageViewCover.image
                                                         success:^(NSDictionary *results) {
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"VKPost"]}];
                                                           } failed:^(NSString *error) {
                                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"VKPost"]}];
                                                           }];
                });
                break;
            }
            case 1:{
                // post to fb
//                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [myQueue addOperationWithBlock:^{
                    /*[FacebookHandler postWishId: [wish getId]
                                           name: [wish getName]
                                        caption: @""
                                           desc: [NSString stringWithFormat:@"Желание %@ исполнилось. %@", [wish getName], _textViewDesc.text]
                                          imageLink: [item getCoverExecutedOriginal]
                                        success:^(NSDictionary *results) {
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"FacebookPost"]}];
                                            
                                        } failed:^(NSString *error) {
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"FacebookPost"]}];
                                        } withQueue:myQueue];*/
                }];
                break;
            }
            case 2: {
                // post to tw
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    [TwitterHandler postTweetWithWishId:[wish getId]
                                                   text:[NSString stringWithFormat:@"Желание %@ исполнилось. %@", [wish getName], _textViewDesc.text]
                                                  image:_imageViewCover.image success:^{
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"TwitterPost"]}];
                                                  } canceled:^(NSString *string) {
                        
                                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"TwitterPost"]}];
                                                  }];
                });
                break;
            }
            case 3:
                // post to instagram
                [self trySendInstagramPost];
                break;
                
            default:
                break;
        }
        
    }];
}

- (BOOL)isInstagramSelected
{
    __block BOOL isIt = NO;
    
    [[_collectionViewSocialShare selectedSocial] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ( [obj integerValue] == 3 ) {
            isIt = YES;
        }
    }];
    
    return isIt;
}

- (void)trySendInstagramPost
{
    [InstagramHandler fileUrlToImage:_imageViewCover.image afterSave:^(NSURL *filePath) {
        if ( self.documentController == nil ) {
            self.documentController = [[InstagramDocumentInteractionController alloc] initWithUrl:filePath];
            self.documentController.delegate = (id<UIDocumentInteractionControllerDelegate>)self;
        } else {
            self.documentController.URL = filePath;
        }
        
        self.documentController.annotation = [NSDictionary dictionaryWithObject: [[ActionTexts sharedInstance] userName:@"Моё" executeWishName:[wish getName]] forKey:@"InstagramCaption"];
        
        [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView: self.view animated: YES ];
    }];
}

#pragma mark -
#pragma mark UIDocumentInteractionControllerDelegate
- (void)documentInteractionControllerDidDismissOpenInMenu:(UIDocumentInteractionController *)controller
{
    [self actionBackButton:nil];
}

- (IBAction)actionBackButton:(id)sender {
    [(MainNavigationController *)self.navigationController willEnableGestureBack:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setDetail:(ModelWish *)w
{
    wish = w;
}


#pragma mark -
#pragma mark SocialShareCollectionViewDelegate
- (void)showNotify:(NSInteger)type withText:text
{
    [_notification showNotify:type withText:text];
}

@end
