//
//  ExecuteWishViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 01/04/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SocialShareCollectionView.h"
#import "CCGrowingTextView.h"
#import "NotificationLabel.h"
#import "CameraButton.h"
#import "ModelWish.h"
#import "InstagramDocumentInteractionController.h"
#import "GAITrackedViewController.h"


@interface ExecuteWishViewController : GAITrackedViewController

- (IBAction)actionExecuteButton:(id)sender;
- (IBAction)actionBackButton:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView               *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView                *imageViewCover;
@property (weak, nonatomic) IBOutlet CameraButton               *buttonCamera;
@property (weak, nonatomic) IBOutlet CCGrowingTextView          *textViewDesc;
@property (weak, nonatomic) IBOutlet UILabel                    *labelSocialShare;
@property (weak, nonatomic) IBOutlet SocialShareCollectionView  *collectionViewSocialShare;
@property (weak, nonatomic) IBOutlet NotificationLabel          *notification;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint         *coverHeight;
@property (nonatomic, strong) InstagramDocumentInteractionController *documentController;

- (void)setDetail:(ModelWish *)cover;

@end
