//
//  CameraPhotoController.h
//  Startwish
//
//  Created by Pavel Makukha on 18/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol CameraDelegate;
@interface CameraPhotoController : UIViewController

@property (nonatomic, weak) id<CameraDelegate> cameraDelegate;

- (IBAction)actionCameraMode:(id)sender;
- (IBAction)actionTakePhoto:(id)sender;
- (IBAction)actionTakeVideo:(id)sender;
- (IBAction)actionFlash:(id)sender;
- (IBAction)actionFrontCamera:(id)sender;
- (IBAction)actionGrid:(id)sender;
- (IBAction)actionCloseCamera:(id)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionRemoveVideo:(id)sender;
- (IBAction)actionCameraAccess:(id)sender;
- (IBAction)actionLibrary:(id)sender;

@end

@protocol CameraDelegate <NSObject>
@required
- (CGSize)cropViewSize;
- (void)takePhoto:(UIImage *)photo;
@end
