//
//  CameraFiltersController.m
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CameraFiltersController.h"
#import "Routing.h"
#import "CameraFiltersCollectionView.h"
#import "UIImage+Helper.h"
#import "ImageFilterControl.h"
#import "ClearFilter.h"
#import "ActionTexts.h"
#import "CustomImagePicker.h"
#import "GPUImageView.h"


@interface CameraFiltersController ()

@property (nonatomic, strong) id backToCtrl;
@property (nonatomic, strong) ImageFilterControl *photo;
@property (nonatomic, strong) PhotoEditTool *currentTool;
@property (weak, nonatomic) IBOutlet GPUImageView *gpuPhotoView;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet CameraFiltersCollectionView *collectionTools;
@property (weak, nonatomic) IBOutlet UIButton *buttonNext;
@property (weak, nonatomic) IBOutlet UILabel *labelSlider;
@property (nonatomic, strong) UIAlertController *alertViewControllerCloseCamera;

@property (weak, nonatomic) IBOutlet UIView *scrollRotatedView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTrailingLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewConstraintVerticalTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;

@end


@implementation CameraFiltersController


#pragma mark - LifeCircle

- (void)viewDidLoad {
    [super viewDidLoad];
//    _photo = [[ImageFilterControl alloc] initWithImage:[[UIImage imageNamed:@"screen5"] fixOrientation]];
    _photo.gpuImageView = self.gpuPhotoView;
    [_photo loadActualProperties];
    
//    if ( _photoView.image.size.width > _photoView.image.size.height ) {
//        _imageViewConstraintVerticalTop.constant = _topViewHeightConstraint.constant / 2;
//        [_photoView setNeedsLayout];
//    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanFiltersMemory) name:@"STClearFilters" object:nil];
    [self renderView];
    [self renderUISlider];
    
    self.revealViewController.panGestureRecognizer.enabled = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [_photo loadActualProperties];
}

- (void)viewWillLayoutSubviews
{
    _photo.rv = self.scrollRotatedView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setDetail:(UIImage *)image finishCtrl:(id)ctrl
{
    _photo = [[ImageFilterControl alloc] initWithImage:image];
    _backToCtrl = ctrl;
}


#pragma mark - Action Buttons

- (IBAction)actionBack:(id)sender
{
    if ( self.currentTool ) {
        
        if ( [self.currentTool isKindOfClass:[PhotoEditToolFilter class]] ) {
            
            // If we want close ctrl
            if ( _slider.hidden ) {
                [self tryCloseFilterCtrl];
                return;
            }
            
            CGFloat v = [(PhotoEditToolFilter *)self.currentTool actualValue];
            ((PhotoEditToolFilter *)self.currentTool).value = v;
            [_photo setFilter:(PhotoEditToolFilter *)self.currentTool withPercent:v];
            
            [self renderUISlider];
            return;
            
        }
        
        [self updateUIForTool:nil];
        
        [_photo loadActualProperties];
        return;
    }
    
    [self tryCloseFilterCtrl];
}

- (IBAction)actionNext:(id)sender
{
    if ( _slider.hidden ) {
        UIImage *image = [_photo getRenderedImage];
        
        [CustomImagePicker saveImagetoStartwishAlbum:[image fixOrientation]];

        if ( !_backToCtrl ) {            
            [[Routing sharedInstance] showCreateWishControllerWithImage:image];
        } else {
            if ( [_backToCtrl respondsToSelector:@selector(setImageSelected:)] ) {
                [_backToCtrl setImageSelected:image];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STClearFilters" object:nil];
            [[Routing sharedInstance] backToCtrl:_backToCtrl];
        }
    } else {
        
        if ( self.currentTool ) {
            
            if ( [self.currentTool isKindOfClass:[PhotoEditToolFilter class]] ) {
                [(PhotoEditToolFilter *)self.currentTool saveValue];
                [_photo setFilter:(PhotoEditToolFilter *)self.currentTool withPercent:((PhotoEditToolFilter *)self.currentTool).value];
                
                [self renderUISlider];
                
                return;
            }
            
            [_photo saveProperties];
            [_photo loadActualProperties];
            
            [self updateUIForTool:nil];
        }
    }
}

- (void)tryCloseFilterCtrl
{
    if ( _photo.exposure != 0.0 || _photo.contrast != 1.0 || _photo.angle != 0.0 || ([self.currentTool isKindOfClass:[PhotoEditToolFilter class]] && ![self.currentTool isKindOfClass:[ClearFilter class]]) ) {
        [self presentViewController:self.alertViewControllerCloseCamera animated:YES completion:nil];
        return;
    }
    
    [self cleanFiltersMemory];
    [[Routing sharedInstance] closeCameraController];
}

- (void)cleanFiltersMemory
{
    _alertViewControllerCloseCamera = nil;
}


#pragma mark - Slide Change Position

- (void)changeSliderPositionByBaseFilter
{
    SEL setVariable = NSSelectorFromString( [NSString stringWithFormat:@"i_set%@:", self.currentTool.type] );
    
    [self updateUIThumbSlider];
    
    if ( ![_photo respondsToSelector:setVariable]) {
        NSLog(@"Unsupported filter type %@", self.currentTool.type);
        return;
    }
    
    #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [_photo performSelector:setVariable withObject:[NSNumber numberWithFloat:[_currentTool actualValueBySliderValue:_slider.value withMaxSliderValue:_slider.maximumValue]]];
    #pragma clang diagnostic pop
}

- (void)changeSliderPositionByFilter
{
    [self updateUIThumbSlider];
    [_photo setFilter:(PhotoEditToolFilter *)self.currentTool withPercent:_slider.value];
}

- (void)changeSliderPosition
{
    if ( [self.currentTool isKindOfClass:[PhotoEditToolBase class]] ) {
        [self changeSliderPositionByBaseFilter];
        return;
    }
    
    [self changeSliderPositionByFilter];
    
}

#pragma mark - CameraFiltersCollectionViewDelegate
- (void)selectTool:(PhotoEditTool *)tool
{
    
    if ( [tool isKindOfClass:[PhotoEditToolFilter class]] ) {
        if ( _currentTool != tool ) {
            self.currentTool = tool;
            [_photo setFilter:(PhotoEditToolFilter *)tool];
            [(PhotoEditToolFilter *)tool saveValue];
            return;
        }
        
        if ( [tool isKindOfClass:[ClearFilter class]] ) {
            return;
        }
    }
    
    [self updateUIForTool:tool];
}


#pragma mark - UI Logic
- (void)renderView
{
    _collectionTools.filtersDelegate = (id<CameraFiltersCollectionViewDelegate>)self;
    [self.slider addTarget:self action:@selector(changeSliderPosition) forControlEvents:UIControlEventValueChanged];
    
    self.alertViewControllerCloseCamera = [UIAlertController alertControllerWithTitle: [[ActionTexts sharedInstance] titleText:@"Attention"]
                                                                                   message: [[ActionTexts sharedInstance] text:@"AttentionChangePhotoMode"]
                                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self.alertViewControllerCloseCamera addAction:[UIAlertAction actionWithTitle:@"Да"
                                                                                 style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction *action) {
                                                                                   [self cleanFiltersMemory];
                                                                                   [[Routing sharedInstance] closeCameraController];
                                                                               }]];
    
    [self.alertViewControllerCloseCamera addAction:[UIAlertAction actionWithTitle:@"Нет"
                                                                                 style:UIAlertActionStyleCancel
                                                                               handler:nil]];
}

- (void)renderUISlider
{
    _collectionTools.hidden = NO;
    _slider.hidden = YES;
    _labelSlider.hidden = YES;
    [_buttonNext setImage:[[UIImage imageNamed:@"next"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
}

- (void)updateUIForTool:(PhotoEditTool *)tool
{
    CGFloat actualFilterValue = 0.f;
    
    self.currentTool = tool;
    
    if ( !tool ) {
        [self renderUISlider];
        return;
    }
    
    [_buttonNext setImage:[[UIImage imageNamed:@"done_in_circle_white"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:UIControlStateNormal];
    _collectionTools.hidden = YES;
    _labelSlider.hidden = NO;
    _slider.maximumValue = tool.sliderMaxValue;
    _slider.minimumValue = tool.sliderMinValue;
    _slider.hidden = NO;
    _slider.tintColor = tool.bgColor;
    _slider.maximumTrackTintColor = [UIColor blackColor];   // hack to correct render color bgColor
    _slider.maximumTrackTintColor = tool.bgColor;
    _labelSlider.textColor = tool.bgColor;
    [_slider setThumbImage: [UIImage makeRoundedImage:[UIImage imageNamed:@"slider_thumb_white2"] radius:0 withBorderColor:tool.bgColor width:2.0] forState:UIControlStateNormal];
    
    if ( [tool isKindOfClass:[PhotoEditToolBase class]] ) {
        actualFilterValue = [_photo actualPropertyValue:tool.type];
    }
    
    [_slider setValue: [_currentTool sliderValueFromActualValue:actualFilterValue withMaxSliderValue:_slider.maximumValue] animated:NO];
    
    [_slider setNeedsDisplay];
    [self changeSliderPosition];
}

- (void)updateUIThumbSlider
{
    CGRect trackRect = [_slider trackRectForBounds:_slider.bounds];
    CGRect thumbRect = [_slider thumbRectForBounds:_slider.bounds
                                         trackRect:trackRect
                                             value:_slider.value];
    
    _labelSlider.text = [NSString stringWithFormat:@"%d", (int)_slider.value];
    [_labelSlider sizeToFit];
    _constraintTrailingLabel.constant = thumbRect.origin.x + thumbRect.size.width/2 + _slider.frame.origin.x - _labelSlider.frame.size.width/2;
    [_labelSlider setNeedsLayout];
    
}

@end
