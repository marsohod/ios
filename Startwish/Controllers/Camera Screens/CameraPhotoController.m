//
//  CameraPhotoController.m
//  Startwish
//
//  Created by Pavel Makukha on 18/06/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "CameraPhotoController.h"
#import "CameraVM.h"
#import "PBJFocusView.h"
#import "Routing.h"
#import "ReactiveCocoa.h"
#import "ActionTexts.h"
#import "BackButton.h"
#import "UIImage+Helper.h"


@interface CameraPhotoController ()

@property (weak, nonatomic) IBOutlet UIView *cameraDenyView;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, strong) UIImageView *imageGrid;
@property (weak, nonatomic) IBOutlet UIButton *gridButton;
@property (weak, nonatomic) IBOutlet UIButton *frontCameraButton;
@property (weak, nonatomic) IBOutlet UIButton *flashButton;
@property (weak, nonatomic) IBOutlet BackButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *cameraLayer;
@property (nonatomic, strong) CameraVM *cameraViewModel;
@property (nonatomic, strong) PBJFocusView *focusView;
@property (weak, nonatomic) IBOutlet UIButton *shootButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *libraryButton;
@property (weak, nonatomic) IBOutlet UIButton *removeVideoButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressVideo;
@property (weak, nonatomic) IBOutlet UIButton *cameraAccessButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cameraViewTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cameraViewSpaceFromBottomSuperView;


@property (nonatomic, strong) UIImage *photoImage;

@property (nonatomic, strong) UIAlertController *alertViewControllerChangeModeCamera;
@property (nonatomic, strong) UIAlertController *alertViewControllerCloseCamera;

// Signals
@property (nonatomic, strong) RACSignal *capturedPhotoSignal;
@property (nonatomic, strong) RACSignal *capturingVideoDurationSignal;

@end

@implementation CameraPhotoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cameraViewModel = [[CameraVM alloc] init];
    [self setupViewModelSignals];
    [self setupCameraView];
    [self.cameraViewModel setupPhotoCamera];
    
    [self checkCameraAccess];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:[UIDevice currentDevice]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cleanController) name:@"STClearFilters" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self setupCameraView];
    
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        [self.cameraViewModel setupPhotoCamera];
        [self updateUIForCameraModePhoto];
    }
    else
    {
        [self.cameraViewModel setupVideoCamera];
        [self updateUIForCameraModeVideo];
    }
    
    self.revealViewController.panGestureRecognizer.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.revealViewController.panGestureRecognizer.enabled = YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super willTransitionToTraitCollection:newCollection withTransitionCoordinator:coordinator];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{

}

#pragma mark - Setup

- (void)setupViewModelSignals
{
    _capturedPhotoSignal = [self.cameraViewModel photoShootResultSignal];
    _capturingVideoDurationSignal = [self.cameraViewModel capturingVideoDurationSignal];
}

- (void)setupCameraView
{
    CGRect cameraFrame;
    
    self.view.backgroundColor = self.bottomView.backgroundColor;
    self.cameraLayer = [[PBJVision sharedInstance] previewLayer];
    self.cameraLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    self.cameraView.backgroundColor = [UIColor blackColor];
    
    if ( [self.cameraDelegate respondsToSelector:@selector(cropViewSize)] ) {
        CGFloat heightPadding = (self.cameraView.bounds.size.height - [self.cameraDelegate cropViewSize].height) / 2;
        cameraFrame = CGRectMake(0.f, heightPadding < _topView.frame.size.height ? _topView.frame.size.height : heightPadding, [self.cameraDelegate cropViewSize].width, [self.cameraDelegate cropViewSize].height);
        self.cameraLayer.frame = cameraFrame;
        [self.cameraViewModel setupPreviewFrame:cameraFrame];
    } else {
        cameraFrame = CGRectMake(0.f, 0.f, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - self.cameraViewSpaceFromBottomSuperView.constant);
        self.cameraLayer.frame = cameraFrame;
        [self.cameraViewModel setupPreviewFrame:cameraFrame];
    }

    [self.cameraView.layer addSublayer:self.cameraLayer];
    
    self.imageGrid = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, cameraFrame.size.width, cameraFrame.size.height)];

    [self.imageGrid setImage:[[UIImage imageNamed:@"grid"] scaleToSize:cameraFrame.size]];
    
    [self.cameraLayer addSublayer:self.imageGrid.layer];
    self.imageGrid.alpha = 0.f;
    
    self.focusView = [[PBJFocusView alloc] initWithFrame:CGRectZero];
    [self.cameraView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(focusIn:)]];

    [_capturingVideoDurationSignal subscribeNext:^(NSNumber *seconds) {
        [self.progressVideo setProgress:seconds.floatValue/10/2 animated:YES];
        
        if (seconds.floatValue > 0.f)
        {
            self.libraryButton.hidden = YES;
            self.removeVideoButton.hidden = NO;
        }
        if (seconds.floatValue > 4.f)
        {
            self.nextButton.enabled = YES;
        }
        else
        {
            self.nextButton.enabled = NO;
        }
        if (seconds.floatValue >= 20.f)
        {
            self.shootButton.enabled = NO;
        }
    }];

    [_capturedPhotoSignal subscribeNext:^(UIImage *photo) {
        if ( !photo || photo == _photoImage) {
            return;
        }
        
        _photoImage = photo;
        
        if ( [self.cameraDelegate respondsToSelector:@selector(takePhoto:)] ) {
            [self.cameraDelegate takePhoto:photo];
            return;
        }
        
        [[Routing sharedInstance] showCameraFiltersController:photo];
    }];
    
    self.alertViewControllerChangeModeCamera = [UIAlertController alertControllerWithTitle: [[ActionTexts sharedInstance] titleText:@"Attention"]
                                                                                   message: [[ActionTexts sharedInstance] text:@"AttentionChangePhotoMode"]
                                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self.alertViewControllerChangeModeCamera addAction:[UIAlertAction actionWithTitle:@"Да"
                                                                                 style:UIAlertActionStyleDefault
                                                                               handler:^(UIAlertAction *action) {
                                                                                   [self updateUIForCameraModePhoto];
                                                                                   [self changeCameraModeAnimation];
                                                                               }]];
    
    [self.alertViewControllerChangeModeCamera addAction:[UIAlertAction actionWithTitle:@"Нет"
                                                                                 style:UIAlertActionStyleCancel
                                                                               handler:nil]];
    
    self.alertViewControllerCloseCamera = [UIAlertController alertControllerWithTitle: [[ActionTexts sharedInstance] titleText:@"Attention"]
                                                                              message: [[ActionTexts sharedInstance] text:@"AttentionCloseCamera"]
                                                                       preferredStyle: UIAlertControllerStyleAlert];
    
    [self.alertViewControllerCloseCamera addAction:[UIAlertAction actionWithTitle:@"Да"
                                                                            style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction *action) {
                                                                              [self cleanController];
                                                                              [[Routing sharedInstance] closeCameraController];
                                                                          }]];
    
    [self.alertViewControllerCloseCamera addAction:[UIAlertAction actionWithTitle:@"Нет"
                                                                            style:UIAlertActionStyleCancel
                                                                          handler:nil]];
     
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(focusEnd) name:@"STCameraFocused" object:nil];
}

- (void)cleanController
{
    [self.cameraViewModel disableCamera];
    [self.imageGrid.layer removeFromSuperlayer];
    self.cameraLayer = nil;
    self.cameraView.layer.sublayers = nil;
    self.alertViewControllerCloseCamera = nil;
    self.alertViewControllerChangeModeCamera = nil;
    self.cameraViewModel = nil;
}

#pragma mark - Camera logic

- (void)enableGrid
{
    if (!self.imageGrid.alpha)
    {
        [UIView animateWithDuration:0.1f animations:^{
            self.imageGrid.alpha = 0.5f;
        } completion:^(BOOL finished) {
            self.imageGrid.alpha = 1.0f;
        }];
    }
    else
    {
        [UIView animateWithDuration:0.1f animations:^{
            self.imageGrid.alpha = 0.5f;
        } completion:^(BOOL finished) {
            self.imageGrid.alpha = 0.0f;
        }];
    }
}


- (IBAction)actionCameraMode:(id)sender
{
    if (self.cameraViewModel.vision.cameraMode == PBJCameraModePhoto)
    {
        [self updateUIForCameraModeVideo];
        [self changeCameraModeAnimation];
        [self checkMicrophoneAccess];
    }
    else
    {
        if (self.progressVideo.progress > 0.f)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:self.alertViewControllerChangeModeCamera animated:YES completion:nil];
            });
        }
        else
        {
            [self updateUIForCameraModePhoto];
            [self changeCameraModeAnimation];
        }
    }
}

- (void)removeCapturedVideoAndStartNew
{
    self.cameraViewModel.isVideoRemoved = YES;
    [self.cameraViewModel endVideoCapturing];
    self.progressVideo.progress = 0.0;
    self.shootButton.enabled = YES;
    self.nextButton.enabled = NO;
    self.removeVideoButton.hidden = YES;
    self.libraryButton.hidden = NO;
}

- (IBAction)actionTakePhoto:(id)sender
{
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        [self.cameraViewModel takePhoto];
    }
    else
    {
        [self.cameraViewModel pauseVideoCapturing];
    }
}

- (IBAction)actionTakeVideo:(id)sender
{
    if (!self.cameraViewModel.isPhotoModeCamera)
    {
        [self.cameraViewModel startVideoCapturing];
        
        if (self.progressVideo.progress > 0)
        {
            [self.cameraViewModel resumeVideoCapturing];
        }
    }
}

- (IBAction)actionFlash:(id)sender
{
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        [self.cameraViewModel enableFlash];
        [self changeFlashButton];
    }
    else
    {
        [self.cameraViewModel changeCameraDevice];
    }
}

- (IBAction)actionFrontCamera:(id)sender
{
    [self.cameraViewModel changeCameraDevice];
}

- (IBAction)actionGrid:(id)sender
{
    [self enableGrid];
}

- (IBAction)actionCloseCamera:(id)sender
{
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        [self cleanController];
        [[Routing sharedInstance] closeCameraController];
    }
    else
    {
        if (self.progressVideo.progress > 0.f)
        {
            [self presentViewController:self.alertViewControllerCloseCamera animated:YES completion:nil];
        }
        else
        {
            [self cleanController];
            [[Routing sharedInstance] closeCameraController];
        }
    }
}

- (IBAction)actionNext:(id)sender
{
}

- (IBAction)actionRemoveVideo:(id)sender
{
    [self removeCapturedVideoAndStartNew];
}

- (IBAction)actionLibrary:(id)sender
{
    __weak id s = self;
    CGSize size = CGSizeZero;
    
    if ( [self.cameraDelegate respondsToSelector:@selector(cropViewSize)] ) {
        size = [self.cameraDelegate cropViewSize];
    }
    
    [[Routing sharedInstance] openPhotoLibraryControllerWithDelegate:s withCropSize:size];
}

- (IBAction)actionCameraAccess:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

const NSInteger tag = 22233;

- (IBAction)actionMicrophoneAccess:(id)sender
{
//    BOOL canOpenSettings = (&UIApplicationOpenSettingsURLString != NULL);
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: [[ActionTexts sharedInstance] titleText:@"AccessMicrophone"]
                          message: [[ActionTexts sharedInstance] text:@"AccessMicrophoneDeny"]
                          delegate:self
                          cancelButtonTitle: @"OK"
                          otherButtonTitles: [[ActionTexts sharedInstance] titleText:@"GoTo"], nil];
    alert.tag = tag;
    
    [alert show];
}

- (IBAction)focusIn:(UIGestureRecognizer *)gestureRecognizer
{
    CGPoint tapPoint = [gestureRecognizer locationInView:_cameraView];
    
    // auto focus is occuring, display focus view
    [_focusView setFrame:[self.cameraViewModel convertFocusPoint:tapPoint toFrame:_focusView.frame]];
    
    [_cameraView addSubview:_focusView];
    [_focusView startAnimation];
    
    [self.cameraViewModel focusToPoint:tapPoint inFrame:_cameraView.frame];
}

- (void)focusEnd
{
    if (_focusView && [_focusView superview]) {
        [_focusView stopAnimation];
    }
}



#pragma mark - UI logic

- (void)changeCameraModeAnimation
{
    CATransition *animation = [CATransition animation];
    animation.duration = 1.0f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = @"cameraIrisHollowOpen";
    
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        animation.subtype = kCATransitionFromRight;
    }
    else
    {
        animation.subtype = kCATransitionFromLeft;
    }
    
    [self.cameraLayer addAnimation:animation forKey:nil];
    
    CATransition *animationBottomView = [CATransition animation];
    animationBottomView.duration = 1.0f;
    animationBottomView.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animationBottomView.type = kCATransitionPush;
    
    if (self.cameraViewModel.isPhotoModeCamera)
    {
        animationBottomView.subtype = kCATransitionFromLeft;
    }
    else
    {
        animationBottomView.subtype = kCATransitionFromRight;
    }
    [self.bottomView.layer addAnimation:animationBottomView forKey:nil];
}

- (void)updateUIForCameraModeVideo
{
    self.nextButton.hidden = NO;
    self.nextButton.enabled = NO;
    self.removeVideoButton.hidden = YES;
    self.libraryButton.hidden = NO;
    self.frontCameraButton.hidden = YES;
    self.gridButton.hidden = YES;
    self.progressVideo.hidden = NO;
    self.imageGrid.alpha = 0.f;
    
    self.bottomView.backgroundColor = [UIColor colorWithRed:26/255
                                                      green:26/255
                                                       blue:26/255
                                                      alpha:0.3];
    self.cameraLayer.frame = self.view.bounds;
    
    [self.flashButton setImage:[UIImage imageNamed:@"change_camera_white"] forState:UIControlStateNormal];
    [self.shootButton setImage:[UIImage imageNamed:@"shoot_red"] forState:UIControlStateNormal];
    [self.videoButton setImage:[UIImage imageNamed:@"camera_white"] forState:UIControlStateNormal];
    
    [self.cameraViewModel setupVideoCamera];
}

- (void)updateUIForCameraModePhoto
{
    self.libraryButton.hidden = NO;
    self.removeVideoButton.hidden = YES;
    self.cameraViewModel.isVideoRemoved = YES;
    [self.cameraViewModel endVideoCapturing];
    self.nextButton.hidden = YES;
    self.frontCameraButton.hidden = NO;
    self.gridButton.hidden = NO;
    self.progressVideo.progress = 0.f;
    self.progressVideo.hidden = YES;
    
    self.bottomView.backgroundColor = [UIColor colorWithRed:26/255
                                                      green:26/255
                                                       blue:26/255
                                                      alpha:0.f];
    
    self.cameraLayer.frame = self.cameraView.bounds;
    self.cameraView.preservesSuperviewLayoutMargins = NO;
    
    [self.flashButton setImage:[UIImage imageNamed:@"flash_white"] forState:UIControlStateNormal];
    [self.shootButton setImage:[UIImage imageNamed:@"shoot_green"] forState:UIControlStateNormal];
    [self.videoButton setImage:[UIImage imageNamed:@"video_white"] forState:UIControlStateNormal];
    
    [self.cameraViewModel setupPhotoCamera];
}

- (void)changeFlashButton
{
    if ( self.cameraViewModel.isFlashAuto ) {
        [self.flashButton setImage:[UIImage imageNamed:@"flash_auto_white"] forState:UIControlStateNormal];
        return;
    }
    
    if ( self.cameraViewModel.isFlashOn ) {
        [self.flashButton setImage:[UIImage imageNamed:@"flash_on_white"] forState:UIControlStateNormal];
        return;
    }
    
    [self.flashButton setImage:[UIImage imageNamed:@"flash_white"] forState:UIControlStateNormal];
}

#pragma mark - Device logic

- (void)orientationChanged:(NSNotification *)note
{
    UIDevice *device = note.object;
    CGFloat degree = 0.f;
    
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            degree = 0.f;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            degree = M_PI;
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            degree = M_PI_2;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            degree = -M_PI_2;
            break;
            
        default:
            break;
    };
    
    [UIView animateWithDuration:0.3f animations:^{
        self.flashButton.transform = CGAffineTransformMakeRotation(degree);
        self.frontCameraButton.transform = CGAffineTransformMakeRotation(degree);
        self.gridButton.transform = CGAffineTransformMakeRotation(degree);
        self.backButton.transform = CGAffineTransformMakeRotation(degree);
        self.nextButton.transform = CGAffineTransformMakeRotation(degree);
        self.videoButton.transform = CGAffineTransformMakeRotation(degree);
    }];
    
    [self.cameraViewModel changeOrientationCamera:device.orientation];
}

- (void)checkCameraAccess
{
    if ( [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined ) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted){
            _cameraDenyView.hidden = granted;
        }];
        return;
    }
    
    if ( [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] != AVAuthorizationStatusAuthorized ) {
        _cameraDenyView.hidden = NO;
    }
}

- (void)checkMicrophoneAccess
{
    if ( [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio] == AVAuthorizationStatusNotDetermined ) {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted){
            if ( !granted ) {
                [self actionMicrophoneAccess:nil];
            }
        }];
        return;
    }
    
    if ( [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeAudio] != AVAuthorizationStatusAuthorized ) {
        [self actionMicrophoneAccess:nil];
    }
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == tag && buttonIndex == 1) {
//        if ( &UIApplicationOpenSettingsURLString != NULL )
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}


#pragma mark - IPDelegate

- (void)setImageSelected:(UIImage *)image
{    
    if ( [self.cameraDelegate respondsToSelector:@selector(takePhoto:)] ) {
        [self.cameraDelegate takePhoto:image];
        return;
    }
    
    [[Routing sharedInstance] showCameraFiltersController: image];
}

@end
