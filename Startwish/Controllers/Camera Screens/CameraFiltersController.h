//
//  CameraFiltersController.h
//  Startwish
//
//  Created by Pavel Makukha on 13/07/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface CameraFiltersController : UIViewController

- (void)setDetail:(UIImage *)image finishCtrl:(id)ctrl;
- (IBAction)actionBack:(id)sender;
- (IBAction)actionNext:(id)sender;

@end
