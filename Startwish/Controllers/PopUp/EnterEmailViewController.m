//
//  EnterEmailViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 12/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "EnterEmailViewController.h"
#import "NSString+Helper.h"
#import "ActionTexts.h"
#import "LoginAPI.h"


@interface EnterEmailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet UIView *popupView;

- (IBAction)actionButtonBack:(id)sender;
- (IBAction)actionButtonSend:(id)sender;
- (IBAction)actionButtonSendEndTyping:(id)sender;

@end


@implementation EnterEmailViewController

- (instancetype)init
{
    self = [super initWithNibName:@"EnterEmail" bundle:nil];
    
    if ( self ) {
        self.screenName = @"Enter Email after VK auth";
    }
    
    return self;
}

+ (EnterEmailViewController *)sharedInstance
{
    static EnterEmailViewController *ctrl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ctrl = [[EnterEmailViewController alloc] init];
    });
    
    return ctrl;
}




- (void)viewDidLoad {
    [super viewDidLoad];
    self.popupView.layer.cornerRadius = 5.f;
    self.popupView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)actionButtonBack:(id)sender
{
    [self.delegate cancelPopUp];
    [self removeAnimate];
}

- (IBAction)actionButtonSend:(id)sender
{
    if ( [NSString h_isEmailString:_textFieldEmail.text] ) {
        [self.delegate successPopUp:_textFieldEmail.text];
        return;
    }
    
    [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailBad"]];
}

- (IBAction)actionButtonSendEndTyping:(id)sender
{
    [self actionButtonSend:nil];
}
@end
