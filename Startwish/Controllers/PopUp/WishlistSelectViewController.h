//
//  WishlistSelectViewController.h
//  Startwish
//
//  Created by marsohod on 11/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PopUpViewController.h"


@protocol PUViewProtocol;
@interface WishlistSelectViewController : PopUpViewController

+ (WishlistSelectViewController *)sharedInstance;

@property (nonatomic) uint32_t hideWishlistId;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIButton *buttonSuccess;
@property (weak, nonatomic) IBOutlet UIButton *buttonDismiss;
@property (weak, nonatomic) IBOutlet UIButton *buttonAdd;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerViewWishlists;

@property (nonatomic, weak) id<PUViewProtocol> delegate;

- (IBAction)actionDismiss:(id)sender;
- (IBAction)actionSuccess:(id)sender;
- (IBAction)actionAdd:(id)sender;

@end

@protocol PUViewProtocol <NSObject>
@required
- (void)successPopUp:(uint32_t)wishlistId;

@end
