//
//  WishlistSelectViewController.m
//  Startwish
//
//  Created by marsohod on 11/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "WishlistSelectViewController.h"
#import "UIFont+Helper.h"
#import "WishlistsAPI.h"
#import "ModelWishlist.h"
#import "Routing.h"
#import "Me.h"


@interface WishlistSelectViewController ()
{
    NSMutableArray *userWishlists;
}
@end


@implementation WishlistSelectViewController

- (instancetype)init
{
    self = [super initWithNibName:@"WishlistSelect" bundle:nil];
    
    if ( self ) {
        self.screenName = @"Wishlist Select Popup Screen";
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshWishlists) name:@"STRefreshWishlists" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeWishlists:) name:@"STWishlistDelete" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectLast) name:@"STSelectLast" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectFirst) name:@"STSelectFirst" object:nil];
    }
    
    [self loadWishlists];
    
    return self;
}

+ (WishlistSelectViewController *)sharedInstance
{
    static WishlistSelectViewController *ctrl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ctrl = [[WishlistSelectViewController alloc] init];
    });
    
    return ctrl;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshWishlists];
    [self hideWishlistIfNeed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)actionDismiss:(id)sender;
{
    [self removeAnimate];
    _hideWishlistId = 0;
}

- (IBAction)actionSuccess:(id)sender
{
    [self removeAnimate];
    [self.delegate successPopUp:[userWishlists[[_pickerViewWishlists selectedRowInComponent:0]] getId]];
    _hideWishlistId = 0;
}

- (IBAction)actionAdd:(id)sender
{
    [[Routing sharedInstance] showCreateWishlistControllerWithWishlist:nil];
}


- (void)loadWishlists
{
    [[WishlistsAPI sharedInstance] getMyAllWishlistsWithCallBack:^(NSArray *items) {
        // TODO:
        // uncomment
//        [Me setMyWishlist:items];
        userWishlists = [[NSMutableArray alloc] initWithArray:items];
        [_pickerViewWishlists reloadAllComponents];
    }];
}

- (void)refreshWishlists
{
    if ( [[Me myWishlists] count] != 0 ) {
        userWishlists = [[NSMutableArray alloc] initWithArray:[Me myWishlists]];
        [_pickerViewWishlists reloadAllComponents];
    } else {
        [self loadWishlists];
    }
}

- (void)removeWishlists:(NSNotification *)notif
{
    if ( [[Me myWishlists] count] != 0 ) {
        [userWishlists enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ( [notif.userInfo[@"wishlistId"] integerValue] == [obj getId] ) {
                [userWishlists removeObject:obj];
                *stop = YES;
            }
        }];
        
        [Me setMyWishlist:userWishlists];
        
        [_pickerViewWishlists reloadAllComponents];
    }
}

- (void)selectLast
{
    [_pickerViewWishlists selectRow:[userWishlists count] - 1 inComponent:0 animated:YES];
}

- (void)selectFirst
{
    [_pickerViewWishlists selectRow:0 inComponent:0 animated:YES];
}

- (void)hideWishlistIfNeed
{
    [userWishlists enumerateObjectsUsingBlock:^(ModelWishlist *obj, NSUInteger idx, BOOL *stop) {
        if ( [obj getId] == _hideWishlistId ) {
            [userWishlists removeObject:obj];
        }
    }];
    
    [_pickerViewWishlists reloadAllComponents];
}

#pragma mark -
#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)componentIndex
{
    return [userWishlists count];
}


#pragma mark -
#pragma mark - UIPickerViewDelegate

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)componentIndex {
    
    return [UIScreen mainScreen].bounds.size.width - 60;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)componentIndex reusingView:(UIView *)view
{
    UILabel *label;
    
    if ([view isKindOfClass:[UILabel class]]) {
        label = (UILabel *) view;
    }
    else {
        label = [[UILabel alloc] init];
        [label setFont: [UIFont h_defaultFontSize:15.0]];
        [label setUserInteractionEnabled: YES];
        [label setBackgroundColor: [UIColor clearColor]];
    }

    [label setText: [userWishlists[row] getName]];
    
    return label;
}


@end
