//
//  EnterEmailViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 12/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PopUpViewController.h"
#import "NotificationLabel.h"


@protocol PUViewEnterProtocol;
@interface EnterEmailViewController : PopUpViewController

+ (EnterEmailViewController *)sharedInstance;

@property (nonatomic, weak) id<PUViewEnterProtocol> delegate;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

@end

@protocol PUViewEnterProtocol <NSObject>
@required
- (void)successPopUp:(NSString *)email;
- (void)cancelPopUp;

@end
