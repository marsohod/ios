//
//  PopUpViewController.h
//  Startwish
//
//  Created by marsohod on 11/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "GAITrackedViewController.h"


@interface PopUpViewController : GAITrackedViewController

- (void)showInView:(UIView *)aView animated:(BOOL)animated;
- (void)removeAnimate;
- (BOOL)isShowing;

@end
