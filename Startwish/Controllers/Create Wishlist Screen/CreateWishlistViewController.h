//
//  CreateWishlistViewController.h
//  Startwish
//
//  Created by marsohod on 19/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "ModelWishlist.h"
#import "NotificationLabel.h"
#import "OrangeMiddleButton.h"
#import "CCGrowingTextView.h"
#import "GAITrackedViewController.h"


@interface CreateWishlistViewController : GAITrackedViewController
{
    ModelWishlist   *wishlist;
    BOOL            isLoading;
}
@property (weak, nonatomic) IBOutlet UILabel            *labelTitleCtrl;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldName;
@property (weak, nonatomic) IBOutlet CCGrowingTextView  *textViewDesc;
@property (weak, nonatomic) IBOutlet UILabel            *labelPrivateSegmentControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlPrivate;
@property (weak, nonatomic) IBOutlet NotificationLabel  *notification;
@property (weak, nonatomic) IBOutlet OrangeMiddleButton *buttonCreate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTitleAlignment;

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionCreateButton:(id)sender;
- (void)setDetail:(id)item;

@end
