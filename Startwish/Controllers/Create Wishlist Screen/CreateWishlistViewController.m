//
//  CreateWishlistViewController.m
//  Startwish
//
//  Created by marsohod on 19/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import "CreateWishlistViewController.h"
#import "UISegmentedControl+CustomTintColor.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "UIImage+Helper.h"
#import "WishlistsAPI.h"
#import "ActionTexts.h"
#import "MBProgressHUD.h"
#import "Me.h"


@implementation CreateWishlistViewController

#define kTagFirst 111
#define kTagSecond 112
#define kTagThird 113
#define SegmentFontSize 16.0
#define TopPaddingTextSegment 15.0


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self renderLabelTitle];
    [self renderTextFields];
    [self renderLabelSegmentControl];
    [self renderPrivateSegmentControl];
    [self renderButtonCreate];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endTyping)]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Create Wishlist Screen";
}


#pragma mark -
#pragma mark Render

- (void)renderLabelTitle
{
    [_labelTitleCtrl setTextColor: [UIColor h_smoothGreen]];
    [_labelTitleCtrl setFont: [UIFont h_defaultFontSize:16.0]];
    [_labelTitleCtrl setText: [_labelTitleCtrl.text uppercaseString]];
    [_labelTitleCtrl setText:(wishlist ? [[ActionTexts sharedInstance] titleText:@"WishlistEdit"] : [[ActionTexts sharedInstance] titleText:@"WishlistCreate"])];
    [_labelTitleCtrl setText:[_labelTitleCtrl.text uppercaseString]];
    _labelTitleAlignment.constant = wishlist ? -15 : 0;
    [_labelTitleCtrl setNeedsLayout];
}

- (void)renderTextFields
{
    [_textFieldName setColorDefault: @"black"];
    [_textFieldName setColorHighlight: @"black"];
    [_textFieldName setIcon:@"wishlist_name" toTheRightDirect:NO withMaxIconWidth:35.0 paddingTop:-10.0 paddingLeft:0.0];
    [_textFieldName setTextColor:[UIColor blackColor]];
    [_textFieldName setTintColor:[UIColor blackColor]];
    
    _textViewDesc.delegate = (id<UITextViewDelegate>)self;
    [_textViewDesc setPlaceholderColor:[UIColor blackColor]];
    [_textViewDesc setPlaceholder:@"описание"];
    
    [_textViewDesc.constraints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_textViewDesc removeConstraint:obj];
    }];
    
    if ([_textViewDesc respondsToSelector:@selector(textContainerInset)])
        _textViewDesc.textContainerInset = UIEdgeInsetsMake(5, 29, 4, 5);
    
    [_textViewDesc setFont:[UIFont h_defaultFontLightSize: 21.0]];
    _textViewDesc.maxNumberOfLine = 4;
    _textViewDesc.delegate = (id<UITextViewDelegate>)self;
    
    [_textFieldName addTarget:self action:@selector(endTyping) forControlEvents:UIControlEventEditingDidEnd];
    
    if ( wishlist ) {
        _textFieldName.text = [wishlist getName];
        _textViewDesc.text = [wishlist getDescription];
//        _textFieldDesc.text = [wishlist getDescription];
    }
}

- (void)renderLabelSegmentControl
{
    [_labelPrivateSegmentControl setFont: [UIFont h_defaultFontMediumSize: 8.0]];
    [_labelPrivateSegmentControl setTextColor: [UIColor h_gray]];
    [_labelPrivateSegmentControl setText: [_labelPrivateSegmentControl.text uppercaseString]];
}

- (void)renderButtonCreate
{
    [_buttonCreate setTitle:(wishlist ? [[ActionTexts sharedInstance] titleText:@"Save"] : [[ActionTexts sharedInstance] titleText:@"Create"]) forState:UIControlStateNormal];
}

- (void)endTyping
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark Segment Control
- (void)renderPrivateSegmentControl
{
    UIImage *imageSelected = [UIImage imageNamed:@"segment_bg_red.png"];
    UIImage *imageDefault = [UIImage imageNamed:@"transparent.png"];
    
    // Настраиваем неактивный сегмент
    [_segmentControlPrivate setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor h_orange2], NSFontAttributeName: [UIFont h_defaultFontSize:SegmentFontSize]} forState:UIControlStateNormal];
    
    // Настраиваем активный сегмент
    [_segmentControlPrivate setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont h_defaultFontBoldSize:SegmentFontSize]} forState:UIControlStateSelected];
    
    // Ставим фон у неактивного сегмента
    [_segmentControlPrivate setBackgroundImage: [imageDefault scaleToSize:CGSizeMake(_segmentControlPrivate.frame.size.width / 3, _segmentControlPrivate.frame.size.height)]
                               forState:UIControlStateNormal
                             barMetrics:UIBarMetricsDefault];
    
    // Ставим фон у активного сегмента
    [_segmentControlPrivate setBackgroundImage: [imageSelected scaleToSize:CGSizeMake(_segmentControlPrivate.frame.size.width / 3, _segmentControlPrivate.frame.size.height)]
                               forState:UIControlStateSelected
                             barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между активным и неактивным сегментом
    [_segmentControlPrivate setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, _segmentControlPrivate.frame.size.height)] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    // Ставим разделитель между неактивным и активным сегментом
    [_segmentControlPrivate setDividerImage:[imageSelected scaleToSize:CGSizeMake(1.0, _segmentControlPrivate.frame.size.height)] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    // Ставим теги для каждого сегмента, чтобы можно было расскрасить картинки для них
    [_segmentControlPrivate setTag:kTagFirst forSegmentAtIndex:0];
    [_segmentControlPrivate setTag:kTagSecond forSegmentAtIndex:1];
    [_segmentControlPrivate setTag:kTagThird forSegmentAtIndex:2];

    [_segmentControlPrivate setContentOffset:CGSizeMake( 0, -TopPaddingTextSegment ) forSegmentAtIndex:0];
    
    _segmentControlPrivate.layer.cornerRadius = 5.0;
    _segmentControlPrivate.clipsToBounds = YES;
    _segmentControlPrivate.layer.borderWidth = 1.0;
    _segmentControlPrivate.layer.borderColor = [UIColor h_orange2].CGColor;
    
    [_segmentControlPrivate addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    
    if ( wishlist ) {
        [_segmentControlPrivate setSelectedSegmentIndex:[wishlist getPrivacy]];
    }
    
    [self customSetTitleToSegment];
}

- (void)customSetTitleToSegment
{
    UIFont *TagThirdFont = [UIFont h_defaultFontSize:SegmentFontSize];
    UIFont *TagSecondFont = [UIFont h_defaultFontSize:SegmentFontSize];
    
    switch ( [_segmentControlPrivate selectedSegmentIndex] ) {
        case 2:
            TagThirdFont = [UIFont h_defaultFontBoldSize:SegmentFontSize];
            break;
        
        case 1:
            TagSecondFont = [UIFont h_defaultFontBoldSize:SegmentFontSize];
            break;
            
        default:
            break;
    }
    
    // рисуем текст в картинке
    [_segmentControlPrivate setImage: [UIImage imageFromImage:[UIImage imageNamed:@"lock_orange.png"] string:@"Мне" color:[UIColor whiteColor] font:TagThirdFont position: CGPointMake(0,3)] forSegmentAtIndex:2];
    [_segmentControlPrivate setImage: [UIImage imageFromImage:[UIImage imageNamed:@"smile_orange.png"] string:@"Друзьям" color:[UIColor whiteColor] font:TagSecondFont position: CGPointMake(0,3)] forSegmentAtIndex:1];
    
    // ставим подцветку текста
    [_segmentControlPrivate setTintColor:[UIColor h_orange2] forTag:kTagFirst];
    [_segmentControlPrivate setTintColor:[UIColor h_orange2] forTag:kTagSecond];
    [_segmentControlPrivate setTintColor:[UIColor h_orange2] forTag:kTagThird];
}

- (void)segmentChanged:(id)sender {
    [self customSetTitleToSegment];
    [self endTyping];
}


#pragma mark -
#pragma mark Action Buttons

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionCreateButton:(id)sender
{
    if ( isLoading || ![self isValidData]) { return; }
    
    isLoading = YES;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ( !wishlist ) {
        [[WishlistsAPI sharedInstance] createWishlist:_textFieldName.text
                                                 desc:_textViewDesc.text
                                         privateLevel:[_segmentControlPrivate selectedSegmentIndex]
                                         withCallBack:^(id item) {
                                             if ( [item isMyWishlist] ) {
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishlistAdd" object:nil userInfo:@{@"increase" : [NSNumber numberWithBool:YES], @"wishlist": item}];
                                             }
                                             
                                             isLoading = NO;
                                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                                             [self updateWishlistsList:(ModelWishlist *)item replace:NO];

                                             
                                             [self actionBackButton:nil];
                                         } failedBlock:^(id item) {
                                             isLoading = NO;
                                             [MBProgressHUD hideHUDForView:self.view animated:YES];
                                             [_notification showNotify:0 withText:[NSString stringWithFormat:@"%@", item]];
                                         }];
    } else {
        [[WishlistsAPI sharedInstance] editWishlistById:[wishlist getId]
                                                   name:_textFieldName.text
                                                   desc:_textViewDesc.text
                                           privateLevel:[_segmentControlPrivate selectedSegmentIndex]
                                           withCallBack:^(id item) {
                                               isLoading = NO;
                                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                                               [self updateWishlistsList:(ModelWishlist *)item replace:YES];

                                               [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishlistUpdated" object:nil userInfo:@{@"wishlist": item}];
                                               
                                               [self actionBackButton:nil];
//            [self.navigationController popViewControllerAnimated:YES];
                                               
        } failedBlock:^(id item) {
            isLoading = NO;
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [_notification showNotify:0 withText:[NSString stringWithFormat:@"%@", item]];
        }];
    }
}

- (void)updateWishlistsList:(ModelWishlist *)item replace:(BOOL)replace
{
    NSMutableArray *list = [[NSMutableArray alloc] initWithArray:[Me myWishlists]];
    
    if ( !replace ) {
        [list insertObject:item atIndex:0];
        
        [Me setMyWishlist:[NSArray arrayWithArray:list]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STRefreshWishlists" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STSelectFirst" object:nil];
    } else {
        [list enumerateObjectsUsingBlock:^(ModelWishlist *obj, NSUInteger idx, BOOL *stop) {
            if ( [obj getId] == [item getId] ) {
                list[ idx ] = item;
                *stop = YES;
            }
        }];
        
        [Me setMyWishlist:[NSArray arrayWithArray:list]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STRefreshWishlists" object:nil];
    }
}

- (BOOL)isValidData
{
    if ( [[_textFieldName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""] ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"WishlistEmptyName"]];
        [_textFieldName becomeFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)setDetail:(id)item
{
    wishlist = (ModelWishlist *)item;
}


#pragma mark -
#pragma mark UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView
{
    [_textViewDesc setText:[_textViewDesc.text stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]];
}

@end
