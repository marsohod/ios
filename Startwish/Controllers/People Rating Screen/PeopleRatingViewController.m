//
//  PeopleRatingViewController.m
//  Startwish
//
//  Created by marsohod on 22/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "PeopleRatingViewController.h"
#import "RevealViewController.h"
#import "ContextMenuPeopleRating.h"
#import "UserAPI.h"
#import "ModelUser.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "Routing.h"
#import "Circle.h"


#define CONTENT_WIDTH 1000.0
#define CONTENT_HEIGHT 1000.0
#define WIDTH_BETWEEN_CIRCLES 6.0


@interface PeopleRatingViewController ()
{
    UITapGestureRecognizer *tap;
    NSTimer         *timer;
    BOOL            isRequestFromMenu;
    BOOL            isNotAvailable;
    BOOL            isLoading;
    BOOL            isMovingCircles;
    NSMutableArray* addedLayers;
    NSString*       lastSearch;
    NSUInteger      page;
    NSInteger       lastInserSquare;  // 1,2,3,4 - как четверти на оси коориднат. Относильно центральной авы
    /*
     Четверти
      ___
     (3|4)
     (2|1)
      ¯¯¯
     */
}
@end

@implementation PeopleRatingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    lastSearch = @"";
    page = 0;
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(circleTap:)];
    addedLayers = [[NSMutableArray alloc] init];
    
    [self.view setBackgroundColor:[UIColor blackColor]];
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [_scrollView setBackgroundColor:[UIColor blackColor]];
    [_scrollView setContentSize: CGSizeMake(CONTENT_WIDTH, CONTENT_HEIGHT)];
    [_scrollView addGestureRecognizer:tap];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    
    _longTapItem = [[NSMutableDictionary alloc] initWithDictionary: @{@"image"          : @"group_white",
                                                                      @"color"          : [UIColor h_smoothGreen],
                                                                      @"title"          : @"Кристина\rБарселона",
                                                                      @"rating"         : @"0",
                                                                      @"titleSelected"  : @"",
                                                                      @"touchedIndex"   : [NSNumber numberWithInteger:999]}];
//    _buttonBack.hidden = isRequestFromMenu;
//    _buttonMenu.hidden = !isRequestFromMenu;
    _buttonBack.hidden = YES;
    _buttonMenu.hidden = NO;
    
    [_textFieldSearch setIcon:@"" toTheRightDirect:NO withMaxIconWidth:0];
    
    [self addContextMenu];
    
    _preload = [[PreloadView alloc] initWithStringColor:@"black"];
    [self.view addSubview:_preload];
    [_preload renderPreloaderDependsSuperView];
    [_preload hide];
    self.items = [[NSMutableArray alloc] init];
    self.items_filtered = [[NSMutableArray alloc] init];
    [self loadItems];
    
    if( [self.items_filtered count] > 0 ) {
        [self pauseAll:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"People Rating Bubbles Screen";
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    // refactor
    // надо ли?
    timer = nil;
    addedLayers = nil;
    _preload = nil;
    _longTapItem = nil;
    _items = nil;
    _items_filtered = nil;
}

- (void)addContextMenu
{
    ContextMenuPeopleRating* overlay = [[ContextMenuPeopleRating alloc] init];
    overlay.dataSource = (id<PRContextOverlayViewDataSource>)self;
    overlay.delegate = (id<PRContextOverlayViewDelegate>)self;
    overlay.frame = CGRectMake(0.0, _scrollView.frame.origin.y, CONTENT_WIDTH, CONTENT_HEIGHT);
    [overlay setNeedsDisplay];
    // Do any additional setup after loading the view.
    //    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];
    [_scrollView addGestureRecognizer:_longPressRecognizer];
}


- (void)searchItemsLocal
{
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSRange searchRange = [[obj[@"item"] getFullname] rangeOfString: self.textFieldSearch.text options:NSCaseInsensitiveSearch];
        
        if ( searchRange.length > 0 ) {
            [self.items_filtered addObject: @{@"range"  : [NSValue valueWithRange:searchRange],
                                              @"item"   : obj[@"item"],
                                              @"circle" : obj[@"circle"]
                                            }];
        }
    }];
}

- (void)addItemsFiltered
{
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.items_filtered addObject: @{@"range"  : [NSValue valueWithRange:((NSRange){0,0})],
                                          @"item"   : obj[@"item"],
                                          @"circle" : obj[@"circle"]
                                         }];
    }];
}



#pragma mark -
#pragma mark Navigation

- (void)goToUserDetailFromPoint:(CGPoint)point
{
    for ( NSUInteger i = 0; i < [self.items count]; i++ ) {
        Circle *circle = self.items[i][@"circle"];
        
        if ( circle.x < point.x &&
            circle.x + 2 * circle.radius > point.x &&
            circle.y < point.y &&
            circle.y + 2 * circle.radius > point.y) {
            
            [self performSegueWithIdentifier:@"userDetail" sender:self.items[i][@"item"]];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self pauseAll:YES];
    [Routing prepareForSegue:segue sender:sender];
}

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionChangeEndSearch:(id)sender
{
    page = 0;

    [addedLayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [addedLayers removeAllObjects];
    [_scrollView setNeedsDisplay];
    
    _scrollView.contentOffset = CGPointMake(CONTENT_WIDTH / 2 - _scrollView.frame.size.width / 2, CONTENT_HEIGHT / 2 - _scrollView.frame.size.height / 2);
    
    
    [self.items_filtered removeAllObjects];
    
    isNotAvailable = NO;
    
    // Если поиска нет, то передаём всех вытащенных друзей
    // Иначе ищем совпадения в имени пользователя по строке поиска
    if( [_textFieldSearch.text isEqualToString: @""] ) {
        [self addItemsFiltered];
    } else {
        [self searchItemsLocal];
    }
    
    [self loadItems];
    
    // Обновляем уже найденный людей пока ищутся другие
    [self renderItemsLast:[self.items_filtered count] from:self.items_filtered];
}


#pragma mark -
#pragma mark Load and Render Items

- (void)loadItems
{
//    if( [lastSearch isEqualToString:_textFieldSearch.text] ) { return; }
    if ( isLoading || isNotAvailable ) { return; }
    
    isLoading = YES;
    [_preload showPreload];
    lastSearch = _textFieldSearch.text;
    
    [[UserAPI sharedInstance] searchPeopleByName:_textFieldSearch.text withPage:page withCallback:^(NSArray *people) {
        isLoading = NO;
        
        if( [people count] == 0 ) {
            isNotAvailable = YES;
            return;
        }
        
            
        // если новый запрос на поиск
        if ( page == 0 ) {
            [self.items removeAllObjects];
            [addedLayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
            [addedLayers removeAllObjects];
            [_scrollView setNeedsDisplay];
            
            _scrollView.contentOffset = CGPointMake(CONTENT_WIDTH / 2 - _scrollView.frame.size.width / 2, CONTENT_HEIGHT / 2 - _scrollView.frame.size.height / 2);
        }
        
        
        [people enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//                        if ( idx >= 10 ) {return; }
            [self.items addObject:@{@"item"      : obj,
                                    @"circle"    : [self circleByPeople:(ModelUser *)obj atIndex:idx]
                                    }];
            
            //            [self renderItemsLast:idx + 1];
        }];
        

        [self.items_filtered removeAllObjects];
        
        // Если поиска нет, то передаём всех вытащенных друзей
        // Иначе ищем совпадения в имени пользователя по строке поиска
        if( [_textFieldSearch.text isEqualToString: @""] ) {
            [self addItemsFiltered];
        } else {
            [self searchItemsLocal];
        }
        
        [_preload hidePreload];
        
        [self renderItemsLast:[people count] from:self.items_filtered];
    } failed:^{
        
    }];
}

- (void)renderItemsLast:(NSInteger)peopleCount from:(NSMutableArray *)items
{
    
    for( NSUInteger i = [items count] - peopleCount; i < [items count]; i++ ) {
        
        Circle *circle = items[i][@"circle"];
//        NSLog(@"item %u x: %f, y: %f, radius: %f", i, circle.x, circle.y, circle.radius);
        
        CALayer *layer = [CALayer layer];
        CALayer *layerImage = [CALayer layer];
        CALayer *layerImageDark = [CALayer layer];
        
        layer.cornerRadius = circle.radius;
        layer.masksToBounds = YES;
        layer.frame = CGRectMake(circle.x, circle.y, circle.radius * 2, circle.radius * 2);
        
//        [layer setBackgroundColor:[UIColor whiteColor].CGColor];
        
        layerImage.cornerRadius = circle.radius - WIDTH_BETWEEN_CIRCLES;
        layerImage.masksToBounds = YES;
        layerImage.frame = CGRectMake(WIDTH_BETWEEN_CIRCLES, WIDTH_BETWEEN_CIRCLES, circle.radius * 2 - 2 * WIDTH_BETWEEN_CIRCLES, circle.radius * 2 - 2 * WIDTH_BETWEEN_CIRCLES);
        [layerImage hitTest:[tap locationInView:_scrollView]];
        
        // Для красивого появления кругляшей
        layerImageDark.cornerRadius = circle.radius - WIDTH_BETWEEN_CIRCLES;
        layerImageDark.masksToBounds = YES;
        layerImageDark.borderColor = [UIColor h_randomWishBackgroundColor].CGColor;
        layerImageDark.borderWidth = circle.radius - WIDTH_BETWEEN_CIRCLES;
        layerImageDark.frame = CGRectMake(WIDTH_BETWEEN_CIRCLES, WIDTH_BETWEEN_CIRCLES, circle.radius * 2 - 2 * WIDTH_BETWEEN_CIRCLES, circle.radius * 2 - 2 * WIDTH_BETWEEN_CIRCLES);
//        [layerImageDark setBackgroundColor:[UIColor redColor].CGColor];
        
        [layer addSublayer:layerImage];
        [layer addSublayer:layerImageDark];
        
        [_scrollView.layer addSublayer:layer];
        [addedLayers addObject:layer];
        circle.addedLayer = layer;

        // Пытаемся найти шары, у которых траектории движения пересекаются
        for( NSUInteger j = i+1; j < [items count]; j++ ) {
            Circle *nextCircle = items[j][@"circle"];
            [circle followToCrossingCircle:nextCircle];
        }
        
        
        [UIImage downloadWithUrl: [NSURL URLWithString: [items[i][@"item"] getAvatar]] onCompleteChange:^(UIImage *image) {
            layerImage.contents = (id)image.CGImage;
            
            [CATransaction begin];
            [CATransaction setAnimationDuration:0.5f];
            [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [CATransaction setCompletionBlock:^{
                [layerImageDark removeFromSuperlayer];
                [circle startMove];
            }];
            layerImageDark.borderWidth = 0;
            [CATransaction commit];
        }];
    }
    
    isMovingCircles = YES;
    [self moveAllCircle];
    
//    if( timer != nil ) {
//        [timer invalidate];
//    }
//    
//    timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(moveAllCircle) userInfo:nil repeats:YES];
}

- (void)circleTap:(UITapGestureRecognizer *)recognizer
{
    [self goToUserDetailFromPoint: [recognizer locationInView:_scrollView]];
}

#pragma mark -
#pragma mark Determine position of new Circle

- (Circle *)circleByPeople:(ModelUser *)user atIndex:(NSInteger)index
{
    Circle *peopleCircle = [[Circle alloc] init];
    peopleCircle.moveDelegate = (id<PRCircleProtocol>)self;
    peopleCircle.radius = [self circleRadiusByPeople:user];
    peopleCircle.angleMove = arc4random_uniform(6);

    if( index == 0 ) {
        // Ставим первую аву в центр скрол вью
        peopleCircle.x = CONTENT_WIDTH / 2 - peopleCircle.radius;
        peopleCircle.y = CONTENT_WIDTH / 2 - peopleCircle.radius;
        
        return peopleCircle;
    } else if ( index == 1 ) {
        double angle;
        
        // Берём произвольный угол от первой и ставим вторую аву
        Circle *firstCircle = [self circleByPeople:[self.items objectAtIndex:0][@"item"] atIndex:0];
        angle = 5.5;//arc4random_uniform(360);
        
        
        // По теореме Пифагора расчитываем координаты
        [peopleCircle setCoordinates:CGPointMake(
                                                ([firstCircle getCenter].x) + (cos(angle) * (peopleCircle.radius + firstCircle.radius)) - (peopleCircle.radius),
                                                ([firstCircle getCenter].y) + (sin(angle) * (peopleCircle.radius + firstCircle.radius)) - (peopleCircle.radius))];
        
        [self insertedToSquare:peopleCircle];
        
        return peopleCircle;
        
    } else if ( index == 2 ) {
        
        CGPoint location = [self circlePositionWithRadius:peopleCircle.radius between:[self.items objectAtIndex:index-2][@"circle"] and:[self.items objectAtIndex:index-1][@"circle"] withIndex:index];
        
        [peopleCircle setCoordinates:location];
    
        [self insertedToSquare:peopleCircle];
        
        return peopleCircle;
    }
    
    
    Circle *subling = [self sublingCirclesFor:[self.items objectAtIndex:index - 1][@"circle"] until:index-1];
    
    CGPoint location = [self circlePositionWithRadius:peopleCircle.radius between:[self.items objectAtIndex:index-1][@"circle"] and:subling withIndex:index];
    
    [peopleCircle setCoordinates:location];
    
    [self insertedToSquare:peopleCircle];
    
    return peopleCircle;
}

- (float)circleRadiusByPeople:(ModelUser *)user
{
    // TODO:
//    add getRating method
    float radius = 0 / 50;
//    float radius = [user getRating] / 50;
    
    if ( radius < 40.0 ) {
//        radius = arc4random_uniform(75.0);
        radius = 40.0;
    } else if ( radius > 75.0 ) {
        radius = 75.0;
    }
    
    return radius;
}

- (Circle *)sublingCirclesFor:(Circle *)current until:(NSInteger)index
{
    __block Circle *subling;
    NSArray *sublings = [self sublingsCirclesFor:current until:index];
    
    if( index == 10 ) {
        
    }
    // Двигаемся по часовой стрелки
    switch ( lastInserSquare ) {
        case 1: {
            // Вниз и влево
            // Ищем соседний круг, который ниже и левее, чем текущий
            [sublings enumerateObjectsUsingBlock:^(Circle *obj, NSUInteger idx, BOOL *stop) {
                if ( [current getCenter].y > [obj getCenter].y ) {
                    if ( subling == nil ) { subling = obj; }
                    else {
                        if( [subling getCenter].x > [obj getCenter].x ) { subling = obj; }
                    }
                } else {
                    if ( subling == nil ) { subling = obj; }
                    else if ( [current getCenter].x > [obj getCenter].x ) {
                        subling = obj;
                    }
                }
                
            }];
            break;
        }
        case 2: {
            // Влево и вверх
            // Ищем соседний круг, который левее и выше, чем текущий
            [sublings enumerateObjectsUsingBlock:^(Circle *obj, NSUInteger idx, BOOL *stop) {
                
                if ( [current getCenter].x > [obj getCenter].x ) {
                    if ( subling == nil ) { subling = obj; }
                    else {
                        if( [subling getCenter].x > [obj getCenter].x ) { subling = obj; }
                    }
                } else {
                    if ( subling == nil ) { subling = obj; }
                    else if ( ([current getCenter].x < [subling getCenter].x) && [subling getCenter].y > [obj getCenter].y ) {
                        subling = obj;
                    }
                    
                }
                
            }];

            break;
        }
        case 3: {
            if ( index == 11 ) {
                
            }
            // Вверх и Вправо
            // Ищем соседний круг, который выше и правее, чем текущий
            [sublings enumerateObjectsUsingBlock:^(Circle *obj, NSUInteger idx, BOOL *stop) {
//                NSLog(@"x,y,r, %f, %f, %f", obj.x, obj.y, obj.radius);
                if ( [current getCenter].y < [obj getCenter].y ) {
                    if ( subling == nil ) { subling = obj; }
                    else {
                        if( [subling getCenter].x < [obj getCenter].x ) { subling = obj; }
                    }
                } else {
                    if ( [current getCenter].x < [obj getCenter].x ) {
                        subling = obj;
                    }
                }
            }];
            break;
        }
        case 4: {
            // Вправо и вниз
            // Ищем соседний круг, который правее и ниже, чем текущий
            [sublings enumerateObjectsUsingBlock:^(Circle *obj, NSUInteger idx, BOOL *stop) {
                
                if ( [current getCenter].x < [obj getCenter].x ) {
                    if ( subling == nil ) { subling = obj; }
                    else {
                        if( [subling getCenter].x < [obj getCenter].x ) { subling = obj; }
                    }
                } else {
                    if ( subling == nil ) { subling = obj; }
                    else if ( ([current getCenter].x > [subling getCenter].x) && [subling getCenter].y < [obj getCenter].y ) {
                        subling = obj;
                    }
                }
            }];
            
            
            break;
        }
    }
    
    return subling;
}

- (NSArray *)sublingsCirclesFor:(Circle *)current until:(NSInteger)index
{
    NSMutableArray *circles = [[NSMutableArray alloc] init];
    
    // Находим прилегающие окружности к последней вставленной
    for(NSInteger i = 0; i < index; i++ ) {
        if ( [self isCircle:current crossing:self.items[i][@"circle"]] ) {
            [circles addObject:self.items[i][@"circle"]];
        }
    }
    
    return circles;
}

- (BOOL)isCircle:(Circle *)first crossing:(Circle *)second
{
    BOOL isCross;
    float betweenCenterOfCircles = roundf(sqrt( pow([second getCenter].x - ([first getCenter].x), 2) + pow([second getCenter].y - ([first getCenter].y), 2) ));
    
    isCross = betweenCenterOfCircles == roundf(first.radius + second.radius) ? YES : NO;
    
    return isCross;
}

- (CGPoint)circlePositionWithRadius:(float)circleRadius between:(Circle *)first and:(Circle *)second withIndex:(NSInteger)index
{
    return [self circlePositionWithRadius:circleRadius between:first and:second withIndex:index isSublings:YES];
}


- (CGPoint)circlePositionWithRadius:(float)circleRadius between:(Circle *)first and:(Circle *)second withIndex:(NSInteger)index isSublings:(BOOL)isSublings
{
    // вычисляем координату точки круга через длины векторов
    // от центра первого круга до искомого и от центра второго круга до искомого круга
    CGFloat X1 = [first getCenter].x;
    CGFloat Y1 = [first getCenter].y;
    CGFloat X2 = [second getCenter].x;
    CGFloat Y2 = [second getCenter].y;
    CGFloat AC = first.radius + circleRadius;
    CGFloat BC = second.radius + circleRadius;
    CGFloat AB = isSublings ? first.radius + second.radius : sqrtf( powf(X2 - X1, 2) + powf(Y2 - Y1, 2) );
    
    
    // для упрощения итоговой формулы
    double alpha = powf(AC, 2) - powf(BC, 2) + powf(X2, 2) - powf(X1, 2) - powf(Y1, 2) + powf(Y2, 2);
    double K = alpha / ( 2 * (X2 - X1));
    double L = K - X2;
    double M = (Y2 - Y1) / (X2 - X1);
    double G = (Y2 + L * M) / (1 + pow(M, 2));
    CGFloat y = sqrtf(
                     ( (powf(BC, 2) - pow(L, 2) - powf(Y2, 2)) /
                     ( 1 + pow(M, 2)) )
                     + pow( G, 2)
                    ) + ((Y2 + L * M) / (1 + pow(M, 2)));
    
    CGFloat x = K - ( y * M );
    
    // поворот со смещением оси
    CGFloat OAC = acosf((x - X1) / (circleRadius + first.radius));
    OAC = y < Y1 ? -OAC : OAC;

//    CGFloat OBC = acosf((x - X2) / (circleRadius + second.radius));
    CGFloat cosAlpha = (powf(AC, 2) + powf(AB, 2) - powf(BC, 2)) / (2 * (AC * AB));
    CGFloat doubleAlpha = 2 * acosf(cosAlpha);
    CGFloat angleTurn = - (X1 > X2 ? doubleAlpha + OAC : OAC - doubleAlpha);
    
    CGFloat newOSX = AC * cosf( angleTurn );
    CGFloat newOSY = AC * sinf( angleTurn );
    
    CGFloat X4 = (X1 > X2 ? X1 + newOSX : X1 + newOSX);
    CGFloat Y4 = (Y1 < Y2 ? Y1 - newOSY : Y1 - newOSY);

//    CGFloat AC1 = second.radius + circleRadius;
//    CGFloat BC1 = first.radius + circleRadius;
//    CGFloat AB1 = first.radius + second.radius;
//    CGFloat X11 = second.x + second.radius;
//    CGFloat Y11 = second.y + second.radius;
//    CGFloat X21 = first.x + first.radius;
//    CGFloat Y21 = first.y + first.radius;
//    
//    // для упрощения итоговой формулы
//    CGFloat alpha1 = pow(AC1, 2) - pow(BC1, 2) + pow(X21, 2) - pow(X11, 2) - pow(Y11, 2) + pow(Y21, 2);
//    CGFloat K1 = alpha1 / ( 2 * (X21 - X11));
//    CGFloat L1 = K1 - X21;
//    CGFloat M1 = (Y21 - Y11) / (X21 - X11);
//    CGFloat G1 = (Y21 + L1 * M1) / (1 + pow(M1, 2));
//    CGFloat Y4 = sqrt(
//                     ( (pow(BC1, 2) - pow(L1, 2) - pow(Y21, 2)) /
//                      ( 1 + pow(M1, 2)) )
//                     + pow( G1, 2)
//                     ) + ((Y21 + L1 * M1) / (1 + pow(M1, 2)));
//    
//    CGFloat X4 = K1 - ( Y4 * M1 );
    
    // Находим координаты второй касательной окружности
//    CGFloat OA = sqrt(pow(X1, 2) + pow(Y1, 2));
//    CGFloat OB = sqrt(pow(X2, 2) + pow(Y2, 2));
//    CGFloat cosT = (pow(AC, 2) + pow(AB, 2) - pow(BC, 2)) / (2 * (AC * AB));
//    CGFloat cosF = (pow(OA, 2) + pow(AB, 2) - pow(OB, 2)) / (2 * (OA * AB));
//    CGFloat cosS = (pow(OB, 2) + pow(OA, 2) - pow(AB, 2)) / (2 * (OB * OA));
//    
//    CGFloat S = acosf(cosS);
//    CGFloat FT = acosf(cosF) - acosf(cosT);
//    CGFloat O1D = sinf(FT) * (first.radius + circleRadius);
//    CGFloat OD = O1D / sinf(S);
//    
//    CGFloat cosA1OA = X1 / OA;
//    CGFloat cosB1OB = X2 / OB;
//    CGFloat A1OA = acosf(cosA1OA);
//    CGFloat B1OB = acosf(cosB1OB);
//    CGFloat B = (B1OB > A1OA ? B1OB : A1OA) - S;
//    
//    CGFloat X4 = cosf(B) * OD;
//    CGFloat Y4 = sinf(B) * OD;

//    CGFloat X4 = (first.radius + circleRadius)*cosf(-doubleAlpha*M_PI/180) + X1;
//    CGFloat Y4 = -(first.radius + circleRadius)*sinf(-doubleAlpha*M_PI/180) + Y1;
//    CGFloat newOSX = x * cosf(-doubleAlpha) - y * sinf(-doubleAlpha);
//    CGFloat newOSY = y * cosf(-doubleAlpha) + x * sinf(-doubleAlpha);
//    CGFloat X4 = X2 * AC / newOSX;
//    CGFloat Y4 = Y2 * BC / newOSY;
//    CGFloat cosA = (pow(AB, 2) + pow(BC, 2) - pow(AC, 2)) / (2 * (AB * BC));
//    CGFloat dY = sqrt( pow((second.radius), 2) + pow(BC, 2) - (2 * second.radius * BC * cosA ));
//    CGFloat CD = 2 * dY;


    // основываясь на О.О
//    CGFloat XYA = pow(X1, 2) + pow(Y1, 2);
//    CGFloat K4 = OD;//sqrt(XYA) - BC;
//    CGFloat J4 = pow(BC, 2) - XYA + pow(K4, 2);
//    CGFloat Y4 = sqrt( (4 * pow(X1, 2) * pow(K4, 2) - pow(J4, 2) ) / (4 * XYA)
//                      + pow( Y1 * J4 / ( 2 * XYA ), 2) )
//                 - ( Y1 * J4 / ( 2 * XYA ) );
//    
//    CGFloat X4 = sqrt(pow(K4, 2) - pow(Y4, 2));
    
    // Наоборот выразив у через х
    // AD == AC
    // для упрощения итоговой формулы
//    alpha = pow(AC, 2) - pow(CD, 2) + pow(x, 2) - pow(X1, 2) - pow(Y1, 2) + pow(y, 2);
//    K = alpha / ( 2 * (x - X1));
//    L = K - x;
//    M = (y - Y1) / (x - X1);
//    G = (y + L * M) / (1 + pow(M, 2));
//    CGFloat Y4 = sqrt(
//                     ( (pow(CD, 2) - pow(L, 2) - pow(y, 2)) /
//                      ( 1 + pow(M, 2)) )
//                     + pow( G, 2)
//                     ) + ((y + L * M) / (1 + pow(M, 2)));
//    
//    CGFloat X4 = K - ( Y4 * M );
    
    
//    CGFloat N = (X2 - X1) / (Y2 - Y1);
//    CGFloat O = pow(BC, 2) - pow(N, 2) * pow(K, 2) - pow(Y2, 2) - pow(X2, 2) + 2 * Y2 * N * K;
//    CGFloat P = O / (1 + pow(N, 2));
//    CGFloat R = (Y2*N - X2 - K * pow(N, 2)) / (1 + pow(N, 2));
//    
//    CGFloat X = sqrt(P + pow(R, 2) ) - R;
//    CGFloat Y = (K - X) * N;
    
//    NSLog(@"\r\r i: %ld x:%f y:%f X4: %f Y4: %f", index, x, y, X4, Y4);
//    NSLog(@"first.x, y, radius: %f, %f %f second.x,y.radius: %f %f %f", first.x, first.y, first.radius, second.x, second.y, second.radius);
    CGFloat Xout;
    CGFloat Yout;
    
    if ( lastInserSquare == 1 ) {
        if ( y > Y4 ) {
            Xout = x;
            Yout = y;
        } else {
            Xout = X4;
            Yout = Y4;
        }
    } else if ( lastInserSquare == 2 ) {
        if ( x < X4 ) {
            Xout = x;
            Yout = y;
        } else {
            Xout = X4;
            Yout = Y4;
        }
    } else if ( lastInserSquare == 3 ) {
        if ( y < Y4 ) {
            Xout = x;
            Yout = y;
        } else {
            Xout = X4;
            Yout = Y4;
        }
    } else {
        if ( x > X4 ) {
            Xout = x;
            Yout = y;
        } else {
            Xout = X4;
            Yout = Y4;
        }
    }
    // если одни координаты пересекают прилегающие окружности
    if( isnan(x) || isnan(y) ) {
        Xout = X4;
        Yout = Y4;
    }
    
    if( isnan(X4) || isnan(Y4) ) {
        Xout = x;
        Yout = y;
    }
    
    for( NSInteger i = 0; i < index; i++ ) {
        Circle *c = self.items[i][@"circle"];
        
        if ( roundf(sqrtf(powf(Xout - ([c getCenter].x), 2) + powf(Yout - ([c getCenter].y), 2))) < roundf(c.radius + circleRadius)) {
//            NSLog(@"find cross circle index: %ld %f %f < %f ,,, c.x: %f c.y: %f, c.radius: %f, Xout: %f, Yout: %f", i, Yout - [c getCenter].y, roundf(sqrtf(pow(Xout - (c.x + c.radius), 2) + pow(Yout - (c.y + c.radius), 2))), c.radius + circleRadius, c.x + c.radius, c.y + c.radius, c.radius, Xout, Yout);
            if( roundf( roundf([c getCenter].x * 100) / 100 == roundf(Xout * 100) / 100 ) ) {
                Xout = Xout == x ? X4 : x;
                Yout = Yout == y ? Y4 : y;
                i = 0;
                continue;
            }
//
//            for( NSInteger j = 0; i < index; i++ ) {
//                Circle *c = self.items[j][@"circle"];
//                //NSLog(@"2 find cross circle index: %ld %f < %f ,,, c.x: %f c.y: %f, c.radius: %f, Xout: %f, Yout: %f", j, roundf(sqrtf(powf(Xout - ([c getCenter].x), 2) + powf(Yout - ([c getCenter].y), 2))), c.radius + circleRadius, c.x + c.radius, c.y + c.radius, c.radius, Xout, Yout);
//                if ( roundf(sqrtf(powf(Xout - ([c getCenter].x), 2) + powf(Yout - ([c getCenter].y), 2))) < c.radius + circleRadius) {
//                    if( roundf( roundf([c getCenter].x * 100) / 100 != roundf(Xout * 100) / 100 ) ) {
//                        return [self circlePositionWithRadius:circleRadius between:first and:c withIndex: index isSublings:NO];
//                    }
//                }
//            }

            return [self circlePositionWithRadius:circleRadius between:first and:c withIndex: index isSublings:NO];
//            }
        }
    }

    return CGPointMake(Xout - circleRadius, Yout - circleRadius);

}

- (void)insertedToSquare:(Circle *)circle
{
    // 1 и 2
    if ( [circle getCenter].y > _scrollView.contentSize.height / 2 ) {
        if ( [circle getCenter].x < _scrollView.contentSize.width / 2 ) {
            lastInserSquare = 2;
        } else {
            lastInserSquare = 1;
        }
    } else {
    // 3 и 4
        if ( [circle getCenter].x < _scrollView.contentSize.width / 2 ) {
            lastInserSquare = 3;
        } else {
            lastInserSquare = 4;
        }
    }
}





#pragma mark - GHMenu methods

- (NSInteger) numberOfMenuItems
{
    return [_longTapItem[@"title"] isEqualToString:@""] ? 0 : 1;
}

- (UIImage*)imageForItemAtIndex:(NSInteger)index
{
    return [UIImage imageNamed:_longTapItem[@"image"]];
}

- (UIColor*) colorForItemAtIndex:(NSInteger) index
{
    
    return _longTapItem[@"color"];
}

- (NSString *) titleForItemAtIndex:(NSInteger) index
{
    return _longTapItem[@"title"];
}

- (NSString*) ratingForItemAtIndex:(NSInteger) index
{
    return _longTapItem[@"rating"];
}

- (void)pauseAll:(BOOL)state
{
    isMovingCircles = !state;
    
    if( isMovingCircles ) {
        [self moveAllCircle];
    }
}

- (void) didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point
{
    [self goToUserDetailFromPoint:point];
}

- (void)GHMenuItemsAtPoint:(CGPoint)touchPoint
{
    _longTapItem[@"title"] = @"";
    _longTapItem[@"touchedIndex"] = [NSNumber numberWithInteger:999];
    
    for (NSInteger i = 0; i < [self.items count]; i++ ) {
        Circle *circle = self.items[i][@"circle"];
        
        if ( circle.x < touchPoint.x &&
            circle.x + 2 * circle.radius > touchPoint.x &&
            circle.y < touchPoint.y &&
            circle.y + 2 * circle.radius > touchPoint.y)
        {
            _longTapItem[@"title"] = [[self.items[i][@"item"] getFullname] stringByReplacingOccurrencesOfString:@" " withString:@"\r"];
            _longTapItem[@"touchedIndex"] = [NSNumber numberWithInteger:i];
//            _longTapItem[@"rating"] = [NSString stringWithFormat:@"%ld", (long)[self.items[i][@"item"] getRating]];
        }
    }
}

- (NSDictionary *)GHLocationAtPoint:(CGPoint)touchPoint
{
    if ( [self.items count] > [_longTapItem[@"touchedIndex"] integerValue] ) {
        Circle *circle = self.items[[_longTapItem[@"touchedIndex"] integerValue]][@"circle"];

        return [[NSDictionary alloc] initWithObjects:@[
                                                       [NSNumber numberWithFloat:[circle getCenter].x - _scrollView.contentOffset.x],
                                                       [NSNumber numberWithFloat:[circle getCenter].y - _scrollView.contentOffset.y],
                                                       [NSNumber numberWithFloat:circle.radius - WIDTH_BETWEEN_CIRCLES]]
                                             forKeys:@[@"x", @"y", @"radius"]];
        
    }
    
    return nil;
}


#pragma mark -
#pragma mark PRCircleProtocol

- (void)moveAllCircle
{
    [timer invalidate];
    
    if ( isMovingCircles && self.navigationController.visibleViewController == self ) {
//    if ( isMovingCircles ) {
        for (NSUInteger i = 0; i < [self.items_filtered count]; i++ ) {
            [self.items_filtered[i][@"circle"] moveCircle];
        }
        
        [self isCrossing];
        
        timer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(moveAllCircle) userInfo:nil repeats:NO];
    }
}

- (void)isCrossing
{
    NSUInteger iteration = 0;
//    CGRectIntersectsRect
    for (NSUInteger i = 0; i < [self.items_filtered count]; i++ ) {
        Circle *circle = self.items_filtered[i][@"circle"];
        circle.isChangeAngleMove = NO;
        iteration++;
    }
    
    for (NSUInteger i = 0; i < [self.items_filtered count]; i++ ) {
        Circle *circle = self.items_filtered[i][@"circle"];
        
        iteration++;
        
        for (NSUInteger j = i + 1; j < [self.items_filtered count]; j++ ) {
            Circle *c = self.items_filtered[j][@"circle"];
            CGFloat distanceBetweenCenterOfCircles = sqrtf(powf([circle getCenter].x - [c getCenter].x, 2) + powf([circle getCenter].y - [c getCenter].y, 2));
            CGFloat minDistanceBetweenCenterOfCircles = circle.radius + c.radius - (2 * WIDTH_BETWEEN_CIRCLES);
            
            iteration++;
            if ( distanceBetweenCenterOfCircles <= minDistanceBetweenCenterOfCircles ) {
                if( !circle.isChangeAngleMove ) {
//                    circle.angleMove = M_PI - circle.angleMove;
//                        circle.speedMove = CGPointMake(-circle.speedMove.x, -circle.speedMove.y);
                    [circle calculateAngleAfterCrossWithCircle:c];
                    circle.isChangeAngleMove = YES;
                    c.isChangeAngleMove = YES;
                }
                
                if( !c.isChangeAngleMove ) {
                    c.isChangeAngleMove = YES;
                    [c calculateAngleAfterCrossWithCircle:circle];
//                    circle.angleMove = M_PI - circle.angleMove;
//                        c.speedMove = CGPointMake(-c.speedMove.x, -c.speedMove.y);
                }
            }
        }
    }
    

}

- (void)ifCircleAbroad:(Circle *)circle;
{
    if ( circle.x + WIDTH_BETWEEN_CIRCLES < 0 ) {
        circle.angleMove = circle.angleMove > M_PI ? 3 * M_PI - circle.angleMove : M_PI - circle.angleMove;
    } else if ( [circle getCenter].x + circle.radius - WIDTH_BETWEEN_CIRCLES > CONTENT_WIDTH ) {
        circle.angleMove = circle.angleMove > M_PI ? 3 * M_PI - circle.angleMove : M_PI - circle.angleMove;
    }
    
    if ( circle.y + WIDTH_BETWEEN_CIRCLES < 0 ) {
        circle.angleMove = 2 * M_PI - circle.angleMove;
    } else if ([circle getCenter].y + circle.radius - WIDTH_BETWEEN_CIRCLES > CONTENT_HEIGHT ) {
        circle.angleMove = 2 * M_PI - circle.angleMove;
    }
}

@end
