//
//  PeopleRatingViewController.h
//  Startwish
//
//  Created by marsohod on 22/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CustomTextField.h"
#import "BackButton.h"
#import "MenuButton.h"
#import "PreloadView.h"
#import "GAITrackedViewController.h"


@interface PeopleRatingViewController : GAITrackedViewController

- (IBAction)actionMenuButton:(id)sender;
- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionChangeEndSearch:(id)sender;

@property (strong, nonatomic) PreloadView *preload;
@property (nonatomic, strong) NSMutableDictionary       *longTapItem;
@property (nonatomic, strong) NSMutableArray            *items;
@property (nonatomic, strong) NSMutableArray            *items_filtered;
@property (weak, nonatomic) IBOutlet CustomTextField    *textFieldSearch;
@property (weak, nonatomic) IBOutlet BackButton         *buttonBack;
@property (weak, nonatomic) IBOutlet MenuButton         *buttonMenu;
@property (weak, nonatomic) IBOutlet UIScrollView       *scrollView;


@end
