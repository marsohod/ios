//
//  MailViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 12/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "MailViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"


#define BODY_WIDTH 800

@interface MailViewController ()
{
    NSMutableString *body;
}
@end

@implementation MailViewController

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        self.mailComposeDelegate = (id<MFMailComposeViewControllerDelegate>)self;        // Required to invoke mailComposeController when send
//        NSString *stylesheets = @"@media (max-width: 480px){body { max-width: 480px!important!; width: 480px!important; } .content { width: 440px!important; }";
//        
//        body = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"<!DOCTYPE html><html><head><meta charset='utf-8'><style>%@</style></head><body><center><table align='center' width='100%%' height='100%%' cellspacing='0' cellpadding='0' style='border-collapse:collapse;margin:0;padding:0;height:100%%!important;width:100%%!important'><tr><td align='center' valign='top' style='margin:0;padding:20px;border-top:0;height:100%%!important;width:100%%!important'><table width='%u' cellspacing='0' cellpadding='0' class='content'><tr><td style='width:100%%'><table style='width:100%%' cellspacing='0' cellpadding='0' border='0'><tr><td></td><td style='width: 100%%; vertical-align: middle;'>", stylesheets, BODY_WIDTH]];
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName
           value:@"Share Wish by Email"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertTextToBody:(NSString *)text
{
    [body appendString: [NSString stringWithFormat:@"<p>%@</p>", text]];
}

- (void)insertImageToBody:(UIImage *)image
{

    [self addAttachmentData:UIImageJPEGRepresentation(image, 0.7)
                   mimeType:@"image/jpeg"
                   fileName:@"Startwish wish.jpeg"];
}

- (void)sendMail
{
    //close the HTML formatting
//    [body appendString:@"</td><td></td></tr></table></td></tr></table></td></tr><table></center></body></html>"];
    [self setMessageBody:@"" isHTML:NO];
}


#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            
            if ( _notification != nil ) {
                [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"EmailSend"]];
            } else {
                NSLog(@"You sent the email.");
            }
            
            break;
        case MFMailComposeResultSaved:
            
            if ( _notification != nil ) {
                [_notification showNotify:1 withText:[[ActionTexts sharedInstance] successText:@"EmailDraftSave"]];
            } else {
                NSLog(@"You saved a draft of this email");
            }
            
            break;
        case MFMailComposeResultCancelled:
            
            if ( _notification != nil ) {
//                [_notification showNotify:1 withText:@"Отменилось"];
            } else {
                NSLog(@"You cancelled sending this email.");
            }
            
            break;
        case MFMailComposeResultFailed:
            
            if ( _notification != nil ) {
                [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailSend"]];
            } else {
                NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            }
            
            break;
        default:
            
            if ( _notification != nil ) {
                [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"default2"]];
            } else {
                NSLog(@"An error occurred when trying to compose this email");
            }
            
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
