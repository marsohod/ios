//
//  MailViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 12/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <MessageUI/MessageUI.h>
#import "NotificationLabel.h"
#import "ActionTexts.h"


@interface MailViewController : MFMailComposeViewController

@property (nonatomic, weak) NotificationLabel *notification;

- (void)insertTextToBody:(NSString *)text;
- (void)insertImageToBody:(UIImage *)image;
- (void)sendMail;

@end
