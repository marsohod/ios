//
//  ExplorerViewController.h
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PinterestViewController.h"
#import "CustomTextField.h"
#import "MenuButton.h"
#import "UICollectionViewWishWithSegmentControl.h"
#import "PreloadView.h"
#import "NotificationLabel.h"
#import "WishlistSelectViewController.h"
#import "BGBlueView.h"
#import "CategoriesTableView.h"


@interface ExplorerViewController : PinterestViewController

@property (strong, nonatomic) PreloadView*                                      preload;
@property (weak, nonatomic) IBOutlet BGBlueView*                                viewTop;
@property (weak, nonatomic) IBOutlet UIView*                                    viewWishes;
@property (weak, nonatomic) IBOutlet CustomTextField*                           textFieldSearch;
@property (weak, nonatomic) IBOutlet MenuButton*                                buttonMenu;
@property (weak, nonatomic) IBOutlet CategoriesTableView *                      tableView;
@property (weak, nonatomic) IBOutlet UICollectionViewWishWithSegmentControl*    collectionView;
@property (weak, nonatomic) IBOutlet NotificationLabel*                         notification;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint*                        viewTopMargin;
@property (weak, nonatomic) IBOutlet UIView*                                    viewEmptyWishes;

- (IBAction)actionButtonMenu:(id)sender;
- (IBAction)actionEditDidEndTextField:(id)sender;

- (void)setOpenedCategory:(uint32_t)categoryId;

@end
