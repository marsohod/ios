//
//  ExplorerViewController.m
//  Startwish
//
//  Created by marsohod on 18/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ExplorerViewController.h"
#import "UIColor+Helper.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "RevealViewController.h"
#import "WishesAPI.h"
#import "Routing.h"
#import "ActionTexts.h"
#import "ButtonBeast.h"
#import "SeguePinterestOpenUnwind.h"
#import "ModelWishCategory.h"


@implementation ExplorerViewController
{
    NSString*   cellIdentifier;
    NSArray*    categories;
    NSString*   lastSearch;
    NSInteger   lastCategoryIdSearch;
    BOOL        isNeedPopularWishes;
    BOOL        isLoading;
    ModelWish   *wishInAction;
    uint32_t    openedCategory;
}

const CGFloat animation_duration = 0.5f;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lastSearch = @"";
    
    [self prepareCollectionView];
    
    // Разукрашиваем фон
    [self.view setBackgroundColor: [UIColor h_smoothGreen]];
    
    // Убираем клавиатуру после того, как закончили что-то вводить
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    tapView.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapView];
    
    // Инициализируем ячейку таблицы с категориями
    cellIdentifier = @"explorerCell";
    [_tableView registerNib: [UINib nibWithNibName: @"CategoryCell" bundle:nil] forCellReuseIdentifier: cellIdentifier];

    
    // Ставим иконку для textField
    [_textFieldSearch setIcon:@"" toTheRightDirect:NO withMaxIconWidth:0.0];
    _textFieldSearch.delegate = (id<UITextFieldDelegate>)self;
    
    // Инициализируем заглушку загрузки
    _preload = [[PreloadView alloc] initWithStringColor:@"gray"];
    _preload.hidden = YES;
    [_viewWishes addSubview:self.preload];
    
    [_preload hide];
    
    // Следим за тем, когда клавиатура появляется или исчезает
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(renderWishCell:)
                                                 name:@"STWishUpdated"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(actionEditDidEndTextField:)
                                                 name:@"STEditDidEndTextField"
                                               object:nil];
    
    [self loadCategories];
    
    buttonBeast = [[ButtonBeast alloc] initWithPoint:CGPointMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - _viewTop.frame.size.height)];
    [_viewWishes insertSubview:buttonBeast belowSubview:self.preload];
    buttonBeast.hidden = YES;
    
    if ( openedCategory ) {
        [self actionEditDidEndTextField:nil];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [WishlistSelectViewController sharedInstance].delegate = (id<PUViewProtocol>)self;
    self.screenName = @"Explorer Wishes Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ( _buttonMenu.hidden == NO ) {
        [_buttonMenu updateEventCount:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)searchItems
{
//    [_preload showPreload];
//    buttonBeast.hidden = YES;
    [self loadItems];
}

- (void)loadItems
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    lastCategoryIdSearch = [_tableView indexPathForSelectedRow] != nil ? [[_tableView.items objectAtIndex:[_tableView indexPathForSelectedRow].row] getId] : 0;
    lastSearch = _textFieldSearch.text;
    
    if ( [_collectionView currentPage] == 0 ) {
        [_preload showPreload];
        [_collectionView reloadData];
        _collectionView.contentOffset = CGPointMake(0, 0);
        _viewEmptyWishes.hidden = YES;
    }
    
    [_collectionView handleBeginRefreshing];
    
    [[WishesAPI sharedInstance] searchWishesByName: lastSearch
                                        categoryId: lastCategoryIdSearch
                                     popularStatus: isNeedPopularWishes
                                          withPage: [_collectionView currentPage]
                                            update: ^(NSArray *items) {
                                                _viewEmptyWishes.hidden = !( [items count] == 0 && [_collectionView currentPage] == 0 );
                                                
                                                //@[[[Wish alloc] initWithObject:@{}]]
                                                [_collectionView updateWithItems:items];
                                                [_preload hidePreload];
                                                [_collectionView handleEndRefreshing];

                                                isLoading = NO;
                                            } failed:^{
                                                isLoading = NO;
                                                [_preload hidePreload];
                                                [_collectionView handleEndRefreshing];
                                            }];
}

//- (void)pullToRefreshViewDidStartLoading:(SSPullToRefreshView *)view {
//    
//}

- (void)loadCategories
{
    // TODO REFACTOR
    // Add id to cache array
    if ( [[WishesAPI getCategoriesFromCache] count] > 0 ) {
        _tableView.items = [self addFirstCategory: [WishesAPI getCategoriesFromCache]];
        [_tableView reloadData];
        [self selectOpenedCategory];
    }
    
    [[WishesAPI sharedInstance] getCategories:^(NSArray *items) {
        NSArray *list = [self addFirstCategory:items];
        
        if ( [_tableView.items isEqualToArray:list] ) {
            return;
        }
        
        _tableView.items = list;
        [WishesAPI setCategoriesToCache:items];
        [_tableView reloadData];
        [self selectOpenedCategory];
    }];
}

- (void)selectOpenedCategory
{
    if ( openedCategory != 0 ) {
        for (NSInteger i = 0; i < [_tableView.items count]; i++ ) {
            if ( [_tableView.items[i] getId] == openedCategory ) {
                [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
                break;
            }
        }
    }
}

- (NSArray *)addFirstCategory:(NSArray *)items
{
    
    ModelWishCategory *categoryAll = [[ModelWishCategory alloc] init];
    
    [categoryAll setID:@"0"];
    [categoryAll setName:@"Все"];
    [categoryAll setPoster:@"http://www.startwish.ru/images/build/about_bottom_1__2x.png"];

    NSMutableArray *list = [[NSMutableArray alloc] initWithArray:items];
    [list insertObject:categoryAll atIndex:0];
    
    return [[NSArray alloc] initWithArray:list];
}

- (void)setOpenedCategory:(uint32_t)categoryId
{
    openedCategory = categoryId;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
//    [self.view endEditing:YES];
}

- (IBAction)actionButtonMenu:(id)sender {
    [_textFieldSearch resignFirstResponder];
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionEditDidEndTextField:(id)sender
{
    NSInteger selectedCategoryId = [_tableView indexPathForSelectedRow] != nil ? [[_tableView.items objectAtIndex:[_tableView indexPathForSelectedRow].row] getId] : 0;
    
    [self.view endEditing:YES];
    // Если ещё нет загруженных желаний, то не показываем ничего
    if ( [_collectionView currentPage] != 0 ) {
        [UIView animateWithDuration:animation_duration animations:^{
            _tableView.layer.opacity = 0.0f;
            _collectionView.layer.opacity = 1.0f;
        } completion:^(BOOL finished) {
            _collectionView.hidden = NO;
            _tableView.hidden = YES;
        }];
        
        [buttonBeast show];
    }
    
    // Если что-то изменилось в поиске, то очищаем view и ищем
    if ( ![lastSearch isEqualToString:_textFieldSearch.text] || [lastSearch isEqualToString:@""] || lastCategoryIdSearch != selectedCategoryId ) {
        
        // Показываем коллекцию в любом, потому что идёт загрузка желаний
        [UIView animateWithDuration:animation_duration animations:^{
            _tableView.layer.opacity = 0.0f;            
            _collectionView.layer.opacity = 1.0f;
        } completion:^(BOOL finished) {
            _collectionView.hidden = NO;
            _tableView.hidden = YES;
        }];
        
        [buttonBeast show];
        
        [_collectionView clean];
        isLoading = NO;
        [self loadItems];
    }
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.2f animations:^{
        _tableView.layer.opacity = 1.0f;
        _collectionView.layer.opacity = 0.0f;
    } completion:^(BOOL finished) {
        _tableView.hidden = NO;
        _collectionView.hidden = YES;
        buttonBeast.hidden = YES;
    }];
    
}


// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{    
    [_preload renderPreloaderDependsSuperView];
}

- (void)renderWishCell:(NSNotification *)notif
{
    [_collectionView reloadItem:notif.userInfo[@"wish"]];
}

- (void)prepareCollectionView
{
    [self.collectionView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    [self.collectionView.pullToRefreshView removeFromSuperview];
    //    self.collectionView.pullToRefreshView.delegate = (id<SSPullToRefreshViewDelegate>)self;
    self.collectionView.tapDelegate = (id<UICollectionViewWishDelegate>)self;
    self.collectionView.contentSize = self.collectionView.frame.size;
}

#pragma mark -
#pragma mark UICollectionViewWishDelegate

- (void)showUserDetail:(id)user
{
    [self performSegueWithIdentifier:@"userDetail" sender:user];
}

- (void)showWishDetail:(id)wish
{
    [self performSegueWithIdentifier:@"wishDetail" sender:wish];
}

- (void)showCommentDetail:(id)wish
{
    [self performSegueWithIdentifier:@"comments" sender:wish];
}

- (void)implementWish:(ModelWish *)wish
{
    [self performSegueWithIdentifier:@"executeWish" sender:wish];
}

- (void)rewish:(ModelWish *)wish
{
    wishInAction = wish;
    [[WishlistSelectViewController sharedInstance] showInView:self.view animated:YES];
}

- (void)deleteWish:(ModelWish *)wish
{
    wishInAction = wish;
    
    if ( [wish isMyWish] ) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[ActionTexts sharedInstance] titleText:@"WishDelete"]
                                                        message:[[ActionTexts sharedInstance] text:@"WishDelete"]
                                                       delegate:self
                                              cancelButtonTitle:@"Нет"
                                              otherButtonTitles:[[ActionTexts sharedInstance] titleText:@"Delete"], nil];
        [alert show];
    } else {
        [self tryDeleteWish];
    }
}

- (void)tryDeleteWish
{
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] deleteWish:[wishInAction getId] withCallBack:^(id item) {
        isLoading = NO;
        
        if ( [wishInAction isMyWish] ) {
            [_collectionView removeItem:wishInAction];
        } else {
            [_notification showNotify:2 withText:[[ActionTexts sharedInstance] successText:@"WishDelete"]];
            [wishInAction wishInWishlist:NO];
        }
        
    } failedBlock:^(id item) {
        wishInAction = nil;
        isLoading = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": item}];
    }];
}

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex;
{
    if ( buttonIndex == 1) {
        [self tryDeleteWish];
    }
}

#pragma mark -
#pragma mark PUViewProtocol

- (void)successPopUp:(uint32_t)wlistId
{
    if ( wishInAction == nil ) {return;}
    
    if ( isLoading ) { return; }
    isLoading = YES;
    
    [[WishesAPI sharedInstance] rewish:[wishInAction getId] toWishlistId:wlistId withCallBack:^(id item) {
        [wishInAction wishInWishlist];
        [_collectionView reloadItem:wishInAction];
        
        isLoading = NO;
        wishInAction = nil;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"2", @"text": [[ActionTexts sharedInstance] successText:@"WishRewish"]}];
    } failedBlock:^(id item) {
        NSString *errorText = ( item == nil || [item isEqualToString:@""] ) ? [[ActionTexts sharedInstance] failedText:@"WishImplement"] : item;
        
        wishInAction = nil;
        isLoading = NO;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"0", @"text": errorText}];
    }];
}


#pragma mark -
#pragma mark Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.identifier isEqualToString:@"comments"] ) {
        [Routing goToCommentsController:segue wishId:[sender getId] wishName:[sender getName] comments:nil maxCount:[sender getCommentsCount] replyToCommentId:0 replyToUserName:nil];
    } else {
        [Routing prepareForSegue:segue sender:sender];
    }
}

- (CGRect)prepareWishToOpen
{
    return [_collectionView prepareToOpenWish];
}

- (CGRect)prepareWishToClose
{
    return [_collectionView prepareToCloseWish];
}

- (UIView *)cellWishOpen
{
    return [_collectionView selectedWishCell];
}

- (void)cellWishClose
{
    [_collectionView deselectSelectedItem];
}

#pragma mark -
#pragma mark UICollectionViewHeaderDelegate

- (void)getWishesWithPopularStatus:(BOOL)status
{
    isNeedPopularWishes = status;
    [_collectionView clean];
    [_collectionView reloadData];
    [self searchItems];
}

#pragma mark -
#pragma mark UIScrollViewDelegate

#define heightTopView 65.0

// invoke from PinterestViewController
- (void)hideTopLeftButton:(BOOL)state
{
    if ( state ) {
        if ( _viewTop.frame.origin.y == 0.0 ) {
            [_viewTop layoutIfNeeded];
            [_viewWishes layoutIfNeeded];
            
            [UIView animateWithDuration:animation_duration animations:^{
                _viewTopMargin.constant = -heightTopView;
                [_viewTop layoutIfNeeded];
                [_viewWishes layoutIfNeeded];
            }];
        }
        
        return;
    }
    
    if ( _viewTop.frame.origin.y == -_viewTop.frame.size.height ) {
        [_viewTop layoutIfNeeded];
        [_viewWishes layoutIfNeeded];
        
        [UIView animateWithDuration:animation_duration animations:^{
            _viewTopMargin.constant = 0;
            
            [_viewTop layoutIfNeeded];
            [_viewWishes layoutIfNeeded];
        }];
    }
}


#pragma mark -
#pragma mark Keyboard


- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets;
    contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0);
    
    _tableView.contentInset = contentInsets;
    _tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _tableView.contentInset = UIEdgeInsetsZero;
}


#pragma mark -
#pragma mark SegueForUnwind
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    if ( [identifier isEqualToString:@"wishDetailUnwind"] ) {
        return [[SeguePinterestOpenUnwind alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
    }
    
    return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

- (IBAction)returnFromSegueActions:(UIStoryboardSegue *)sender
{
    
}


@end
