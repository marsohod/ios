//
//  LoginController.m
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LoginController.h"
#import "NSString+Helper.h"
#import "LoginAPI.h"
#import "UserAPI.h"
#import "Routing.h"
#import "Me.h"
#import "ModelUser.h"
#import "SegueFromLeftToRightUnwind.h"
#import "ActionTexts.h"
#import "MBProgressHUD.h"
#import "WishlistsAPI.h"


@interface LoginController ()
{

}
@end

@implementation LoginController

#define ICON_WIDTH_WITH_PADDING 28.0

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.notification.hidden = YES;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    [self.view addGestureRecognizer:tapView];
    
    // Устанавливаем иконки для полей
    [self.textFieldEmail setIcon:@"email" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-20.0 paddingLeft:0.0];
    [self.textFieldPassword setIcon:@"lock" toTheRightDirect:NO withMaxIconWidth:ICON_WIDTH_WITH_PADDING paddingTop:-8.0 paddingLeft:0.0];
    
    // Устанавливаем отступ справа от кнопки
    self.textFieldPassword.rightView = self.buttonForgotPassword;
    self.textFieldPassword.rightViewMode = UITextFieldViewModeAlways;
    
    // Биндим проверку полей по нажатию на кнопку SEND в поле пароля
    [self.textFieldPassword addTarget:self action:@selector(actionEnterButton:) forControlEvents:UIControlEventEditingDidEndOnExit];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.textFieldEmail becomeFirstResponder];
    self.screenName = @"Login By Email Screen";
}

// Делаем видимым сообщение об ошибке
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.notification.hidden = NO;
}

// Скрываем сообщение об ошибке
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.notification.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}


#pragma mark -
#pragma mark Navigation
// Вернуться к предыдущему контроллеру
- (IBAction)actionBackButton:(id)sender {
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if( [segue.identifier isEqualToString:@"forgotPassword"] ) {
        [Routing goToForgotPasswordCtrl: segue email:self.textFieldEmail.text];
    }
}


- (BOOL)isFormValid
{
    if( [self.textFieldEmail.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailNotEnter"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    } else if( ![NSString h_isEmailString:self.textFieldEmail.text] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmailBad"]];
        [self.textFieldEmail becomeFirstResponder];
        
        return NO;
        
    } else if( [self.textFieldPassword.text isEqualToString:@""] ) {
        
        [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"PasswordNotEnter"]];
        [self.textFieldPassword becomeFirstResponder];
        
        return NO;
    }
    
    return YES;
}

- (IBAction)actionEnterButton:(id)sender {
    if ( [self isFormValid] ) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [self actionLogin];
    }
}

- (void)actionLogin
{
    [[LoginAPI sharedInstance] login:self.textFieldEmail.text pass:self.textFieldPassword.text withCallBack:^(NSDictionary *response) {
        [[UserAPI sharedInstance] getMe:^(ModelUser *man) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [Me setProfileAfterLogin:man];

            if ( [Me needMoreSubscriptions] ) {
                [[Routing sharedInstance] showChannelRecomendationController];
                return;
            }
            
            [self performSegueWithIdentifier:@"welcome" sender:nil];
        } failedBlock:^(NSString *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.notification showNotify:0 withText:error];
        }];
    } withCallBackError:^(NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.notification showNotify:0 withText:error];
    }];
}

- (IBAction)actionEnterEmailDidEnd:(id)sender
{
    [_textFieldPassword becomeFirstResponder];
}
@end
