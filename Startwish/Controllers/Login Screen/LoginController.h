//
//  LoginController.h
//  Startwish
//
//  Created by marsohod on 25/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "BackButton.h"
#import "OrangeButton.h"
#import "CustomTextField.h"
#import "NotificationLabel.h"
#import "GAITrackedViewController.h"


@interface LoginController : GAITrackedViewController

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionEnterButton:(id)sender;
- (IBAction)actionEnterEmailDidEnd:(id)sender;

@property (weak, nonatomic) IBOutlet CustomTextField *textFieldEmail;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldPassword;
@property (weak, nonatomic) IBOutlet UIButton *buttonForgotPassword;
@property (weak, nonatomic) IBOutlet OrangeButton *buttonSendPassword;
@property (weak, nonatomic) IBOutlet UILabel *labelForgotPassword;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

@end
