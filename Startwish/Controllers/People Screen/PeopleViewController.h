//
//  PeopleViewController.h
//  Startwish
//
//  Created by marsohod on 23/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "CustomTextField.h"
#import "PeopleTableView.h"
#import "BackButton.h"
#import "MenuButton.h"
#import "PreloadView.h"
#import "GAITrackedViewController.h"
#import "EmptyView.h"
#import "NotificationLabel.h"


@interface PeopleViewController : GAITrackedViewController
{
    NSInteger peopleType;
    uint32_t userId;
    NSString *userName;
    BOOL isRequestFromMenu;
}

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionMenuButton:(id)sender;

@property (strong, nonatomic) PreloadView *preload;
@property (weak, nonatomic) IBOutlet CustomTextField *textFieldSearch;
@property (weak, nonatomic) IBOutlet PeopleTableView *tableView;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *items_filtered;
@property (weak, nonatomic) IBOutlet BackButton *buttonBack;
@property (weak, nonatomic) IBOutlet MenuButton *buttonMenu;
@property (strong, nonatomic) EmptyView *viewEmpty;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

- (IBAction)actionChangeSearchTextField:(id)sender;
- (IBAction)actionChangeDidEnd:(id)sender;

- (void)peopleType:(NSInteger)type byUserId:(uint32_t)u_id;
- (void)peopleType:(NSInteger)type byUserId:(uint32_t)u_id fromMenu:(BOOL)state;

@end
