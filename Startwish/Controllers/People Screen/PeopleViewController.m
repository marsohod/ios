//
//  PeopleViewController.m
//  Startwish
//
//  Created by marsohod on 23/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "PeopleViewController.h"
#import "PeopleTableViewCell.h"
#import "UIScrollView+BottomRefreshControl.h"
#import "UserAPI.h"
#import "Me.h"
#import "UIColor+Helper.h"
#import "Routing.h"
#import "ModelUser.h"
#import "RevealViewController.h"
#import "ActionTexts.h"


@interface PeopleViewController ()
{
    BOOL isLoading;
    BOOL isOpeningUser;
    BOOL isNotAvailable;
}
@end

@implementation PeopleViewController

static void(^prepareItemsBlock)(NSArray *people);
static void(^failedGetItemsBlock)();


// используется 2 заглушки во время загрузки данных
// 1 зверёк на белом фоне при открытии контроллера
// 2 BottomRefreshControl для обновления по скроллу вниз

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setCallBackBlocks:NO];
    
    _viewEmpty = [[EmptyView alloc] initToView:self.view withText:[self textForEmptyView] desc:[self descForEmptyView] buttonTitle:@"найти друзей"];
    _viewEmpty.edelegate = (id<EmptyViewDelegate>)self;
    _viewEmpty.needMenuButton = isRequestFromMenu;
    
    [self.view insertSubview:_viewEmpty belowSubview:_notification];
    
    _buttonBack.hidden = isRequestFromMenu;
    _buttonMenu.hidden = !isRequestFromMenu;
    
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    
    tapView.cancelsTouchesInView = NO;
    
    [self.view setBackgroundColor: [UIColor h_smoothGreen]];

    _tableView.cellDelegate = (id<PeopleTableViewDelegate>)self;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_tableView.bottomRefreshControl addTarget:self action:@selector(loadItems) forControlEvents:UIControlEventValueChanged];
    [_textFieldSearch setIcon:@"" toTheRightDirect:NO withMaxIconWidth:0];
    
    self.preload = [[PreloadView alloc] init];
    [self.view addSubview:self.preload];

    [_preload showPreload];
    
    _textFieldSearch.hidden = peopleType > 4;
    _tableView.cellType = peopleType > 4 ? 1 : 0;
    [self setTitleForNavigationItem];
    [self loadItems];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setCallBackBlocks:NO];
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if( indexPath ) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    
    NSString *peopleTypeString;
    
    switch (peopleType) {
        case 0:
            peopleTypeString = @"Friends";
            break;
            
        case 1:
            peopleTypeString = @"Followers";
            break;
            
        case 2:
            peopleTypeString = @"Follows";
            break;
            
        case 3:
            peopleTypeString = @"Friends Mutual";
            break;
            
        case 4:
            peopleTypeString = @"Search";
            break;
            
        case 10:
            peopleTypeString = @"Friends Search VK";
            break;
            
        case 11:
            peopleTypeString = @"Friends Search FB";
            break;
            
        case 12:
            peopleTypeString = @"Friends Search Contacts";
            break;
            
        default:
            break;
    }
    
    self.screenName = [NSString stringWithFormat: @"People %@ Screen", peopleTypeString];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ( _buttonMenu.hidden == NO ) {
        [_buttonMenu updateEventCount:nil];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setCallBackBlocks:YES];
}

// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}

- (void)matchingItemsOnly:(BOOL)isOnly
{
    NSMutableArray *itemsWithRange = [NSMutableArray array];
    
    [self.items_filtered removeAllObjects];
    
    [self.items enumerateObjectsUsingBlock:^(ModelUser* obj, NSUInteger idx, BOOL *stop) {
        NSRange searchRange = [[obj getFullname] rangeOfString: self.textFieldSearch.text options:NSCaseInsensitiveSearch];
        NSDictionary *rangeItem = @{@"range": [NSValue valueWithRange:searchRange] , @"item": obj};
        [itemsWithRange addObject: rangeItem];
        
        if ( isOnly ) {
            if ( searchRange.length > 0 ) {
                [self.items_filtered addObject: rangeItem];
            }
        } else {
            [self.items_filtered addObject: rangeItem];
        }
        
    }];
    
    if ( [self.items_filtered count] == 0 && [_textFieldSearch.text isEqualToString:@""] ) {
        self.items_filtered = itemsWithRange;
    }
    
}

- (void)setCallBackBlocks:(BOOL)isEmpty
{
    if ( !isEmpty ) {
        isLoading = NO;
        
        // Обработка массива людей после того, как их получили
        prepareItemsBlock = ^(NSArray *people){
            isLoading = NO;
            [_tableView handleEndRefreshing];
    //        [_indicator stopAnimating];
            
            if( [people count] == 0 ) {
                isNotAvailable = YES;
                [self.preload hidePreload];
                
                // show empty view if no one people exist
                if (self.tableView.currentPage == 0 && [_textFieldSearch.text isEqualToString:@""]) {
                    [_viewEmpty show:YES withButton:[Me isMyProfile:userId]];
                }
                
                return;
            }
            
            if( self.items == nil ) {
                self.items = [[NSMutableArray alloc] initWithArray:people];
            } else {
                
                // если новый запрос на поиск
                if ( [_tableView currentPage] == 0 ) {
                    [self.items removeAllObjects];
                    //                [_tableView updateWithItems:@[]];
                }
                
                [self.items addObjectsFromArray: people];
            }
            
            if( self.items_filtered == nil ) {
                self.items_filtered = [[NSMutableArray alloc] init];
            } else {
                [self.items_filtered removeAllObjects];
            }
            
            // Если поиска нет, то передаём всех вытащенных друзей
            // Иначе ищем совпадения в имени пользователя по строке поиска
            if( [_textFieldSearch.text isEqualToString: @""] ) {
                [self addItemsFiltered];
            } else {
                [self matchingItemsOnly:NO];
            }
            
            [self.preload hidePreload];
            
            [self.tableView updateWithItemsIncludeOldItems: self.items_filtered animation:YES];
        };
        
        failedGetItemsBlock = ^(){
            isLoading = NO;
            [_tableView handleEndRefreshing];
            [self.preload hidePreload];
        };
        
        return;
    }
    
    prepareItemsBlock = nil;
    failedGetItemsBlock = nil;
}

- (void)addItemsFiltered
{
    [self.items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.items_filtered addObject: @{@"range": [NSValue valueWithRange:((NSRange){0,0})] , @"item": obj}];
    }];
}

- (IBAction)actionChangeSearchTextField:(id)sender {
    [self matchingItemsOnly:YES];
    [_tableView removeAllObjects];
    [_tableView updateWithItems:_items_filtered animation:NO];
}

- (IBAction)actionChangeDidEnd:(id)sender
{
    if ( isOpeningUser ) {
        isOpeningUser = NO;
        return;
    }
    
    [self.tableView clean];
    
    isNotAvailable = NO;
    isLoading = NO;
    
    // Если поиска нет, то передаём всех вытащенных друзей
    // Иначе ищем совпадения в имени пользователя по строке поиска
    if( [_textFieldSearch.text isEqualToString: @""] ) {
        [self addItemsFiltered];
    } else {
        _tableView.contentOffset = CGPointMake(0, 10);
    }
    
    // Перед поиском удалённо нам надо обнулить page таблицы
    [self.tableView cleanPage];
    
    [self loadItems];
}


- (void)loadItems
{
    if ( isLoading ) { return; }
    if ( isNotAvailable ) {
        [_tableView handleEndRefreshing];
        return;
    }
    
    isLoading = YES;
    [_tableView handleBeginRefreshing];
    [_viewEmpty hide];
    
    switch (peopleType) {
        case 0: {
            [[UserAPI sharedInstance] searchFriendsFromUserId:userId byName:_textFieldSearch.text withCallback:prepareItemsBlock withPage:[_tableView currentPage] failed:failedGetItemsBlock];
            }
            break;
            
        case 1: {
                [[UserAPI sharedInstance] searchFollowersFromUserId:userId byName:_textFieldSearch.text withCallback:prepareItemsBlock withPage:[_tableView currentPage] failed:failedGetItemsBlock];
            }
            break;
            
        case 2: {
                [[UserAPI sharedInstance] searchFollowsFromUserId:userId byName:_textFieldSearch.text withCallback:prepareItemsBlock withPage:[_tableView currentPage] failed:failedGetItemsBlock];
            }
            break;
            
        case 3: {
            [[UserAPI sharedInstance] searchMutualFriendsFromUserId:userId byName:_textFieldSearch.text withCallback:prepareItemsBlock withPage:[_tableView currentPage] failed:failedGetItemsBlock];
            break;
        }
            
        case 4: {
            if ( [_textFieldSearch.text isEqualToString:@""] ) {
                [[UserAPI sharedInstance] searchActivePeopleByName:_textFieldSearch.text withPage:[_tableView currentPage] withCallback:prepareItemsBlock failed:failedGetItemsBlock];
            } else {
                [[UserAPI sharedInstance] searchPeopleByName:_textFieldSearch.text withPage:[_tableView currentPage] withCallback:prepareItemsBlock failed:failedGetItemsBlock];
            }
            break;
        }
            
        case 10: {
            // Делаем только 1 запрос для всех контактов
            if ( [_items_filtered count] == 0 ) {
                [[UserAPI sharedInstance] getFriendsVK:prepareItemsBlock failedBlock:^(id item) {
                    [_tableView handleEndRefreshing];
                    
                    // show empty view if no one people exist
                    if (self.tableView.currentPage == 0 && [_textFieldSearch.text isEqualToString:@""]) {
                        [_viewEmpty show:YES withButton:[Me isMyProfile:userId]];
                    }
                }];
                
                return;
            }
            
            [_tableView handleEndRefreshing];
            break;
        }
            
        case 11: {
            if ( [_items_filtered count] == 0 ) {
                /*[[UserAPI sharedInstance] searchFriendsFB:[FacebookHandler token] withCallback:prepareItemsBlock failedBlock:^(id item) {
                    [_tableView handleEndRefreshing];
                }];*/
                
                return;
            }

            [_tableView handleEndRefreshing];
            break;
        }
            
        case 12: {
            if ( [_items_filtered count] == 0 ) {
                [[UserAPI sharedInstance] searchFriendsContacts:prepareItemsBlock failedBlock:^(id item) {
                    [_tableView handleEndRefreshing];
                    
                    // show empty view if no one people exist
                    if (self.tableView.currentPage == 0 && [_textFieldSearch.text isEqualToString:@""]) {
                        [_viewEmpty show:YES withButton:[Me isMyProfile:userId]];
                    }
                }];
                
                return;
            }
            
            [_tableView handleEndRefreshing];
            break;
        }
            
        default:
            break;
    }
}

- (void)peopleType:(NSInteger)type byUserId:(uint32_t)u_id fromMenu:(BOOL)state
{
    peopleType = type;
    userId = u_id;
    isRequestFromMenu = state;
}

- (void)peopleType:(NSInteger)type byUserId:(uint32_t)u_id
{
    [self peopleType:type byUserId:u_id fromMenu:NO];
}

- (void)setTitleForNavigationItem
{
    NSString *placeholder;
    
    switch (peopleType) {
        case 0:
            placeholder = @"поиск друзей";
            break;
            
        case 1:
            placeholder = @"поиск подписчиков";
            break;
            
        case 2:
            placeholder = @"поиск подписок";
            break;
            
        case 4:
            placeholder = @"поиск людей";
            break;
        
        case 10:
            placeholder = @"поиск друзей ВК";
            break;
            
        case 11:
            placeholder = @"поиск друзей FB";
            break;
            
        case 12:
            placeholder = @"поиск друзей";
            break;
            
        default:
            break;
    }
    
    [_textFieldSearch customSetPlaceholder: placeholder];
}

- (NSString *)textForEmptyView
{
    NSString *userType = [Me isMyProfile:userId] ? @"вас" : @"пользователя";
    NSString *type;
    NSString *searchFriendsBySocial = @"Друзья не найдены";
    
    switch (peopleType) {
        case 0:
            type = @"друзей";
            break;
            
        case 1:
            type = @"подписчиков";
            break;
            
        case 2:
            type = @"подписок";
            break;
            
        case 3:
            type = @"c вами общих друзей";
            break;
            
        case 10:
            return searchFriendsBySocial;
            
        case 11:
            return searchFriendsBySocial;
            
        case 12:
            return searchFriendsBySocial;
            
        default:
            break;
    }
    
    
    return [NSString stringWithFormat:@"У %@ пока нет \r %@.", userType, type];
}

- (NSString *)descForEmptyView
{
    return [Me isMyProfile:userId] ? [[ActionTexts sharedInstance] titleText:@"EmptyFriends"] : nil;
}

#pragma mark -
#pragma mark PeopleTableViewDelegate

- (void)showUser:(id)user
{
    isOpeningUser = YES;
    [[Routing sharedInstance] showUserControllerWithUser:user fromMenu:NO];
}


#pragma mark -
#pragma mark PeopleTableViewCellDelegate
- (void)subscribeToUser:(uint32_t)uId
{
    [[UserAPI sharedInstance] subscribe:YES toUserId:uId withCallBack:^(id item) {} failedBlock:^(id item) {}];
}

- (void)unsubscribeFromUser:(uint32_t)uId
{
    [[UserAPI sharedInstance] subscribe:NO toUserId:uId withCallBack:^(id item) {} failedBlock:^(id item) {}];
}


#pragma mark -
#pragma mark Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [Routing prepareForSegue:segue sender:sender];
}

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

#pragma mark - EmptyViewDelegate
- (IBAction)actionButton:(id)sender {
    [[Routing sharedInstance] showPeopleSearchCtrlFromMenu:NO];
}



@end
