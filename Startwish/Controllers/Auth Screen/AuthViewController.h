//
//  AuthViewController.h
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "OrangeButton.h"
#import "RevealViewController.h"
#import "GAITrackedViewController.h"
#import "NotificationLabel.h"


@interface AuthViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIView         *viewTopWebView;
@property (weak, nonatomic) IBOutlet UIWebView      *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *webLoadIndicator;
@property (weak, nonatomic) IBOutlet OrangeButton   *buttonEnterFromVK;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingCircleToTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingTitleToTop;
@property (weak, nonatomic) RevealViewController      *revealController;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;

- (IBAction)actionButtonVK:(id)sender;
- (IBAction)actionShowTermsAndPrivacy:(id)sender;
- (IBAction)actionCloseWebview:(id)sender;

@end
