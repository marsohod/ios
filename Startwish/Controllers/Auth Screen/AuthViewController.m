//
//  AuthViewController.m
//  Startwish
//
//  Created by marsohod on 24/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "AuthViewController.h"
#import "ActionTexts.h"
#import "EnterEmailViewController.h"
#import "LoginAPI.h"
#import "MBProgressHUD.h"
#import "Me.h"
#import "NSString+Helper.h"
#import "Routing.h"
#import "SegueFromLeftToRightUnwind.h"
#import "Settings.h"
#import "SettingsAPI.h"
#import "UIScreen+Helper.h"
#import "UserAPI.h"
#import "VKHandler.h"
#import "WishlistsAPI.h"


@interface AuthViewController ()
{
    NSMutableDictionary *userVK;
}
@end

@implementation AuthViewController

#define ICON_PADDING 5.0
#define ANIMATION_DURATION 0.5

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ( self.revealController ) {
        self.revealController = nil;
    }
    
    [[LoginAPI sharedInstance] cleanProfile];

    [self renderButtonEnterVK];
    [self renderViewsForCurrentDevice];
    
    _webView.delegate = (id<UIWebViewDelegate>)self;

    [_buttonEnterFromVK hideUnderline];
    
    [VKHandler sharedInstance].ctrl = self;
    [VKHandler sharedInstance].handlerDelegate = (id<VKHandlerDelegate>)self;
    [[VKHandler sharedInstance] wakeUpSession];
    userVK = [NSMutableDictionary dictionary];
    
    [EnterEmailViewController sharedInstance].delegate = (id<PUViewEnterProtocol>)self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Auth Select Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.revealViewController.panGestureRecognizer.enabled = NO;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)renderButtonEnterVK
{   
    [self.buttonEnterFromVK.titleLabel setTintAdjustmentMode:UIViewTintAdjustmentModeNormal];
    UIImage *vk = [[UIImage imageNamed:@"vk_white.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [self.buttonEnterFromVK setImage: vk forState: UIControlStateNormal];
    [self.buttonEnterFromVK setImage: vk forState: UIControlStateHighlighted];
    [self.buttonEnterFromVK setImage: vk forState: UIControlStateApplication];
    [self.buttonEnterFromVK setImage: vk forState: UIControlStateSelected];

    
    [self.buttonEnterFromVK setTintColor: [UIColor whiteColor]];
    [self.buttonEnterFromVK setTitleEdgeInsets: UIEdgeInsetsMake(0.0, -self.buttonEnterFromVK.imageView.frame.size.width - ICON_PADDING, 0.0, self.buttonEnterFromVK.imageView.frame.size.width + ICON_PADDING)];
    [self.buttonEnterFromVK setImageEdgeInsets: UIEdgeInsetsMake(0.0, self.buttonEnterFromVK.titleLabel.frame.size.width + ICON_PADDING, 0.0, -self.buttonEnterFromVK.titleLabel.frame.size.width - ICON_PADDING)];
}

- (void)renderViewsForCurrentDevice
{
    if ( [UIScreen isIphone4] ) {
        _paddingCircleToTop.constant = 10.0;
        _paddingTitleToTop.constant = 10.0;
    } else if ( [UIScreen isIphone6] ) {
        _paddingCircleToTop.constant = 85.0;
    } else if ( [UIScreen isIphone6Plus] ) {
        _paddingCircleToTop.constant = 110.0;
    }
    
    [self.view setNeedsLayout];
}

- (IBAction)actionButtonVK:(id)sender
{
    if ( [VKHandler canOpenApp] ) {
        [[VKHandler sharedInstance] auth];
        return;
    }
    
    [[VKHandler sharedInstance] authToStartwish];
}

- (IBAction)actionShowTermsAndPrivacy:(id)sender
{
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@tos?app=1", [Settings sharedInstance].domain]]]];
    _webView.scalesPageToFit = NO;
    [_webLoadIndicator startAnimating];
    
    [self showWebView];
}

- (IBAction)actionCloseWebview:(id)sender
{
    [self hideWebView];
    [_webView stopLoading];
}


#pragma mark - Error

- (void)showErrorLoginVK:(NSString *)text
{
    [_notification showNotify:0 withText:text];
    [[VKHandler sharedInstance] logout];
}
- (void)showErrorLoginVK
{
    [[VKHandler sharedInstance] logout];
    [self showErrorLoginVK:[[ActionTexts sharedInstance] text:@"VKAuthDeny"]];
}


#pragma mark - VKHandlerDelegate
- (void)allowAccess
{
    [userVK removeAllObjects];
    userVK[@"identifier"]   = [VKHandler userIdString];
    userVK[@"token"]        = [VKHandler accessToken];
    
    [[VKHandler sharedInstance] getUser:^(NSDictionary *user) {
        [userVK addEntriesFromDictionary:user];
        
        [MBProgressHUD showHUDAddedTo: self.view animated:YES];
        [self tryEnterToStartwish];
        
    } failed:^(NSString *error) {
        if ( ![VKHandler email] ) {
            [[EnterEmailViewController sharedInstance] showInView:self.view animated:YES];
            return;
        }
        
        userVK[@"login"]    = [VKHandler email];
        userVK[@"username"] = @"NoName";
        
        [MBProgressHUD showHUDAddedTo: self.view animated:YES];
        [self tryEnterToStartwish];
    }];
}

- (void)denyAccess
{
    [self showErrorLoginVK:[[ActionTexts sharedInstance] failedText:@"VKAuthDeny"]];
}


#pragma mark -

- (void)tryEnterToStartwish
{
    [[LoginAPI sharedInstance] enterVK:userVK[@"token"] user:userVK withCallBack:^(NSDictionary *response) {
        
        [[UserAPI sharedInstance] getMe:^(ModelUser *man) {
            [MBProgressHUD hideHUDForView: self.view animated:NO];
            
            [Me setProfileAfterLogin:man];
            
            if ( [Me needMoreSubscriptions] ) {
                [[Routing sharedInstance] showChannelRecomendationController];
                return;
            }
            
            [[EnterEmailViewController sharedInstance] removeAnimate];
            
            [self performSegueWithIdentifier:@"welcome" sender:nil];
        } failedBlock:^(NSString *error) {
            [MBProgressHUD hideHUDForView: self.view animated:NO];
            [[EnterEmailViewController sharedInstance] removeAnimate];
            [self.notification showNotify:0 withText:error];
        }];
        
    } withCallBackError:^(NSString *error) {

        if( [error rangeOfString:@"http"].location != NSNotFound ) {
            [MBProgressHUD hideHUDForView: self.view animated:NO];
            [self.notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"VKAccess"]];
            return;
        }
        
        
        if ( !userVK[@"login"] ) {
            [MBProgressHUD hideHUDForView:[EnterEmailViewController sharedInstance].view animated:YES];
            [[EnterEmailViewController sharedInstance] showInView:self.view animated:YES];
            return;
        }
        
        [MBProgressHUD hideHUDForView: self.view animated:NO];
        [[EnterEmailViewController sharedInstance] removeAnimate];
        
        [self.notification showNotify:0 withText:error];
    }];
}

#pragma mark - PUViewEnterProtocol

- (void)successPopUp:(NSString *)email
{
    [MBProgressHUD showHUDAddedTo: [EnterEmailViewController sharedInstance].view animated:YES];
    
    userVK[@"login"] = email;
    [self tryEnterToStartwish];
}

- (void)cancelPopUp
{
    [MBProgressHUD hideHUDForView: self.view animated:NO];
    [[EnterEmailViewController sharedInstance] removeAnimate];
    [[VKHandler sharedInstance] logout];
}


#pragma mark -
#pragma mark UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_webLoadIndicator stopAnimating];
    
    NSString *token = [VKHandler searchTokenInUrl:_webView.request.URL.absoluteString];
    
    if ( token ) {
        [self hideWebView];
        userVK[@"token"] = token;
        [self tryEnterToStartwish];
    }
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}


#pragma mark -
#pragma mark WebView
- (void)showWebView
{
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        [_webView setAlpha:1.0];
        [_viewTopWebView setAlpha:1.0];
        _webView.hidden = NO;
        _viewTopWebView.hidden = NO;
    }];
}

- (void)hideWebView
{
    [UIView animateWithDuration:ANIMATION_DURATION animations:^{
        [_webView setAlpha:0.0];
        [_viewTopWebView setAlpha:0.0];

        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    } completion:^(BOOL finished) {
        _webView.hidden = YES;
        _viewTopWebView.hidden = YES;
    }];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    
    return YES;
}


@end
