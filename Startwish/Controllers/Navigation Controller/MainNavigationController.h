//
//  MainNavigationController.h
//  Startwish
//
//  Created by Pavel Makukha on 16/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "RevealViewController.h"


@interface MainNavigationController : UINavigationController <UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic) BOOL isSetPan;
@property (nonatomic) NSTimer *timerEventsUpdate;

- (void)willEnableGestureBack:(BOOL)enable;
- (void)removeGesture;

@end
