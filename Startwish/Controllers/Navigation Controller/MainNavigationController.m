//
//  MainNavigationController.m
//  Startwish
//
//  Created by Pavel Makukha on 16/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "MainNavigationController.h"
#import "Me.h"
#import "UserAPI.h"
#import "LoginAPI.h"
#import "Routing.h"
#import "PushNotificationHandler.h"



@interface MainNavigationController()
{
    BOOL isLoggin;
}

@end

@implementation MainNavigationController

- (id)init
{
    self = [super init];
//    [UIApplication sharedApplication].delegate
    
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [Routing sharedInstanceWithNavigationController:self];
    _isSetPan = NO;
    
    [self setGesture];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToAuthController) name:@"STNeedAuthUser" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(preparePush:) name:@"STPushIsReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTimerUpdates) name:@"STLoggedUser" object:nil];
    
    isLoggin = [[LoginAPI sharedInstance] isLogged];
    
    if ( isLoggin ) {
        [self setTimerUpdates];
    }
    
    [self setRootViewController];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ( !_isSetPan ) {
        [self setGesture];
    }
}

- (void)viewDidLayoutSubviews
{
    if ( !_isSetPan ) {
        [self setGesture];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setGesture
{
    __weak id s = self;
    
    SWRevealViewController *revealController = [self revealViewController];
    
    if ( revealController.panGestureRecognizer ) {
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = NO;
        
        self.topViewController.view.userInteractionEnabled = YES;

        revealController.delegate = s;
        [self.view addGestureRecognizer:revealController.panGestureRecognizer];
        _isSetPan = YES;
    }
}

- (void)removeGesture
{
    SWRevealViewController *revealController = [self revealViewController];
    
    if ( revealController.panGestureRecognizer ) {
        [self.view removeGestureRecognizer:revealController.panGestureRecognizer];
    }
}

#pragma mark - SWRevealViewController Delegate Methods

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionRight) {               // Menu will get revealed
        self.tapGestureRecognizer.enabled = YES;                 // Enable the tap gesture Recognizer
        self.interactivePopGestureRecognizer.enabled = NO;        // Prevents the iOS7's pan gesture
        self.topViewController.view.userInteractionEnabled = NO;       // Disable the topViewController's interaction
    }
    else if (position == FrontViewPositionLeftSide || position == FrontViewPositionLeft){      // Menu will close
        self.tapGestureRecognizer.enabled = NO;
        self.interactivePopGestureRecognizer.enabled = YES;
        self.topViewController.view.userInteractionEnabled = YES;
    }
}

- (void)updateEventsCount
{
    [[UserAPI sharedInstance] newEventsCount:^(NSArray *items) {
        if ( [UIApplication sharedApplication].applicationIconBadgeNumber != [items count] ) {
            [UIApplication sharedApplication].applicationIconBadgeNumber = [items count];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil];
        }
        
//        [self updateEventsCount];
    } failedBlock:^(id item) {
//        [self updateEventsCount];
    }];
}

// Перехватывает событие и посылает его предпоследнему контроллеру для segueForUnwindingToViewController
// Для анимации открытия желания
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
    NSArray *controllers = [self viewControllers];
    UIViewController *controller;
    
    if ( [controllers count] > 1 ) {
        controller = controllers[[controllers count] - 2];
    } else {
        controller = [controllers lastObject];
    }
    
    return [controller segueForUnwindingToViewController:toViewController
                                      fromViewController:fromViewController
                                              identifier:identifier];
}

- (void)setTimerUpdates
{
    if ( _timerEventsUpdate ) {
        [_timerEventsUpdate invalidate];
        _timerEventsUpdate = nil;
    }
    
    _timerEventsUpdate = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateEventsCount) userInfo:nil repeats:YES];
    [self updateEventsCount];
}

- (void)goToAuthController
{
    [_timerEventsUpdate invalidate];
    _timerEventsUpdate = nil;
    [[Routing sharedInstance] showAuthController];
}

- (void)preparePush:(NSNotification *)notif
{
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    
    [PushNotificationHandler prepareControllerToPush:notif.userInfo[@"push"]];
}

- (void)setRootViewController
{
//    [[LoginAPI sharedInstance] logout];
    if ( !isLoggin ) {
        [self goToAuthController];
        return;
    }
    
    if ( [Me needMoreSubscriptions] ) {
        [[Routing sharedInstance] showChannelRecomendationControllerFromStart:YES];
        return;
    }
    
    [[Routing sharedInstance] showFeedController];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [Routing prepareForSegue:segue sender:sender];
}

- (void)willEnableGestureBack:(BOOL)enable
{
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = enable;
    }
}

@end
