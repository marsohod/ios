//
//  WelcomeViewController.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WelcomeViewController.h"
#import "UserAPI.h"
#import "RevealViewController.h"
#import "UIView+Animations.h"
#import "UIScreen+Helper.h"
#import "NSString+Helper.h"
#import "Routing.h"


@interface WelcomeViewController ()
{
    NSArray *items;
    BOOL isTimerFired;
    BOOL isFeedShowing;
}
@end

@implementation WelcomeViewController

// время, которое точно будет крутиться кругляшь
#define ANIMATION_APPROVED_TIEM 1.5

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)viewDidLayoutSubviews
{
    // Выставляем вращающийся кругляшь прямо по середине
    // с равными отступами
    // между жёлтым тайтлом и нижним лого
    [self setFrameToRainbow];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // Устанавливаем таймер на выполение проверки загрузки желаний через время ANIMATION_APPROVED_TIEM
    [NSTimer scheduledTimerWithTimeInterval:ANIMATION_APPROVED_TIEM
                                     target:self
                                   selector:@selector(showFeedControllerIfItemsAlreadyLoaded)
                                   userInfo:nil
                                    repeats:NO];
    // Добавляем анимацию
    [self setAnimationToRainbow];
    [self setAnimationToBeast];
    
    // Параллельно с анимацией получаем желания
    [self loadItems];
    
    self.screenName = @"Welcome Screen";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *name;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    name = [[userDefaults objectForKey:@"userName"] componentsSeparatedByString:@" "];
    _labelWelcome.text = [NSString stringWithFormat: @"Добро пожаловать,\r%@", [name count] > 0 ? name[0] : @""];
    [self renderViewsForCurrentDevice];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STLoggedUser" object:nil];
}


- (void)renderViewsForCurrentDevice
{
    if ( [UIScreen isIphone4] ) {
        _paddingTitleToTop.constant = 20.0;
    } else if ( [UIScreen isIphone6] ) {

    } else if ( [UIScreen isIphone6Plus] ) {

    }
    
    [self.view setNeedsLayout];
}

- (void)setAnimationToRainbow
{
    // Добавляем анимацию для радужного круга.
    // Используем CATransaction для установки callback блока
    [CATransaction begin];
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 10.0 * 2.0 ];
    rotationAnimation.duration = 10.0;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = 1;
    
    // Callback после окончания анимации
    // Если желания уже загрузились, то хватит пялиться на крутилку
    // Переходим в другой экран
    [CATransaction setCompletionBlock:^{
        
        // Только если желания не загружены - переходим в контроллер
        // Если желания уже были загружены, то переход осуществился ранее.
        if ( items == nil || [items count] == 0 ) {
            [self showFeedController];
        }
    }];
    
    [self.imageViewRainbow.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    [CATransaction commit];
}

- (void)setAnimationToBeast
{
    [_imageViewBeast a_heartStop:&isFeedShowing];
}


- (void)setFrameToRainbow
{
    // Получаем отступ от жёлтого верхнего тайтла
    float paddingYFromLabelTitle = ( self.imageLogo.frame.origin.y - ( self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height ) - self.imageViewRainbow.frame.size.height ) / 2;
    
    // Получаем позицию Y относительно экрана для радужного круга
    float imageOriginY = self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height + paddingYFromLabelTitle;
    
    CGRect rainbowFrame = CGRectMake(self.imageViewRainbow.frame.origin.x, imageOriginY, self.imageViewRainbow.frame.size.width, self.imageViewRainbow.frame.size.height);
    
    self.imageViewRainbow.frame = rainbowFrame;
    self.imageViewCircle.frame = rainbowFrame;
    
    // Не забываем про зверька и устанавливаем его в центр круга
    CGRect beastFrame = CGRectMake(self.imageBeast.frame.origin.x, self.imageViewCircle.center.y - self.imageBeast.frame.size.height / 2, self.imageBeast.frame.size.width, self.imageBeast.frame.size.height);
    
    self.imageBeast.frame = beastFrame;
}

- (void)showFeedControllerIfItemsAlreadyLoaded {
    isTimerFired = YES;
    
    if ( items != nil ) {
        [self showFeedController];
    }
}


- (void)loadItems
{
    [[UserAPI sharedInstance] feedsBeforeUpdate:[NSString h_dateNow] withCallBack:^(NSArray *newWishes) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STLoadFirstFeed" object:nil userInfo:@{@"items": newWishes}];
        
        items = [[NSArray alloc] initWithArray:newWishes];
        
        if ( isTimerFired == YES ) {
            [self showFeedController];
        }
        
    } withFailedCallBack:^(id item) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STLoadFirstFeed" object:nil userInfo:@{@"items": @[]}];
    }];
}


- (void)showFeedController
{
    if ( isFeedShowing ) {
        return;
    }
    
    isFeedShowing = YES;
    [[Routing sharedInstance] showFeedController:items andHideModal:YES];
}

@end
