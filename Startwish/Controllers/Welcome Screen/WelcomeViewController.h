//
//  WelcomeViewController.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TitleLabel.h"
#import "HeaderLabel.h"
#import "GAITrackedViewController.h"


@interface WelcomeViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIImageView        *imageViewRainbow;
@property (weak, nonatomic) IBOutlet UIImageView        *imageLogo;
@property (weak, nonatomic) IBOutlet TitleLabel         *labelTitle;
@property (weak, nonatomic) IBOutlet HeaderLabel        *labelWelcome;
@property (weak, nonatomic) IBOutlet UIImageView        *imageViewCircle;
@property (weak, nonatomic) IBOutlet UIImageView        *imageBeast;
@property (weak, nonatomic) IBOutlet UIImageView        *imageViewBeast;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *paddingTitleToTop;

@end
