//
//  RegisterDetailViewController.h
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIViewAvatar.h"
#import "CustomTextField.h"
#import "CustomDataPicker.h"
#import "OrangeMiddleButton.h"
#import "LabelLikeTextField.h"
#import "SegmentControlSettings.h"
#import "SevenSwitch.h"
#import "MenuButton.h"
#import "BackButton.h"
#import "NotificationLabel.h"
#import "CustomScrollView.h"
#import "GAITrackedViewController.h"
#import "RedButton.h"


@interface SettingsViewController : GAITrackedViewController
- (IBAction)actionMenuButton:(id)sender;
- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionSendProfile:(id)sender;
- (IBAction)actionExitButton:(id)sender;
- (IBAction)actionRemoveCityButton:(id)sender;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint         *heightViewForPickerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint         *scrollViewBottomPadding;
@property (weak, nonatomic) IBOutlet NotificationLabel          *notification;
@property (weak, nonatomic) IBOutlet MenuButton                 *buttonMenu;
@property (weak, nonatomic) IBOutlet BackButton                 *buttonBack;
@property (weak, nonatomic) IBOutlet UIViewAvatar               *viewAvatar;
@property (weak, nonatomic) IBOutlet UILabel                    *labelAvatar;
@property (weak, nonatomic) IBOutlet CustomTextField            *textFieldUserName;
@property (weak, nonatomic) IBOutlet CustomTextField            *textFieldUserSurname;
@property (weak, nonatomic) IBOutlet CustomTextField            *textFieldUserDomain;
@property (weak, nonatomic) IBOutlet LabelLikeTextField         *labelCity;
@property (weak, nonatomic) IBOutlet UIButton                   *buttonCityRemove;
@property (weak, nonatomic) IBOutlet LabelLikeTextField         *labelBirthday;
@property (weak, nonatomic) IBOutlet CustomDataPicker           *pickerView;
@property (weak, nonatomic) IBOutlet OrangeMiddleButton         *buttonComplete;
@property (weak, nonatomic) IBOutlet CustomScrollView           *scrollView;
@property (weak, nonatomic) IBOutlet UIView                     *viewWrapperPickerView;
@property (weak, nonatomic) IBOutlet UIView                     *viewSwitches;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint         *viewSwitchesHeight;
@property (weak, nonatomic) IBOutlet SegmentControlSettings     *segmentControlGender;
@property (weak, nonatomic) IBOutlet UILabel                    *labelShowBirthday;
@property (weak, nonatomic) IBOutlet SevenSwitch                *switchShowBirthday;
@property (strong, nonatomic) IBOutlet SevenSwitch              *switchShowBirthdayCopy;
@property (weak, nonatomic) IBOutlet RedButton                  *buttonLogout;
@property (weak, nonatomic) IBOutlet UILabel                    *labelShowProfile;
@property (weak, nonatomic) IBOutlet SegmentControlSettings     *segmentControlShowProfile;
@property (weak, nonatomic) IBOutlet UILabel                    *labelShowContacts;
@property (weak, nonatomic) IBOutlet SegmentControlSettings     *segmentControlShowContacts;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    *indicatorLoading;
@property (weak, nonatomic) IBOutlet UILabel                    *labelSocialVK;
@property (weak, nonatomic) IBOutlet UILabel                    *labelSocialTwitter;
@property (weak, nonatomic) IBOutlet SevenSwitch                *switchVK;
@property (strong, nonatomic) IBOutlet SevenSwitch              *switchVKCopy;
@property (weak, nonatomic) IBOutlet SevenSwitch                *switchTwitter;
@property (strong, nonatomic) IBOutlet SevenSwitch              *switchTwitterCopy;
@property (weak, nonatomic) IBOutlet LabelLikeTextField         *labelSearchFriendsVK;
@property (weak, nonatomic) IBOutlet LabelLikeTextField         *labelSearchFriendsContacts;


- (IBAction)actionTouchDownSegment:(id)sender;
- (void)setDetail:(BOOL)ar;
- (void)displayFromMenu:(BOOL)state;

@end
