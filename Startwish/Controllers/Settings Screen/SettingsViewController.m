//
//  RegisterDetailViewController.m
//  Startwish
//
//  Created by marsohod on 02/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <AVFoundation/AVFoundation.h>
#import "SettingsViewController.h"
#import "RevealViewController.h"
#import "MainNavigationController.h"
#import "CustomImagePicker.h"
#import "ClusterPrePermissions+Helper.h"
#import "Routing.h"
#import "LoginAPI.h"
#import "UserAPI.h"
#import "ModelUser.h"
#import "Me.h"
#import "UIImage+Helper.h"
#import "ActionTexts.h"
#import "UIFont+Helper.h"
#import "UIColor+Helper.h"
#import "UIScrollView+Helper.h"
#import "SegueFromLeftToRightUnwind.h"
#import "VKHandler.h"
#import "TwitterHandler.h"
#import "MBProgressHUD.h"
#import "PushNotificationHandler.h"


@interface SettingsViewController ()
{
    NSString            *birthdayFormat;
    BOOL                afterRegister;
    BOOL                isRequestFromMenu;
    BOOL                isAvatarChanged;
    NSDictionary        *city;
}
@end

@implementation SettingsViewController

#define FONT_SIZE_LABEL_SEARCH_FRIENDS 18.0
#define ICON_FIELD_MAX_WIDTH 32.0
#define HEIGHT_WRAPPER_PICKER_VIEW 162.0
#define DEFAULT_DATE @"0000-00-00"

- (void)viewDidLoad {
    [super viewDidLoad];

    [VKHandler sharedInstance].ctrl = self;
    [VKHandler sharedInstance].handlerDelegate = (id<VKHandlerDelegate>)self;
    [[VKHandler sharedInstance] wakeUpSession];
    
    birthdayFormat = DEFAULT_DATE;
    
    UITapGestureRecognizer *avatarTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openSelectImageDialog:)];
    
    // Обработчик на закрытие всех клавиатур
    UITapGestureRecognizer *tapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEnterTextFieldAction:)];
    
    // Тапаем на День рождения, чтобы показался PickerView
    UITapGestureRecognizer *tapToBirth = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTouchBirthday:)];
    
    // Тапаем на Город, чтобы показалась View со списком городов
    UITapGestureRecognizer *tapToCity = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTouchCity:)];
    
    UITapGestureRecognizer *tapToSearchVK = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSearchVK:)];
    UITapGestureRecognizer *tapToSearchContacts = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionSearchContacts:)];
    
    [self.view addGestureRecognizer:tapView];
    
    [self renderAvatar];
    
    // Добавляем открытие диалога с выбором способа вставки изображения
    [_viewAvatar addGestureRecognizer: avatarTap];
    

    [self.labelCity addGestureRecognizer:tapToCity];
    [self.labelBirthday addGestureRecognizer:tapToBirth];
    [self.labelSearchFriendsVK addGestureRecognizer:tapToSearchVK];
    [self.labelSearchFriendsContacts addGestureRecognizer:tapToSearchContacts];
    [self renderButtons];
    [self renderLabels];
    [self renderTextFields];
    [self renderSwitches];
    [self renderSwitchesView];
    [self renderSegmentControls];
    [self renderPickerView];
    
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _scrollView.delegate = (id<UIScrollViewDelegate>)self;
    _notification.hidden = YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Settings Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _notification.hidden = NO;
}

- (void)viewDidLayoutSubviews
{
    [_switchShowBirthday setNeedsDisplay];
    [_scrollView h_redrawHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    // Dispose of any resources that can be recreated.
}


// Закрыть все клавиатуры по тапу
- (IBAction)endEnterTextFieldAction:(id)sender
{
    [self.view endEditing:YES];
}


#pragma mark -
#pragma mark Render
- (void)renderAvatar
{
    _viewAvatar.isHighlighted = YES;
    [_viewAvatar customDrawView];
    // Убираем картинку по умолчанию. Тут она нам не нужна
    _viewAvatar.imageView.image = nil;
    
    if ( !afterRegister ) {

        // Ставим аватарку
        if( [Me myAvatarUrl] != nil ) {
            [UIImage downloadWithUrl: [NSURL URLWithString:[Me myAvatarUrl]] onCompleteChange:^(UIImage *image) {
                [_viewAvatar setImage:image];
                _viewAvatar.imageView.hidden = NO;
                _labelAvatar.hidden = YES;
//                [_viewAvatar.imageView setImage:image];
                //            self.imageViewAvatar.image = [[self.imageViewAvatar prepareImageToAvatarImage: image] scaleToSize: [UIImageViewAvatarInMenu avatarImageSize]];
            }];
        }
        
    }
}

- (void)renderButtons
{
    if ( !afterRegister ) {
        _buttonBack.hidden = isRequestFromMenu;
        _buttonMenu.hidden = !isRequestFromMenu;
        
        // Если мы пришли сюда из меню
        if ( isRequestFromMenu ) {
//            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
        
        [_buttonComplete removeFromSuperview];
        _scrollViewBottomPadding.constant = 0;
    } else {
        _buttonBack.hidden = YES;
        _buttonMenu.hidden = YES;
    }
}

- (void)renderTextFields
{
    // Устанавливаем иконки для полей
    [_textFieldUserName setIcon:@"user" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: -5.0 paddingLeft:0.0];
    [_textFieldUserSurname setIcon:@"user" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: -5.0 paddingLeft:0.0];
    [_textFieldUserDomain setIcon:@"user" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: -5.0 paddingLeft:0.0];
    
    if ( !afterRegister ) {
        NSArray *fio = [[Me myProfileName] componentsSeparatedByString:@" "];
        
        if ( [fio count] > 0 ) {
            _textFieldUserName.text = fio[0];
        }
        
        if ( [fio count] > 1 ) {
            // 1 - пробел, который мы не пишем
            _textFieldUserSurname.text = [[Me myProfileName] substringWithRange:NSMakeRange([fio[0] length] + 1, [[Me myProfileName] length] - [fio[0] length] - 1)];
        }
        
        _textFieldUserDomain.text = [Me myProfileDomain];
    } else {
        NSArray *fio = [[Me myProfileEmail] componentsSeparatedByString:@"@"];
        
        if ( [fio count] > 0 ) {
            _textFieldUserName.text = fio[0];
        }
    }
    
    [_textFieldUserName addTarget:self action:@selector(checkBeforeSaveUserProfile:) forControlEvents:UIControlEventEditingDidEnd];
    [_textFieldUserSurname addTarget:self action:@selector(checkBeforeSaveUserProfile:) forControlEvents:UIControlEventEditingDidEnd];
    [_textFieldUserDomain addTarget:self action:@selector(checkBeforeSaveUserProfile:) forControlEvents:UIControlEventEditingDidEnd];
}

- (void)renderLabels
{
    [_labelCity setIcon:@"city" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: 0.0 paddingLeft: 0.0];
    [_labelBirthday setIcon:@"birthday" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH];
    [_labelSearchFriendsVK setIcon:@"vk" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: -4.0 paddingLeft: 0.0];
    [_labelSearchFriendsContacts setIcon:@"contacts" toTheRightDirect:NO withMaxIconWidth: ICON_FIELD_MAX_WIDTH paddingTop: -4.0 paddingLeft: 0.0];
    
    [_labelSearchFriendsVK customSetText: @"Найти друзей ВКонтакте"];
    [_labelSearchFriendsContacts customSetText: @"Найти друзей из Контактов"];
    [_labelBirthday customSetText:@""];
    
    if ( !afterRegister ) {
        [self chooseCity:[Me myProfileCityName] cityId:[Me myProfileCityId]];
    } else {
        [_labelCity customSetText:@""];
    }
    
    [_labelShowBirthday setFont: [UIFont h_defaultFontLightSize: 20.0]];
    [_labelShowBirthday setTextColor: [UIColor h_smoothYellow]];
    
    
    [_labelSocialVK setFont: [UIFont h_defaultFontLightSize: 20.0]];
    [_labelSocialVK setTextColor: [UIColor h_smoothYellow]];
    
    [_labelSocialTwitter setFont: [UIFont h_defaultFontLightSize: 20.0]];
    [_labelSocialTwitter setTextColor: [UIColor h_smoothYellow]];
    
    [_labelShowProfile setFont: [UIFont h_defaultFontMediumSize: 8.0]];
    [_labelShowProfile setTextColor: [UIColor h_smoothYellow]];
    [_labelShowProfile setText: [_labelShowProfile.text uppercaseString]];
    
    [_labelShowContacts setFont: [UIFont h_defaultFontMediumSize: 8.0]];
    [_labelShowContacts setTextColor: [UIColor h_smoothYellow]];
    [_labelShowContacts setText: [_labelShowContacts.text uppercaseString]];
    
    [_labelSearchFriendsVK setFont: [UIFont h_defaultFontLightSize: FONT_SIZE_LABEL_SEARCH_FRIENDS]];
    [_labelSearchFriendsVK setTextColor:[UIColor whiteColor]];
    
    [_labelSearchFriendsContacts setFont: [UIFont h_defaultFontLightSize: FONT_SIZE_LABEL_SEARCH_FRIENDS]];
    [_labelSearchFriendsContacts setTextColor:[UIColor whiteColor]];
}

#define MARGIN_RIGHT_SWITCH 20.0
#define WIDTH_SWITCH 53.0
#define HEIGHT_SWITCH 33.0
#define BOUNDS_X_SWITCH [UIScreen mainScreen].bounds.size.width - MARGIN_RIGHT_SWITCH - WIDTH_SWITCH
- (void)renderSwitches
{
    self.switchShowBirthdayCopy = [[SevenSwitch alloc] initWithFrame:CGRectMake(BOUNDS_X_SWITCH, _switchShowBirthday.frame.origin.y, WIDTH_SWITCH, HEIGHT_SWITCH)];
    self.switchVKCopy = [[SevenSwitch alloc] initWithFrame:CGRectMake(BOUNDS_X_SWITCH, _switchVK.frame.origin.y - 10, WIDTH_SWITCH, HEIGHT_SWITCH)];
    self.switchTwitterCopy = [[SevenSwitch alloc] initWithFrame:CGRectMake(BOUNDS_X_SWITCH, _switchTwitter.frame.origin.y - 6, WIDTH_SWITCH, HEIGHT_SWITCH)];
    
    [_viewSwitches addSubview:_switchShowBirthdayCopy];
    [_viewSwitches addSubview:_switchVKCopy];
    [_viewSwitches addSubview:_switchTwitterCopy];
    
    [_switchShowBirthday removeFromSuperview];
    [_switchVK removeFromSuperview];
    [_switchTwitter removeFromSuperview];
    
    [self renderSwitch:_switchShowBirthdayCopy];
    [self renderSwitch:_switchVKCopy];
    [self renderSwitch:_switchTwitterCopy];

    [_switchShowBirthdayCopy addTarget:self action:@selector(saveUserSettings) forControlEvents:UIControlEventValueChanged];
    [_switchVKCopy addTarget:self action:@selector(loginVK) forControlEvents:UIControlEventValueChanged];
    [_switchTwitterCopy addTarget:self action:@selector(loginTW) forControlEvents:UIControlEventValueChanged];
    
    if ( [Me mySettingsShowBirthday] ) {
        [_switchShowBirthdayCopy setOn:YES];
    }
    
    if ( [[VKHandler sharedInstance] hasPermission] ) {
        [_switchVKCopy setOn:YES animated:NO];
    }
    
    if ( [TwitterHandler isLogin] ) {
        [_switchTwitterCopy setOn:YES animated:NO];
    }
}


- (void)renderSwitch:(SevenSwitch *)switchCopy
{
    //    _switchShowBirthday.activeColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch_off.png"]];
    //    _switchShowBirthday.onImage = [UIImage imageNamed:@"switch_on.png"];
    switchCopy.onTintColor = [UIColor h_switchOn];
    switchCopy.thumbTintColor = [UIColor h_smoothYellow];
    [switchCopy setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"switch_off.png"]]];
    switchCopy.layer.cornerRadius = 16.0;
    switchCopy.layer.borderWidth = 1.0;
    switchCopy.layer.borderColor = [UIColor h_smoothYellow].CGColor;
    switchCopy.clipsToBounds = YES;
    switchCopy.shadowColor = [UIColor blackColor];
    switchCopy.exclusiveTouch = YES;
}

- (void)renderSwitchesView
{
    _viewSwitches.hidden = afterRegister;
    
    if ( afterRegister ) {
        _viewSwitchesHeight.constant = 0;
    }
}

- (void)renderSegmentControls
{
//    NSLog(@"настройки: %d %d", [Me mySettingsShowContacts], [Me mySettingsShowProfile]);
    [_segmentControlGender setSelectedSegmentIndex: ([Me amIMan] ? 0 : 1)];
    [_segmentControlShowContacts setSelectedSegmentIndex: ![Me mySettingsShowContacts]];
    [_segmentControlShowProfile setSelectedSegmentIndex: ![Me mySettingsShowProfile]];
    
    [_segmentControlGender addTarget:self action:@selector(checkBeforeSaveUserProfile:) forControlEvents:UIControlEventValueChanged];
    [_segmentControlShowContacts addTarget:self action:@selector(saveUserSettings) forControlEvents:UIControlEventValueChanged];
    [_segmentControlShowProfile addTarget:self action:@selector(saveShowProfileSettings) forControlEvents:UIControlEventValueChanged];
    
    _segmentControlShowContacts.enabled = _segmentControlShowProfile.selectedSegmentIndex == 1;
}

- (void)renderPickerView
{
    NSString *date = [Me myProfileBirthday];
    
    NSNotificationCenter *notif = [NSNotificationCenter defaultCenter];
    [notif addObserver: self selector:@selector(pickerViewChangeValue:) name:@"pickerViewChangeValue" object:nil];
    
    [_pickerView setMaximumDateForBirthday];
    
    if ( ![date isEqualToString:@""] ) {
        [self.pickerView setDateFromString:date];
        
        birthdayFormat = date;
        
    } else {
        [_pickerView sendEventToController];
    }
    
   _viewWrapperPickerView.clipsToBounds = YES;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( scrollView.contentOffset.y > 150  ) {
        [self hideOnScroll];
        return;
    }
    
    [self showOnScroll];
    
}

- (void)hideOnScroll
{
    if ( _buttonMenu.layer.opacity != 0.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            _buttonMenu.layer.opacity = 0.0;
        }];
    }
    
}

- (void)showOnScroll
{
    if ( _buttonMenu.layer.opacity != 1.0 ) {
        [UIView animateWithDuration:0.5 animations:^{
            _buttonMenu.layer.opacity = 1.0;
        }];
    }
}

#pragma mark -
#pragma mark Upload Avatar
- (void)openSelectImageDialog:(UITapGestureRecognizer *)gesture
{
    __weak id d = self;
    
    [[Routing sharedInstance] showCameraControllerWithDelegate:d];
}


#pragma mark - CameraDelegate

- (CGSize)cropViewSize
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width);
}

- (void)takePhoto:(UIImage *)photo
{
    [_viewAvatar setImage:photo];
    _viewAvatar.imageView.hidden = NO;
    _labelAvatar.hidden = YES;
    isAvatarChanged = YES;
    [self saveUserProfile:@{@"avatar" : [photo fixOrientation]}];
    

    [[Routing sharedInstance] closeCameraController];
}

#pragma mark -
#pragma mark Navigation

- (void)setDetail:(BOOL)ar
{
    afterRegister = ar;
}

- (void)displayFromMenu:(BOOL)state
{
    isRequestFromMenu = state;
}

- (IBAction)actionMenuButton:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionExitButton:(id)sender
{
    if ( self.buttonLogout.alpha == 0.7f ) {return;}
    self.buttonLogout.alpha = 0.7f;
    
    if( ![LoginAPI isInternetConnectionAvailable] ) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[ActionTexts sharedInstance] failedText:@"Logout"] message:[[ActionTexts sharedInstance] text:@"LogoutWithoutInternet"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *logout = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"Logout"]
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                            
                                                            [[LoginAPI sharedInstance] cleanProfile];
                                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedAuthUser" object:nil];
                                                            self.buttonLogout.alpha = 1.f;
                                                            
                                                            [[VKHandler sharedInstance] logout];
                                                            [TwitterHandler logout];
                                                        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[ActionTexts sharedInstance] titleText:@"Cancel"]
                                                         style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                             self.buttonLogout.alpha = 1.f;
                                                         }];

        [alert addAction:cancel];
        [alert addAction:logout];
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        
        UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        popPresenter.sourceView = _buttonLogout;
        popPresenter.sourceRect = _buttonLogout.bounds;
        
        [self.navigationController presentViewController:alert animated:YES completion:nil];

        return;
    }
    
    [[LoginAPI sharedInstance] logoutWithCallBack:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedAuthUser" object:nil];
        self.buttonLogout.alpha = 1.f;
        
        [[VKHandler sharedInstance] logout];
        [TwitterHandler logout];
        
    } failedCallBack:^(NSString *error) {
        [_notification showNotify:0 withText: [[ActionTexts sharedInstance] failedText:@"Logout"]];
        self.buttonLogout.alpha = 1.f;
    }];
    
}

- (void)showAlert
{
//    [self showAlert];
    _indicatorLoading.hidden = NO;
}

- (void)hideAlert:(id)sender
{
    _indicatorLoading.hidden = YES;
//    [alertView dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if( [segue.identifier isEqualToString:@"welcome"] ) {
        
    } else if( [segue.identifier isEqualToString:@"selectCity"] ) {
        
        [Routing goToCitySearchController: segue delegate: self];
        
    } else if ([segue.identifier isEqualToString:@"searchFriends"]) {
        
        uint32_t userId = 0;
        
        if ( [sender[@"type"] integerValue] == 10 ) {
            userId = [VKHandler userId];
        }
        
        [Routing goToPeopleCtrl:segue userId:userId peopleType:[sender[@"type"] integerValue]];
        
    }
}


#pragma mark -
#pragma mark CityDelegate
- (void)chooseCityWithUpdate:(NSString *)cityName cityId:(NSNumber *)cityId
{
    if ( city[@"name"] == nil || ![city[@"name"] isEqualToString:cityName] ) {
        [self chooseCity:cityName cityId:cityId];
        [self saveUserLocation];
    } else {
        [self chooseCity:cityName cityId:cityId];
    }
    
}

- (void)chooseCity:(NSString *)cityName cityId:(NSNumber *)cityId
{
    if ( cityName != nil && cityId != nil && ![cityName isEqualToString:@""] ) {
        city = [[NSDictionary alloc] initWithObjects:@[cityName, cityId] forKeys:@[@"name", @"id"]];
        [self.labelCity customSetText: cityName];
        _buttonCityRemove.hidden = NO;
    } else {
        _buttonCityRemove.hidden = YES;
    }
}

- (IBAction)actionRemoveCityButton:(id)sender
{
    if ( !_buttonCityRemove.hidden ) {
        _buttonCityRemove.hidden = YES;
        city = [[NSDictionary alloc] initWithObjects:@[@"", [NSNumber numberWithInteger:0]] forKeys:@[@"name", @"id"]];
        [self.labelCity customSetText: @""];
        [self checkBeforeSaveUserProfile:nil];
    }
}


#pragma mark -
#pragma mark PickerView

- (void)pickerViewChangeValue:(NSNotification *)notif
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    
    birthdayFormat = [NSString stringWithFormat:@"%@-%@-%@", notif.userInfo[@"year"], notif.userInfo[@"month"], notif.userInfo[@"day"]];
    
    NSString *value = [NSString stringWithFormat: @"%@ %@ %@", notif.userInfo[@"day"], [[dateFormatter.monthSymbols objectAtIndex:([notif.userInfo[@"month"] integerValue] - 1)] capitalizedString], notif.userInfo[@"year"]];
    
    
    [self.labelBirthday customSetText: value];
}

- (void)showHidePickerView
{
    float viewHeight;
    CGRect frame = self.viewWrapperPickerView.frame;
    frame.size = CGSizeMake(frame.size.width, frame.size.height == 0.0 ? HEIGHT_WRAPPER_PICKER_VIEW : 0.0);
    
    viewHeight = _viewWrapperPickerView.frame.size.height == 0 ? HEIGHT_WRAPPER_PICKER_VIEW : 0.0;

    self.heightViewForPickerView.constant = viewHeight;
    
    [UIView animateWithDuration:0.5f delay:0.0f options:UIViewAnimationOptionLayoutSubviews animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

- (IBAction)actionTouchCity:(UITapGestureRecognizer *)sender {
    [self performSegueWithIdentifier:@"selectCity" sender:nil];
}

- (IBAction)actionSearchVK:(id)sender {
    if ( [[VKHandler sharedInstance] hasPermission] ) {
        [self performSegueWithIdentifier:@"searchFriends" sender:@{@"type": [NSNumber numberWithInt:10]}];
        [_labelSearchFriendsVK setAlpha:1.0];
    } else {
        [_labelSearchFriendsVK setAlpha:0.9];
        [[VKHandler sharedInstance] auth];
    }
}

- (IBAction)actionSearchFB:(id)sender {
    
    /*if ( [FacebookHandler isLogin] ) {
        [self performSegueWithIdentifier:@"searchFriends" sender:@{@"type": [NSNumber numberWithInt:11]}];
        return;
    }
    
    [FacebookHandler loginWithSuccess:^{
        [self actionSearchFB:nil];
    } failed:^{
        [_notification showNotify:0 withText: [[ActionTexts sharedInstance] failedText:@"FacebookAccess"]];
    }];*/
}

- (IBAction)actionSearchContacts:(id)sender {
    [ClusterPrePermissions h_showContactsPermissionsWithCallBack:^{
        [self performSegueWithIdentifier:@"searchFriends" sender:@{@"type": [NSNumber numberWithInt:12]}];
    } rejectFirstStep:^{
        
    } rejectSecondStep:^{
        
    }];
}

- (IBAction)actionTouchBirthday:(id)sender {
    [self expandScrollView: self.viewWrapperPickerView.frame.size.height == 0 ? YES : NO];
    [self showHidePickerView];
}

- (void)expandScrollView:(BOOL)expand
{
    // Если скрываем, то сначала анимация, потом изменение размеров
    if ( !expand ) {
        [self scrollToBottom:expand];
        [_scrollView h_redrawHeight];
    }

    
    if ( expand ) {
//        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, [_scrollView h_contentHeight].size.height + HEIGHT_WRAPPER_PICKER_VIEW)];
        [_scrollView h_redrawHeight];
        [self scrollToBottom:expand];
    }
    
}

// Scroll scrollView to Point
- (void)scrollToBottom:(BOOL)bottom
{
    if ( bottom ) {
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.bounds.size.height - _viewSwitches.frame.size.height);
        
        [UIView beginAnimations:@"scrollTo" context:nil];
        [UIView setAnimationDuration:0.5f];

        [self.scrollView setContentOffset:bottomOffset];
        [UIView commitAnimations];
    } else {
        [self checkBeforeSaveUserProfile:nil];
    }
}

- (IBAction)actionTouchDownSegment:(id)sender
{

}


#pragma mark -
#pragma mark Save Data
- (IBAction)actionSendProfile:(id)sender
{
    // deprecated
    if ( afterRegister ) {
        if ( [Me needMoreSubscriptions] ) {
            [[Routing sharedInstance] showChannelRecomendationController];
            return;
        }
        
        [self performSegueWithIdentifier:@"welcome" sender:nil];
        
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if ( afterRegister ) {
        [[UserAPI sharedInstance] setUserImage: UIImageJPEGRepresentation([_viewAvatar.imageView.image fixOrientation], 0.7)
                                        domain: _textFieldUserDomain.text
                                      fullname: [[NSString stringWithFormat:@"%@ %@", self.textFieldUserName.text, self.textFieldUserSurname.text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]
                                        gender: [_segmentControlGender selectedSegmentIndex] == 0 ? [ModelUser genderManValue] : [ModelUser genderWomenValue]
                                          city: city[@"name"]
                                        cityId: city[@"id"]
                                      birthday: birthdayFormat
                                  withCallback: ^(ModelUser *man) {
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      
                                      [Me setProfile:man withEmail:[Me myProfileEmail] cityName:city[@"name"] cityId:city[@"id"] birthday:birthdayFormat];
                                      
                                      if ( [Me needMoreSubscriptions] ) {
                                          [[Routing sharedInstance] showChannelRecomendationController];
                                          return;
                                      }
                                      
                                      [self performSegueWithIdentifier:@"welcome" sender:nil];
                                  } failedBlock:^(id item) {
                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
                                      [_notification showNotify:0 withText:[NSString stringWithFormat:@"%@", item]];
                                  }];
    }
}

- (void)checkBeforeSaveUserProfile:(id)sender
{
    NSString *fullname = [_textFieldUserSurname.text isEqualToString:@""] ? _textFieldUserName.text : [NSString stringWithFormat:@"%@ %@", _textFieldUserName.text, _textFieldUserSurname.text];
    
    if ( [_textFieldUserSurname.text isEqualToString:@""] && [_textFieldUserName.text isEqualToString:@""] ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"EmptyUserName"]];
        return;
    }
    
    if ( ![[Me myProfileName] isEqualToString:fullname] ) {
        [self saveUserProfile:@{@"username" : [[NSString stringWithFormat:@"%@ %@", self.textFieldUserName.text, self.textFieldUserSurname.text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]}];
        return;
    }
    
    if ( ![[Me myProfileCityName] isEqualToString:city[@"name"]] && city[@"name"] != nil) {
        [self saveUserLocation];
        return;
    }
    
    if ( [Me amIMan] && [_segmentControlGender selectedSegmentIndex] == 1 ) {
        [self saveUserProfile:@{@"sex" : [NSNumber numberWithInteger:[ModelUser genderWomenValue]]}];
        return;
    }
    
    if ( ![Me amIMan] && [_segmentControlGender selectedSegmentIndex] == 0 ) {
        [self saveUserProfile:@{@"sex" : [NSNumber numberWithInteger:[ModelUser genderManValue]]}];
        return;
    }
    
    if ( ![[Me myProfileDomain] isEqualToString:_textFieldUserDomain.text] ) {
        if ( [Me isDomainValid:_textFieldUserDomain.text] ) {
            [self saveUserProfile:@{@"page_identity" : _textFieldUserDomain.text}];
            return;
        }
        
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"DomainIsInvalid"]];
    }
    
    if ( ![[Me myProfileBirthday] isEqualToString:birthdayFormat] && ![birthdayFormat isEqualToString:DEFAULT_DATE]) {
        [self saveUserProfile:@{@"birthday" : birthdayFormat}];
        return;
    }
}

- (void)saveUserProfile:(NSDictionary *)prof
{
    [self showAlert];
    
    [[UserAPI sharedInstance] setUserProfile:prof withCallback:^(ModelUser *man) {
        [self hideAlert:nil];
        [Me setProfile:man withEmail:[Me myProfileEmail] cityName:@"" cityId:[NSNumber numberWithInteger:0] birthday:birthdayFormat];
        
        [_notification showNotify:2 withText: [[ActionTexts sharedInstance] successText:@"Settings"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STProfileUpdate" object:nil];

    } failedBlock:^(id item) {
        [self hideAlert:nil];
        [_notification showNotify:0 withText:[NSString stringWithFormat:@"%@", item]];
    }];
}

- (void)saveUserLocation
{
    // Это всё только для настроек
    if ( afterRegister ) { return; }
    [self showAlert];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // TODO:
        
        [[UserAPI sharedInstance] setUserLocationCity:(uint32_t)[city[@"id"] unsignedLongValue]
                                     withCallback:^(id item) {
                                         [self hideAlert:nil];
                                         [_notification showNotify:2 withText: [[ActionTexts sharedInstance] successText:@"Settings"]];
                                         [Me setProfileLocation:(city[@"name"] != nil ? city[@"name"] : @"")
                                                         cityId:(city[@"id"] != nil ? city[@"id"] : [NSNumber numberWithInteger:0])];
                                     } failedBlock:^(id item) {
                                         [self hideAlert:nil];
                                         [_notification showNotify:2 withText:[NSString stringWithFormat:@"%@", item]];
                                     }];
    });
}

- (IBAction)saveShowProfileSettings
{
    _segmentControlShowContacts.enabled = _segmentControlShowProfile.selectedSegmentIndex == 1;
    
    [self saveUserSettings];
}

- (IBAction)saveUserSettings
{
    // Это всё только для настроек
    if ( afterRegister ) { return; }
    [self showAlert];
    [[UserAPI sharedInstance] setUserSettingsShowBirthday:!_switchShowBirthdayCopy.isOn
                                              showProfile:![_segmentControlShowProfile selectedSegmentIndex]
                                             showContacts:![_segmentControlShowContacts selectedSegmentIndex]
                                             withCallback:^(id item) {
                                                 [self hideAlert:nil];
                                                 
                                                 // записываем настройки
                                                 [Me setProfileSettingsShowBirthday:_switchShowBirthdayCopy.isOn
                                                                          showProfile:![_segmentControlShowProfile selectedSegmentIndex]
                                                                         showContacts:![_segmentControlShowContacts selectedSegmentIndex]];
                                                
//                                                [self saveUserProfile];
                                                 [_notification showNotify:2 withText: [[ActionTexts sharedInstance] successText:@"Settings"]];
                                             } failedBlock:^(id item) {
                                                 [_notification showNotify:0 withText: [NSString stringWithFormat:@"%@", item]];
                                                 [self hideAlert:nil];
                                             }];
}


#pragma mark -
#pragma mark Social
- (void)loginVK
{
    if ( !_switchVKCopy.isOn ) {

        [[UserAPI sharedInstance] unsetNetworkById:[[Me mySocialByToken:[VKHandler accessToken]] getId] success:^(id item) {} failedBlock:^(id item) {}];
        [[VKHandler sharedInstance] logout];
    } else {
        [[VKHandler sharedInstance] auth];
    }
}

- (void)loginTW
{
    if ( !_switchTwitterCopy.isOn ) {
        [TwitterHandler logout];
    } else {
        [TwitterHandler login:^{
            [_switchTwitterCopy setOn:YES animated:YES];
        } canceled:^(NSString *error) {
            [_switchTwitterCopy setOn:NO animated:YES];
        } ctrl:self];
    }
}


#pragma mark -
#pragma mark VKHandlerDelegate
- (void)allowAccess
{
    [_switchVKCopy setOn:YES animated:YES];
    
    if ( _labelSearchFriendsVK.alpha != 1.0 ) {
        [self actionSearchVK:nil];
    }
    
    // save social network
    [[UserAPI sharedInstance] setVKToken:[VKHandler accessToken] success:^(id item) {} failedBlock:^(id item) {}];
}

- (void)denyAccess
{
    [_switchVKCopy setOn:NO animated:YES];
}



@end
