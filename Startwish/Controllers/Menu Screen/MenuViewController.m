//
//  MenuViewController.m
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "MenuViewController.h"
#import "MenuItemTableCell.h"
#import "MainNavigationController.h"
#import "FeedViewController.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"
#import "UILabel+typeOfText.h"
#import "Routing.h"
#import "LoginApi.h"
#import "Me.h"


@interface MenuViewController ()
{
    NSArray *items;
}
@end

@implementation MenuViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewAvatar.isHighlighted = YES;
    [_viewAvatar customDrawView];
    
    UIImage *separator = [[UIImage imageNamed: @"pickerView_border.png"] scaleToSize: CGSizeMake(self.tableViewMenuItems.frame.size.width, 1.0)];
    
    // Говорим какую ячейку отображать
    [self.tableViewMenuItems registerNib: [UINib nibWithNibName:@"MenuItemCell" bundle:nil] forCellReuseIdentifier:@"menuCell"];
    
    self.tableViewMenuItems.scrollEnabled = NO;
    
    // Разукрашиваем таблицу:
    // Ставим прозрачный фон и разделитель ячейки
    [self.tableViewMenuItems setBackgroundColor: [UIColor clearColor]];
    [self.tableViewMenuItems setSeparatorColor: [UIColor colorWithPatternImage: separator]];
    
    
    // Добавляем бордер к верхней ячейки таблицы.
    // Точнее бордер к таблице
    UIImageView *borderTop = [[UIImageView alloc] initWithFrame: CGRectMake( 0.0, 0.0, self.tableViewMenuItems.frame.size.width, 0.5)];
    
    borderTop.image = separator;
    
    [self.tableViewMenuItems addSubview: borderTop];
    
    items = @[
              @{
                  @"icon"   : @"want",
                  @"title"  : @"Мои желания",
                  @"segue"  : @"wishes"},
              @{
                  @"icon"   : @"feed",
                  @"title"  : @"Моя лента",
                  @"segue"  : @"sw_front"},
              @{
                  @"icon"   : @"smile",
                  @"title"  : @"Мои друзья",
                  @"segue"  : @"people"},
              @{
                  @"icon"   : @"explore",
                  @"title"  : @"Обзор желаний",
                  @"segue"  : @"explorer"},
              @{
                  @"icon"   : @"friends",
                  @"title"  : @"Люди",
                  @"segue"  : @"peopleRating"},
              [[NSMutableDictionary alloc] initWithDictionary:@{
                  @"icon"       : @"alarm",
                  @"title"      : @"События",
                  @"segue"      : @"events",
                  @"count"      : [NSNumber numberWithInteger:[UIApplication sharedApplication].applicationIconBadgeNumber],
                  @"countColor" : @"yellow"}],
              
//              [[NSMutableDictionary alloc] initWithDictionary:@{
//                  @"icon"       : @"magic_stick",
//                  @"title"      : @"Возможности",
//                  @"segue"      : @"events",
//                  @"count"      : [NSString stringWithFormat: @"%d", 3],
//                  @"countColor" : @"red"}],
              
//              @{
//                  @"icon"   : @"share",
//                  @"title"  : @"Рассказать о Startwish",
//                  @"segue"  : @"events"}
              ];
    
    UITapGestureRecognizer *tapAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfile:)];
    [_viewAvatar addGestureRecognizer:tapAvatar];
    
    // Ставим иконку рейтинга и значение
    [self renderUserInfo];
    
    _viewControllerCache = [[NSMutableDictionary alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateEventCount:) name:@"STNewEventsCountUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(renderUserInfo) name:@"STProfileUpdate" object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Выбор ячейки по умолчанию при первом появлении
    if ( [_tableViewMenuItems indexPathForSelectedRow] == nil ) {
        [_tableViewMenuItems selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
    
    [self renderUserInfo];
    
    self.screenName = @"Menu Screen";
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.revealViewController.view removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)renderUserInfo
{
    // Ставим аватарку
    if( [Me myAvatarUrl] != nil ) {
        [UIImage downloadWithUrl: [NSURL URLWithString:[Me myAvatarUrl]] onCompleteChange:^(UIImage *image) {
            [_viewAvatar setImage:image];
        }];
    }
    
    // Ставим Имя Фамилию
    if ( [Me myProfileName] != nil ) {
        self.labelUserName.text = [Me myProfileName];
    }
}

// Убираем паддинг разделителя слева
- (void)viewDidLayoutSubviews
{
    if ([self.tableViewMenuItems respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableViewMenuItems setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Паддинг разделителей для пустых ячеек
    if ([self.tableViewMenuItems respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableViewMenuItems setLayoutMargins:UIEdgeInsetsZero];
    }
    

    
    [self.tableViewMenuItems removeConstraints: [self.tableViewMenuItems constraints]];
    
    [self.tableViewMenuItems addConstraint: [NSLayoutConstraint constraintWithItem: self.tableViewMenuItems
                                                                         attribute: NSLayoutAttributeHeight
                                                                         relatedBy: NSLayoutRelationEqual
                                                                            toItem: nil
                                                                         attribute: NSLayoutAttributeHeight
                                                                        multiplier: 1.0
                                                                          constant: self.tableViewMenuItems.contentSize.height]];
}


- (void)updateEventCount:(NSNotification *)notif
{
    NSIndexPath *indexPath = [_tableViewMenuItems indexPathForSelectedRow];
    
    [items[5] setObject:[NSNumber numberWithInteger: [UIApplication sharedApplication].applicationIconBadgeNumber] forKey:@"count"];
    [_tableViewMenuItems reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:5 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    [_tableViewMenuItems selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
}


// Если мы сейчас находимся на первом экране UINavigationController
// то осхраняем его в кэш

- (void)cacheMainCtrl
{
    UINavigationController *ctrl = (UINavigationController *)self.revealViewController.frontViewController;
    
    if( [[ctrl viewControllers] count] > 0 ) {
        if( [[ctrl viewControllers][0] isMemberOfClass:[FeedViewController class]]) {
            [self.viewControllerCache setObject: [(UINavigationController *)self.revealViewController.frontViewController viewControllers][0] forKey:@"main"];
        }
    }
}

- (FeedViewController *)isCacheMainCtrl
{
    return [self.viewControllerCache objectForKey:@"main"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -
#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuItemTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    
    if( indexPath ) {
        [cell setMenuItem: items[indexPath.row]];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45.0;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [self cacheMainCtrl];
    
    // Если пытаемся перейти обратно на главный экран с желаниями
    if( [items[indexPath.row][@"segue"] isEqualToString: @"sw_front"] ) {

        // Если у нас есть закэшированный контроллер с желаниями, то вставляем его в NC
        if( [self isCacheMainCtrl] != nil ) {
            [[Routing sharedInstance].navigationController setViewControllers:@[[self isCacheMainCtrl]] animated:NO];
        }
        
    } else if ( [items[indexPath.row][@"segue"] isEqualToString: @"wishes"] ) {
        // Показываем желания пользователя
        
        [[Routing sharedInstance] goToWishesCtrlByOwn];
        
    } else if ( [items[indexPath.row][@"segue"] isEqualToString: @"people"] ) {
        // Показываем друзей пользователя
        
        [[Routing sharedInstance] showPeopleCtrl];
        
    } else if ( [items[indexPath.row][@"segue"] isEqualToString: @"explorer"] ) {
        // Показываем поиск желаний
        
        [[Routing sharedInstance] showExplorerController];
        
    } else if ( [items[indexPath.row][@"segue"] isEqualToString: @"events"] ) {
        // Показываем события пользователя
        
        [[Routing sharedInstance] showEventsController];
        
    } else if ( [items[indexPath.row][@"segue"] isEqualToString: @"peopleRating"] ) {
        
        [[Routing sharedInstance] showPeopleSearchCtrlFromMenu:YES];
        
    } else {
        [self performSegueWithIdentifier:items[indexPath.row][@"segue"] sender:nil];
    }
    
    
    // Переходим на новый экран
    [self.revealViewController setFrontViewController:[Routing sharedInstance].navigationController];
    
    // Скрываем меню
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
}



#pragma mark -
#pragma mark Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (void)goToProfile:(UITapGestureRecognizer *)recognizer
{
    [self cacheMainCtrl];
    [_tableViewMenuItems deselectRowAtIndexPath: [_tableViewMenuItems indexPathForSelectedRow] animated:YES];
    
    [[Routing sharedInstance] showUserControllerWithOwn];

    [self.revealViewController setFrontViewController:[Routing sharedInstance].navigationController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
}


- (IBAction)actionSettingsButton:(id)sender {
    [self cacheMainCtrl];
    [_tableViewMenuItems deselectRowAtIndexPath: [_tableViewMenuItems indexPathForSelectedRow] animated:YES];
    
    [[Routing sharedInstance] showSettingsController];
    
    [self.revealViewController setFrontViewController: [Routing sharedInstance].navigationController];
    [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
}
@end
