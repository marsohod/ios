//
//  MenuViewController.h
//  Startwish
//
//  Created by marsohod on 05/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIViewAvatar.h"
#import "SWRevealViewController.h"
#import "HeaderLabel.h"
#import "GAITrackedViewController.h"


@interface MenuViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIViewAvatar *viewAvatar;
@property (weak, nonatomic) IBOutlet HeaderLabel *labelUserName;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMenuItems;
@property (nonatomic, strong) NSMutableDictionary *viewControllerCache;

- (IBAction)actionSettingsButton:(id)sender;

@end
