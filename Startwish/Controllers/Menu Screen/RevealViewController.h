//
//  RevealViewController.h
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "SWRevealViewController.h"


@interface RevealViewController : SWRevealViewController

@property (strong, nonatomic) NSArray *feedItems;

- (void)setStartFeed:(NSArray *)newFeedItems;

@end
