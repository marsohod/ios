//
//  RevealViewController.m
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "RevealViewController.h"
#import "Routing.h"


@implementation RevealViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setStartFeed:(NSArray *)items
{
    self.feedItems = items;
}

@end
