//
//  SpamViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface SpamViewController : UIViewController

- (void)setDetail:(uint32_t)wishId;

@end
