//
//  SpamViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 24/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SpamViewController.h"
#import "SpamTableView.h"
#import "SettingsAPI.h"
#import "WishesAPI.h"
#import "NotificationLabel.h"
#import "ActionTexts.h"


@interface SpamViewController ()

- (IBAction)actionBackButton:(id)sender;

@property (weak, nonatomic) IBOutlet SpamTableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (weak, nonatomic) IBOutlet UILabel *labelSuccess;
@property (nonatomic) uint32_t wishId;

@end


@implementation SpamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.spamDelegate = (id<SpamTableViewDelegate>)self;
    [self loadReasonItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setDetail:(uint32_t)wishId
{
    self.wishId = wishId;
}

- (void)loadReasonItems
{
    [[SettingsAPI sharedInstance] spamReasonItems:^(NSArray *items) {
        _tableView.items = items;
        [_tableView reloadData];
        [_indicator stopAnimating];
    } failedBack:^(id item) {
        [_indicator stopAnimating];
    }];    
}

- (IBAction)actionBackButton:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - SpamTableViewDelegate

- (void)willSelectCellId:(NSInteger)cellId
{
    [_indicator startAnimating];
    
    [self.view setUserInteractionEnabled:NO];
    
    [[SettingsAPI sharedInstance] spamWishId:self.wishId reason:cellId callBack:^(id item) {
        [_indicator stopAnimating];
        [self.view setUserInteractionEnabled:YES];
        _labelSuccess.text = [[ActionTexts sharedInstance] successText:@"SendSpam"];
        _labelSuccess.hidden = NO;
        _tableView.items = @[];
        [_tableView reloadData];
    } failedBlock:^(id item) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"SendSpam"]];
        [_indicator stopAnimating];
        [self.view setUserInteractionEnabled:YES];
    }];
    
    
}

@end
