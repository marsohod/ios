//
//  SocialShareWishViewController.h
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ContactsTableView.h"
#import "NotificationLabel.h"
#import "ModelWish.h"
#import "InstagramDocumentInteractionController.h"
#import "GAITrackedViewController.h"


@interface SocialShareWishViewController : GAITrackedViewController

- (IBAction)actionBackButton:(id)sender;
- (IBAction)actionSendButton:(id)sender;

@property (weak, nonatomic) IBOutlet NotificationLabel *notification;
@property (weak, nonatomic) IBOutlet UILabel *labelView;
@property (weak, nonatomic) IBOutlet ContactsTableView *tableView;
//@property (weak, nonatomic) WishDetailViewController *wishCtrl;
@property (strong, nonatomic) NSArray *shareButtons;
@property (strong, nonatomic) ModelWish *wish;
@property (nonatomic, strong) InstagramDocumentInteractionController *documentController;

- (void)setDetail:(ModelWish *)wish;

@end
