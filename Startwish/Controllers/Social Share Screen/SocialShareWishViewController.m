//
//  SocialShareWishViewController.m
//  Startwish
//
//  Created by Pavel Makukha on 20/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SocialShareWishViewController.h"
#import "UIColor+Helper.h"
#import "UIFont+Helper.h"
#import "VKHandler.h"
#import "TwitterHandler.h"
#import "InstagramHandler.h"
#import "ActionTexts.h"
#import "MBProgressHUD.h"
#import "UserAPI.h"


#import "UIImage+Helper.h"


@implementation SocialShareWishViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _shareButtons = @[@"vkontakte", @"twitter", @"instagram"];
    
    [self renderLabelView];
    [self.view setBackgroundColor:[UIColor h_bg_profile_circle]];
    _tableView.tableDelegate = (id<ContactsTableViewDelegate>)self;
    _tableView.itemsKey = _shareButtons;
    _tableView.itemsValue = _shareButtons;
    _tableView.allowsMultipleSelection = YES;
    [_tableView reloadData];
    [VKHandler sharedInstance].ctrl = self;
    [VKHandler sharedInstance].handlerDelegate = (id<VKHandlerDelegate>)self;
    [[VKHandler sharedInstance] wakeUpSession];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Social Share Wish Screen";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _notification.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _notification.hidden = YES;
}

- (void)dealloc
{
    
}

- (void)renderLabelView
{
    [_labelView setFont: [UIFont h_defaultFontSize:16.0]];
    [_labelView setTextAlignment: NSTextAlignmentCenter];
    [_labelView setTextColor: [UIColor h_bg_contacts]];
    [_labelView setText: [_labelView.text uppercaseString]];
}

- (IBAction)actionBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionSendButton:(id)sender {
    NSString *postText;
    __block NSUInteger socialLength = [[_tableView indexPathsForSelectedRows] count];
    dispatch_queue_t queue = dispatch_queue_create("com.startwish.sharequeue", DISPATCH_QUEUE_CONCURRENT);
//    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    
    if ( socialLength == 0 ) {
        [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"SocialNetworkNotSelected"]];
        return;
    }
    
    postText = [_wish isImplement] ? ([[ActionTexts sharedInstance] userName:[_wish isMyWish] ? @"Моё" : [[_wish getOwner] getFullname]
                                                             executeWishName:[_wish getName]])
                                    : ([[ActionTexts sharedInstance] userName:[_wish isMyWish] ? @"Я" : [[_wish getOwner] getFullname]
                                                              wantWishName:[_wish getName]]);
    
    [[_tableView indexPathsForSelectedRows] enumerateObjectsUsingBlock:^(NSIndexPath *obj, NSUInteger idx, BOOL *stop) {
        switch ( obj.row ) {
            case 0:{
                dispatch_async(queue, ^{
                    [[VKHandler sharedInstance] postToWallWishId: [_wish getId]
                                                            name: [_wish getName]
                                                            desc: postText
                                                       imageLink: [_wish getCoverOriginal] success:^(NSDictionary *results) {
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"VKPost"]}];
                                                       } failed:^(NSString *error) {
                                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"VKPost"]}];
                                                       }];
                });
                break;
            }
//            case 1: {
//                // send fb
//                [myQueue addOperationWithBlock:^{
//                    [FacebookHandler postWishId:[_wish getId]
//                                           name:[_wish getName]
//                                        caption:@""
//                                           desc:postText
//                                      imageLink:[_wish getCoverOriginal]
//                                        success:^(NSDictionary *results) {
//                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"FacebookPost"]}];
//                                        }
//                                         failed:^(NSString *error) {
//                                             [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"FacebookPost"]}];
//                                         }
//                                      withQueue:myQueue];
//                }];
//                break;
//            }
            case 1: {
                // send twitter
                dispatch_async(queue, ^{
                    [TwitterHandler postTweetWithWishId: [_wish getId]
                                                   text: postText
                                              imageLink: [_wish getCoverOriginal] success:^{
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"2", @"text": [[ActionTexts sharedInstance] successText:@"TwitterPost"]}];
                                              } canceled:^(NSString *string) {
                                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type": @"0", @"text": [[ActionTexts sharedInstance] failedText:@"TwitterPost"]}];
                                              }];
                });
                break;
            }
            case 2: {
                [self trySendInstagramPost];
                break;
            }
                
            default:
                break;
        }
    }];
        
    if ( ![self isInstagramSelected] ) {
        [self actionBackButton:nil];
    }
}

- (BOOL)isInstagramSelected
{
    __block BOOL isIt = NO;
    
    [[_tableView indexPathsForSelectedRows] enumerateObjectsUsingBlock:^(NSIndexPath *obj, NSUInteger idx, BOOL *stop) {
        if ( obj.row == 2 ) {
            isIt = YES;
        }
    }];
    
    return isIt;
}

- (void)trySendInstagramPost
{
    [InstagramHandler fileUrlToImageUrl:[_wish getCoverOriginal] afterSave:^(NSURL *filePath) {
        if ( self.documentController == nil ) {
            self.documentController = [[InstagramDocumentInteractionController alloc] initWithUrl:filePath];
            self.documentController.delegate = (id<UIDocumentInteractionControllerDelegate>)self;
        } else {
            self.documentController.URL = filePath;
        }
        
        self.documentController.annotation = [NSDictionary dictionaryWithObject: [[ActionTexts sharedInstance] userName:[_wish isMyWish] ? @"Я" : [[_wish getOwner] getFullname] wantWishName:[_wish getName]] forKey:@"InstagramCaption"];
        
        [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView: self.view animated: YES ];
    }];
}


#pragma mark -
#pragma mark UIDocumentInteractionControllerDelegate
-(void)documentInteractionController: (UIDocumentInteractionController *)controller didEndSendingToApplication:(NSString *)application
{
    [self actionBackButton:nil];
}


#pragma mark -
#pragma mark ContactsTableViewDelegate
- (void)selectItemKey:(NSUInteger)idx
{
    if ( idx < [_shareButtons count] && [_shareButtons[idx] isEqualToString:@"vkontakte"] ) {
        
        if ( ![[VKHandler sharedInstance] hasPermission] ) {
            [[VKHandler sharedInstance] auth];
        }
        
    } else if ( idx < [_shareButtons count] && [_shareButtons[idx] isEqualToString:@"facebook"] ) {
        /*if ( ![FacebookHandler isLogin] ) {
            [FacebookHandler loginWithSuccess:^{
                
            } failed:^{
                [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"FacebookAccess"]];
                [_tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:YES];
            }];
            
            return;
        }
        
        
        [FacebookHandler hadPublishPermission:^{
            
        } failed:^{
            [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"FacebookAccessPublish"]];
            [_tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:YES];
        }];*/
        
    } else if ( idx < [_shareButtons count] && [_shareButtons[idx] isEqualToString:@"twitter"] ) {
        if ( ![TwitterHandler isLogin] ) {
            [TwitterHandler login:^{
                
            } canceled:^(NSString *error) {
                [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"TwitterAccess"]];
                [_tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:YES];
            } ctrl:self];
        }
    } else if ( idx < [_shareButtons count] && [_shareButtons[idx] isEqualToString:@"instagram"] ) {
        if ( ![InstagramHandler canOpenApp] ) {
            [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"InstagramAccess"]];
            [_tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0] animated:YES];
        }
    }
}

- (void)setDetail:(ModelWish *)wish
{
    _wish = wish;
}



#pragma mark -
#pragma mark VKHandlerDelegate
- (void)allowAccess
{
    // save social network to profile
    [[UserAPI sharedInstance] setVKToken:[VKHandler accessToken] success:^(id item) {} failedBlock:^(id item) {}];
}

- (void)denyAccess
{
    [_notification showNotify:0 withText:[[ActionTexts sharedInstance] failedText:@"VKAccess"]];
    [_tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
    [VKHandler sharedInstance].denyByUser = YES;
}


@end
