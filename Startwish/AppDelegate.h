//
//  AppDelegate.h
//  Startwish
//
//  Created by marsohod on 30/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

