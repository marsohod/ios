//
//  Errors.h
//  Startwish
//
//  Created by marsohod on 11/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ActionTexts : NSObject

+ (ActionTexts *)sharedInstance;

- (NSString *)failedText:(NSString *)actionName;
- (NSString *)successText:(NSString *)actionName;
- (NSString *)titleText:(NSString *)actionName;
- (NSString *)text:(NSString *)actionName;
- (NSString *)translateError:(NSString *)actionName;
- (NSString *)translateErrorType:(NSString *)actionName;
- (NSDictionary *)translateErrorClass:(NSString *)actionName;

- (NSString *)userName:(NSString *)name wantWishName:(NSString *)wishName;
- (NSString *)userName:(NSString *)name executeWishName:(NSString *)wishName;

@end
