//
//  Errors.m
//  Startwish
//
//  Created by marsohod on 11/02/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ActionTexts.h"

@interface ActionTexts ()
@property (nonatomic) NSDictionary *ru;
@property (nonatomic) NSDictionary *defaultLang;
@end

@implementation ActionTexts

typedef enum {
    ru = 0
} LangTypes;
/*
 * Events titles inside EventTableViewCell.m
 */

+ (ActionTexts *)sharedInstance
{
    static dispatch_once_t onceToken;
    static ActionTexts *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[ActionTexts alloc] init];
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        [self initRU];
        
        self.defaultLang = self.ru;
    }
    
    return self;
}

- (NSDictionary *)getFailed:(LangCode)code
{

    switch (code) {
        case ru:
            return @{
                     @"default"                    :   @"Ошибка",
                     @"default2"                   :   @"Неизвестная ошибка",
                     @"WishImplement"              :   @"Исполнить не удалось",
                     @"WishDelete"                 :   @"Удалить не удалось",
                     @"WishRewish"                 :   @"Добавить желание не удалось",
                     @"WishEmptyNameFull"          :   @"Название желания не заполнено",
                     @"NotLink"                    :   @"Ссылка в неверном формате",
                     @"CategoryNotSelected"        :   @"Необходимо выбрать категорию",
                     @"WishlistNotSelected"        :   @"Необходимо выбрать вишлист",
                     @"WishlistEmptyNameFull"      :   @"Название вишлиста не заполнено",
                     @"WishlistNameToShort"        :   @"Название вишлиста меньше 3 символов",
                     @"WishlistEmptyName"          :   @"Не заполнено название",
                     @"WishlistDelete"             :   @"Вишлист не удалён",
                     @"TryRemoveLastWishlist"      :   @"Не рекомендуется удалять последний вишлист",
                     @"EmailSend"                  :   @"Письмо не отправлено",
                     @"AccessPhotosDeny"           :   @"Нет доступа к галереи",
                     @"LocationDoesNotDetermine"   :   @"Не удалось опеределить местоположение",
                     @"FacebookPost"               :   @"Не удалось добавить запись Facebook",
                     @"FacebookAccess"             :   @"Не удалось подключить соц.сеть",
                     @"FacebookAccessPublish"      :   @"Нет прав на публикацию поста",
                     @"InstagramAccess"            :   @"У вас нет приложения Instagram",
                     @"TwitterAccess"              :   @"Не удалось авторизоваться в Twitter",
                     @"TwitterPost"                :   @"Не удалось обновить запись в Twitter",
                     @"VKAccess"                   :   @"Не удалось подключить соц.сеть",
                     @"VKPost"                     :   @"Не удалось добавить запись VK",
                     @"VKAuth"                     :   @"У аккаунта не указан email. Вход невозможен",
                     @"VKAuthDeny"                 :   @"Нет доступа к аккаунту",
                     @"VKAuthEmailToOtherUser"     :   @"Этот пользователь VK привязан к другому профилю",
                     @"VKAuthEnterEmailToOtherUser":   @"Email занят другим пользователем. Авторизуйтесь через email и привяжите соцсеть к аккаунту в настройках",
                     @"ContactsSave"               :   @"Не удалось сохранить контакты",
                     @"PasswordsNotEqual"          :   @"Пароли не совпадают",
                     @"PasswordNotEnter"           :   @"Забыли ввести пароль",
                     @"PasswordOldNotEnter"        :   @"Забыли ввести cтарый пароль",
                     @"PasswordNewNotEnter"        :   @"Забыли ввести новый пароль",
                     @"PasswordMustBeMore6letters" :   @"Пароль должен быть больше 6 символов",
                     @"EmailNotEnter"              :   @"Введите email",
                     @"EmailBad"                   :   @"Некорректный email адрес",
                     @"SocialNetworkNotSelected"   :   @"Выберите хотя бы одну соц. сеть",
                     @"Logout"                     :   @"Выйти не удалось",
                     @"NoInternetConnection"       :   @"Нет доступа к сети",
                     @"Subscribe"                  :   @"Не удалось подписаться",
                     @"Unsubscribe"                :   @"Не удалось отписаться",
                     @"DomainIsInvalid"            :   @"Короткий адрес может содержать цифры, символы латинского алфавита и \"-\" \"_\"",
                     @"EmptyUserName"              :   @"Необходимо заполнить или Имя или Фамилию",
                     @"UploadLargeImage"           :   @"Попытка загрузить слишком большую картинку. Выберите другую",
                     @"SendSpam"                   :   @"Не удалось отправить отзыв",
                     @"AppAuth"                    :   @"Проверьте соединение с интернетом или перезагрузите приложение"
                     };
    }
    
    return @{};
}

- (NSDictionary *)getSuccess:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                      @"default"            :   @"Получилось",
                      @"WishCreate"         :   @"Желание добавлено",
                      @"WishImplement"      :   @"Исполнилось!",
                      @"WishDelete"         :   @"Удалено",
                      @"WishRewish"         :   @"Желание добавлено",
                      @"Settings"           :   @"Настройки сохранены",
                      @"EmailSend"          :   @"Письмо отправлено",
                      @"MailSendToEmail"    :   @"Письмо отправлено",
                      @"EmailDraftSave"     :   @"Сохранено в черновики",
                      @"PhotoSaved"         :   @"Изображение сохранено",
                      @"EmailShare"         :   @"Желание отправлено",
                      @"SocialShare"        :   @"Желание отправлено",
                      @"TwitterPost"        :   @"Запись отправлена в Twitter",
                      @"FacebookPost"       :   @"Запись отправлена в Facebook",
                      @"VKPost"             :   @"Запись отправлена в VK",
                      @"ContactsSave"       :   @"Контакты сохранены",
                      @"PasswordUpdated"    :   @"Пароль обновлён",
                      @"Executed"           :   @"Исполнено!\r",        //\r нужно
                      @"SendSpam"           :   @"Ваше обращение зафиксировано. Мы рассмотрим его в течение суток. Спасибо.",
                      @"CopyProfileUrl"     :   @"Ссылка на профиль скопирована",
                      @"CopyWishUrl"        :   @"Ссылка на желание скопирована",
                      @"CopyWishlistUrl"    :   @"Ссылка на вишлист скопирована",
                    };
    }
    
    return @{};
}

- (NSDictionary *)getTitleTexts:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                    @"default"                   :   @"Что бум делать?",
                    @"Attention"                 :   @"Внимание",
                    @"WishDelete"                :   @"Удаление желания",
                    @"SendRequest"               :   @"Посылаю данные...", //deprecated
                    @"AccessLocation"            :   @"Доступ к Геолокации",
                    @"AccessCamera"              :   @"Доступ к Камере",
                    @"AccessContacts"            :   @"Доступ к Контактам",
                    @"AccessPhotos"              :   @"Доступ к Фотографиям",
                    @"AccessMicrophone"          :   @"Доступ к Микрофону",
                    @"SelectImage"               :   @"Выбрать изображение",
                    @"AccessRejected"            :   @"Не сейчас",
                    @"AccessApproved"            :   @"Разрешить",
                    @"ActionSheetSelectCamera"   :   @"Cнять",
                    @"ActionSheetSelectPhotos"   :   @"Выбрать в кулуарах",
                    @"VKAuthDeny"                :   @"Не вышло авторизоваться",
                    @"Close"                     :   @"Закрыть",
                    @"Cancel"                    :   @"Отмена",
                    @"InSettings"                :   @"В Настройки",
                    @"GoTo"                      :   @"Перейти",
                    @"Edit"                      :   @"Редактировать",
                    @"Save"                      :   @"Сохранить",
                    @"Create"                    :   @"Создать",
                    @"Delete"                    :   @"Удалить",
                    @"Remove"                    :   @"Убрать",
                    @"Execute"                   :   @"Исполнить",
                    @"Want"                      :   @"Хочу",
                    @"WantImportant"             :   @"Хочу!",
                    @"Send"                      :   @"Отправить",
                    @"Add"                       :   @"Добавить",
                    @"Comment"                   :   @"Комментировать",
                    @"Subscribe"                 :   @"Подписаться",
                    @"Unsubscribe"               :   @"Подписан",
                    @"FacebookPost"              :   @"Рассказать о желании через Facebook",
                    @"TwitterPost"               :   @"Запостить в Twitter",
                    @"InstagramStartwish"        :   @"@startwish_ru",
                    @"WishlistEdit"              :   @"Редактировать вишлист",
                    @"WishlistCreate"            :   @"Создать вишлист",
                    @"WishlistDelete"            :   @"Что делаем с желаниями из вишлиста?",
                    @"WishlistDeleteWishDelete"  :   @"Удаляем",
                    @"WishlistDeleteWishImplement":  @"Исполняем",
                    @"WishlistDeleteWishMove"    :   @"Перемещаем в другой вишлист",
                    @"WishlistEmpty"             :   @"В вишлисте \r нет желаний",
                    @"EmptyViewDesc"             :   @"Самое время их загадать",
                    @"EmptyFriends"              :   @"Самое время их найти",
                    @"EmptyFeed"                 :   @"Сейчас лента пуста",
                    @"EmptyFeedDescription"      :   @"Найдите друзей, чтобы быть вкурсе их желаний",
                    @"EmptyEvents"               :   @"Событий пока нет",
                    @"EmptyEventsDescription"    :   @"Подпишитесь на друзей, чтобы знать что у них происходит",
                    @"YourComment"               :   @"Ваш комментарий",
                    @"AddPicture"                :   @"Добавьте картинку",
                    @"SavePicture"               :   @"Сохранить картинку",
                    @"SocialShare"               :   @"Поделиться в соц. сетях",
                    @"SendByEmail"               :   @"Отправить по email",
                    @"Spam"                      :   @"Пожаловаться на контент",
                    @"AddWish"                   :   @"добавить желание",        //с маленькой буквы
                    @"CRCategories"              :   @"Выберите категории желаний, которые вам интересны",
                    @"CRPeople"                  :   @"Подпишитесь на активных пользователей, чтобы заполнить ленту новостей",
                    @"CopyProfileUrl"            :   @"Скопировать ссылку на профиль",
                    @"CopyWishUrl"               :   @"Скопировать ссылку желания",
                    @"Logout"                    :   @"Выйти",
                    @"BirthdayYesterday"         :   @"Вчера у пользователя был ДР!",
                    @"BirthdayToday"             :   @"Сегодня у пользователя ДР!",
                    @"BirthdayTomorrow"          :   @"Завтра у пользователя ДР!",
                    @"EventNewFriend"            :   @"Теперь вы друзья",
                    @"NotFound1"                 :   @"не найден",
                    @"NotFound2"                 :   @"не найдено",
                    @"This1"                     :   @"Такой",
                    @"This2"                     :   @"Такое",
                    @"This3"                     :   @"Такая",
                };
    }
    
    return @{};
}

- (NSDictionary *)getTexts:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                    @"default"                       :   @"Тебе необходимо нажать на одну из кнопок",
                    @"WishDelete"                    :   @"Желание будет удалено. Продолжить?",
                    @"AccessLocationAllow"           :   @"Разрешить Startwish получить доступ к геолокации?",
                    @"AccessLocationDeny"            :   @"Добавьте доступ приложению Startwish к Геолокации в Настройках -> Приватность -> Геолокация",
                    @"AccessContactsAllow"           :   @"Предоставить Startwsih доступ к контактам?",
                    @"AccessContactsDeny"            :   @"Добавьте доступ приложению Startwish к Контактам в Настройках -> Приватность -> Контакты",
                    @"AccessCameraAllow"             :   @"Разрешить Startwish пользоваться камерой?",
                    @"AccessCameraDeny"              :   @"Добавьте доступ приложению Startwish к Камере в Настройках -> Приватность -> Камера",
                    @"AccessMicrophoneDeny"          :   @"Добавьте доступ приложению Startwish к Микрофону в Настройках -> Приватность -> Микрофон",
                    @"AccessPhotosDeny"              :   @"Добавьте доступ приложению Startwish к вашим фотограмиям в Настройках -> Приватность -> Фотографии",
                    @"AccessPhotosAllow"             :   @"Разрешить доступ к Camera Roll для приложения Startish?",
                    @"VKAuthDeny"                    :   @"Нет разрешение на использования VK",
                    @"EmailDenySend"                 :   @"Нет возможности отправлять email",
                    @"AttentionChangePhotoMode"      :   @"Если вы перейдете к фото-режиму, снятое видео будет удалено.\nВы действительно хотите перейти к фото съемке?",
                    @"AttentionCloseCamera"          :   @"Снятое видео будет удалено.\nВы действительно хотите выйти?",
                    @"FriendWantWishByEmail"         :   @"%@ хочет %@ <a href='%@wish/%u'>%@wish/%u</a>",
                    @"FriendWantWishByEmailSubject"  :   @"%@ хочет %@",
                    @"CRCategories"                  :   @"",
                    @"CRPeople"                      :   @"",
                    @"LogoutWithoutInternet"         :   @"Интернет соединение отсутствует. При выходе, возможно, вы будете продолжать получать Push-уведомления",
                    @"BadInputs"                     :   @"Неверно заполнены: ",
                    @"AlreadyExist"                  :   @"уже существует в системе",
                    @"NotFound"                      :   @"Не найден",
                    @"ServerError"                   :   @"Ошибка на сервера. Повторите попытку позже",
                    };
    }
    
    return @{};
}

- (NSDictionary *)getServerErrors:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                     @"default"         : @"",
                     /* Social Fields */
                     @"identifier"      : @"Идентификатор пользователя в сети",
                     @"email"           : @"Email",
                     @"sn_id"           : @"Тип сети",
                     @"sn_app_id"       : @"Тип приложения",
                     
                     /* User Fields */
                     @"avatar"          : @"Аватар",
                     @"birthday"        : @"День рождения",
                     @"city_id"         : @"Город",
                     @"country_id"      : @"Страна",
                     @"district_id"     : @"Район",
                     @"login"           : @"Email",
                     @"page_identity"   : @"Короткий адрес",
                     @"username"        : @"Имя, фамилия",
                     @"password"        : @"Пароль",
                     @"invalid_grant"   : @"Логин или пароль",
                     
                     /* Wish Fields */
                     @"price"           : @"",
                     @"exec_img_height" : @"",
                     @"exec_img_width"  : @"",
                     @"name"            : @"Название",
                     @"img_src"         : @"Картинка желания",
                     @"privacy"         : @"Приватность",
                     @"wishlist"        : @"Вишлист",
                     @"category"        : @"Категория",
                     @"description"     : @"Описание",
                     @"master_id"       : @"Желание"
                    };
    }
    
    return @{};
}

- (NSDictionary *)getErrorTypes:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                     @"default"         : @"",
                     @"SWE-40000"       : @"неверно заполнены",
                     @"SWE-40003"       : @"слишком длинная строка",
                     @"SWE-40004"       : @"слишком короткая строка",
                     };
    }
    
    return @{};
}

- (NSDictionary *)getErrorClass:(LangCode)code
{
    switch (code) {
        case ru:
            return @{
                     @"User"            : @{
                             @"name"    : @"Пользователь",
                             @"type"    : @"1"},
                     @"Wish"            : @{
                             @"name"    : @"Желание",
                             @"type"    : @"2"},
                     @"Wishlist"        : @{
                             @"name"    : @"Вишлист",
                             @"type"    : @"1"},
                     @"master_id"       : @{
                             @"name"    : @"Желание",
                             @"type"    : @"2"},
                     };
    }
    
    return @{};
}

- (void)initRU
{
    self.ru = @{
                @"text"         : [self getTexts:ru],
                @"titleText"    : [self getTitleTexts:ru],
                @"success"      : [self getSuccess:ru],
                @"failed"       : [self getFailed:ru],
                @"serverErrors" : [self getServerErrors:ru],
                @"errorTypes"   : [self getErrorTypes:ru],
                @"errorClass"   : [self getErrorClass:ru],
                };
}


#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
- (NSDictionary *)getLanguageDictionary
{
    NSString *language      = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    
    if ( [self respondsToSelector:NSSelectorFromString(language)] ) {
        return [self performSelector:NSSelectorFromString(language)];
    }
    
    return self.defaultLang;
}

- (NSString *)failedText:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"failed"][actionName] ? lang[@"failed"][actionName] : lang[@"failed"][@"default"];
}

- (NSString *)successText:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"success"][actionName] ? lang[@"success"][actionName] : lang[@"success"][@"default"];
}

- (NSString *)titleText:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"titleText"][actionName] ? lang[@"titleText"][actionName] : lang[@"titleText"][@"default"];
}

- (NSString *)text:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"text"][actionName] ? lang[@"text"][actionName] : lang[@"text"][@"default"];
}

- (NSString *)translateError:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"serverErrors"][actionName] ? lang[@"serverErrors"][actionName] : lang[@"serverErrors"][@"default"];
}

- (NSString *)translateErrorType:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"errorTypes"][actionName] ? lang[@"errorTypes"][actionName] : lang[@"errorTypes"][@"default"];
}

- (NSDictionary *)translateErrorClass:(NSString *)actionName
{
    NSDictionary *lang = [self getLanguageDictionary];
    
    return lang[@"errorClass"][actionName] ? lang[@"errorClass"][actionName] : @{};
}

#pragma clang diagnostic pop

- (NSString *)userName:(NSString *)name wantWishName:(NSString *)wishName
{
    return [NSString stringWithFormat:@"%@ хоч%@ %@ #startwish %@", name, [name isEqualToString:@"Я"] ? @"у" : @"ет", wishName, [[ActionTexts sharedInstance] titleText:@"InstagramStartwish"]];
}

- (NSString *)userName:(NSString *)name executeWishName:(NSString *)wishName;
{
    return [NSString stringWithFormat:@"%@ \"%@\" исполнилось #startwish %@", [name isEqualToString:@"Моё"] ? @"Моё желание" : [NSString stringWithFormat:@"Желание %@", name], wishName, [[ActionTexts sharedInstance] titleText:@"InstagramStartwish"]];
}


@end
