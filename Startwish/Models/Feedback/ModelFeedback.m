//
//  ModelFeedback.m
//  Startwish
//
//  Created by Pavel Makukha on 15/06/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelFeedback.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"
#import "NSString+Helper.h"


@implementation ModelFeedback

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"feedbackReasons"];
        
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"status"];
        
    });
    
    return __descriptor;
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

@end
