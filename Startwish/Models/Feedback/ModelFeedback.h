//
//  ModelFeedback.h
//  Startwish
//
//  Created by Pavel Makukha on 15/06/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "JSONAPIResourceBase.h"


@interface ModelFeedback : JSONAPIResourceBase

@property (nonatomic) NSInteger         status;
@property (nonatomic, strong, getter=getName) NSString  *name;

- (uint32_t)getId;

@end
