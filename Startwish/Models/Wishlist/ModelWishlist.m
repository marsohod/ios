//
//  ModelWishlist.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelWishlist.h"
#import "ModelCounters.h"
#import "Me.h"
#import "NSString+Helper.h"
#import "Settings.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelWishlist()

@property (nonatomic, strong) ModelCounters *counters;
@property (nonatomic, strong) NSString      *updated;
@property (nonatomic) NSInteger             status;

@end


@implementation ModelWishlist

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.created forKey:@"created"];
    [aCoder encodeObject:self.updated forKey:@"updated"];
    [aCoder encodeObject:self.desc forKey:@"desc"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.owner forKey:@"owner"];
    [aCoder encodeObject:self.img_src forKey:@"img_src"];
    [aCoder encodeObject:[NSNumber numberWithFloat:self.img_height] forKey:@"img_height"];
    [aCoder encodeObject:[NSNumber numberWithFloat:self.img_width] forKey:@"img_width"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.status] forKey:@"status"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.owner_id] forKey:@"owner_id"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.privacy] forKey:@"privacy"];
    [aCoder encodeObject:self.counters forKey:@"counters"];
    [aCoder encodeObject:self.wishes forKey:@"wishes"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super init]){
        self.ID                 = [aDecoder decodeObjectForKey:@"ID"];
        self.created            = [aDecoder decodeObjectForKey:@"created"];
        self.updated            = [aDecoder decodeObjectForKey:@"updated"];
        self.desc               = [aDecoder decodeObjectForKey:@"desc"];
        self.name               = [aDecoder decodeObjectForKey:@"name"];
        self.owner              = [aDecoder decodeObjectForKey:@"owner"];
        self.img_src            = [aDecoder decodeObjectForKey:@"img_src"];
        self.img_height         = [[aDecoder decodeObjectForKey:@"img_height"] floatValue];
        self.img_width          = [[aDecoder decodeObjectForKey:@"img_width"] floatValue];
        self.status             = [[aDecoder decodeObjectForKey:@"status"] integerValue];
        self.owner_id           = (uint32_t)[[aDecoder decodeObjectForKey:@"owner_id"] unsignedLongValue];
        self.privacy            = [[aDecoder decodeObjectForKey:@"privacy"] integerValue];
        self.counters           = [aDecoder decodeObjectForKey:@"counters"];
        self.wishes             = [aDecoder decodeObjectForKey:@"wishes"];
    }
    return self;
}


static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // TODO:
        // replace wishlSists by wishlists
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"wishlists"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"updated"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"desc" withJsonName:@"description"];
        [__descriptor addProperty:@"img_height"];
        [__descriptor addProperty:@"img_src"];
        [__descriptor addProperty:@"img_width"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"owner_id"];
        [__descriptor addProperty:@"privacy"];
        [__descriptor addProperty:@"owner"];
        [__descriptor hasMany:[ModelCounters class] withName:@"counters"];
        [__descriptor hasMany:[ModelWish class] withName:@"wishes"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelWishlist: { \r name: %@ \r desc: %@ \r id: %u \r created: %@ \r status: %ld \r img_src: %@ \r img_height: %f \r img_width: %f \r owner_id: %u \r privacy: %ld \r owner: %@ \r wishes: %@ \r}", [self getName], [self getDescription], [self getId], [self getCreatedDate], self.status, [self getCover], [self getCoverHeight], [self getCoverWidth], [self getOwnerId], [self getPrivacy], [self getOwner], [self getWishes]];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (NSString *)getPathToWishlist
{
    return [NSString stringWithFormat:@"%@profile/%zd/wishlists/%zd", [Settings sharedInstance].domain, [self getOwnerId], [self getId]];
}

- (NSInteger)getWishesCount
{
    return [self.counters getWishes];
}

- (BOOL)isMyWishlist
{
    return [Me isMyProfile:[self getOwnerId]];
}

- (void)reduceWishesCount:(BOOL)isReduce
{
    [self setWishesCount:[self getWishesCount] + (isReduce ? -1 : 1)];
}

- (void)setWishesCount:(NSUInteger)c
{
    [self.counters setWishes:c];
}

- (void)removeWishById:(uint32_t)wishId
{
    [self.wishes enumerateObjectsUsingBlock:^(ModelWish *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj getId] == wishId ) {
            [self.wishes removeObjectAtIndex:idx];
            *stop = YES;
        }
    }];
}

- (void)addWish:(ModelWish *)wish
{    
    [self.wishes addObject:wish];
}

- (NSMutableArray *)getWishes
{
    if ( !_wishes ) {
        _wishes = [[NSMutableArray alloc] init];
    }
    
    return _wishes;
}

- (ModelCounters *)getCounters
{
    if ( !_counters ) {
        _counters = [[ModelCounters alloc] init];
    }
    
    return _counters;
}

@end
