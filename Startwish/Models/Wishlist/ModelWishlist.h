//
//  ModelWishlist.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JSONAPIResourceBase.h"
#import "ModelWish.h"


@interface ModelWishlist : JSONAPIResourceBase

@property (nonatomic, strong, getter=getCreatedDate) NSString               *created;
@property (nonatomic, strong, getter=getDescription, setter=setDescription:) NSString *desc;
@property (nonatomic, strong, getter=getName, setter=setName:) NSString     *name;
@property (nonatomic, strong, getter=getOwner) id                           owner;
@property (nonatomic, strong, getter=getCover) NSString                     *img_src;
@property (nonatomic, getter=getCoverWidth) CGFloat                         img_height;
@property (nonatomic, getter=getCoverHeight) CGFloat                        img_width;
@property (nonatomic, getter=getOwnerId) uint32_t                           owner_id;
@property (nonatomic, getter=getPrivacy, setter=setPrivacy:) NSInteger      privacy;
@property (nonatomic, getter=getWishes, setter=setWishes:) NSMutableArray   *wishes;

- (uint32_t)    getId;
- (NSString *)  getPathToWishlist;
- (NSInteger)   getWishesCount;
- (BOOL)        isMyWishlist;

- (void)        addWish:(ModelWish *)wish;
- (void)        removeWishById:(uint32_t)wishId;
- (void)        reduceWishesCount:(BOOL)isReduce;

@end
