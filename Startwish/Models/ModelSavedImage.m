//
//  ModelSavedImage.m
//  Startwish
//
//  Created by Pavel Makukha on 15/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelSavedImage.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"

@interface ModelSavedImage()

@property (nonatomic, strong) NSDictionary *rawImage;

@end

@implementation ModelSavedImage

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"utils"];
        
//        [__descriptor addProperty:@"rawImage" withDescription:[[JSONAPIPropertyDescriptor alloc] initWithJsonName:@"image"]];
        [__descriptor addProperty:@"cover"];
        [__descriptor addProperty:@"original"];
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelSavedImage: { \r cover: %@ \r original: %@ \r}", [self getCover], [self getOriginal]];
}

- (NSString *)getCover
{
    return self.cover[@"src"];
}

- (NSInteger)getCoverWidth
{
    return [self.cover[@"width"] integerValue];
}

- (NSInteger)getCoverHeight
{
    return [self.cover[@"height"] integerValue];
}

- (NSString *)getOriginal
{
    return self.original[@"src"];
}

- (NSInteger)getOriginalWidth
{
    return [self.original[@"width"] integerValue];
}

- (NSInteger)getOriginalHeight
{
    return [self.original[@"height"] integerValue];
}

@end
