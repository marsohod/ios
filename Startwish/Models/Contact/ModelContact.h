//
//  ModelContact.h
//  Startwish
//
//  Created by Pavel Makukha on 11/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelContact : JSONAPIResourceBase

@property (nonatomic, getter=getKey)                        NSString    *key;
@property (nonatomic, getter=getValue, setter=setValue:)    NSString    *value;

+ (void)setValue:(id)value forKey:(NSString *)key inItems:(NSMutableArray *)items;
@end
