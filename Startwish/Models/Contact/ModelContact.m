//
//  ModelContact.m
//  Startwish
//
//  Created by Pavel Makukha on 11/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelContact.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelContact()

@property (nonatomic, strong) NSString  *created;
@property (nonatomic) NSInteger         status;
@property (nonatomic) uint32_t          user_id;

@end


@implementation ModelContact

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"contacts"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"user_id"];
        [__descriptor addProperty:@"key"];
        [__descriptor addProperty:@"value"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelContact: { \r key: %@ \r value: %@ \r id: %@ \r created: %@ \r}", self.key, self.value, self.ID, self.created];
}

- (NSString *)getValue
{
    return _value.length != 0 ? _value : @"";
}

+ (void)setValue:(id)value forKey:(NSString *)key inItems:(NSMutableArray *)items
{
    [items enumerateObjectsUsingBlock:^(ModelContact* _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj getKey] == key ) {
            [obj setValue:value];
            *stop = YES;
        }
    }];
}

@end
