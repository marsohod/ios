//
//  ModelUser.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelUser : JSONAPIResourceBase

@property (nonatomic, strong, readonly, getter=getRegistered)           NSString        *created;
@property (nonatomic, strong, getter=getDomain, setter=setDomain:)      NSString        *page_identity;
@property (nonatomic, strong, getter=getFullname, setter=setFullname:)  NSString        *username;
@property (nonatomic, strong, getter=getAvatar, setter=setAvatar:)      NSString        *avatar;
@property (nonatomic, strong, getter=getBirthday, setter=setBirthday:)  NSString        *birthday;
@property (nonatomic, strong, readonly, getter=getEmail)                NSString        *login;
@property (nonatomic, strong, getter=getWishlists)                      NSMutableArray  *wishlists;
@property (nonatomic, strong, getter=getSocials)                        NSMutableArray  *socials;
@property (nonatomic, strong, getter=getDescription)                    NSString        *aboutme;
@property (nonatomic, getter=getGender)                                 NSInteger       sex;
@property (nonatomic)                                                   uint32_t        country_id;
@property (nonatomic)                                                   uint32_t        city_id;
@property (nonatomic)                                                   uint32_t        district_id;

- (instancetype)initWithId:(uint32_t)userId;
- (instancetype)initWithDictionary:(NSDictionary *)dic;

- (uint32_t)    getId;
- (NSString *)  getName;
- (NSString *)  getCityName;
- (uint32_t)    getCityId;
- (NSString *)  getCountryName;
- (NSString *)  getDistrictName;
- (NSString *)  getPathToProfile;

/* counters and mutuality*/
- (NSInteger)   getCountSubscribers;
- (NSInteger)   getCountSubscriptions;
- (NSInteger)   getCountWishes;
- (NSInteger)   getCountWishesMutual;
- (NSInteger)   getCountWishesExecuted;
- (NSInteger)   getCountWishlists;
- (NSInteger)   getCountFriends;
- (NSUInteger)  getCountFriendsMutual;

- (void)        setSubscribed:(BOOL)state;
- (BOOL)        isPrivacyBirthday;
- (BOOL)        isPrivacyContact;
- (BOOL)        isPrivacyProfile;
- (BOOL)        isMan;
- (BOOL)        isSubscriber;
- (BOOL)        isFriend;
- (BOOL)        isMySubscription;
- (BOOL)        canSeeContacts;
- (BOOL)        canSeeProfile;
- (BOOL)        canSeeBirthday;

- (NSInteger)   increaseCountWishlists:(BOOL)increase;
- (NSInteger)   increaseCountWishes:(BOOL)increase;
- (NSInteger)   increaseCountWishes:(BOOL)increase byCount:(NSInteger)count;
- (NSInteger)   increaseCountWishesExecuted:(BOOL)increase;
- (NSInteger)   increaseCountWishesExecuted:(BOOL)increase byCount:(NSInteger)count;
- (NSInteger)   increaseCountFriends:(BOOL)increase;
- (NSInteger)   increaseCountSubscribers:(BOOL)increase;

- (NSDictionary *)getContactsList;
- (void)        setContactsList:(NSMutableArray *)items;
+ (NSInteger)   genderManValue;
+ (NSInteger)   genderWomenValue;

@end