//
//  ModelUserCounters.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelCounters.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@implementation ModelCounters

static JSONAPIResourceDescriptor *__descriptor = nil;

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.wishes_active] forKey:@"wishes_active"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.wishes_executed] forKey:@"wishes_executed"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.wishlists] forKey:@"wishlists"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.followers] forKey:@"followers"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.follows] forKey:@"follows"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.friends] forKey:@"friends"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.events_unread] forKey:@"events_unread"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.rewishes] forKey:@"rewishes"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.comments] forKey:@"comments"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]){
        self.ID                 = [aDecoder decodeObjectForKey:@"ID"];
        self.wishes_active      = [[aDecoder decodeObjectForKey:@"wishes_active"] integerValue];
        self.wishes_executed    = [[aDecoder decodeObjectForKey:@"wishes_executed"] integerValue];
        self.wishlists          = [[aDecoder decodeObjectForKey:@"wishlists"] integerValue];
        self.followers          = [[aDecoder decodeObjectForKey:@"followers"] integerValue];
        self.follows            = [[aDecoder decodeObjectForKey:@"follows"] integerValue];
        self.friends            = [[aDecoder decodeObjectForKey:@"friends"] integerValue];
        self.events_unread      = [[aDecoder decodeObjectForKey:@"events_unread"] integerValue];
        self.rewishes           = [[aDecoder decodeObjectForKey:@"rewishes"] integerValue];
        self.comments           = [[aDecoder decodeObjectForKey:@"comments"] integerValue];
    }
    
    return self;
}


+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"counters"];
        
        // for users
        [__descriptor addProperty:@"wishes_active"];
        [__descriptor addProperty:@"wishes_executed"];
        [__descriptor addProperty:@"wishlists"];
        [__descriptor addProperty:@"followers"];
        [__descriptor addProperty:@"follows"];
        [__descriptor addProperty:@"friends"];
        [__descriptor addProperty:@"events_unread"];
        
        // for wishes
        [__descriptor addProperty:@"rewishes"];
        [__descriptor addProperty:@"comments"];

    });

    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelUserCounters: { \r wishes_active: %ld \r wishes_executed: %ld \r wishlists: %ld \r followers: %ld \r follows: %ld \r friends: %ld \r events_unread: %ld \r}", [self getWishes], [self getWishesExecuted], [self getWishlists], [self getFollowers], [self getFollows], [self getFriends], [self getEventsUnread]];
}

@end
