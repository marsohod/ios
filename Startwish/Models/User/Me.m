//
//  Me.m
//  Startwish
//
//  Created by Pavel Makukha on 06/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "Me.h"
#import "NSString+Helper.h"


#define USER_NAME                           @"userName"
#define USER_DOMAIN                         @"userDomain"
#define USER_ID                             @"userId"
#define USER_AVATAR_URL                     @"userAvatarUrl"
#define USER_EMAIL                          @"userEmail"
#define USER_RATING                         @"userRating"
#define USER_BIRTHDAY                       @"userBirthday"
#define USER_CITY_ID                        @"userCityId"
#define USER_CITY_NAME                      @"userCityName"
#define USER_GENDER                         @"userGender"
#define USER_SHOW_BIRTHDAY                  @"userSettingsShowBirthday"
#define USER_SHOW_PROFILE                   @"userSettingsShowProfile"
#define USER_SHOW_CONTACTS                  @"userSettingsShowContacts"
#define USER_SUBSCRIPTION_COUNT             @"userSubscriptionCount"
#define USER_WISHLISTS                      @"userWishlists"
#define USER_DONT_WANT_MORE_SUBSCRIPTIONS   @"userDontNeedMoreSubscriptions"
#define USER_SOCIALS                        @"userSociasl"

@implementation Me

const NSInteger minNeedSubscriptions = 5;
/*
+ (void)setProfile:(ModelUser *)user
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *obj = [NSKeyedArchiver archivedDataWithRootObject:user];
    
    [userDefaults setObject:obj forKey:USER];
    [userDefaults synchronize];
}

+ (ModelUser *)getProfile
{
    id user = [[NSUserDefaults standardUserDefaults] objectForKey:USER];
    
    return user ? [NSKeyedUnarchiver unarchiveObjectWithData:user] : nil;
}
*/




+ (BOOL)isMyProfile:(uint32_t)userId
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] unsignedLongValue] == userId;
}

+ (uint32_t)myProfileId
{
    return (uint32_t)[[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID] unsignedLongValue];
}

+ (NSString *)myAvatarUrl
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_AVATAR_URL];
}

+ (ModelUser*)myProfile
{
    return [[ModelUser alloc] initWithDictionary:@{
                                                   @"ID"            : [NSNumber numberWithUnsignedLong:[Me myProfileId]],
                                                   @"avatar"        : [Me myAvatarUrl],
                                                   @"username"      : [Me myProfileName],
                                                   @"page_identity" : [Me myProfileDomain] }];
}

+ (NSString *)myProfileDomain
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_DOMAIN] == NULL ? @"" : [[NSUserDefaults standardUserDefaults] objectForKey:USER_DOMAIN];
}

+ (NSString *)myProfileName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_NAME];
}

+ (NSString *)myProfileEmail
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_EMAIL];
}

+ (NSString *)myProfileRating
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_RATING];
}

+ (NSString *)myProfileBirthday
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:USER_BIRTHDAY] isEqualToString:@"0000-00-00"] || [[NSUserDefaults standardUserDefaults] objectForKey:USER_BIRTHDAY] == nil) {
        return @"";
    }
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_BIRTHDAY];
}

+ (NSNumber *)myProfileCityId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_CITY_ID];
}

+ (NSString *)myProfileCityName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:USER_CITY_NAME];
}

+ (NSInteger)myProfileGender
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_GENDER] integerValue];
}

+ (NSArray *)myWishlists
{
    NSArray *archiveArray = [[NSUserDefaults standardUserDefaults] objectForKey:USER_WISHLISTS];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:archiveArray.count];
    
    for (NSData *i in archiveArray) {
        id c = [NSKeyedUnarchiver unarchiveObjectWithData:i];
        [items addObject:c];
    }
    
    return items;
}

+ (NSInteger)myWishlistsCount
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_WISHLISTS] count];
}

+ (ModelSocial *)mySocialByToken:(NSString *)token
{
    NSArray *socials = [self mySocials];
    
    for ( ModelSocial *s in socials ) {
        if ( [[s getToken] isEqualToString:token] ) {
            return s;
        }
    }
    
    return nil;
}

+ (NSArray *)mySocials
{
    NSArray *archiveArray = [[NSUserDefaults standardUserDefaults] objectForKey:USER_SOCIALS];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:archiveArray.count];
    
    for (NSData *i in archiveArray) {
        id c = [NSKeyedUnarchiver unarchiveObjectWithData:i];
        [items addObject:c];
    }
    
    return items;
}

+ (BOOL)amIMan
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_GENDER] integerValue] == [ModelUser genderManValue];
}

+ (NSInteger)mySubscriptionsCount
{
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    [userDefaults setObject: [NSNumber numberWithInteger: 0] forKey:USER_SUBSCRIPTION_COUNT];
//    [userDefaults synchronize];
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_SUBSCRIPTION_COUNT] integerValue];
}

+ (NSInteger)needMoreSubscriptions
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if ( [self mySubscriptionsCount] < minNeedSubscriptions && (![userDefaults objectForKey:USER_DONT_WANT_MORE_SUBSCRIPTIONS] || ![[userDefaults objectForKey:USER_DONT_WANT_MORE_SUBSCRIPTIONS] boolValue] ) ) {
        return minNeedSubscriptions - [self mySubscriptionsCount];
    }
    
    return 0;
}

+ (void)dontNeedMoreSubsciptions
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:USER_DONT_WANT_MORE_SUBSCRIPTIONS];
    [userDefaults synchronize];
}

+ (NSInteger)minSubscriptions
{
    return minNeedSubscriptions;
}

+ (void)increaseSubscriptionCount:(BOOL)isIncrease
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject: [NSNumber numberWithInteger: [self mySubscriptionsCount] + (isIncrease ? 1 : -1)] forKey:USER_SUBSCRIPTION_COUNT];
    [userDefaults synchronize];
}

+ (BOOL)isDomainValid:(NSString *)domain
{
    NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:@"^[0-9a-z_-]+$" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if( [regExp numberOfMatchesInString:domain options:0 range:NSMakeRange(0, [domain length])] == 1 || [domain isEqualToString:@""] ) {
        return YES;
    }
    
    return NO;
}

+ (void)setMyWishlist:(NSArray *)items
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:items.count];
    
    for (id i in items) {
        NSData *obj = [NSKeyedArchiver archivedDataWithRootObject:i];
        [archiveArray addObject:obj];
    }
    
    [userDefaults setObject:archiveArray forKey:USER_WISHLISTS];
    [userDefaults synchronize];
}

+ (void)setMySocials:(NSArray *)items
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:items.count];
    
    for (id i in items) {
        NSData *obj = [NSKeyedArchiver archivedDataWithRootObject:i];
        [archiveArray addObject:obj];
    }
    
    [userDefaults setObject:archiveArray forKey:USER_SOCIALS];
    [userDefaults synchronize];

}

+ (void)setSocial:(id)social
{
    if ( !social ) {
        return;
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSData *obj = [NSKeyedArchiver archivedDataWithRootObject:social];
    
    [userDefaults setObject:@[obj] forKey:USER_SOCIALS];
    [userDefaults synchronize];
}

+ (void)unsetSocialById:(uint32_t)socialId
{
    NSMutableArray *socials = [[NSMutableArray alloc] initWithArray:[self mySocials]];
    
    for ( ModelSocial *s in socials ) {
        if ( [s getId] == socialId ) {
            [socials removeObject:s];
            break;
        }
    }
    
    [self setMySocials:socials];
}

+ (void)setPrivacySettings:(NSDictionary *)settings
{
    [Me setProfileSettingsShowBirthday:(settings[@"private_birthday"] != (id)[NSNull null] ? [settings[@"private_birthday"] boolValue] : 0)
                           showProfile:(settings[@"private_contacts"] != (id)[NSNull null] ? [settings[@"private_contacts"] integerValue] : 0)
                          showContacts:(settings[@"private_profile"] != (id)[NSNull null] ? [settings[@"private_profile"] integerValue] : 0)];
}

+ (void)setProfileSettingsShowBirthday:(BOOL)birtday
                           showProfile:(NSInteger)profile
                          showContacts:(NSInteger)contacts
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject: [NSNumber numberWithBool:birtday] forKey:USER_SHOW_BIRTHDAY];
    [userDefaults setObject: [NSNumber numberWithInteger:profile] forKey:USER_SHOW_PROFILE];
    [userDefaults setObject: [NSNumber numberWithInteger:contacts] forKey:USER_SHOW_CONTACTS];
    [userDefaults synchronize];
}

+ (void)setProfileLocation:(NSString *)cityName cityId:(NSNumber *)cityId
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject: cityName forKey:USER_CITY_NAME];
    [userDefaults setObject: cityId forKey:USER_CITY_ID];
    [userDefaults synchronize];
    
}

+ (void)setProfileAfterLogin:(ModelUser *)user
{
    [self setProfile: user
           withEmail: [user getEmail]
            cityName: [user getCityName]
              cityId: [NSNumber numberWithUnsignedLong:[user getCityId]]
            birthday: [user getBirthday]];
    
    [self setProfileSettingsShowBirthday: [user isPrivacyBirthday]
                             showProfile: [user isPrivacyProfile]
                            showContacts: [user isPrivacyContact]];
    
    [self setMyWishlist:[user getWishlists]];
    
    [self setMySocials:[user getSocials]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STProfileUpdate" object:nil];
}

+ (void)setProfile:(ModelUser *)user withEmail:(NSString *)email
{
    [self setProfile:user withEmail:email cityName:[user getCityName] cityId:[NSNumber numberWithUnsignedLong:0] birthday:[user getBirthday]];
}

//TODO:
// Не везде поправил тип юзера
+ (void)setProfile:(ModelUser *)user withEmail:(NSString *)email cityName:(NSString *)cityName cityId:(NSNumber *)cityId birthday:(NSString *)birthday
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject: ([user getDomain] != (id)[NSNull null] ? [user getDomain] : @"") forKey:USER_DOMAIN];
    [userDefaults setObject: [user getFullname] forKey:USER_NAME];
    
    if ( email != nil ) {
        [userDefaults setObject: email forKey:USER_EMAIL];
    }
    
    [userDefaults setObject: [user getAvatar] forKey:USER_AVATAR_URL];
//    [userDefaults setObject: [NSString stringWithFormat:@"%ld", (long)[user getRating]] forKey:USER_RATING];
    [userDefaults setObject: [NSNumber numberWithUnsignedLong: [user getId]] forKey:USER_ID];
    
    [userDefaults setObject: [NSNumber numberWithInteger:[user getGender]] forKey:USER_GENDER];
    if ( ![cityName isEqualToString: @""] ) {
        [userDefaults setObject: cityName forKey:USER_CITY_NAME];
        [userDefaults setObject: cityId forKey:USER_CITY_ID];
    }
    
    [userDefaults setObject: [NSNumber numberWithInteger:[user getCountSubscriptions]] forKey:USER_SUBSCRIPTION_COUNT];
    
    [userDefaults setObject: birthday forKey:USER_BIRTHDAY];
    [userDefaults synchronize];
}

+ (void)cleanProfile
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject: @"" forKey:USER_DOMAIN];
    [userDefaults setObject: @"" forKey:USER_NAME];
    [userDefaults setObject: @"" forKey:USER_AVATAR_URL];
    [userDefaults setObject: [NSString stringWithFormat:@"%u", 0] forKey:USER_RATING];
    [userDefaults setObject: [NSNumber numberWithUnsignedLong: 0] forKey:USER_ID];
    [userDefaults setObject: [NSNumber numberWithBool:NO] forKey:USER_GENDER];
    [userDefaults setObject: @"" forKey:USER_CITY_NAME];
    [userDefaults setObject: @"" forKey:USER_CITY_ID];
    [userDefaults setObject: @"" forKey:USER_BIRTHDAY];
    [userDefaults setObject: [NSNumber numberWithBool:YES] forKey:USER_SHOW_BIRTHDAY];
    [userDefaults setObject: [NSNumber numberWithInteger:0] forKey:USER_SHOW_PROFILE];
    [userDefaults setObject: [NSNumber numberWithInteger:0] forKey:USER_SHOW_CONTACTS];
    [userDefaults setObject: @[] forKey:USER_WISHLISTS];
    [userDefaults setObject: [NSNumber numberWithBool:NO] forKey:USER_DONT_WANT_MORE_SUBSCRIPTIONS];
    
    [userDefaults synchronize];
}

+ (NSInteger)mySettingsShowProfile
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_SHOW_PROFILE] integerValue];
}

+ (NSInteger)mySettingsShowContacts
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_SHOW_CONTACTS] integerValue];
}

+ (BOOL)mySettingsShowBirthday
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:USER_SHOW_BIRTHDAY] boolValue];
}

+ (BOOL)hasDefaultAvatar
{
    NSString *avatar = [[NSUserDefaults standardUserDefaults] objectForKey:USER_AVATAR_URL];
    
    NSRegularExpression *regExp = [[NSRegularExpression alloc] initWithPattern:@"break_cover\\.png$" options:NSRegularExpressionCaseInsensitive error:nil];
    
    if( [regExp numberOfMatchesInString:avatar options:0 range:NSMakeRange(0, [avatar length])] == 1 ) {
        return YES;
    }
    
    return NO;
}

@end
