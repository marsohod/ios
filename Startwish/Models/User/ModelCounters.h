//
//  ModelUserCounters.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelCounters : JSONAPIResourceBase

@property (nonatomic, getter=getWishes, setter=setWishes:) NSInteger                    wishes_active;
@property (nonatomic, getter=getWishesExecuted, setter=setWishesExecuted:) NSInteger    wishes_executed;
@property (nonatomic, getter=getWishlists, setter=setWishlists:) NSInteger              wishlists;
@property (nonatomic, getter=getFollowers, setter=setFollowers:) NSInteger              followers;
@property (nonatomic, getter=getFollows, setter=setFollows:) NSInteger                  follows;
@property (nonatomic, getter=getFriends, setter=setFriends:) NSInteger                  friends;
@property (nonatomic, getter=getEventsUnread, setter=setEventsUnread:) NSInteger        events_unread;
@property (nonatomic, getter=getRewishes, setter=setRewishes:) NSUInteger               rewishes;
@property (nonatomic, getter=getComments, setter=setComments:) NSUInteger               comments;

@end
