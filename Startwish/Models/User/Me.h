//
//  Me.h
//  Startwish
//
//  Created by Pavel Makukha on 06/08/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "ModelUser.h"
#import "ModelSocial.h"


@interface Me : NSObject

//+ (void)        setProfile:(ModelUser *)user;
//+ (ModelUser *) getProfile;

+ (BOOL)        isMyProfile:(uint32_t)userId;
+ (uint32_t)    myProfileId;
+ (BOOL)        hasDefaultAvatar;
+ (NSString *)  myAvatarUrl;
+ (ModelUser*)  myProfile;
+ (NSString *)  myProfileDomain;
+ (NSString *)  myProfileName;
+ (NSString *)  myProfileEmail;
+ (NSString *)  myProfileRating;
+ (NSString *)  myProfileBirthday;
+ (NSNumber *)  myProfileCityId;
+ (NSString *)  myProfileCityName;
+ (NSInteger)   myProfileGender;
+ (NSArray *)   myWishlists;
+ (ModelSocial*)mySocialByToken:(NSString *)token;
+ (NSInteger)   myWishlistsCount;
+ (NSInteger)   mySettingsShowProfile;
+ (NSInteger)   mySettingsShowContacts;
+ (BOOL)        mySettingsShowBirthday;
+ (NSInteger)   mySubscriptionsCount;

+ (void)        dontNeedMoreSubsciptions;
+ (NSInteger)   needMoreSubscriptions;
+ (NSInteger)   minSubscriptions;

+ (BOOL)isDomainValid:(NSString *)domain;
+ (BOOL)amIMan;
+ (void)increaseSubscriptionCount:(BOOL)isIncrease;

+ (void)setMyWishlist:(NSArray *)items;
+ (void)setProfileAfterLogin:(ModelUser *)user;
+ (void)setProfile:(ModelUser *)user withEmail:(NSString *)email;
+ (void)setProfile:(ModelUser *)user withEmail:(NSString *)email cityName:(NSString *)cityName cityId:(NSNumber *)cityId birthday:(NSString *)birthday;
+ (void)setProfileLocation:(NSString *)cityName cityId:(NSNumber *)cityId;
+ (void)setProfileSettingsShowBirthday:(BOOL)birtday
                           showProfile:(NSInteger)profile
                          showContacts:(NSInteger)contacts;
+ (void)setPrivacySettings:(NSDictionary *)settings;
+ (void)setSocial:(id)social;
+ (void)unsetSocialById:(uint32_t)socialId;
+ (void)cleanProfile;

@end
