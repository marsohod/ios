//
//  ModelUser.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelUser.h"
#import "ModelContact.h"
#import "ModelCounters.h"
#import "ModelMutuality.h"
#import "ModelSocial.h"
#import "ModelCity.h"
#import "ModelCountry.h"
#import "ModelCity.h"
#import "ModelDistrict.h"
#import "ModelWishlist.h"
#import "ModelSettings.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelUser()

@property (nonatomic, strong) NSString  *identity;
@property (nonatomic) NSInteger         status;
@property (nonatomic, strong) ModelCounters         *counters;
@property (nonatomic, strong) ModelMutuality        *mutuality;
@property (nonatomic, strong) ModelCountry          *country;
@property (nonatomic, strong) ModelCity             *city;
@property (nonatomic, strong) ModelDistrict         *district;
@property (nonatomic, strong) NSMutableArray        *settings;
@property (nonatomic, strong) NSMutableArray        *contacts;

@end

@implementation ModelUser

const NSInteger manValue    = 2;
const NSInteger womenValue  = 1;
/*
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.created forKey:@"created"];
    [aCoder encodeObject:self.identity forKey:@"identity"];
    [aCoder encodeObject:self.counters forKey:@"counters"];
    [aCoder encodeObject:self.mutuality forKey:@"mutuality"];
    [aCoder encodeObject:self.country forKey:@"country"];
    [aCoder encodeObject:self.city forKey:@"city"];
    [aCoder encodeObject:self.district forKey:@"district"];
    [aCoder encodeObject:self.socials forKey:@"socials"];
    [aCoder encodeObject:self.settings forKey:@"settings"];
    [aCoder encodeObject:self.contacts forKey:@"contacts"];
    [aCoder encodeObject:self.page_identity forKey:@"page_identity"];
    [aCoder encodeObject:self.username forKey:@"username"];
    [aCoder encodeObject:self.avatar forKey:@"avatar"];
    [aCoder encodeObject:self.birthday forKey:@"birthday"];
    [aCoder encodeObject:self.login forKey:@"login"];
    [aCoder encodeObject:self.wishlists forKey:@"wishlists"];
    [aCoder encodeObject:self.aboutme forKey:@"aboutme"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.sex] forKey:@"sex"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.country_id] forKey:@"country_id"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.city_id] forKey:@"city_id"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.district_id] forKey:@"district_id"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.status] forKey:@"status"];
}

- (id)initWithCoder:(NSCoder *)aDecoder {

    if(self = [super init]){
        self.ID                 = [aDecoder decodeObjectForKey:@"ID"];
        _created                = [aDecoder decodeObjectForKey:@"created"];
        self.identity           = [aDecoder decodeObjectForKey:@"identity"];
        self.counters           = [aDecoder decodeObjectForKey:@"counters"];
        self.mutuality          = [aDecoder decodeObjectForKey:@"mutuality"];
        self.country            = [aDecoder decodeObjectForKey:@"country"];
        self.city               = [aDecoder decodeObjectForKey:@"city"];
        self.district           = [aDecoder decodeObjectForKey:@"district"];
        self.socials            = [aDecoder decodeObjectForKey:@"socials"];
        self.settings           = [aDecoder decodeObjectForKey:@"settings"];
        self.contacts           = [aDecoder decodeObjectForKey:@"contacts"];
        self.page_identity      = [aDecoder decodeObjectForKey:@"page_identity"];
        self.username           = [aDecoder decodeObjectForKey:@"username"];
        self.avatar             = [aDecoder decodeObjectForKey:@"avatar"];
        self.birthday           = [aDecoder decodeObjectForKey:@"birthday"];
        _login                  = [aDecoder decodeObjectForKey:@"login"];
        self.wishlists          = [aDecoder decodeObjectForKey:@"wishlists"];
        self.aboutme            = [aDecoder decodeObjectForKey:@"aboutme"];
        self.sex                = [[aDecoder decodeObjectForKey:@"sex"] floatValue];
        self.country_id         = (uint32_t)[[aDecoder decodeObjectForKey:@"country_id"] unsignedLongValue];
        self.city_id            = (uint32_t)[[aDecoder decodeObjectForKey:@"city_id"] unsignedLongValue];
        self.district_id        = (uint32_t)[[aDecoder decodeObjectForKey:@"district_id"] unsignedLongValue];
        self.status             = [[aDecoder decodeObjectForKey:@"status"] integerValue];
    }
    
    return self;
}
*/

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"users"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"page_identity"];
        [__descriptor addProperty:@"login"];
        [__descriptor addProperty:@"username"];
        [__descriptor addProperty:@"identity"];
        [__descriptor addProperty:@"avatar"];
        [__descriptor addProperty:@"birthday"];
        [__descriptor addProperty:@"sex"];
        [__descriptor addProperty:@"country_id"];
        [__descriptor addProperty:@"city_id"];
        [__descriptor addProperty:@"district_id"];
        [__descriptor addProperty:@"aboutme"];
        [__descriptor addProperty:@"status"];
        [__descriptor hasOne:[ModelCity class] withName:@"city"];
        [__descriptor hasOne:[ModelCountry class] withName:@"country"];
        [__descriptor hasOne:[ModelDistrict class] withName:@"district"];
        [__descriptor hasOne:[ModelCounters class] withName:@"counters"];
        [__descriptor hasOne:[ModelMutuality class] withName:@"mutuality"];
        [__descriptor hasMany:[ModelSettings class] withName:@"settings"];
        [__descriptor hasMany:[ModelWishlist class] withName:@"wishlists"];
        [__descriptor hasMany:[ModelSocial class] withName:@"socials"];
        [__descriptor hasMany:[ModelContact class] withName:@"contacts"];
        
        
    });
    
    return __descriptor;
}



- (instancetype)initWithId:(uint32_t)userId
{
    if( self = [super init] ) {
        self.ID = [NSNumber numberWithUnsignedLong:userId];
    }
    
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)dic
{
    if( self = [super init] ) {
        self.ID             = [dic objectForKey:@"ID"];
        self.avatar         = [dic objectForKey:@"avatar"];
        self.page_identity  = [dic objectForKey:@"page_identity"];
        self.username       = [dic objectForKey:@"username"];
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"ModelUser: {\r id: %u \r page_identiry: %@ \r login: %@ \r username: %@ \r avatar: %@ \r birthday: %@ \r sex: %ld \r country_id: %u \r city_id: %u \r district_id: %u \r aboutme: %@ \r status: %ld \r city: %@ \r country: %@ \r district: %@ \r counters: %@ \r settings: %@ \r wishlists: %@ \r socials: %@ \r contacts: %@ \r}", [self getId], [self getDomain], self.login, [self getFullname], [self getAvatar], [self getBirthday], [self getGender], self.country_id, self.city_id, self.district_id, [self getDescription], self.status, self.city, self.country, self.district, self.counters, self.settings, self.wishlists, self.socials, self.contacts];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (NSString *)getPathToProfile
{
    return ![self getDomain] ? [NSString stringWithFormat:@"profile/%zd", [self getId]] : [NSString stringWithFormat:@"%@", [self getDomain]];
}

- (uint32_t)getCityId
{
    if ( self.city != nil ) {
        NSLog(@"city: %@", self.city.ID);
        return [NSString stringToUint32_t:self.city.ID];
    }
    
    if ( self.city_id ) {
        return self.city_id;
    }
    
    return 0;
}

- (NSString *)getCityName
{
    if ( self.city ) {
        NSLog(@"city: %@", [self.city getName]);
        return [self.city getName];
    }
    
    return @"";
}

- (NSString *)getCountryName
{
    if ( self.country ) {
        NSLog(@"country: %@", [self.country getName]);
        return [self.country getName];
    }
    
    return @"";
}

- (NSString *)getDistrictName
{
    if ( self.district ) {
        NSLog(@"district: %@", [self.district getName]);
        return [self.district getName];
    }
    
    return @"";
}

- (NSInteger)getCountSubscribers
{
    return [self.counters getFollowers];
}

- (NSInteger)getCountSubscriptions
{
    return [self.counters getFollows];
}

- (NSInteger)getCountWishes
{
    return [self.counters getWishes];
}

- (NSInteger)getCountWishesMutual
{
    return [self.mutuality getWishesMutualCount];
}
- (NSInteger)getCountWishesExecuted
{
    return [self.counters getWishesExecuted];
}

- (NSInteger)getCountWishlists
{
    return [self.counters getWishlists];
}

- (NSInteger)getCountFriends
{
    return [self.counters getFriends];
}

- (NSUInteger)getCountFriendsMutual
{
    return [self.mutuality getFriendsMutualCount];
}

- (BOOL)isPrivacyBirthday
{
    return [ModelSettings privacyBirthday:self.settings];
}

- (BOOL)isPrivacyContact
{
    return [ModelSettings privacyContacts:self.settings];
}

- (BOOL)isPrivacyProfile
{
    return [ModelSettings privacyProfile:self.settings];
}

- (BOOL)isMan
{
    return [self getGender] == manValue;
}

- (BOOL)isSubscriber
{
    if ( [self isFriend] || [self getFriendshipStatus] == 3 ) {
        return YES;
    }
    
    return NO;
}

- (BOOL)isMySubscription
{
    return [self getFriendshipStatus] == 2;
}

- (BOOL)isFriend
{
    return [self getFriendshipStatus] == 1;
}

//const FRIENDSHIP_STATUS_NOBODY = 0;
//const FRIENDSHIP_STATUS_FRIEND = 1;
//const FRIENDSHIP_STATUS_SUBSCRIBER = 2;
//const FRIENDSHIP_STATUS_SUBSCRIPTION = 3;
- (NSInteger)getFriendshipStatus
{
    return [self.mutuality getFriendshipStatus];
}

- (void)setSubscribed:(BOOL)state
{
    switch ( [self getFriendshipStatus] ) {
        case 0:
            if ( state ) {
                [self.mutuality setFriendshipStatus:3];
            }
            break;
            
        case 1:
            if ( !state ) {
                [self.mutuality setFriendshipStatus:2];
            }
            break;
            
        case 2:
            if ( state ) {
                [self.mutuality setFriendshipStatus:1];
            }
            break;
            
        case 3:
            if ( !state ) {
                [self.mutuality setFriendshipStatus:0];
            }
            break;
            
        default:
            break;
    }
}

- (BOOL)canSeeContacts
{
    if ( [self isPrivacyContact] ) {
        return [self isFriend];
    }
    
    return YES;
}

- (BOOL)canSeeProfile
{
    if ( [self isPrivacyProfile] ) {
        return [self isFriend];
    }
    
    return YES;
}

- (BOOL)canSeeBirthday
{
    return ![self isPrivacyBirthday];
}


- (NSString *)getName
{
    NSArray *fio = [[self getFullname] componentsSeparatedByString:@" "];
    
    if ( [fio count] > 0 ) {
        return [NSString stringWithFormat:@"%@", [fio firstObject]];
    }
    
    return [self getFullname];
}

- (NSInteger)increaseCountWishlists:(BOOL)increase
{
    [self.counters setWishlists: [self.counters getWishlists] + ( increase ? 1 : -1 )];
    
    return [self getCountWishlists];
}

- (NSInteger)increaseCountWishes:(BOOL)increase
{
    return [self increaseCountWishes:increase byCount:1];
}

- (NSInteger)increaseCountWishes:(BOOL)increase byCount:(NSInteger)count
{
    [self.counters setWishes: [self.counters getWishes] + ( increase ? count : -count )];
    
    return [self getCountWishes];
    
}

- (NSInteger)increaseCountWishesExecuted:(BOOL)increase
{
    return [self increaseCountWishesExecuted:increase byCount:1];
}

- (NSInteger)increaseCountWishesExecuted:(BOOL)increase byCount:(NSInteger)count
{
    [self.counters setWishesExecuted:[self.counters getWishesExecuted] + ( increase ? count : -count )];
    
    return [self getCountWishesExecuted];
}

- (NSInteger)increaseCountFriends:(BOOL)increase
{
    [self.counters setFriends:[self.counters getFriends] + ( increase ? 1 : -1 )];
    
    return [self getCountFriends];
}

- (NSInteger)increaseCountSubscribers:(BOOL)increase
{
    [self.counters setFollowers:[self.counters getFollowers] + ( increase ? 1 : -1 )];
    
    return [self getCountSubscribers];
}

- (NSDictionary *)getContactsList
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];

    for ( ModelContact *i in _contacts ) {
        [d setObject:[i getValue] forKey:[i getKey]];
    }
    
    return d;
}

- (void)setContactsList:(NSMutableArray *)items
{
    self.contacts = items;
//    [items enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
//        [ModelContact setValue:obj forKey:key inItems:_contacts];
//    }];
}


+ (NSInteger)genderManValue
{
    return manValue;
}

+ (NSInteger)genderWomenValue
{
    return womenValue;
}



@end
