//
//  ModelMutuality.m
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelMutuality.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@implementation ModelMutuality

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"mutuality"];
        
        // for users
        [__descriptor addProperty:@"friends_mutual_count"];
        [__descriptor addProperty:@"friendship_status"];
        [__descriptor addProperty:@"wishes_mutual_count"];
        
        // for wishes
        [__descriptor addProperty:@"friends_want_count"];
        [__descriptor addProperty:@"has_wish"];
    });
    
    return __descriptor;
}

@end
