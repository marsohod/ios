//
//  ModelMutuality.h
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"

@interface ModelMutuality : JSONAPIResourceBase

// for user
@property (nonatomic, getter=getFriendsMutualCount) NSInteger    friends_mutual_count;
@property (nonatomic, getter=getFriendshipStatus, setter=setFriendshipStatus:) NSInteger    friendship_status;
@property (nonatomic, getter=getWishesMutualCount) NSInteger   wishes_mutual_count;

// for wish
@property (nonatomic, getter=getFriendsWantCount, setter=setFriendsWantCount:) NSInteger    friends_want_count;
@property (nonatomic, getter=getHasWish, setter=setHasWish:) BOOL                           has_wish;

@end
