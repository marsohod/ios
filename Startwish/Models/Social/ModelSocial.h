//
//  ModelSocial.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelSocial : JSONAPIResourceBase

@property (nonatomic) NSString                      *created;
@property (nonatomic, getter=getToken) NSString     *token;
@property (nonatomic, getter=getSnId) NSInteger     sn_id;
@property (nonatomic, getter=getStatus) NSInteger   status;
@property (nonatomic, getter=getSnAppId) uint32_t   sn_app_id;
@property (nonatomic, getter=getUserId) uint32_t    user_id;
@property (nonatomic, getter=getUserSnId) uint32_t  user_sn_id;

- (uint32_t)getId;

@end
