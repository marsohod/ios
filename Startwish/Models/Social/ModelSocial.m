//
//  ModelSocial.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelSocial.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@implementation ModelSocial

static JSONAPIResourceDescriptor *__descriptor = nil;

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.created forKey:@"created"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.status] forKey:@"status"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.sn_id] forKey:@"sn_id"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.sn_app_id] forKey:@"sn_app_id"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.user_id] forKey:@"user_id"];
    [aCoder encodeObject:[NSNumber numberWithUnsignedLong:self.user_sn_id] forKey:@"user_sn_id"];

    
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super init]){
        self.ID                 = [aDecoder decodeObjectForKey:@"ID"];
        _created                = [aDecoder decodeObjectForKey:@"created"];
        self.token              = [aDecoder decodeObjectForKey:@"token"];
        self.status             = [[aDecoder decodeObjectForKey:@"status"] integerValue];
        self.sn_id              = [[aDecoder decodeObjectForKey:@"sn_id"] integerValue];
        self.sn_app_id          = (uint32_t)[[aDecoder decodeObjectForKey:@"sn_app_id"] unsignedLongValue];
        self.user_id            = (uint32_t)[[aDecoder decodeObjectForKey:@"user_id"] unsignedLongValue];
        self.user_sn_id         = (uint32_t)[[aDecoder decodeObjectForKey:@"user_sn_id"] unsignedLongValue];
    }
    
    return self;
}

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"socials"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"sn_app_id"];
        [__descriptor addProperty:@"sn_id"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"token"];
        [__descriptor addProperty:@"user_id"];
        [__descriptor addProperty:@"user_sn_id"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelUserCounters: { \r id: %@ \r sn_app_id: %u \r sn_id: %ld \r status: %ld \r token: %@ \r user_id: %u \r user_sn_id: %u \r \r}", self.ID, self.sn_app_id, self.sn_id, self.status, self.token, self.user_id, self.user_sn_id];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

@end
