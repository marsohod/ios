//
//  ManagerNotification.h
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ManagerNotification : NSObject

+ (NSMutableArray *)    getObjects:(NSArray *)items;
+ (NSArray *)           getWishesId:(NSArray *)items;
+ (NSArray *)           getCommentsId:(NSArray *)items;
+ (NSArray *)           getUsersId:(NSArray *)items;

/*! Set wishes for notification with wishes type and return Array of wishes owners */
+ (NSArray *)           setObjects:(NSArray *)wishes toItems:(NSMutableArray *)items;

/*! Set comments notification like first comment to exist wishes and return Array of notifications */
+ (NSArray *)           setComments:(NSArray *)comments forWishes:(NSArray *)wishes toItems:(NSMutableArray *)items;

+ (NSMutableArray *)    removeDublicates:(NSMutableArray *)items;
//+ (NSArray *)getUsersIdFromUsersTypeEvent:(NSArray *)items;

@end
