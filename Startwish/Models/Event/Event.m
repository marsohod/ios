//
//  Event.m
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "Event.h"


@interface Event()
@property (nonatomic, strong) id eventCommentWish;
@end

@implementation Event
{
    NSDictionary    *item;
    id              objectOfEvent;
}

typedef enum {
    tEventWish,
    tEventUser,
    tEventComment,
    tEventStartwish
} EventType;

- (EventType)getEventType
{
    if ( [self sectorId] == 1 ) {
        return tEventComment;
    } else if ( [self sectorId] == 3 || [self sectorId] == 8 || [self sectorId] == 12 ) {
        return tEventUser;
    } else if ( [self sectorId] == 10 ) {
        return tEventWish;
    } else if ( [self sectorId] == 11 ) {
        return tEventStartwish;
    }
    
    return tEventWish;
}

- (BOOL)isEventComment
{
    return [self getEventType] == tEventComment;
}

- (BOOL)isEventWish
{
    return [self getEventType] == tEventWish;
}

- (BOOL)isEventUser
{
    return [self getEventType] == tEventUser;
}

- (BOOL)isEventStartwish
{
    return [self getEventType] == tEventStartwish;
}

- (Event *)initWithObject:(id)object
{
    self = [super init];
    
    if( self ) {
        [self setEvent:object];
    }
    
    return self;
}

- (void)setEvent:(NSDictionary *)event
{
    
    item = event[@"event"];
    if ( [self isEventComment] ) {
        // refactor
        // должен приходить не массив комментов, а конкретный коммент
        
        // TODO: 
//        objectOfEvent = nil;[[Comment alloc] initWithObject:event[@"item"]];
        
        if( event[@"item"] != (id)[NSNull null] && event[@"item"] != nil ) {
            if( event[@"item"][@"wish"] != (id)[NSNull null] && event[@"item"][@"wish"] != nil ) {
            }
        }
    }
    
}


- (uint32_t)getId
{
    uint32_t n;
    
    if( [item[@"id"] isKindOfClass:[NSNumber class]] ) {
        //            NSLog(@"id class %ld \r\r\r", [_item[@"id"] unsignedLongValue]);
        n = (uint32_t)[item[@"id"] unsignedLongValue];
    } else {
        //        NSLog(@"id %s", [_item[@"id"] UTF8String]);
        sscanf([item[@"id"] UTF8String], "%u", &n);
    }
    //    NSLog(@"uint: %u , id: %@", n, _item[@"id"]);
    return n;
}

- (NSString *)getDate
{
    return [NSString stringWithFormat:@"%@", item[@"created"]];
}

- (NSString *)getText
{
    return [NSString stringWithFormat:@"%@", objectOfEvent[@"text"]];
}

- (NSString *)getTitle
{
    return [NSString stringWithFormat:@"%@", objectOfEvent[@"title"]];
}

- (NSString *)getNetwork
{
    if( item[@"network"] != (id)[NSNull null] && item[@"network"] != nil ) {
        if( [item[@"network"] isEqualToString:@"fb"] ) {
            return @"Facebook";
        } else if( [item[@"network"] isEqualToString:@"vk"] ) {
            return @"Вконтакте";
        }
    }
    
    return @"";
}

- (NSString *)getUpdatedDate
{
    return item[@"updated"] != (id)[NSNull null] ? item[@"updated"] : @"";
}

- (ModelComment *)eventComment
{
    return (ModelComment *)objectOfEvent;
}

- (NSInteger)sectorId
{
    return item[@"sectorId"] != (id)[NSNull null] ? [item[@"sectorId"] integerValue] : 0;
}

@end
