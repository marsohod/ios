//
//  Event.h
//  Startwish
//
//  Created by marsohod on 26/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ModelComment.h"


@interface Event : NSObject

- (Event *)initWithObject:(id)object;

- (NSInteger)   sectorId;
- (uint32_t)    getId;
- (NSString *)  getDate;
- (NSString *)  getText;
- (NSString *)  getTitle;
- (NSString *)  getNetwork;
- (NSString *)  getUpdatedDate;


@end
