//
//  ManagerNotification.m
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ManagerNotification.h"
#import "ModelNotification.h"
#import "ModelWish.h"


@implementation ManagerNotification

+ (NSMutableArray *)getObjects:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];

    for ( ModelNotification* m in items ) {
        [a addObject:[m getObject] ? [m getObject] : @{}];
    }
        
    return a;
}

+ (NSArray *)getWishesId:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    [items enumerateObjectsUsingBlock:^(ModelNotification*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj isNotificationWish] ) {
            [a addObject:[NSNumber numberWithUnsignedLong:[obj getObjectId]]];
        } else if ( [obj isNotificationComment] ) {
//            [a addObject:[NSNumber numberWithUnsignedLong:[obj getCommentWishId]]];
        }
    }];
    
    return a;
}

+ (NSArray *)getCommentsId:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    [items enumerateObjectsUsingBlock:^(ModelNotification*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj isNotificationComment] ) {
            [a addObject:[NSNumber numberWithUnsignedLong:[obj getObjectId]]];
        }
    }];
    
    return a;
}

+ (NSArray *)getUsersId:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    [items enumerateObjectsUsingBlock:^(ModelNotification*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ( [obj isNotificationUser] ) {
//            NSLog(@"%d %d %d", [obj getId], [obj getObjectId], [obj getEmitterId]);
            [a addObject:[NSNumber numberWithUnsignedLong:[obj getEmitterId]]];
//            [a addObject:[NSNumber numberWithUnsignedLong:[obj getObjectId]]];
        } else if ( [obj isNotificationComment] ) {
            // TODO:
            // need get all users from all comment
            //            [a addObject:[NSNumber numberWithUnsignedLong:[obj getObjectWishId]]];
        }
    }];

    return a;
}

+ (NSArray *)setObjects:(NSArray *)objects toItems:(NSMutableArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    for ( ModelNotification *m in items ) {
        if ( [m isNotificationComment] ) {
            
            // insert only wish without comments
            for ( ModelWish* w in objects ) {
                if ( [m getObject] == nil && [w isKindOfClass:[ModelWish class]] && [w hasCommentId:[m getObjectId]] ) {
                    [m setObject:w];
                    [a addObject:[NSNumber numberWithUnsignedLong:[w getOwnerId]]];
                } else if ( [[m getObject] isKindOfClass:[ModelComment class]] ) {
                    [m setObject:nil];
                }
            }
        } else if ( [m isNotificationWish] ) {
            for ( ModelWish* w in objects ) {
                if ( [w getId] == [m getObjectId] ) {
                    [w setUpdated:[m getCreatedDate]];
                    
                    if ( [m isNotificationNewWish] ) {
                        ModelWish *ww = [w copy];
                        
                        [ww setDefaultStatus];
                        [m setObject:ww];
                    } else {
                        [m setObject:w];
                    }
                    
                    [a addObject:[NSNumber numberWithUnsignedLong:[w getOwnerId]]];
                }
            }
        } else if ( [m isNotificationUser] ) {
            for ( ModelUser* w in objects ) {
                if ( [w getId] == [m getObjectId] ) {
                    [m setObject:w];
                    [a addObject:[NSNumber numberWithUnsignedLong:[w getId]]];
                }
                
                if ( [w getId] == [m getEmitterId] ) {
                    [m setEmitter:w];
                    [a addObject:[NSNumber numberWithUnsignedLong:[w getId]]];
                }
            }
        }
    }

    
    return a;
}

+ (NSArray *)setComments:(NSArray *)comments forWishes:(NSArray *)wishes toItems:(NSMutableArray *)items
{
    for ( ModelNotification *m in items ) {
        if ( [m isNotificationComment] ) {
            for ( ModelComment* c in comments ) {
                if ( [c getId] == [m getObjectId] ) {
                    for ( ModelWish* w in wishes ) {
                        
                        // insert copy wish with one comment of event
                        if( [w getId] == [c getWishId] ) {
                            ModelWish *o = [w copy];
                            
                            [o setComments:[[NSMutableArray alloc] initWithObjects:c, nil]];
                            [m setObject:o];
                        }
                    }
                }
            }
        }
    }
    
    return nil;
}

+ (NSMutableArray *)removeDublicates:(NSMutableArray *)items
{
    for( NSInteger i = 0; i < [items count]; i++ ) {
        for( NSInteger j = i + 1; j < [items count]; j++ ) {
            if ( [items[i] getObject] ) {
                if ( [items[i] isEqualTypes:items[j]] ) {
                    if ( [items[i] isNotificationRelation] ) {
                        if ( [items[i] getEmitterId] == [items[j] getEmitterId] ) {
                            [items removeObjectAtIndex:j];
                            j--;
                        }
                    } else {
                        if ( [items[i] getObjectId] == [items[j] getObjectId] ) {
                            [items removeObjectAtIndex:j];
                            j--;
                        }
                    }
                }
            }
        }
    }
    
    return items;
}

//+ (NSArray *)getUsersIdFromUsersTypeEvent:(NSArray *)items
//{
//    NSMutableArray *a = [[NSMutableArray alloc] init];
//
//    [items enumerateObjectsUsingBlock:^(ModelNotification*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        if ( [obj isNotificationRelation] ) {
//            [a addObject:[NSNumber numberWithUnsignedLong:[obj getEmitterId]]];
//        }
//    }];
//    
//    return a;
//}

@end
