//
//  ModelNotification.h
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"
#import "ModelWish.h"
#import "ModelUser.h"
#import "ModelComment.h"
#import "ModelSocial.h"


@interface ModelNotification : JSONAPIResourceBase

@property (nonatomic, getter=getCreatedDate) NSString*              created;
@property (nonatomic, getter=getEmitterId) uint32_t                 emitter_id;
@property (nonatomic, getter=getObjectId) uint32_t                  object_id;
@property (nonatomic) uint32_t                                      receiver_id;
@property (nonatomic, getter=getEmitter ) id                        emitter;
@property (nonatomic, getter=getObject) id                          object;

- (uint32_t)    getId;
- (uint32_t)    getCommentWishId;
- (NSString *)  getNetwork;
- (NSString *)  getText;
- (NSString *)  getTitle;
- (NSString *)  getEventUserAvatar;
- (NSString *)  getEventUserName;
- (ModelUser *) getEventUser;
- (NSString *)  getUpdatedDate;

- (BOOL)isEqualTypes:(ModelNotification *)e;
- (BOOL)isNotificationComment;
- (BOOL)isNotificationNewWish;
- (BOOL)isNotificationWish;
- (BOOL)isNotificationUser;
- (BOOL)isNotificationUserRegister;
- (BOOL)isNotificationStartwish;
- (BOOL)isNotificationRelation;
- (BOOL)isNotificationBirthday;

@end
