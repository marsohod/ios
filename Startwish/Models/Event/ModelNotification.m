//
//  ModelNotification.m
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelNotification.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelNotification()

@property (nonatomic) NSUInteger    type_id;
@property (nonatomic) NSUInteger    status;

@end


@implementation ModelNotification

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"notifications"];
        
        [__descriptor addProperty:@"created"];
//        [__descriptor addProperty:@"updated"];
        [__descriptor addProperty:@"emitter_id"];
        [__descriptor addProperty:@"object_id"];
        [__descriptor addProperty:@"receiver_id"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"type_id"];
        [__descriptor addProperty:@"emitter"];
        [__descriptor addProperty:@"object"];
    });
    
    return __descriptor;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"ModelNotification: { \r id: %u \r emitter_id: %u \r object_id: %d \r receiver_id: %u \r type_id: %ld \r status: %ld \r emitter: %@ \r object: %@ \r}", [self getId], self.emitter_id, self.object_id, self.receiver_id, self.type_id, self.status, self.emitter, self.object];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (uint32_t)getCommentWishId
{
    return [[self getObject] getWishId];
}

- (NSString *)getText
{
    if ( [self isNotificationComment] ) {
        ModelComment *c = [[(ModelWish *)[self getObject] getComments] firstObject];

        return c ? [c getText] : @"";
    } else if ( [self isNotificationStartwish] ) {
        //TODO:
        // ModelStartwishNotification add
        return @"";
    } else {
        return @"";
    }
}

- (NSString *)getTitle
{
    // TODO:
    // ModelStartwishNotification add
    return @"";
}

- (NSString *)getNetwork
{
    switch ([(ModelSocial *)[self getObject] getSnId]) {
        case 1:
            return @"Facebook";
        case 2:
            return @"Вконтакте";
        case 3:
            return @"Вконтакте";
            
        default:
            return @"";
            break;
    }
}

- (NSString *)getEventUserAvatar
{
    if ( [self isNotificationComment] ) {
        ModelComment *c = [[(ModelWish *)[self getObject] getComments] firstObject];
        
        return c ? [[c getUser] getAvatar] : @"";
    } else if ( [self isNotificationUser] ) {
        return [[self getEmitter] getAvatar];
    } else if ( [self isNotificationWish] ) {
        return [[[self getObject] getOwner] getAvatar];
    }
    
    return @"";
}

- (NSString *)getEventUserName
{
    if ( [self isNotificationComment] ) {
        ModelComment *c = [[(ModelWish *)[self getObject] getComments] firstObject];
        
        return c ? [[c getUser] getFullname] : @"";
    } else if ( [self isNotificationUser] ) {
        return [[self getEmitter] getFullname];
    } else if ( [self isNotificationWish] ) {
        return [[[self getObject] getOwner] getFullname];
    }
    
    return @"";
}

- (ModelUser *)getEventUser
{
    if ( [self isNotificationComment] ) {
        ModelComment *c = [[(ModelWish *)[self getObject] getComments] firstObject];
        
        return c ? [c getUser] : [[self getObject] getOwner];
    } else if ( [self isNotificationUser] ) {
        return [self getEmitter];
    } else if ( [self isNotificationWish] ) {
        return [[self getObject] getOwner];
    }
    
    return nil;
}

- (NSString *)getUpdatedDate
{
    return [(ModelWish *)[self getObject] getUpdatedDate] != (id)[NSNull null] ? [(ModelWish *)[self getObject] getUpdatedDate] : @"";
}



// @type id
//  1. Комментарий
//  2. Новое желание
//  3. +1 подписчик если friendship_status == 0; Вы друзья если friendship_status == 1;
//  4.
//  5.
//  6. Желание исполнилось
//  7.
//  8. День Рождения
//  9.
//  10. Общее желание
//  11. Событие от Startwish
//  12. Зарегистрировался друг из VK или FB зависит от snType
//  13.

- (BOOL)isEqualTypes:(ModelNotification *)e
{
    return self.type_id == e.type_id;
}

- (BOOL)isNotificationComment
{
    return self.type_id == 1;
}

- (BOOL)isNotificationWish
{
    return ([self isNotificationNewWish] || self.type_id == 6 || self.type_id == 10);
}

- (BOOL)isNotificationNewWish
{
    return self.type_id == 2;
}

- (BOOL)isNotificationUser
{
    return ([self isNotificationRelation] || [self isNotificationBirthday] || [self isNotificationUserRegister]);
}

- (BOOL)isNotificationRelation
{
    return self.type_id == 3;
}

- (BOOL)isNotificationBirthday
{
    return self.type_id == 8;
}

- (BOOL)isNotificationUserRegister
{
    return self.type_id == 12;
}

- (BOOL)isNotificationStartwish
{
    return self.type_id == 11;
}


@end
