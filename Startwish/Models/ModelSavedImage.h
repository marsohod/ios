//
//  ModelSavedImage.h
//  Startwish
//
//  Created by Pavel Makukha on 15/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "JSONAPIResourceBase.h"


@interface ModelSavedImage : JSONAPIResourceBase

@property (nonatomic) NSDictionary *cover;
@property (nonatomic) NSDictionary *original;

- (NSString *)  getCover;
- (NSInteger)   getCoverWidth;
- (NSInteger)   getCoverHeight;
- (NSString *)  getOriginal;
- (NSInteger)   getOriginalWidth;
- (NSInteger)   getOriginalHeight;
@end
