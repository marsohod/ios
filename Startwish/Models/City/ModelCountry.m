//
//  ModelCountry.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelCountry.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"

@interface ModelCountry()

@property (nonatomic, readonly) NSString  *created;
@property (nonatomic, readonly) NSInteger         status;

@end

@implementation ModelCountry

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"country"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"code"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"status"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelCountry: { \r name: %@ \r code: %@ \r}", self.name, self.code];
}

@end
