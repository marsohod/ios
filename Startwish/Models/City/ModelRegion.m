//
//  ModelRegion.m
//  Startwish
//
//  Created by Pavel Makukha on 15/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelRegion.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelRegion()

@property (nonatomic, readonly) NSString  *created;
@property (nonatomic, readonly) NSInteger country_id;
@property (nonatomic, readonly) NSInteger status;

@end


@implementation ModelRegion

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"regions"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"country_id"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"status"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelRegion: { \r name: %@ \r countryId: %ld \r}", self.name, self.country_id];
}

@end
