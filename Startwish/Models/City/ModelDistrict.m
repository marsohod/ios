//
//  ModelDistrict.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelDistrict.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelDistrict()

@property (nonatomic, readonly) NSString  *created;
@property (nonatomic, readonly) NSInteger status;
@property (nonatomic, readonly) NSInteger city_id;
@property (nonatomic, readonly) NSInteger country_id;
@property (nonatomic, readonly) BOOL      is_metro;

@end

@implementation ModelDistrict

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"districts"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"city_id"];
        [__descriptor addProperty:@"country_id"];
        [__descriptor addProperty:@"is_metro"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"status"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelDistrict: { \r name: %@ \r is_metro: %d \r city_id: %ld \r country_id: %ld \r}", self.name, self.is_metro, self.city_id, self.country_id];
}

@end