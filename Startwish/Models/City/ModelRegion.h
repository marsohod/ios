//
//  ModelRegion.h
//  Startwish
//
//  Created by Pavel Makukha on 15/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelRegion : JSONAPIResourceBase

@property (nonatomic, strong, readonly, getter=getName) NSString    *name;

@end
