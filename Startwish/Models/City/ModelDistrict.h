//
//  ModelDistrict.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelDistrict : JSONAPIResourceBase

@property (nonatomic, strong, getter=getName) NSString  *name;

@end
