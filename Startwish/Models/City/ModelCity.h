//
//  ModelCity.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelCity : JSONAPIResourceBase

@property (nonatomic, strong, readonly, getter=getName) NSString    *name;
@property (nonatomic, getter=isMain) BOOL                           isMain;
@property (nonatomic, readonly, getter=getCountryId) NSInteger      country_id;

- (uint32_t)getId;
- (NSString *)getRegionName;

@end
