//
//  ModelCity.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelCity.h"
#import "ModelRegion.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelCity()

@property (nonatomic, readonly) NSString    *created;
@property (nonatomic, readonly) NSInteger   region_id;
@property (nonatomic, readonly) ModelRegion *region;
@property (nonatomic, readonly) NSInteger   status;

@end

@implementation ModelCity

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"cities"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"country_id"];
        [__descriptor addProperty:@"region_id"];
        [__descriptor addProperty:@"isMain" withJsonName:@"main_city"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"status"];
        [__descriptor hasOne:[ModelRegion class] withName:@"region"];
        
    });
    
    return __descriptor;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelCity: { \r name: %@ \r isMain: %d \r}", self.name, self.isMain];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (NSString *)getRegionName
{
    return [self.region getName];
}
@end
