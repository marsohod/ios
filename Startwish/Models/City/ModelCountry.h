//
//  ModelCountry.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelCountry : JSONAPIResourceBase

@property (nonatomic, strong, readonly, getter=getName) NSString  *name;
@property (nonatomic, strong, readonly, getter=getCode) NSString  *code;

@end
