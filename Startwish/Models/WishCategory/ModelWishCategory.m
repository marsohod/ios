//
//  Category.m
//  Startwish
//
//  Created by Pavel Makukha on 29/01/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelWishCategory.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"
#import "NSDateFormatter+JSONAPIDateFormatter.h"


@interface ModelWishCategory()

@end

@implementation ModelWishCategory

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.created forKey:@"created"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.poster forKey:@"poster"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.status] forKey:@"status"];
    [aCoder encodeObject:self.uri forKey:@"uri"];
//    [aCoder encodeObject:self.wishes forKey:@"wishes"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.wishes_active] forKey:@"wishes_active"];
    [aCoder encodeObject:[NSNumber numberWithInteger:self.wishes_executed] forKey:@"wishes_executed"];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if(self = [super init]){
        self.ID                 = [aDecoder decodeObjectForKey:@"ID"];
        self.created            = [aDecoder decodeObjectForKey:@"created"];
        self.name               = [aDecoder decodeObjectForKey:@"name"];
        self.poster             = [aDecoder decodeObjectForKey:@"poster"];
        self.status             = [[aDecoder decodeObjectForKey:@"status"] integerValue];
        self.uri                = [aDecoder decodeObjectForKey:@"uri"];
//        self.wishes             = [aDecoder decodeObjectForKey:@"wishes"];
        self.wishes_active      = [[aDecoder decodeObjectForKey:@"wishes_active"] integerValue];
        self.wishes_executed    = [[aDecoder decodeObjectForKey:@"wishes_executed"] integerValue];
    }
    return self;
}

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"categories"];
        
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"poster"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"uri"];
        [__descriptor addProperty:@"wishes_active"];
        [__descriptor addProperty:@"wishes_executed"];
    });
    
    return __descriptor;
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

@end
