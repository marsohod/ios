//
//  Category.h
//  Startwish
//
//  Created by Pavel Makukha on 29/01/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"

@interface ModelWishCategory : JSONAPIResourceBase

@property (nonatomic, strong, getter=getCreated) NSString*    created;
@property (nonatomic, strong, getter=getName, setter=setName:) NSString*       name;
@property (nonatomic, strong, getter=getPoster, setter=setPoster:) NSString*     poster;
@property (nonatomic, getter=getStatus) NSInteger             status;
@property (nonatomic, strong, getter=getLink) NSString*       uri;
//@property (nonatomic, getter=getWishes) NSDictionary*         wishes;
@property (nonatomic, getter=getWishesActive) NSInteger       wishes_active;
@property (nonatomic, getter=getWishesExecuted) NSInteger     wishes_executed;

- (uint32_t)getId;
@end
