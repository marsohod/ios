#pragma mark -  Wish.h
#pragma mark -  Startwish
#pragma mark -  Created by marsohod on 25/09/14.
#pragma mark -  Copyright (c) 2014 marsohod. All rights reserved.


#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "ModelUser.h"
#import "ModelCounters.h"
#import "ModelMutuality.h"
#import "ModelComment.h"
#import "JSONAPIResourceBase.h"


@interface ModelWish : JSONAPIResourceBase

@property (nonatomic, getter=getOwner, setter=setOwner:) ModelUser  *owner;
@property (nonatomic, getter=getCategoryId) NSInteger               category_id;
@property (nonatomic, getter=getCreatedDate) NSString               *created;
@property (nonatomic, getter=getUpdatedDate) NSString               *updated;
@property (nonatomic, getter=getExecutedDate) NSString              *executed;
@property (nonatomic, getter=getCurrency) NSString                  *currency;
@property (nonatomic, getter=getDeeplink) NSInteger                 deeplink;
@property (nonatomic, getter=getDescription) NSString               *desc;
@property (nonatomic, getter=getCoverExecutedHeight) CGFloat        exec_img_height;
@property (nonatomic, getter=getCoverExecuted) NSString             *exec_img_src;
@property (nonatomic, getter=getCoverExecutedOriginal) NSString     *exec_img_src_orig;
@property (nonatomic, getter=getCoverExecutedWidth) CGFloat         exec_img_width;
@property (nonatomic, getter=getCoverExecutedOriginalTemp, setter=setCoverExecutedOriginalTemp:) UIImage   *exec_img_src_temp;
@property (nonatomic, getter=getCoverHeight) CGFloat                img_height;
@property (nonatomic, getter=getCover) NSString                     *img_src;
@property (nonatomic, getter=getCoverOriginal) NSString             *img_src_orig;
@property (nonatomic, getter=getCoverWidth) CGFloat                 img_width;
@property (nonatomic) uint32_t                                      master_id;
@property (nonatomic, getter=getName) NSString                      *name;
@property (nonatomic, getter=getOwnerId) uint32_t                   owner_id;
@property (nonatomic) CGFloat                                       price;
@property (nonatomic, getter=getPrivacy) NSInteger                  privacy;
@property (nonatomic) NSInteger                                     shop_id;
@property (nonatomic, getter=getUrl) NSString                       *url;
@property (nonatomic, getter=getWishlistId) uint32_t                wishlist_id;
@property (nonatomic, getter=getComments) NSMutableArray            *comments;

@property(nonatomic) BOOL isChanged;

- (uint32_t)    getId;
- (BOOL)        hasExecutedImageWithSizes;
- (BOOL)        isImplement;
- (BOOL)        isDeleted;
- (void)        implement;
- (void)        setDefaultStatus;
- (void)        deleteWish;
- (BOOL)        isMyWish;
- (BOOL)        hasCommentId:(uint32_t)commentId;

- (NSUInteger)  getRewishesCount;
- (NSUInteger)  getCommentsCount;
- (void)        setCommentsCount:(NSInteger)cnt;
- (void)        pushComment:(ModelComment *)comment;
- (void)        pushComments:(NSArray *)items;
- (void)        pushCommentToTheEnd:(NSArray *)items;
- (NSArray *)   deleteComment:(id)obj;

- (BOOL)        isWishInWishlist;
- (void)        wishInWishlist;
- (void)        wishInWishlist:(BOOL)status;

+ (void)        setDefaults:(ModelWish *)wish;

@end
