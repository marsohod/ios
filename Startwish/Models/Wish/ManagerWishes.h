//
//  ManagerWishes.h
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ManagerWishes : NSObject

+ (NSArray *)   getOwnersId:(NSArray *)items;
+ (void)        setOwners:(NSArray *)owners toItems:(NSMutableArray *)items;
+ (void)        setComments:(NSArray *)comments toItems:(NSMutableArray *)items;

@end
