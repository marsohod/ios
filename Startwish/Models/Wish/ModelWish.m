//
//  ModelWish.m
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelWish.h"
#import "Me.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"
#import "NSDateFormatter+JSONAPIDateFormatter.h"


@interface ModelWish()

@property (nonatomic,strong) ModelCounters*     counters;
@property (nonatomic,strong) ModelMutuality*    mutuality;
@property (nonatomic) NSInteger                 status;

@end


@implementation ModelWish

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"wishes"];
        
        [__descriptor addProperty:@"category_id"];
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"updated"];
        [__descriptor addProperty:@"currency"];
        [__descriptor addProperty:@"deeplink"];
        [__descriptor addProperty:@"desc" withJsonName:@"description"];
        [__descriptor addProperty:@"exec_img_height"];
        [__descriptor addProperty:@"exec_img_src"];
        [__descriptor addProperty:@"exec_img_src_orig"];
        [__descriptor addProperty:@"exec_img_width"];
        [__descriptor addProperty:@"executed"];
        [__descriptor addProperty:@"img_height"];
        [__descriptor addProperty:@"img_src"];
        [__descriptor addProperty:@"img_src_orig"];
        [__descriptor addProperty:@"img_width"];
        [__descriptor addProperty:@"master_id"];
        [__descriptor addProperty:@"name"];
        [__descriptor addProperty:@"owner_id"];
        [__descriptor addProperty:@"price"];
        [__descriptor addProperty:@"privacy"];
        [__descriptor addProperty:@"shop_id"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"url"];
        [__descriptor addProperty:@"wishlist_id"];
        //        [__descriptor addProperty:@"counters"];
        //        [__descriptor addProperty:@"date"
        //                  withDescription:[[JSONAPIPropertyDescriptor alloc] initWithJsonName:@"date" withFormat:[NSDateFormatter RFC3339DateFormatter]]];
        
        //        [__descriptor hasOne:[User class] withName:@"owner"];
        [__descriptor hasOne:[ModelCounters class] withName:@"counters"];
        [__descriptor hasOne:[ModelMutuality class] withName:@"mutuality"];
        [__descriptor hasMany:[ModelComment class] withName:@"comments"];
        
    });
    
    return __descriptor;
}

- (instancetype)init
{
    if ( self = [super init] ) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addCommentFromCommentsController:) name:@"addCommentFromCC" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeCommentFromCommentsController:) name:@"removeCommentFromCC" object:nil];

        [self addObserver:self forKeyPath:@"isChanged" options:NSKeyValueObservingOptionNew context:nil];
    }
    
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"isChanged"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)copyWithZone:(NSZone *)zone
{
    ModelWish * w = [[ModelWish alloc] init];
    w.ID                                = self.ID;
    w.category_id                       = self.category_id;
    w.created                           = self.created;
    w.currency                          = self.currency;
    w.deeplink                          = self.deeplink;
    w.desc                              = self.desc;
    w.exec_img_height                   = self.exec_img_height;
    w.exec_img_src                      = self.exec_img_src;
    w.exec_img_src_orig                 = self.exec_img_src_orig;
    w.exec_img_width                    = self.exec_img_width;
    w.executed                          = self.executed;
    w.img_height                        = self.img_height;
    w.img_src                           = self.img_src;
    w.img_src_orig                      = self.img_src_orig;
    w.img_width                         = self.img_width;
    w.master_id                         = self.master_id;
    w.name                              = self.name;
    w.owner_id                          = self.owner_id;
    w.price                             = self.price;
    w.privacy                           = self.privacy;
    w.shop_id                           = self.shop_id;
    w.status                            = self.status;
    w.url                               = self.url;
    w.wishlist_id                       = self.wishlist_id;
    w.comments                          = [[NSMutableArray alloc] initWithArray:self.comments copyItems:YES];
    w.mutuality                         = self.mutuality;
    w.counters                          = self.counters;
    w.owner                             = self.owner;
    w.updated                           = self.updated;
    
    return w;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelWish: { id: %@ \r name: %@ \r updated: %d \r}", self.ID, self.name, self.updated];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (BOOL)hasExecutedImageWithSizes
{
    if ( ![[self getCoverExecuted] isEqualToString:@""] && [self getCoverExecutedHeight] != 0.0 && [self getCoverExecutedWidth] != 0.0 && [self isImplement] ) {
        return YES;
    }
    
    return NO;
}

- (NSString *)getUpdatedDate
{
    return _updated ? _updated : _created;
}

- (BOOL)isImplement
{
    return _status == 2 ? YES : NO;
}

- (BOOL)isDeleted
{
    return _status == 0 ? YES : NO;
}

- (void)implement
{
    _status = 2;
    [self.counters setRewishes: [self getRewishesCount]-1];
    self.isChanged = YES;
}

- (void)setDefaultStatus
{
    _status = 1;
}

- (void)deleteWish
{
    _status = 0;
    self.isChanged = YES;
}

- (BOOL)isMyWish
{
    return [Me isMyProfile:[self getOwnerId]];
}

- (BOOL)hasCommentId:(uint32_t)commentId
{
    for ( ModelComment *c in self.comments) {
        if ( [c getId] == commentId ) {
            return YES;
        }
    }
    
    return NO;
}

- (NSUInteger)getRewishesCount
{
    return [self.counters getRewishes];
}

- (NSUInteger)getCommentsCount
{
    return [self.counters getComments];
}

- (void)setCommentsCount:(NSInteger)cnt
{
    [self.counters setComments:cnt];
    self.isChanged = YES;
}

- (void)commentsCountIncrease:(BOOL)increase
{
    [self commentsCountIncrease:increase byCount:1];
}

- (void)commentsCountIncrease:(BOOL)increase byCount:(NSInteger)cnt
{
    [self.counters setComments: [self.counters getComments] + (increase ? cnt : -cnt)];
}

- (NSMutableArray *)getComments
{
    if ( !_comments ) {
        _comments = [[NSMutableArray alloc] init];
    }
    
    return _comments;
}

- (void)pushComment:(ModelComment *)comment
{
    if ( comment != nil ) {
        [self.comments insertObject:comment atIndex:0];
    }
}

- (void)pushComments:(NSArray *)items
{
    [items enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.comments insertObject:obj atIndex:0];
    }];
}

- (void)pushCommentToTheEnd:(NSArray *)items
{
    [items enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self.comments addObject:obj];
    }];
}

- (NSArray *)deleteComment:(id)obj
{
    [self.comments removeObject:obj];
    self.isChanged = YES;
    return self.comments;
}

- (BOOL)isWishInWishlist
{
    return [self.mutuality getHasWish];
}

- (void)wishInWishlist
{
    [self wishInWishlist:YES];
}

- (void)wishInWishlist:(BOOL)status
{
    [self.mutuality setHasWish:status];
    [self.counters setRewishes: [self getRewishesCount] + (status == YES ? 1 : -1) ];
    self.isChanged = YES;
}

- (NSString *)getDescription
{
    return _desc.length > 0 ? _desc : @"";
}

- (NSString *)getUrl
{
    return _url.length > 0 ? _url : @"";
}

- (void)addCommentFromCommentsController:(NSNotification *)notif
{
    //        NSLog(@"%@ %@ %lu, %u", notif.userInfo, notif.userInfo[@"wishId"], [notif.userInfo[@"wishId"] unsignedLongValue], [self getId]);
    if ( [notif.userInfo[@"wishId"] unsignedLongValue] == [self getId] ) {
        [self pushCommentToTheEnd:@[notif.userInfo[@"comment"]]];
        [self commentsCountIncrease:YES];
        self.isChanged = YES;
    }
}


- (void)removeCommentFromCommentsController:(NSNotification *)notif
{
    //    NSLog(@"%@ %@ %lu, %u", notif.userInfo, notif.userInfo[@"wishId"], [notif.userInfo[@"wishId"] unsignedLongValue], [wish getId]);
    if ( [notif.userInfo[@"wishId"] unsignedIntegerValue] == [self getId] ) {
        [[self getComments] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ( [notif.userInfo[@"commentId"]  unsignedLongValue] == [obj getId] ) {
                [self commentsCountIncrease:NO];
                [self deleteComment:obj];
                *stop = YES;
                return;
            }
        }];
    }
}


#pragma mark - Observer
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if ( [change[@"new"] boolValue] == YES ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STWishUpdated" object:nil userInfo:@{@"wish": self}];
        
        self.isChanged = NO;
    }
}

+ (void)setDefaults:(ModelWish *)wish
{
    if ( !wish ) { return; }
    if ( !wish.counters ) { wish.counters   = [[ModelCounters alloc] init]; }
    if ( !wish.mutuality ) { wish.mutuality = [[ModelMutuality alloc] init]; }
    
    [wish.counters setRewishes:1];
    [wish.counters setComments:0];
    [wish.mutuality setHasWish:YES];
    
}


@end
