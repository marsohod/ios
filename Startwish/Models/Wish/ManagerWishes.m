//
//  ManagerWishes.m
//  Startwish
//
//  Created by Pavel Makukha on 09/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ManagerWishes.h"
#import "ModelWish.h"
#import "ModelUser.h"
#import "ModelComment.h"


@implementation ManagerWishes

+ (NSArray *)getOwnersId:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    [items enumerateObjectsUsingBlock:^(ModelWish*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [a addObject:[NSNumber numberWithUnsignedLong:[obj getOwnerId]]];
    }];
    
    return a;
}

+ (void)setOwners:(NSArray *)owners toItems:(NSMutableArray *)items
{
    [owners enumerateObjectsUsingBlock:^(ModelUser*  _Nonnull owner, NSUInteger idx, BOOL * _Nonnull stop) {
        [items enumerateObjectsUsingBlock:^(ModelWish* _Nonnull wish, NSUInteger idxWish, BOOL * _Nonnull stopWish) {
            if ( [wish getOwnerId] == [owner getId] ) {
                [wish setOwner:owner];
            }
        }];
    }];
}

+ (void)setComments:(NSArray *)comments toItems:(NSMutableArray *)items
{
    [comments enumerateObjectsUsingBlock:^(ModelComment*  _Nonnull comment, NSUInteger idx, BOOL * _Nonnull stop) {
        [items enumerateObjectsUsingBlock:^(ModelWish* _Nonnull wish, NSUInteger idxWish, BOOL * _Nonnull stopWish) {
            if ( [wish getId] == [comment getWishId] ) {
                NSLog(@"wishId: %zd, commentID: %zd", [wish getId], [comment getWishId]);
                [wish pushComment:comment];
            }
        }];
    }];
    
}

@end
