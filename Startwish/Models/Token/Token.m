//
//  Token.m
//  Startwish
//
//  Created by Pavel Makukha on 03/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "Token.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface TokenAccess()

@end

@implementation TokenAccess

static NSString *access_title   = @"access_token";
static NSString *refresh_title  = @"refresh_token";
static NSString *expires_title  = @"expires_in";
static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"auths"];
        
        [__descriptor addProperty:access_title];
        [__descriptor addProperty:refresh_title];
        [__descriptor addProperty:expires_title];
    });
    
    return __descriptor;
}

+ (BOOL)isLogged
{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    
//    [ud setObject:@"adf" forKey:access_title];
//    [ud synchronize];
    
    return [self getToken].length != 0;
}

+ (void)setTokenFull:(TokenAccess *)t
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud setObject:t.access_token forKey:access_title];
    [ud setObject:t.refresh_token forKey:refresh_title];
    [ud setObject:t.expires_in forKey:expires_title];
    [ud synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STTokenChange" object:nil];
}

+ (void)clean
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    [ud removeObjectForKey:access_title];
    [ud removeObjectForKey:refresh_title];
    [ud removeObjectForKey:expires_title];
    [ud synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STTokenChange" object:nil];
}

+ (NSString *)getToken
{
    NSLog(@"token %@\r token_refresh: %@", [[NSUserDefaults standardUserDefaults] objectForKey:access_title], [[NSUserDefaults standardUserDefaults] objectForKey:refresh_title]);
    return [[NSUserDefaults standardUserDefaults] objectForKey:access_title];
}

+ (NSString *)getTokenRefresh
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:refresh_title];
}

+ (NSString *)getExpires
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:expires_title];
}

@end