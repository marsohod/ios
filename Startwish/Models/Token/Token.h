//
//  Token.h
//  Startwish
//
//  Created by Pavel Makukha on 03/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface TokenAccess : JSONAPIResourceBase

@property (nonatomic, strong) NSString  *access_token;
@property (nonatomic, strong) NSString  *refresh_token;
@property (nonatomic, strong) NSNumber  *expires_in;

+ (BOOL)isLogged;
+ (void)setTokenFull:(id)t;
+ (void)clean;

+ (NSString *)getToken;
+ (NSString *)getTokenRefresh;
+ (NSString *)getExpires;

@end

