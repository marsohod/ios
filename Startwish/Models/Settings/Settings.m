//
//  Settings.m
//  Startwish
//
//  Created by Pavel Makukha on 19/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//

#import "Settings.h"


@implementation Settings

- (instancetype)init
{
    self = [super init];
    
    if ( self ) {
        _domain = @"http://startwish.ru/";
//        _domain = @"http://startwish.dev:8080/";
        _domainShareWish = [NSString stringWithFormat:@"%@wish/", _domain];
//        _domainAPI = @"http://apiz.startwish.ru/v1";
//        _domainAPI = @"http://api-new.startwish.dev:8080/v1";
        _domainAPI = @"http://api.dev.startwish.ru/v1";
    }
    
    return self;
}

+ (Settings *)sharedInstance
{
    static id s = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[Settings alloc] init];
    });
    
    return s;
}

+ (NSString *)getMacAddress
{
//    NSLog(@"mac: %@", [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]);
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+ (CGFloat)maxPhotoWidth
{
    return 1280.0f;
}

@end
