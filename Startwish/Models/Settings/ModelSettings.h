//
//  ModelSettings.h
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelSettings : JSONAPIResourceBase

@property (nonatomic, strong) NSString  *key;
@property (nonatomic) NSInteger         value;

+ (BOOL)privacyBirthday:(NSArray *)list;
+ (BOOL)privacyContacts:(NSArray *)list;
+ (BOOL)privacyProfile:(NSArray *)list;

@end
