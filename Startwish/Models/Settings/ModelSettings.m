//
//  ModelSettings.m
//  Startwish
//
//  Created by Pavel Makukha on 08/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelSettings.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelSettings()

@property (nonatomic, strong) NSString  *created;
@property (nonatomic) NSInteger         status;
@property (nonatomic) uint32_t          user_id;

@end

@implementation ModelSettings

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"settings"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"user_id"];
        [__descriptor addProperty:@"key"];
        [__descriptor addProperty:@"value"];
        
    });
    
    return __descriptor;
}

+ (BOOL)privacyBirthday:(NSArray *)list
{
    return [self hasSetting:@"private_birthday" inList:list];
}

+ (BOOL)privacyContacts:(NSArray *)list
{
    return [self hasSetting:@"private_contacts" inList:list];
}

+ (BOOL)privacyProfile:(NSArray *)list
{
    return [self hasSetting:@"private_profile" inList:list];
}

+ (BOOL)hasSetting:(NSString *)key inList:(NSArray *)list
{
    for ( NSInteger i = 0; i < [list count]; i++ ) {
        if( [((ModelSettings *)list[i]).key isEqualToString:key] ) {
            return (BOOL)((ModelSettings *)list[i]).value;
        }
    }
    
    return NO;
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"ModelSettings: { \r key: %@ \r value: %ld \r id: %@ \r created: %@ \r}", self.key, self.value, self.ID, self.created];
}

@end
