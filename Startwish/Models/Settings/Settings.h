//
//  Settings.h
//  Startwish
//
//  Created by Pavel Makukha on 19/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface Settings : NSObject
@property(strong, nonatomic) NSString *domain;
@property(strong, nonatomic) NSString *domainAPI;
@property(strong, nonatomic) NSString *domainShareWish;

+ (Settings *)sharedInstance;
+ (NSString *)getMacAddress;
+ (CGFloat)maxPhotoWidth;

@end
