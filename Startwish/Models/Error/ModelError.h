//
//  ModelError.h
//  Startwish
//
//  Created by Pavel Makukha on 03/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "JSONAPIResourceBase.h"


@interface ModelError : JSONAPIResourceBase

@property (nonatomic) NSInteger         code;
@property (nonatomic, strong) NSString  *classError;
@property (nonatomic, strong) id        errors;
@property (nonatomic, strong) NSString  *type;

- (NSArray *)getErrorsArray;
- (NSString *)getError;

@end
