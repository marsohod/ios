//
//  Error.m
//  Startwish
//
//  Created by Pavel Makukha on 03/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ModelError.h"
#import "ActionTexts.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"


@interface ModelError()

@end

@implementation ModelError

static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"error"];
        
        [__descriptor addProperty:@"code"];
        [__descriptor addProperty:@"classError" withJsonName:@"class"];
        [__descriptor addProperty:@"errors"];
        [__descriptor addProperty:@"type"];
        
    });
    
    return __descriptor;
}

- (NSArray *)getErrorsArray
{
    NSMutableArray *arrayOfStrings = [[NSMutableArray alloc] init];
    
    if ( [self.errors isKindOfClass:[NSArray class]] ) {
        [self.errors enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if( [obj isKindOfClass:[NSDictionary class]] ) {
                for ( NSString *key in obj ) {
                    [arrayOfStrings addObject:[obj objectForKey:key]];
                }
            } else {
                [arrayOfStrings addObject:obj];
            }
        }];
    } else if ( [self.errors isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in self.errors ) {
            [arrayOfStrings addObject:[self.errors objectForKey:key]];
        }
    }
    
    return arrayOfStrings;
}

- (NSArray *)getErrorsKeys
{
    NSMutableArray *arrayOfStrings = [[NSMutableArray alloc] init];
    
    if ( [self.errors isKindOfClass:[NSArray class]] ) {
        [self.errors enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if( [obj isKindOfClass:[NSDictionary class]] ) {
                for ( NSString *key in obj ) {
                    [arrayOfStrings addObject:key];
                }
            } else if ( [obj isKindOfClass:[NSString class]] ) {
                [arrayOfStrings addObject:obj];
            }
        }];
    } else if ( [self.errors isKindOfClass:[NSDictionary class]] ) {
        for ( NSString *key in self.errors ) {
            [arrayOfStrings addObject:key];
        }
    }
    
    return arrayOfStrings;
}

- (NSString *)getError
{
    NSString *text  = @"";
    NSArray *keys = [self getErrorsKeys];
    NSMutableArray *errorsWithTexts = [[NSMutableArray alloc] init];
    
    switch( self.code ) {
        case 400:
            for (NSInteger i = 0; i < [keys count]; i++ ) {
                if ( ![[[ActionTexts sharedInstance] translateError:keys[i]] isEqualToString:@""] ) {
                    if ( ![[[ActionTexts sharedInstance] translateErrorType:self.type] isEqualToString:@""] ) {
                        [errorsWithTexts addObject: [NSString stringWithFormat:@"%@ %@", [[ActionTexts sharedInstance] translateError:keys[i]], [[ActionTexts sharedInstance] translateErrorType:self.type]]];
                    } else {
                        [errorsWithTexts addObject: [[ActionTexts sharedInstance] translateError:keys[i]]];
                    }
                }
            }
            
            text = [errorsWithTexts componentsJoinedByString:@", "];
            break;
            
        case 401:
            return @"";
            break;
            
        case 404:{
            for (NSInteger i = 0; i < [keys count]; i++ ) {
                NSDictionary *ec = [[ActionTexts sharedInstance] translateErrorClass:keys[i]];
                
                if ( ec[@"name"] ) {
                    [errorsWithTexts addObject: [NSString stringWithFormat:@"%@ %@", ec[@"name"], [[ActionTexts sharedInstance] titleText:[NSString stringWithFormat:@"NotFound%@", ec[@"type"]]]]];
                }
            }
            
            text = [errorsWithTexts componentsJoinedByString:@", "];
            break;
        }
        case 409:
            for (NSInteger i = 0; i < [keys count]; i++ ) {
                if ( ![[[ActionTexts sharedInstance] translateError:keys[i]] isEqualToString:@""] ) {
                    NSDictionary *ec    = [[ActionTexts sharedInstance] translateErrorClass:keys[i]];
                    NSString *pre;
                    
                    if ( ec[@"name"] ) {
                        pre = [[ActionTexts sharedInstance] titleText:[NSString stringWithFormat:@"This%@", ec[@"type"]]];
                        [errorsWithTexts addObject:[NSString stringWithFormat:@"%@ %@", pre, ec[@"name"]]];
                    } else {
                        [errorsWithTexts addObject: [[ActionTexts sharedInstance] translateError:keys[i]]];
                    }
                }
            }
            text = [NSString stringWithFormat:@"%@ %@", [errorsWithTexts componentsJoinedByString:@", "], [[ActionTexts sharedInstance] text:@"AlreadyExist"]];
            break;
            
        case 500:
            return [[ActionTexts sharedInstance] text:@"ServerError"];
            break;
    }
    
    return text;
}

@end
