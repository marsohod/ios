//
//  ManagerComments.m
//  Startwish
//
//  Created by Pavel Makukha on 14/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import "ManagerComments.h"


@implementation ManagerComments

+ (NSArray *)getWishesId:(NSArray *)items
{
    NSMutableArray *a = [[NSMutableArray alloc] init];
    
    [items enumerateObjectsUsingBlock:^(ModelComment*  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [a addObject:[NSNumber numberWithUnsignedLong:[obj getWishId]]];
    }];
    
    return a;
}

+ (void)setOwners:(NSArray *)owners forItems:(NSArray *)items
{
    for ( ModelUser *u in owners ) {
        for ( ModelComment *c in items ) {
            if ( [u getId] == [c getUserId] ) {
                [c setUser:u];
            }
        }
    }
}

@end
