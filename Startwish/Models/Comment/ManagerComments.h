//
//  ManagerComments.h
//  Startwish
//
//  Created by Pavel Makukha on 14/03/16.
//  Copyright © 2016 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ModelComment.h"


@interface ManagerComments : NSObject

+ (NSArray *)getWishesId:(NSArray *)items;
+ (void)setOwners:(NSArray *)owners forItems:(NSArray *)items;

@end
