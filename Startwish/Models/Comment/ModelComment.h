//
//  Comment.h
//  Startwish
//
//  Created by marsohod on 07/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ModelUser.h"
#import "JSONAPIResourceBase.h"


@interface ModelComment : JSONAPIResourceBase

@property (nonatomic, strong, getter=getDate)   NSString*   created;
@property (nonatomic, strong, getter=getText)   NSString*   text;
@property (nonatomic, strong, getter=getUser, setter=setUser:)   ModelUser*  owner;
@property (nonatomic, getter=getWishId)         uint32_t    wish_id;
@property (nonatomic, getter=getUserId)         uint32_t    user_id;


//- (void)setComment:(NSDictionary *)comment;
- (uint32_t)    getId;
- (BOOL)        isMyComment;

@end
