//
//  Comment.m
//  Startwish
//
//  Created by marsohod on 07/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "ModelComment.h"
#import "Me.h"
#import "NSString+Helper.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPIPropertyDescriptor.h"
#import "NSDateFormatter+JSONAPIDateFormatter.h"


@interface ModelComment()

@property (nonatomic) uint32_t          to;
@property (nonatomic, strong) NSString  *updated;
@property (nonatomic) NSInteger         status;

@end

@implementation ModelComment


static JSONAPIResourceDescriptor *__descriptor = nil;

+ (JSONAPIResourceDescriptor*)descriptor {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __descriptor = [[JSONAPIResourceDescriptor alloc] initWithClass:[self class] forLinkedType:@"comments"];
        
        [__descriptor addProperty:@"created"];
        [__descriptor addProperty:@"updated"];
        [__descriptor addProperty:@"to"];
        [__descriptor addProperty:@"text"];
        [__descriptor addProperty:@"status"];
        [__descriptor addProperty:@"wish_id"];
        [__descriptor addProperty:@"user_id"];
        
        [__descriptor hasOne:[ModelUser class] withName:@"owner"];
    });
    
    return __descriptor;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"\rModelComment: {\r created: %@ \r to: %zd \r text: %@ \r wish_id: %zd \r user_id: %zd \r status: %ld \r}", self.created, self.to, self.text, self.wish_id, self.user_id, self.status];
}

- (uint32_t)getId
{
    return [NSString stringToUint32_t:self.ID];
}

- (void)setComment:(NSDictionary *)comment
{
    // TODO
//    item = comment;
//    user = item[@"author"] != (id)[NSNull null] ? [[User alloc] initWithObject:item[@"author"]] : [[User alloc] initWithObject:@{}];
}

- (BOOL)isMyComment
{
    return [Me isMyProfile:[[self getUser] getId]];
}

@end
