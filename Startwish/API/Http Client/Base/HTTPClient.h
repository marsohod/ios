//
//  HTTPClient.h
//  Startwish
//
//  Created by marsohod on 25/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "HTTPManager.h"


@interface HTTPClient : NSObject
{
    HTTPManager *manager;
    NSInteger itemPerPage;
    NSString *host;
    BOOL debug;
}

typedef void(^successGetItems)(id requestObject);
typedef void(^badGetItems)(id requestObject);
typedef void(^failedGetItems)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

+ (void)setUploadProgress:(NSProgress *)operation;

- (BOOL)isEmptyResponse:(id)responseObject;

- (void)requestInfo:(NSString *)name response:(NSURLSessionDataTask *)task operation:(NSURLSessionTask *)operation requestParameters:(NSDictionary *)parameters;
- (NSString *)pagerWithPage:(NSUInteger)page;
- (NSString *)pagerWithOffset:(NSUInteger)offset;
- (NSString *)pagerWithOffset:(NSUInteger)offset limit:(NSUInteger)limit;
- (NSString *)pagerOnlyWithOffset:(NSUInteger)offset;
- (NSString *)pagerOnlyWithLimit:(NSUInteger)limit;
- (NSMutableArray *)searchFromDictionary:(NSDictionary *)searchDictionary;
- (NSMutableDictionary *)defaultParamsFromDictionary:(NSDictionary *)searchDictionary;
- (NSMutableDictionary *)add:(NSDictionary *)dic toDefaultParams:(NSDictionary *)params;
- (NSString *)debugParams;

- (id)removeNullProperties:(id)responseObject;
- (BOOL)isReachable;
@end