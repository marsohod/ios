//
//  HTTPManager.m
//  Startwish
//
//  Created by Pavel Makukha on 18/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "HTTPManager.h"
#import "Token.h"


@interface HTTPManager()

@end


@implementation HTTPManager

+ (HTTPManager *)sharedInstance
{
    static HTTPManager *m = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        m = [[HTTPManager alloc] init];
    });
    
    return  m;
}

- (instancetype)init
{
    self = [super init];
    
    self.isInternetConnectionAvailable = YES;
    
    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    //        [serializer setRemovesKeysWithNullValues:YES];
    self.responseSerializer = serializer;
    self.requestSerializer = [AFHTTPRequestSerializer serializer];
    self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    self.requestSerializer.HTTPMethodsEncodingParametersInURI = [NSSet setWithObjects:@"GET", @"HEAD", nil];
    //        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", nil];
    
    [self changeToken];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        self.isInternetConnectionAvailable = (status == AFNetworkReachabilityStatusReachableViaWWAN || status == AFNetworkReachabilityStatusReachableViaWiFi);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeToken) name:@"STTokenChange" object:nil];
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)changeToken
{
    [self.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", [TokenAccess getToken]] forHTTPHeaderField:@"Authorization"];
}

@end
