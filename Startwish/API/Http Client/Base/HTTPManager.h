//
//  HTTPManager.h
//  Startwish
//
//  Created by Pavel Makukha on 18/09/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "AFHTTPSessionManager.h"

@interface HTTPManager : AFHTTPSessionManager

+ (HTTPManager *)sharedInstance;

@property (nonatomic) BOOL isInternetConnectionAvailable;

@end
