//
//  HTTPClient.m
//  Startwish
//
//  Created by marsohod on 25/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HTTPClient.h"
#import "AFNetworking.h"
#import "Settings.h"
#import "ExtendNSLog.h"


@interface HTTPClient ()
{

}

@end

@implementation HTTPClient

- (instancetype)init
{
    self = [super init];
    if (self) {
        manager = [HTTPManager sharedInstance];
        itemPerPage = 40;
        host = [Settings sharedInstance].domainAPI;
        debug = NO;
    }
    
    return self;
}


#pragma mark - Common
+ (void)setUploadProgress:(NSProgress *)progress
{
//    NSLog(@"coplete: %ld %ld", progress.totalUnitCount, progress.completedUnitCount);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STUploadProgress"
                                                        object:nil
                                                      userInfo:@{@"upload": [NSNumber numberWithLongLong:progress.completedUnitCount],
                                                                 @"total": [NSNumber numberWithLongLong:progress.totalUnitCount]}];
}

- (BOOL)isEmptyResponse:(id)responseObject
{
//    NSLog(@"%@", [responseObject isMemberOfClass:[NSArray class]]);
    if( [responseObject isMemberOfClass:[NSArray class]] ) {
        return NO;
    }
    
    return responseObject[@"status"] != nil ? NO : YES;
}

- (NSString *)pagerWithPage:(NSUInteger )page;
{
    return [self pagerWithOffset:page*itemPerPage];
}

- (NSString *)pagerWithOffset:(NSUInteger)offset;
{
    return [self pagerWithOffset:offset limit:itemPerPage];
}

- (NSString *)pagerOnlyWithOffset:(NSUInteger)offset;
{
    return [self pagerWithOffset:offset limit:0];
}

- (NSString *)pagerOnlyWithLimit:(NSUInteger)limit;
{
    return [self pagerWithLimit:limit == 0 ? itemPerPage : limit];
}

- (NSString *)pagerWithLimit:(NSUInteger)limit
{
    return [NSString stringWithFormat:@"(limit:%ld)", (long)limit];
}

- (NSString *)pagerWithOffset:(NSUInteger)offset limit:(NSUInteger)limit;
{
    if ( limit == 0 ) {
        return [NSString stringWithFormat:@"(offset:%ld)", (long)offset];
    } else {
        return [NSString stringWithFormat:@"(offset:%ld,limit:%ld)", (long)offset, (long)limit];
    }
}

- (NSMutableArray *)searchFromDictionary:(NSDictionary *)searchDictionary
{
    NSMutableArray *searchArray = [[NSMutableArray alloc] init];
    
    [searchDictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ( [obj isKindOfClass:[NSDictionary class]] ) {
            [obj enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull keyObj, id  _Nonnull objInner, BOOL * _Nonnull stopInner) {
                [searchArray addObject:[NSString stringWithFormat:@"%@:%@", keyObj, objInner]];
            }];
        } else if ( [obj isKindOfClass:[NSArray class]] ) {
            [obj enumerateObjectsUsingBlock:^(id  _Nonnull objInner, NSUInteger idx, BOOL * _Nonnull stopInner) {
                [searchArray addObject:[NSString stringWithFormat:@"%@:%@", key, objInner]];
            }];
        } else {
            if ( ![[NSString stringWithFormat:@"%@", obj] isEqualToString:@""] ) {
                [searchArray addObject:[NSString stringWithFormat:@"%@:%@", key, obj]];
            }
        }
    }];
    
    return searchArray;
}

- (NSMutableDictionary *)defaultParamsFromDictionary:(NSDictionary *)searchDictionary
{
    NSMutableDictionary *a = [[NSMutableDictionary alloc] init];
    
    [searchDictionary enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ( [obj isKindOfClass:[NSDictionary class]] ) {
            [a setObject:[NSString stringWithFormat:@"(%@)", [[self searchFromDictionary:obj] componentsJoinedByString:@","]] forKey:key];
        } else {
            if ( [obj isKindOfClass:[NSArray class]] ) {
                [a setObject:[self searchFromDictionary:@{key: obj}] forKey:key];
            } else {
                [a setObject:[self searchFromDictionary:obj] forKey:key];
            }
        }
    }];
    
    return a;
}

- (NSMutableDictionary *)add:(NSDictionary *)dic toDefaultParams:(NSDictionary *)params
{
    NSMutableDictionary *p = [[NSMutableDictionary alloc] initWithDictionary:params];
    
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ( p[key] ) {
            [p setObject:[[NSMutableDictionary alloc] initWithDictionary:p[key]] forKey:key];
            
            [obj enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull ikey, id  _Nonnull iobj, BOOL * _Nonnull istop) {
                [p[key] setObject:iobj forKey:ikey];
            }];
        } else {
            [p setObject:obj forKey:key];
        }
    }];

    return p;
}

- (NSString *)debugParams
{
//    return @"";
    return debug == YES ? @"1" : @"";
}

- (BOOL)isReachable
{
    return manager.isInternetConnectionAvailable;
}

- (void)requestInfo:(NSString *)name response:(id)response operation:(NSURLSessionTask *)operation requestParameters:(NSDictionary *)parameters
{
    NSHTTPURLResponse *r = (NSHTTPURLResponse *)operation.response;
    
    NSLog(@"#HidEFILENAME");
    NSLog(@"#HIDEFUNCTIONNAME");
    NSLog(@"#HIDELINENUMBER");

    NSLog(@"#?\r\r запрос: %@ \r method: %@ \r url: %@", name, operation.originalRequest.HTTPMethod, [[operation originalRequest].URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    NSLog(@"#.\r\r заголовки запроса: %@", operation.originalRequest.allHTTPHeaderFields);
    NSLog(@"#.\r\r параметры запроса: %@", parameters);
    NSLog(@"#.\r\r вложения: %@", operation.originalRequest.HTTPBodyStream);
    NSLog(@"#.\r\r заголовки ответа: %@", [r allHeaderFields]);
    NSLog(@"#3\r\r ответ: %@", response);
}

- (id)removeNullProperties:(id)responseObject
{
    if ( [responseObject isKindOfClass:[NSArray class]] ) {
        NSMutableArray *items = [[NSMutableArray alloc] initWithArray:responseObject];
        
        [items enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ( [obj isKindOfClass:[NSDictionary class]] ) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithDictionary:obj];
                [dic enumerateKeysAndObjectsUsingBlock:^(id key, id objDic, BOOL *stop) {
                    if ( objDic == (id)[NSNull null] ) {
                        [dic setObject:@"adf" forKey:key];
                    }
                }];
                
                items[idx] = dic;
            }
        }];
        
        return [[NSArray alloc] initWithArray:items];
    }
    
    return responseObject;
}

@end
