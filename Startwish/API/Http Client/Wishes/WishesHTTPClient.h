//
#pragma mark -  WishesHTTPClient.h
#pragma mark -  Startwish
//
#pragma mark -  Created by marsohod on 10/10/14.
#pragma mark -  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HTTPClient.h"

typedef void(^successGetItem)(id requestObject);

@interface WishesHTTPClient : HTTPClient

+ (WishesHTTPClient *)sharedInstance;


#pragma mark - Желание

- (void)getWishById:(uint32_t)wishId prepareWish:(successGetItem)prepareWish failedBlock:(failedGetItems)failedBlock;

#pragma mark - Желания (популярные в том числе)

- (void)getWishesWithPage:(NSUInteger)page popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)getWishesAfterUpdate:(NSString *)dateString popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)getWishesBeforeUpdate:(NSString *)dateString popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

- (void)getWishesWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock;


#pragma mark - Желания пользователя

- (void)getWishesUserById:(uint32_t)userId params:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock;
- (void)getWishesUserById:(uint32_t)userId params:(NSDictionary *)params offset:(NSUInteger)offset limit:(NSInteger)limit prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock;


#pragma mark - Исполнение желания пользователя
- (void)executeWishId:(uint32_t)wishId params:(NSDictionary *)params successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Общие Желания с пользователем

- (void)getMutualWishesUserById:(uint32_t)userId withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Поиск желаний по имени, категории и популярности

- (void)searchWishes:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)searchWishesPopular:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
/*
- (void)searchWishes:(NSDictionary *)params afterDate:(NSString *)dateString prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)searchWishes:(NSDictionary *)params beforeDate:(NSString *)dateString prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
*/

#pragma mark - Категории
- (void)getCategories:(successGetItems)prepareItems failedBlock:(failedGetItems)failedBlock;


#pragma mark - Комментарии
/*! Get comments by search Disctionary */
- (void)getComments:(NSDictionary *)search offset:(NSInteger)offset limit:(NSInteger)limit prepareWish:(successGetItem)prepareWish failedBlock:(failedGetItems)failedBlock;
- (void)putComment:(NSMutableDictionary *)params prepareItem:(successGetItem)prepareItem failedBlock:(failedGetItems)failedBlock;
- (void)deleteComment:(uint32_t)commentId successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Ревиш

- (void)putRewish:(NSDictionary *)params successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Удалить

- (void)deleteWish:(uint32_t)wishId successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Добавить

- (void)createWishWithParams:(NSMutableDictionary *)params prepareItem:(successGetItem)prepareItem failedBlock:(failedGetItems)failedBlock;
@end
