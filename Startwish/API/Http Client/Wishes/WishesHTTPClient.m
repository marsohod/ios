//
//  WishesHTTPClient.m
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishesHTTPClient.h"
#import "NSString+Helper.h"


@interface WishesHTTPClient ()
{
    NSInteger itemPerPage;
}
@end

@implementation WishesHTTPClient

+ (WishesHTTPClient *)sharedInstance
{
    static WishesHTTPClient *client = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[WishesHTTPClient alloc] init];
    });
    
    return client;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        itemPerPage = 20;
    }
    return self;
}


#pragma mark -
#pragma mark Желание
- (void)getWishById:(uint32_t)wishId prepareWish:(successGetItem)prepareWish failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/wishes/%zd", host, wishId]
      parameters: @{@"debug"        : [self debugParams],
                    @"with"         : @"(counters)"}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wish" response:responseObject operation:operation requestParameters:nil];
             prepareWish(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Желания
- (void)getWishesWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock
{
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/wishes", host]
      parameters: @{@"pager"        : [self pagerWithOffset:offset limit:limit],
                    @"order"        : @"(created:desc)",
                    @"search"       : [searchArray count] > 0 ? [NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]] : @"",
                    @"with"         : @"(counters,mutuality)"
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)getWishesWithPage:(NSUInteger)page popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    
    
    [manager GET: [NSString stringWithFormat: @"%@/wishes", host]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"order"        : @"(created:desc)",
                    @"search"       : @"(status:1)",
                    @"with"         : @"(counters,mutuality)"
//                    @"debug"        : [self debugParams]
                    }
        progress: nil
//                    @"orderType"    : @"desc"}
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
        
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
    
}

- (void)getWishesAfterUpdate:(NSString *)dateString popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    
    
    [manager GET: [NSString stringWithFormat: @"%@/wishes", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(id:desc)",
                    @"search"       : [NSString stringWithFormat:@"(status:1,updated-after:%@)", [dateString urlencode]],
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
//             [self requestInfo:@"get wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
    
}

- (void)getWishesBeforeUpdate:(NSString *)dateString popularStatus:(BOOL)isPopular prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/wishes", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(id:desc)",
                    @"search"       : [NSString stringWithFormat:@"(status:1,updated-before:%@)", [dateString urlencode]],
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
    
}


#pragma mark -
#pragma mark Желания пользователя
- (void)getWishesUserById:(uint32_t)userId params:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock
{
    [self getWishesUserById:userId params:(NSDictionary *)params offset:page*itemPerPage limit:itemPerPage prepareWishes:prepareWishes failedBlock:failedBlock];
}

- (void)getWishesUserById:(uint32_t)userId params:(NSDictionary *)params offset:(NSUInteger)offset limit:(NSInteger)limit prepareWishes:(successGetItems)prepareWishes failedBlock:(failedGetItems)failedBlock
{
 
    NSMutableDictionary *p = [self defaultParamsFromDictionary:params];
    
    [p addEntriesFromDictionary:@{@"pager"        : [self pagerWithOffset:offset limit:limit],
                                  @"with"         : @"(counters,mutuality)",
//                                  @"debug"        : [self debugParams]
                                  }];
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/wishes", host, userId]
      parameters: p
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"user wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}


#pragma mark -
#pragma mark Исполненные желания

- (void)executeWishId:(uint32_t)wishId params:(NSDictionary *)params successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock
{
    NSMutableDictionary *p = [[NSMutableDictionary alloc] initWithDictionary:params];
    [p setObject:[self debugParams] forKey:@"debug"];
    
    [manager POST: [NSString stringWithFormat: @"%@/wishes/%zd/execute", host, wishId]
       parameters: p
         progress: ^(NSProgress * _Nonnull uploadProgress) {
             [HTTPClient setUploadProgress:uploadProgress];
         }
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"execute wish: " response:responseObject operation:operation requestParameters:p];
              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, p);
          }];
}


#pragma mark -
#pragma mark Общие желания
- (void)getMutualWishesUserById:(uint32_t)userId withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{

    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/mutual_wishes", host, userId]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"search"       : @"(status:1)",
                    @"with"         : @"(counters,mutuality)",
//                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"user mutual wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Поиск желаний
- (void)searchWishes:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock

{
    NSMutableDictionary *p = [self defaultParamsFromDictionary:params];
    
    [p addEntriesFromDictionary:@{@"pager"       : [self pagerWithPage:page],
                                  @"with"        : @"(counters,mutuality)"
                                  }];

    [self searchWishesWithParams:p prepareWishes:prepareWishes endWishes:badBlock failedBlock:failedBlock];
}

- (void)searchWishesPopular:(NSDictionary *)params withPage:(NSUInteger)page prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock

{
    NSMutableDictionary *p = [self defaultParamsFromDictionary:params];
    
    [p addEntriesFromDictionary:@{@"pager"       : [self pagerWithPage:page],
                                  @"with"        : @"(counters,mutuality)"
                                  }];
    
    [self searchWishesPopularWithParams:p prepareWishes:prepareWishes endWishes:badBlock failedBlock:failedBlock];
}
/*
- (void)searchWishes:(NSDictionary *)params afterDate:(NSString *)dateString prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    NSMutableDictionary *p = [self defaultParamsFromDictionary:params];
    
    [p addEntriesFromDictionary:@{@"pager"       : [self pagerOnlyWithLimit:0],
                                  @"status"      : @"1",
                                  @"with"        : @"(counters,mutuality)"
//                                  @"updated"     : dateString
                                  }];
    
    [self searchWishesWithParams:params prepareWishes:prepareWishes endWishes:badBlock failedBlock:failedBlock];
}

- (void)searchWishes:(NSDictionary *)params beforeDate:(NSString *)dateString prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    
    NSMutableDictionary *p = [self defaultParamsFromDictionary:params];
    
    [p addEntriesFromDictionary:@{@"pager"       : [self pagerOnlyWithLimit:0],
                                  @"status"      : @"1",
                                  @"with"        : @"(counters,mutuality)"
                                  //                                  @"updated"     : dateString
                                  }];
    
//    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:
//                                   @{@"pager"       : [self pagerOnlyWithLimit:0],
//                                     @"search"      : [NSString stringWithFormat:@"(name:%@,category_id:%lu,status:1,updated:%@)", name, (long)categoryId, dateString],
//                                     @"order"       : [NSString stringWithFormat:@"(id:desc%@)", isPopular == YES ? @",popularity:desc": @""],
////                                     @"debug"       : [self debugParams]
//                                     }];
//    
    //    if( isPopular == YES ) {
    //        [params setObject: [NSNumber numberWithInteger:1] forKey:@"popular"];
    //    }
    
    
    [self searchWishesWithParams:params prepareWishes:prepareWishes endWishes:badBlock failedBlock:failedBlock];
}
*/
- (void)searchWishesWithParams:(NSDictionary *)params prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/wishes", host]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"search wishes" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)searchWishesPopularWithParams:(NSDictionary *)params prepareWishes:(successGetItems)prepareWishes endWishes:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/wishes/popular", host]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"search wishes popular" response:responseObject operation:operation requestParameters:nil];
             prepareWishes(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}


#pragma mark -
#pragma mark Категории
- (void)getCategories:(successGetItems)prepareItems failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/categories", host]
      parameters: @{@"search"      : @"(status:1)",
//                    @"debug"       : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get categories" response:responseObject operation:operation requestParameters:nil];
             prepareItems([self removeNullProperties:responseObject]);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Комментарии
- (void)getComments:(NSDictionary *)search offset:(NSInteger)offset limit:(NSInteger)limit prepareWish:(successGetItem)prepareWish failedBlock:(failedGetItems)failedBlock
{
    
    NSMutableDictionary *p = [[NSMutableDictionary alloc] initWithDictionary:[self add:@{@"search": @{@"status" : @"1"}} toDefaultParams:search]];
    
    
    
    NSMutableDictionary *params = [self defaultParamsFromDictionary:p];
    
    [params setObject:[self debugParams] forKey:@"debug"];
    [params setObject:[self pagerWithOffset:offset limit: limit != 0 ? limit : 10] forKey:@"pager"];
    [params setObject:@"(owner)" forKey:@"with"];
    
    [manager GET: [NSString stringWithFormat: @"%@/comments", host]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get comments wish" response:responseObject operation:operation requestParameters:params];
             prepareWish(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)putComment:(NSMutableDictionary *)params prepareItem:(successGetItem)prepareItem failedBlock:(failedGetItems)failedBlock
{
//    [params setObject:[self debugParams] forKey:@"debug"];
    
    [manager PUT: [NSString stringWithFormat: @"%@/comments?with=(owner)", host]
       parameters: params
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"add comment" response:responseObject operation:operation requestParameters:params];
              prepareItem(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)deleteComment:(uint32_t)commentId successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock;
{
    [manager DELETE: [NSString stringWithFormat: @"%@/comments/%zd", host, commentId]
         parameters: @{@"debug": [self debugParams]}
            success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"delete comment" response:responseObject operation:operation requestParameters:nil];
              successBlock(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Ревиш
- (void)putRewish:(NSDictionary *)params successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock
{
    [manager PUT: [NSString stringWithFormat: @"%@/wishes/%@/rewish?with=(counters,mutuality)", host, params[@"wish_id"]]
      parameters: @{@"wishlist_id"  : params[@"wishlist_id"]}
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"rewish" response:responseObject operation:operation requestParameters:params];
              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}


#pragma mark -
#pragma mark Удаление
- (void)deleteWish:(uint32_t)wishId successBlock:(successGetItem)successBlock failedBlock:(failedGetItems)failedBlock
{
    [manager DELETE: [NSString stringWithFormat: @"%@/wishes/%zd", host, wishId]
         parameters: @{@"debug"   : [self debugParams]}
            success: ^(NSURLSessionDataTask *operation, id responseObject) {
                [self requestInfo:@"Удаление желания" response:responseObject operation:operation requestParameters:nil];
                successBlock(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Добавление
- (void)createWishWithParams:(NSMutableDictionary *)params prepareItem:(successGetItem)prepareItem failedBlock:(failedGetItems)failedBlock
{
    [manager PUT: [NSString stringWithFormat: @"%@/wishes", host]
       parameters: params
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"Добавление желания" response:responseObject operation:operation requestParameters:params];
              prepareItem(responseObject);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}
@end
