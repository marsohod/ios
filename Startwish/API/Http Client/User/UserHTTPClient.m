//
//  UserHTTPClient.m
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UserHTTPClient.h"
#import "VKHandler.h"
#import "NSString+Helper.h"
#import "Token.h"


@implementation UserHTTPClient

//@synthesize isReachable = _isReachable;

+ (UserHTTPClient *)sharedInstance
{
    static UserHTTPClient *client = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[UserHTTPClient alloc] init];
    });
    
    return  client;
}


#pragma mark -
#pragma mark Изменить данные пользователя
//deprecated
- (void)setUserImage:(NSData *)image
              domain:(NSString *)domain
            fullname:(NSString *)name
              gender:(NSInteger)gender
                city:(NSString *)city
              cityId:(NSString *)cityId
            birthday:(NSString *)birth
           goodBlock:(successGetUser)goodBlock
         failedBlock:(failedGetItems)failedBlock
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                    @"domain"   : domain,
                                                                                    @"fullname" : name,
                                                                                    @"gender"   : [NSNumber numberWithInteger: gender],
                                                                                    @"debug"    : [self debugParams]
                                                                                   }];
    
    if( ![city isEqualToString:@""] && city != nil ){
        [params setObject:city forKey:@"city"];
        [params setObject:cityId forKey:@"cityId"];
    }
    
    if( ![birth isEqualToString:@""] ){
        [params setObject:birth forKey:@"birthday"];
    }
    
    if ( image != nil ) {
        [params setObject:[NSNumber numberWithBool:YES] forKey:@"avatar"];
    }

    [self postUserProfile:params goodBlock:goodBlock failedBlock:failedBlock];
}

- (void)setUserProfile:(NSDictionary *)prof goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock
{

    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:prof];
    
    [params setObject:[self debugParams] forKey:@"debug"];
    
    [self postUserProfile:params goodBlock:goodBlock failedBlock:failedBlock];
}

- (void)postUserProfile:(NSDictionary *)params goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock
{
    
    [manager POST: [NSString stringWithFormat: @"%@/users/me?with=(counters,wishlists,socials,city,settings)", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"Пользователь" response:responseObject operation:operation requestParameters:params];
              goodBlock(responseObject);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)setUserLocationCity:(uint32_t)cityId
              goodBlock:(successGetUser)goodBlock
            failedBlock:(failedGetItems)failedBlock
{
    if ( !cityId || cityId == 0 ) {
        [manager DELETE: [NSString stringWithFormat: @"%@/users/me/city", host]
          parameters: nil
             success: ^(NSURLSessionDataTask *operation, id responseObject) {
                 [self requestInfo:@"настройки" response:responseObject operation:operation requestParameters:nil];
                 goodBlock(responseObject);
             } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failedBlock(task, error, nil);
             }];
    } else {
        [manager PUT: [NSString stringWithFormat: @"%@/users/me/city/%zd", host, cityId]
           parameters: nil
              success: ^(NSURLSessionDataTask *operation, id responseObject) {
                  [self requestInfo:@"настройки" response:responseObject operation:operation requestParameters:nil];
                  goodBlock(responseObject);
              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failedBlock(task, error, nil);
              }];
    }
}

- (void)setUserSettings:(NSDictionary *)settings goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock
{

//    NSDictionary *params = @{@"debug": [self debugParams]};
    
    [manager POST: [NSString stringWithFormat: @"%@/users/me/settings", host]
       parameters: settings
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"настройки" response:responseObject operation:operation requestParameters:settings];
             if( responseObject != nil ) {
                 goodBlock(responseObject);
             } else {
                 
             }
             
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, settings);
          }];
}

#pragma mark -
#pragma mark Пользователь
- (void)getUserById:(uint32_t)user_id prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd", host, user_id]
      parameters: @{
                    @"with"     : @"(counters,contacts,mutuality,city,country,district,settings)"
//                    @"debug": [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"пользователь" response:responseObject operation:operation requestParameters:nil];
            if( responseObject != nil ) {
                prepareUser(responseObject);
            } else {
                
            }
        
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)getMe:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/me", host]
      parameters: @{
//                    @"debug"    : [self debugParams],
                    @"with"     : @"(counters,wishlists,socials,city,settings)"}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"пользователь Me" response:responseObject operation:operation requestParameters:nil];
             if( responseObject != nil ) {
                 prepareUser(responseObject);
             } else {
                 
             }
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark - Получить пользователей
- (void)getUsersWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [self getUsersWithOffset:offset limit:limit search:search full:NO prepareUser:prepareUser failedBlock:failedBlock];
}

- (void)getUsersWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search full:(BOOL)isFull prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users", host]
      parameters: @{@"pager"        : [self pagerWithOffset:offset limit:limit],
                    @"order"        : @"(id:desc)",
                    @"with"         : isFull ? @"(mutuality)" : @"",
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get users" response:responseObject operation:operation requestParameters:nil];
             prepareUser(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}


#pragma mark -
#pragma mark Друзья
- (void)getUserFriendsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/friends", host, user_id]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                    @"with"         : @"(counters,mutuality)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"друзья пользователя" response:responseObject operation:operation requestParameters:nil];
            prepareUser(responseObject);
        
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)getFriendsByVK:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock
{
    [self getFriendsByNetwork:[VKHandler getSnId] prepareUser:successBlock failedBlock:failedBlock];
}

- (void)getFriendsByFB:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock
{
//    [self getFriendsByNetwork:sn_id_fb prepareUser:successBlock failedBlock:failedBlock];
}

- (void)getFriendsByNetwork:(NSString *)net prepareUser:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"search"   : [NSString stringWithFormat:@"(sn_id:%@,sn_app_id:%@)", net, [net isEqualToString:[VKHandler getSnId]] ? [VKHandler getAppId] : @""],
                                                                                    @"with"     : @"(mutuality)",
                                                                                    //                                                                                    @"debug"    : [self debugParams],
                                                                                    }];
    [manager GET: [NSString stringWithFormat: @"%@/users/me/socials/friends", host]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"поиск друзей через соц.сеть" response:responseObject operation:operation requestParameters:nil];
             successBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}

#pragma mark - Соц. Сети

- (void)setVK:(NSString *)token success:(successGetItems)success failedBlock:(failedGetItems)failed
{
    [self setNetwork:[VKHandler getSnId] withToken:token success:success failedBlock:failed];
}

- (void)setFB:(NSString *)token success:(successGetItems)success failedBlock:(failedGetItems)failed
{
//    [self setNetwork:sn_id_fb withToken:token success:success failedBlock:failed];
}

- (void)setNetwork:(NSString *)net withToken:(NSString *)token success:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock
{
    NSMutableDictionary * params    = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                        @"sn_id"        : net,
                                                                                        @"sn_app_id"    : [net isEqualToString:[VKHandler getSnId]] ? [VKHandler getAppId] : @"",
                                                                                        @"token"        : token,
                                                                                        //                              @"debug"        : [self debugParams]
                                                                                        }];
    
    [manager PUT: [NSString stringWithFormat: @"%@/users/me/socials", host]
      parameters: params
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"set Network" response:responseObject operation:operation requestParameters:params];
             successBlock(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}

- (void)unsetNetwork:(uint32_t)socialId success:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock
{
    [manager DELETE: [NSString stringWithFormat: @"%@/users/me/socials/%zd", host, socialId]
         parameters: nil
            success: ^(NSURLSessionDataTask *operation, id responseObject) {
                [self requestInfo:@"delete Network" response:responseObject operation:operation requestParameters:nil];
                successBlock(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failedBlock(task, error, nil);
            }];
}


#pragma mark -
#pragma mark Подписчики
- (void)getUserFollowersById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/followers", host, user_id]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                    @"with"         : @"(counters,mutuality)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"фолловеры пользователя" response:responseObject operation:operation requestParameters:nil];
             prepareUser(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)putFollower:(uint32_t)followerId toUserId:(uint32_t)userId withCallBack:(successGetUser)callBack failedBlock:(failedGetItems)failedBlock
{
    [manager PUT: [NSString stringWithFormat: @"%@/users/%zd/followers/%zd/", host, userId, followerId]
       parameters: @{@"debug"        : [self debugParams]}
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"подписка на пользователя" response:responseObject operation:operation requestParameters:nil];
              callBack(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          failedBlock(task, error, nil);
      }];
}

- (void)deleteFollower:(uint32_t)followerId toUserId:(uint32_t)userId withCallBack:(successGetUser)callBack failedBlock:(failedGetItems)failedBlock
{
    [manager DELETE: [NSString stringWithFormat: @"%@/users/%zd/followers/%zd/", host, userId, followerId]
         parameters: @{@"debug"        : [self debugParams]}
            success: ^(NSURLSessionDataTask *operation, id responseObject) {
                [self requestInfo:@"отписка от пользователя" response:responseObject operation:operation requestParameters:nil];
                callBack(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failedBlock(task, error, nil);
            }];
}


#pragma mark -
#pragma mark Подписки
- (void)getUserFollowsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/follows", host, user_id]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                    @"with"         : @"(counters,mutuality)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"подписчики пользователя" response:responseObject operation:operation requestParameters:nil];
             prepareUser(responseObject);
        
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Общие друзья
- (void)getUserMutualFriendsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users/%zd/mutual_friends", host, user_id]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                    @"with"         : @"(counters,mutuality)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"общие друзья с пользователем" response:responseObject operation:operation requestParameters:nil];
             prepareUser(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}


#pragma mark -
#pragma mark Поиск людей
- (void)getPeople:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    NSInteger limit = 20;
    
    [manager.operationQueue cancelAllOperations];
    
    NSArray *searchArray        = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/users", host]
      parameters: @{@"pager"        : [self pagerWithOffset:page*limit limit:limit],
                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @""
//                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"popular people" response:responseObject operation:operation requestParameters:nil];
             if ( prepareUser ) {
                 prepareUser(responseObject);
             }
         } failure:^(NSURLSessionDataTask *operation, NSError *error) {
             if ( failedBlock ) {
                 failedBlock(operation, error, nil);
             }
         }];
}


- (void)getActivePeople:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    
    NSArray *searchArray        = [self searchFromDictionary:search];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"pager"        : [self pagerWithPage:page],
                                                                                    @"search"       : [searchArray count] > 0 ? ([NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]]) : @"",
                                                                                    @"with"         : @"(mutuality)"
                                                                                    }];
    
    [manager GET: [NSString stringWithFormat: @"%@/users/active", host]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get active people" response:responseObject operation:operation requestParameters:nil];
             
             if ( prepareUser ) {
                 prepareUser(responseObject);
             }
         } failure:^(NSURLSessionDataTask *operation, NSError *error) {
             if ( failedBlock ) {
                 failedBlock(operation, error, params);
             }
         }];
}


#pragma mark -
#pragma mark События
- (void)eventsWithOffset:(NSUInteger)offset prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/me/events", host]
      parameters: @{@"pager"        : [self pagerWithOffset:offset],
                    @"order"        : @"(updated:desc)",
                    @"with"         : @"(emitter,object)",
                    @"debug"        : [self debugParams]}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get events" response:responseObject operation:operation requestParameters:nil];
             prepareUser(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)eventsAfterUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    [manager GET: [NSString stringWithFormat: @"%@/users/me/events", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(updated:desc)",
                    @"search"       : [NSString stringWithFormat:@"(created-after:%@)", dateString],
                    @"with"         : @"(emitter,object)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get events after: " response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
    
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)eventsBeforeUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager.operationQueue cancelAllOperations];
    [manager GET: [NSString stringWithFormat: @"%@/users/me/events", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(updated:desc)",
                    @"search"       : [NSString stringWithFormat:@"(created-before:%@)", dateString],
                    @"with"         : @"(emitter,object)",
                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get events before: " response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
    
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}



- (void)newEventsCount:(successGetUser)prepareItem failedBlock:(failedGetItems)failedBlock
{
    AFHTTPSessionManager *m = [AFHTTPSessionManager manager];
    m.requestSerializer = [AFHTTPRequestSerializer serializer];
    m.responseSerializer = [AFJSONResponseSerializer serializer];
    m.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    m.requestSerializer.timeoutInterval = 0;
    [m.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", [TokenAccess getToken]] forHTTPHeaderField:@"Authorization"];

    [m GET: [NSString stringWithFormat: @"%@/users/me/events", host]
      parameters: @{@"debug"        : [self debugParams],
                    @"search"       : @"(status:2)",
                    @"pager"        : [self pagerOnlyWithLimit:100]}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"events" response:responseObject operation:operation requestParameters:nil];
             prepareItem(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)redeemEvents
{
    [manager POST: [NSString stringWithFormat: @"%@/users/me/events/redeem", host]
       parameters: @{}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"redeem events: " response:responseObject operation:operation requestParameters:nil];
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         }];
}


#pragma mark -
#pragma mark Тлента
- (void)feedsPage:(NSUInteger)page prepareItems:(successGetItems)prepareItems failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/me/feed", host]
      parameters: @{@"pager"        : [self pagerWithPage:page],
                    @"debug"        : [self debugParams],
                    @"with"         : @"(emitter,object)",}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get feeds" response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
}

- (void)feedsBeforeUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/me/feed", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(created:desc)",
                    @"with"         : @"(emitter,object)",
                    @"search"       : [NSString stringWithFormat:@"(created-before:%@)", dateString],
//                    @"debug"        : [self debugParams]
                    //status:1,
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get feed" response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
    
}

- (void)feedsAfterUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/users/me/feed", host]
      parameters: @{@"pager"        : [self pagerOnlyWithLimit:0],
                    @"order"        : @"(created:desc)",
                    @"with"         : @"(emitter,object)",
                    @"search"       : [NSString stringWithFormat:@"(created-after:%@)", dateString],
//                    @"debug"        : [self debugParams]
                    //status:1,
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishes" response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, nil);
          }];
    
}


#pragma mark -
#pragma mark Контакты
- (void)setContacts:(NSDictionary *)contacts goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock
{
    [manager POST: [NSString stringWithFormat: @"%@/users/me/contacts", host]
      parameters: contacts
         progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"set concats" response:responseObject operation:operation requestParameters:contacts];
             goodBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, contacts);
          }];
}


#pragma mark -
#pragma mark Push
- (void)registerPushNotification:(NSString *)token success:(successGetItems)update failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"type"     : @"1",
                             @"token"    : token,
                             @"debug"    : [self debugParams]};
    
    [manager PUT: [NSString stringWithFormat: @"%@/users/me/devices", host]
       parameters: params
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"register push" response:responseObject operation:operation requestParameters:params];
             update(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)removePushNotification:(NSString *)token callback:(successGetItems)update failedBlock:(failedGetItems)failedBlock
{    
    NSDictionary *params = @{@"type"     : @"1",
                             @"debug"    : [self debugParams]};
    
    [manager DELETE:[NSString stringWithFormat: @"%@/users/me/devices/%@", host, token]
         parameters: params
            success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self requestInfo:@"delete device token" response:responseObject operation:task requestParameters:params];
                update(nil);
            }
            failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                failedBlock(task, error, params);
                update(nil);
            }];
}

@end
