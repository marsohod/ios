//
//  UserHTTPClient.h
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HTTPClient.h"


@interface UserHTTPClient : HTTPClient

//@property (nonatomic) BOOL isReachable;

typedef void(^successGetUser)(id requestObject);

+ (UserHTTPClient *)sharedInstance;


#pragma mark - Изменить данные пользователя

- (void)setUserProfile:(NSDictionary *)prof goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock;
- (void)setUserImage:(NSData *)image
              domain:(NSString *)domain
            fullname:(NSString *)name
              gender:(NSInteger)gender
                city:(NSString *)city
              cityId:(NSString *)cityId
            birthday:(NSString *)birth
           goodBlock:(successGetUser)goodBlock
         failedBlock:(failedGetItems)failedBlock;

- (void)setUserLocationCity:(uint32_t)cityId
                  goodBlock:(successGetUser)goodBlock
                failedBlock:(failedGetItems)failedBlock;

- (void)setUserSettings:(NSDictionary *)settings goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Получить пользователя по Id

- (void)getUserById:(uint32_t)user_id prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;
- (void)getMe:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;


#pragma mark - Получить пользователей
- (void)getUsersWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;
- (void)getUsersWithOffset:(NSUInteger)offset limit:(NSUInteger)limit search:(NSDictionary *)search full:(BOOL)isFull prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;


#pragma mark - Друзья

- (void)getUserFriendsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;
- (void)getFriendsByVK:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock;
- (void)getFriendsByFB:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock;

#pragma mark - Соц. Сети

- (void)setVK:(NSString *)token success:(successGetItems)success failedBlock:(failedGetItems)failed;
- (void)setFB:(NSString *)token success:(successGetItems)success failedBlock:(failedGetItems)failed;
- (void)unsetNetwork:(uint32_t)socialId success:(successGetItems)successBlock failedBlock:(failedGetItems)failedBlock;

#pragma mark - Подписчики

- (void)getUserFollowersById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;

- (void)putFollower:(uint32_t)followerId toUserId:(uint32_t)userId withCallBack:(successGetUser)callBack failedBlock:(failedGetItems)failedBlock;
- (void)deleteFollower:(uint32_t)followerId toUserId:(uint32_t)userId withCallBack:(successGetUser)callBack failedBlock:(failedGetItems)failedBlock;


#pragma mark - Подписки

- (void)getUserFollowsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;


#pragma mark - Общие друзья

- (void)getUserMutualFriendsById:(uint32_t)user_id search:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;


#pragma mark - Поиск людей

- (void)getPeople:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;
- (void)getActivePeople:(NSDictionary *)search withPage:(NSUInteger)page prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;

#pragma mark - События

- (void)eventsWithOffset:(NSUInteger)offset prepareUser:(successGetUser)prepareUser failedBlock:(failedGetItems)failedBlock;
- (void)eventsAfterUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)eventsBeforeUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)newEventsCount:(successGetUser)prepareItem failedBlock:(failedGetItems)failedBlock;
- (void)redeemEvents;

#pragma mark - Лента

- (void)feedsPage:(NSUInteger)page prepareItems:(successGetItems)prepareItems failedBlock:(failedGetItems)failedBlock;
- (void)feedsAfterUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)feedsBeforeUpdate:(NSString *)dateString prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Контакты

- (void)setContacts:(NSDictionary *)contacts goodBlock:(successGetUser)goodBlock failedBlock:(failedGetItems)failedBlock;


#pragma mark - Push

- (void)registerPushNotification:(NSString *)token success:(successGetItems)update failedBlock:(failedGetItems)failedBlock;
- (void)removePushNotification:(NSString *)token callback:(successGetItems)update failedBlock:(failedGetItems)failedBlock;

@end
