//
//  LoginHTTPClient.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HTTPClient.h"


@interface LoginHTTPClient : HTTPClient

+ (LoginHTTPClient *)sharedInstance;


#pragma mark - Login
- (void)login:(NSString *)email pass:(NSString *)passwd success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;
- (void)reloginWithToken:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;

#pragma mark - Login by Social
- (void)loginVK:(NSString *)email userId:(NSString *)userId token:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;
- (void)loginFB:(NSString *)token email:(NSString *)email userId:(NSString *)userId userName:(NSString *)userName success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;
- (void)socialCheck:(NSString *)net email:(NSString *)email userId:(NSString *)userId token:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;
- (void)registerVK:(NSString *)token user:(NSDictionary *)user success:(successGetItems)successBlock failed:(failedGetItems)failedBlock;

#pragma mark - Register
- (void)registerEmail:(NSString *)email withFullName:(NSString *)fullname withPasswods:(NSString *)password success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock;

#pragma mark - Repair
- (void)reparePasswordToEmail:(NSString *)email success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock;

@end
