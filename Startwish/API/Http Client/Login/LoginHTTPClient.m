//
//  LoginHTTPClient.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LoginHTTPClient.h"
#import "VKHandler.h"
#import "Settings.h"
#import "PushNotificationHandler.h"


@implementation LoginHTTPClient

#define sn_id_fb        @"1"

+ (LoginHTTPClient *)sharedInstance
{
    static LoginHTTPClient *client = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[LoginHTTPClient alloc] init];
    });
    
    return client;
}

- (void)login:(NSString *)email pass:(NSString *)passwd success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"grant_type"   : @"password",
                              @"client_id"    : @"web",
                              @"username"     : email,
                              @"password"     : passwd,
//                              @"mac"          : [Settings getMacAddress],
                              @"debug"        : [self debugParams]};
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/token", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"login" response:responseObject operation:operation requestParameters:params];
            successBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}

- (void)reloginWithToken:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"grant_type"   : @"refresh_token",
                              @"client_id"    : @"ios",
                              @"refresh_token": token.length == 0 ? @"" : token,
                              @"debug"        : [self debugParams]};
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/token", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"relogin" response:responseObject operation:operation requestParameters:params];
              successBlock(responseObject);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)socialCheck:(NSString *)net email:(NSString *)email userId:(NSString *)userId token:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"sn_id"        : [net isEqualToString:[VKHandler getSnId]] ? [VKHandler getSnId] : sn_id_fb,
                              @"email"        : email != nil ? email : @"",
                              @"identifier"   : userId,
                              @"token"        : token != nil ? token : @""
                              };
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/social/check", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"login social check" response:responseObject operation:operation requestParameters:params];
              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)loginFB:(NSString *)token email:(NSString *)email userId:(NSString *)userId userName:(NSString *)userName success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    [self loginByNetwork:sn_id_fb email:email userId:userId token:(NSString *)token success:successBlock failed:failedBlock];
}

- (void)loginVK:(NSString *)email userId:(NSString *)userId token:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    [self loginByNetwork:[VKHandler getSnId] email:email userId:userId token:(NSString *)token success:successBlock failed:failedBlock];
}

- (void)loginByNetwork:(NSString *)net email:(NSString *)email userId:(NSString *)userId token:(NSString *)token success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"grant_type"   : @"password",
                              @"client_id"    : @"ios",
                              @"sn_id"        : net,
                              @"email"        : email != nil ? email : @"",
                              @"identifier"   : userId,
                              @"token"        : token != nil ? token : @""
//                              @"mac"          : [Settings getMacAddress],
//                              @"debug"        : [self debugParams]
                              };
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/social/login", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"login social" response:responseObject operation:operation requestParameters:params];
              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)registerVK:(NSString *)token user:(NSDictionary *)user success:(successGetItems)successBlock failed:(failedGetItems)failedBlock
{
    NSMutableDictionary * params    = [[NSMutableDictionary alloc] initWithDictionary:@{
                                            @"grant_type"   : @"password",
                                            @"client_id"    : @"ios",
                                            @"sn_id"        : [VKHandler getSnId],
                                            @"token"        : token,
//                                            @"mac"          : [Settings getMacAddress],
                                            //                              @"debug"        : [self debugParams]
                                        }];
    
    [params addEntriesFromDictionary:user];
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/social/register", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"login" response:responseObject operation:operation requestParameters:params];

              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

- (void)reparePasswordToEmail:(NSString *)email success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"email"    : email,
                              @"debug"    : [self debugParams]};
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/remind", host]
      parameters: params
         progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"remind" response:responseObject operation:operation requestParameters:params];
             
             successBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}


- (void)registerEmail:(NSString *)email withFullName:(NSString *)fullname withPasswods:(NSString *)password success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock
{
    NSDictionary * params = @{@"login"          : email,
//                              @"username"       : fullname,
                              @"sex"            : @"2",
                              @"password"       : password,
                              @"grant_type"     : @"password",
                              @"client_id"      : @"ios",
//                              @"debug"       : [self debugParams]
                              };
    
    [manager POST: [NSString stringWithFormat: @"%@/auth/register", host]
       parameters: params
         progress: nil
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"register user" response:responseObject operation:operation requestParameters:params];
             successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              failedBlock(task, error, params);
          }];
}

@end
