//
//  WishlistsHTTPClient.h
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "HTTPClient.h"


@interface WishlistsHTTPClient : HTTPClient

+ (WishlistsHTTPClient *)sharedInstance;

// Отдельный Вишлист
- (void)getWishlistById:(uint32_t)itemId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

// Желания Вишлиста
- (void)getWishesFromWishlistId:(uint32_t)itemId page:(NSInteger)page prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

- (void)getShortWishesFromWishlistId:(uint32_t)itemId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

// Список Вишлистов
- (void)getItemsFromPage:(NSInteger)page byUserId:(uint32_t)userId search:(NSDictionary *)search prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;
- (void)getItemsWithOffset:(NSInteger)offset limit:(NSInteger)limit byUserId:(uint32_t)userId search:(NSDictionary *)search prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

- (void)getAllWishlistsByUserId:(uint32_t)userId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock;

// Добавить Вишлист
- (void)createWishlist:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel prepareItem:(successGetItems)prepareItem failedBlock:(failedGetItems)failedBlock;

- (void)editWishlistById:(uint32_t)wishlistId name:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel prepareItem:(successGetItems)prepareItem failedBlock:(failedGetItems)failedBlock;

// Удалить Вишлист
- (void)removeWishlistById:(uint32_t)wishlistId reason:(NSInteger)reason toWishlistId:(uint32_t)toWid withCallBack:(successGetItems)update failedBlock:(failedGetItems)failedBlock;
@end
