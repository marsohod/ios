//
//  WishlistsHTTPClient.m
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishlistsHTTPClient.h"


@implementation WishlistsHTTPClient

+ (WishlistsHTTPClient *)sharedInstance
{
    static WishlistsHTTPClient *client = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        client = [[WishlistsHTTPClient alloc] init];
    });
    
    return client;
}


#pragma mark -
#pragma mark Вишлист
- (void)getWishlistById:(uint32_t)itemId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{

    [manager GET: [NSString stringWithFormat:@"%@/wishlists/%zd/", host, itemId]
      parameters: @{
                    @"with"         : @"(counters)",
//                    @"debug"        : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
            [self requestInfo:@"wishlist" response:responseObject operation:operation requestParameters:nil];
             prepareItems(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}


#pragma mark -
#pragma mark Желания Вишлиста
- (void)getWishesFromWishlistId:(uint32_t)itemId page:(NSInteger)page prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"pager"        : [self pagerWithPage:page],
                            @"search"       : @"(status:1)",
                            @"order"        : @"(id:desc)",
                            @"with"         : @"(counters,mutuality)",
                            @"debug"        : [self debugParams]
                            };
    
    [manager GET: [NSString stringWithFormat:@"%@/wishlists/%zd/wishes", host, itemId]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishes wishlist" response:responseObject operation:operation requestParameters:params];
             prepareItems(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}

- (void)getShortWishesFromWishlistId:(uint32_t)itemId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"pager"        : [self pagerWithOffset:0 limit:6],
                             @"search"       : @"(status:1)",
                             @"fields"       : @"(created,id,name,img_src,img_height,img_width,owner_id)",
                             @"debug"        : [self debugParams],
                             @"order"        : @"(id:desc)"
                             };
    
    [manager GET: [NSString stringWithFormat:@"%@/wishlists/%zd/wishes", host, itemId]
      parameters: params
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get short wishes wishlist" response:responseObject operation:operation requestParameters:params];
             prepareItems(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}


#pragma mark -
#pragma mark Список Вишлистов
- (void)getItemsFromPage:(NSInteger)page byUserId:(uint32_t)userId search:(NSDictionary *)search prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    [self getItemsWithOffset:page*itemPerPage limit:itemPerPage byUserId:userId search:search prepareItems:prepareItems endItems:badBlock failedBlock:failedBlock];
}

- (void)getItemsWithOffset:(NSInteger)offset limit:(NSInteger)limit byUserId:(uint32_t)userId search:(NSDictionary *)search prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [searchArray addObject:@"status:1"];
    
    NSDictionary *p = @{@"pager"        : [self pagerWithOffset:offset limit:limit],
                        @"search"       : [NSString stringWithFormat:@"(%@)", [searchArray componentsJoinedByString:@","]],
                        @"order"        : @"(created:desc)",
                        @"with"         : @"(counters)"
//                        @"debug"        : [self debugParams]
                        };
    
    [manager GET: [NSString stringWithFormat:@"%@/users/%zd/wishlists", host, userId]
      parameters: p
        progress: nil
         success:^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get wishlists" response:responseObject operation:operation requestParameters:p];
             prepareItems(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, p);
         }];
}

- (void)getAllWishlistsByUserId:(uint32_t)userId prepareItems:(successGetItems)prepareItems endItems:(badGetItems)badBlock failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"pager"        : [self pagerOnlyWithOffset:0],
                             @"search"       : @"(status:1)",
                             @"fields"       : @"(id,name,privacy)",
                             @"order"        : @"(created:desc)",
//                             @"debug"        : [self debugParams]
                             };
    
    [manager GET: [NSString stringWithFormat:@"%@/users/%zd/wishlists", host, userId]
      parameters: params
        progress: nil
         success:^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get my wishlists" response:responseObject operation:operation requestParameters:params];
             prepareItems(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}


#pragma mark -
#pragma mark Добавить Вишлист
- (void)createWishlist:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel prepareItem:(successGetItems)prepareItem failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"name"         : name,
                             @"description"  : desc,
                             @"privacy"      : [NSString stringWithFormat:@"%lu", (unsigned long)privateLevel],
                             @"debug"        : [self debugParams]};
    
    [manager PUT: [NSString stringWithFormat:@"%@/wishlists", host]
      parameters: params
         success:^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"create wishlist" response:responseObject operation:operation requestParameters:params];
             prepareItem(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}

- (void)editWishlistById:(uint32_t)wishlistId name:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel prepareItem:(successGetItems)prepareItem failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"name"         : name,
                             @"description"  : desc,
                             @"privacy"      : [NSString stringWithFormat:@"%lu", (unsigned long)privateLevel],
                             @"debug"        : [self debugParams]};
    
    [manager POST: [NSString stringWithFormat:@"%@/wishlists/%zd", host, wishlistId]
       parameters: params
         progress: nil
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"create wishlist" response:responseObject operation:operation requestParameters:params];
              prepareItem(responseObject);
              
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, params);
         }];
}


#pragma mark -
#pragma mark Удалить Вишлист
- (void)removeWishlistById:(uint32_t)wishlistId reason:(NSInteger)reason toWishlistId:(uint32_t)toWid withCallBack:(successGetItems)update failedBlock:(failedGetItems)failedBlock
{
    if ( reason == 2 ) {
        [manager POST: [NSString stringWithFormat:@"%@/wishlists/%zd/execute", host, wishlistId]
           parameters: nil
             progress: nil
              success:^(NSURLSessionDataTask *operation, id responseObject) {
                  [self requestInfo:@"remove wishlist with execute wishes" response:responseObject operation:operation requestParameters:nil];
                  update(responseObject);
              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failedBlock(task, error, nil);
              }];
        
        return;
    }
    
    if ( reason == 3 ) {
        [manager POST: [NSString stringWithFormat:@"%@/wishlists/%zd/wishes/move/%zd", host, wishlistId, toWid]
           parameters: nil
             progress: nil
              success:^(NSURLSessionDataTask *operation, id responseObject) {
                  [self requestInfo:@"remove wishlist with execute wishes" response:responseObject operation:operation requestParameters:nil];
                  update(responseObject);
              } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                  failedBlock(task, error, nil);
              }];
        
        return;
    }

    [manager DELETE: [NSString stringWithFormat:@"%@/wishlists/%zd", host, wishlistId]
         parameters: nil
            success:^(NSURLSessionDataTask *operation, id responseObject) {
                [self requestInfo:@"remove wishlist" response:responseObject operation:operation requestParameters:nil];
                update(responseObject);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}

@end
