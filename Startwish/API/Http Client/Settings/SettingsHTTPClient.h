//
//  SettingsHTTPClient.h
//  Startwish
//
//  Created by marsohod on 08/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "HTTPClient.h"


@interface SettingsHTTPClient : HTTPClient

+ (SettingsHTTPClient *)sharedInstance;

- (void)cities:(NSDictionary *)search success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock;
- (void)cityByLatitude:(double)lat longitude:(double)lon success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock;


#pragma mark - Жалобы Spam

- (void)spamWishId:(uint32_t)wishId reason:(NSInteger)reason callBack:(successGetItems)update failedBlock:(failedGetItems)failed;
- (void)spamReasonItems:(successGetItems)successBlock failedBack:(failedGetItems)failedBlock;

#pragma mark - Сохранить изображения на Storage

/*! Save image to Storage. return height, width, image */
- (void)saveImage:(UIImage *)image success:(successGetItems)successBlock failedBack:(failedGetItems)failedBlock;

@end
