//
//  SettingsHTTPClient.m
//  Startwish
//
//  Created by marsohod on 08/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SettingsHTTPClient.h"
#import "Settings.h"
#import "UIImage+Helper.h"


@implementation SettingsHTTPClient

+ (SettingsHTTPClient *)sharedInstance
{
    static SettingsHTTPClient *client = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        client = [[SettingsHTTPClient alloc] init];
    });
    
    return client;
}

- (void)cities:(NSDictionary *)search success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock
{
    NSMutableArray *searchArray = [self searchFromDictionary:search];
    
    [manager GET: [NSString stringWithFormat: @"%@/cities", host]
      parameters: @{@"search"   : ([searchArray count] > 0 ? [searchArray componentsJoinedByString:@","] : @""),
                    @"with"     : @"(region)"
//                    @"debug"    : [self debugParams]
                    }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get cities: " response:responseObject operation:operation requestParameters:nil];
             successBlock(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}

- (void)cityByLatitude:(double)lat longitude:(double)lon success:(successGetItems)successBlock error:(badGetItems)badBlock failed:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/utils/geocodeReverse", host]
      parameters: @{@"lat"   : [NSNumber numberWithDouble:lat],
                    @"lon"   : [NSNumber numberWithDouble:lon],
                    @"with"     : @"(region)",
                    @"debug"    : [self debugParams]
                  }
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get cities by Latitude: " response:responseObject operation:operation requestParameters:nil];
             
             successBlock(responseObject);
             
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}


#pragma mark - Жалобы Spam

- (void)spamReasonItems:(successGetItems)successBlock failedBack:(failedGetItems)failedBlock
{
    [manager GET: [NSString stringWithFormat: @"%@/feedback/reasons", host]
      parameters: @{@"debug"    : [self debugParams]}
        progress: nil
         success: ^(NSURLSessionDataTask *operation, id responseObject) {
             [self requestInfo:@"get spam reasons: " response:responseObject operation:operation requestParameters:nil];
             
             successBlock(responseObject);
         } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             failedBlock(task, error, nil);
         }];
}

- (void)spamWishId:(uint32_t)wishId reason:(NSInteger)reason callBack:(successGetItems)update failedBlock:(failedGetItems)failedBlock
{
    NSDictionary *params = @{@"debug"       : [self debugParams],
                             @"wish_id"     : [NSNumber numberWithUnsignedLong:wishId],
                             @"reason_id"   : [NSNumber numberWithInteger:reason]};
    
    [manager PUT: [NSString stringWithFormat: @"%@/feedback", host]
       parameters: params
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"post spam: " response:responseObject operation:operation requestParameters:params];
              update(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              
              failedBlock(task, error, params);
          }];
}

#pragma mark - Сохранить изображения на Storage

- (void)saveImage:(UIImage *)image success:(successGetItems)successBlock failedBack:(failedGetItems)failedBlock
{    
    [manager POST: [NSString stringWithFormat: @"%@/utils/storeImage", host]
       parameters: nil
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    if( image ) {
        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, [UIImage h_max_qualityOfImage:image afterQuality:0.7]) name:@"file" fileName:@"image.jpg" mimeType:@"image/jpeg"];
    }
}
         progress:  ^(NSProgress * _Nonnull uploadProgress) {
             [HTTPClient setUploadProgress:uploadProgress];
         }
          success: ^(NSURLSessionDataTask *operation, id responseObject) {
              [self requestInfo:@"добавление картинки" response:responseObject operation:operation requestParameters:nil];
              successBlock(responseObject);
          } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
              [self requestInfo:@"добавление картинки" response:nil operation:task requestParameters:nil];
              failedBlock(task, error, nil);
          }];

}

@end
