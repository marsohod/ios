//
//  DBAPI.h
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface DBAPI : NSObject

+ (DBAPI *)sharedInstance;

@end
