//
//  Routing.h
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MainNavigationController.h"
#import "WishesViewController.h"
#import "AuthViewController.h"
#import "WishDetailViewController.h"
#import "RevealViewController.h"
#import "ForgotPasswordViewController.h"
#import "CitySearchViewController.h"
#import "ContactsViewController.h"
#import "PeopleViewController.h"
#import "UserDetailViewController.h"
#import "WishlistsViewController.h"
#import "ExplorerViewController.h"
#import "EventsViewController.h"
#import "CommentsViewController.h"
#import "CreateWishlistViewController.h"
#import "CreateWishViewController.h"
#import "SettingsViewController.h"
#import "PeopleRatingViewController.h"
#import "ExecuteWishViewController.h"
#import "ChannelRecomendationViewController.h"
#import "FeedViewController.h"
#import "SpamViewController.h"


@interface Routing : NSObject

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) UIStoryboard *storyboard;


#pragma mark - Init

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController;

+ (Routing *)sharedInstance;
+ (Routing *)sharedInstanceWithNavigationController:(UINavigationController *)navigationController;


#pragma mark - Common Segues

+ (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;


#pragma mark - Channel Recomendation
+ (id)channeRecomendationController;

#pragma mark - Auth screen
- (void)showAuthController;

#pragma mark - User Profile screen

- (void)showUserControllerWithUser:(ModelUser *)user fromMenu:(BOOL)isFromMenu;
- (void)showUserControllerWithOwn;


#pragma mark - Password screen

+ (void)goToForgotPasswordCtrl:(UIStoryboardSegue *)segue email:(NSString *)email;


#pragma mark - City screen

+ (void)goToCitySearchController:(UIStoryboardSegue *)segue delegate:(id)delegate;


#pragma mark - Contacts screen

+ (void)goToContactsController:(UIStoryboardSegue *)segue canSeeContacts:(BOOL)privateStatus contacts:(NSDictionary *)contacts userName:(NSString *)name userId:(uint32_t)userId isUserMan:(BOOL)isUserMan;


#pragma mark - Comments screen

+ (void)goToCommentsController:(UIStoryboardSegue *)segue wishId:(uint32_t)wishId wishName:(NSString *)wishName comments:(NSArray *)comments maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName;
- (void)showCommentsControllerWithWishId:(uint32_t)wishId wishName:(NSString *)wishName comments:(NSArray *)comments maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName;


#pragma mark - Events screen

- (void)showEventsController;


#pragma mark - Feed screen

- (void)showFeedController;
- (void)showFeedController:(NSArray *)items andHideModal:(BOOL)isNeedHide;

#pragma mark - Wishlists screen

+ (void)goToWishlistsCtrl:(UIStoryboardSegue *)segue user:(ModelUser *)user;
+ (void)goToWishlistDetailCtrl:(UIStoryboardSegue *)segue wishlistId:(uint32_t)wishlistid byUser:(ModelUser *)u;
- (void)goToWishlistById:(uint32_t)wishlistid byUser:(ModelUser *)u;
- (void)showCreateWishlistControllerWithWishlist:(id)wishlist;


#pragma mark - Wishes screen

+ (void)goToWishesCtrl:segue user:(ModelUser *)u isCommonWishes:(BOOL)isCommon isImplementWishes:(BOOL)isImplement;
- (void)goToWishesCtrlByOwn;
- (void)showWishCtrlWithWish:(ModelWish *)wish;
- (void)showWishCtrlWithWishId:(uint32_t)wishId;
- (void)showExplorerController;
- (void)showExplorerControllerWithCatregoryId:(uint32_t)categoryId;
- (void)showCreateWishControllerWithImage:(UIImage *)image;
- (void)closeCreateWishController;


#pragma mark - People screen

- (void)showPeopleControllerType:(NSInteger)peopleType byUserId:(uint32_t)userId fromMenu:(BOOL)isFromMenu;
- (void)showPeopleCtrl;
- (void)showPeopleSearchCtrlFromMenu:(BOOL)fromMenu;
+ (void)goToPeopleCtrl:segue userId:(uint32_t)userId peopleType:(NSInteger)peopleType;
- (void)showPeopleRatingCtrl;


#pragma mark - Settings screen
- (void)showSettingsController;


#pragma mark - Camera screen

- (void)showCameraController;
- (void)showCameraControllerWithDelegate:(id)delegate;
- (void)showCameraFiltersController:(UIImage *)photo;
- (void)showCameraFiltersController:(UIImage *)photo withFinishController:(id)ctrl;
- (void)showCameraFiltersController:(UIImage *)photo withFinishController:(id)ctrl animated:(BOOL)animated;
- (void)closeCameraController;
- (void)openPhotoLibraryControllerWithDelegate:(id)delegate withCropSize:(CGSize)size;
- (void)backToCtrl:(id)ctrl;
- (void)backToCtrl:(id)ctrl animated:(BOOL)animated;

#pragma mark - Channel Recomendation

- (void)showChannelRecomendationController;
- (void)showChannelRecomendationControllerFromStart:(BOOL)fromStart;


#pragma mark - Spam
+ (void)showSpamController:(UIStoryboardSegue *)segue wishId:(uint32_t)wishId;


#pragma mark - Open Link in App

- (void)openLinkInApp:(NSURL *)url;
@end
