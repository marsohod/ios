//
//  Routing.m
//  Startwish
//
//  Created by marsohod on 15/12/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "Routing.h"
#import "CameraPhotoController.h"
#import "CameraFiltersController.h"
#import "CustomImagePicker.h"
#import "Me.h"
#import "NSString+Helper.h"


@implementation Routing

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController
{
    if (self = [super init])
    {
        _storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        _navigationController = navigationController ? navigationController : [self mainNavigationController];
    }
    return self;
}

+ (Routing *)sharedInstance
{
    static Routing *r = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        r = [Routing sharedInstanceWithNavigationController:nil];
    });
    
    return r;
}

+ (Routing *)sharedInstanceWithNavigationController:(UINavigationController *)navigationController
{
    static Routing *r = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        r = [[Routing alloc] initWithNavigationController:navigationController];
    });
    
    return r;
}


#pragma mark - NC plus common Prepare

- (MainNavigationController *)mainNavigationController
{
    return (MainNavigationController *)[_storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
}

+ (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if( [segue.identifier isEqualToString: @"wishDetail"] ) {
        
        [(WishDetailViewController *)[segue destinationViewController] setDetail: sender];
        
    } else if( [segue.identifier isEqualToString:@"userDetail"] ) {
        
        [(UserDetailViewController *)[segue destinationViewController] setDetail: sender fromMenu:NO];
        
    } else if( [segue.identifier isEqualToString:@"startDetail"] ) {
        
        [(RevealViewController *)segue.destinationViewController setStartFeed: sender];
        
    } else if ( [segue.identifier isEqualToString:@"authDetail"] ) {
        
    } else if ( [segue.identifier isEqualToString:@"executeWish"] ) {
        [(ExecuteWishViewController *)[segue destinationViewController] setDetail: sender];
    }
}


#pragma mark - Auth

+ (id)channeRecomendationController
{
    return [[ChannelRecomendationViewController alloc] initWithNibName:@"ChannelRecomendation" bundle:nil];
}

#pragma mark - Auth

- (void)showAuthController
{
    if ( [[self.navigationController.viewControllers firstObject] isKindOfClass:[AuthViewController class]] || [self.navigationController.visibleViewController isKindOfClass:[AuthViewController class]] ) {
        return;
    }
    
    AuthViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"AuthCtrl"];
    [self.navigationController setViewControllers:@[ctrl] animated:YES];
}


#pragma mark - Profile

- (void)showUserControllerWithOwn
{
    [self showUserControllerWithUser:[[ModelUser alloc] initWithId:[Me myProfileId]] fromMenu:YES];
}

- (void)showUserControllerWithUser:(ModelUser *)user fromMenu:(BOOL)isFromMenu
{
    if ( [self.navigationController.visibleViewController isKindOfClass:[UserDetailViewController class]] ) {
        if ( [(UserDetailViewController *)self.navigationController.visibleViewController thisUserIsOpened:user] ) {
            return;
        }
    }
    
    UserDetailViewController *userCtrl = [_storyboard instantiateViewControllerWithIdentifier:@"UserDetailCtrl"];
    [userCtrl setDetail:user fromMenu:isFromMenu];
    [self.navigationController pushViewController:userCtrl animated:YES];
}


#pragma mark - Forgot Password

+ (void)goToForgotPasswordCtrl:(UIStoryboardSegue *)segue email:(NSString *)email;
{
    [(ForgotPasswordViewController *)segue.destinationViewController setDetail: email];
}


#pragma mark - City Search

+ (void)goToCitySearchController:(UIStoryboardSegue *)segue delegate:(id)delegate
{
    CitySearchViewController *ctrl = (CitySearchViewController *)segue.destinationViewController;
    ctrl.chooseCityDelegate = delegate;
}


#pragma mark - Contacts

+ (void)goToContactsController:(UIStoryboardSegue *)segue canSeeContacts:(BOOL)privateStatus contacts:(NSDictionary *)contacts userName:(NSString *)name userId:(uint32_t)userId isUserMan:(BOOL)isMan;
{
    [(ContactsViewController *)[segue destinationViewController] setDetail: contacts canSeeContacts: privateStatus userName:name userId:userId isUserMan:isMan];
}


#pragma mark - Comments

+ (void)goToCommentsController:(UIStoryboardSegue *)segue wishId:(uint32_t)wishId wishName:(NSString *)wishName comments:(NSArray *)comments maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName
{
    [(CommentsViewController *)[segue destinationViewController] setComments:comments fromWishId:wishId wishName:wishName maxCount:count replyToCommentId:commentId replyToUserName: userName];
}

- (void)showCommentsControllerWithWishId:(uint32_t)wishId wishName:(NSString *)wishName comments:(NSArray *)comments maxCount:(NSUInteger)count replyToCommentId:(uint32_t)commentId replyToUserName:(NSString *)userName
{
    CommentsViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"CommentsCtrl"];
    
    [ctrl setComments:comments fromWishId:wishId wishName:wishName maxCount:count replyToCommentId:commentId replyToUserName:userName];
    
    [self.navigationController pushViewController:ctrl animated:YES];
}


#pragma mark - Wishlists

+ (void)goToWishlistsCtrl:(UIStoryboardSegue *)segue user:(ModelUser *)user
{
    [(WishlistsViewController *)[segue destinationViewController] wishlistsByUser: user];
}

+ (void)goToWishlistDetailCtrl:(UIStoryboardSegue *)segue wishlistId:(uint32_t)wishlistid byUser:(ModelUser *)u
{
    [(WishesViewController *)[segue destinationViewController] setDetailWishlist: wishlistid byUser:(ModelUser *)u];
}

- (void)goToWishlistById:(uint32_t)wishlistid byUser:(ModelUser *)u
{
    WishesViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"WishesCtrl"];
    
    [ctrl setDetailWishlist:wishlistid byUser:u];
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)showCreateWishlistControllerWithWishlist:(id)wishlist
{
    CreateWishlistViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"CreateWishlist"];
    
    if ( wishlist ) {
        [ctrl setDetail:wishlist];
    }
    
    [self.navigationController pushViewController:ctrl animated:YES];
}


#pragma mark - Wishes
- (void)showCreateWishControllerWithImage:(UIImage *)image
{
    CreateWishViewController *ctrl =[_storyboard instantiateViewControllerWithIdentifier:@"CreateWish"];
    [ctrl setDetail:image];
    [self.navigationController pushViewController:ctrl animated:YES];
    
}

+ (void)goToWishesCtrl:segue user:(ModelUser *)user isCommonWishes:(BOOL)isCommon isImplementWishes:(BOOL)isImplement
{
    [(WishesViewController *)[segue destinationViewController] setDetailUserWishes:user mutualWishes:isCommon implementWishes:isImplement];
}

- (void)goToWishesCtrlByOwn
{
    id ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"WishesCtrl"];
    
    if( [Me myProfileId] == 0 ) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedAuthUser" object:nil];
    } else {
        [(WishesViewController *)ctrl setDetailUserWishes:[Me myProfile] fromMenu:YES];
    }
    
    [self.navigationController setViewControllers:@[ctrl] animated:NO];
}

- (void)showWishCtrlWithWish:(ModelWish *)wish
{
    WishDetailViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"WishDetailCtrl"];
    
    [ctrl setDetail:wish];
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)showWishCtrlWithWishId:(uint32_t)wishId
{
    WishDetailViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"WishDetailCtrl"];
    
    [ctrl setWishId:wishId];
    
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (void)closeCreateWishController
{
    __block BOOL isPoped;
    
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(UIViewController *obj, NSUInteger idx, BOOL *stop) {
        if ( obj.view.tag == tagControllerFromOpenCamera ) {
            [self.navigationController popToViewController:obj animated:YES];
            isPoped = YES;
            *stop = YES;
        }
    }];

    if ( !isPoped ) {
        [self.navigationController setViewControllers:[NSArray arrayWithObject:[self.navigationController.viewControllers firstObject]] animated:YES];
    }
}


#pragma mark - People

- (PeopleViewController *)peopleCtrl
{
    return [_storyboard instantiateViewControllerWithIdentifier:@"PeopleCtrl"];
}

- (void)showPeopleSearchCtrlFromMenu:(BOOL)fromMenu
{
    [self showPeopleControllerType:4 byUserId:0 fromMenu:fromMenu];
}

- (void)showPeopleControllerType:(NSInteger)peopleType byUserId:(uint32_t)userId fromMenu:(BOOL)isFromMenu
{
    id ctrl = [self peopleCtrl];
    
    [ctrl peopleType:peopleType byUserId:userId fromMenu:isFromMenu];
    
    if ( isFromMenu ) {
        [self.navigationController setViewControllers:@[ctrl] animated:NO];
    } else {
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

- (void)showPeopleCtrl
{
    id ctrl = [self peopleCtrl];
    [ctrl peopleType:0 byUserId:[Me myProfileId] fromMenu:YES];
    [self.navigationController setViewControllers:@[ctrl] animated:NO];
}

+ (void)goToPeopleCtrl:segue userId:(uint32_t)userId peopleType:(NSInteger)peopleType
{
    [(PeopleViewController *)[segue destinationViewController] peopleType:peopleType byUserId:userId];
}

- (void)showPeopleRatingCtrl
{
    [self.navigationController pushViewController:[_storyboard instantiateViewControllerWithIdentifier:@"PeopleRatingCtrl"] animated:YES];
}


#pragma mark - Settings
- (void)showSettingsController
{
    SettingsViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"SettingsCtrl"];
    [ctrl displayFromMenu:YES];
    [self.navigationController setViewControllers:@[ctrl] animated:NO];
}


#pragma mark - Explorer

- (void)showExplorerController
{
    [self.navigationController setViewControllers:@[[_storyboard instantiateViewControllerWithIdentifier:@"ExplorerCtrl"]] animated:NO];
}

- (void)showExplorerControllerWithCatregoryId:(uint32_t)categoryId
{
    ExplorerViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"ExplorerCtrl"];
    
    [ctrl setOpenedCategory:categoryId];
    [self.navigationController pushViewController:ctrl animated:NO];
}



#pragma mark - Events

- (void)showEventsController;
{
    [self.navigationController setViewControllers:@[[_storyboard instantiateViewControllerWithIdentifier:@"EventsCtrl"]] animated:NO];
}


#pragma mark - Feed screen

- (void)showFeedController
{
    [self showFeedController:nil andHideModal:NO];
}

- (void)showFeedController:(NSArray *)items andHideModal:(BOOL)isNeedHide
{
//    NSLog(@"class: %@ %@", [[self.navigationController.viewControllers firstObject] class], [self.navigationController.visibleViewController class] );
    if ( [[self.navigationController.viewControllers firstObject] isKindOfClass:[FeedViewController class]] || [self.navigationController.visibleViewController isKindOfClass:[FeedViewController class]]) {
        return;
    }
    
    FeedViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"FeedViewCtrl"];
    [ctrl setStartFeed:items];
    
    [self.navigationController setViewControllers:@[ctrl] animated:!isNeedHide];
    
    if ( isNeedHide ) {
        [self.navigationController.visibleViewController dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Camera Screen
const NSInteger tagControllerFromOpenCamera = 567;

- (void)showCameraController
{
    [self.navigationController.viewControllers enumerateObjectsUsingBlock:^(UIViewController *ctrl, NSUInteger idx, BOOL *stop) {
        ctrl.view.tag = 0;
    }];
    
    self.navigationController.visibleViewController.view.tag = tagControllerFromOpenCamera;
    [self showCameraControllerWithDelegate:nil];
}

- (void)showCameraControllerWithDelegate:(id)delegate
{
    CameraPhotoController *ctrl = [[CameraPhotoController alloc] initWithNibName:@"CameraPhoto" bundle:nil];
    ctrl.cameraDelegate = delegate;
    [self.navigationController pushViewController:ctrl animated:YES];
}

- (id)presentCameraControllerWithDelegate:(id)delegate
{
    CameraPhotoController *ctrl = [[CameraPhotoController alloc] initWithNibName:@"CameraPhoto" bundle:nil];
    ctrl.cameraDelegate = delegate;
    return ctrl;
}

- (void)closeCameraController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)openPhotoLibraryControllerWithDelegate:(id)delegate withCropSize:(CGSize)size;
{
    CustomImagePicker *ctrl = [[CustomImagePicker alloc] init];
    ctrl.ctrlDelegate = (id<IPDelegate>)delegate;
    
    if ( !CGSizeEqualToSize(CGSizeZero, size) ) {
        [ctrl setCropToSize:size];
    }
    
    [self.navigationController presentViewController:ctrl animated:YES completion:nil];
}

- (id)photoLibraryControllerWithDelegate:(id)delegate withCropSize:(CGSize)size
{
    CustomImagePicker *ctrl = [[CustomImagePicker alloc] init];
    ctrl.ctrlDelegate = (id<IPDelegate>)delegate;
    
    if ( !CGSizeEqualToSize(CGSizeZero, size) ) {
        [ctrl setCropToSize:size];
    }
    
    return ctrl;
}


- (void)showCameraFiltersController:(UIImage *)photo
{
    [self showCameraFiltersController:photo withFinishController:nil animated:YES];
}

- (void)showCameraFiltersController:(UIImage *)photo withFinishController:(id)ctrl
{
    [self showCameraFiltersController:photo withFinishController:ctrl animated:YES];
}

- (void)showCameraFiltersController:(UIImage *)photo withFinishController:(id)ctrl animated:(BOOL)animated
{
    CameraFiltersController *c = [[CameraFiltersController alloc] initWithNibName:@"CameraFilters" bundle:nil];
    [c setDetail:photo finishCtrl:ctrl];
    [self.navigationController pushViewController:c animated:animated];
}

- (void)backToCtrl:(id)ctrl
{
    [self backToCtrl:ctrl animated:YES];
}

- (void)backToCtrl:(id)ctrl animated:(BOOL)animated
{
    if ( ctrl != nil ) {
        [self.navigationController popToViewController:ctrl animated:YES];
    }
}


#pragma mark - Channel Recomendation

- (void)showChannelRecomendationController
{
    [self showChannelRecomendationControllerFromStart:NO];
}

- (void)showChannelRecomendationControllerFromStart:(BOOL)fromStart
{
    ChannelRecomendationViewController *ctrl = [_storyboard instantiateViewControllerWithIdentifier:@"ChannelRecomendation"];
    
    if ( fromStart ) {
        [self.navigationController setViewControllers:@[ctrl] animated:YES];
        return;
    }
    
    [self.navigationController pushViewController:ctrl animated:YES];
}


#pragma mark - Spam

+ (void)showSpamController:(UIStoryboardSegue *)segue wishId:(uint32_t)wishId
{
    [(SpamViewController *)[segue destinationViewController] setDetail:wishId];
}


#pragma mark - Open Link in App

- (void)openLinkInApp:(NSURL *)url
{
    
//    NSLog(@"scheme: %@\nhost: %@\nbaseUrl: %@\npath: %@\nfragment: %@\nrelativePath: %@\nrelativeString: %@", url.scheme, url.host, url.baseURL, url.path, url.fragment, url.relativePath, url.relativeString);
    
    if ( [url.scheme isEqualToString:@"startwish"] ) {
        NSString *itemId = [url.path stringByReplacingOccurrencesOfString:@"/" withString:@""];
        
        if ( [url.host isEqualToString:@"users"] ) {
            NSArray *a = [url.path componentsSeparatedByString:@"/"];
            
            if ( [a count] > 2 ) {
                itemId = a[1];
                
                if ( [a[2] isEqualToString:@"wishlists"] && [a count] > 3 ) {
                    [[Routing sharedInstance] goToWishlistById:[NSString stringToUint32_t:a[3]] byUser:[[ModelUser alloc] initWithDictionary:@{@"ID": itemId}]];
                }
                
                return;
            }
            
            [self showUserControllerWithUser:[[ModelUser alloc] initWithDictionary:@{@"ID": itemId}] fromMenu:NO];
        } else if ( [url.host isEqualToString:@"wish"] ) {
            [self showWishCtrlWithWishId:[NSString stringToUint32_t:itemId]];
        } if ( [url.host isEqualToString:@"categories"] ) {
            [[Routing sharedInstance] showExplorerControllerWithCatregoryId:[NSString stringToUint32_t:itemId]];
        }
    }
}


@end
