//
//  WishlistsAPI.m
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishlistsAPI.h"
#import "WishlistsHTTPClient.h"
#import "ModelWishlist.h"
#import "Me.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPI.h"


@implementation WishlistsAPI

static void(^failedBlock)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

+ (WishlistsAPI *) sharedInstance
{
    static WishlistsAPI *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[WishlistsAPI alloc] init];
    });
    
    return api;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        failedBlock = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:YES];
        };
    }
    return self;
}


#pragma mark -
#pragma mark Вишлист
- (void)getWishlistById:(uint32_t)itemId withCallBack:(prepareItem)update
{
    [[WishlistsHTTPClient sharedInstance] getWishlistById:itemId prepareItems:^(id requestObject) {
        JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(w.resource);
    } endItems:^(id requestObject) {
        update(@{});

    } failedBlock:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error, NSDictionary *requestParams) {
          failedBlock(task, error, requestParams);
      }];

}


#pragma mark -
#pragma mark Желания Вишлиста
- (void)getWishesWishlistById:(uint32_t)itemId page:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    if ( ![WishlistsHTTPClient sharedInstance].isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [[WishlistsHTTPClient sharedInstance] getWishesFromWishlistId:itemId page:(NSInteger)page prepareItems:^(id requestObject) {
        JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(w.resources);
    } endItems:^(id requestObject) {
        update(@[]);
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        failed( [API textError:error byOperation:operation] );
    }];
}

- (void)getShortWishesWishlistById:(uint32_t)itemId withCallBack:(prepareItems)update
{
    [[WishlistsHTTPClient sharedInstance] getShortWishesFromWishlistId:itemId prepareItems:^(id requestObject) {
        JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(w.resources);
    } endItems:^(id requestObject) {
        update(@[]);
        
    } failedBlock:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error, NSDictionary *requestParams) {
        failedBlock(task, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Список Вишлистов
- (void)getItemsFromPage:(NSUInteger)page byUserId:(uint32_t)userId withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    if ( ![WishlistsHTTPClient sharedInstance].isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [[WishlistsHTTPClient sharedInstance] getItemsFromPage:page byUserId:userId search:@{} prepareItems:^(id requestObject) {
        JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(w.resources);
        
    } endItems:^(id requestObject) {
        update(@[]);
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        failed([API textError:error byOperation:operation]);
    }];

}

- (void)getMyAllWishlistsWithCallBack:(prepareItems)update
{
    [[WishlistsHTTPClient sharedInstance] getItemsWithOffset:0 limit:[Me myWishlistsCount] byUserId:[Me myProfileId] search:nil prepareItems:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        update(jsonApi.resources);
    } endItems:^(id requestObject) {
        update(@[]);
        
    } failedBlock:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error, NSDictionary *requestParams) {
              failedBlock(task, error, requestParams);
          }];
}


#pragma mark -
#pragma mark Добавить Вишлист
- (void)createWishlist:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [[WishlistsHTTPClient sharedInstance] createWishlist:name desc:desc privateLevel:privateLevel prepareItem:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        update(jsonApi.resource);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        failedBlock(operation, error, requestParams);
    }];
}

- (void)editWishlistById:(uint32_t)wishlistId name:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [[WishlistsHTTPClient sharedInstance] editWishlistById:wishlistId name:name desc:desc privateLevel:privateLevel prepareItem:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        update(jsonApi.resource);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        failedBlock(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Удалить Вишлист
- (void)removeWishlistById:(uint32_t)wishlistId reason:(NSInteger)reason toWishlistId:(uint32_t)toWid withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [[WishlistsHTTPClient sharedInstance] removeWishlistById:wishlistId reason:reason toWishlistId:toWid withCallBack:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        update(jsonApi.resource);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        failedBlock(operation, error, requestParams);
    }];
}

@end
