//
//  WishlistsAPI.h
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "API.h"


@interface WishlistsAPI : API

+ (WishlistsAPI *) sharedInstance;

// Отдельный Вишлист
- (void)getWishlistById:(uint32_t)itemId withCallBack:(prepareItem)update;

// Желания Вишлиста
- (void)getWishesWishlistById:(uint32_t)itemId page:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)getShortWishesWishlistById:(uint32_t)itemId withCallBack:(prepareItems)update;

// Список Вишлистов
- (void)getItemsFromPage:(NSUInteger)page byUserId:(uint32_t)userId withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)getMyAllWishlistsWithCallBack:(prepareItems)update;

// Добавить Вишлист
- (void)createWishlist:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;
- (void)editWishlistById:(uint32_t)wishlistId name:(NSString *)name desc:(NSString *)desc privateLevel:(NSUInteger)privateLevel withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;

// Удалить Вишлист
- (void)removeWishlistById:(uint32_t)wishlistId reason:(NSInteger)reason toWishlistId:(uint32_t)toWid withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;
@end
