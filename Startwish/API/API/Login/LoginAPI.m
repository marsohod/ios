 //
//  LoginAPI.m
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "LoginAPI.h"
#import "LoginHTTPClient.h"
#import "Me.h"
#import "VKHandler.h"
#import "PushNotificationHandler.h"
#import "JSONAPI.h"
#import "Token.h"


@interface LoginAPI ()
{
    LoginHTTPClient *httpClient;
}
@end

@implementation LoginAPI

static void(^faildBlock)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);
static void(^failedBlockText)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

// Создаём синглтон
+ (LoginAPI *) sharedInstance
{
    static LoginAPI *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[LoginAPI alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:api selector:@selector(relogin:) name:@"STNeedRefreshToken" object:nil];
    });
    
    return api;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:[LoginAPI sharedInstance]];
}

// Инициируем API и httpClient для осуществления запросов
- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        httpClient = [LoginHTTPClient sharedInstance];
        faildBlock = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:YES];
        };
        failedBlockText = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:NO];
        };
    }
    
    return self;
}

- (BOOL)isLogged
{
    return [TokenAccess isLogged];
}


- (void)login:(NSString *)email pass:(NSString *)passwd withCallBack:(updateDataWithResponse)update withCallBackError:(errorResponse)failed;
{
    [httpClient login:email pass:passwd success:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        [TokenAccess setTokenFull:jsonApi.resource];
        
        update(jsonApi.resource);

    } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
        failedBlockText(operation, error, requestParams);
        failed([API textError:error byOperation:operation]);
    }];
}

- (void)relogin:(NSNotification *)notif
{
    [httpClient reloginWithToken:[TokenAccess getTokenRefresh] success:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        [TokenAccess setTokenFull:jsonApi.resource];
    } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedAuthUser" object:nil];
    }];
}

- (void)enterVK:(NSString *)token user:(NSDictionary *)user withCallBack:(updateDataWithResponse)update withCallBackError:(errorResponse)failed
{
//    [httpClient socialCheck:@"3" email:user[@"email"] userId:user[@"identifier"] token:token success:^(id requestCheck) {

        [httpClient loginVK: user[@"login"]
                     userId: user[@"identifier"]
                      token: token
                    success: ^(id requestObject) {
                        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];

                        [TokenAccess setTokenFull:jsonApi.resource];
                        
                        update(requestObject);
                    }
                     failed: ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
                         failedBlockText(operation, error, requestParams);
                         
                         NSArray *errorsArray = [API responseBodyByError:error];
                         
                         // if VK sent redirect_uri
                         if ( [errorsArray count] > 0 && [errorsArray[0] rangeOfString:@"http"].location != NSNotFound ) {
                             failed(errorsArray[0]);
                             return;
                         }
                         
                         if ( !user[@"login"] ) {
                             failed([API textError:error byOperation:operation]);
                             return;
                         }
                         
                         [httpClient registerVK:token user:user success:^(id requestObject) {
                             [httpClient loginVK: user[@"login"]
                                          userId: user[@"identifier"]
                                           token: token
                                         success: ^(id requestObject) {
                                             JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
                                             
                                             [TokenAccess setTokenFull:jsonApi.resource];
                                             
                                             update(requestObject);
                                         }
                                          failed: ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
                                              failed([API textError:error byOperation:operation]);
                                              failedBlockText(operation, error, requestParams);
                                          }];
                             
                         } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                             failed([API textError:error byOperation:operation]);
                             failedBlockText(operation, error, requestParams);
                         }];

                     }];
        
 /*   } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            }];
    */
}

- (void)logout
{
    [self logoutWithCallBack:nil failedCallBack:nil];
}

- (void)logoutWithCallBack:(emptyCallBack)update
{
    [self logoutWithCallBack:update failedCallBack:nil];
}

- (void)logoutWithCallBack:(emptyCallBack)update failedCallBack:(errorResponse)failed
{
    [self cleanProfile];
    [[VKHandler sharedInstance] logout];
    
    if ( update ) {
        update();
    }
}



// Говорим http клиенту, чтобы восстановил пароль и выполнил метод update после этого.
- (void)remindPasswordToEmail:(NSString *)email withCallBack:(updateDataWithResponse)update withError:(errorResponse)callBackError
{
    [httpClient reparePasswordToEmail:email success:^(id requestObject) {
        update(requestObject);
    } error:^(id requestObject) {

    } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
        failedBlockText(operation, error, requestParams);
        callBackError( [API textError:error byOperation:operation] );
    }];
}


// Регистрируем нового пользователя
- (void)registerEmail:(NSString *)email withPassword:(NSString *)password withCallBack:(prepareItem)update withFailedCallBack:(errorResponse)failedCallBack
{
    NSArray *name = [email componentsSeparatedByString:@"@"];
    
    [httpClient registerEmail: email
                 withFullName: [name firstObject]
                 withPasswods: password
                      success: ^(id requestObject) {
                          [self login:email pass:password withCallBack:update withCallBackError:failedCallBack];
                      }
                        error: nil
                       failed: ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
                           failedBlockText(operation, error, requestParams);
                           failedCallBack([API textError:error byOperation:operation]);
                       }];
}

- (void)cleanProfile
{
    [PushNotificationHandler cleanTokenWithCallBack:^{
        [Me cleanProfile];
        [TokenAccess clean];
    }];

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil];
}
@end
