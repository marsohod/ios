//
//  LoginAPI.h
//  Startwish
//
//  Created by marsohod on 26/11/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "API.h"


@interface LoginAPI : API

typedef void(^updateDataWithResponse)(NSDictionary *response);
typedef void(^errorResponse)(NSString *error);

+ (LoginAPI *) sharedInstance;


#pragma mark - Login

- (BOOL)isLogged;
- (void)login:(NSString *)email pass:(NSString *)passwd withCallBack:(updateDataWithResponse)update withCallBackError:(errorResponse)failed;
- (void)enterVK:(NSString *)token user:(NSDictionary *)user withCallBack:(updateDataWithResponse)update withCallBackError:(errorResponse)failed;

#pragma mark - Logout

- (void)logout;
- (void)logoutWithCallBack:(emptyCallBack)update;
- (void)logoutWithCallBack:(emptyCallBack)update failedCallBack:(errorResponse)failed;
- (void)cleanProfile;

#pragma mark - Password

/*! Send link for repair password to email */
- (void)remindPasswordToEmail:(NSString *)email withCallBack:(updateDataWithResponse)update withError:(errorResponse)callBackError;

#pragma mark - Email

/*! Register new user by email and password */
- (void)registerEmail:(NSString *)email withPassword:(NSString *)password withCallBack:(prepareItem)update withFailedCallBack:(errorResponse)failedCallBack;


@end
