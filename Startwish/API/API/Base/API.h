//
//  API.h
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.


#import <Foundation/Foundation.h>


@interface API : NSObject

typedef void(^prepareItem)(id item);
typedef void(^prepareItems)(NSArray *items);
typedef void(^emptyCallBack)();

+ (void)failedOperation: (NSURLSessionDataTask *)operation withError: (NSError *)error requestParams:(NSDictionary *)requestParams fromSender: (id)sender needReLoggin:(BOOL)isNeedReloggin;
+ (NSString *)textError:(NSError *)error byOperation:(NSURLSessionDataTask *)operation;

/*!
 By default reachable is NO. And first request to API does not execute. What wrong.
 So for check internet connection in requests you must use HttClient.isReacheble or implement own method for custom API
 */
+ (BOOL)isInternetConnectionAvailable;
+ (void)notifyAboutNoInternetConnection;
+ (NSArray *)responseBodyByError:(NSError *)error;
@end
