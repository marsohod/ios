//
//  API.m
//  Startwish
//
//  Created by marsohod on 29/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "API.h"
#import "Routing.h"
#import "AFNetworking.h"
#import "ActionTexts.h"
#import "ExtendNSLog.h"
#import "Token.h"
#import "ModelError.h"
#import "ModelUser.h"
#import "ModelContact.h"
#import "ModelCounters.h"
#import "ModelMutuality.h"
#import "ModelSocial.h"
#import "ModelCity.h"
#import "ModelCountry.h"
#import "ModelDistrict.h"
#import "ModelRegion.h"
#import "ModelWishlist.h"
#import "ModelSettings.h"
#import "ModelWishCategory.h"
#import "ModelWish.h"
#import "ModelComment.h"
#import "ModelSavedImage.h"
#import "ModelNotification.h"
#import "ModelFeedback.h"
#import "JSONAPIResourceDescriptor.h"
#import "JSONAPI.h"


@implementation API

- (instancetype)init {
    if( self = [super init] ) {
        [JSONAPIResourceDescriptor addResource:[TokenAccess class]];
        [JSONAPIResourceDescriptor addResource:[ModelError class]];
        [JSONAPIResourceDescriptor addResource:[ModelUser class]];
        [JSONAPIResourceDescriptor addResource:[ModelContact class]];
        [JSONAPIResourceDescriptor addResource:[ModelCounters class]];
        [JSONAPIResourceDescriptor addResource:[ModelMutuality class]];
        [JSONAPIResourceDescriptor addResource:[ModelSocial class]];
        [JSONAPIResourceDescriptor addResource:[ModelCity class]];
        [JSONAPIResourceDescriptor addResource:[ModelCountry class]];
        [JSONAPIResourceDescriptor addResource:[ModelDistrict class]];
        [JSONAPIResourceDescriptor addResource:[ModelRegion class]];
        [JSONAPIResourceDescriptor addResource:[ModelWishlist class]];
        [JSONAPIResourceDescriptor addResource:[ModelSettings class]];
        [JSONAPIResourceDescriptor addResource:[ModelSavedImage class]];
        [JSONAPIResourceDescriptor addResource:[ModelFeedback class]];
        
        [JSONAPIResourceDescriptor addResource:[ModelNotification class]];
        [JSONAPIResourceDescriptor addResource:[ModelWish class]];
        [JSONAPIResourceDescriptor addResource:[ModelWishCategory class]];
        [JSONAPIResourceDescriptor addResource:[ModelComment class]];
    }
    
    return self;
}

+ (void)failedOperation: (NSURLSessionDataTask *)operation withError: (NSError *)error requestParams:(NSDictionary *)requestParams fromSender: (id)sender needReLoggin:(BOOL)isNeedReloggin
{
    NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
    NSArray *responseBody       = [self responseBodyByError:error];
    
    if ( error.code != NSURLErrorCancelled ) {
        NSLog(@"#!\r\r url: %@ \r\r параметры запроса: %@ \r\r заголовки ответа: %@ \r\r ответ: %@ \r\r", [[operation originalRequest].URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding], requestParams , [response allHeaderFields], [responseBody componentsJoinedByString:@" | "]);

        if ( ![self isInternetConnectionAvailable] ) {
            [self notifyAboutNoInternetConnection];
            return;
        }

        if ( isNeedReloggin && response.statusCode == 401 ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNeedRefreshToken" object:nil];
            return;
        }
        
        if ( response.statusCode == 411 ) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"3", @"text": [[ActionTexts sharedInstance] failedText:@"UploadLargeImage"]}];
        }
    }
}

+ (NSArray *)responseBodyByError:(NSError *)error
{
    NSData *errorData   = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *dic   = errorData != nil ? [NSJSONSerialization JSONObjectWithData: errorData options:NSJSONReadingAllowFragments error:nil] : nil;
    JSONAPI *jsonApi;
    
    
    if( errorData == nil || dic == nil ) {
        NSMutableArray *r = [[NSMutableArray alloc] init];
        
        [error.userInfo enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [r addObject:[NSString stringWithFormat:@"%@: %@", key, obj]];
        }];
        
        return r;
    }
    
    jsonApi     = [JSONAPI jsonAPIWithDictionary:dic];
    
    return jsonApi.resource != nil ? [jsonApi.resource getErrorsArray] : @[];
}

+ (NSString *)textError:(NSError *)error byOperation:(NSURLSessionDataTask *)operation
{
    NSData *errorData               = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
    NSDictionary *serializedData    = errorData ? [NSJSONSerialization JSONObjectWithData: errorData options:NSJSONReadingAllowFragments error:nil] : @{};
    NSString *errorText             = [error isKindOfClass:[NSString class]] ? error : (serializedData[@"error"] != nil ? serializedData[@"error"] : [error localizedDescription]);
    JSONAPI *jsonApi                = [JSONAPI jsonAPIWithDictionary:serializedData];
    
    if ( jsonApi.resource ) {
        errorText = [jsonApi.resource getError];
    }
    
    if ( ![self isInternetConnectionAvailable] ) {
        return [[ActionTexts sharedInstance] failedText:@"NoInternetConnection"];
    }
    
    return errorText;
}

+ (BOOL)isInternetConnectionAvailable
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+ (void)notifyAboutNoInternetConnection
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNotification" object:nil userInfo:@{@"type":@"3", @"text": [[ActionTexts sharedInstance] failedText:@"NoInternetConnection"]}];
}
@end
