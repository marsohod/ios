//
//  DBAPI.m
//  Startwish
//
//  Created by Pavel Makukha on 26/03/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "DBAPI.h"
#import "FMDatabase.h"
#import "City.h"


@interface DBAPI()
@property (nonatomic, strong) FMDatabase *database;
@end


@implementation DBAPI

+ (DBAPI *)sharedInstance
{
    static DBAPI *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[DBAPI alloc] init];
    });
    
    return api;
}

- (instancetype)init
{
    self = [super init];
    
    NSString *const kDataBaseFileName = @"startwish.sqlite";
    
    // Create object fileManager
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    //Get Path array to our app
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    //path our app
    NSString *docDir = [paths objectAtIndex:0];
    
    //set path to database file
    NSString *dataBasePath = [docDir stringByAppendingPathComponent:kDataBaseFileName];
    
    //if database file does not exist we create new database file
    if(![fileManager fileExistsAtPath:dataBasePath]) {
        //[NSBundle mainBundle] is path to "my_news" folder. resourcePath get path to Resource folder.
        NSString *defaultDataBaseFilePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:kDataBaseFileName];
        
        //copy file to need path
        [fileManager copyItemAtPath:defaultDataBaseFilePath toPath:dataBasePath error:nil];
    }
    
    FMDatabase *database = [FMDatabase databaseWithPath:dataBasePath];
    
    if( ![database open] ) {
        return nil;
    }
    
    self.database = database;
    
    return self;

}

#pragma mark -
#pragma mark Has Cached
- (BOOL)hasCachedCities
{
    return [self hasCachedFromTable:@"cities"];
}

- (BOOL)hasCachedCategories
{
    return [self hasCachedFromTable:@"categories"];
}

- (BOOL)hasCachedFromTable:(NSString *)tableName
{
    NSInteger rowCount = 0;
    FMResultSet *resultSet = [self.database executeQuery:[NSString stringWithFormat:@"SELECT COUNT(*) FROM %@", tableName]];
    
    if( [resultSet next] ) {
        rowCount = [resultSet intForColumnIndex:0];
    }
    
    return rowCount != 0;
}


#pragma mark -
#pragma mark Cities
- (NSArray *)cities
{
    return [self citiesSearch:@""];
}

- (NSArray *)citiesSearch:(NSString *)search
{
    FMDatabase *database = self.database;
    
    FMResultSet * resultSet = [database executeQuery:[NSString stringWithFormat:@"SELECT * FROM cities where name='%@' ORDER BY name", search]];
    NSMutableArray *result = [NSMutableArray array];
    
    while( [resultSet next] ) {
        NSMutableDictionary *item = [[NSMutableDictionary alloc] init];
        
        [item setObject:[NSNumber numberWithInt:[resultSet intForColumn:@"id"]] forKey:@"id"];
        [item setObject:[resultSet stringForColumn:@"name"] forKey:@"name"];
        [item setObject:@{@"name":[resultSet stringForColumn:@"regionName"]} forKey:@"region"];
        [item setObject:[NSNumber numberWithBool:[resultSet boolForColumn:@"isMain"]] forKey:@"isMain"];
        
        [result addObject:item];
    }
    
    return result;
}

- (void)addCities:(NSArray *)cities
{
//    NSMutableArray *items;
    
    for( City *city in cities ) {
        NSMutableArray *item = [NSMutableArray arrayWithCapacity:5];
        
        [item addObject:[NSNumber numberWithLongLong:[city getId]]];
        [item addObject:[city getName]];
        [item addObject:[NSNumber numberWithLongLong:[city getCountryId]]];
        [item addObject:[NSNumber numberWithLongLong:[city getRegionId]]];
        [item addObject:[NSNumber numberWithBool:[city isMain]]];
        [item addObject:[city getRegionName]];
        
    
        BOOL updateRequest = [self.database executeUpdate:@"INSERT OR REPLACE INTO cities VALUES (?, ?, ?, ?, ?, ?)" withArgumentsInArray:item];

        if( !updateRequest ) {

        }
    }
//
//    if(successBlock) {
//        NSArray *cachedFeed = [self fetchCachedRssFeed];
//        successBlock(cachedFeed);
//    }

}



@end
