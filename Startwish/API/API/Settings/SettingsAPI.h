//
//  SettingsAPI.h
//  Startwish
//
//  Created by marsohod on 08/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "API.h"


@interface SettingsAPI : API

//typedef void(^updateDataWithResponse)(NSDictionary *response);

+ (SettingsAPI *) sharedInstance;

/*! Search cities by string */
- (void)citiesByName:(NSString *)name callBack:(prepareItems)update failedBack:(prepareItem)failedBlock;

/*! Search cities by default geo position */
- (void)cityByLatitude:(double)lat longitude:(double)lon callBack:(prepareItem)update failedBack:(prepareItem)failedBlock;


#pragma mark - Жалобы Spam
/*! Get spam reasons */
- (void)spamReasonItems:(prepareItems)update failedBack:(prepareItem)failedBlock;

/*! Post spam reason for wish */
- (void)spamWishId:(uint32_t)wishId reason:(NSInteger)reason callBack:(prepareItem)update failedBlock:(prepareItem)failed;

@end
