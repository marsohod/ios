//
//  SettingsAPI.m
//  Startwish
//
//  Created by marsohod on 08/01/15.
//  Copyright (c) 2015 marsohod. All rights reserved.
//


#import "SettingsAPI.h"
#import "SettingsHTTPClient.h"
#import "ModelCity.h"
#import "JSONAPI.h"


@interface SettingsAPI ()
{
    SettingsHTTPClient *httpClient;
}
@end

@implementation SettingsAPI

static void(^faildBlock)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

+ (SettingsAPI *)sharedInstance
{
    static SettingsAPI *api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[SettingsAPI alloc] init];
    });
    
    return api;
}

// Инициируем API и httpClient для осуществления запросов
- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        httpClient = [SettingsHTTPClient sharedInstance];
        faildBlock = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:YES];
        };
    }
    
    return self;
}


- (void)citiesByName:(NSString *)name callBack:(prepareItems)update failedBack:(prepareItem)failedBlock
{
    [httpClient cities:@{@"name": (name != nil ? name : @"")}  success:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        update(jsonApi.resources);
    } error:^(id requestObject) {

    } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock( [API textError:error byOperation:operation] );
        faildBlock(operation, error, requestParams);
    }];
}

- (void)cityByLatitude:(double)lat longitude:(double)lon callBack:(prepareItem)update failedBack:(prepareItem)failedBlock;
{
    [httpClient cityByLatitude:lat longitude:lon success:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        if ( [jsonApi.resources count] > 0 && [jsonApi.resources[0] isKindOfClass:[ModelCity class]] ) {
            update(jsonApi.resources[0]);
            return;
        }
        
        update(nil);
    } error:^(id requestObject) {

    } failed:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock( [API textError:error byOperation:operation] );
        faildBlock(operation, error, requestParams);
    }];
}

- (void)spamReasonItems:(prepareItems)update failedBack:(prepareItem)failedBlock
{
    [httpClient spamReasonItems:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(jsonApi.resources);
    } failedBack:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock([API textError:error byOperation:operation]);
    }];
}

- (void)spamWishId:(uint32_t)wishId reason:(NSInteger)reason callBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [httpClient spamWishId:wishId reason:reason callBack:^(id requestObject) {
        update(requestObject);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        faildBlock(operation, error, requestParams);
    }];
}

@end
