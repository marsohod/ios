//
#pragma mark -  WishesAPI.h
#pragma mark -  Startwish
//
#pragma mark -  Created by marsohod on 25/09/14.
#pragma mark -  Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "API.h"
#import "ModelComment.h"


@interface WishesAPI : API

+ (WishesAPI *)sharedInstance;


#pragma mark - Желания

- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular afterUpdate:(NSString *)dateString;
- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular beforeUpdate:(NSString *)dateString;
- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular withPage:(NSInteger)page;

// TODO:
// refactor isPopular
- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular withPage:(NSInteger)page;
- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular afterUpdate:(NSString *)dateString;
- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular beforeUpdate:(NSString *)dateString;


#pragma mark - Желаниe

- (void)getWishById:(uint32_t)wishId withCallBack:(prepareItem)update;

#pragma mark - Желания пользователя

- (void)getWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;


#pragma mark - Исполненные желания пользователя
- (void)getExecutedWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)implementWish:(uint32_t)wishId cover:(UIImage *)image desc:(NSString *)desc withCallBack:(prepareItem)update failedBlock:(emptyCallBack)failed;


#pragma mark - Общие желания активного пользователя с просматриваемым пользователем

- (void)getMutualWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;


#pragma mark - Поиск желаний

- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular withPage:(NSInteger)page update:(prepareItems)update failed:(emptyCallBack)fb;
/*
- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular beforeDate:(NSString *)dateString update:(prepareItems)update failed:(emptyCallBack)fb;
- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular afterDate:(NSString *)dateString update:(prepareItems)update failed:(emptyCallBack)fb;
*/

#pragma mark - Категории

- (void)getCategories:(prepareItems)update;
+ (void)setCategoriesToCache:(NSArray *)items;
+ (NSArray *)getCategoriesFromCache;

#pragma mark - Комментарии желания
- (void)getCommentsWishById:(uint32_t)wishId offset:(NSInteger)offset update:(prepareItems)prepareComments failedBlock:(prepareItem)failed;
- (void)postComment:(NSString *)text toWishId:(uint32_t)wishId replyToComments:(NSArray *)commentsIds withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;
- (void)deleteComment:(uint32_t)commentId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;


#pragma mark - Ревиш

- (void)rewish:(uint32_t)wishId toWishlistId:(uint32_t)wishlistId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;


#pragma mark - Удаление

- (void)deleteWish:(uint32_t)wishId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;


#pragma mark - Добавление

- (void)createWish:(NSString *)name desc:(NSString *)desc wishlist:(uint32_t)wishlistId category:(uint32_t)categoryId cover:(UIImage *)image link:(NSString *)urlString withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;

@end
