//
//  WishesAPI.m
//  Startwish
//
//  Created by marsohod on 25/09/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "WishesAPI.h"
#import "WishesHTTPClient.h"
#import "SettingsHTTPClient.h"
#import "UserHTTPClient.h"
#import "ModelWishCategory.h"
#import "ModelWish.h"
#import "ModelSavedImage.h"
#import "ManagerWishes.h"
#import "JSONAPI.h"


@interface WishesAPI()
{
    WishesHTTPClient *httpClient;
    UserHTTPClient *httpClientUsers;
    SettingsHTTPClient *httpClientSettings;
}
@end

@implementation WishesAPI

static void(^failedBlock)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);
static void(^failedBlockText)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

+ (WishesAPI *)sharedInstance
{
    static id api = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[self alloc] init];
    });
    
    return api;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        httpClient          = [WishesHTTPClient sharedInstance];
        httpClientSettings  = [SettingsHTTPClient sharedInstance];
        httpClientUsers     = [UserHTTPClient sharedInstance];
        
        failedBlock = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:YES];
        };
        
        failedBlockText = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:NO];
        };
    }
    return self;
}

- (NSArray *)getWishes:(id)requestObject
{
    NSMutableArray *wishes = [[NSMutableArray alloc] init];
    
    if( [requestObject isKindOfClass:[NSArray class]]) {
    
        [requestObject enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if( obj != false ) {
//                if ( idx != [requestObject count] - 1 ) {
//                [wishes addObject:[[Wish alloc] initWithObject:obj]];
//                }
            }
        }];
        
    }
    /*else if( [requestObject isKindOfClass:[NSDictionary class]]) {
        
        [requestObject enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if( obj != false ) {
                if ( ![key isEqualToString:@"dbgcache"] ) {
                    [wishes addObject:[[Wish alloc] initWithObject:obj]];
                }
            }
        }];
        
    }*/
    
    return wishes;
}


#pragma mark -
#pragma mark Желание
- (void)getWishById:(uint32_t)wishId withCallBack:(prepareItem)update
{
    [httpClient getWishById:wishId prepareWish:^(id requestObject) {
        JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
        update( w.resource );
    } failedBlock:failedBlock];
}


#pragma mark -
#pragma mark Желания
- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular afterUpdate:(NSString *)dateString
{
    [self getWishesWithCallBack:update withFailedCallBack:^(id item) {} popularStatus:isPopular afterUpdate:dateString];
}

- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular beforeUpdate:(NSString *)dateString
{
    [self getWishesWithCallBack:update withFailedCallBack:^(id item) {} popularStatus:isPopular beforeUpdate:dateString];
}


- (void)getWishesWithCallBack:(prepareItems)update popularStatus:(BOOL)isPopular withPage:(NSInteger)page
{
    [self getWishesWithCallBack:update withFailedCallBack:^(id item) {} popularStatus:isPopular withPage:page];
}

- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular withPage:(NSInteger)page
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getWishesWithPage:page popularStatus:isPopular prepareWishes:^(id requestObject) {
        if ( update ) {
            update( [self getWishes:requestObject]);
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular afterUpdate:(NSString *)dateString
{
    [httpClient getWishesAfterUpdate:dateString popularStatus:isPopular prepareWishes:^(id requestObject) {
        if ( update ) {
            update( [self getWishes:requestObject]);
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)getWishesWithCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed popularStatus:(BOOL)isPopular beforeUpdate:(NSString *)dateString
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getWishesBeforeUpdate:dateString popularStatus:isPopular prepareWishes:^(id requestObject) {
        if ( update ) {
            update( [self getWishes:requestObject]);
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}


#pragma mark -
#pragma mark Желания пользователя
- (void)getWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    [self getWishesByUserId:userId
                 withParams:@{@"search": @{@"status": @"1"},
                              @"order" : @{@"id"    : @"desc"}}
                   withPage:page
               withCallBack:update
         withFailedCallBack:failed];
}


#pragma mark -
#pragma mark Исполненные желания
- (void)getExecutedWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
{
    [self getWishesByUserId:userId
                 withParams:@{@"search": @{@"status": @"2"},
                              @"order" : @{@"executed"    : @"desc"}}
                   withPage:page
               withCallBack:update
         withFailedCallBack:failed];
}

- (void)getWishesByUserId:(uint32_t)userId withParams:(NSDictionary *)params withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getWishesUserById: userId
                           params: params
                         withPage: page
                    prepareWishes: ^(id requestObject) {
                          if ( update ) {
                              JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
                              update( w.resources );
                          }
                      } failedBlock: ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                          failedBlock(operation, error, requestParams);
                          if ( failed ) {
                              failed([API textError:error byOperation:operation]);
                          }
                      }];
}

- (void)implementWish:(uint32_t)wishId cover:(UIImage *)image desc:(NSString *)desc withCallBack:(prepareItem)update failedBlock:(emptyCallBack)failed
{
    if ( image ) {
        [httpClientSettings saveImage:image success:^(id requestObject) {
            JSONAPI *json           = [JSONAPI jsonAPIWithDictionary:requestObject];
            ModelSavedImage *img    = json.resource;
            
            [httpClient executeWishId:wishId
                               params:@{@"exec_img_src"   : [img getOriginal],
                                        @"exec_img_height": [NSNumber numberWithFloat:[img getOriginalHeight]],
                                        @"exec_img_width" : [NSNumber numberWithFloat:[img getOriginalWidth]]}
                         successBlock:^(id requestObject) {
                                     
                JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
                update( w.resources );
            } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                failedBlock(operation, error, requestParams);
                failed([API textError:error byOperation:operation]);
            }];
        } failedBack:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            failedBlock(operation, error, requestParams);
            failed([API textError:error byOperation:operation]);
        }];
    } else {
        [httpClient executeWishId:wishId params:nil successBlock:^(id requestObject) {
            JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
            update( w.resources );
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            failedBlock(operation, error, requestParams);
            failed([API textError:error byOperation:operation]);
        }];
    }
}


#pragma mark -
#pragma mark Общие желания

- (void)getMutualWishesByUserId:(uint32_t)userId withPage:(NSInteger)page withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getMutualWishesUserById:userId withPage:page prepareWishes:^(id requestObject) {
        if ( update ) {
            JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
            update( w.resources );
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
        
        failedBlock(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Поиск желаний
- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular withPage:(NSInteger)page update:(prepareItems)update failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    NSMutableDictionary *searchDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"name": name, @"status": @"1"}];
    
    NSMutableDictionary *params;
    
    if ( categoryId != 0 ) {
        [searchDic setObject:[NSNumber numberWithInteger:categoryId] forKey:@"category_id"];
    }
    
    if ( categoryId == 0 || name.length > 0 ) {
        [searchDic setObject:@"null" forKey:@"master_id"];
    }
    
    params = [[NSMutableDictionary alloc] initWithDictionary:@{@"search" : searchDic,
                                                               @"order"  : @{@"id" : @"desc"}}];
    
    if ( isPopular ) {
        [httpClient searchWishesPopular:params
                        withPage:page prepareWishes:^(id requestObject) {
                            if ( update ) {
                                JSONAPI *wishes = [JSONAPI jsonAPIWithDictionary:requestObject];
                                NSArray *listUsersId = [ManagerWishes getOwnersId:wishes.resources];
                                
                                [httpClientUsers getUsersWithOffset:0 limit:[listUsersId count] search:@{@"id" : listUsersId} prepareUser:^(id requestObjectUsers) {
                                    JSONAPI *u = [JSONAPI jsonAPIWithDictionary:requestObjectUsers];
                                    
                                    [ManagerWishes setOwners:u.resources toItems:wishes.resources];
                                    
                                    update( wishes.resources);
                                } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                                    if ( fb != nil ) {
                                        fb();
                                    }
                                    failedBlock(operation, error, requestParams);
                                }];
                            }
                        } endWishes:^(id requestObject) {
                            
                        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                            if ( fb != nil ) {
                                fb();
                            }
                            failedBlock(operation, error, requestParams);
                        }];
    } else {
        [httpClient searchWishes:params
                        withPage:page prepareWishes:^(id requestObject) {
                            if ( update ) {
                                JSONAPI *wishes = [JSONAPI jsonAPIWithDictionary:requestObject];
                                NSArray *listUsersId = [ManagerWishes getOwnersId:wishes.resources];
                                
                                [httpClientUsers getUsersWithOffset:0 limit:[listUsersId count] search:@{@"id" : listUsersId} prepareUser:^(id requestObjectUsers) {
                                    JSONAPI *u = [JSONAPI jsonAPIWithDictionary:requestObjectUsers];
                                    
                                    [ManagerWishes setOwners:u.resources toItems:wishes.resources];
                                    
                                    update( wishes.resources);
                                } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                                    if ( fb != nil ) {
                                        fb();
                                    }
                                    failedBlock(operation, error, requestParams);
                                }];
                            }
                        } endWishes:^(id requestObject) {
            
                        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                            if ( fb != nil ) {
                                fb();
                            }
                            failedBlock(operation, error, requestParams);
                        }];
    }
}

/*
- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular beforeDate:(NSString *)dateString update:(prepareItems)update failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient searchWishes:@{@"search": @{@"name": name, @"category_id" : [NSNumber numberWithInteger:categoryId]}} afterDate:dateString prepareWishes:^(id requestObject) {
        if ( update ) {
            update( [self getWishes:requestObject]);
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil ) {
            fb();
        }
        failedBlock(operation, error, requestParams);
    }];
}

- (void)searchWishesByName:(NSString *)name categoryId:(NSInteger)categoryId popularStatus:(BOOL)isPopular afterDate:(NSString *)dateString update:(prepareItems)update failed:(emptyCallBack)fb
{
    [httpClient searchWishes:@{@"search": @{@"name": name, @"category_id" : [NSNumber numberWithInteger:categoryId]}} beforeDate:dateString prepareWishes:^(id requestObject) {
        if ( update ) {
            update( [self getWishes:requestObject]);
        }
    } endWishes:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil ) {
            fb();
        }
        failedBlock(operation, error, requestParams);
    }];
}
*/

#pragma mark - Категории
#define CATEGORIES_CACHE @"userCategories"

- (void)getCategories:(prepareItems)update
{
    [httpClient getCategories:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        update(jsonApi.resources);
    } failedBlock:failedBlock];
    
}

+ (void)setCategoriesToCache:(NSArray *)items
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:items.count];
    
    for (ModelWishCategory *i in items) {
        NSData *obj = [NSKeyedArchiver archivedDataWithRootObject:i];
        [archiveArray addObject:obj];
    }
    
    [userDefaults setObject:archiveArray forKey:CATEGORIES_CACHE];
    [userDefaults synchronize];
}

+ (NSArray *)getCategoriesFromCache
{    
    NSArray *archiveArray = [[NSUserDefaults standardUserDefaults] objectForKey:CATEGORIES_CACHE];
    NSMutableArray *items = [NSMutableArray arrayWithCapacity:archiveArray.count];
    
    for (NSData *i in archiveArray) {
        //Для предотвращения крэшей старой прилажки 1.0.2
        if ( [i isKindOfClass:[NSDictionary class]] ) { continue; }
        
        ModelWishCategory *c = [NSKeyedUnarchiver unarchiveObjectWithData:i];
        [items addObject:c];
    }
    
    return items;
}


#pragma mark -
#pragma mark Комментарии

// second method with getting count and comments
- (void)getCommentsWishById:(uint32_t)wishId offset:(NSInteger)offset update:(prepareItems)prepareComments failedBlock:(prepareItem)failed
{
    [httpClient getComments:@{@"search" : @{@"wish_id": [NSNumber numberWithUnsignedLong:wishId]}, @"order" : @{@"id": @"desc"}} offset:offset limit:0 prepareWish:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        if ( prepareComments ) {
            prepareComments([[jsonApi.resources reverseObjectEnumerator] allObjects]);
        }
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
        failedBlock(operation, error, requestParams);
    }];
}

- (void)postComment:(NSString *)text toWishId:(uint32_t)wishId replyToComments:(NSArray *)commentsIds withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{@"wish_id"  : [NSNumber numberWithUnsignedLong:wishId],
                                                                                    @"text"     : text}];
    
    if ( commentsIds && [commentsIds count] > 0 ) {
        [params setObject:[commentsIds componentsJoinedByString:@","] forKey:@"to"];
    }
    
    [httpClient putComment:params prepareItem:^(id requestObject) {
        JSONAPI *c = [JSONAPI jsonAPIWithDictionary:requestObject];
        update( c.resource );
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
        failedBlock(operation, error, requestParams);
    }];
}

- (void)deleteComment:(uint32_t)commentId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [httpClient deleteComment:commentId successBlock:^(id requestObject) {
        update(requestObject);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        failedBlockText(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Ревиш
- (void)rewish:(uint32_t)wishId toWishlistId:(uint32_t)wishlistId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    [httpClient putRewish:@{@"wish_id"      : [NSNumber numberWithUnsignedLong:wishId],
                            @"wishlist_id"  : [NSNumber numberWithUnsignedLong:wishlistId]}
             successBlock:^(id requestObject) {
                 JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
                 update(w.resource);
                 
             } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                 failed([API textError:error byOperation:operation]);
                 failedBlockText(operation, error, requestParams);
             }];
}


#pragma mark -
#pragma mark Удаление желания
- (void)deleteWish:(uint32_t)wishId withCallBack:(prepareItem)update failedBlock:(prepareItem)failed;
{
    [httpClient deleteWish:wishId successBlock:^(id requestObject) {
        update(requestObject);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed([API textError:error byOperation:operation]);
        failedBlockText(operation, error, requestParams);
    }];
}




#pragma mark -
#pragma mark Добавление желания
-(void)createWish:(NSString *)name desc:(NSString *)desc wishlist:(uint32_t)wishlistId category:(uint32_t)categoryId cover:(UIImage *)image link:(NSString *)urlString withCallBack:(prepareItem)update failedBlock:(prepareItem)failed
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                    @"name"         : name,
                                                                                    @"description"  : desc,
                                                                                    @"wishlist_id"  : [NSNumber numberWithUnsignedLong:wishlistId],
                                                                                    @"category_id"  : [NSNumber numberWithUnsignedLong:categoryId],
                                                                                    @"url"          : urlString,
//                                                                                    @"debug"    : [self debugParams]
                                                                                    }];
    
    [httpClientSettings saveImage:image success:^(id requestObject) {
        JSONAPI *json           = [JSONAPI jsonAPIWithDictionary:requestObject];
        ModelSavedImage *img    = json.resource;
        
        [params setObject:[img getOriginal] forKey:@"img_src"];
        [params setObject:[NSNumber numberWithFloat:[img getOriginalHeight]] forKey:@"img_height"];
        [params setObject:[NSNumber numberWithFloat:[img getOriginalWidth]] forKey:@"img_width"];
        
        [httpClient createWishWithParams:params
                             prepareItem:^(id requestObject) {
                                 JSONAPI *w = [JSONAPI jsonAPIWithDictionary:requestObject];
                                 
                                 [ModelWish setDefaults:w.resource];
                                 
                                 update(w.resource);
                             }
                             failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                                 failed([API textError:error byOperation:operation]);
                                 failedBlockText(operation, error, requestParams);
                             }];
    } failedBack:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failedBlock(operation, error, requestParams);
        failed([API textError:error byOperation:operation]);
    }];
}

@end
