//
// UserAPI.h
// Startwish
//
// Created by marsohod on 10/10/14.
// Copyright (c) 2014 marsohod. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "API.h"
#import "ModelUser.h"


typedef void(^prepareManOld)(NSDictionary *man);
typedef void(^prepareMan)(ModelUser *man);

@interface UserAPI : API


+ (UserAPI *)sharedInstance;

#pragma mark - Получить данные пользователя

- (void)getUserById:(uint32_t)userId withCallback:(prepareMan)prepareMan;
- (void)getMe:(prepareMan)prepareMan failedBlock:(prepareItem)failed;
- (void)getUsers:(NSDictionary *)search withCallback:(prepareItems)preparePeople;

#pragma mark - Изменить данные пользователя

/*! Get logged user info from server */
- (void)updateMyProfileInfo;

/*! Save logged user profile info in app */
- (void)setUserProfile:(NSDictionary *)prof withCallback:(prepareMan)update failedBlock:(prepareItem)failed;

/*! Save logged user profile info in app */
// deprecated
- (void)setUserImage:(NSData *)image
              domain:(NSString *)domain
            fullname:(NSString *)name
              gender:(NSInteger)gender
                city:(NSString *)city
              cityId:(NSString *)cityId
            birthday:(NSString *)birth
        withCallback:(prepareMan)update
         failedBlock:(prepareItem)failed;

/*! Save user profile city info to server */
- (void)setUserLocationCity:(uint32_t)cityId
           withCallback:(prepareMan)update
            failedBlock:(prepareItem)failed;

/*! Save user profile settings private birthday, private profile, private contacts to server */
- (void)setUserSettingsShowBirthday:(BOOL)birthday showProfile:(NSInteger)profile showContacts:(NSInteger)contacts withCallback:(prepareMan)update failedBlock:(prepareItem)failed;


#pragma mark - Друзья

- (void)searchFriendsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)failedBlock;
- (void)getFriendsVK:(prepareItems)preparePeople failedBlock:(prepareItem)failed;
- (void)getFriendsFB:(prepareItems)preparePeople failedBlock:(prepareItem)failed;
- (void)searchFriendsContacts:(prepareItems)preparePeople failedBlock:(prepareItem)failed;


#pragma mark - Соц. Сети
/*!
 Setup network to logged account. For login by social without obstacle in future
 @network Vkontakte | Facebook
 @userId is id by social
 */
- (void)setVKToken:(NSString *)token success:(prepareItem)success failedBlock:(prepareItem)failed;
- (void)setFBToken:(NSString *)token success:(prepareItem)success failedBlock:(prepareItem)failed;
- (void)unsetNetworkById:(uint32_t)socialId success:(prepareItem)success failedBlock:(prepareItem)failed;

#pragma mark - Общие друзья

- (void)searchMutualFriendsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)failedBlock;


#pragma mark - Подписчики

- (void)searchFollowersFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)failedBlock;
- (void)subscribe:(BOOL)state toUserId:(uint32_t)userId withCallBack:(prepareItem)callBack failedBlock:(prepareItem)failedBlock;


#pragma mark - Подписки

- (void)searchFollowsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)failedBlock;


#pragma mark - Поиск людей

- (void)searchPeopleByName:(NSString *)name withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock;
- (void)searchActivePeopleByName:(NSString *)name withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock;
- (void)searchActivePeopleByCategories:(NSArray *)categories withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock;
- (void)searchActivePeople:(NSDictionary *)search withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock;


#pragma mark - События

- (void)eventsWithOffset:(NSInteger)offset success:(prepareItems)prepareItems failed:(prepareItem)failed;
- (void)eventsAfterUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)eventsBeforeUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)newEventsCount:(prepareItem)prepareItem failedBlock:(prepareItem)failedBlock;
- (void)redeemEvents;


#pragma mark - Лента пользователя

//- (void)feedsWithCallback:(prepareItems)prepareItems withFailedCallBack:(prepareItem)failed withPage:(NSInteger)page;
- (void)feedsAfterUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;
- (void)feedsBeforeUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed;

#pragma mark - Контакты

- (void)setContacts:(NSDictionary *)contacts goodBlock:(prepareItem)goodBlock failedBlock:(prepareItem)failed;


#pragma mark - Push

- (void)registerPushNotification:(NSString *)token success:(prepareItem)update failedBlock:(prepareItem)failedBlock;
- (void)removePushNotification:(NSString *)token callback:(emptyCallBack)callbackFn;

@end
