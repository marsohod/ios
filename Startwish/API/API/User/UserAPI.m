//
//  UserAPI.m
//  Startwish
//
//  Created by marsohod on 10/10/14.
//  Copyright (c) 2014 marsohod. All rights reserved.
//


#import "UserAPI.h"
#import "UserHTTPClient.h"
#import "WishesHTTPClient.h"
#import "SettingsHTTPClient.h"
#import "APAddressBook.h"
#import "APContact.h"
#import "Me.h"
#import "ModelUser.h"
#import "Token.h"
#import "ManagerNotification.h"
#import "ManagerWishes.h"
#import "ManagerComments.h"
#import "ModelNotification.h"
#import "ModelSavedImage.h"

#import "JSONAPI.h"


@interface UserAPI ()
{
    UserHTTPClient*     httpClient;
    WishesHTTPClient*   httpClientWishes;
    SettingsHTTPClient* httpClientSettings;
}
@end

@implementation UserAPI

static void(^faildBlock)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);
static void(^failedBlockText)(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams);

+ (UserAPI *)sharedInstance
{
    static id api = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        api = [[UserAPI alloc] init];
    });
    
    return api;
}

- (instancetype)init
{
    self = [super init];
    
    if( self ) {
        httpClient          = [UserHTTPClient sharedInstance];
        httpClientWishes    = [WishesHTTPClient sharedInstance];
        httpClientSettings  = [SettingsHTTPClient sharedInstance];
        
        faildBlock = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams){
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:YES];
        };
        failedBlockText = ^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            [API failedOperation:operation withError:error requestParams:requestParams fromSender:self needReLoggin:NO];
        };
    }
    
    return self;
}

- (void)parseEvents:(id)requestObject success:(prepareItems)prepareItems failed:(prepareItem)failed
{
    JSONAPI *events         = [JSONAPI jsonAPIWithDictionary:requestObject];
    NSArray *listWishesId   = [ManagerNotification getWishesId:events.resources];
    NSArray *listCommentsId = [ManagerNotification getCommentsId:events.resources];
    NSArray *listUsersId    = [ManagerNotification getUsersId:events.resources];
    
    if ( [events.resources count] == 0 ) {
        prepareItems( events.resources );
        return;
    }
    
    [httpClientWishes getComments:@{@"search": @{@"id": listCommentsId}, @"order": @{@"id": @"asc"}} offset:0 limit:[listCommentsId count] prepareWish:^(id requestObjectComments) {
        JSONAPI *comments       = [JSONAPI jsonAPIWithDictionary:requestObjectComments];
        NSMutableArray* wId     = [[NSMutableArray alloc] initWithArray:[ManagerComments getWishesId:comments.resources]];
        
        [wId addObjectsFromArray:listWishesId];
        
        [httpClientWishes getWishesWithOffset:0 limit:[wId count] search:@{@"id": wId} prepareWishes:^(id requestObjectWishes) {
            JSONAPI *wishes                 = [JSONAPI jsonAPIWithDictionary:requestObjectWishes];
            NSMutableArray *listOwnersId    = [[NSMutableArray alloc] initWithArray:[ManagerWishes getOwnersId:wishes.resources]];
            
            [listOwnersId addObjectsFromArray:listUsersId];
            
            [httpClient getUsersWithOffset:0 limit:[listOwnersId count] search:@{@"id": listOwnersId} full:YES prepareUser:^(id requestObjectOwners) {
                JSONAPI *owners         = [JSONAPI jsonAPIWithDictionary:requestObjectOwners];
                
                [ManagerComments setOwners:owners.resources forItems:comments.resources];
                [ManagerWishes setOwners:owners.resources toItems:wishes.resources];
                [ManagerNotification setComments:comments.resources forWishes:wishes.resources toItems:events.resources];
                
                //                    [ManagerWishes setComments:comments.resources toItems:wishes.resources];
                [ManagerNotification setObjects:wishes.resources toItems:events.resources];
                //                    [ManagerNotification setObjects:comments.resources toItems:events.resources];
                [ManagerNotification setObjects:owners.resources toItems:events.resources];
                
                prepareItems( events.resources );
                
            } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                faildBlock(operation, error, requestParams);
                if ( failed ) {
                    failed([API textError:error byOperation:operation]);
                }
            }];
            
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            faildBlock(operation, error, requestParams);
            if ( failed ) {
                failed([API textError:error byOperation:operation]);
            }
        }];
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)parseFeed:(id)requestObject success:(prepareItems)prepareItems failed:(prepareItem)failed
{
    JSONAPI *feeds          = [JSONAPI jsonAPIWithDictionary:requestObject];
    NSArray *listWishesId   = [ManagerNotification getWishesId:feeds.resources];

    if ( [feeds.resources count] == 0 ) {
        prepareItems( feeds.resources );
        return;
    }
    
    [ManagerNotification removeDublicates:feeds.resources];
    
    [httpClientWishes getWishesWithOffset:0 limit:[listWishesId count] search:@{@"id": listWishesId, @"status": @[@"1", @"2"]} prepareWishes:^(id requestObjectWishes) {
        JSONAPI *wishes         = [JSONAPI jsonAPIWithDictionary:requestObjectWishes];
        NSArray *listOwnersId   = [ManagerWishes getOwnersId:wishes.resources];
        
        [httpClient getUsersWithOffset:0 limit:[listOwnersId count] search:@{@"id": listOwnersId} prepareUser:^(id requestObjectOwners) {
            JSONAPI *owners         = [JSONAPI jsonAPIWithDictionary:requestObjectOwners];
            
            [ManagerWishes setOwners:owners.resources toItems:wishes.resources];
            [ManagerNotification setObjects:wishes.resources toItems:feeds.resources];
            
//            NSLog(@"feeds wishes: %@", [ManagerNotification getObjects:feeds.resources]);
//            prepareItems( [ManagerNotification getObjects:feeds.resources] );
            prepareItems( [ManagerNotification getObjects:feeds.resources] );
            
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            faildBlock(operation, error, requestParams);
            if ( failed ) {
                failed([API textError:error byOperation:operation]);
            }
        }];
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}


#pragma mark -
#pragma mark Изменить данные пользователя
//deprecated
- (void)setUserImage:(NSData *)image
              domain:(NSString *)domain
            fullname:(NSString *)name
              gender:(NSInteger)gender
                city:(NSString *)city
              cityId:(NSString *)cityId
            birthday:(NSString *)birth
        withCallback:(prepareMan)update
         failedBlock:(prepareItem)failed
{
    [httpClient setUserImage:image
                      domain:domain
                    fullname:name
                      gender:gender
                        city:city
                      cityId:cityId
                    birthday:birth
                   goodBlock:^(id requestObject) {
                       JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
                       update(jsonApi.resource);
                   } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                        failed( [API textError:error byOperation:operation] );
                        faildBlock(operation, error, requestParams);
                   }];
}

- (void)setUserProfile:(NSDictionary *)prof withCallback:(prepareMan)update failedBlock:(prepareItem)failed
{
    if ( prof[@"avatar"] && [prof[@"avatar"] isKindOfClass:[UIImage class]] ) {
        NSMutableDictionary *profile = [[NSMutableDictionary alloc] initWithDictionary:prof];
        
        [httpClientSettings saveImage:prof[@"avatar"]
                              success:^(id requestObject) {
                                  JSONAPI *json = [JSONAPI jsonAPIWithDictionary:requestObject];

                                  [profile setObject:[(ModelSavedImage *)json.resource getOriginal] forKey:@"avatar"];

                                  [self setUser:profile withCallback:update failedBlock:failed];
                              }
                           failedBack:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                               if ( failed ) {
                                   failed( [API textError:error byOperation:operation] );
                               }
                               faildBlock(operation, error, requestParams);
                           }];
        
    } else {
        [self setUser:prof withCallback:update failedBlock:failed];
    }
}

- (void)setUser:(NSDictionary *)prof withCallback:(prepareMan)update failedBlock:(prepareItem)failed
{
    [httpClient setUserProfile: prof
                     goodBlock:^(id requestObject) {
                         if ( update ) {
                             JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
                             update(jsonApi.resource);
                         }
                     } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                         if ( failed ) {
                             failed( [API textError:error byOperation:operation] );
                         }
                         faildBlock(operation, error, requestParams);
                     }];

}

- (void)setUserLocationCity:(uint32_t)cityId
           withCallback:(prepareMan)update
            failedBlock:(prepareItem)failed
{
    [httpClient setUserLocationCity:cityId
                          goodBlock:^(id requestObject) {
                              JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
                              update(jsonApi.resource);
                          }
                        failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                            failed( [API textError:error byOperation:operation] );
                            faildBlock(operation, error, requestParams);
                        }];
}

- (void)setUserSettingsShowBirthday:(BOOL)birthday showProfile:(NSInteger)profile showContacts:(NSInteger)contacts withCallback:(prepareMan)update failedBlock:(prepareItem)failed
{
    [httpClient setUserSettings:@{@"private_contacts"   : [NSNumber numberWithInteger:contacts],
                                  @"private_birthday"   : [NSNumber numberWithBool:birthday],
                                  @"private_profile"    : [NSNumber numberWithInteger:profile]}
                      goodBlock:^(id requestObject) {
                          update(requestObject);
                      }
                    failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                        failed( [API textError:error byOperation:operation] );
                        faildBlock( operation, error, requestParams );
                    }];
}


#pragma mark -
#pragma mark Пользователь
- (void)getUserById:(uint32_t)userId withCallback:(prepareMan)prepareMan
{
    [httpClient getUserById:userId prepareUser:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        prepareMan(jsonApi.resource);
    } failedBlock:faildBlock];
    
}

- (void)getUsers:(NSDictionary *)search withCallback:(prepareItems)preparePeople
{
    [httpClient getUsersWithOffset:0 limit:40 search:search full:YES prepareUser:^(id requestObjectOwners) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObjectOwners];
        preparePeople(jsonApi.resources);
    } failedBlock:faildBlock];
}

- (void)getMe:(prepareMan)prepareMan failedBlock:(prepareItem)failed
{
    [httpClient getMe:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        prepareMan(jsonApi.resource);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( failed ) {
            failed( [API textError:error byOperation:operation] );
        }
        faildBlock( operation, error, requestParams );
    }];
}

- (void)updateMyProfileInfo
{
    if ( [TokenAccess isLogged] ) {
        [self getMe:^(ModelUser *man) {
            [Me setProfileAfterLogin:man];
        } failedBlock:nil];
    }
}


#pragma mark -
#pragma mark Друзья
- (void)searchFriendsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getUserFriendsById:userId search:@{@"username": name} withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil ) {
            fb();
        }
        
        faildBlock(operation, error, requestParams);
    }];
}

- (void)getFriendsVK:(prepareItems)preparePeople failedBlock:(prepareItem)failed
{
    [httpClient getFriendsByVK:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        preparePeople(jsonApi.resources);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed( [API textError:error byOperation:operation] );
        faildBlock( operation, error, requestParams );
    }];
}

//deprecated
- (void)getFriendsFB:(prepareItems)preparePeople failedBlock:(prepareItem)failed
{
    [httpClient getFriendsByFB:^(id requestObject) {
        JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
        
        preparePeople(jsonApi.resources);
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        failed( [API textError:error byOperation:operation] );
        faildBlock( operation, error, requestParams );
    }];
}

- (void)searchFriendsContacts:(prepareItems)preparePeople failedBlock:(prepareItem)failed
{
    [self searchFriendsFromContacts:^(NSArray *items) {
        [httpClient getUsersWithOffset:0 limit:[items count] search:@{@"login" : items} full:YES prepareUser:^(id requestObject) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            failed( [API textError:error byOperation:operation] );
            faildBlock( operation, error, requestParams );
        }];
    } failedBlock:^(id item) {
        failed( item );
    }];
}

- (void)searchFriendsFromContacts:(prepareItems)preparePeople failedBlock:(prepareItem)failed
{
    if ( [APAddressBook access] == APAddressBookAccessGranted ) {
        APAddressBook *addressBook = [[APAddressBook alloc] init];
        
        addressBook.fieldsMask = APContactFieldEmails;
        
        addressBook.filterBlock = ^BOOL(APContact *contact) {
            return [contact.emails count] > 0;
        };
        
        [addressBook loadContacts:^(NSArray *contacts, NSError *error) {
            // hide activity
            if (!error)
            {
                NSMutableArray *items = [[NSMutableArray alloc] init];
                
                [contacts enumerateObjectsUsingBlock:^(APContact *obj, NSUInteger idx, BOOL *stop) {
                    [obj.emails enumerateObjectsUsingBlock:^(id email, NSUInteger idxj, BOOL *stopj) {
                        [items addObject:email];
                    }];
                }];
                
                preparePeople( items );
            }
            else
            {
                failed(@"нет контактов с email");
            }
        }];
    }
}


#pragma mark - Соц. Сети
- (void)setVKToken:(NSString *)token success:(prepareItem)success failedBlock:(prepareItem)failed
{
    [httpClient setVK:token
              success:^(id requestObject) {
                  if ( success ) {
                      JSONAPI *s = [JSONAPI jsonAPIWithDictionary:requestObject];
                      [Me setSocial:s.resource];
                      success(s.resource);
                  }
              }
          failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
              failedBlockText(operation, error, requestParams);
              if ( failed ) {
                  failedBlockText(operation, error, requestParams);
              }
          }];
}

- (void)setFBToken:(NSString *)token success:(prepareItem)success failedBlock:(prepareItem)failed
{
    [httpClient setFB:token
              success:^(id requestObject) {
                  if ( success ) {
                      JSONAPI *s = [JSONAPI jsonAPIWithDictionary:requestObject];
                      [Me setSocial:s.resource];
                      success(s.resource);
                  }
              }
          failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
              failedBlockText(operation, error, requestParams);
              if ( failed ) {
                  failed([API textError:error byOperation:operation]);
              }
          }];
}

- (void)unsetNetworkById:(uint32_t)socialId success:(prepareItem)success failedBlock:(prepareItem)failed
{
    [httpClient unsetNetwork:socialId
                     success:^(id requestObject) {
                         if ( success ) {
                             JSONAPI *s = [JSONAPI jsonAPIWithDictionary:requestObject];
                             [Me unsetSocialById:socialId];
                             success(s.resource);
                         }
                     }
                 failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                     failedBlockText(operation, error, requestParams);
                     if ( failed ) {
                         failed([API textError:error byOperation:operation]);
                     }
                 }];
}


#pragma mark -
#pragma mark Подписчики
- (void)searchFollowersFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getUserFollowersById:userId search:@{@"username": name} withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil && error.code != NSURLErrorCancelled  ) {
            fb();
        }
        
        faildBlock(operation, error, requestParams);
    }];
}

- (void)subscribe:(BOOL)state toUserId:(uint32_t)userId withCallBack:(prepareItem)callBack failedBlock:(prepareItem)failedBlock
{
    if ( state ) {
        [httpClient putFollower:[Me myProfileId] toUserId:userId withCallBack:^(id requestObject) {
            if ( callBack ) {
                callBack(requestObject);
            }
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            if ( failedBlock ) {
                failedBlock([API textError:error byOperation:operation]);
            }
            faildBlock(operation, error, requestParams);
        }];
    } else {
        [httpClient deleteFollower:[Me myProfileId] toUserId:userId withCallBack:^(id requestObject) {
            if ( callBack ) {
                callBack(requestObject);
            }
        } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
            if ( failedBlock ) {
                failedBlock([API textError:error byOperation:operation]);
            }
            faildBlock(operation, error, requestParams);
        }];
    }
}


#pragma mark -
#pragma mark Подписки
- (void)searchFollowsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getUserFollowsById:userId search:@{@"username": name} withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil && error.code != NSURLErrorCancelled ) {
            fb();
        }
        
        faildBlock(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Общие друзья
- (void)searchMutualFriendsFromUserId:(uint32_t)userId byName:(NSString *)name withCallback:(prepareItems)preparePeople withPage:(NSInteger)page failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getUserMutualFriendsById:userId search:@{@"username": name} withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil && error.code != NSURLErrorCancelled ) {
            fb();
        }
        
        faildBlock(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark Поиск людей
- (void)searchPeopleByName:(NSString *)name withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)fb
{
    if ( !httpClient.isReachable ) {
        if ( fb ) {
            fb(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getPeople:@{@"search" : @{@"username": name != nil ? name : @""}} withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *jsonApi = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople(jsonApi.resources);
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( fb != nil && error.code != NSURLErrorCancelled ) {
            fb();
        }
        
        faildBlock(operation, error, requestParams);
    }];
}

- (void)searchActivePeopleByName:(NSString *)name withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock
{
    [self searchActivePeople:@{@"search" : @{@"username": name != nil ? name : @""}} withPage:(NSUInteger)page withCallback:preparePeople failed:failedBlock];
}

- (void)searchActivePeopleByCategories:(NSArray *)categories withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock
{
    [self searchActivePeople:@{@"category_id": categories} withPage:(NSUInteger)page withCallback:preparePeople failed:failedBlock];
}

- (void)searchActivePeople:(NSDictionary *)search withPage:(NSUInteger)page withCallback:(prepareItems)preparePeople failed:(emptyCallBack)failedBlock
{
    if ( !httpClient.isReachable ) {
        if ( failedBlock ) {
            failedBlock(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient getActivePeople:search withPage:page prepareUser:^(id requestObject) {
        if ( preparePeople ) {
            JSONAPI *users = [JSONAPI jsonAPIWithDictionary:requestObject];
            
            preparePeople( users.resources );
        }
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        if ( failedBlock != nil && error.code != NSURLErrorCancelled ) {
            failedBlock();
        }
     
        faildBlock(operation, error, requestParams);
    }];
}


#pragma mark -
#pragma mark События
- (void)eventsWithOffset:(NSInteger)offset success:(prepareItems)prepareItems failed:(prepareItem)failed
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient eventsWithOffset:offset prepareUser:^(id requestObject) {
        
        [self parseEvents:requestObject success:prepareItems failed:failed];
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed != nil ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)eventsAfterUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    [httpClient eventsAfterUpdate:dateString prepareItems:^(id requestObject) {
        if ( update ) {
            [self parseEvents:requestObject success:update failed:failed];
        }
    } endItems:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)eventsBeforeUpdate:(NSString *)dateString withCallBack:(prepareItems)update withFailedCallBack:(prepareItem)failed
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient eventsBeforeUpdate:dateString prepareItems:^(id requestObject) {
        if ( update ) {
            [self parseEvents:requestObject success:update failed:failed];
        }
    } endItems:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}

- (void)newEventsCount:(prepareItem)prepareItem failedBlock:(prepareItem)fb
{
    [httpClient newEventsCount:^(id requestObject) {
        JSONAPI *items       = [JSONAPI jsonAPIWithDictionary:requestObject];
        prepareItem(items.resources);
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        fb( [error localizedDescription] );
        failedBlockText(operation, error, requestParams);
    }];
}

- (void)redeemEvents
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"STNewEventsCountUpdate" object:nil];
    [httpClient redeemEvents];
}


#pragma mark -
#pragma mark Лента
- (void)feedsAfterUpdate:(NSString *)dateString withCallBack:(prepareItems)prepareItems withFailedCallBack:(prepareItem)failed
{
    [httpClient feedsAfterUpdate:dateString prepareItems:^(id requestObject) {
        [self parseFeed:requestObject success:prepareItems failed:failed];
    } endItems:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];

}

- (void)feedsBeforeUpdate:(NSString *)dateString withCallBack:(prepareItems)prepareItems withFailedCallBack:(prepareItem)failed
{
    if ( !httpClient.isReachable ) {
        if ( failed ) {
            failed(@"");
        }
        
        [API notifyAboutNoInternetConnection];
        return;
    }
    
    [httpClient feedsBeforeUpdate:dateString prepareItems:^(id requestObject) {
        [self parseFeed:requestObject success:prepareItems failed:failed];
    } endItems:^(id requestObject) {
        
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock(operation, error, requestParams);
        if ( failed ) {
            failed([API textError:error byOperation:operation]);
        }
    }];
}


#pragma mark -
#pragma mark Лента
- (void)setContacts:(NSDictionary *)contacts goodBlock:(prepareItem)goodBlock failedBlock:(prepareItem)failed
{
    
    [httpClient setContacts:[[NSDictionary alloc] initWithObjects:[contacts allValues] forKeys:[contacts allKeys]]
                  goodBlock:^(id requestObject) {
                      JSONAPI *c = [JSONAPI jsonAPIWithDictionary:requestObject];
                    
                      goodBlock(c.resources);
                    
                  } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
                      failed([error localizedDescription]);
                      faildBlock(operation, error, requestParams);
                  }];
}


#pragma mark - Push
- (void)registerPushNotification:(NSString *)token success:(prepareItem)update failedBlock:(prepareItem)failedBlock
{
    [httpClient registerPushNotification:token success:^(id requestObject) {
        update( requestObject );
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock( operation, error, requestParams );
        failedBlock( [error localizedDescription] );
    }];
}

- (void)removePushNotification:(NSString *)token callback:(emptyCallBack)callbackFn
{
    if ( [token isEqualToString:@""] ) {
        if ( callbackFn ) {
            callbackFn();
        }
        
        return;
    }
    
    [httpClient removePushNotification:token callback:^(id requestObject) {
        callbackFn();
    } failedBlock:^(NSURLSessionDataTask *operation, NSError *error, NSDictionary *requestParams) {
        faildBlock( operation, error, requestParams );
    }];
}

@end
