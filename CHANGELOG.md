# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] [1.1.0]
### Added
* Open app by special url startwish://

### Fixed
* Add Link to wish

## [1.0.2]
### Added
* Information image for empty feed
* Information image for empty search wishes
* Go to comment screen by longtap to comment
* New category "Все" in Explorer screen
* Use landscape photos for add and exexute wish
* Use filters for add executed cover wish

### Changed
* Channel recomendation screen:
    * Add button "Пропустить"
* Privacy field by request create wishlist

### Fixed
* Open profile during load more people in friends/subscribers/subscriptions/people
* Longpress available only by wish press
* Wishlist trimmed in picker view in Select Wishlist PopUp
* Show error during try sharing wish by non installed instagram
* Available only 150 letters for comment